import math

import pandas as pd
from numpy import log, exp
from scipy.special import gammainc, gamma, digamma

from Scripts.pvt_new.core.storage.mongo import fetch
from Scripts.pvt_new.validation.validate import average


def calcMass(df_o, a, b, eta, plus=None):
    df = df_o.copy(deep=True)
    df.loc[df.cummziinside > 0.999999999999, "cummziinside"] = 0.999999999999
    df["qi"] = df.apply(
        lambda row: log(1 / (1 - (row.cummziinside if row.cummziinside < 0.99999999 else 0.99999999))), axis=1)
    df["incg"] = df.apply(lambda row: gammainc(1 + 1 / b, row.qi) * gamma(1 + 1 / b), axis=1)
    if plus is not None:
        q = 0.99999999
        qi = log(1 / (1 - q))
        incg = gammainc(1 + 1 / b, qi) * gamma(1 + 1 / b)
        df["pav"] = (1 / (1 - df.cummziinside.values)) * (a / b) ** (1 / b) * (incg - df.incg.values)
    else:
        df['cummziinsidediff'] = df['cummziinside'].sub(df['cummziinside'].shift())
        df.cummziinsidediff = df.cummziinsidediff.fillna(df['cummziinside'].iloc[0])
        df['incgdiff'] = df['incg'].sub(df['incg'].shift())
        df.incgdiff = df.incgdiff.fillna(df['incg'].iloc[0])
        df["pav"] = (1 / df.cummziinsidediff.values) * (a / b) ** (1 / b) * df.incgdiff.values
    df["mass"] = eta * (1 + df.pav.values)
    return df


def SumTermSpecial(b, qi, G9):
    index = 1 + 1 / b + 1
    st = 0
    i = 0
    while True:
        st_ind = (digamma(index)) * qi ** i / (gamma(index)) - (i * qi ** (i - 1) * G9 / gamma(index))
        if math.isnan(st_ind):
            st_ind = 0
        if abs(st_ind) < 10 ** -12:
            break
        st = st + st_ind
        index += 1
        i += 1
    st = exp(-qi) * qi ** (1 + 1 / b) * gamma(1 + 1 / b) * st
    return st


def DifferentiateSpecial(b, qi, incg, G9):
    st = SumTermSpecial(b, qi, G9)
    st = incg * (-1 * G9 + log(qi) + ((1 + 1 / b) / qi) * G9 + digamma(1 + 1 / b)) - st
    return (-1 / b ** 2) * st


def SumTerm(b, qi):
    index = 1 + 1 / b + 1
    st = 0
    while True:
        st_ind = (digamma(index)) * ((qi ** (index - 1)) / (gamma(index)))
        if math.isnan(st_ind):
            st_ind = 0
        if abs(st_ind) < 10 ** -12:
            break
        st = st + st_ind
        index += 1
    st = exp(-qi) * gamma(1 + 1 / b) * st
    return st


def Differentiate(b, qi, incg):
    st = SumTerm(b, qi)
    if qi == 0:
        qi = 10 ** -30
    return (-1 / b ** 2) * (incg * (log(qi) + digamma(1 + 1 / b)) - st)


def getValidatedData(well_conn):
    df = fetch(well_conn, property="pvt_output", identifier="validated_averaged_data", plus_or_individual="individual")
    dfplus = fetch(well_conn, property="pvt_output", identifier="validated_averaged_data", plus_or_individual="plus")
    df = average(df, df.index.min())
    return df, dfplus


def revertFractions(well_conn, df_o, dfplus_o):
    df_ind, df_plus = getValidatedData(well_conn)
    df_o["expmass"] = df_ind.expmass
    df_o["zimi"] = df_o.mf * df_o.mass
    dfplus_o.drop(dfplus_o.loc[(dfplus_o.index != 4) & (dfplus_o.index != 5)].index, inplace=True)
    dfplus = df_plus[["expmass", "scn"]]
    dfplus = pd.concat([dfplus, dfplus_o[["expmass", "scn"]]], sort=True)
    dfplus.sort_index(inplace=True)
    dfplus["mf"] = dfplus.apply(lambda row: df_o.loc[df_o.index >= row.name, "mf"].sum(), axis=1)
    dfplus["mass"] = dfplus.apply(
        lambda row: df_o.loc[df_o.index >= row.name, "zimi"].sum() / df_o.loc[df_o.index >= row.name, "mf"].sum(),
        axis=1)
    return df_o, dfplus
