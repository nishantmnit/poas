import numpy as np
import pandas as pd
from scipy import optimize

from Scripts.pvt_new.core.get_logger import get_logger
from common import revertFractions
from mass_input import mass_input
from start_point import findVarsStartingPoint
from Scripts.pvt_new.core.storage.save import save

logger = get_logger("pvt general mass solver 6")


def addPOASMass(iter_vars, df_o):
    df = df_o.copy(deep=True)
    df = pd.DataFrame()
    df["scn"] = ["C%s" % x for x in range(5, 122)]
    df.index = [x for x in range(5, 122)]
    df_o = pd.concat([df_o, df.loc[df.index > df_o.index.max()]], axis=0, sort=True)
    df["poas_mass_slope"] = df.apply(
        lambda row: (-0.0000000081143800838 * row.name ** 5 + 2.54535602253662E-06 * row.name ** 4 -
                     0.000278130020201878 * row.name ** 3 + 0.0132162236519363 * row.name ** 2 - 0.258386996099669 *
                     row.name + 15.4092766381938) if row.name <= 100
        else (0.509448043249933 * row.name ** 0.768554188962656), axis=1)
    df["poas_mass_plus_slope"] = df.poas_mass_slope
    df["poas_mass_slope"] = df.poas_mass_slope * iter_vars[0]
    df["poas_mass"] = df.apply(
        lambda row: 82.81 + df.loc[(df.index <= row.name) & (df.index >= [7]), "poas_mass_slope"].sum(), axis=1)
    df["mass_correction"] = (4.81668632961298 * np.exp(0.0140674955555049 * np.asarray(df.index))) * iter_vars[1]
    df["poas_mass"] = df.poas_mass + df.poas_mass * df.mass_correction / 100.
    df_o[["poas_mass", "poas_mass_plus_slope"]] = df[["poas_mass", "poas_mass_plus_slope"]]
    df_o.loc[df_o.index == 5, "poas_mass"] = 72.15
    df_o.loc[df_o.index == 6, "poas_mass"] = 85.21
    return df_o


def calculateMassPlus(df, dfplus):
    mass_plus, poas_mass_plus = list(), list()
    for index, row in df.iterrows():
        if 5 == index:
            mass = dfplus.loc[dfplus.index == index, "expmass"].values[0]
            poas_mass = mass
        elif index >= dfplus.index.max():
            mass = mass_plus[-1] + row.poas_mass_plus_slope
            poas_mass = poas_mass_plus[-1] + row.poas_mass_plus_slope
        else:
            prow = df.loc[df.index == row.name - 1]
            mass = (mass_plus[-1] * prow.zi_plus - prow.expmass * prow.mf) / row.zi_plus
            poas_mass = poas_mass_plus[-1] + row.poas_mass_plus_slope if index > 7 else mass
        mass_plus.append(mass)
        poas_mass_plus.append(poas_mass)
    df["mass_plus"] = mass_plus
    df["poas_mass_plus"] = poas_mass_plus
    df.loc[df.index == df.index.max(), "mass"] = df.loc[df.index == df.index.max(), "mass_plus"]
    return df


def calculateZi(df, dfplus):
    zi_plus, zi = list(), list()
    for index, row in df.iterrows():
        if index < dfplus.index.max() - 1:
            zi.append(row.mf)
            zi_plus.append(row.zi_plus)
        elif index == dfplus.index.max() - 1:
            zi_plus.append(row.zi_plus)
            zi_plus[-1] = 0 if zi_plus[-1] <= 10 ** -20 else zi_plus[-1]
            try:
                mass_plus = df.loc[df.index == row.name + 1, "mass_plus"].values[0]
            except:
                mass_plus = 0
            zi.append(zi_plus[-1] * (mass_plus - row.mass_plus) / (mass_plus - row.mass))
        else:
            zi_plus.append(zi_plus[-1] - zi[-1])
            try:
                mass_plus = df.loc[df.index == row.name + 1, "mass_plus"].values[0]
            except:
                mass_plus = 0
            zi.append(zi_plus[-1] * (mass_plus - row.mass_plus) / (mass_plus - row.mass))
            zi[-1] = 10 ** -20 if zi[-1] <= 10 ** -20 else zi[-1]
    df["zi_plus"] = zi_plus
    df["zi"] = zi
    return df


def calculateMass(iter_vars, df_o, dfplus):
    df_o = addPOASMass(iter_vars, df_o)
    df = df_o.copy(deep=True)
    df["mass"] = df.apply(lambda row: row.expmass if row.name < dfplus.index.max() else row.poas_mass, axis=1)
    df["zi_plus"] = df.apply(lambda row: dfplus.loc[dfplus.index == 5, "mf"].values[0] - df.loc[
        df.index < row.name, "mf"].sum() if row.name <= dfplus.index.max() else np.NaN, axis=1)
    df = calculateMassPlus(df, dfplus)
    df = calculateZi(df, dfplus)
    df["final_zi"] = df.apply(lambda row: row.mf if row.name < dfplus.index.max() else row.zi, axis=1)
    df["zimi"] = df.final_zi * df.mass
    df_o[["zi", "final_zi", "mass", "zimi", "poas_mass", "mass_plus", "poas_mass_plus"]] = df[
        ["zi", "final_zi", "mass", "zimi", "poas_mass", "mass_plus", "poas_mass_plus"]]
    dfplus["zi"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "final_zi"].sum(), axis=1)
    dfplus["mass"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "zimi"].sum() / row.zi, axis=1)
    return df_o, dfplus


def calculateErrors(iter_vars, df_o, dfplus):
    df, dfplus = calculateMass(iter_vars, df_o, dfplus)
    df["zi_error"] = np.abs((df.mf - df.zi) / df.mf)
    df["mass_error"] = np.abs((df.poas_mass - df.expmass) / df.expmass)
    dfplus["zi_error"] = np.abs((dfplus.mf - dfplus.zi) / dfplus.mf)
    dfplus["mass_error"] = np.abs((dfplus.mass - dfplus.expmass) / dfplus.expmass)
    df["poas_error"] = np.abs((df.mass_plus - df.poas_mass_plus) / df.mass_plus)
    return df, dfplus


def targetCost(val, target):
    return val / target


def cost_function(iter_vars, df_o, dfplus):
    df, dfplus = calculateErrors(iter_vars, df_o, dfplus)
    last_mf_ratio = (
        0 if df.loc[df.index == 121, "zi"].values[0] == 0 or df.loc[df.index == 120, "zi"].values[0] == 0 else
        df.loc[df.index == 121, "zi"].values[0] / df.loc[df.index == 120, "zi"].values[0])
    last_mass_ratio = (
        0 if df.loc[df.index == 121, "mass"].values[0] == 0 or df.loc[df.index == 120, "mass"].values[0] == 0 else
        df.loc[df.index == 121, "mass"].values[0] / df.loc[df.index == 120, "mass"].values[0])
    error = targetCost(dfplus.loc[dfplus.index == dfplus.index.max(), "zi_error"].sum(), 0.001)
    error += targetCost(dfplus.loc[dfplus.index == dfplus.index.max(), "mass_error"].sum(), 0.01)
    error += targetCost(df.loc[df.index <= dfplus.index.max(), "poas_error"].sum(), 0.01)
    error += targetCost(df.loc[df.index == dfplus.index.max() - 1, "zi_error"].sum(), 0.001)
    error += targetCost(df.loc[df.index == dfplus.index.max() - 1, "mass_error"].sum(), 0.01)
    error += targetCost(np.abs(1.3 - last_mf_ratio) / 1.3, 0.1)
    error += targetCost(np.abs(1.3 - last_mass_ratio) / 1.3, 0.1)
    return error


def calculateFinalMass(res, df, dfplus):
    iter_vars = res.x
    df, dfplus = calculateMass(iter_vars, df, dfplus)
    df["mass"] = df.apply(lambda row: row.expmass if row.name < dfplus.index.max() else row.mass, axis=1)
    df["mf"] = df.apply(lambda row: row.mf if row.name < dfplus.index.max() else row.zi, axis=1)
    df["zimi"] = df.mf * df.mass
    dfplus["mass"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "zimi"].sum() / row.zi, axis=1)
    df.drop(columns=["zi"], inplace=True)
    dfplus.drop(columns=["zi"], inplace=True)
    return df, dfplus


def process_result(res, df, dfplus, user_args):
    df, dfplus = calculateFinalMass(res, df, dfplus)
    df, dfplus = revertFractions(user_args.well_conn, df, dfplus)
    divisor = (dfplus.loc[dfplus.index == 5, "expmass"].values[0] * dfplus.loc[dfplus.index == 5, "mf"].values[0])
    df["wf"] = (df["mass"] * df["mf"]) / divisor
    save(df, user_args, identifier="mass", plus_or_individual="individual", solver=6, distribution="general")
    save(dfplus, user_args, identifier="mass", plus_or_individual="plus", solver=6, distribution="general")
    return True


def solver6(user_args):
    logger.info("Executing General Mass Solver 6.")
    df, dfplus = mass_input(6, user_args.well_conn)
    # The Starting Point of Solver 6, is required to be assigned from SOLVE5 as Starting Point4-5_Desi logic
    start_point = findVarsStartingPoint(df, dfplus)
    start_point = [start_point[2], start_point[1]]
    bound = [[0.95, 1.1], [-1.5, 2.5]]
    res = optimize.minimize(cost_function, np.asarray(start_point), bounds=bound, method="SLSQP", args=(df, dfplus),
                            options={'maxiter': 500})
    df_res, dfplus_res = calculateFinalMass(res, df, dfplus)
    # In case any of the Zi <=10^-20, then print the results at starting point
    if not res.success or df_res.loc[(df_res.index >= dfplus_res.index.max()) & (df_res.mf < 10 ** -20)].shape[0] >= 1:
        logger.debug("%s" % res)
        logger.info("General Mass Solver 6 failed to find a feasible solution.")
        logger.info(
            "Replacing solver variables with starting point and returning the output on start point as final output")
        res.x = start_point
        res.success = True
    logger.info("Completed POAS Special Mass Solver 6.")
    logger.info(
        "General Mass solver final variables - %s, objective - %s" % (",".join([str(x) for x in res.x]), res.fun))
    return process_result(res, df, dfplus, user_args)


"""
Executed only when mass solver 5 fails
"""
