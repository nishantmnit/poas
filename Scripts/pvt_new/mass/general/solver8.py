from Scripts.pvt_new.core.get_logger import get_logger
from common import revertFractions
from solver7 import runSolver, calculateMass
from wismooth_new import smooth
from Scripts.pvt_new.mass.gamma.mass_input import mass_input
from Scripts.pvt_new.core.storage.save import save

logger = get_logger("pvt general mass solver 7")


def solver8(user_args):
    logger.info("Executing General Mass Solver 8")
    df, dfplus = mass_input(2, user_args.well_conn)
    success, df = smooth(df, dfplus, 8)
    res, df, dfplus = runSolver(df, dfplus, 8)
    return process_result(res, df, dfplus, user_args)


def process_result(res, df, dfplus, user_args):
    df, dfplus = calculateMass(res.x, df, dfplus)
    df, dfplus = revertFractions(user_args.well_conn, df, dfplus)
    divisor = df.zimi.sum()
    df["wf"] = (df["mass"] * df["mf"]) / divisor
    save(df, user_args, identifier="mass", plus_or_individual="individual", solver=8,
         distribution="general")
    save(dfplus, user_args, identifier="mass", plus_or_individual="plus", solver=8, distribution="general")
    return True


"""
executed when input max plus fraction is > 19
"""
