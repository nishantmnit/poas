import numpy as np
from scipy import optimize

from Scripts.pvt_new.core.get_logger import get_logger

logger = get_logger("pvt mass wi smooth")


def get_slope(df, scn, bracket):
    max_wi = df.wiinside.max()
    max_wi_scn = df.loc[df.wiinside == max_wi].index.min()
    if max_wi_scn == (bracket[0] + bracket[1]) / 2.:
        if scn == bracket[1]:
            return (df.loc[df.index == bracket[1], "wiinside"].values[0] - max_wi) / (bracket[1] - max_wi_scn)
        else:
            return (max_wi - df.loc[df.index == bracket[0], "wiinside"].values[0]) / (max_wi_scn - bracket[0])
    else:
        return (df.loc[df.index == bracket[1], "wiinside"].values[0] -
                df.loc[df.index == bracket[0], "wiinside"].values[0]) / (bracket[1] - bracket[0])


def find_bracket(scn, bracket=None):
    if bracket is not None and scn <= bracket[1]:
        return bracket
    brackets = [[6, 8], [8, 10], [10, 12], [12, 14]]
    for bracket in brackets:
        if bracket[0] <= scn <= bracket[1]:
            return bracket


def add_wi(df, dfplus):
    df["zimi"] = df.mf * df.mass
    df["wiinside"] = df.zimi / df.zimi.sum()
    new_wi = list()
    bracket = None
    for scn in range(5, 122):
        if scn < dfplus.index.max() or scn <= 6 or scn >= 15:
            new_wi.append(df.loc[df.index == scn, "wiinside"].values[0])
        else:
            bracket = find_bracket(scn, bracket)
            slope = get_slope(df, scn, bracket)
            new_wi.append(new_wi[-1] + slope)
    df["new_wiinside"] = new_wi
    return df


def prepare_for_solver(df):
    zimisum = df.zimi.sum()
    df["zimi"] = df.new_wiinside * zimisum
    df["zi"] = df.zimi / df.mass
    return df


def fix_zi_mass(df, dfplus):
    if dfplus.loc[dfplus.index == dfplus.index.max(), "mass"].values[0] != \
            dfplus.loc[dfplus.index == dfplus.index.max(), "expmass"].values[0]:
        multiplier = dfplus.loc[dfplus.index == dfplus.index.max(), "expmass"].values[0] / \
                     dfplus.loc[dfplus.index == dfplus.index.max(), "mass"].values[0]
        df.loc[df.index >= dfplus.index.max(), "mass"] = df.loc[df.index >= dfplus.index.max(), "mass"] * multiplier
    if dfplus.loc[dfplus.index == dfplus.index.max(), "mf"].values[0] != \
            dfplus.loc[dfplus.index == dfplus.index.max(), "zi"].values[0]:
        multiplier = dfplus.loc[dfplus.index == dfplus.index.max(), "mf"].values[0] / \
                     dfplus.loc[dfplus.index == dfplus.index.max(), "zi"].values[0]
        df.loc[df.index >= dfplus.index.max(), "zi"] = df.loc[df.index >= dfplus.index.max(), "zi"] * multiplier
    df["zimi"] = df.zi * df.mass
    dfplus["zi"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "zi"].sum(), axis=1)
    dfplus["mass"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "zimi"].sum() / row.zi, axis=1)
    return df, dfplus


def calculate_mass(iter_vars, df_o, dfplus):
    df = df_o
    df.loc[df.index >= dfplus.index.max(), "zi"] = df.loc[df.index >= dfplus.index.max(), "zi"] * iter_vars[0]
    df["zimi"] = df.zi * df.mass
    dfplus["zi"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "zi"].sum(), axis=1)
    dfplus["mass"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "zimi"].sum() / row.zi, axis=1)
    return df, dfplus


def target_cost(val, target):
    return val / target


def calculate_final_mass(res, df_o, dfplus):
    iter_vars = res.x
    df, dfplus = calculate_mass(iter_vars, df_o, dfplus)
    df, dfplus = fix_zi_mass(df, dfplus)
    df["mf"] = df.zi
    dfplus["mf"] = dfplus.zi
    df.drop(columns=["zi"], inplace=True)
    dfplus.drop(columns=["zi"], inplace=True)
    return df, dfplus


def cost_function(iter_vars, df_o, dfplus):
    df, dfplus = calculate_mass(iter_vars, df_o, dfplus)
    dfplus["zi_error"] = np.abs((dfplus.mf - dfplus.zi) / dfplus.mf)
    dfplus["mass_error"] = np.abs((dfplus.mass - dfplus.expmass) / dfplus.expmass)
    error = target_cost(dfplus.loc[dfplus.index == dfplus.index.min(), "zi_error"].sum(), 0.001)
    error += target_cost(dfplus.loc[dfplus.index == dfplus.index.max(), "zi_error"].sum(), 0.001)
    error += target_cost(dfplus.loc[dfplus.index == dfplus.index.max(), "mass_error"].sum(), 0.01)
    return error


def smooth(df, dfplus):
    df = add_wi(df, dfplus)
    df = prepare_for_solver(df)
    logger.info("Executing WI Smoothing Solver")
    res = optimize.minimize(cost_function, np.asarray(1.), bounds=[[0.5, 2.]], method="SLSQP", args=(df, dfplus),
                            options={'maxiter': 500})
    if not res.success:
        print res
        msg = 'Error:{"Type": "Mass Solver", "Message": "Zi Smoothing, Mass Solver failed to find ' \
              'a feasible solution. Please check input data. If issue persists, please raise with POAS support team."}'
        raise Exception(msg)
    logger.info(
        "WI smooth solver final variables - %s, objective - %s" % (",".join([str(x) for x in res.x]), res.fun))
    df, dfplus = calculate_final_mass(res, df, dfplus)
    return df, dfplus
