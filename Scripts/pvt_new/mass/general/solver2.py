import numpy as np
from numpy import exp, ceil, log
from scipy.special import gammainc, gamma

from Scripts.pvt_new.core.get_logger import get_logger
from Scripts.pvt_new.core.optimizers.generic_solver import GenericSolver
from common import Differentiate, calcMass
from common import revertFractions
from mass_input import mass_input
from mass_smooth import smooth
from Scripts.pvt_new.core.storage.save import save

logger = get_logger("pvt general mass solver 2")


def costPlus(iter_vars, args):
    a, b, eta = iter_vars
    dfplus = args[1].copy(deep=True)
    dfplus = calcMass(dfplus, a, b, eta, 1)
    dfplus = dfplus.loc[dfplus.index == dfplus.index.max()]
    dfplus["error"] = ((dfplus.expmass - dfplus.mass) / dfplus.expmass) ** 2
    q = 0.99999999
    qi = log(1 / (1 - q))
    incg = gammainc(1 + 1 / b, qi) * gamma(1 + 1 / b)
    lastdifferentiation = Differentiate(b, qi, incg)
    dfplus["diffincg"] = dfplus.apply(lambda row: Differentiate(b, row.qi, row.incg), axis=1)
    dfplus["diffmasswrtb"] = (1 / (1 - dfplus.cummziinside.values)) * (a / b) ** (1 / b) * (
            (incg - dfplus.incg.values) * (-(log(a / b) + 1) / b ** 2) +
            (lastdifferentiation - dfplus.diffincg.values)) * eta
    dfplus["diffmasswrta"] = dfplus.pav.values * eta / (a * b)
    dfplus["diffmasswrteta"] = 1 + dfplus.pav.values
    dfplus["differrorwrta"] = dfplus.diffmasswrta.values * 2 * (
            (dfplus.mass.values - dfplus.expmass.values) / dfplus.expmass.values ** 2)
    dfplus["differrorwrtb"] = dfplus.diffmasswrtb.values * 2 * (
            (dfplus.mass.values - dfplus.expmass.values) / dfplus.expmass.values ** 2)
    dfplus["differrorwrteta"] = dfplus.diffmasswrteta.values * 2 * (
            (dfplus.mass.values - dfplus.expmass.values) / dfplus.expmass.values ** 2)
    dfplus["derivativea"] = dfplus.differrorwrta.values * dfplus.error.values
    dfplus["derivativeb"] = dfplus.differrorwrtb.values * dfplus.error.values
    dfplus["derivativeeta"] = dfplus.differrorwrteta.values * dfplus.error.values
    avg = dfplus.expmass.mean()
    dfplus["r21"] = (dfplus.mass.values - dfplus.expmass.values) ** 2
    dfplus["r22"] = (dfplus.mass.values - avg) ** 2
    r2 = 1 - dfplus.r21.sum() / dfplus.r22.sum()
    dfplus.drop(["r21", "r22"], axis=1, inplace=True)
    return dfplus["error"].sum(), [dfplus["derivativea"].sum(), dfplus["derivativeb"].sum(),
                                   dfplus["derivativeeta"].sum()], r2


def costIndividual(iter_vars, args):
    a, b, eta = iter_vars
    df = args[0].copy(deep=True)
    df = calcMass(df, a, b, eta)
    df["error"] = ((df.expmass - df.mass) / df.expmass) ** 2
    df["diffincg"] = df.apply(lambda row: Differentiate(b, row.qi, row.incg), axis=1)
    df["diffincgdiff"] = df['diffincg'].sub(df['diffincg'].shift())
    df.diffincgdiff = df.diffincgdiff.fillna(df['diffincg'].iloc[0])
    df = df.loc[df.index >= df.index.max() - 1]
    df["diffmasswrta"] = df.pav.values * eta / (a * b)
    df["diffmasswrteta"] = 1 + df.pav.values
    df["diffmasswrtb"] = (1 / df.cummziinsidediff.values) * (a / b) ** (1 / b) * (
            df.incgdiff.values * (-(log(a / b) + 1) / b ** 2) + df.diffincgdiff.values) * eta
    df["differrorwrta"] = df.diffmasswrta.values * 2 * (
            (df.mass.values - df.expmass.values) / df.expmass.values ** 2)
    df["differrorwrtb"] = df.diffmasswrtb.values * 2 * (
            (df.mass.values - df.expmass.values) / df.expmass.values ** 2)
    df["differrorwrteta"] = df.diffmasswrteta.values * 2 * (
            (df.mass.values - df.expmass.values) / df.expmass.values ** 2)
    df["derivativea"] = df.differrorwrta.values * df.error.values
    df["derivativeb"] = df.differrorwrtb.values * df.error.values
    df["derivativeeta"] = df.differrorwrteta.values * df.error.values
    avg = df.expmass.mean()
    df["r21"] = (df.mass.values - df.expmass.values) ** 2
    df["r22"] = (df.mass.values - avg) ** 2
    r2 = 1 - df.r21.sum() / df.r22.sum()
    df.drop(["r21", "r22"], axis=1, inplace=True)
    return df["error"].sum(), [df["derivativea"].sum(), df["derivativeb"].sum(), df["derivativeeta"].sum()], r2


def cost_function(iter_vars, args):
    error1, differentiations1, r = costIndividual(iter_vars, args)
    error2, differentiations2, r2 = costPlus(iter_vars, args)
    differentiations = list()
    for i in range(0, len(differentiations1)):
        differentiations.append(differentiations1[i] + differentiations2[i])
    return error1 + error2, differentiations, r


def maximizeR2(df_o, bounds):
    df = df_o.copy(deep=True)
    count = df.shape[0]
    eta = bounds["Eta"][0]
    result = dict()
    result["r2"] = list()
    result["iter_iter_vars"] = list()
    while eta < df.expmass.min():
        df["yi"] = df.apply(lambda row: log((row.expmass - eta) / eta), axis=1)
        df["xiyi"] = df.xi.values * df.yi.values
        df["yi2"] = df.yi.values ** 2
        r2 = (count * df.xiyi.sum() - df.xi.sum() * df.yi.sum()) ** 2 / (
                (count * df.xi2.sum() - df.xi.sum() ** 2) * (count * df.yi2.sum() - df.yi.sum() ** 2))
        c2 = (df.xi.sum() * df.yi.sum() - count * df.xiyi.sum()) / (df.xi.sum() ** 2 - count * df.xi2.sum())
        c1 = (df.yi.sum() - c2 * df.xi.sum()) / count
        b = 1 / c2
        a = b * exp(c1 * b)
        result["r2"].append(r2)
        result["iter_iter_vars"].append([a, b, eta])
        df.drop(["yi", "xiyi", "yi2"], axis=1, inplace=True)
        eta += 0.1
    maxima = result["r2"].index(max(result["r2"]))
    bounds["A"] = [0.2, ceil(result["iter_iter_vars"][maxima][0] * 3)]
    bounds["B"] = [0.2, ceil(result["iter_iter_vars"][maxima][1] * 3)]
    return result["iter_iter_vars"][maxima]


def get_bounds(df):
    bounds = dict()
    bounds["Eta"] = [10., 70.]
    df['xzci'] = df.cummziinside / 2 + df.cummziinside.shift() / 2
    df.xzci = df.xzci.fillna(df['cummziinside'].iloc[0] / 2)
    df["xi"] = df.apply(lambda row: log(log(1 / (1 - row.xzci))), axis=1)
    df["xi2"] = df.xi ** 2
    A, B, eta = maximizeR2(df.copy(deep=True), bounds)
    initialEtaFn = (eta - bounds["Eta"][0]) / (bounds["Eta"][1] - bounds["Eta"][0])
    initialAFn = (A - bounds["A"][0]) / (bounds["A"][1] - bounds["A"][0])
    initialBFn = (B - bounds["B"][0]) / (bounds["B"][1] - bounds["B"][0])
    return ([initialAFn, initialBFn, initialEtaFn],
            [[bounds["A"][0], bounds["A"][1]], [bounds["B"][0], bounds["B"][1]],
             [bounds["Eta"][0], bounds["Eta"][1]]])


def process_result(res, df, dfplus, user_args):
    a, b, eta = res["Values"]
    df = calcMass(df, a, b, eta)
    df["mass"] = df.apply(lambda row: row.expmass if row.name < dfplus.index.max() - 2 else row.mass, axis=1)
    df["zimi"] = df.mf * df.mass
    dfplus["mass"] = dfplus.apply(
        lambda row: df.loc[df.index >= row.name, "zimi"].sum() / df.loc[df.index >= row.name, "mf"].sum(), axis=1)
    logger.info("Trying to smoothen output.")
    df, dfplus = revertFractions(user_args.well_conn, df, dfplus)
    df, dfplus = smooth(df, dfplus, df.loc[~df.expmass.isnull()].index.max(), 2)
    divisor = (dfplus.loc[dfplus.index == 4, "expmass"].values[0] * dfplus.loc[dfplus.index == 4, "mf"].values[0])
    df["wf"] = (df["mass"] * df["mf"]) / divisor
    if df.index.max() >= dfplus.index.max():
        save(df, user_args, identifier="mass", plus_or_individual="individual", solver=2,
             distribution="general")
        save(dfplus, user_args, identifier="mass", plus_or_individual="plus", solver=2, distribution="general")
        return True
    logger.info("Max Plus fraction is less than user provided plus fraction. Discarding output.")
    return False


def solver2(user_args):
    df, dfplus = mass_input(2, user_args.well_conn)
    df_intermediate = df.loc[~np.isnan(df.expmass)].copy(deep=True)
    start_points, bounds = get_bounds(df_intermediate)
    res = run_solver2(start_points, bounds, df_intermediate, dfplus, user_args, 2)
    return process_result(res, df, dfplus, user_args)


def run_solver2(start_points, bounds, df, dfplus, user_args, printname):
    logger.info("Executing General Mass Solver %s" % printname)
    res = GenericSolver(start_points, bounds, cost_function, debug=user_args.debug,
                        args=(df.copy(deep=True), dfplus.copy(deep=True)))
    if not res["success"]:
        logger.debug(res)
        logger.info("General Mass Solver %s failed to find a feasible solution." % printname)
        return "Failed", df, dfplus
    logger.info("Result -- %s" % res)
    logger.info("Completed General Mass Solver %s" % printname)
    return res


"""
executed when user provided max plus fraction is > 19
"""

if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection
    import pandas as pd
    from Scripts.pvt_new.settings import DEBUG_DIR
    import os

    well_name = "ALUMM"

    debug_dir = os.path.join(DEBUG_DIR, well_name)

    db = get_connection(well_name)
    test_user_args = pd.Series({"debug": True, "well_conn": db, "bias": "general", "debug_dir": debug_dir})
    solver2(test_user_args)
