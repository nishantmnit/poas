import math

import numpy as np
import pandas as pd
from scipy import optimize
from scipy.special import gammainc

from Scripts.pvt_new.core.get_logger import get_logger
from mass_input import mass_input
from solver5 import solver5
from wismooth import smooth

logger = get_logger("pvt gamma bias mass solver 1")


def calculateFinalMass(df, dfplus):
    df["mass"] = df.apply(lambda row: row.expmass if row.name < dfplus.index.max() else row.mass, axis=1)
    df["mf"] = df.apply(lambda row: row.mf_bkp if row.name < dfplus.index.max() else row.mf, axis=1)
    df["zimi"] = df.mf * df.mass
    dfplus["mass"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "zimi"].sum() / row.mf, axis=1)
    return df, dfplus


def targetCost(val, target):
    if math.isnan(val):
        val = 0
    return val / target


def addPOASMass(i_vars, df_o):
    df = df_o.copy(deep=True)
    df = pd.DataFrame()
    df["scn"] = ["C%s" % x for x in range(5, 122)]
    df.index = [x for x in range(5, 122)]
    df_o = pd.concat([df_o, df.loc[df.index > df_o.index.max()]], axis=0, sort=True)
    df["poas_mass_slope"] = i_vars[1] * df.apply(lambda row: (
            -0.0000000081143800838 * row.name ** 5 + 2.54535602253662E-06 * row.name ** 4 -
            0.000278130020201878 * row.name ** 3 + 0.0132162236519363 * row.name ** 2 - 0.258386996099669 *
            row.name + 15.4092766381938) if row.name <= 100 else (
            0.509448043249933 * row.name ** 0.768554188962656), axis=1)
    df["poas_mass"] = df.apply(
        lambda row: 82.81 + df.loc[(df.index <= row.name) & (df.index >= np.asarray(7)), "poas_mass_slope"].sum(),
        axis=1)
    df["mass_correction"] = (4.81668632961298 * np.exp(0.0140674955555049 * np.asarray(df.index))) * i_vars[0]
    df["poas_mass"] = df.poas_mass + df.poas_mass * df.mass_correction / 100.
    df_o["poas_mass"] = df["poas_mass"]
    df_o.loc[df_o.index == 5, "poas_mass"] = df_o.loc[df_o.index == 5, "expmass"]
    df_o.loc[df_o.index == 6, "poas_mass"] = df_o.loc[df_o.index == 6, "expmass"]
    return df_o


def calculateMass(i_vars, df_o, dfplus):
    df_o = addPOASMass(i_vars, df_o)
    df = df_o

    df["mb"] = (df.poas_mass + df.poas_mass.shift(-1)) / 2.
    df.loc[df.index == df.index.max(), "mb"] = 10000.

    alpha, eta = i_vars[2], i_vars[3]
    beta = (dfplus.loc[dfplus.index == 5, "expmass"].values[0] - eta) / alpha

    df["y"] = (df.mb - eta) / beta
    df["p0"] = df.apply(lambda row: gammainc(alpha, row.y), axis=1)
    df["p1"] = df.apply(lambda row: gammainc(alpha + 1, row.y), axis=1)
    df['p0diff'] = df['p0'].sub(df['p0'].shift())
    df["p0diff"] = df["p0diff"].fillna(df['p0'].iloc[0])
    df['p1diff'] = df['p1'].sub(df['p1'].shift())
    df['p1diff'] = df['p1diff'].fillna(df['p1'].iloc[0])
    df.loc[df.p0diff == 0, "p0diff"] = 1.
    df["mass"] = eta + alpha * beta * (df.p1diff / df.p0diff)

    df["ziinside"] = df.p0 - df.p0.shift(1)
    df.loc[df.index == df.index.min(), 'ziinside'] = df['p0'].iloc[0]
    df["zi"] = df.ziinside * dfplus.loc[dfplus.index == 5, "mf"].values[0]
    df["zimi"] = df.zi * df.mass
    df_o[["zi", "mass", "zimi"]] = df[["zi", "mass", "zimi"]]
    dfplus["zi"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "zi"].sum(), axis=1)
    dfplus["mass"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "zimi"].sum() / row.zi, axis=1)
    return df_o, dfplus


def calculateErrors(i_vars, df_o, dfplus):
    df, dfplus = calculateMass(i_vars, df_o, dfplus)
    dfplus["zi_error"] = ((dfplus.mf - dfplus.zi) / dfplus.mf) ** 2
    dfplus["mass_error"] = ((dfplus.mass - dfplus.expmass) / dfplus.expmass) ** 2
    return df, dfplus


def cost_function(i_vars, df_o, dfplus):
    df, dfplus = calculateErrors(i_vars, df_o, dfplus)
    error = 0
    for index, row in dfplus.iterrows():
        error += targetCost(dfplus.loc[dfplus.index == index, "zi_error"].values[0],
                            0.001) if index >= 6 else 0
        error += targetCost(dfplus.loc[dfplus.index == index, "mass_error"].values[0],
                            0.001) if index >= 6 else 0
    return error


def prepare_data_for_solver5(res, df, dfplus):
    df, dfplus = calculateMass(res.x, df, dfplus)
    df[["mf", "mf_bkp"]] = df[["zi", "mf"]]
    logger.info("Trying to smoothen output.")
    df, dfplus = smooth(df, dfplus)
    df, dfplus = calculateFinalMass(df, dfplus)

    # create 16+
    dfplus["zimi"] = dfplus.expmass * dfplus.mf
    df["zimi"] = df.mass * df.mf
    mf = dfplus.loc[dfplus.index == dfplus.index.max(), "mf"].values[0] - df.loc[
        (df.index >= dfplus.index.max()) & (df.index < 16), "mf"].sum()
    mass = (dfplus.loc[dfplus.index == dfplus.index.max(), "zimi"].values[0] - df.loc[
        (df.index >= dfplus.index.max()) & (df.index < 16), "zimi"].sum()) / mf
    dfplus_temp = pd.DataFrame(data=[[mf, mass, mass, "C16"]], columns=["mf", "expmass", "mass", "scn"], index=[16])
    df.drop(df.loc[df.index >= 16].index, inplace=True)
    df["expmass"] = df.mass
    dfplus = pd.concat([dfplus, dfplus_temp], axis=0, sort=True)
    return df, dfplus


def solver1_gamma(user_args):
    logger.info("Executing Gamma Mass Solver 1")
    df, dfplus = mass_input(1, user_args.well_conn)

    mc5 = dfplus.loc[dfplus.index == 5, "expmass"].values[0]
    a = max(0.9, (1.22905198035228 * np.log(mc5) - 5.1827424270118))
    sp = [0., 1, a, 70.]
    res = optimize.minimize(cost_function, np.asarray(sp), bounds=[[-1.2, 2.], [0.95, 1.1], [0.5, 2.25 * a],
                                                                   [35., 72.1]], method="SLSQP", args=(df, dfplus),
                            options={'maxiter': 500})
    if not res.success:
        logger.debug(res)
        logger.info(
            "Gamma Mass Solver 1 failed to find a feasible solution. Considering start point as final result.")
        res.x = sp
    logger.info("Completed Gamma Special Mass Solver 1")
    logger.info(
        "Gamma Mass solver final variables - %s, objective - %s" % (",".join([str(x) for x in res.x]), res.fun))
    df, dfplus = prepare_data_for_solver5(res, df, dfplus)
    return solver5(user_args, df, dfplus)


"""
Executed when user selected bias is gamma and user provided max plus fraction is 9+
result of this solver is not saved, instead, solver5 is executed after completion of this.
"""
