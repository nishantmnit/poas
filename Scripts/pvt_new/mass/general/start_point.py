from collections import OrderedDict

import numpy as np
import pandas as pd
from scipy.interpolate import PchipInterpolator


def get_slope(scn):
    return (-0.0000554601725806236 * scn ** 5 + 0.00754033655953279 * scn ** 4 -
            0.230779871372405 * scn ** 3 + 2.48690642287389 * scn ** 2 -
            6.95180703002185 * scn + 1.50595054461093)


def get_iter_vars3():
    x = 1.1
    iter_vars = list()
    while x >= 0.95:
        iter_vars.append(x)
        x -= 0.005
    return iter_vars


def var2_3(df_o, dfplus):
    df = df_o.copy(deep=True)
    df.drop(df.loc[(df.index < 7) | (df.index >= dfplus.index.max())].index, inplace=True)
    iter_vars3 = get_iter_vars3()
    iter_vars, errors = list(), list()
    for var3 in iter_vars3:
        df["poas_mass_slope"] = var3 * df.apply(
            lambda row: (-0.0000000081143800838 * row.name ** 5 + 2.54535602253662E-06 * row.name ** 4 -
                         0.000278130020201878 * row.name ** 3 + 0.0132162236519363 * row.name ** 2 -
                         0.258386996099669 * row.name + 15.4092766381938) if row.name <= 100
            else (0.509448043249933 * row.name ** 0.768554188962656), axis=1)
        df["poas_mass"] = df.apply(
            lambda row: 82.81 + df.loc[(df.index <= row.name) & (df.index >= 7), "poas_mass_slope"].sum(), axis=1)
        mass_correction_last_scn = (4.81668632961298 * np.exp(0.0140674955555049 * df.index.max()))
        var2 = (df.loc[df.index == df.index.max(), "expmass"].values[0] * 100. -
                df.loc[df.index == df.index.max(), "poas_mass"].values[0] * 100.) / (
                       df.loc[df.index == df.index.max(), "poas_mass"].values[0] * mass_correction_last_scn)
        var2 = min(max(-1.2, var2), 2)
        df["poas_mass"] = df.poas_mass + df.poas_mass * var2 * (
                4.81668632961298 * np.exp(0.0140674955555049 * df.index)) / 100.
        df["error"] = np.abs(df.expmass - df.poas_mass) / df.expmass
        iter_vars.append([var2, var3])
        errors.append(df.error.sum())
    return iter_vars[errors.index(min(errors))]


def slopeNumber(var1):
    slope15 = get_slope(15) * var1
    slope16 = get_slope(16) * var1
    slope = (np.log(slope16) - np.log(slope15)) / (np.log(16) - np.log(15))
    return slope16, slope


def fifthVar(dfplus, var1, var4):
    slope16, slope = slopeNumber(var1)
    last_plus = dfplus.index.max()
    slope_at = max(35, last_plus + 15)
    slope1 = np.exp(np.log(slope16) + slope * var4 * (np.log(slope_at + 4) - np.log(16)))
    slope2 = np.exp(np.log(slope16) + slope * var4 * (np.log(slope_at + 5) - np.log(16)))
    slope = (np.log(slope2) - np.log(slope1)) / (np.log(slope_at + 5) - np.log(slope_at + 4))
    var = (np.log(slope2 / 2. if slope2 / 2. >= 11.5 else 11.5) - np.log(slope2)) / (
            (np.log(121) - np.log(slope_at + 5)) * slope)
    return var


def requiredSlope(df):
    df["slope"] = (np.log(df.mass_plus) - np.log(df.mass_plus.shift(1))) / (np.log(df.index) - np.log(df.index - 1))
    avg_slope = np.mean(df.slope.tail(4 if df.index.max() >= 11 else 3))
    bounds = OrderedDict(zip([160, 170, 200, 300, 1000], [0.8, 0.815, 0.95, 1.12, 1.15]))
    c7mass = df.loc[df.index == 7, "mass_plus"].values[0]
    for (key, value) in bounds.iteritems():
        if c7mass <= key:
            if avg_slope < value:
                avg_slope = value
            break
    lpm = df.loc[df.index == df.index.max(), "mass_plus"].values[0]
    ls = df.index.max()
    slope_at = max(35, ls + 15)
    required_slope = (np.exp(np.log(lpm) + avg_slope * (np.log(slope_at) - np.log(ls))) - np.exp(
        np.log(lpm) + avg_slope * (np.log(slope_at - 1) - np.log(ls))))
    if required_slope <= 12:
        required_slope = 12
    elif required_slope >= 65:
        required_slope = 65
    return required_slope


def fourthVar(df, dfplus, var1):
    slope16, slope = slopeNumber(var1)
    last_plus = dfplus.index.max()
    required_slope = requiredSlope(df) if dfplus.index.max() <= 16 else max(4, df.loc[
        df.index == last_plus, "mass_plus"].values[0] - df.loc[df.index == last_plus - 1, "mass_plus"].values[0])
    last_plus = 35 if dfplus.index.max() <= 16 else dfplus.index.max()
    var = (np.log(required_slope) - np.log(slope16)) / ((np.log(last_plus) - np.log(16)) * slope)
    return var


def finalFirstVar(df, dfplus, tempVar1):
    last_plus = dfplus.index.max()
    iter_vars, slopes = list(), list()
    multiplier = 80.1
    while multiplier >= 0.1:
        multiplier -= 0.1
        var1 = tempVar1 * multiplier
        var4 = fourthVar(df, dfplus, var1)
        slope_at = max(35, last_plus + 15)
        slope16, slope = slopeNumber(var1)
        slope = np.exp(np.log(slope16) + var4 * slope * (np.log(slope_at) - np.log(16)))
        iter_vars.append(var1)
        slopes.append(slope)
    required_slope = requiredSlope(df)
    return PchipInterpolator(np.asarray(slopes), np.asarray(iter_vars), extrapolate=True)(required_slope).item(0)


def firstVar(df, dfplus):
    last_plus = min(16, dfplus.index.max())
    # mass of last plus fraction -1
    mass_plus_olm1 = df.loc[df.index == last_plus - 1, "mass_plus"].values[0]
    last_slope = get_slope(last_plus)
    var = max(4, (df.loc[df.index == last_plus, "mass_plus"].values[0] - mass_plus_olm1)) / last_slope
    if dfplus.index.max() > 16:
        var = finalFirstVar(df, dfplus, var)
    return var


def findVarsStartingPoint(df_o, dfplus):
    df = df_o.copy(deep=True)
    df = pd.concat([df, dfplus.loc[dfplus.index == dfplus.index.max()]], axis=0, sort=True)
    df["zimi"] = df.expmass * df.mf
    df["mass_plus"] = df.apply(
        lambda row: df.loc[df.index >= row.name, "zimi"].sum() / df.loc[df.index >= row.name, "mf"].sum(), axis=1)
    var1 = firstVar(df, dfplus)
    var2, var3 = var2_3(df, dfplus)
    var4 = fourthVar(df, dfplus, var1)
    var5 = fifthVar(dfplus, var1, var4)
    return [var1, var2, var3, var4, var5]

# Plus Fraction Slope Multiplier-till SCN 16  - var1
# First Slope Multiplier  - var4
# Second Slope Multiplier - var5
# Mass correction Multiplier  - var2
# Mass Slope Multiplier  - var3
