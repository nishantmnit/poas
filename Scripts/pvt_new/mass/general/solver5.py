import numpy as np
import pandas as pd

from Scripts.pvt_new.core.get_logger import get_logger
from common import revertFractions
from mass_input import mass_input
from solver6 import solver6
from start_point import findVarsStartingPoint
from Scripts.pvt_new.core.storage.save import save

logger = get_logger("pvt general mass solver 5")


def massMultiplierDistribution(df, dfplus):
    m121 = df.loc[df.index == df.index.max(), "mass_plus"].values[0]
    multi = 1.02 if (2.889447236181E-08 * m121 ** 2 + 0.0000409296482412009 * m121 + 0.879321608040208) < 1.02 else \
        (2.889447236181E-08 * m121 ** 2 + 0.0000409296482412009 * m121 + 0.879321608040208)
    m120 = df.loc[df.index == df.index.max() - 1, "poas_mass"].values[0]
    m120r = m120 if m121 / multi <= m120 <= m121 / 1.02 else m121 / multi
    pmcr = (m120r - m120) / m120 * 100.
    mc120 = (4.81668632961298 * np.exp(0.0140674955555049 * 120.))
    multiplier = pmcr / mc120
    ls = max(19, dfplus.index.max() - 1)
    mmd = multiplier / (120. - ls)
    return mmd, ls


def addPOASMass(i_vars, df, dfplus):
    df["poas_mass_slope"] = i_vars[2] * df.apply(lambda row: (
            -0.0000000081143800838 * row.name ** 5 + 2.54535602253662E-06 * row.name ** 4 - 0.000278130020201878 *
            row.name ** 3 + 0.0132162236519363 * row.name ** 2 - 0.258386996099669 * row.name + 15.4092766381938)
    if row.name <= 100 else (0.509448043249933 * row.name ** 0.768554188962656), axis=1)
    df["poas_mass"] = df.apply(
        lambda row: 82.81 + df.loc[(df.index <= row.name) & (df.index >= 7), "poas_mass_slope"].sum(), axis=1)
    df["mass_correction"] = (4.81668632961298 * np.exp(0.0140674955555049 * df.index)) * i_vars[1]
    df["poas_mass"] = df.poas_mass + df.poas_mass * df.mass_correction / 100.
    df.loc[df.index == 5, "poas_mass"] = df.loc[df.index == 5, "expmass"]
    df.loc[df.index == 6, "poas_mass"] = df.loc[df.index == 6, "expmass"]
    mmd, ls = massMultiplierDistribution(df, dfplus)
    df.loc[df.index >= 20, "poas_mass"] = df.poas_mass + df.poas_mass * (
            4.81668632961298 * np.exp(0.0140674955555049 * df.index)) * mmd * (df.index - ls) / 100.
    df.loc[df.index == df.index.max(), "poas_mass"] = df.loc[df.index == df.index.max(), "mass_plus"].values[0]
    return df


def calculateMassPlus(i_vars, df_o, dfplus):
    df = pd.DataFrame()
    df["scn"] = ["C%s" % x for x in range(5, 122)]
    df.index = [x for x in range(5, 122)]
    df = pd.concat([df_o, df.loc[df.index > df_o.index.max()]], axis=0, sort=True)
    slopes = list()
    last_plus = dfplus.index.max()
    slope_at = max(35, last_plus + 15)
    change_at = slope_at + 5
    slope = 0
    for index, row in df.iterrows():
        if index <= 16:
            slopes.append(i_vars[0] * (
                    -0.0000554601725806236 * index ** 5 + 0.00754033655953279 *
                    index ** 4 - 0.230779871372405 * index ** 3 + 2.48690642287389 *
                    index ** 2 - 6.95180703002185 * index + 1.50595054461093))
            if index == 16:
                slope = ((np.log(slopes[-1]) - np.log(slopes[-2])) / (np.log(index) - np.log(index - 1))) * i_vars[3]
        elif index <= change_at:
            slopes.append(np.exp(np.log(slopes[-1]) + slope * (np.log(index) - np.log(index - 1))))
            if index == change_at:
                slope = ((np.log(slopes[-1]) - np.log(slopes[-2])) / (np.log(index) - np.log(index - 1))) * i_vars[4]
        else:
            slopes.append(np.exp(np.log(slopes[-1]) + slope * (np.log(index) - np.log(index - 1))))
    df["poas_mass_plus_slope"] = slopes
    df["zi_plus"] = df.apply(lambda row1: df.loc[df.index >= row1.name, "mf"].sum() + dfplus.loc[
        (dfplus.index >= row1.name) & (dfplus.index == dfplus.index.max()), "mf"].sum(), axis=1)
    mass_plus = list()
    for index, row in df.iterrows():
        if 5 == index:
            mass = dfplus.loc[dfplus.index == index, "expmass"].values[0]
        elif index >= dfplus.index.max():
            mass = mass_plus[-1] + row.poas_mass_plus_slope
        else:
            prow = df.loc[df.index == row.name - 1].squeeze()
            mass = (mass_plus[-1] * prow.zi_plus - prow.expmass * prow.mf) / row.zi_plus
        mass_plus.append(mass)
    df["mass_plus"] = mass_plus
    return df


def calculateZi(df, dfplus):
    zi_plus, zi = list(), list()
    for index, row in df.iterrows():
        if index < dfplus.index.max() - 1:
            zi.append(row.mf)
            zi_plus.append(row.zi_plus)
        elif index == dfplus.index.max() - 1:
            zi_plus.append(row.zi_plus)
            try:
                mass_plus = df.loc[df.index == row.name + 1, "mass_plus"].values[0]
            except:
                mass_plus = 0
            zi.append(zi_plus[-1] * (mass_plus - row.mass_plus) / (mass_plus - row.mass))
        else:
            zi_plus.append(zi_plus[-1] - zi[-1])
            zi_plus[-1] = 0 if zi_plus[-1] <= 10 ** -20 else zi_plus[-1]
            try:
                mass_plus = df.loc[df.index == row.name + 1, "mass_plus"].values[0]
            except:
                mass_plus = 0
            zi.append(zi_plus[-1] * (mass_plus - row.mass_plus) / (mass_plus - row.mass))
            zi[-1] = 10 ** -20 if zi[-1] <= 10 ** -20 else zi[-1]
    df["zi_plus"] = zi_plus
    df["zi"] = zi
    return df


def calculateMass(i_vars, df_o, dfplus):
    df = calculateMassPlus(i_vars, df_o, dfplus)
    df = addPOASMass(i_vars, df, dfplus)
    df["mass"] = df.apply(lambda row: row.expmass if row.name < dfplus.index.max() else row.poas_mass, axis=1)
    df = calculateZi(df, dfplus)
    df["final_zi"] = df.apply(lambda row: row.mf if row.name < dfplus.index.max() else row.zi, axis=1)
    df["zimi"] = df.final_zi * df.mass
    dfplus["zi"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "final_zi"].sum(), axis=1)
    dfplus["mass"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "zimi"].sum() / row.zi, axis=1)
    return df, dfplus


def calculateFinalMass(res, df, dfplus):
    i_vars = res
    df, dfplus = calculateMass(i_vars, df, dfplus)
    df["mass"] = df.apply(lambda row: row.expmass if row.name < dfplus.index.max() else row.mass, axis=1)
    df["mf"] = df.apply(lambda row: row.mf if row.name < dfplus.index.max() else row.zi, axis=1)
    df["zimi"] = df.mf * df.mass
    dfplus["mass"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "zimi"].sum() / row.zi, axis=1)
    df.drop(columns=["zi"], inplace=True)
    dfplus.drop(columns=["zi"], inplace=True)
    return df, dfplus


def process_result(res, df, dfplus, user_args):
    df, dfplus = calculateFinalMass(res, df, dfplus)
    # In Case the Last Plus Fraction Mole Fraction is <Previous One, make the last plus fraction = previous one.
    if df.loc[df.index == df.index.max(), "mf"].values[0] < df.loc[df.index == df.index.max() - 1, "mf"].values[0]:
        df.loc[df.index == df.index.max(), "mf"].values[0] = df.loc[df.index == df.index.max() - 1, "mf"].values[0]

    # get all mfs
    df["slope"] = df.mf - df.mf.shift(1)
    slopes = df.loc[df.index >= 7, "slope"].tolist()
    trend, max_count = 0, 0
    for slope in slopes:
        if slope >= 10 ** -5:
            trend += 1
        else:
            if trend >= 5:
                max_count += 1
            trend = 0
    if max_count > 1:
        logger.info("Solver 5 result contain more than 1 maxima for zi. Executing solver 6.")
        return solver6(user_args)
    else:
        df, dfplus = revertFractions(user_args.well_conn, df, dfplus)
        divisor = (dfplus.loc[dfplus.index == 5, "expmass"].values[0] * dfplus.loc[dfplus.index == 5, "mf"].values[
            0])
        df["wf"] = (df["mass"] * df["mf"]) / divisor
        save(df, user_args, identifier="mass", plus_or_individual="individual", solver=5, distribution="general")
        save(dfplus, user_args, identifier="mass", plus_or_individual="plus", solver=5, distribution="general")
        return True


def solver5(user_args, df=None, dfplus=None):
    logger.info("Executing General Mass Solver 5")
    if df is None and dfplus is None:
        df, dfplus = mass_input(5, user_args.well_conn)
    logger.info("Finding start point for General Mass Solver 5")
    start_point = findVarsStartingPoint(df, dfplus)
    logger.info("General Mass solver final variables - %s" % (",".join([str(x) for x in start_point])))
    return process_result(start_point, df, dfplus, user_args)


"""
Executed in all cases but input varies.
When max input plus fraction is 9+ then it is called from solver1_* otherwise directly. 
"""
