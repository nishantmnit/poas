import importlib

from Scripts.pvt_new.core.storage.mongo import delete_all
from Scripts.pvt_new.core.storage.mongo import fetch
from collections import OrderedDict
from Scripts.pvt_new.core.get_logger import get_logger
from Scripts.pvt_new.core.storage.fetch_most import set_recommended

logger = get_logger("pvt general mass run")

available_solvers = ("solver1_general", "solver1_gamma", "solver1_beta", "solver2", "solver3",
                     "solver4", "solver5", "solver6", "solver7", "solver8")
groups = OrderedDict(
    {9: ("solver1", "solver7", "solver8"),
     19: ("solver5", "solver7", "solver8"),
     121: ("solver5", "solver2", "solver3", "solver4", "solver7", "solver8")}
)


def get_solvers(max_scn, bias):
    for scn, group in groups.items():
        if max_scn <= scn:
            if scn == 9:
                group = list(group)
                group[0] = "solver1_%s" % bias
            return group


def general(user_args):
    logger.info("Deleting all previous general mass output")
    delete_all(user_args.well_conn, property="pvt_output", identifier="mass", distribution="general")
    max_scn = get_max_plus_fraction(user_args)
    logger.info("max available plus fraction from user = %s+" % max_scn)
    solvers = get_solvers(max_scn, user_args.bias)
    logger.info("Executing general mass solvers = %s" % ", ".join(solvers))
    for solver_module in solvers:
        solver = importlib.import_module("Scripts.pvt_new.mass.general.%s" % solver_module)
        getattr(solver, solver_module)(user_args)
        set_recommended(user_args.well_conn, "mass")


def get_max_plus_fraction(user_args):
    try:
        dfplus = fetch(user_args.well_conn, property="pvt_output", identifier="validated_averaged_data",
                       plus_or_individual="plus")
    except Exception as e:
        print 'Error:{"Type": "Validation", "Message": "Data validation failed. Please check and fix input data ' \
              'first."} '
        raise Exception("validated input data not available")
    return dfplus.index.max()


if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection
    import pandas as pd
    from Scripts.pvt_new.settings import DEBUG_DIR
    import os

    well_name = "ALUMM"

    debug_dir = os.path.join(DEBUG_DIR, well_name)

    db = get_connection(well_name)
    test_user_args = pd.Series({"debug": True, "well_conn": db, "bias": "general", "debug_dir": debug_dir})
    general(test_user_args)
