import numpy as np
from scipy import optimize

from Scripts.pvt_new.core.get_logger import get_logger
from common import revertFractions
from wismooth_new import smooth
from Scripts.pvt_new.mass.gamma.mass_input import mass_input
from Scripts.pvt_new.core.storage.save import save

logger = get_logger("pvt general mass solver 7")


def calculateMass(var, df, dfplus):
    df["correction"] = (4.81668632961298 * np.exp(0.0140674955555049 * df.index)) * var[0]
    df.loc[df.index <= 5, "mass"] = df.expmass
    df.loc[df.index > 5, "mass"] = df.mass5 + df.mass5 * df.correction / 100.
    df["zimi"] = df.mf * df.mass
    dfplus["mf"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "mf"].sum(), axis=1)
    dfplus["mass"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "zimi"].sum() / row.mf, axis=1)
    return df, dfplus


def cost_function(var, df_o, dfplus):
    df = df_o.copy(deep=True)
    df, dfplus = calculateMass(var, df, dfplus)
    dfplus["error"] = ((dfplus.expmass - dfplus.mass) / dfplus.expmass) ** 2 / 0.001
    error = dfplus.error.sum()
    return error


def calcNewMF(df):
    df["zimi"] = df.mass5 * df.mf
    df["new_mf"] = df.wf * df.zimi.sum() / df.mass5
    df["new_mf"] = df.new_mf * (df.mf.sum() / df.new_mf.sum())
    df["mf"] = df.new_mf
    df.drop(columns=["new_mf"], inplace=True)
    return df


def runSolver(df, dfplus, solver):
    df = calcNewMF(df)
    res = optimize.minimize(cost_function, np.asarray([0.]), bounds=[[-3., 3.]], method="SLSQP", args=(df, dfplus),
                            options={'maxiter': 500})
    if not res.success:
        print res
        logger.info("General Mass Solver %s failed to find a feasible solution." % solver)
        res.x = [1.]
    logger.info("Completed General Mass Solver %s" % solver)
    logger.info("General Mass solver %s final variables - %s, objective - %s" % (
        solver, ",".join([str(x) for x in res.x]), res.fun))
    return res, df, dfplus


def process_result(res, df, dfplus, user_args):
    df, dfplus = calculateMass(res.x, df, dfplus)
    df, dfplus = revertFractions(user_args.well_conn, df, dfplus)
    divisor = df.zimi.sum()
    df["wf"] = (df["mass"] * df["mf"]) / divisor
    save(df, user_args, identifier="mass", plus_or_individual="individual", solver=7,
         distribution="general")
    save(dfplus, user_args, identifier="mass", plus_or_individual="plus", solver=7, distribution="general")
    return True


def solver7(user_args):
    logger.info("Executing General Mass Solver 7")
    df, dfplus = mass_input(2, user_args.well_conn)
    success, df = smooth(df, dfplus, 7)
    if not success:
        print 'Warning:{"Type": "Mass Solver", "Message": "General Mass Solver 7 failed to find a ' \
              'feasible solution. Executing Solver 8."}'
        return False
    res, df, dfplus = runSolver(df, dfplus, 7)
    return process_result(res, df, dfplus, user_args)


"""
executed when input max plus fraction is > 19
"""
