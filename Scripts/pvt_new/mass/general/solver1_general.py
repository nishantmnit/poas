import math

import numpy as np
import pandas as pd
from scipy import optimize
from scipy.special import gamma, gammainc

from Scripts.pvt_new.core.get_logger import get_logger
from mass_input import mass_input
from solver5 import solver5
from wismooth import smooth

logger = get_logger("pvt general bias mass solver 1")


def get_slope(row):
    if row.name <= 100:
        return -0.0000000081143800838 * row.name ** 5 + 2.54535602253662E-06 * row.name ** 4 - \
               0.000278130020201878 * row.name ** 3 + 0.0132162236519363 * row.name ** 2 - 0.258386996099669 * \
               row.name + 15.4092766381938
    else:
        return 0.509448043249933 * row.name ** 0.768554188962656


def add_poas_mass(iter_vars, df_o):
    df = df_o.copy(deep=True)
    df = pd.DataFrame()
    df["scn"] = ["C%s" % x for x in range(5, 122)]
    df.index = [x for x in range(5, 122)]
    df_o = pd.concat([df_o, df.loc[df.index > df_o.index.max()]], axis=0, sort=True)
    df["poas_mass_slope"] = iter_vars[1] * df.apply(lambda row: get_slope(row), axis=1)
    df["poas_mass"] = df.apply(lambda row:
                               82.81 + df.loc[(df.index <= row.name) & (np.asarray(df.index) >= 7),
                                              "poas_mass_slope"].sum(), axis=1)
    df["mass_correction"] = (4.81668632961298 * np.exp(0.0140674955555049 * np.asarray(df.index))) * iter_vars[0]
    df["poas_mass"] = df.poas_mass + df.poas_mass * df.mass_correction / 100.
    df_o["poas_mass"] = df["poas_mass"]
    df_o.loc[df_o.index == 5, "poas_mass"] = df_o.loc[df_o.index == 5, "expmass"]
    df_o.loc[df_o.index == 6, "poas_mass"] = df_o.loc[df_o.index == 6, "expmass"]
    df_o.loc[df_o.index == 121, "poas_mass"] = 15000
    return df_o


def calculate_A(iter_vars, dfplus):
    pstar_avg = (dfplus.loc[dfplus.index == 5, "expmass"].values[0] - iter_vars[3]) / iter_vars[3]
    A = iter_vars[2] * (pstar_avg / (gamma(1. + 1. / iter_vars[2]))) ** iter_vars[2]
    return A


def calculate_mass(iter_vars, df_o, dfplus):
    df_o = add_poas_mass(iter_vars, df_o)
    df = df_o
    A = calculate_A(iter_vars, dfplus)
    df["pstar"] = (df.poas_mass - iter_vars[3]) / iter_vars[3]
    df["qi"] = (iter_vars[2] / A) * df.pstar ** iter_vars[2]
    df["zicumm"] = df.apply(lambda row: 10 ** -20 if np.exp(-1. * row.qi) <= 10 ** -20 else np.exp(-1. * row.qi),
                            axis=1)
    df["ziinside"] = df.zicumm.shift(1) - df.zicumm
    df.loc[df.index == df.index.min(), 'ziinside'] = 1. - df['zicumm'].iloc[0]
    df["incg"] = df.apply(lambda row: gammainc(1. + 1. / iter_vars[2], row.qi) * gamma(1. + 1. / iter_vars[2]), axis=1)
    df['incgdiff'] = df['incg'].sub(df['incg'].shift())
    df.incgdiff = df.incgdiff.fillna(df['incg'].iloc[0])
    df["pav"] = (1 / df.ziinside) * (A / iter_vars[2]) ** (1. / iter_vars[2]) * df.incgdiff
    df["mass"] = df.apply(lambda row: 0 if row.pav == 0 else iter_vars[3] * (1. + row.pav), axis=1)
    df["zi"] = df.ziinside * dfplus.loc[dfplus.index == 5, "mf"].values[0]
    df["zimi"] = df.zi * df.mass
    df_o[["zi", "mass", "zimi"]] = df[["zi", "mass", "zimi"]]
    dfplus["zi"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "zi"].sum(), axis=1)
    dfplus["mass"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "zimi"].sum() / row.zi, axis=1)
    return df_o, dfplus


def calculate_errors(iter_vars, df_o, dfplus):
    df, dfplus = calculate_mass(iter_vars, df_o, dfplus)
    dfplus["zi_error"] = ((dfplus.mf - dfplus.zi) / dfplus.mf) ** 2
    dfplus["mass_error"] = ((dfplus.mass - dfplus.expmass) / dfplus.expmass) ** 2
    return df, dfplus


def target_cost(val, target):
    if math.isnan(val):
        val = 0
    return val / target


def cost_function(iter_vars, df_o, dfplus):
    df, dfplus = calculate_errors(iter_vars, df_o, dfplus)
    error = 0
    for index, row in dfplus.iterrows():
        error += target_cost(dfplus.loc[dfplus.index == index, "zi_error"].values[0],
                             0.001) if index >= 6 else 0
        error += target_cost(dfplus.loc[dfplus.index == index, "mass_error"].values[0],
                             0.001) if index >= 6 else 0
    return error


def calculate_final_mass(df, dfplus):
    df["mass"] = df.apply(lambda row: row.expmass if row.name < dfplus.index.max() else row.mass, axis=1)
    df["mf"] = df.apply(lambda row: row.mf_bkp if row.name < dfplus.index.max() else row.mf, axis=1)
    df["zimi"] = df.mf * df.mass
    dfplus["mass"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "zimi"].sum() / row.mf, axis=1)
    return df, dfplus


def prepare_data_for_solver5(res, df, dfplus):
    df, dfplus = calculate_mass(res.x, df, dfplus)
    df[["mf", "mf_bkp"]] = df[["zi", "mf"]]
    logger.info("Trying to smoothen output.")
    df, dfplus = smooth(df, dfplus)
    df, dfplus = calculate_final_mass(df, dfplus)

    # create 16+
    dfplus["zimi"] = dfplus.expmass * dfplus.mf
    df["zimi"] = df.mass * df.mf
    mf = dfplus.loc[dfplus.index == dfplus.index.max(), "mf"].values[0] - df.loc[
        (df.index >= dfplus.index.max()) & (df.index < 16), "mf"].sum()
    mass = (dfplus.loc[dfplus.index == dfplus.index.max(), "zimi"].values[0] - df.loc[
        (df.index >= dfplus.index.max()) & (df.index < 16), "zimi"].sum()) / mf
    dfplus_temp = pd.DataFrame(data=[[mf, mass, mass, "C16"]], columns=["mf", "expmass", "mass", "scn"], index=[16])
    df.drop(df.loc[df.index >= 16].index, inplace=True)
    df["expmass"] = df.mass
    dfplus = pd.concat([dfplus, dfplus_temp], axis=0, sort=True)
    return df, dfplus


def solver1_general(user_args):
    logger.info("Executing General Mass Solver 1")
    df, dfplus = mass_input(1, user_args.well_conn)
    b = 1.05001515362526000000 * np.log(dfplus.loc[dfplus.index == 5, "expmass"].values[0]) - 4.24581065084051000000
    res = optimize.minimize(cost_function, np.array([0., 1, b, 70.]),
                            bounds=[[-1.5, 4.], [0.95, 1.1], [0.5, 5.], [68., 72.1]], method="SLSQP",
                            args=(df, dfplus), options={'maxiter': 500})
    if not res.success:
        logger.debug(res)
        logger.warning("General Mass Solver 1 failed to find a feasible solution.")
        return False
    logger.info("Completed General Special Mass Solver 1")
    logger.info(
        "General Mass solver final variables - %s, objective - %s" % (",".join([str(x) for x in res.x]), res.fun))
    df, dfplus = prepare_data_for_solver5(res, df, dfplus)
    return solver5(user_args, df, dfplus)


"""
Executed when user selected bias is general and user provided max plus fraction is 9+
result of this solver is not saved, instead on success, solver5 is executed.
on failure, nothing happens.
"""
