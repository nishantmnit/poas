import pandas as pd

from Scripts.pvt_new.core.storage.mongo import fetch
from Scripts.pvt_new.validation.validate import average
from Scripts.pvt_new.validation.whitson import define_plus_fractions
from Scripts.pvt_new.core.storage.fetch_most import validated_data


def mass_input(solver, well_conn):
    df, dfplus = validated_data(well_conn)
    df = average(df, df.index.min())
    desired = range(4, dfplus.index.max() + 1) if solver == 1 else None
    dfplus = define_plus_fractions(df, dfplus, desired)
    df.drop(df.loc[df.index >= dfplus.index.max()].index, inplace=True)
    df.drop(columns=["expbp", "expdensity", "expri"], inplace=True)
    if solver in [1, 5, 6]:
        df.drop([4], inplace=True)
        dfplus.drop([4], inplace=True)
    else:
        dfplus.drop([5], inplace=True)

        try:
            mf = fetch(well_conn, property="pvt_output", identifier="mass", distribution="general", solver=5,
                       plus_or_individual="individual")
        except:
            mf = fetch(well_conn, property="pvt_output", identifier="mass", distribution="general", solver=6,
                       plus_or_individual="individual")
        df = pd.concat([df, mf.loc[mf.index > df.index.max()]], axis=0, sort=True)
        df["scn"] = df.apply(lambda row: "C%s" % row.name, axis=1)
        df.drop(columns=["mass"], inplace=True)
        dfplus["mf"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "mf"].sum(), axis=1)
        df['ziinside'] = df.mf.values / dfplus.loc[dfplus.index == 4, "mf"].values[0]
        df['cummziinside'] = df.ziinside.cumsum()
        dfplus['ziinside'] = 1 - dfplus.mf.values / dfplus.loc[dfplus.index == 4, "mf"].values[0]
        dfplus.loc[dfplus.ziinside <= 0, "ziinside"] = 10 ** -30
        dfplus['cummziinside'] = dfplus['ziinside']

    if solver in [3, 4]:
        df.rename(columns={'mf': 'expzi', 'ziinside': 'expziinside', 'cummziinside': 'expcummziinside'},
                  inplace=True)
        dfplus.rename(columns={'mf': 'expzi', 'ziinside': 'expziinside', 'cummziinside': 'expcummziinside'},
                      inplace=True)

    return df, dfplus


if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection

    db = get_connection("AB8")
    print mass_input(1, db)
