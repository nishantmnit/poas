import numpy as np
import pandas as pd
from numpy import log
from scipy.special import gamma, digamma

from Scripts.pvt_new.core.get_logger import get_logger
from Scripts.pvt_new.core.optimizers.generic_solver import GenericSolver
from common import revertFractions, calcMass
from mass_input import mass_input
from mass_smooth import smooth
from solver2 import run_solver2, get_bounds as solver2_bounds
from Scripts.pvt_new.mass.common import findLastZi
from common import DifferentiateSpecial
from Scripts.pvt_new.core.storage.save import save

logger = get_logger("pvt general mass solver 3")


def get_bounds(dfplus):
    c4mass = dfplus.loc[dfplus.index == 4, "expmass"].values[0]
    bounds = dict()
    eta, slope = 51.11, 13.5
    c7mass = dfplus.loc[dfplus.index == 7, "expmass"].values[0]
    B = 0.98 if (1.01449364938449 * np.log(c7mass) - 4.02010653506624) * 1.35 < 0.98 else (
            1.01449364938449 * np.log(c7mass) - 4.02010653506624)
    bounds["B"] = [B * 0.82, B * 1.25]
    bounds["Eta"] = [10., 70.]
    bounds["Slope"] = [9., 15.5]
    initialBFn = (B - bounds["B"][0]) / (bounds["B"][1] - bounds["B"][0])
    initialEtaFn = (eta - bounds["Eta"][0]) / (bounds["Eta"][1] - bounds["Eta"][0])
    initialSlopeFn = (slope - bounds["Slope"][0]) / (bounds["Slope"][1] - bounds["Slope"][0])
    return ([initialBFn, initialEtaFn, initialSlopeFn],
            [[bounds["B"][0], bounds["B"][1]], [bounds["Eta"][0], bounds["Eta"][1]],
             [bounds["Slope"][0], bounds["Slope"][1]]],
            c4mass)


def get_intercept(df, dfplus, i_vars, c4mass):
    b, eta, slope = i_vars
    a = ((((c4mass - eta) / eta) / gamma(1. + 1. / b)) ** b) * b
    c6pluszi = 1 - (dfplus.loc[dfplus.index == 7, "expzi"].values[0] +
                    df.loc[df.index == 6, "expzi"].values[0]) / dfplus.loc[dfplus.index == 4, "expzi"].values[0]
    c6pluspstar = ((log(1 / (1 - c6pluszi))) * (a / b)) ** (1 / b)
    c6plusmb = c6pluspstar * eta + eta
    intercept = (2 * c6plusmb - slope * (5. - 5. + 6. - 5.)) / 2
    return intercept


def gradients(i_vars, df, dfplus, c4mass):
    b, eta, slope = i_vars
    a = ((((c4mass - eta) / eta) / gamma(1 + 1 / b)) ** b) * b
    c6pluszi = 1 - (dfplus.loc[dfplus.index == 7, "expzi"].values[0] +
                    df.loc[df.index == 6, "expzi"].values[0]) / dfplus.loc[dfplus.index == 4, "expzi"].values[0]
    c6plusqi = (log(1 / (1 - c6pluszi)))
    df["diffawrtb"] = (a / b) * (1 + log(a / b) + digamma(1 + 1 / b))
    df["diffawrteta"] = a * b * (1 / ((c4mass - eta) / eta)) * (-c4mass / eta ** 2)
    df["diffpwrtb"] = ((c6plusqi * (a / b)) ** (1 / b)) * (
            -1 / b * np.log(((c6plusqi * (a / b)) ** (1 / b))) + 1 / (a * b) * df.diffawrtb - 1 / b ** 2)
    df["diffpwrteta"] = (-1) * (slope * (df.scn + df.scn + 1 - 11)) / (2 * eta ** 2) + (
            (c6plusqi * (a / b)) ** (1 / b)) * (1 / (a * b) * df.diffawrteta)
    df["diffpwrtslope"] = (df.scn + df.scn + 1 - 11) / (2 * eta)
    df["diffqiwrtb"] = ((b / a) * df.pmb ** b) * (
            1 / b - 1 / a * df.diffawrtb + np.log(df.pmb) + (b / df.pmb) * df.diffpwrtb)
    df["diffqiwrteta"] = ((b / a) * df.pmb ** b) * ((b / df.pmb) * df.diffpwrteta - 1 / a * df.diffawrteta)
    df["diffqiwrtslope"] = ((b / a) * df.pmb ** b) * ((b / df.pmb) * df.diffpwrtslope)
    df["difflwrtb"] = np.exp(-df.qimb) * (-1) * df.diffqiwrtb
    df["difflwrteta"] = np.exp(-df.qimb) * (-1) * df.diffqiwrteta
    df["difflwrtslope"] = np.exp(-df.qimb) * (-1) * df.diffqiwrtslope
    df["changeinziwrtb"] = df['difflwrtb'].sub(df['difflwrtb'].shift())
    df["changeinziwrtb"] = -1 * df["changeinziwrtb"].fillna(df['difflwrtb'].iloc[0])
    df["changeinziwrteta"] = df['difflwrteta'].sub(df['difflwrteta'].shift())
    df["changeinziwrteta"] = -1 * df["changeinziwrteta"].fillna(df['difflwrteta'].iloc[0])
    df["changeinziwrtslope"] = df['difflwrtslope'].sub(df['difflwrtslope'].shift())
    df["changeinziwrtslope"] = -1 * df["changeinziwrtslope"].fillna(df['difflwrtslope'].iloc[0])
    df["changeinincgwrtqi"] = np.exp(-1 * df.qi) * df.qi ** (1 / b)
    df["changeinqi"] = df.diffqiwrtb * (-1 / ((1 + 1 / b) - 1) ** 2)
    df["changeinincgwrtb"] = df.apply(
        lambda row: DifferentiateSpecial(b, row.qi, row.incg, row.changeinqi), axis=1)
    df["changeinincgwrteta"] = df.changeinincgwrtqi * df.diffqiwrteta
    df["changeinincgwrtslope"] = df.changeinincgwrtqi * df.diffqiwrtslope
    df['tempdiff'] = df['changeinincgwrtb'].sub(df['changeinincgwrtb'].shift())
    df["tempdiff"] = df["tempdiff"].fillna(df['changeinincgwrtb'].iloc[0])
    df["changeinmasswrtb"] = ((1 / df.ziinside * (a / b) ** (1 / b)) * df.tempdiff + df.incgdiff * (
            1 / df.ziinside * (a / b) ** (1 / b)) * (-1 / df.ziinside * df.changeinziwrtb + 1 /
                                                     (a * b) * df.diffawrtb - 1 / b ** 2 * (1 + log(a / b)))) * eta
    df['tempdiff'] = df['changeinincgwrteta'].sub(df['changeinincgwrteta'].shift())
    df["tempdiff"] = df["tempdiff"].fillna(df['changeinincgwrteta'].iloc[0])
    df["changeinmasswrteta"] = ((1 / df.ziinside * (a / b) ** (1 / b)) * df.tempdiff + df.incgdiff * (
            1 / df.ziinside * (a / b) ** (1 / b)) * (-1 / df.ziinside * df.changeinziwrteta + 1 /
                                                     (a * b) * df.diffawrteta)) * eta + df.pav + 1
    df['tempdiff'] = df['changeinincgwrtslope'].sub(df['changeinincgwrtslope'].shift())
    df["tempdiff"] = df["tempdiff"].fillna(df['changeinincgwrtslope'].iloc[0])
    df["changeinmasswrtslope"] = ((a / b) ** (1 / b)) * (
            1 / df.ziinside * df.tempdiff - 1 / df.ziinside ** 2 * df.changeinziwrtslope * df.incgdiff) * eta

    df["changeinzimiwrtb"] = df.expzi * df.changeinmasswrtb
    df["changeinzimiwrteta"] = df.expzi * df.changeinmasswrteta
    df["changeinzimiwrtslope"] = df.expzi * df.changeinmasswrtslope
    last_expmass_scn = df.loc[~df.expmass.isnull()].index.max()
    df.loc[df.index <= last_expmass_scn, "changeinzimiwrtb"] = 0
    df.loc[df.index <= last_expmass_scn, "changeinzimiwrteta"] = 0
    df.loc[df.index <= last_expmass_scn, "changeinzimiwrtslope"] = 0

    dfplus.loc[dfplus.index < dfplus.index.max(), "changeinmasswrtb"] = dfplus.apply(
        lambda row: ((df.loc[df.index >= row.name, "changeinzimiwrtb"].sum()) / row.expzi), axis=1)
    dfplus.loc[dfplus.index < dfplus.index.max(), "changeinmasswrteta"] = dfplus.apply(
        lambda row: ((df.loc[df.index >= row.name, "changeinzimiwrteta"].sum()) / row.expzi), axis=1)
    dfplus.loc[dfplus.index < dfplus.index.max(), "changeinmasswrtslope"] = dfplus.apply(
        lambda row: ((df.loc[df.index >= row.name, "changeinzimiwrtslope"].sum()) / row.expzi), axis=1)
    dfplus = dfplus.drop(dfplus.tail(1).index)
    errors = dfplus["masserror"].tolist() + df["masserror"].tolist() + df["qierror"].tolist()
    derivativesb = (dfplus["changeinmasswrtb"] * (
            2 * (dfplus.mass - dfplus.expmass) / dfplus.expmass ** 2)).tolist() + (
                           df["changeinmasswrtb"] * (2 * (df.mass - df.expmass) / df.expmass ** 2)).tolist() + (
                           df["diffqiwrtb"] * (2 * (df.qimb - df.qizi) / df.qizi ** 2)).tolist()
    derivativeseta = (dfplus["changeinmasswrteta"] *
                      (2 * (dfplus.mass - dfplus.expmass) / dfplus.expmass ** 2)).tolist() + \
                     (df["changeinmasswrteta"] * (2 * (df.mass - df.expmass) / df.expmass ** 2)).tolist() + \
                     (df["diffqiwrteta"] * (2 * (df.qimb - df.qizi) / df.qizi ** 2)).tolist()
    derivativesslope = (dfplus["changeinmasswrtslope"] *
                        (2 * (dfplus.mass - dfplus.expmass) / dfplus.expmass ** 2)).tolist() + \
                       (df["changeinmasswrtslope"] * (2 * (df.mass - df.expmass) / df.expmass ** 2)).tolist() + \
                       (df["diffqiwrtslope"] * (2 * (df.qimb - df.qizi) / df.qizi ** 2)).tolist()
    derivatives = np.matmul(errors, np.transpose([derivativesb, derivativeseta, derivativesslope])).tolist()
    return sum(errors), derivatives, 0


def mass(i_vars, args, c4mass, truncate=True):
    b, eta, slope = i_vars
    a = ((((c4mass - eta) / eta) / gamma(1 + 1 / b)) ** b) * b
    df = args[0].copy(deep=True)
    dfplus = args[1]
    intercept = get_intercept(df, dfplus, i_vars, c4mass)

    index = [x for x in range(df.index.max() + 1, df.index.max() + 2)]
    df = pd.concat([df, pd.DataFrame(index, index=index, columns=["scn"])], sort=False)

    df["scn"] = df.index
    df["pzi"] = ((np.log(1 / (1 - df.expcummziinside))) * a / b) ** (1 / b)
    df["qizi"] = (np.log(1 / (1 - df.expcummziinside)))
    df["massnalkane"] = df.apply(lambda row: slope * (row.scn - 5) + intercept, axis=1)
    df["mb"] = df.massnalkane.add(df.massnalkane.shift(-1)) / 2
    df = df.drop(df.tail(1).index)
    df["pmb"] = (df.mb - eta) / eta
    df["qimb"] = (b / a) * df.pmb ** b

    df["ziinside"] = np.exp(-1 * df.qimb.shift(1)) - np.exp(-1 * df.qimb)
    df.loc[df.index == df.index.min(), "ziinside"] = 1 - np.exp(-1 * df.qimb.iloc[0])
    df["zicalc"] = df.ziinside * dfplus.loc[dfplus.index == 4, "expzi"].values[0]
    maxscn, df = findLastZi(df, "zicalc", dfplus.loc[dfplus.index == 4, "expzi"].values[0])
    df.loc[df.index == maxscn, "zicalc"] = dfplus.loc[dfplus.index == 4, "expzi"].values[0] - df.loc[
        df.index < maxscn, "zicalc"].sum()
    df["cummziinside"] = df.ziinside.cumsum()

    df = calcMass(df, a, b, eta)

    last_expmass_scn = df.loc[~df.expmass.isnull()].index.max()
    df.loc[df.index <= last_expmass_scn, "zimi"] = df.expzi * df.expmass
    df.loc[df.index > last_expmass_scn, "zimi"] = df.expzi * df.mass

    if truncate:
        df = df.loc[df.index < dfplus.index.max()]
    dfplus["zimi"] = dfplus.expmass * dfplus.expzi
    maxzimi = ((dfplus.loc[dfplus.index == dfplus.index.max(), "expmass"] * dfplus.loc[
        dfplus.index == dfplus.index.max(), "expzi"]).values)[0]
    dfplus.loc[dfplus.index < dfplus.index.max(), "mass"] = dfplus.apply(
        lambda row: ((df.loc[df.index >= row.name, "zimi"].sum() + maxzimi) / row.expzi), axis=1)

    # calculate errors
    df["masserror"] = df.apply(
        lambda row: np.NaN if row.expmass == 0 else ((row.expmass - row.mass) / row.expmass) ** 2, axis=1)
    df["qierror"] = ((df.qizi - df.qimb) / df.qizi) ** 2
    dfplus["masserror"] = ((dfplus.expmass - dfplus.mass) / dfplus.expmass) ** 2
    return df, dfplus


def cost_function(i_vars, args):
    c4mass = args[2]
    df, dfplus = mass(i_vars, args, c4mass)
    return gradients(i_vars, df, dfplus, c4mass)


def solver3_2(res, df, dfplus, user_args, c4mass):
    df, dfplus = mass(res["Values"], [df, dfplus], c4mass, False)
    df["zimi"] = df.mass * df.expzi
    dfplus["mass"] = dfplus.apply(lambda row: ((df.loc[df.index >= row.name, "zimi"].sum()) / row.expzi), axis=1)
    df[["mf", "ziinside", "cummziinside", "expmass"]] = df[["expzi", "expziinside", "expcummziinside", "mass"]]
    dfplus[["mf", "ziinside", "cummziinside"]] = dfplus[["expzi", "expziinside", "expcummziinside"]]
    df.drop(df.loc[df.index >= dfplus.index.max()].index, inplace=True)
    start_points, bounds = solver2_bounds(df)
    res = run_solver2(start_points, bounds, df, dfplus, user_args, printname="3-2")
    return res


def solver3_1(start_points, bounds, df, dfplus, user_args, c4mass):
    logger.info("Executing General Mass Solver 3-1")
    res = GenericSolver(start_points, bounds, cost_function, debug=user_args.debug,
                        args=(df.copy(deep=True), dfplus.copy(deep=True), c4mass))
    logger.info("Result -- %s" % res)
    logger.info("Completed General Mass Solver 3-1")
    return res


def process_result(res, df, dfplus, user_args):
    a, b, eta = res["Values"]
    df.rename(columns={'expzi': 'mf', 'expziinside': 'ziinside', 'expcummziinside': 'cummziinside'}, inplace=True)
    dfplus.rename(columns={'expzi': 'mf', 'expziinside': 'ziinside', 'expcummziinside': 'cummziinside'},
                  inplace=True)
    df = calcMass(df, a, b, eta)
    max_replacement = dfplus.index.max() - 2
    df["mass"] = df.apply(lambda row: row.expmass if row.name < max_replacement else row.mass, axis=1)
    divisor = (dfplus.loc[dfplus.index == 4, "expmass"].values[0] * dfplus.loc[dfplus.index == 4, "mf"].values[0])
    logger.info("Trying to smoothen output.")
    df, dfplus = revertFractions(user_args.well_conn, df, dfplus)
    df, dfplus = smooth(df, dfplus, df.loc[~df.expmass.isnull()].index.max(), 3)
    df["wf"] = (df["mass"] * df["mf"]) / divisor
    if df.index.max() >= dfplus.index.max():
        save(df, user_args, identifier="mass", plus_or_individual="individual", solver=3,
             distribution="general")
        save(dfplus, user_args, identifier="mass", plus_or_individual="plus", solver=3, distribution="general")
        return True

    logger.info("Max Plus fraction is less than user provided plus fraction. Discarding output.")
    return False


def solver3(user_args):
    df, dfplus = mass_input(3, user_args.well_conn)
    start_points, bounds, c4mass = get_bounds(dfplus)
    res = solver3_1(start_points, bounds, df.copy(deep=True), dfplus.copy(deep=True), user_args, c4mass)
    if not res["success"]:
        logger.debug(res)
        logger.info("General Mass Solver 3 failed to find a feasible solution.")
        return False
    res = solver3_2(res, df.copy(deep=True), dfplus.copy(deep=True), user_args, c4mass)
    if not res["success"]:
        print 'Error:{"Type": "Mass Solver", "Message": "Mass Solver failed to find a feasible solution. ' \
              'Please check input data. If issue persists, please raise with POAS support team."}'
        raise Exception("Failed mass solver 3")
    return process_result(res, df, dfplus, user_args)


"""
executed when input max plus fraction is > 19
"""

if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection
    from Scripts.pvt_new.settings import DEBUG_DIR
    import os

    well_name = "ALUMM"

    debug_dir = os.path.join(DEBUG_DIR, well_name)

    db = get_connection(well_name)
    test_user_args = pd.Series({"debug": True, "well_conn": db, "bias": "general", "debug_dir": debug_dir})
    solver3(test_user_args)
