import numpy as np
from scipy import optimize

from Scripts.pvt_new.core.get_logger import get_logger

logger = get_logger("pvt beta wi smooth")


def calcWf(var, df, solver=3):
    fs = max(8, df.index.min() + 1)
    fs = max(fs + 1, 10)
    df_s = df["wf"]
    for i in range(0, df.shape[0] - 5):
        ti = i + df.index.min()  # true index
        if ti < fs - 2:
            continue
        if solver == 3:
            df.loc[df.index == ti + 2, 'new_wf'] = (1. / 8. * df_s.iloc[i] + 1. / 4. * df_s.iloc[i + 1] + 1. / 4. *
                                                    df_s.iloc[i + 2] + 1. / 4. * df_s.iloc[i + 3] + 1. / 8. *
                                                    df_s.iloc[i + 4]) * var[0]
        else:
            df.loc[df.index == ti + 2, 'new_wf'] = np.median(
                [df_s.iloc[i], df_s.iloc[i + 1], df_s.iloc[i + 2], df_s.iloc[i + 3], df_s.iloc[i + 4]])
    df.loc[df.new_wf.isnull(), "new_wf"] = df.wf
    return df


def cost_function(var, df_o):
    df = df_o.copy(deep=True)
    df = calcWf(var, df)
    df["error"] = np.abs((df.wf - df.new_wf) / df.wf)
    df.loc[df.error > 10., "error"] = 0
    return df.error.sum() * 0.01


def smooth(df, solver):
    logger.info("Executing Beta Mass Solver %s stage 1" % solver)
    df["zimi"] = df.mass5 * df.mf
    df["wf"] = df.zimi / df.zimi.sum()
    df["slope"] = (df.mf - df.mf.shift(1))
    fs = max(8, df.index.min() + 1)
    avg_slope = np.mean(df.loc[df.index.isin([fs, fs + 1]), "slope"].tolist())
    swf = df.loc[df.index == fs - 1, "wf"].values[0] + avg_slope
    if not (df.loc[df.index == fs, "wf"].values[0] * 0.9 <= swf <= df.loc[df.index == fs, "wf"].values[0] * 1.1):
        df.loc[df.index == fs, "wf"] = swf
    if solver == 3:
        res = optimize.minimize(cost_function, np.array([1.]), bounds=[[0.8, 1.2]], method="SLSQP",
                                args=df, options={'maxiter': 500})
        if not res.success:
            logger.debug(res)
            logger.info("Beta Mass Solver 3 stage 1 failed to find a feasible solution.")
            return False
        logger.info("Completed Beta Mass Solver 3 stage 1")
        logger.info("Beta Mass solver stage 1 final variables - %s, objective - %s" % (
            ",".join([str(x) for x in res.x]), res.fun))
        df = calcWf(res.x, df)
    else:
        df = calcWf([1.], df, solver)
    df["wf"] = df.new_wf
    df["wf"] = df.wf * (1. / df.wf.sum())
    df.drop(columns=["new_wf"], inplace=True)
    return df
