from scipy.stats import beta as betaInbuilt


def calcMass(df, alpha, eta, f_mass):
    beta = calcBeta(alpha, eta, f_mass)
    b_max = 15000.
    df["y"] = (df.mb - eta) / (b_max - eta)
    df.loc[df.y > 1., "y"] = 0.9999999999
    df["p0"] = df.apply(lambda row1: betaInbuilt.cdf(row1.y, alpha, beta), axis=1)
    df.loc[df.p0 > 1., "p0"] = 0.9999999999
    df["p1"] = df.apply(lambda row1: betaInbuilt.cdf(row1.y, alpha + 1, beta), axis=1)
    df.loc[df.p1 > 1., "p1"] = 0.9999999999
    df['p0diff'] = df['p0'].sub(df['p0'].shift())
    df['p0diff'] = df['p0diff'].fillna(df['p0'].iloc[0])
    df['p1diff'] = df['p1'].sub(df['p1'].shift())
    df['p1diff'] = df['p1diff'].fillna(df['p1'].iloc[0])
    df.loc[df.p0diff == 0, "p0diff"] = 1.
    df["mass"] = eta + (alpha / (alpha + beta)) * (b_max - eta) * df.p1diff.values / df.p0diff.values
    df["mass_Beta"] = df.mass
    for scn, row in df.iterrows():
        if (scn - 1.) in df.index:
            prow = df.loc[df.index == row.name - 1].squeeze()
            if row.mass < prow.mass:
                df.loc[df.index == scn, "mass"] = row.mass5
    for scn, row in df.iterrows():
        if (scn + 1.) in df.index:
            n_row = df.loc[df.index == row.name + 1].squeeze()
            if row.mass > n_row.mass:
                df.loc[df.index == scn, "mass"] = row.mass5
    return df


def calcDensity(df, alpha, eta, beta):
    b_max = 3.
    df["y"] = (df.db - eta) / (b_max - eta)
    df.loc[df.y > 1., "y"] = 0.9999999999
    df["p0"] = df.apply(lambda row1: betaInbuilt.cdf(row1.y, alpha, beta), axis=1)
    df.loc[df.p0 > 1., "p0"] = 0.9999999999
    df["p1"] = df.apply(lambda row1: betaInbuilt.cdf(row1.y, alpha + 1, beta), axis=1)
    df.loc[df.p1 > 1., "p1"] = 0.9999999999
    df['p0diff'] = df['p0'].sub(df['p0'].shift())
    df['p0diff'] = df['p0diff'].fillna(df['p0'].iloc[0])
    df['p1diff'] = df['p1'].sub(df['p1'].shift())
    df['p1diff'] = df['p1diff'].fillna(df['p1'].iloc[0])
    df.loc[df.p0diff == 0, "p0diff"] = 1.
    df["density"] = eta + (alpha / (alpha + beta)) * (b_max - eta) * df.p1diff.values / df.p0diff.values
    df["density_Beta"] = df.density
    for scn, row in df.iterrows():
        if (scn - 1.) in df.index:
            prow = df.loc[df.index == row.name - 1].squeeze()
            if row.density < prow.poas_density:
                df.loc[df.index == scn, "density"] = row.poas_density
    for scn, row in df.iterrows():
        if (scn + 1.) in df.index:
            n_row = df.loc[df.index == row.name + 1].squeeze()
            if row.density > n_row.poas_density:
                df.loc[df.index == scn, "density"] = row.poas_density
    return df


def calcBeta(alpha, eta, f_mass):
    b_max = 15000.
    return alpha * (b_max - f_mass) / (f_mass - eta)
