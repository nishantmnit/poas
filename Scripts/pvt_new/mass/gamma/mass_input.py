import pandas as pd

from Scripts.pvt_new.core.storage.mongo import fetch
from Scripts.pvt_new.validation.validate import average
from Scripts.pvt_new.validation.whitson import define_plus_fractions


def mass_input(solver, well_conn):
    df_o = fetch(well_conn, property="pvt_output", identifier="validated_averaged_data",
                 plus_or_individual="individual")
    dfplus_o = fetch(well_conn, property="pvt_output", identifier="validated_averaged_data", plus_or_individual="plus")
    df_o = average(df_o, df_o.index.min())
    try:
        df = fetch(well_conn, property="pvt_output", identifier="mass", distribution="general", solver=5,
                   plus_or_individual="individual")
        dfplus = fetch(well_conn, property="pvt_output", identifier="mass", distribution="general", solver=5,
                       plus_or_individual="plus")
    except:
        df = fetch(well_conn, property="pvt_output", identifier="mass", distribution="general", solver=6,
                   plus_or_individual="individual")
        dfplus = fetch(well_conn, property="pvt_output", identifier="mass", distribution="general", solver=6,
                       plus_or_individual="plus")
    df[["expmass", "expdensity"]] = df_o[["expmass", "expdensity"]]
    dfplus[["expmass", "expdensity"]] = dfplus_o[["expmass", "expdensity"]]
    added = False
    if df.index.max() not in dfplus.index:
        dfplus = pd.concat([dfplus, df.loc[df.index == df.index.max()]], axis=0, sort=True)
        added = True
    dfplus["expmass"] = dfplus["mass"]
    df["expmass"] = df["mass"]
    fs = df.loc[df.mf > 10 ** -5].index.min()
    df.drop(df.loc[df.index < fs].index, inplace=True)
    desired = [7, 12, 20, 25, 30, 36] + dfplus.index.tolist() + [max(7, df.index.min())]
    desired = list(set(desired))
    desired = [scn for scn in desired if scn >= max(7, df.index.min())]
    desired.sort()
    dfplus = define_plus_fractions(df, dfplus, desired)
    df[["expmass", "expdensity"]] = df_o[["expmass", "expdensity"]]
    if added:
        dfplus.drop([df.index.max()], inplace=True)

    df["scn"] = df.apply(lambda row: "C%s" % row.name, axis=1)
    df.drop(columns=["expdensity"], inplace=True)
    dfplus.drop(columns=["expdensity"], inplace=True)
    df.rename(columns={'mass': 'mass5'}, inplace=True)
    dfplus.rename(columns={'mass': 'mass5'}, inplace=True)

    if solver > 2:
        df_2 = fetch(well_conn, property="pvt_output", identifier="mass", distribution="gamma", solver=2,
                     plus_or_individual="individual")
        df["mass2"] = df_2.mass

    return df, dfplus


if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection

    db = get_connection("AB8")
    print mass_input(1, db)
