import numpy as np
from scipy import optimize

from Scripts.pvt_new.core.get_logger import get_logger
from Scripts.pvt_new.core.storage.save import save
from Scripts.pvt_new.mass.general.common import revertFractions
from mass_input import mass_input
from wismooth import smooth

logger = get_logger("pvt gamma mass solver 3")


def calcNewMF(df):
    df["zimi"] = df.mass5 * df.mf
    df["new_mf"] = df.wf * df.zimi.sum() / df.mass5
    df["new_mf"] = df.new_mf * (df.mf.sum() / df.new_mf.sum())
    df["mf"] = df.new_mf
    df.drop(columns=["new_mf"], inplace=True)
    return df


def calculateMass(var, df, dfplus):
    df["correction"] = (4.81668632961298 * np.exp(0.0140674955555049 * df.index)) * var[0]
    df.loc[df.index <= 5, "mass"] = df.expmass
    df.loc[df.index > 5, "mass"] = df.mass2 + df.mass2 * df.correction / 100.
    df["zimi"] = df.mf * df.mass
    dfplus["mf"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "mf"].sum(), axis=1)
    dfplus["mass"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "zimi"].sum() / row.mf, axis=1)
    return df, dfplus


def process_result(res, df, dfplus, user_args):
    df, dfplus = calculateMass(res.x, df, dfplus)
    df, dfplus = revertFractions(user_args.well_conn, df, dfplus)

    divisor = df.zimi.sum()
    df["wf"] = (df["mass"] * df["mf"]) / divisor
    save(df, user_args, identifier="mass", plus_or_individual="individual", solver=3, distribution="gamma")
    save(dfplus, user_args, identifier="mass", plus_or_individual="plus", solver=3, distribution="gamma")
    return True


def cost_function(var, df_o, dfplus):
    df = df_o.copy(deep=True)
    df, dfplus = calculateMass(var, df, dfplus)
    dfplus["error"] = ((dfplus.expmass - dfplus.mass) / dfplus.expmass) ** 2 / 0.001
    error = dfplus.error.sum()
    return error


def runSolver(df, dfplus, solver):
    df = calcNewMF(df)
    res = optimize.minimize(cost_function, np.array([0.]), bounds=[[-3., 3.]], method="SLSQP", args=(df, dfplus),
                            options={'maxiter': 500})
    if not res.success:
        print res
        logger.info("Gamma Mass Solver %s failed to find a feasible solution." % solver)
        res.x = [1.]
    logger.info("Completed Gamma Mass Solver %s" % solver)
    logger.info("Gamma Mass solver %s final variables - %s, objective - %s" % (
        solver, ",".join([str(x) for x in res.x]), res.fun))
    return res, df, dfplus


def solver3(user_args):
    logger.info("Executing Gamma Mass Solver 3")
    df, dfplus = mass_input(3, user_args.well_conn)
    df = smooth(df, 3)
    if isinstance(df, bool):
        print 'Warning:{"Type": "Mass Solver", "Message": "Gamma Mass Solver 3 failed to find a ' \
              'feasible solution. Executing Solver 4."}'
        return False
    res, df, dfplus = runSolver(df, dfplus, 3)
    return process_result(res, df, dfplus, user_args)


"""
executed for all inputs
"""

if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection
    import pandas as pd
    from Scripts.pvt_new.settings import DEBUG_DIR
    import os

    well_name = "ALUMM"

    debug_dir = os.path.join(DEBUG_DIR, well_name)

    db = get_connection(well_name)
    test_user_args = pd.Series({"debug": True, "well_conn": db, "bias": "general", "debug_dir": debug_dir})
    solver3(test_user_args)
