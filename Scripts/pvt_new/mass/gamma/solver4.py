from Scripts.pvt_new.core.get_logger import get_logger
from Scripts.pvt_new.core.storage.save import save
from Scripts.pvt_new.mass.general.common import revertFractions
from mass_input import mass_input
from solver3 import runSolver, calculateMass
from wismooth import smooth

logger = get_logger("pvt gamma mass solver 4")


def solver4(user_args):
    logger.info("Executing Gamma Mass Solver 4")
    df, dfplus = mass_input(4, user_args.well_conn)
    df = smooth(df, 4)
    res, df, dfplus = runSolver(df, dfplus, 4)
    return process_return(res, df, dfplus, user_args)


def process_return(res, df, dfplus, user_args):
    df, dfplus = calculateMass(res.x, df, dfplus)
    df, dfplus = revertFractions(user_args.well_conn, df, dfplus)

    divisor = df.zimi.sum()
    df["wf"] = (df["mass"] * df["mf"]) / divisor
    save(df, user_args, identifier="mass", plus_or_individual="individual", solver=4, distribution="gamma")
    save(dfplus, user_args, identifier="mass", plus_or_individual="plus", solver=4, distribution="gamma")
    return True


"""
executed for all inputs
"""

if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection
    import pandas as pd
    from Scripts.pvt_new.settings import DEBUG_DIR
    import os

    well_name = "ALUMM"

    debug_dir = os.path.join(DEBUG_DIR, well_name)

    db = get_connection(well_name)
    test_user_args = pd.Series({"debug": True, "well_conn": db, "bias": "general", "debug_dir": debug_dir})
    solver4(test_user_args)
