import numpy as np
from scipy import optimize

from Scripts.pvt_new.core.get_logger import get_logger
from Scripts.pvt_new.core.storage.save import save
from Scripts.pvt_new.mass.general.common import revertFractions
from common import calcMass
from mass_input import mass_input

logger = get_logger("pvt gamma mass solver 2")


class MassSolver2:
    def __init__(self):
        self.alpha = None
        self.eta = None

    def optimize(self, user_args):
        logger.info("Executing Gamma Mass Solver 2")
        df, dfplus = mass_input(2, user_args.well_conn)
        sp, bounds = self.startPoint(df, dfplus)
        res, attempt = "Failed", 1
        while type(res) == str:
            res, df, dfplus = self.runSolver(df, dfplus, sp, bounds, attempt)
            attempt += 1
        logger.info("Completed Gamma Mass Solver 2")
        logger.info(
            "Gamma Mass solver final variables - %s, objective - %s" % (",".join([str(x) for x in res.x]), res.fun))
        return self.process_result(res, df, dfplus, user_args)

    def runSolver(self, df, dfplus, sp, bounds, attempt):
        target = None if attempt == 1 else 1
        res = optimize.minimize(self.cost_function, sp, bounds=bounds, method="SLSQP", args=(df, dfplus, target),
                                options={'maxiter': 500})
        if not res.success:
            if attempt > 2:
                res.x = [1., 1., 1.]
                return res, df, dfplus
            else:
                print res
                logger.info("Gamma Mass Solver 2 failed to find a feasible solution with Target = %s" % target)
                return "Failed", df, dfplus
        return res, df, dfplus

    def startPoint(self, df_o, dfplus):
        df = df_o
        df_o["mb"] = (df_o.mass5 + df_o.mass5.shift(-1)) / 2.
        df_o.loc[df_o.index == df_o.index.max(), "mb"] = 10000.
        df["ziinside"] = df.mf / df.mf.sum()
        df["ziinsidemi"] = df.ziinside * df.mass5
        df["variance"] = df.ziinside * (df.mass5 - df.ziinsidemi.sum()) ** 2
        variance = df.variance.sum()
        m_mass = df.ziinsidemi.sum()
        eta = max(df.mass5.head(1).values[0] * 0.98, 65.)
        alpha = (m_mass - eta) ** 2 / variance
        alpha_bound = [max(alpha * 0.65, 0.5) / alpha,
                       max((1.96085987239067 * np.log(dfplus.expmass.head(1).values[0]) - 7.28596631070479), 2) / alpha]
        # mass BD multiplier, alpha multiplier, eta multiplier
        self.alpha, self.eta = alpha, eta
        return [1., max(alpha_bound[0] * 2.3, 2.3), 1.], [[0.98, 1.2], alpha_bound, [0.5, (
                np.mean(df.mass5.head(2).values) * 0.98 * (1. - 0.01 / 100.)) / eta]]

    def cost_function(self, iter_vars, df_o, dfplus, target=None):
        df = df_o.copy(deep=True)
        df, dfplus = self.calculateMass(iter_vars, df, dfplus)
        dfplus["error"] = ((dfplus.expmass - dfplus.mass) / dfplus.expmass) ** 2 / (0.001 if target is None else 1)
        error = dfplus.error.sum() + df.error.sum() + df.error.head(1).values[0] / (0.01 if target is None else 1)
        return error

    def calculateMass(self, iter_vars, df, dfplus):
        mb_multi, alpha_multi, eta_multi = iter_vars
        alpha, eta = self.alpha * alpha_multi, self.eta * eta_multi
        df["mb"] = df.mb * mb_multi
        df["zimi"] = df.mass5 * df.mf
        df = calcMass(df, alpha, eta, df.zimi.sum() / df.mf.sum())
        df["error"] = ((df.mass5 - df.mass) / df.mass5) ** 2
        df.loc[df.mass5 == 0, "error"] = 0
        df.loc[~df.expmass.isnull(), "mass"] = df.expmass
        df["zimi"] = df.mf * df.mass
        dfplus["mass"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "zimi"].sum() / row.mf, axis=1)
        return df, dfplus

    def process_result(self, res, df, dfplus, user_args):
        df, dfplus = self.calculateMass(res.x, df, dfplus)
        df, dfplus = revertFractions(user_args.well_conn, df, dfplus)

        divisor = df.zimi.sum()
        df["wf"] = (df["mass"] * df["mf"]) / divisor
        save(df, user_args, identifier="mass", plus_or_individual="individual", solver=2, distribution="gamma")
        save(dfplus, user_args, identifier="mass", plus_or_individual="plus", solver=2, distribution="gamma")
        return True


def solver2(user_args):
    obj = MassSolver2()
    obj.optimize(user_args)


"""
executed for all inputs
"""

if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection
    import pandas as pd
    from Scripts.pvt_new.settings import DEBUG_DIR
    import os

    well_name = "ALUMM"

    debug_dir = os.path.join(DEBUG_DIR, well_name)

    db = get_connection(well_name)
    test_user_args = pd.Series({"debug": True, "well_conn": db, "bias": "general", "debug_dir": debug_dir})
    solver2(test_user_args)
