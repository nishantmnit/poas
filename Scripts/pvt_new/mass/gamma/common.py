from scipy.special import gammainc, gammaincinv


def calcBeta(alpha, eta, f_mass):
    return (f_mass - eta) / alpha


def calcBp(X, Y):
    alpha, eta, beta = X
    df = Y.copy(deep=True)
    df["y"] = df.apply(lambda row: gammaincinv(alpha, row.cummxvi if row.cummxvi < 0.99999999 else 0.99999999),
                       axis=1)
    df["p0"] = df.apply(lambda row: gammainc(alpha, row.y), axis=1)
    df["p1"] = df.apply(lambda row: gammainc(alpha + 1, row.y), axis=1)
    df['p0diff'] = df['p0'].sub(df['p0'].shift())
    df["p0diff"] = df["p0diff"].fillna(df['p0'].iloc[0])
    df['p1diff'] = df['p1'].sub(df['p1'].shift())
    df['p1diff'] = df['p1diff'].fillna(df['p1'].iloc[0])
    df["bp"] = eta + alpha * beta * (df.p1diff.values / df.p0diff.values)
    df["error"] = ((df.expbp - df.bp) / df.expbp) ** 2
    return df


def calcMass(df, alpha, eta, f_mass):
    beta = calcBeta(alpha, eta, f_mass)
    df["y"] = (df.mb - eta) / beta
    df["p0"] = df.apply(lambda row1: gammainc(alpha, row1.y), axis=1)
    df["p1"] = df.apply(lambda row1: gammainc(alpha + 1, row1.y), axis=1)
    df['p0diff'] = df['p0'].sub(df['p0'].shift())
    df["p0diff"] = df["p0diff"].fillna(df['p0'].iloc[0])
    df['p1diff'] = df['p1'].sub(df['p1'].shift())
    df['p1diff'] = df['p1diff'].fillna(df['p1'].iloc[0])
    df.loc[df.p0diff == 0, "p0diff"] = 1.
    df["mass"] = eta + alpha * beta * (df.p1diff / df.p0diff)
    df["mass_gamma"] = df.mass
    for scn, row in df.iterrows():
        if (scn - 1.) in df.index:
            prow = df.loc[df.index == row.name - 1].squeeze()
            if row.mass < prow.mass:
                df.loc[df.index == scn, "mass"] = row.mass5
    for scn, row in df.iterrows():
        if (scn + 1.) in df.index:
            nrow = df.loc[df.index == row.name + 1].squeeze()
            if row.mass > nrow.mass:
                df.loc[df.index == scn, "mass"] = row.mass5
    return df
