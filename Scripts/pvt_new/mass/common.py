def findLastZi(df, col, max_zi):
    df["cummtemp"] = df[col].cumsum()
    maxscn = df.loc[df.cummtemp >= max_zi].index.values
    if len(maxscn) > 0:
        maxscn = maxscn[0]
        df.drop(df.loc[df.index > maxscn].index, inplace=True)
    else:
        maxscn = df.index.max()
    return maxscn, df
