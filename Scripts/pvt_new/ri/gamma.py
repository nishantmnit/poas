import numpy as np
from scipy import optimize
from scipy.special import gammainc

from Scripts.pvt_new.core.get_logger import get_logger
from Scripts.pvt_new.core.storage.mongo import fetch
from Scripts.pvt_new.core.storage.save import save

logger = get_logger("pvt gamma ri")


def getPoasRi(well_conn):
    df = fetch(well_conn, identifier="ri", plus_or_individual="individual", solver=1, distribution="poas")
    return df


class RiSolver1:
    def __init__(self):
        self.beta = None
        self.alpha = None
        self.eta = None

    def optimize(self, user_args):
        logger.info("Starting Gamma RI Solver 1")
        df = getPoasRi(user_args.well_conn)
        sp, bounds = self.startPoint(df)
        res = optimize.minimize(self.cost_function, np.array(sp), bounds=bounds, method="SLSQP", args=df,
                                options={'maxiter': 500})
        if not res.success:
            logger.debug(res)
            logger.info("Gamma RI Solver failed to find a feasible solution.")
            res.x = [1., 1., 1., 1.]
        logger.info("Completed Gamma RI Solver 1")
        logger.info(
            "Gamma RI solver final variables - %s, objective - %s" % (",".join([str(x) for x in res.x]), res.fun))
        return self.process_result(res, df, user_args)

    def startPoint(self, df_o):
        df = df_o.copy(deep=True)
        df["xviri"] = df.xvi * df.poas_ri
        smean = df.xviri.sum()
        df["variance"] = df.xvi * (smean - df.poas_ri) ** 2
        variance = df.variance.sum()
        eta = min(0.196, df.poas_ri.head(1).values[0] * 0.98)
        alpha = min(2., (smean - eta) ** 2 / variance)
        beta = (smean - eta) / alpha
        # mass boundary, alpha, beta, eta
        self.eta, self.alpha, self.beta = eta, alpha, beta
        df_o["rb"] = (df_o.poas_ri + df_o.poas_ri.shift(-1)) / 2.
        df.loc[df.index == df.index.max(), "rb"] = max(0.65, df.loc[df.index == df.index.max(), "poas_ri"].values[0])
        bounds = [[0.98, 1.2], [0.2, 3.], [0.2, 3], [0.5, (df_o.rb.min() * 0.98 * (1. - 0.01 / 100.)) / eta]]
        return [1., 1., 1., 1.], bounds

    def process_result(self, res, df, user_args):
        df = self.calculate_ri(res.x, df)
        save(df, user_args, identifier="ri", plus_or_individual="individual", solver=1, distribution="gamma")

    def cost_function(self, i_vars, df_o):
        df = df_o.copy(deep=True)
        df = self.calculate_ri(i_vars, df)
        c4_c = (df.xvi * df.ri).sum()
        mc4 = df.zimi.sum() / df.mf.sum()
        dc4 = df.zimi.sum() / (df.zimi / df.density).sum()
        c4_e = (0.12399 *
                np.exp(3.4622 * 10 ** -4 * mc4 + 0.90389 * dc4 - 6.0955 * 10 ** -4 * mc4 * dc4) * mc4 ** 0.02264 *
                dc4 ** 0.22423) if mc4 <= 300 else (
                0.01102 *
                np.exp(-8.61126 * 10 ** -4 * mc4 + 3.228607 * dc4 + 9.07171 * 10 ** -4 * mc4 * dc4) * mc4 ** 0.02426 *
                dc4 ** -2.25051)
        error = df.exp_error.sum() / 0.1 + df.poas_error.sum() / 0.1 + (np.abs(c4_e - c4_c) / c4_e)
        return error

    def calculate_ri(self, i_vars, df):
        rb_multi, alpha_multi, beta_multi, eta_multi = i_vars
        alpha, beta, eta = self.alpha * alpha_multi, self.beta * beta_multi, self.eta * eta_multi
        # ri calculations
        df["rb"] = df.rb * rb_multi
        df["y"] = (df.rb - eta) / beta
        df["p0"] = df.apply(lambda row1: gammainc(alpha, row1.y), axis=1)
        df["p1"] = df.apply(lambda row1: gammainc(alpha + 1, row1.y), axis=1)
        df['p0diff'] = df['p0'].sub(df['p0'].shift())
        df["p0diff"] = df["p0diff"].fillna(df['p0'].iloc[0])
        df['p1diff'] = df['p1'].sub(df['p1'].shift())
        df['p1diff'] = df['p1diff'].fillna(df['p1'].iloc[0])
        df.loc[df.p0diff == 0, "p0diff"] = 1.
        df["ri"] = eta + alpha * beta * (df.p1diff / df.p0diff)
        df["ri_gamma"] = df.ri
        df["seq"] = range(0, df.shape[0])

        for scn, row in df.iterrows():
            if (row.seq - 1.) in df.seq.tolist():
                prow = df.loc[df.seq == row.seq - 1].squeeze()
                if row.ri < prow.ri:
                    df.loc[df.seq == row.seq, "ri"] = row.poas_ri
        for scn, row in df.iterrows():
            if (row.seq + 1.) in df.seq.tolist():
                n_row = df.loc[df.seq == row.seq + 1].squeeze()
                if row.ri > n_row.ri:
                    df.loc[df.seq == row.seq, "ri"] = row.poas_ri

        df["exp_error"] = ((df.expri - df.ri) / df.expri) ** 2
        df["poas_error"] = ((df.poas_ri - df.ri) / df.poas_ri) ** 2

        # df.scn = df.index
        # df.index = df.seq
        df.loc[df.index <= 5, "ri"] = df.loc[df.index <= 5, "expri"]
        # df.index = df.scn
        return df


def gamma(user_args):
    obj = RiSolver1()
    obj.optimize(user_args)


"""
executed for all inputs
"""

if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection
    import pandas as pd
    from Scripts.pvt_new.settings import DEBUG_DIR
    import os

    well_name = "ALUMM"

    debug_dir = os.path.join(DEBUG_DIR, well_name)

    db = get_connection(well_name)
    test_user_args = pd.Series({"debug": True, "well_conn": db, "bias": "general", "debug_dir": debug_dir})
    gamma(test_user_args)
