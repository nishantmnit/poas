import numpy as np

from Scripts.pvt_new.core.get_logger import get_logger
from Scripts.pvt_new.core.storage.save import save
from ri_input import ri_input

logger = get_logger("pvt poas ri")


def calcRi(d20):
    if d20 <= 0.65:
        return -0.771988205690377 * d20 ** 2 + 1.25683786884901 * d20 - 0.263582391020546
    elif d20 <= 0.685:
        return 0.323066708963071 * d20 ** 0.825367199519558
    elif d20 <= 0.694:
        return 9.33747573594155 * d20 ** 2 - 12.6643155256813 * d20 + 4.52953399612808
    elif d20 <= 0.7:
        return 45.2446673429513 * d20 ** 2 - 62.3150182598514 * d20 + 21.6929685696088
    elif d20 <= 0.74:
        return 0.310905890442151 * d20 ** 0.697222127007676
    elif d20 <= 0.9:
        return 0.131843282949603 * d20 ** 2 + 0.100833492150919 * d20 + 0.100895494309563 + 0.004
    elif d20 <= 1.17:
        return 0.280866083905693 * d20 ** 2 - 0.209842167791342 * d20 + 0.254487058045577 + 0.009
    elif d20 <= 2.8:
        return -0.126906413488938 * d20 ** 2 + 0.651119502049113 * d20 - 0.185838323984245
    else:
        return 0.247678193996086 * np.log(d20) + 0.375251277321885 + 0.012580930886738


def process_result(df, user_args):
    df["ri"] = df.poas_ri
    save(df, user_args, identifier="ri", plus_or_individual="individual", solver=1, distribution="poas")
    return True


def poas(user_args):
    logger.info("Executing POAS Ri Finder.")
    df = ri_input(user_args.well_conn)
    df.rename(columns={'expbp': 'expri'}, inplace=True)
    df["d60"] = df.density
    df["d20"] = ((df.d60 * 1000 * np.exp((-1 * (613.97226 / (df.d60 * 1000) ** 2) * (20 - 15) * (
            1 + 0.8 * (613.97226 / (df.d60 * 1000) ** 2) * (20 - 15))))) / 1000)
    df["poas_ri"] = df.apply(lambda row: calcRi(row.d20), axis=1)
    process_result(df, user_args)
    return df


"""
executed for all inputs
"""

if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection
    import pandas as pd
    from Scripts.pvt_new.settings import DEBUG_DIR
    import os

    well_name = "ALUMM"

    debug_dir = os.path.join(DEBUG_DIR, well_name)

    db = get_connection(well_name)
    test_user_args = pd.Series({"debug": True, "well_conn": db, "bias": "general", "debug_dir": debug_dir})
    poas(test_user_args)
