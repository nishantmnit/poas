from Scripts.pvt_new.bp.general import solver1
from Scripts.pvt_new.core.get_logger import get_logger
from Scripts.pvt_new.core.storage.save import save
from common import calcBp
from ri_input import ri_input

logger = get_logger("pvt general ri")


def get_bounds():
    bounds = dict()
    A, B, eta = 0.9, 2.5, 0.18
    bounds["A"] = [0.2, 5.]
    bounds["B"] = [0.2, 5.]
    bounds["Eta"] = [0., 0.5]
    initialEtaFn = (eta - bounds["Eta"][0]) / (bounds["Eta"][1] - bounds["Eta"][0])
    initialAFn = (A - bounds["A"][0]) / (bounds["A"][1] - bounds["A"][0])
    initialBFn = (B - bounds["B"][0]) / (bounds["B"][1] - bounds["B"][0])
    return ([initialAFn, initialBFn, initialEtaFn],
            [[bounds["A"][0], bounds["A"][1]], [bounds["B"][0], bounds["B"][1]],
             [bounds["Eta"][0], bounds["Eta"][1]]])


def process_result(res, df, user_args):
    df = calcBp(res["Values"], df)
    df.loc[df.index <= 5, "bp"] = df.loc[df.index <= 5, "expbp"]
    df.rename(columns={'expbp': 'expri', 'bp': 'ri'}, inplace=True)
    save(df, user_args, identifier="ri", plus_or_individual="individual", solver=1, distribution="general")


def general(user_args):
    df = ri_input(user_args.well_conn)
    start_points, bounds = get_bounds()
    res = solver1(start_points, bounds, df, user_args, printname="RI")
    if not res["success"]:
        msg = 'Error:{"Type": "RI Solver", "Message": "RI Solver failed to find a feasible solution. ' \
              'Please check input data. If issue persists, please raise with POAS support team."}'
        raise Exception(msg)
    return process_result(res, df, user_args)


"""
executed for all inputs
"""

if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection
    import pandas as pd
    from Scripts.pvt_new.settings import DEBUG_DIR
    import os

    well_name = "ALUMM"

    debug_dir = os.path.join(DEBUG_DIR, well_name)

    db = get_connection(well_name)
    test_user_args = pd.Series({"debug": True, "well_conn": db, "bias": "general", "debug_dir": debug_dir})
    general(test_user_args)
