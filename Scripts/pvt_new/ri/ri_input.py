import math

import numpy as np
import pandas as pd
from numpy import exp

from Scripts.pvt_new.core.get_logger import get_logger
from Scripts.pvt_new.core.storage.fetch_most import recommended, validated_data

logger = get_logger("pvt ri input")


def calExpRefractivity(mw, density, ri=float('NaN')):
    if not math.isnan(float(ri)) and not ri == 0:
        return float(ri)
    else:
        sg = density / 0.999016
        return 0.12399 * exp(
            3.4622 * 10 ** -4 * mw + 0.90389 * sg - 6.0955 * 10 ** -4 * mw * sg) * mw ** 0.02264 * sg ** 0.22423


def createLastFraction(df_o, well_conn):
    df_v, dfplus_v = validated_data(well_conn)
    ls = df_v.index.max()
    # create plus fraction where mf < 10**-8
    df_o["zimibysgi"] = df_o.zimi / df_o.density
    mp = df_o.loc[(df_o.mf < 10 ** -8) & (df_o.index > ls)].index.min() - 1
    if np.isnan(mp):
        return df_o
    else:
        mp = int(mp)
    mf, mass, zimi = df_o.loc[df_o.index >= mp, "mf"].sum(), df_o.loc[df_o.index >= mp, "zimi"].sum() / df_o.loc[
        df_o.index >= mp, "mf"].sum(), df_o.loc[df_o.index >= mp, "zimi"].sum()
    density = zimi / df_o.loc[df_o.index >= mp, "zimibysgi"].sum()
    temp_df = pd.DataFrame(data=[[mf, mass, zimi, "C%s" % mp, density]],
                           columns=["mf", "mass", "zimi", "scn", "density"], index=[mp])
    df_o.drop(df_o.loc[df_o.index >= mp].index, inplace=True)
    df_o = pd.concat([df_o, temp_df], sort=True, axis=0)
    return df_o


def ri_input(well_conn):
    try:
        df, dfplus = validated_data(well_conn)
        dfmass, dfplusmass = recommended(well_conn, "mass")
        dfdensity, dfplusdensity = recommended(well_conn, "density")
        df = pd.concat([df, dfmass.loc[dfmass.index > df.index.max()]], axis=0, sort=True)
        df["mass"] = dfmass["mass"]
        df["density"] = dfdensity["density"]
        df.loc[df.index <= 5, "mass"] = df.loc[df.index <= 5, "expmass"]
        df.loc[df.index <= 5, "density"] = df.loc[df.index <= 5, "expdensity"]
        df["scn"] = df.apply(lambda row: "C%s" % row.name, axis=1)
        df["zimi"] = df.mass * df.mf
        df = createLastFraction(df, well_conn)
        df["xwi"] = df.zimi / df.zimi.sum()
        df["xwibysgi"] = df.xwi / df.density
        df["xvi"] = df.xwibysgi / df.xwibysgi.sum()
        df["cummxvi"] = df.xvi.cumsum()
        df["expri"] = (df.expri ** 2 - 1.) / (df.expri ** 2 + 2.)
        df["expri"] = df.apply(
            lambda row: np.NaN if row.mass > 300 and math.isnan(float(row.expri)) else calExpRefractivity(
                row.mass, row.density, row.expri), axis=1)
        df.drop(columns=["expbp"], inplace=True)
        df.rename(columns={'expri': 'expbp'}, inplace=True)
    except Exception as e:
        logger.info('Error:{"Type": "Initialize", "Message": "Failed to initialize RI solver. '
                    'Please run mass and density solvers first."}')
        raise Exception(e)
    return df


if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection

    db = get_connection("ALUMM")
    print ri_input(db)
