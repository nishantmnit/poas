import numpy as np

from Scripts.pvt_new.core.storage.mongo import fetch
import pandas as pd
import os
from Scripts.pvt_new.settings import CSV_DATA
from scipy.interpolate import PchipInterpolator


def input_data(well_conn):
    gd = fetch(well_conn, identifier="validated_general_data", property="pvt_output")
    oil_type = gd.loc[gd.param == "fluid_type", "value"].values[0]
    temp_res = gd.loc[gd.param == "temp_res", "value"].values[0]
    temp_res_far = (temp_res - 273.15) * (9. / 5.) + 32.
    gor_pb_input = pd.read_csv(os.path.join(CSV_DATA, "pt_collection", oil_type, "bubble_points.csv"))
    gors = gor_pb_input.gor.unique()
    bps = list()
    for gor in gors:
        x = gor_pb_input.loc[gor_pb_input.gor == gor, "temperature"].values
        y = gor_pb_input.loc[gor_pb_input.gor == gor, "pressure"].values
        bps.append(PchipInterpolator(x, y)(temp_res_far))
    df = pd.DataFrame(columns=["gor", "pressure"], data=list(zip(gors, bps)))
    return df


def calc_min_max(df, well_conn):
    gd = fetch(well_conn, identifier="validated_general_data", property="pvt_output")
    pb_res_temp = gd.loc[gd.param == "pb_res_temp", "value"].values[0]
    pb_res_temp_psi = pb_res_temp * 145.038
    df = df.sort_values(by=["gor"], ascending=True)
    df.reset_index(inplace=True)
    if df.loc[df.pressure <= pb_res_temp_psi].shape[0] == 0:
        min_pressure = df.pressure.head(1).values[0].item(0)
        max_pressure = df.pressure.head(2).tail(1).values[0].item(0)
    elif df.loc[df.pressure >= pb_res_temp_psi].shape[0] == 0:
        max_pressure = df.pressure.tail(1).values[0].item(0)
        min_pressure = df.pressure.tail(2).head(1).values[0].item(0)
    else:
        min_pressure = df.loc[df.pressure <= pb_res_temp_psi, "pressure"].tail(1).values[0].item(0)
        max_pressure = df.loc[df.pressure >= pb_res_temp_psi, "pressure"].head(1).values[0].item(0)

    min_gor = df.loc[df.pressure == min_pressure, "gor"].values[0]
    max_gor = df.loc[df.pressure == max_pressure, "gor"].values[0]

    change_in_pressure = ((pb_res_temp_psi - min_pressure) / (max_pressure - min_pressure)) * 100.

    return [min_gor, max_gor], [min_pressure, max_pressure], change_in_pressure, pb_res_temp_psi


def calc_pt(well_conn, gor, change_in_pressure):
    gd = fetch(well_conn, identifier="validated_general_data", property="pvt_output")
    oil_type = gd.loc[gd.param == "fluid_type", "value"].values[0]
    gor_pb_input = pd.read_csv(os.path.join(CSV_DATA, "pt_collection", oil_type, "bubble_points.csv"))
    min_gor = gor_pb_input.loc[gor_pb_input.gor == gor[0]].copy(deep=True)
    max_gor = gor_pb_input.loc[gor_pb_input.gor == gor[1]].copy(deep=True)

    gor_dp_input = pd.read_csv(os.path.join(CSV_DATA, "pt_collection", oil_type, "due_points.csv"))
    min_gor_dp = gor_dp_input.loc[gor_dp_input.gor == gor[0]].copy(deep=True)
    max_gor_dp = gor_dp_input.loc[gor_dp_input.gor == gor[1]].copy(deep=True)

    min_gor_combined = pd.concat([min_gor, min_gor_dp], ignore_index=True)
    max_gor_combined = pd.concat([max_gor, max_gor_dp], ignore_index=True)

    min_gor_temp = min_gor_combined.temperature.max()
    max_gor_temp = max_gor_combined.temperature.max()

    if min_gor_temp == max_gor_temp or min_gor_temp < max_gor_temp:
        df = min_gor_combined[["gor", "temperature"]].copy(deep=True)
    else:
        df = max_gor_combined[["gor", "temperature"]].copy(deep=True)

    df.loc[df.index <= min_gor.shape[0], "pressure_1"] = PchipInterpolator(min_gor.temperature, min_gor.pressure)(
        df.loc[df.index <= min_gor.shape[0], "temperature"])
    df.loc[df.index <= min_gor.shape[0], "pressure_2"] = PchipInterpolator(max_gor.temperature, max_gor.pressure)(
        df.loc[df.index <= min_gor.shape[0], "temperature"])

    x = min_gor_dp.sort_values(by=["temperature"]).temperature
    y = min_gor_dp.sort_values(by=["temperature"]).pressure

    df.loc[df.index > min_gor.shape[0], "pressure_1"] = PchipInterpolator(x, y)(
        df.loc[df.index > min_gor.shape[0], "temperature"])

    x = max_gor_dp.sort_values(by=["temperature"]).temperature
    y = max_gor_dp.sort_values(by=["temperature"]).pressure

    df.loc[df.index > min_gor.shape[0], "pressure_2"] = PchipInterpolator(x, y)(
        df.loc[df.index > min_gor.shape[0], "temperature"])

    df.loc[df.temperature >= min_gor_temp, "pressure_1"] = \
        min_gor_dp.loc[min_gor_dp.temperature == min_gor_temp].pressure.values[0]
    df.loc[df.temperature >= max_gor_temp, "pressure_2"] = \
        max_gor_dp.loc[max_gor_dp.temperature == max_gor_temp].pressure.values[0]

    df["pressure_diff"] = df.pressure_2 - df.pressure_1
    df["offset"] = df.pressure_diff * change_in_pressure / 100.
    df["pressure"] = df.pressure_1 + df.offset

    df["theta"] = np.degrees(np.arctan(df.pressure / df.temperature))
    df["R"] = np.sqrt(df.pressure ** 2 + df.temperature ** 2)

    subset = df.loc[
        df.index >= df.loc[df.temperature == df.temperature.max()].index.values[0] - 2, ["theta", "R"]].head(3)
    subset = pd.concat([subset, df.loc[df.temperature == 150., ["theta", "R"]].tail(1)], ignore_index=True)
    subset = subset.sort_values(by=["theta"])

    start_index = df.loc[df.temperature == df.temperature.max()].index.values[0]
    start_theta = df.loc[df.index == start_index].theta.values[0]
    theta_150 = df.loc[df.temperature == 150., ["theta"]].tail(1).values[0].item(0)

    thetas = [start_theta]
    while thetas[-1] > theta_150:
        thetas.append(thetas[-1] - 0.5)
    thetas.append(theta_150)
    # NOTE: interpolation values are different from excel
    Rs = PchipInterpolator(subset.theta.values, subset.R.values)(thetas)

    temps = Rs * np.cos(np.radians(thetas))
    pressures = Rs * np.sin(np.radians(thetas))

    df1 = pd.DataFrame(columns=["temperature", "pressure"], data=list(zip(temps, pressures)))
    df = df.loc[df.index < start_index, ["temperature", "pressure"]]
    df = pd.concat([df, df1], ignore_index=True)

    df["temperature"] = (df.temperature - 32.) * 5. / 9. + 273.15
    df["pressure"] = df.pressure / 145.038
    df = df.sort_values(by=["temperature"])
    return df


def final_pt(well_conn, df):
    temps = [180, 200, 250, 288.7056, 298.15, 300, 350, 400, 450, 500, 550, 600, 650, 700,
             750, 800, 850, 900, 950, 1000, 1050, 1100, 1150, 1200]
    gd = fetch(well_conn, identifier="validated_general_data", property="pvt_output")
    temp_res = gd.loc[gd.param == "temp_res", "value"].values[0]
    temps = temps + [temp_res]

    max_temp = df.temperature.max()
    max_temp_index = df.loc[df.temperature == max_temp].index.values[0]
    max_pressure = df.loc[df.temperature == max_temp].pressure.values[0]

    x = df.loc[df.index <= max_temp_index].temperature.values
    y = df.loc[df.index <= max_temp_index].pressure.values
    bp = PchipInterpolator(x, y)(temps)

    mbp = fetch(well_conn, property="multiple_bubble_points")
    if mbp.shape[0] > 0:
        temps = temps + mbp.temperature.to_list()
        bp = list(bp) + mbp.bubble_point.to_list()

    out_df = pd.DataFrame(columns=["temperature", "bubble_point"], data=list(zip(temps, bp)))
    out_df.loc[out_df.temperature > max_temp, "bubble_point"] = max_pressure
    out_df.loc[out_df.bubble_point < 0.1, "bubble_point"] = 0.1

    for temp in [temp_res] + mbp.temperature.to_list():
        out_df.loc[out_df.temperature == temp, "user_provided"] = 1

    x = df.loc[df.index >= max_temp_index].temperature.values
    y = df.loc[df.index >= max_temp_index].pressure.values
    out_df["due_point"] = PchipInterpolator(x, y)(temps)
    out_df.loc[out_df.temperature > max_temp, "due_point"] = max_pressure
    out_df.loc[out_df.due_point < 0.01, "due_point"] = 0.01
    out_df.loc[~(out_df.user_provided == 1), "user_provided"] = 0
    return out_df


def run(well_conn):
    df = input_data(well_conn)
    gor, pressure, change_in_pressure, pb_res_temp_psi = calc_min_max(df, well_conn)
    df = calc_pt(well_conn, gor, change_in_pressure)
    df = final_pt(well_conn, df)
    return df


if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection
    db = get_connection("AB8")
    print run(db)
