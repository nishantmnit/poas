import numpy as np
import pandas as pd

from Scripts.pvt_new.core.get_logger import get_logger
from Scripts.pvt_new.core.optimizers.ncg import ncg
from Scripts.pvt_new.core.storage.mongo import fetch
from Scripts.pvt_new.pt_matching.solver.calculate import calculate_cost, calculate_cost_final
from Scripts.pvt_new.pt_matching.solver.input import get_input
from Scripts.pvt_new.pt_matching.solver.variables import define_vars

logger = get_logger("pt_matching")


def define_region(iter_vars, bound, mk_iter_vars):
    lb, ub = list(), list()
    for i in range(len(iter_vars)):
        lb.append(max(bound[i][0], (iter_vars[i] - 4. * iter_vars[i])))
        ub.append(min(bound[i][1], (iter_vars[i] + 4. * iter_vars[i])))
    a_max_l = list()
    for i in range(len(iter_vars)):
        a_max_l.append(
            np.abs(0 if mk_iter_vars[i] == 0
                   else (((iter_vars[i] - ub[i]) / mk_iter_vars[i]) if mk_iter_vars[i] < 0
                         else ((iter_vars[i] - lb[i]) / mk_iter_vars[i]))))

    positive_alphas = [i for i in a_max_l if i > 0]
    alpha_max = 1.2 * min(positive_alphas) if len(positive_alphas) > 0 else 10 ** -6

    if alpha_max / 10 ** 3 > 10 ** -8:
        alpha_min = 10 ** -8
    elif alpha_max / 10 ** 3 < 10 ** -13:
        alpha_min = 10 ** -13
    else:
        alpha_min = alpha_max / 10 ** 3
    return alpha_max, alpha_min


def get_alphas(iter_vars, bound, mk_iter_vars):
    a_max, a_min = define_region(iter_vars, bound, mk_iter_vars)
    q = (np.log(a_max)-np.log(a_min))/(np.log(20.)-np.log(1.))
    alphas = list()
    for i in range(1, 21):
        alphas.append(a_min * i ** q)
    return alphas


def converged(debug, all_vars, all_costs, all_gradients, f_tol):
    # rule 0 - converge if cost is  <= f_tol
    if f_tol is not None:
        if all_costs[-1] <= f_tol:
            if debug:
                logger.info("converged by rule 0 - cost (%s) is <= f_tol (%s)" % (all_costs[-1], f_tol))
            return True

    # rule 1 - if sqrt of sum of squared gradient is <= 10**-3 then exit.
    if np.sqrt(np.sum(np.square(all_gradients[-1]), axis=0)) <= 10 ** -3:
        if debug:
            logger.info("converged by rule 1 - sqrt of sum of squared gradient (%s) is <= 10**-3" % np.sqrt(
                np.sum(np.square(all_gradients[-1]), axis=0)))
        return True
    else:
        # we need to run at-least 4 iterations before declaring it converged
        if len(all_costs) < 4:
            return False
        else:
            # rule 3 - change in all variables <= 10**-10 for consecutive 3 iterations.
            var_diff1 = np.abs(np.array(all_vars[-1]) - np.array(all_vars[-2]))
            var_diff2 = np.abs(np.array(all_vars[-2]) - np.array(all_vars[-3]))
            if np.all(var_diff1 <= 10 ** -10) and np.all(var_diff2 <= 10 ** -10):
                if debug:
                    logger.info("converged by rule 3 - change in all variables <= 10**-10 for consecutive 3 iterations")
                return True
            if len(all_costs) >= 20:
                # rule 4 - if change in cost for consecutive 10 iterations is less than or equal to 10**-2 then exit.
                cost_diff = np.abs(np.ediff1d(np.array(all_costs))[-10:])
                if np.all(cost_diff <= 10 ** -2):
                    if debug:
                        logger.info("converged by rule 4 - change in cost for consecutive 10 "
                                    "iterations is less than or equal to 10**-2")
                    return True
    return False


def run(user_args, kij, temperature, bubble_point_pressure):
    logger.info("starting pt matching")
    ov_bp = get_input(user_args.well_conn, kij, bubble_point_pressure, temperature)
    solver_vars_names, bound, start_point = define_vars(ov_bp)

    logger.info("start of solver")
    final_vars = ncg().minimize(start_point, bound, calculate_cost, max_iter=200,
                                debug=user_args.debug, get_alphas_f=get_alphas,
                                alpha_multiplier=1.15, f_tol=10**-20, converge_f=converged,
                                args=(ov_bp, solver_vars_names))

    # TODO: test code. remove it.
    df_liquid, df_vapour = calculate_cost_final(final_vars, (ov_bp, solver_vars_names))
    df_liquid.to_csv("/Users/nishantagarwal/Downloads/df_liquid.csv")
    df_vapour.to_csv("/Users/nishantagarwal/Downloads/df_vapour.csv")

    logger.info("end of solver")


if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection

    db = get_connection("AB8")
    test_user_args = pd.Series({"debug": True, "well_conn": db, "eos": "poas_4", "kij": "all"})
    test_pt_collection = fetch(db, identifier="pt_collection", property="pvt_output")

    run(test_user_args, "kij-b", 395.372222222222, 12.1836861339033)
