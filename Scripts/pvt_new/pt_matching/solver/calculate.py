import numpy as np
import pandas as pd

from Scripts.pvt_new.core.constants import gasConstant
from Scripts.pvt_new.core.get_logger import get_logger
from common import get_multiplier, penalty
from gradient import grad
from variables import scn_groups

logger = get_logger("pt_matching calculate")


def add_extra_columns(df_liquid, df_vapour, ov, cv):
    df_liquid["volume_at_bp_liquid"] = df_liquid.ZL * gasConstant * ov.temperature / cv.pressure
    df_liquid["volume_at_bp_vapour"] = df_vapour.ZL * gasConstant * ov.temperature / cv.pressure
    df_liquid["aij_mix_liquid"] = df_liquid.A * gasConstant ** 2 * ov.temperature ** 2 / (cv.pressure * 10 ** 6)
    df_liquid["bij_mix_liquid"] = df_liquid.b_mix
    df_liquid["aij_mix_vapour"] = df_vapour.A * gasConstant ** 2 * ov.temperature ** 2 / (cv.pressure * 10 ** 6)
    df_liquid["bij_mix_vapour"] = df_vapour.b_mix
    df_liquid["change_in_pressure_liquid"] = (gasConstant * ov.temperature * (-1.) / (
            df_liquid.volume_at_bp_liquid - df_liquid.bij_mix_liquid * 10 ** 6) ** 2) - (-1) * (
                                                         df_liquid.aij_mix_liquid * 10 ** 6) * (
                                                     2 * df_liquid.volume_at_bp_liquid + 2 * df_liquid.bij_mix_liquid * 10 ** 6) / (
                                                     df_liquid.volume_at_bp_liquid ** 2 + 2 * df_liquid.bij_mix_liquid * 10 ** 6 * df_liquid.volume_at_bp_liquid + (
                                                 -1) * (df_liquid.bij_mix_liquid * 10 ** 6) ** 2) ** 2
    df_liquid["change_in_pressure_vapour"] = (gasConstant * ov.temperature * (-1.) / (
            df_liquid.volume_at_bp_vapour - df_liquid.bij_mix_vapour * 10 ** 6) ** 2) - (-1) * (
                                                         df_liquid.aij_mix_vapour * 10 ** 6) * (
                                                     2 * df_liquid.volume_at_bp_vapour + 2 * df_liquid.bij_mix_vapour * 10 ** 6) / (
                                                     df_liquid.volume_at_bp_vapour ** 2 + 2 * df_liquid.bij_mix_vapour * 10 ** 6 * df_liquid.volume_at_bp_vapour + (
                                                 -1) * (df_liquid.bij_mix_vapour * 10 ** 6) ** 2) ** 2
    return df_liquid, df_vapour


def calculate_cost_final(start_point, args):
    ov_bp, solver_vars_names = args
    cv_bp = calculate_bp(ov_bp, start_point, solver_vars_names)
    cv_bp, df_liquid, df_vapour = tune_yi(cv_bp, start_point, solver_vars_names)
    df_liquid, df_vapour = add_extra_columns(df_liquid, df_vapour, ov_bp, cv_bp)
    return df_liquid, df_vapour


def calculate_cost(start_point, args, only_cost=False):
    ov_bp, solver_vars_names = args
    cv_bp = calculate_bp(ov_bp, start_point, solver_vars_names)
    cv_bp, df_liquid, df_vapour = tune_yi(cv_bp, start_point, solver_vars_names)
    bp_cost = _cost(ov_bp, cv_bp)
    if only_cost:
        return bp_cost
    gradient = grad(ov_bp, cv_bp, start_point, df_liquid, df_vapour)
    return bp_cost, gradient


def calculate_bp(df_o, i_vars, var_names):
    df = df_o.copy(deep=True)
    kij_columns = [str(row.scn) if pd.isna(row.component) else str(row.component) for index, row in df.iterrows()]
    light_gas_count = df.loc[df.scn_no < 5].shape[0]

    # om_b multipliers
    for i in range(light_gas_count):
        df.loc[df["component"] == kij_columns[i], "om_b"] = \
            df.om_b * get_multiplier(i_vars, var_names, "om_b_multiplier_%s" % kij_columns[i])

    for group in scn_groups:
        min_scn, max_scn = group
        if min_scn > df.scn_no.max():
            break
        df.loc[((df.scn_no <= max_scn) & (df.scn_no >= min_scn)), "om_b"] = \
            df.om_b * get_multiplier(i_vars, var_names, "om_b_multiplier_scn_%s_%s" % (min_scn, max_scn))

    df["pressure"] = df.pressure * get_multiplier(i_vars, var_names, "pressure_multiplier")

    df["b"] = df.om_b * gasConstant * df.tc_k / (df.pc_mpa * 10 ** 6)
    df["ac_alpha"] = df.ac * df.alpha_eos
    return df


def calculate_sij(df, i_vars, var_names):
    kij_columns = [str(row.scn) if pd.isna(row.component) else str(row.component) for index, row in df.iterrows()]
    light_gas_count = df.loc[df.scn_no < 5].shape[0]

    sij = np.reshape(np.sqrt(np.kron(df.ac_alpha.values, df.ac_alpha.values)), (df.shape[0], df.shape[0]))
    sij = sij * df.mf.values[: None] * (1. - df[kij_columns].values)
    sij = sij.sum(axis=1)
    df["sij"] = sij

    # sij multipliers
    for i in range(light_gas_count):
        df.loc[df["component"] == kij_columns[i], "sij"] = \
            df.sij * get_multiplier(i_vars, var_names, "sij_multiplier_%s" % kij_columns[i])

    for group in scn_groups:
        min_scn, max_scn = group
        if min_scn > df.scn_no.max():
            break
        df.loc[((df.scn_no <= max_scn) & (df.scn_no >= min_scn)), "sij"] = \
            df.sij * get_multiplier(i_vars, var_names, "sij_multiplier_scn_%s_%s" % (min_scn, max_scn))
    return df


def get_zl_zv(df):
    row = df.head(1).squeeze()
    row["a1"] = (row.u * row.B - row.B - 1)
    row["a2"] = (row.A + row.w * row.B ** 2 - row.u * row.B - row.u * row.B ** 2)
    row["a3"] = -1. * (row.A * row.B + row.w * row.B ** 2 + row.w * row.B ** 3)
    row["Q"] = (3 * row.a2 - row.a1 ** 2) / 9
    row["L"] = (9 * row.a1 * row.a2 - 27 * row.a3 - 2 * row.a1 ** 3) / 54
    row["D"] = row.Q ** 3 + row.L ** 2
    row["S1"] = (-1. * (-1. * (row.L + np.sqrt(row.D))) ** (1. / 3.)) if (row.L + np.sqrt(row.D)) < 0 else (
                                                                                                                   row.L + np.sqrt(
                                                                                                               row.D)) ** (
                                                                                                                   1. / 3.)
    row["S2"] = (-1. * (-1. * (row.L - np.sqrt(row.D))) ** (1. / 3.)) if (row.L - np.sqrt(row.D)) < 0 else (
                                                                                                                   row.L - np.sqrt(
                                                                                                               row.D)) ** (
                                                                                                                   1. / 3.)
    row["Z1"] = 0 if row.Q >= 0 else 2. * np.sqrt(-row.Q) * np.cos(1. / 3. * (np.arccos(
        1. if np.abs(row.L / np.sqrt(-row.Q ** 3)) > 1 else row.L / np.sqrt(
            -row.Q ** 3))) + 2 * np.pi / 3.) - row.a1 / 3.
    row["Z2"] = 0 if row.Q >= 0 else 2. * np.sqrt(-row.Q) * np.cos(1. / 3. * (np.arccos(
        1. if np.abs(row.L / np.sqrt(-row.Q ** 3)) > 1 else row.L / np.sqrt(
            -row.Q ** 3))) + 4 * np.pi / 3.) - row.a1 / 3.
    row["Z3"] = 0 if row.Q >= 0 else 2. * np.sqrt(-row.Q) * np.cos(1. / 3. * (
        np.arccos(1. if np.abs(row.L / np.sqrt(-row.Q ** 3)) > 1 else row.L / np.sqrt(-row.Q ** 3)))) - row.a1 / 3.
    row["Z1_d"] = row.S1 + row.S2 - row.a1 / 3.
    row["Z1_dd"] = ((-1. * (-1. * row.L) ** 1. / 3.) if row.L < 0 else row.L ** (1. / 3.)) * 2. - row.a1 / 3.
    row["Z2_dd"] = (((-1. * (-1. * row.L) ** 1. / 3.) if row.L < 0 else row.L ** (1. / 3.)) + row.a1 / 3.) * -1
    row["Z3_dd"] = row.Z2_dd
    row["ZL"] = _min([row.Z1, row.Z2, row.Z3]) if row.D < 0 else (
        _min([row.Z1_dd, row.Z2_dd, row.Z3_dd]) if row.D == 0 else row.Z1_d)
    row["ZV"] = _max([row.Z1, row.Z2, row.Z3]) if row.D < 0 else (
        _max([row.Z1_dd, row.Z2_dd, row.Z3_dd]) if row.D == 0 else 0)
    return row.ZL, row.ZV


def _max(data):
    data = [x for x in data if x > 0]
    if len(data) > 0:
        return max(data)
    return 0


def _min(data):
    data = [x for x in data if x > 0]
    if len(data) > 0:
        return min(data)
    return 0


def calc_term3_fugacity(row, penalty, term):
    check1 = (row.ZL + row.B * row.r2)
    check2 = (row.ZL + row.B * row.r1)

    if check1 <= 0 or check2 <= 0 or check1 / check2 < 0:
        return penalty[2]
    else:
        return (((-1.) * ((2. * row.w * row.B + row.u * row.ZL) / (row.b_mix * term)) * (row.cj - 1.) + (
                row.A * row.u / (row.B * row.b_mix * term ** (3. / 2.))) * np.log(
            (row.ZL + row.B * row.r2) / (row.ZL + row.B * row.r1))) * row.my_term2)


def calc_term4_fugacity(row, penalty, term):
    check1 = (row.ZL + row.B * row.r2)
    check2 = (row.ZL + row.B * row.r1)

    if check1 <= 0 or check2 <= 0 or check1 / check2 < 0:
        return penalty[3]
    else:
        return ((1. / (row.b_mix * gasConstant * row.temperature * np.sqrt(term))) * (
            np.log((row.ZL + row.B * row.r2) / (row.ZL + row.B * row.r1))) * row.my_term3)


def calc_term5_fugacity(row, penalty, term):
    check1 = (row.ZL + row.B * row.r2)
    check2 = (row.ZL + row.B * row.r1)

    if check1 <= 0 or check2 <= 0 or check1 / check2 < 0:
        return penalty[4]
    else:
        return (((2 * row.ZL + row.u * row.B) / (row.b_mix ** 2 * term) * (row.cj - 1) - (2 *
                                                                                          row.A / (
                                                                                                  row.B * row.b_mix ** 2 * term ** (
                                                                                                  3. / 2.))) * (
                     np.log((row.ZL + row.B * row.r2) / (row.ZL + row.B * row.r1)))) * row.my_term4)


def fugacity(df):
    penalty = [10 ** 4, 1., 10 ** 5, 10 ** 5, 10 ** 5]
    df["Ri"] = df.sij * df.pressure * 10 ** 6 / (gasConstant ** 2 * df.temperature ** 2)
    df["aij_mix"] = df.Ri * df.mf
    df["A"] = df.aij_mix.sum()
    df["b_mix"] = (df.mf * df.b).sum()
    df["B"] = df.b_mix * df.pressure * 10 ** 6 / (gasConstant * df.temperature)
    df["ZL"], df["ZV"] = get_zl_zv(df)
    term = 8.
    df["r1"] = (df.u - np.sqrt(term)) / 2.
    df["r2"] = (df.u + np.sqrt(term)) / 2.
    df.loc[(df.ZL - df.B) < 0, "ZL"] = df.ZV
    df.loc[(df.ZL - df.B) >= 0, "ZL"] = df.ZL
    df["cj"] = 1. / (df.ZL - df.B)

    df["my_term1"] = df.b
    df["my_term2"] = df.b * df.u
    df["my_term3"] = 2. * df.sij
    df["my_term4"] = df.b * 2. * df.b_mix * df.w

    df["term1"] = df.apply(lambda row: penalty[0] if row.ZL - row.B <= 0 else -1. * np.log(row.ZL - row.B), axis=1)

    df["term2"] = (df.B / df.b_mix) * df.cj * df.my_term1

    df["term3"] = df.apply(lambda row: calc_term3_fugacity(row, penalty, term), axis=1)
    df["term4"] = df.apply(lambda row: calc_term4_fugacity(row, penalty, term), axis=1)
    df["term5"] = df.apply(lambda row: calc_term5_fugacity(row, penalty, term), axis=1)

    df["ln_phi"] = (df.term1 + df.term2 + df.term3 - df.term4 + df.term5)
    df["fugacity_factor_liq"] = np.exp(df.ln_phi)
    df["phl"] = df.fugacity_factor_liq
    return df


def calc_ki(row):
    if np.abs(row.fugacity_liquid) <= 10 ** -30 or np.abs(row.phl) <= 10 ** -30:
        return 0
    elif (np.abs(row.fugacity_liquid) <= 10 ** -9 or np.abs(
            row.phl) <= 10 ** -9) and row.fugacity_liquid / row.phl > 0.0005:
        return row.ki_poas
    else:
        return row.fugacity_liquid / row.phl


def calculate_yi(df, i_vars, var_names, liquid_vapour="liquid"):
    df = calculate_sij(df, i_vars, var_names)
    df = fugacity(df)
    if liquid_vapour == "liquid":
        df["ki_poas"] = (df.pc_mpa / df.pressure) * np.exp(5.37 * (1. + df.omega) * (1. - df.tc_k / df.temperature))
        df["YI"] = df.ki_poas * df.zi
    else:
        df["ki"] = df.apply(lambda row: calc_ki(row), axis=1)
        df["YI"] = df.ki * df.zi

    df["yi"] = df.YI / df.YI.sum()
    if liquid_vapour != "liquid":
        df["YI_diff"] = np.abs(df.YI - df.prev_YI)
    else:
        df["YI_diff"] = np.abs(df.YI - 0)
    df["prev_YI"] = df.YI
    df["mf"] = df.yi
    return df


def tune_yi(df, i_vars, var_names):
    # liquid_phase
    df["zi"] = df.mf
    df = calculate_yi(df, i_vars, var_names, "liquid")
    df["fugacity_liquid"] = df["phl"]
    df_liquid = df.copy(deep=True)
    # vapour_phase
    # max iterations 15
    iteration_counter = 1
    while df.YI_diff.sum() > 10 ** -6 and iteration_counter <= 15:
        df = calculate_yi(df, i_vars, var_names, "vapour")
        # logger.info("YI diff = %s. Iteration = %s" % (df.YI_diff.sum(), iteration_counter))
        iteration_counter = iteration_counter + 1
    df_vapour = df.copy(deep=True)
    return df, df_liquid, df_vapour


def _cost(ov, cv):
    pressure = ov.pressure.head(1).values[0]

    cv["fugacity_liquid"] = cv.zi * cv.pressure * cv.fugacity_liquid
    cv["fugacity_vapour"] = cv.YI * cv.pressure * cv.phl
    cv["bubble_point_factor"] = cv.apply(
        lambda row:
        0 if np.abs(row.phl) <= 10 ** -20 or np.abs(row.fugacity_liquid) <= 10 ** -30
        else row.fugacity_liquid / row.phl, axis=1)

    bp_obj = penalty[0] * ((pressure - cv.bubble_point_factor.sum()) / pressure) ** 2
    yi_obj = penalty[1] * (1. - cv.YI.sum()) ** 2

    return bp_obj + yi_obj
