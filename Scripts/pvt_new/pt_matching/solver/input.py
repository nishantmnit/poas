import re
import numpy as np

from Scripts.pvt_new.core.storage.mongo import fetch


def get_input(well_conn, kij, pressure, temperature):
    kij_df = fetch(well_conn, identifier=kij, pressure=pressure, temperature=temperature, property="pvt_output")
    kij_df = fix_mf(kij_df)
    kij_df = get_scn_no(kij_df)
    kij_df["pressure"] = pressure
    kij_df["temperature"] = temperature
    kij_df["u"] = 2.
    kij_df["w"] = -1.
    return kij_df


def fix_mf(df):
    total_mf = df.mf.sum()
    difference = 1. - total_mf
    adjustment = difference / df.loc[df.mf > 10 ** -20].shape[0]
    df.loc[df.mf > 10 ** -20, "mf"] = df.loc[df.mf > 10 ** -20, "mf"] + adjustment
    return df


def get_scn_no(df):
    df["scn_no"] = df.scn.str.extract('(^c\d+)', expand=True, flags=re.IGNORECASE)
    df["scn_no"] = df.scn_no.str.extract('(\d+)', expand=True, flags=re.IGNORECASE)
    df["scn_no"] = df.scn_no.astype(float)
    df["scn_no"] = df["scn_no"].replace(np.nan, 0)
    return df
