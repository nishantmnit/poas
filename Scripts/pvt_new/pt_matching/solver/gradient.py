import numpy as np
import pandas as pd
from Scripts.pvt_new.core.constants import gasConstant
from variables import scn_groups
from common import penalty


def grad_om_b(df, grad_df):
    grad_df["om_b"] = df.om_b
    grad_df["b"] = df.om_b * gasConstant * df.tc_k / (df.pc_mpa * 10 ** 6)
    grad_df["b_mf"] = grad_df.b * df.mf
    return grad_df


def grad_sij(df, i_vars, grad_df, sij):
    kij_columns = [str(row.scn) if pd.isna(row.component) else str(row.component) for index, row in df.iterrows()]
    light_gas_count = df.loc[df.scn_no < 5].shape[0]

    var_number = 0

    for i in range(len(i_vars)):
        if i <= light_gas_count:
            grad_df.loc[grad_df["component"] == kij_columns[i], "sij_wrt_var%s" % (i + 1)] = sij.sij

    for i in range(len(scn_groups)):
        group = scn_groups[i]
        var_number = light_gas_count + i
        min_scn, max_scn = group
        if min_scn > df.scn_no.max():
            break
        grad_df.loc[
            (grad_df.scn_no <= max_scn) & (grad_df.scn_no >= min_scn), "sij_wrt_var%s" % (var_number + 1)] = sij.sij

    for i in range(var_number + 1, len(i_vars)):
        grad_df["sij_wrt_var%s" % (i + 1)] = 0

    for i in range(len(i_vars)):
        grad_df["sij_wrt_var%s" % (i + 1)] = grad_df["sij_wrt_var%s" % (i + 1)].fillna(0)

    return grad_df


def grad_ri(ov, cv, df_liquid_vapour, i_vars, grad_df):
    for i in range(len(i_vars)):
        grad_df["ri_wrt_var%s" % (i + 1)] = grad_df["sij_wrt_var%s" % (i + 1)] * cv.pressure * 10 ** 6 / (
                gasConstant ** 2 * ov.temperature ** 2)
    grad_df["ri_wrt_var%s" % (len(i_vars))] = df_liquid_vapour.sij * ov.pressure * 10 ** 6 / (
            gasConstant ** 2 * ov.temperature ** 2)
    return grad_df


def grad_A(df, i_vars, grad_df):
    for i in range(len(i_vars)):
        grad_df["A_wrt_var%s" % (i + 1)] = (df.mf * grad_df["ri_wrt_var%s" % (i + 1)]).sum()
    return grad_df


def grad_b_mix(df, i_vars, grad_df):
    kij_columns = [str(row.scn) if pd.isna(row.component) else str(row.component) for index, row in df.iterrows()]
    light_gas_count = df.loc[df.scn_no < 5].shape[0]

    om_b_multiplier_start_point = light_gas_count + len(scn_groups)
    # sij Multipliers
    for i in range(1, om_b_multiplier_start_point + 1):
        grad_df["my_term1_wrt_var%s" % i] = 0

    # om_b multipliers
    var_number = om_b_multiplier_start_point
    for i in range(light_gas_count):
        var_number = var_number + 1
        grad_df.loc[grad_df["component"] == kij_columns[i], "my_term1_wrt_var%s" % var_number] = grad_df.b

    for i in range(len(scn_groups)):
        group = scn_groups[i]
        min_scn, max_scn = group
        if min_scn > df.scn_no.max():
            break
        var_number = var_number + 1
        grad_df.loc[
            (grad_df.scn_no <= max_scn) & (grad_df.scn_no >= min_scn),
            "my_term1_wrt_var%s" % var_number] = grad_df["b"]

    for i in range(var_number, len(i_vars)):
        grad_df["my_term1_wrt_var%s" % (i + 1)] = 0

    for i in range(len(i_vars)):
        grad_df["my_term1_wrt_var%s" % (i + 1)] = grad_df["my_term1_wrt_var%s" % (i + 1)].fillna(0)
        grad_df["b_mix_wrt_var%s" % (i + 1)] = (grad_df["my_term1_wrt_var%s" % (i + 1)] * df.mf).sum()
    return grad_df


def grad_B(df, cv, i_vars, grad_df, df_liquid_vapour):
    for i in range(len(i_vars)):
        grad_df["B_wrt_var%s" % (i + 1)] = grad_df["b_mix_wrt_var%s" % (i + 1)] * cv.pressure * 10 ** 6 / (
                gasConstant * df.temperature)
    grad_df["B_wrt_var%s" % (len(i_vars))] = df_liquid_vapour.b_mix * df.pressure * 10 ** 6 / (
            gasConstant * df.temperature)
    return grad_df


def grad_ZL_ZV(df_liquid_vapour, i_vars, grad_df):
    for i in range(len(i_vars)):
        grad_df["u_mix_wrt_var%s" % (i + 1)] = 0
        grad_df["w_mix_wrt_var%s" % (i + 1)] = 0
        grad_df["zl_wrt_var%s" % (i + 1)] = (((grad_df["A_wrt_var%s" % (i + 1)] * df_liquid_vapour.B + df_liquid_vapour[
            "A"] * grad_df["B_wrt_var%s" % (i + 1)]) + (grad_df["w_mix_wrt_var%s" % (
                i + 1)] * df_liquid_vapour.B ** 2 + -1. * 2 * df_liquid_vapour.B * grad_df[
                                                            "B_wrt_var%s" % (i + 1)]) + (
                                                      grad_df["w_mix_wrt_var%s" % (
                                                              i + 1)] * df_liquid_vapour.B ** 3 + -1. * 3 * df_liquid_vapour.B ** 2 *
                                                      grad_df["B_wrt_var%s" % (i + 1)])) -
                                             df_liquid_vapour["ZL"] * (grad_df["A_wrt_var%s" % (i + 1)] + (
                        grad_df["w_mix_wrt_var%s" % (i + 1)] * df_liquid_vapour.B ** 2 + -1. * 2 * df_liquid_vapour.B *
                        grad_df[
                            "B_wrt_var%s" % (i + 1)]) - (
                                                                               grad_df["u_mix_wrt_var%s" % (
                                                                                       i + 1)] * df_liquid_vapour.B + 2. *
                                                                               grad_df[
                                                                                   "B_wrt_var%s" % (i + 1)]) - (
                                                                               grad_df["u_mix_wrt_var%s" % (
                                                                                       i + 1)] * df_liquid_vapour.B ** 2 + 2. * 2 * df_liquid_vapour.B *
                                                                               grad_df["B_wrt_var%s" % (i + 1)])) -
                                             df_liquid_vapour["ZL"] ** 2 * ((grad_df["u_mix_wrt_var%s" % (
                        i + 1)] * df_liquid_vapour.B + 2. * grad_df["B_wrt_var%s" % (i + 1)]) - grad_df[
                                                                                "B_wrt_var%s" % (i + 1)])) / (
                                                    3 * df_liquid_vapour["ZL"] ** 2 + 2 * df_liquid_vapour["ZL"] * (
                                                    2. * df_liquid_vapour.B - df_liquid_vapour.B - 1) + (
                                                            df_liquid_vapour["A"] + -1. * df_liquid_vapour.B ** 2 - 2. *
                                                            df_liquid_vapour.B - 2. * df_liquid_vapour.B ** 2))
        if df_liquid_vapour.ZV.values[0] == 0:
            grad_df["zv_wrt_var%s" % (i + 1)] = 0
        else:
            grad_df["zv_wrt_var%s" % (i + 1)] = grad_df["zl_wrt_var%s" % (i + 1)]
    return grad_df


def grad_cj(cv, df_liquid_vapour, i_vars, grad_df):
    for i in range(len(i_vars)):
        grad_df["cj_wrt_var%s" % (i + 1)] = (-1.) / (df_liquid_vapour.ZL - df_liquid_vapour.B) ** 2 * (
                grad_df["zl_wrt_var%s" % (i + 1)] - grad_df["B_wrt_var%s" % (i + 1)])
    return grad_df


def grad_my_terms(cv, df_liquid_vapour, i_vars, grad_df):
    for i in range(len(i_vars)):
        grad_df["my_term2_wrt_var%s" % (i + 1)] = 2. * grad_df["my_term1_wrt_var%s" % (i + 1)]
        grad_df["my_term3_wrt_var%s" % (i + 1)] = 2. * grad_df["sij_wrt_var%s" % (i + 1)]
        grad_df["my_term4_wrt_var%s" % (i + 1)] = 2. * -1. * (
                df_liquid_vapour.b_mix * grad_df["my_term1_wrt_var%s" % (i + 1)] + grad_df[
            "b_mix_wrt_var%s" % (i + 1)] * cv.b)
    return grad_df


def grad_term1(cv, df_liquid_vapour, i_vars, grad_df):
    check = (df_liquid_vapour.ZL - df_liquid_vapour.B).values[0]
    for i in range(len(i_vars)):
        if check <= 0:
            grad_df["term1_wrt_var%s" % (i + 1)] = 0
        else:
            grad_df["term1_wrt_var%s" % (i + 1)] = (
                    -1. * 1. / check * (grad_df["zl_wrt_var%s" % (i + 1)] - grad_df["B_wrt_var%s" % (i + 1)]))
    return grad_df


def grad_term2(df_liquid_vapour, i_vars, grad_df):
    for i in range(len(i_vars)):
        grad_df["term2_wrt_var%s" % (i + 1)] = df_liquid_vapour.term2 * (
                (1. / df_liquid_vapour.B) * grad_df["B_wrt_var%s" % (i + 1)] - (1. / df_liquid_vapour.b_mix) * grad_df[
            "b_mix_wrt_var%s" % (i + 1)] + (1. / df_liquid_vapour.cj) * grad_df["cj_wrt_var%s" % (i + 1)] + (
                        1. / df_liquid_vapour.my_term1) * grad_df["my_term1_wrt_var%s" % (i + 1)])
    return grad_df


def grad_term3(df_liquid_vapour, i_vars, grad_df):
    check1 = (df_liquid_vapour.ZL + df_liquid_vapour.B * df_liquid_vapour.r2).values[0]
    check2 = (df_liquid_vapour.ZL + df_liquid_vapour.B * df_liquid_vapour.r1).values[0]

    for i in range(len(i_vars)):
        if check1 <= 0 or check2 <= 0 or check1 / check2 < 0:
            grad_df["term3_wrt_var%s" % (i + 1)] = 0
        else:
            grad_df["term3_wrt_var%s" % (i + 1)] = (((-1.) * (
                    ((2 * -1. * df_liquid_vapour.B + 2. * df_liquid_vapour.ZL) / (df_liquid_vapour.b_mix * 8.)) * (
                    df_liquid_vapour.cj - 1.) * (
                            (2 * (0 * df_liquid_vapour.B + -1. * grad_df["B_wrt_var%s" % (i + 1)]) + (
                                    0 * df_liquid_vapour.ZL + 2. * grad_df["zl_wrt_var%s" % (i + 1)])) / (
                                    2 * -1. * df_liquid_vapour.B + 2. * df_liquid_vapour.ZL) - 1. / (
                                    df_liquid_vapour.b_mix * 8.) * (
                                    grad_df["b_mix_wrt_var%s" % (i + 1)] * 8. + df_liquid_vapour.b_mix * 0) + 1 / (
                                    df_liquid_vapour.cj - 1.) * (grad_df["cj_wrt_var%s" % (i + 1)]))) + ((
                                                                                                                 df_liquid_vapour.A * 2. / (
                                                                                                                 df_liquid_vapour.B * df_liquid_vapour.b_mix * 8. ** (
                                                                                                                 3. / 2.))) * (
                                                                                                                 1 / df_liquid_vapour.A *
                                                                                                                 grad_df[
                                                                                                                     "A_wrt_var%s" % (
                                                                                                                             i + 1)] + 1 / 2. * 0 - (
                                                                                                                         1 / df_liquid_vapour.B *
                                                                                                                         grad_df[
                                                                                                                             "B_wrt_var%s" % (
                                                                                                                                     i + 1)] + 1 / df_liquid_vapour.b_mix *
                                                                                                                         grad_df[
                                                                                                                             "b_mix_wrt_var%s" % (
                                                                                                                                     i + 1)] + 3. / 2. * 0 * 1 / 8.))) * np.log(
                (df_liquid_vapour.ZL + df_liquid_vapour.B * df_liquid_vapour.r2) / (
                        df_liquid_vapour.ZL + df_liquid_vapour.B * df_liquid_vapour.r1)) + (
                                                             df_liquid_vapour.A * 2. / (
                                                             df_liquid_vapour.B * df_liquid_vapour.b_mix * 8. ** (
                                                             3. / 2.))) * (
                                                             1 / (
                                                             df_liquid_vapour.ZL + df_liquid_vapour.B * df_liquid_vapour.r2) * (
                                                                     grad_df["zl_wrt_var%s" % (i + 1)] + (
                                                                     grad_df["B_wrt_var%s" % (
                                                                             i + 1)] * df_liquid_vapour.r2 + df_liquid_vapour.B * 0)) - 1. / (
                                                                     df_liquid_vapour.ZL + df_liquid_vapour.B * df_liquid_vapour.r1) * (
                                                                     grad_df["zl_wrt_var%s" % (i + 1)] + (
                                                                     grad_df["B_wrt_var%s" % (
                                                                             i + 1)] * df_liquid_vapour.r1 + df_liquid_vapour.B * 0)))) * df_liquid_vapour.my_term2 + (
                                                            df_liquid_vapour.term3 / df_liquid_vapour.my_term2) *
                                                    grad_df[
                                                        "my_term2_wrt_var%s" % (i + 1)])
    return grad_df


def grad_term4(df_liquid_vapour, i_vars, grad_df):
    check1 = (df_liquid_vapour.ZL + df_liquid_vapour.B * df_liquid_vapour.r2).values[0]
    check2 = (df_liquid_vapour.ZL + df_liquid_vapour.B * df_liquid_vapour.r1).values[0]

    for i in range(len(i_vars)):
        if check1 <= 0 or check2 <= 0 or check1 / check2 < 0:
            grad_df["term4_wrt_var%s" % (i + 1)] = 0
        else:
            grad_df["term4_wrt_var%s" % (i + 1)] = (((1 / (
                    df_liquid_vapour.b_mix * gasConstant * df_liquid_vapour.temperature * np.sqrt(8.))) * (-1. * (
                    1 / df_liquid_vapour.b_mix * grad_df["b_mix_wrt_var%s" % (i + 1)] + 0.5 / 8. * 0)) * (np.log(
                (df_liquid_vapour.ZL + df_liquid_vapour.B * df_liquid_vapour.r2) / (
                        df_liquid_vapour.ZL + df_liquid_vapour.B * df_liquid_vapour.r1))) + (1 / (
                    df_liquid_vapour.b_mix * gasConstant * df_liquid_vapour.temperature * np.sqrt(8.))) * (
                                                             1 / (
                                                             df_liquid_vapour.ZL + df_liquid_vapour.B * df_liquid_vapour.r2) * (
                                                                     grad_df["zl_wrt_var%s" % (i + 1)] + (
                                                                     grad_df["B_wrt_var%s" % (
                                                                             i + 1)] * df_liquid_vapour.r2 + df_liquid_vapour.B * 0)) - 1. / (
                                                                     df_liquid_vapour.ZL + df_liquid_vapour.B * df_liquid_vapour.r1) * (
                                                                     grad_df["zl_wrt_var%s" % (i + 1)] + (
                                                                     grad_df["B_wrt_var%s" % (
                                                                             i + 1)] * df_liquid_vapour.r1 + df_liquid_vapour.B * 0)))) * df_liquid_vapour.my_term3 + (
                                                            df_liquid_vapour.term4 / df_liquid_vapour.my_term3) *
                                                    grad_df[
                                                        "my_term3_wrt_var%s" % (i + 1)])
    return grad_df


def grad_term5(df_liquid_vapour, i_vars, grad_df):
    check1 = (df_liquid_vapour.ZL + df_liquid_vapour.B * df_liquid_vapour.r2).values[0]
    check2 = (df_liquid_vapour.ZL + df_liquid_vapour.B * df_liquid_vapour.r1).values[0]

    for i in range(len(i_vars)):
        if check1 <= 0 or check2 <= 0 or check1 / check2 < 0:
            grad_df["term5_wrt_var%s" % (i + 1)] = 0
        else:
            grad_df["term5_wrt_var%s" % (i + 1)] = (((2 * df_liquid_vapour.ZL + 2. * df_liquid_vapour.B) / (
                    df_liquid_vapour.b_mix ** 2 * 8.) * (df_liquid_vapour.cj - 1.) * (
                                                             1 / (2 * df_liquid_vapour.ZL + 2. * df_liquid_vapour.B) * (
                                                             2 * grad_df[
                                                         "zl_wrt_var%s" % (i + 1)] + 0 * df_liquid_vapour.B + 2. *
                                                             grad_df[
                                                                 "B_wrt_var%s" % (i + 1)]) - 1. / (
                                                                     df_liquid_vapour.b_mix ** 2 * 8.) * (
                                                                     2 * df_liquid_vapour.b_mix * grad_df[
                                                                 "b_mix_wrt_var%s" % (
                                                                         i + 1)] * 8. + df_liquid_vapour.b_mix ** 2 * 0) + 1 / (
                                                                     df_liquid_vapour.cj - 1.) * grad_df[
                                                                 "cj_wrt_var%s" % (i + 1)]) - ((
                                                                                                       2 * df_liquid_vapour.A / (
                                                                                                       df_liquid_vapour.B * df_liquid_vapour.b_mix ** 2 * 8. ** (
                                                                                                       3. / 2.))) * (
                                                                                                       1 / df_liquid_vapour.A *
                                                                                                       grad_df[
                                                                                                           "A_wrt_var%s" % (
                                                                                                                   i + 1)] - (
                                                                                                               1 / df_liquid_vapour.B *
                                                                                                               grad_df[
                                                                                                                   "B_wrt_var%s" % (
                                                                                                                           i + 1)] + 2 / df_liquid_vapour.b_mix *
                                                                                                               grad_df[
                                                                                                                   "b_mix_wrt_var%s" % (
                                                                                                                           i + 1)] + 3. / 2. * (
                                                                                                                       1 / 8.) * 0)) * (
                                                                                                   np.log((
                                                                                                                  df_liquid_vapour.ZL + df_liquid_vapour.B * df_liquid_vapour.r2) / (
                                                                                                                  df_liquid_vapour.ZL + df_liquid_vapour.B * df_liquid_vapour.r1))) + (
                                                                                                       2 * df_liquid_vapour.A / (
                                                                                                       df_liquid_vapour.B * df_liquid_vapour.b_mix ** 2 * 8. ** (
                                                                                                       3. / 2.))) * (
                                                                                                       1 / (
                                                                                                       df_liquid_vapour.ZL + df_liquid_vapour.B * df_liquid_vapour.r2) * (
                                                                                                               grad_df[
                                                                                                                   "zl_wrt_var%s" % (
                                                                                                                           i + 1)] + (
                                                                                                                       grad_df[
                                                                                                                           "B_wrt_var%s" % (
                                                                                                                                   i + 1)] * df_liquid_vapour.r2 + df_liquid_vapour.B * 0)) - 1. / (
                                                                                                               df_liquid_vapour.ZL + df_liquid_vapour.B * df_liquid_vapour.r1) * (
                                                                                                               grad_df[
                                                                                                                   "zl_wrt_var%s" % (
                                                                                                                           i + 1)] + (
                                                                                                                       grad_df[
                                                                                                                           "B_wrt_var%s" % (
                                                                                                                                   i + 1)] * df_liquid_vapour.r1 + df_liquid_vapour.B * 0))))) * df_liquid_vapour.my_term4 + (
                                                            df_liquid_vapour.term5 / df_liquid_vapour.my_term4) *
                                                    grad_df[
                                                        "my_term4_wrt_var%s" % (i + 1)])
    return grad_df


def grad_fugacity(df_liquid_vapour, i_vars, grad_df):
    for i in range(len(i_vars)):
        grad_df["ln_phi_wrt_var%s" % (i + 1)] = (
                grad_df["term1_wrt_var%s" % (i + 1)] + grad_df["term2_wrt_var%s" % (i + 1)] + grad_df[
            "term3_wrt_var%s" % (i + 1)] - grad_df["term4_wrt_var%s" % (i + 1)] + grad_df[
                    "term5_wrt_var%s" % (i + 1)])
        grad_df["fugacity_factor_wrt_var%s" % (i + 1)] = np.exp(df_liquid_vapour.ln_phi) * grad_df[
            "ln_phi_wrt_var%s" % (i + 1)]
    return grad_df


def grad_liquid_vapour(ov, cv, i_vars, df_liquid_vapour, grad_df):
    grad_df = grad_om_b(ov, grad_df)
    grad_df = grad_sij(ov, i_vars, grad_df, df_liquid_vapour)
    grad_df = grad_ri(ov, cv, df_liquid_vapour, i_vars, grad_df)
    grad_df = grad_A(ov, i_vars, grad_df)
    grad_df = grad_b_mix(ov, i_vars, grad_df)
    grad_df = grad_B(ov, cv, i_vars, grad_df, df_liquid_vapour)
    grad_df = grad_ZL_ZV(df_liquid_vapour, i_vars, grad_df)
    grad_df = grad_cj(cv, df_liquid_vapour, i_vars, grad_df)
    grad_df = grad_my_terms(cv, df_liquid_vapour, i_vars, grad_df)
    grad_df = grad_term1(cv, df_liquid_vapour, i_vars, grad_df)
    grad_df = grad_term2(df_liquid_vapour, i_vars, grad_df)
    grad_df = grad_term3(df_liquid_vapour, i_vars, grad_df)
    grad_df = grad_term4(df_liquid_vapour, i_vars, grad_df)
    grad_df = grad_term5(df_liquid_vapour, i_vars, grad_df)
    grad_df = grad_fugacity(df_liquid_vapour, i_vars, grad_df)
    return grad_df


def grad_ki(ov, df_liquid, df_vapour, i_vars, grad_df_liquid, grad_df_vapour):
    grad_df_vapour["phl_liq"] = df_liquid.phl
    grad_df_vapour["phl_vap"] = df_vapour.phl
    grad_df_vapour["phl_liq_by_vap"] = df_liquid.phl / df_vapour.phl
    for i in range(len(i_vars)):
        grad_df_vapour.loc[
            ((np.abs(grad_df_vapour.phl_liq) <= 10 ** -30) | (np.abs(grad_df_vapour.phl_vap <= 10 ** -30))),
            "ki_wrt_var%s" % (i + 1)] = 0
        if (i + 1) == len(i_vars):
            grad_df_vapour.loc[(((grad_df_vapour.phl_liq <= 10 ** -9) | (grad_df_vapour.phl_vap <= 10 ** -4)) & (
                    grad_df_vapour.phl_liq_by_vap > 0.0005)), "ki_wrt_var%s" % (i + 1)] = \
                ((df_liquid.pc_mpa / ov.pressure) * (-1. / i_vars[i] ** 2) * np.exp(
                    5.37 * (1. + df_liquid.omega) *
                    (1. - df_liquid.tc_k /
                     df_liquid.temperature)))
        else:
            grad_df_vapour.loc[(((grad_df_vapour.phl_liq <= 10 ** -9) | (grad_df_vapour.phl_vap <= 10 ** -4)) & (
                    grad_df_vapour.phl_liq_by_vap > 0.0005)), "ki_wrt_var%s" % (i + 1)] = 0

        grad_df_vapour.loc[grad_df_vapour["ki_wrt_var%s" % (i + 1)].isnull(), "ki_wrt_var%s" % (i + 1)] = \
            (grad_df_liquid["fugacity_factor_wrt_var%s" % (i + 1)] * grad_df_vapour.phl_vap -
             grad_df_vapour.phl_liq * grad_df_vapour["fugacity_factor_wrt_var%s" % (i + 1)]) / \
            grad_df_vapour.phl_vap ** 2

    return grad_df_vapour


def grad_YI(grad_df, ov, i_vars):
    for i in range(len(i_vars)):
        grad_df["YI_wrt_var%s" % (i + 1)] = (ov.mf * grad_df["ki_wrt_var%s" % (i + 1)]).sum()
    return grad_df


def grad_fugacity_liq(grad_df_liquid, grad_df, i_vars, ov, cv):
    for i in range(len(i_vars)):
        grad_df["fugacity_liq_wrt_var%s" % (i + 1)] = ov.mf * cv.pressure * grad_df_liquid[
            "fugacity_factor_wrt_var%s" % (i + 1)]
    grad_df["fugacity_liq_wrt_var%s" % (len(i_vars))] = ov.mf * (cv.pressure * grad_df_liquid[
        "fugacity_factor_wrt_var%s" % (len(i_vars))] + ov.pressure * grad_df.phl_liq)
    return grad_df


def grad_bubble_point(grad_df, grad_df_vapour, i_vars, ov, cv):
    for i in range(len(i_vars)):
        grad_df.loc[
            ((np.abs(grad_df.phl_liq) <= 10 ** -30) | (np.abs(grad_df.phl_vap) <= 10 ** -30)),
            "bp_wrt_var%s" % (i + 1)] = 0
        grad_df.loc[grad_df["bp_wrt_var%s" % (i + 1)] != 0, "bp_wrt_var%s" % (i + 1)] = ((grad_df.phl_vap * grad_df[
            "fugacity_liq_wrt_var%s" % (i + 1)] - grad_df_vapour["fugacity_factor_wrt_var%s" % (
                i + 1)] * grad_df.phl_liq * ov.mf * cv.pressure) / grad_df.phl_vap ** 2).sum()

    return grad_df


def grad_obj1(ov, cv, grad_df, i_vars):
    for i in range(len(i_vars)):
        grad_df["obj1_wrt_var%s" % (i + 1)] = penalty[0] * ((-1.) * 2. * (grad_df["bp_wrt_var%s" % (i + 1)]) * (
                cv.pressure - cv.bubble_point_factor.sum()) / cv.pressure ** 2)
    grad_df["obj1_wrt_var%s" % (len(i_vars))] = penalty[0] * (2. * (
            (cv.pressure - cv.bubble_point_factor.sum()) / cv.pressure) *
            ((cv.pressure * (ov.pressure - grad_df["bp_wrt_var%s" % (len(i_vars))]) -
              (cv.pressure - cv.bubble_point_factor.sum()) * ov.pressure) / cv.pressure ** 2)
    )
    return grad_df


def grad_obj2(cv, grad_df, i_vars):
    for i in range(len(i_vars)):
        grad_df["obj2_wrt_var%s" % (i + 1)] = penalty[1] * 2. * (-1.) * (1. - cv.YI.sum()) * (
            grad_df["YI_wrt_var%s" % (i + 1)])
    return grad_df


def grad(ov, cv, i_vars, df_liquid, df_vapour):
    grad_df = pd.DataFrame(index=ov.index)
    grad_df[["component", "scn_no"]] = ov[["component", "scn_no"]]
    grad_df = grad_liquid_vapour(ov, cv, i_vars, df_liquid, grad_df)

    grad_df_liquid = grad_df.copy(deep=True)
    df = ov.copy(deep=True)
    df["mf"] = df_vapour["mf"]
    grad_df_vapour = grad_liquid_vapour(df, cv, i_vars, df_vapour, grad_df)

    grad_df = grad_ki(ov, df_liquid, df_vapour, i_vars, grad_df_liquid, grad_df_vapour)
    grad_df = grad_YI(grad_df, ov, i_vars)
    grad_df = grad_fugacity_liq(grad_df_liquid, grad_df, i_vars, ov, cv)
    grad_df = grad_bubble_point(grad_df, grad_df_vapour, i_vars, ov, cv)

    grad_df = grad_obj1(ov, cv, grad_df, i_vars)
    grad_df = grad_obj2(cv, grad_df, i_vars)

    final_gradients = []
    for i in range(len(i_vars)):
        final_gradients.append(
            grad_df["obj1_wrt_var%s" % (i + 1)].values[0] + grad_df["obj2_wrt_var%s" % (i + 1)].values[0])
    return np.array(final_gradients)
