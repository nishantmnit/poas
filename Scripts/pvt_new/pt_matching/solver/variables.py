import pandas as pd

scn_groups = [
                [5, 8], [9, 13], [14, 18], [19, 23], [24, 30], [31, 35], [36, 40], [41, 45], [46, 50],
                [51, 60], [61, 75], [76, 100], [101, 121]
            ]


def define_vars(df):
    solver_vars_names, start_point, solver_vars_range = [], [], []

    light_gas_count = df.loc[df.scn_no < 5].shape[0]
    kij_columns = [str(row.scn) if pd.isna(row.component) else str(row.component) for index, row in df.iterrows()]

    # sij multipliers
    for i in range(light_gas_count):
        solver_vars_names.append("sij_multiplier_%s" % kij_columns[i])
        solver_vars_range = solver_vars_range + [[0.1, 5.]]
        start_point.append(1.)

    for group in scn_groups:
        min_scn, max_scn = group
        if min_scn > df.scn_no.max():
            break
        solver_vars_names.append("sij_multiplier_scn_%s_%s" % (min_scn, max_scn))
        solver_vars_range = solver_vars_range + [[0.1, 5.]]
        start_point.append(1.)

    # om_b multipliers
    for i in range(light_gas_count):
        solver_vars_names.append("om_b_multiplier_%s" % kij_columns[i])
        solver_vars_range = solver_vars_range + [[1., 1.]]
        start_point.append(1.)

    for group in scn_groups:
        min_scn, max_scn = group
        if min_scn > df.scn_no.max():
            break
        solver_vars_names.append("om_b_multiplier_scn_%s_%s" % (min_scn, max_scn))
        solver_vars_range = solver_vars_range + [[1., 1.]]
        start_point.append(1.)

    # bubble point pressure multipliers
    pressure = df.pressure.values[0]
    solver_vars_names.append("pressure_multiplier")
    solver_vars_range = solver_vars_range + [[0.99999, 1.0001]]
    start_point.append(1.)

    return solver_vars_names, solver_vars_range, start_point
