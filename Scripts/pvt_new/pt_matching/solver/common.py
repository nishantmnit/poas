def pressure_user_provided(pt_collection, df):
    pressure = df.pressure.head(1).values[0]
    temperature = df.temperature.head(1).values[0]

    logic = False
    if pt_collection.loc[((pt_collection.bubble_point == pressure)
                          & (pt_collection.temperature == temperature)
                          & (pt_collection.user_provided == 1))].shape[0] >= 1.:
        logic = True
    return logic


def get_multiplier(variables, var_names, var_name):
    return variables[var_names.index(var_name)]


penalty = [5., 25.]
