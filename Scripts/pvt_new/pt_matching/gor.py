import pandas as pd

from Scripts.pvt_new.core.get_logger import get_logger
from Scripts.pvt_new.core.storage.mongo import fetch
from bubble_point import get_bubble_point

logger = get_logger("pvt gor finder")


def gor_from_general_data(well_conn):
    gd = fetch(well_conn, property="pvt_output", identifier="validated_general_data")
    if gd.loc[(gd.param == "gor_pb") & ~(gd.value.isnull()) | (gd.value == 0)].value.count() > 0:
        temperature = gd.loc[gd.param == "temp_res", "value"].values[0]
        bubble_point = get_bubble_point(well_conn, temperature)
        gor = gd.loc[gd.param == "gor_pb", "value"].values[0]
        return [temperature, bubble_point, gor]
    return []


def gpr_from_dv_cvd(well_conn):
    separator = fetch(well_conn, property="seperator_used_sep_corrected")
    cvd = fetch(well_conn, property="cvd_oil")
    dv = fetch(well_conn, property="dv_oil")
    ref_temperatures = list(cvd.temperature.unique()) + list(dv.temperature.unique())
    gors = []
    for ref_temperature in ref_temperatures:
        sep_sub = separator.loc[
            (separator.temperature == ref_temperature)
            & (separator.stage == "Total at Bubble Point")
            & ~((separator.gor.isnull()) | (separator.gor == 0))
            ]
        sep_sub_1 = separator.loc[
            (separator.temperature == ref_temperature)
            & ~((separator.gor.isnull()) | (separator.gor == 0))
            ]
        if sep_sub.gor.count() > 0:
            gors.append([ref_temperature, get_bubble_point(well_conn, ref_temperature), sep_sub.gor.values[0]])
        elif sep_sub_1.gor.count() > 0:
            gor = sep_sub_1.gor.sum()
            gors.append([ref_temperature, get_bubble_point(well_conn, ref_temperature), gor])
    return gors


def get_gor(well_conn):
    gd = gor_from_general_data(well_conn)
    dv_cvd = gpr_from_dv_cvd(well_conn)
    return gd + dv_cvd


if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection

    db = get_connection("AB8")
    test_user_args = pd.Series({"debug": False, "well_conn": db})
    print get_gor(db)
