import importlib

import pandas as pd

from Scripts.pvt_new.core.constants import valid_eos
from Scripts.pvt_new.core.get_logger import get_logger
from Scripts.pvt_new.core.storage.save import save
from Scripts.pvt_new.pt_matching.kij.eij import eij, eij_input
from Scripts.pvt_new.pt_matching.kij.input.diff_alpha_eos import diff_alpha_eos
from pt_collection import run as pt_collection

logger = get_logger("kij")

kij_methods = ["kij_a", "kij_b", "kij_c"]


def pt_list(user_args):
    well_conn = user_args.well_conn
    pt = pt_collection(well_conn)
    save(pt, user_args, identifier="pt_collection")
    bp = pt[["temperature", "bubble_point"]]
    bp.rename(columns={"bubble_point": "pressure"}, inplace=True)
    dp = pt[["temperature", "due_point"]]
    dp.rename(columns={"due_point": "pressure"}, inplace=True)
    pt = pd.concat([bp, dp], ignore_index=True)
    pt = pt.drop_duplicates()
    pt.sort_values(by=["temperature"], inplace=True)
    pt.reset_index(drop=True, inplace=True)
    return pt


def run_kij_a(pt, eos, user_args):
    previous_temperature, eij_output = None, None
    kij_module = importlib.import_module("Scripts.pvt_new.pt_matching.kij.kij_a")
    for index, row in pt.iterrows():
        temperature, pressure = row.temperature, row.pressure
        temperature_changed = False if previous_temperature == temperature else True
        previous_temperature = temperature
        logger.info("Calculating Kij-A for temperature == %s | pressure == %s" % (temperature, pressure))
        if temperature_changed:
            logger.info("calculating eij")
            eij_input_df = eij_input(db)
            eij_output = eij(eij_input_df, temperature)

        kij_input_df = getattr(kij_module, "kij_input")(db, eos, temperature, pressure)
        kij_output = getattr(kij_module, "kij")(kij_input_df, temperature, eij_output)
        save(kij_output, user_args, identifier="kij-a", eos=eos, temperature=temperature, pressure=pressure)


def run_kij_b(pt, eos, user_args):
    previous_temperature, eij_output = None, None
    kij_module = importlib.import_module("Scripts.pvt_new.pt_matching.kij.kij_b")
    for index, row in pt.iterrows():
        temperature, pressure = row.temperature, row.pressure
        temperature_changed = False if previous_temperature == temperature else True
        previous_temperature = temperature
        logger.info("Calculating Kij-B for temperature == %s | pressure == %s" % (temperature, pressure))
        kij_input_df = getattr(kij_module, "kij_input")(db, eos, temperature, pressure)
        kij_input_df["diff_alpha_wrt_temp"] = diff_alpha_eos(db, eos, temperature, pressure)

        if temperature_changed:
            logger.info("calculating eij")
            eij_input_df = eij_input(db)
            eij_output = eij(eij_input_df, temperature)
            eij_output = getattr(kij_module, "enrich_eij")(eij_output, temperature, kij_input_df)

        kij_output = getattr(kij_module, "kij")(kij_input_df, eij_output)
        save(kij_output, user_args, identifier="kij-b", eos=eos, temperature=temperature, pressure=pressure)


def run_kij_c(pt, eos, user_args):
    kij_module = importlib.import_module("Scripts.pvt_new.pt_matching.kij.kij_c")

    for index, row in pt.iterrows():
        temperature, pressure = row.temperature, row.pressure
        logger.info("Calculating Kij-C for temperature == %s | pressure == %s" % (temperature, pressure))
        kij_input_df = getattr(kij_module, "kij_input")(db, eos, temperature, pressure)

        kij_output = getattr(kij_module, "kij")(kij_input_df, temperature)
        save(kij_output, user_args, identifier="kij-c", eos=eos, temperature=temperature, pressure=pressure)


def run_kij(user_args, eos, pt):
    if user_args.kij == "all" or user_args.kij == "a":
        run_kij_a(pt, eos, user_args)
    if user_args.kij == "all" or user_args.kij == "b":
        run_kij_b(pt, eos, user_args)
    if user_args.kij == "all" or user_args.kij == "c":
        run_kij_c(pt, eos, user_args)


def kij(user_args):
    pt = pt_list(user_args)
    if user_args.eos == "all":
        for eos in valid_eos:
            run_kij(user_args, eos, pt)
    else:
        run_kij(user_args, user_args.eos, pt)


if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection

    db = get_connection("AB8")
    test_user_args = pd.Series({"debug": False, "well_conn": db, "eos": "poas_4", "kij": "b"})
    kij(test_user_args)
