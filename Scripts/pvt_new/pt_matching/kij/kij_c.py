import os.path

import numpy as np
import pandas as pd

from Scripts.pvt_new.core.get_logger import get_logger
from Scripts.pvt_new.settings import CSV_DATA
from input.observed import observed
from input.solubility_data import solubility
from input.volume_298 import volume_298
from Scripts.pvt_new.core.constants import gasConstant
from Scripts.pvt_new.eos.get_zl_zv import get_zl_zv

logger = get_logger("kij c")


def split_data(df):
    pattern = r"(^C[5-9]|^C\d{2,})$"
    hc = df[df.scn.str.match(pattern)].reset_index(drop=True)
    non_hc = df[~df.scn.str.match(pattern)]
    return hc, non_hc


def enrich_non_hc(df):
    cols = ["Sol_Dispersion", "Sol_Polar", "Sol_H Bound", "V ref", "KIJ_factor"]
    api_data = pd.read_csv(os.path.join(CSV_DATA, "api_data.csv"))
    api_data = api_data.loc[api_data.Compound.isin(df.component), ["Compound"] + cols]
    api_data.rename(columns={"Compound": "component"}, inplace=True)
    non_hc = pd.merge(df, api_data, on='component', how='inner')
    non_hc.rename(columns={"Sol_Dispersion": "sol_dispersion",
                           "Sol_Polar": "sol_polar",
                           "Sol_H Bound": "sol_h_bonding",
                           "V ref": "v_ref",
                           "KIJ_factor": "kij_factor"}, inplace=True)
    return non_hc


def enrich_hc(well_conn, df):
    v_ref = volume_298(well_conn)["volume"].values
    sol = solubility(well_conn, v_ref)
    hc = pd.concat([df, sol], axis=1)
    hc["v_ref"] = v_ref
    hc["kij_factor"] = 10.
    return hc


def apply_fugacity_rule(r1, r2, B, ZL, phl, phv):
    if (ZL - B) <= 0 or (B * (r2 - r1)) == 0 or (ZL + r1 * B) == 0 or (ZL + r2 * B) <= 0 or (ZL + r1 * B) <= 0 or (
            (ZL + r2 * B) / (ZL + r1 * B)) <= 0:
        return phv
    return phl


def calc_fugacity(df, pressure, temperature):
    df["u"] = 2.
    df["w"] = -1.
    df["bp"] = df.bp_final1
    df["bp_final1"] = temperature
    for index, row in df.iterrows():
        row = get_zl_zv(row, pressure, [0, 0])
        cols = ["A", "B", "r1", "r2", "b"]
        for col in cols:
            df.loc[df.index == index, col] = row[col]
        df.loc[df.index == index, "fugacity"] = apply_fugacity_rule(row.r1, row.r2, row.B, row.ZL, row.phl, row.phv)
        df.loc[df.index == index, "Z"] = apply_fugacity_rule(row.r1, row.r2, row.B, row.ZL, row.ZL, row.ZV)
    return df


def kij_input(well_conn, eos, temperature, pressure):
    df = observed(well_conn, eos, temperature, pressure)
    hc, non_hc = split_data(df)
    non_hc = enrich_non_hc(non_hc)
    hc = enrich_hc(well_conn, hc)
    df = pd.concat([non_hc, hc], axis=0, ignore_index=True)

    df["sol_dispersion"] = df.sol_dispersion / ((df.v_ref / df.volume) ** (-1.25))
    df["sol_polar"] = df.sol_polar / ((df.v_ref / df.volume) ** (-0.5))
    df["sol_h_bonding"] = df.sol_h_bonding / (np.exp(-1.32 * 10 ** -3 * (298.15 - temperature) -
                                                     np.log((df.v_ref / df.volume) ** 0.5)))

    index5 = df.loc[df.scn == "C5"].index.min()
    df.loc[df.index < index5, "acid_by_base"] = 0.95
    df["sol_h_bonding_acid"] = df.apply(lambda row: 0 if row.sol_h_bonding * row.acid_by_base < 0 else np.sqrt(
        row.sol_h_bonding * row.acid_by_base), axis=1)
    df["sol_h_bonding_base"] = df.apply(
        lambda row: 0 if row.acid_by_base == 0 else row.sol_h_bonding_acid / row.acid_by_base, axis=1)
    df.loc[df.index < index5, "q_par"] = 1.
    scns = np.array([float(scn.replace("C", "")) for scn in df.loc[df.index >= index5].scn.tolist()])
    df.loc[df.index >= index5, "q_par"] = (
            0.000026945833142228 * scns ** 2 - 0.00501089299637658 * scns + 1.01942737237995)
    df["pol_par"] = 1. + 1.15 * df.q_par ** 4 * (1. - np.exp((-1. * 0.002337 * df.sol_polar ** 3).tolist()))
    df["zi_par"] = (0.68 * (df.pol_par - 1.) + ((3.24 - 2.4 * np.exp(
        (-1. * 0.002687 * (df.sol_h_bonding_acid * df.sol_h_bonding_base) ** 1.5).tolist())) **
                                                (293. / temperature)) ** 2)
    df["psi_par"] = df.pol_par + 0.002629 * df.sol_h_bonding_acid * df.sol_h_bonding_base
    df["aa_par"] = np.abs(0.953 - 0.002314 * (df.sol_polar ** 2 + df.sol_h_bonding_acid * df.sol_h_bonding_base))
    df = calc_fugacity(df, pressure, temperature)
    return df


def kij(df, temperature):
    kij_df = df.copy(deep=True)
    for index, row in df.iterrows():
        for_comp = row.scn if pd.isna(row.component) else row.component
        df["d12"] = 1. - (df.b / row.b) ** df.aa_par + df.aa_par * np.log(df.b / row.b)
        df["d21"] = 1. - (row.b / df.b) ** row.aa_par + row.aa_par * np.log(row.b / df.b)
        df["coeff2"] = ((df.b * 10 ** 6 / (gasConstant * temperature)) *
                        ((df.sol_dispersion - row.sol_dispersion) ** 2 +
                         ((row.q_par ** 2 * df.q_par ** 2) * (df.sol_polar - row.sol_polar) ** 2) /
                         row.psi_par + (df.sol_h_bonding_acid - row.sol_h_bonding_acid) *
                         (df.sol_h_bonding_base - row.sol_h_bonding_base) / row.zi_par) + df.d12)
        df["coeff1"] = (row.b * 10 ** 6 / (gasConstant * temperature)) * \
                       ((df.sol_dispersion - row.sol_dispersion) ** 2 +
                        ((row.q_par ** 2 * df.q_par ** 2) * (df.sol_polar - row.sol_polar) ** 2) /
                        df.psi_par + (df.sol_h_bonding_acid - row.sol_h_bonding_acid) *
                        (df.sol_h_bonding_base - row.sol_h_bonding_base) / df.zi_par) + df.d21
        df["s21"] = (df.fugacity + df.coeff2 - (df.b / row.b) * (row.Z - 1.) + np.log(row.Z - row.B))
        df["s22"] = (row.A / (row.B * (row.r1 - row.r2))) * np.log(
            (row.Z + row.B * row.r2) / (row.Z + row.B * row.r1))
        df["s2"] = df.s21 / df.s22
        df["k2_infinity"] = 1 - (df.s2 + (df.b / row.b)) / (2 * np.sqrt(df.ac_alpha / row.ac_alpha))
        df["s11"] = (row.fugacity + df.coeff1 - (row.b / df.b) * (df.Z - 1) + np.log(df.Z - df.B))
        df["s12"] = (df.A / (df.B * (df.r1 - df.r2))) * np.log((df.Z + df.r2 * df.B) / (df.Z + df.r1 * df.B))
        df["s1"] = df.s11 / df.s12
        df["k1_infinity"] = 1 - (df.s1 + row.b / df.b) / (2 * np.sqrt(row.ac_alpha / df.ac_alpha))
        df[for_comp] = df.apply(
            lambda row1: (0.5 * row1.k2_infinity + 0.5 * row1.k1_infinity) / row.kij_factor
            if ((row.mf + row1.mf == 0) or (row.mf == 0) or (row1.mf == 0))
            else ((row.mf / (row.mf + row1.mf)) * row1.k2_infinity +
                  (row1.mf / (row.mf + row1.mf)) * row1.k1_infinity) / row.kij_factor, axis=1)
        kij_df[for_comp] = df[for_comp]
        if pd.isna(row.component):
            kij_df.loc[kij_df.scn == row.scn, row.scn] = 0
        else:
            kij_df.loc[kij_df.component == row.component, row.component] = 0
        kij_df[for_comp] = kij_df.apply(
            lambda row1: row1[for_comp] if np.abs(row1[for_comp]) <= 0.4
            else (0.6 * row1[for_comp] if (int(row1[for_comp]) - (1 if np.sign(row1[for_comp]) < 0 else 0)) == 0
                  else (np.sign(row1[for_comp]) * 0.5 * row1[for_comp] /
                        (int(row1[for_comp]) - (1 if np.sign(row1[for_comp]) < 0 else 0)))), axis=1)
    return kij_df


if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection

    well_name = "AB8"
    db = get_connection(well_name)
    test_temperature = 380.

    logger.info("preparing kij input")
    test_kij_input = kij_input(db, "poas_4", test_temperature, 5.)

    # kij
    logger.info("calculating kij c")
    kij_output = kij(test_kij_input, test_temperature)
    kij_output.to_csv("kij-c.csv")
