import os.path

import pandas as pd

from Scripts.pvt_new.settings import CSV_DATA
from Scripts.pvt_new.core.get_logger import get_logger
from cm_groups import cm_group_data
from Scripts.pvt_new.core.constants import cm_groups as groups
from Scripts.pvt_new.core.storage.mongo import fetch

logger = get_logger("eij")


def eij_input(well_conn):
    hc = cm_group_data(well_conn)
    validated_data = fetch(well_conn, property="pvt_output",
                           identifier="validated_data", plus_or_individual="individual")
    non_hc = validated_data.loc[validated_data.short_name < 5.][["scn", "component", "rf_mf"]]
    non_hc.rename(columns={"rf_mf": "mf"}, inplace=True)

    api_data = pd.read_csv(os.path.join(CSV_DATA, "api_data.csv"))
    api_data = api_data.loc[api_data.Compound.isin(non_hc.component), groups+["Compound"]]
    api_data.rename(columns={"Compound": "component"}, inplace=True)

    non_hc = pd.merge(non_hc, api_data, on='component', how='inner')
    df = pd.concat([non_hc, hc], axis=0)
    df.reset_index(inplace=True, drop=True)
    return df


def get_groups(temperature):
    group_values = pd.read_csv(os.path.join(CSV_DATA, "kij_groups.csv"))
    final_data = pd.DataFrame()
    for group in groups:
        ref_group0 = group_values.loc[group_values.index % 2 == 0, group].reset_index(drop=True)
        ref_group1 = group_values.loc[group_values.index % 2 != 0, group].reset_index(drop=True)
        ref_group = ref_group0 * (298.15 / temperature) ** (ref_group1 / ref_group0 - 1.)
        final_data[group] = ref_group
    final_data.set_index(final_data.columns, inplace=True)
    final_data = final_data.T
    return final_data


def eij(df, temperature):
    group_values = get_groups(temperature)
    df["ng"] = df[groups].sum(axis=1)
    for group in groups:
        df.loc[df.ng == 0, group] = 0
        df.loc[df.ng != 0, group] = df.loc[df.ng != 0, group] / df.loc[df.ng != 0, "ng"]
    eij_df = df[["scn", "component", "mf"]].copy(deep=True)
    for index, row in df.iterrows():
        difference = row[groups] - df[groups]
        products = difference.dot(group_values)
        answer = difference * products
        eij_df[row.scn if pd.isna(row.component) else row.component] = answer.sum(axis=1) * -0.5
    return eij_df


if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection

    well_name = "AB8"
    db = get_connection(well_name)
    test_input = eij_input(db)
    test_eij = eij(test_input, temperature=380)
    test_eij.to_csv("eij.csv")
