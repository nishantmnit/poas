import numpy as np
import pandas as pd

from observed import get_non_hc, get_hc


def intercept_slope(dfs, y_col, temperatures):
    samples = len(temperatures)
    df = pd.DataFrame()
    for i in range(samples):
        temperature = temperatures[i]
        df["x%s" % i] = dfs[temperature]["temperature"]
        df["y%s" % i] = np.log(dfs[temperature][y_col])
        if i == 0:
            df["xi"] = df["x%s" % i]
            df["yi"] = df["y%s" % i]
            df["xiyi"] = df["x%s" % i] * df["y%s" % i]
            df["xi2"] = df["x%s" % i] ** 2
        else:
            df["xi"] = df["xi"] + df["x%s" % i]
            df["yi"] = df["yi"] + df["y%s" % i]
            df["xiyi"] = df["xiyi"] + (df["x%s" % i] * df["y%s" % i])
            df["xi2"] = df["xi2"] + df["x%s" % i] ** 2

    df["%s_intercept" % y_col] = (df.xi * df.xiyi - df.yi * df.xi2) / (df.xi ** 2 - samples * df.xi2)
    df["%s_slope" % y_col] = (df.yi - samples * df["%s_intercept" % y_col]) / df.xi
    value = np.exp(df["%s_slope" % y_col] * temperatures[0] + df["%s_intercept" % y_col])
    multiplier = np.exp(df.y0) / value
    df["diff_alpha_eos"] = multiplier * np.exp(df["%s_slope" % y_col] * temperatures[1] +
                                               df["%s_intercept" % y_col]) * df["%s_slope" % y_col]
    return df["diff_alpha_eos"].values


def diff_alpha_eos(well_conn, eos, required_temperature, pressure):
    temperatures = [required_temperature - 5., required_temperature, required_temperature + 5.]
    dfs = dict()
    for temperature in temperatures:
        non_hc = get_non_hc(well_conn, eos, temperature, pressure)
        hc = get_hc(well_conn, eos, temperature, pressure)
        df = pd.concat([non_hc, hc], axis=0, ignore_index=True)
        df["temperature"] = temperature
        dfs[temperature] = df
    return intercept_slope(dfs, "alpha_eos", temperatures)


if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection
    from Scripts.pvt_new.core.get_logger import get_logger

    logger = get_logger("diff alpha eos")
    logger.info("start")
    well_name = "AB8"
    test_eos = "poas_4"
    db = get_connection(well_name)
    output = diff_alpha_eos(db, test_eos, 380., 10.)
    print output
    logger.info("end")
