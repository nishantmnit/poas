import numpy as np
import pandas as pd

from Scripts.pvt_new.core.constants import solubility_temperatures
from Scripts.pvt_new.core.storage.mongo import fetch
from Scripts.pvt_new.vle.get_vle_data import vle_data
from hc_volume import hc_volume


def get_non_hc(well_conn, eos, temperature, pressure):
    dfs = dict()
    validated_data = fetch(well_conn, property="pvt_output",
                           identifier="validated_data", plus_or_individual="individual")
    non_hc = validated_data.loc[validated_data.short_name < 5.][["component", "scn", "bp", "rf_mf"]]
    non_hc.rename(columns={"bp": "bp_final1", "rf_mf": "mf"}, inplace=True)
    for index, row in non_hc.iterrows():
        dfs[index] = vle_data(row.component, temperature, pressure, eos)
    df = pd.concat(dfs.values(), ignore_index=True)
    non_hc = pd.concat([non_hc, df], axis=1)
    return non_hc


def get_hc(well_conn, eos, temperature, pressure):
    sol = fetch(well_conn, property="pvt_output", identifier="solubility",
                eos=eos, temperature=solubility_temperatures[0]).reset_index()
    sol = sol[["component", "scn", "bp_final1", "mf"]]
    hc = hc_volume(well_conn, eos, temperature, 10.)
    hc1 = hc_volume(well_conn, eos, temperature, pressure)
    hc["volume"] = hc1["volume"]
    sol["scn"] = sol.apply(lambda row: "C%s" % row.scn, axis=1)
    df = pd.concat([sol, hc], axis=1)
    return df


def calc_ai_by_eq(df):
    df["ac_alpha"] = df.ac * df.alpha_eos
    df["multiplier"] = 0.0777960739038885 / df.om_b
    df["ai_eq"] = df.ac_alpha
    df["bi_eq"] = df.b
    df["ai_bi_eq"] = np.sqrt(df.ai_eq * 10 ** 6) / (df.bi_eq * 10 ** 6)
    return df


def observed(well_conn, eos, temperature, pressure):
    non_hc = get_non_hc(well_conn, eos, temperature, pressure)
    hc = get_hc(well_conn, eos, temperature, pressure)
    df = pd.concat([non_hc, hc], axis=0, ignore_index=True)
    df = calc_ai_by_eq(df)
    return df


if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection
    from Scripts.pvt_new.core.get_logger import get_logger

    logger = get_logger("hc volume")
    logger.info("start")
    well_name = "AB8"
    test_eos = "poas_4"
    db = get_connection(well_name)
    output = observed(db, test_eos, 380., 5.)
    # output.to_csv("kij_input.csv")
    logger.info("end")
