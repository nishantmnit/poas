import numpy as np

from Scripts.pvt_new.core.storage.fetch_most import fetch

temperatures = [288.7056, 300.]


def final_thermal_expansion(row):
    if row.thermal_expansion_a > 0 and row.thermal_expansion_b > 0:
        return min(row.thermal_expansion_a, row.thermal_expansion_b)
    if row.thermal_expansion_a <= 0 and row.thermal_expansion_b <= 0:
        return row.thermal_expansion_1
    if row.thermal_expansion_a <= 0 or row.thermal_expansion_b <= 0:
        term = max(row.thermal_expansion_a, row.thermal_expansion_b)
        return min(term, row.thermal_expansion_1)


def volume_298(well_conn, eos="poas_4"):
    df = prepare_input(well_conn, eos)
    df["density_1"] = df.mass / df.v_liq_1
    df["density_2"] = df.mass / df.v_liq_2
    df["thermal_coefficient"] = 0.8
    df["y1"] = np.log(df.density_2 / df.density_1)
    df["a1"] = ((df.temp_2 - 273.15) - (df.temp_1 - 273.15))
    df["a2"] = df.a1 * df.thermal_coefficient
    df["a1a2"] = df.a1 * df.a2
    df["thermal_expansion_1"] = 613.97226 / (df.density_1 * 1000) ** 2
    df["term_1"] = (df.a1 ** 2 - 4. * df.a1a2 * df.y1)
    df["term_2"] = np.abs((-df.a1 + np.sqrt(df.term_1)) / (2. * df.a1a2))
    df["term_3"] = ((-df.a1 + np.sqrt(df.term_1)) / (2. * df.a1a2))
    df["thermal_expansion_a"] = df.apply(lambda row: row.thermal_expansion_1 if row.term_1 < 0 else (
        row.term_2 if row.term_2 <= 10 ** -4 else row.term_3), axis=1)

    df["term_2"] = np.abs((-df.a1 - np.sqrt(df.term_1)) / (2. * df.a1a2))
    df["term_3"] = ((-df.a1 - np.sqrt(df.term_1)) / (2. * df.a1a2))
    df["thermal_expansion_b"] = df.apply(lambda row: row.thermal_expansion_1 if row.term_1 < 0 else (
        row.term_2 if row.term_2 <= 10 ** -4 else row.term_3), axis=1)
    df["thermal_expansion"] = df.apply(
        lambda row: final_thermal_expansion(row),
        axis=1)
    df["density"] = df.density_1 * np.exp(-1. * df.thermal_expansion * ((298.15 - 273.15) - (288.7056 - 273.15)) * (
            1. + df.thermal_coefficient * df.thermal_expansion * ((298.15 - 273.15) - (288.7056 - 273.15))))
    df["volume"] = df.mass / df.density
    return df[["density", "volume"]]


def prepare_input(well_conn, eos="poas_4"):
    df = fetch(well_conn, property="pvt_output", identifier="fcm")[["seq", "mass"]]
    df["scn"] = df.index
    df.reset_index(inplace=True, drop=True)
    df["temp_1"] = temperatures[0]
    df["temp_2"] = temperatures[1]
    df["v_liq_1"] = fetch(well_conn, property="pvt_output", identifier="solubility",
                          eos=eos, temperature=temperatures[0])["v_liq"].values
    df["v_liq_2"] = fetch(well_conn, property="pvt_output", identifier="solubility",
                          eos=eos, temperature=temperatures[1])["v_liq"].values
    return df


if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection

    well_name = "AB8"
    db = get_connection(well_name)
    out_df = volume_298(db)
    print out_df
