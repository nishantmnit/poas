import numpy as np
import pandas as pd

from Scripts.pvt_new.core.constants import gasConstant
from Scripts.pvt_new.core.constants import solubility_temperatures
from Scripts.pvt_new.core.storage.mongo import fetch
from Scripts.pvt_new.eos.get_zl_zv import get_zl_zv
from Scripts.pvt_new.eos.om_b_to_eos_multipliers import eos_multiplier
from hc_om_b_alpha import om_b_alpha_trend


def prepare_input(well_conn, eos, temperature):
    df = fetch(well_conn, property="pvt_output",
               identifier="solubility", eos=eos,
               temperature=solubility_temperatures[0]).reset_index(drop=True)
    cols = ["tc_k", "tcbytb", "pc_mpa", "zc_final", "vc", "omega", "om_b"]
    df = df[cols]
    df.loc[df.tcbytb < 1.0001, "tcbytb"] = 1.0001
    df = pd.concat([df, om_b_alpha_trend(well_conn, eos)], axis=1)
    df["alpha_eos"] = np.exp(df.alpha_eos_slope * temperature + df.alpha_eos_intercept) * df.alpha_eos_multiplier
    df["om_b"] = np.exp(df.om_b_slope * temperature + df.om_b_intercept) * df.om_b_multiplier
    df["bp_final1"] = temperature
    return df


def calc_alpha_eos(df):
    u, w = 2., -1.
    m = ((df.v_combined ** 2 + u * df.b * 10 ** 6 * df.v_combined + w * (df.b * 10 ** 6) ** 2) / (
            df.v_combined ** 2 + df.u * df.b * 10 ** 6 * df.v_combined + df.w * (df.b * 10 ** 6) ** 2))
    ac_alpha = df.ac_alpha * m
    df["alpha_eos"] = ac_alpha / df.ac
    return df


def calc_v_solubility(row, temperature, v_vapour, v_liq):
    if temperature > row.tc_k and v_vapour > 0:
        return v_vapour
    if (v_liq + row.r1 * row.b * 10 ** 6 <= 0 or
            v_liq + row.r2 * row.b * 10 ** 6 <= 0 or
            (v_liq + row.r1 * row.b * 10 ** 6) / (v_liq + row.r2 * row.b * 10 ** 6) < 0):
        return v_vapour
    return v_liq


def calc_volume(df, temperature, pressure):
    for index, row in df.iterrows():
        row = get_zl_zv(row, pressure, [0, 0])
        v_liq = row.ZL * gasConstant * temperature / pressure
        v_vapour = row.ZV * gasConstant * temperature / pressure
        v_combined = max(v_liq, v_vapour)
        df.loc[df.index == index, "v_liq"] = v_liq
        df.loc[df.index == index, "v_vapour"] = v_vapour
        df.loc[df.index == index, "v_combined"] = v_combined
        df.loc[df.index == index, "volume"] = calc_v_solubility(row, temperature, v_vapour, v_liq)
    return df


def hc_volume(well_conn, eos, temperature, pressure):
    df = prepare_input(well_conn, eos, temperature)
    df = eos_multiplier(df, eos)
    df = calc_volume(df, temperature, pressure)
    df = calc_alpha_eos(df)
    cols = ["tc_k", "pc_mpa", "zc_final", "vc", "omega", "volume"]

    return df[["om_b", "b", "ac", "alpha_eos"] + cols]


if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection
    from Scripts.pvt_new.core.get_logger import get_logger

    logger = get_logger("hc volume")

    logger.info("start")

    well_name = "AB8"
    test_eos = "poas_4"
    db = get_connection(well_name)
    output = hc_volume(db, test_eos, 380.)
    print output
    logger.info("end")
