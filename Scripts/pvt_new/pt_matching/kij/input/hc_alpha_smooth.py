import numpy as np

from Scripts.pvt_new.core.storage.mongo import fetch
from solubility_data import solubility
from volume_298 import volume_298


def prepare_input(well_conn, eos, temperature):
    volume = volume_298(well_conn, eos)
    sol = solubility(well_conn, volume.volume.values)

    df = fetch(well_conn, property="pvt_output",
               identifier="solubility", eos=eos,
               temperature=temperature)[["scn", "component", "v_liq"]].reset_index()
    df["solubility"] = np.sqrt(
        ((sol.sol_dispersion / ((volume["volume"] / df.v_liq) ** (-1.25))) ** 2 +
         (sol.sol_polar / ((volume["volume"] / df.v_liq) ** (-0.5))) ** 2 +
         (sol.sol_h_bonding / (np.exp(-1. * 1.32 * 10. ** -3. * (298.15 - temperature) -
                                      np.log((volume["volume"] / df.v_liq) ** 0.5)))) ** 2).values)
    sol[["scn", "component", "solubility"]] = df[["scn", "component", "solubility"]]
    sol["temperature"] = temperature
    return sol


def smooth_alpha(well_conn, eos, sol, temperature):
    df = fetch(well_conn, property="pvt_output", identifier="solubility",
               eos=eos, temperature=temperature)[
        ["scn", "v_liq", "solubility", "alpha_eos", "alpha_pr", "alpha_l", "alpha_m", "r1", "r2", "b", "ac", "t2",
         "t3", "tc_k", "alpha_multiplier"]].reset_index()
    df["error"] = 100. * np.abs((sol["solubility"] - df.solubility) / sol["solubility"])
    df["t1"] = sol["solubility"] ** 2 / (np.abs(df.t2 - df.t3))
    df["factor_k"] = (df.v_liq * df.t1 * df.b * (df.r2 - df.r1)) / df.ac
    df["factor_s"] = df.alpha_pr - df.alpha_pr * temperature * (
            ((2. * df.alpha_m - 2.) / (temperature / df.tc_k)
             + df.alpha_l * (-2. * df.alpha_m) * (temperature / df.tc_k)
             ** (2. * df.alpha_m - 1.)) / df.tc_k)
    df["expected_alpha_multiplier"] = df.factor_k / df.factor_s
    df["smooth_alpha_multiplier"] = df.apply(lambda row: row.alpha_multiplier if row.solubility <= 5 else (
        row.expected_alpha_multiplier if row.error >= 5 else row.alpha_multiplier), axis=1)
    sol["alpha_eos"] = df.smooth_alpha_multiplier * df.alpha_pr

    sol[["tc_k", "alpha_l", "alpha_m"]] = df[["tc_k", "alpha_l", "alpha_m"]]
    return sol


def alpha_eos(well_conn, temperature, eos):
    sol = prepare_input(well_conn, eos, temperature)
    sol = smooth_alpha(well_conn, eos, sol, temperature)
    return sol.alpha_eos.values


if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection

    well_name = "AB8"
    test_temperature = 200
    db = get_connection(well_name)
    output = alpha_eos(db, test_temperature, "sw_poas")
    print output
