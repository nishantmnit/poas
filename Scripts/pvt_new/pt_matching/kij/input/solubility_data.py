import os

import numpy as np
import pandas as pd

from Scripts.pvt_new.core.storage.mongo import fetch
from Scripts.pvt_new.settings import CSV_DATA

sol_input = os.path.join(CSV_DATA, "solubility_param.csv")


def solubility(well_conn, volume=None):
    groups = pd.read_csv(sol_input)
    cols = [col for col in groups.columns if col != "group"]
    cm = fetch(well_conn, property="pvt_output", identifier="fcm")
    cm["scn"] = cm.index
    cm.reset_index(inplace=True, drop=True)
    cm["Five Membered ring"] = cm.nap5_ring
    cm["Six Membered ring"] = cm.nap6_ring
    cm["Conjugation_Hoy"] = (cm.ar_cc + cm.nap5_cc + cm.nap6_cc) / 2.

    for index, row in groups.iterrows():
        cm[row.group] = cm[cols].dot(row[cols].T).tolist()

    if volume is None:
        vs = fetch(well_conn, property="pvt_output", identifier="volume_solver")
        cm["volume_298.15"] = vs["volume_298.15"]
    else:
        cm["volume_298.15"] = volume

    cm["Hoy-FT"] = np.abs(cm["Hoy-FT"])

    cm["hoy_total_sol"] = (cm["Hoy-FT"] + 277.) / cm["volume_298.15"]
    cm["alpha_factor1"] = 10 ** (3.39066 * (cm.bp_final1 / cm.tc_k_final) - np.log10(cm["volume_298.15"]) - 0.15848)
    cm["hoy_polar_sol"] = cm.hoy_total_sol * (cm["Hoy-Fp"] / (cm.alpha_factor1 * (cm["Hoy-FT"] + 277.))) ** 0.5
    cm["hydrogen_bond_sol"] = np.sqrt((cm["Eh=Hydrogen-bond"] / cm["volume_298.15"]))
    cm.loc[cm.hydrogen_bond_sol > cm.hoy_polar_sol, "hydrogen_bond_sol"] = 0.8 * cm.loc[
        cm.hydrogen_bond_sol > cm.hoy_polar_sol, "hoy_polar_sol"]
    cm["hoy_dispersion_sol"] = (cm.hoy_total_sol ** 2 - cm.hoy_polar_sol ** 2 - cm.hydrogen_bond_sol ** 2) ** 0.5
    cm.loc[cm.hoy_dispersion_sol < 0, "hoy_dispersion_sol"] = cm.hoy_total_sol * 0.6
    cm.loc[cm.Hbase != 0, "acid_by_base"] = cm.loc[cm.Hbase != 0, "Hacid"] / cm.loc[cm.Hbase != 0, "Hbase"]
    cm.loc[cm.Hbase == 0, "acid_by_base"] = 0

    cm["sol_total"] = cm.hoy_total_sol
    cm["sol_polar"] = cm.hoy_polar_sol
    cm["sol_h_bonding"] = cm.hydrogen_bond_sol
    cm["sol_dispersion"] = cm.hoy_dispersion_sol
    cm["sol_h_bonding_acid"] = np.sqrt((cm.sol_h_bonding * cm.acid_by_base))
    cm["sol_h_bonding_base"] = cm.sol_h_bonding_acid / cm.acid_by_base
    cm["acid_by_base"] = cm.acid_by_base
    needed = ["sol_total", "sol_polar", "sol_h_bonding", "sol_dispersion", "sol_h_bonding_acid", "sol_h_bonding_base",
              "acid_by_base"]
    return cm[needed]


if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection
    from volume_298 import volume_298

    well_name = "AB8"
    test_eos = "poas_4"
    db = get_connection(well_name)
    out_df = solubility(db)
    print out_df
