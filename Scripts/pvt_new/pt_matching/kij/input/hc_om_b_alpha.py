import numpy as np
import pandas as pd

from Scripts.pvt_new.core.constants import gasConstant, pressure
from Scripts.pvt_new.core.constants import solubility_temperatures
from Scripts.pvt_new.core.storage.mongo import fetch
from Scripts.pvt_new.eos.om_b_to_eos_multipliers import eos_multiplier
from hc_alpha_smooth import alpha_eos


def prepare_input(well_conn, eos, temperature):
    df = fetch(well_conn, property="pvt_output",
               identifier="solubility", eos=eos,
               temperature=temperature).reset_index(drop=True)
    cols = ["tc_k", "tcbytb", "pc_mpa", "zc_final", "vc", "omega", "om_b"]
    df = df[cols]
    df["alpha_eos"] = alpha_eos(well_conn, temperature, eos)
    df["temperature"] = temperature
    df["rll_wc"] = fetch(well_conn, property="pvt_output", identifier="fcm")["rll_wc"].reset_index(drop=True)
    df["r_star"] = df.rll_wc / 6.987
    return df


def intercept_slope(dfs, y_col):
    samples = len(solubility_temperatures)
    df = pd.DataFrame()
    for i in range(samples):
        df["x%s" % i] = dfs[i]["temperature"]
        df["y%s" % i] = np.log(dfs[i][y_col])
        if i == 0:
            df["xi"] = df["x%s" % i]
            df["yi"] = df["y%s" % i]
            df["xiyi"] = df["x%s" % i] * df["y%s" % i]
            df["xi2"] = df["x%s" % i] ** 2
        else:
            df["xi"] = df["xi"] + df["x%s" % i]
            df["yi"] = df["yi"] + df["y%s" % i]
            df["xiyi"] = df["xiyi"] + (df["x%s" % i] * df["y%s" % i])
            df["xi2"] = df["xi2"] + df["x%s" % i] ** 2

    df["%s_intercept" % y_col] = (df.xi * df.xiyi - df.yi * df.xi2) / (df.xi ** 2 - samples * df.xi2)
    df["%s_slope" % y_col] = (df.yi - samples * df["%s_intercept" % y_col]) / df.xi
    if y_col == "alpha_eos":
        col = "alpha_eos_slope"
        df.loc[df[col] > 0, col] = -1. * df.loc[df[col] > 0, col]
    temperature = solubility_temperatures[0]
    value = np.exp(df["%s_slope" % y_col] * temperature + df["%s_intercept" % y_col])
    df["%s_multiplier" % y_col] = np.exp(df.y0) / value
    return df[["%s_intercept" % y_col, "%s_slope" % y_col, "%s_multiplier" % y_col]]


def calibrate_alpha(dfs):
    for key, df in dfs.items():
        df["factor_a"] = (df.b * 10 ** 6) ** 2 * (df.w - df.u) - (df.b * 10 ** 6) * df.u * (
                gasConstant * df.temperature / pressure)
        df["term"] = df.factor_1 - df.factor_a
        df["ac_alpha"] = (df.term * pressure) / 10 ** 6
        df["alpha_eos"] = df.ac_alpha / df.ac
        dfs[key] = df
    return dfs


def get_multiplier_200(dfs):
    df = dfs[0]
    om_b = (1. / (
            1. + (df.r_star - 1.) * (0.02 * (1. - 0.092 * np.exp(-1000. * np.abs(200. / df.tc_k - 1.))) - 0.035 *
                                     (200. / df.tc_k - 1.)))) * 0.08664
    multiplier = df.om_b / om_b
    for key, df in dfs.items():
        dfs[key]["multiplier_200"] = multiplier
    return dfs


def calibrate_om_b(dfs, eos):
    dfs = get_multiplier_200(dfs)
    for key, df in dfs.items():
        df["factor_1"] = (df.b * 10 ** 6) ** 2 * (df.w - df.u) - (df.b * 10 ** 6) * df.u * (
                gasConstant * df.temperature / pressure) + (df.ac_alpha * 10 ** 6 / pressure)
        df["om_b"] = (1 / (1 + (df.r_star - 1) * (
                0.02 * (1 - 0.092 * np.exp(-1000 * np.abs(df.temperature / df.tc_k - 1))) - 0.035 *
                (df.temperature / df.tc_k - 1)))) * 0.08664 * df.multiplier_200
        df = eos_multiplier(df, eos)
        dfs[key] = df
    return dfs


def om_b_alpha_trend(well_conn, eos):
    dfs = dict()
    for i in range(len(solubility_temperatures)):
        temperature = solubility_temperatures[i]
        df = prepare_input(well_conn, eos, temperature)
        df = eos_multiplier(df, eos)
        dfs[i] = df

    dfs = calibrate_om_b(dfs, eos)
    dfs = calibrate_alpha(dfs)
    om_b_trend = intercept_slope(dfs, "om_b")
    alpha_eos_trend = intercept_slope(dfs, "alpha_eos")
    df = pd.concat([om_b_trend, alpha_eos_trend], axis=1)
    return df


if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection

    well_name = "AB8"
    test_eos = "pt_poas"
    db = get_connection(well_name)
    output = om_b_alpha_trend(db, test_eos)
    print output
