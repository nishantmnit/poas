import numpy as np
import pandas as pd

from Scripts.pvt_new.core.get_logger import get_logger
from Scripts.pvt_new.core.pandas.dataframe import drop_cols
from input.observed import observed

logger = get_logger("kij b")


def kij_input(well_conn, eos, temperature, pressure):
    df = observed(well_conn, eos, temperature, pressure)
    return df


def calc_cpar(df):
    df["u"] = 2.
    df["w"] = -1.
    df["term"] = (df.u ** 2 - 4. * df.w)
    df.loc[df.term < 0, "epsilon"] = 0
    df.loc[df.term >= 0, "epsilon"] = (df.u + np.sqrt(df.u ** 2 - 4. * df.w)) / 2.
    df.loc[df.term < 0, "sigma"] = 0
    df.loc[df.term >= 0, "sigma"] = (df.u - np.sqrt(df.u ** 2 - 4. * df.w)) / 2.
    df["cpar"] = df.apply(
        lambda row1: 0.587 if row1.epsilon - row1.sigma <= 0 or 1. + row1.epsilon <= 0 or (1. + row1.epsilon) / (
                1. + row1.sigma) < 0 else (1. / (row1.epsilon - row1.sigma)) * np.log(
            (1. + row1.epsilon) / (1. + row1.sigma)), axis=1)
    return df


def enrich_eij(df, temperature, kij_input_df):
    kij_df = kij_input_df.copy(deep=True)
    drop_cols(kij_df, df.columns)
    df = pd.concat([df, kij_df], axis=1)
    df["delta"] = np.sqrt(df.ac_alpha * 10 ** 6) / (df.b * 10 ** 6)
    df["t1"] = (df.diff_alpha_wrt_temp * df.ac * 10 ** 6) / (df.b * 10 ** 6) ** 2
    df["t2"] = (df.diff_alpha_wrt_temp * df.ac * 10 ** 6) / (df.ac_alpha * 10 ** 6)
    df = calc_cpar(df)
    eij_df = df[["scn", "component", "mf"]].copy(deep=True)
    for index, row in df.iterrows():
        eij_df[row.scn if pd.isna(row.component) else row.component] = \
            ((df.cpar + row.cpar) / 2.) * ((row.delta - df.delta) ** 2 - temperature *
                                           ((row.t1 + df.t1) - (np.sqrt(row.ac_alpha * 10 ** 6 * df.ac_alpha * 10 ** 6)
                                                                / (row.b * 10 ** 6 * df.b * 10 ** 6)) *
                                            (row.t2 + df.t2)))
        if pd.isna(row.component):
            eij_df.loc[eij_df.scn == row.scn, row.scn] = 0
        else:
            eij_df.loc[eij_df.component == row.component, row.component] = 0
    return eij_df


def kij(df, eij_df):
    df["delta"] = np.sqrt(df.ac_alpha * 10 ** 6) / (df.b * 10 ** 6)
    kij_df = df.copy(deep=True)
    for index, row in df.iterrows():
        for_comp = row.scn if pd.isna(row.component) else row.component
        df["k1_others"] = 1. - (eij_df[for_comp] - row.delta ** 2 - df.delta ** 2) / (-2. * row.delta * df.delta)
        kij_df[
            for_comp] = df.k1_others
        if pd.isna(row.component):
            kij_df.loc[kij_df.scn == row.scn, row.scn] = 0
        else:
            kij_df.loc[kij_df.component == row.component, row.component] = 0
        kij_df[for_comp] = kij_df.apply(lambda row1: row1[for_comp] if np.abs(row1[for_comp]) <= 0.4 else (
            0.6 * row1[for_comp] if (int(row1[for_comp]) - (1. if np.sign(row1[for_comp]) < 0 else 0)) == 0 else (
                    np.sign(row1[for_comp]) * 0.5 * row1[for_comp] /
                    (int(row1[for_comp]) - (1 if np.sign(row1[for_comp]) < 0 else 0)))), axis=1)
    return kij_df


if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection
    from eij import eij, eij_input
    from input.diff_alpha_eos import diff_alpha_eos

    well_name = "AB8"
    db = get_connection(well_name)
    test_temperature = 395.372222222222
    test_pressure = 12.1836861339033

    # first calculate eij
    logger.info("preparing kij input")
    test_kij_input = kij_input(db, "poas_4", test_temperature, test_pressure)
    logger.info("calculating eij")
    test_kij_input["diff_alpha_wrt_temp"] = diff_alpha_eos(db, "poas_4", test_temperature, test_pressure)
    eij_input = eij_input(db)
    eij_output = eij(eij_input, test_temperature)
    logger.info("enriching eij")
    eij_output = enrich_eij(eij_output, test_temperature, test_kij_input)

    # kij
    logger.info("calculating kij b")
    kij_output = kij(test_kij_input, eij_output)
    test_user_args = pd.Series({"debug": False, "well_conn": db, "eos": "poas_4", "kij": "b"})
    from Scripts.pvt_new.core.storage.save import save
    save(kij_output, test_user_args, identifier="kij-b", eos="poas_4", temperature=test_temperature, pressure=test_pressure)

    kij_output.to_csv("kij-b.csv")
