import os

import pandas as pd

from Scripts.pvt_new.core.storage.mongo import fetch
from Scripts.pvt_new.settings import CSV_DATA


def cm_group_data(well_conn):
    cm = fetch(well_conn, property="pvt_output", identifier="fcm")
    cm["scn"] = cm.apply(lambda row1: "C%s" % row1.name, axis=1)
    cm.reset_index(drop=True, inplace=True)

    groups = pd.read_csv(os.path.join(CSV_DATA, "cm_groups.csv"))
    cols = [col for col in groups.columns if col != "group"]

    df = pd.DataFrame()

    for index, row in groups.iterrows():
        tdf = pd.DataFrame()
        for col in cols:
            tdf[col] = cm[col] * row[col]
        df[row.group] = tdf.sum(axis=1)
    df = pd.concat([cm[["scn", "component", "mf"]], df], axis=1)
    return df


if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection

    well_name = "AB8"
    test_eos = "pt_poas"
    db = get_connection(well_name)
    out_df = cm_group_data(db)
    print out_df
