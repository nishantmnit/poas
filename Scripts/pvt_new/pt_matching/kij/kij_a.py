import numpy as np
import pandas as pd

from Scripts.pvt_new.core.get_logger import get_logger
from input.observed import observed

logger = get_logger("kij a")


def kij_input(well_conn, eos, temperature, pressure):
    df = observed(well_conn, eos, temperature, pressure)
    return df


def calc_ai_bi_eq(df, temperature):
    df["k"] = df.apply(lambda row: (
            0.379642 + 1.48503 * row.omega - 0.164423 * row.omega ** 2 +
            0.016666 * row.omega ** 3) if row.omega > 0.491 else (
        (0.37464 + 1.54226 * row.omega - 0.26992 * row.omega ** 2) if row.omega <= 0.49099 else (
                432.093117 * row.omega - 211.086595)), axis=1)
    df["alpha_for_eos"] = (1. + df.k * (1. - np.sqrt(temperature / df.tc_k))) ** 2
    df["ai"] = (0.457235528921382 * ((8.314472 * df.tc_k) ** 2) / (df.pc_mpa * 10 ** 6)) * df.alpha_for_eos
    df["bi"] = 0.0777960739038885 * (8.314472 * df.tc_k / (df.pc_mpa * 10 ** 6))
    df["ai_bi"] = np.sqrt(df.ai * 10 ** 6) / (df.bi * 10 ** 6)
    return df


def kij(df, temperature, eij_df):
    df = calc_ai_bi_eq(df, temperature)
    kij_df = df.copy(deep=True)
    for index, row in df.iterrows():
        for_comp = row.scn if pd.isna(row.component) else row.component
        df["k1_others"] = (eij_df[for_comp] - (row.ai_bi - df["ai_bi"]) ** 2) / \
                          (row.ai_bi * df["ai_bi"] * 2.)
        kij_df[for_comp] = (2. * (df.multiplier + row.multiplier) / 2. * df.k1_others * row.ai_bi * df.ai_bi + (
                df.multiplier + row.multiplier) / 2. * (row.ai_bi - df.ai_bi) ** 2 - (
                                    row.ai_bi_eq - df.ai_bi_eq) ** 2) / (2. * row.ai_bi_eq * df.ai_bi_eq)
        kij_df[for_comp] = kij_df.apply(lambda row1: row1[for_comp] if np.abs(row1[for_comp]) <= 0.4 else (
            0.6 * row1[for_comp] if int(row1[for_comp]) == 0 else (np.sign(row1[for_comp]) * 0.5 * row1[for_comp] / (
                    int(row1[for_comp]) - (1 if int(row1[for_comp]) < 0 else 0)))), axis=1)
    return kij_df


if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection
    from eij import eij, eij_input
    from Scripts.pvt_new.core.storage.save import save

    well_name = "AB8"
    db = get_connection(well_name)
    test_temperature = 288.7056
    test_pressure = 0.2
    test_user_args = pd.Series({"debug": False, "well_conn": db, "eos": "poas_4", "kij": "a"})

    # first calculate eij
    logger.info("calculating eij")
    eij_input = eij_input(db)
    eij_output = eij(eij_input, test_temperature)

    # kij
    logger.info("calculating kij a")
    test_kij_input = kij_input(db, "poas_4", test_temperature, test_pressure)
    kij_output = kij(test_kij_input, test_temperature, eij_output)
    save(kij_output, test_user_args, identifier="kij-a", eos="poas_4", temperature=test_temperature,
         pressure=test_pressure)
