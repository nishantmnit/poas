import numpy as np
import os
import pandas as pd

from Scripts.pvt_new.core.get_logger import get_logger
from Scripts.pvt_new.core.storage.mongo import fetch
from Scripts.pvt_new.core.least_square import intercept_slope
from Scripts.pvt_new.settings import CSV_DATA


logger = get_logger("pvt bubble point finder")


def bubble_point_calc_input(well_conn):
    hc = fetch(well_conn, property="pvt_output", identifier="fcm")[
        ["component", "mf", "tc_k_final", "pc_mpa", "omega_final"]]
    hc.rename(columns={"tc_k_final": "tc_k", "omega_final": "omega"}, inplace=True)

    validated_data = fetch(well_conn, property="pvt_output",
                           identifier="validated_data", plus_or_individual="individual")
    non_hc = validated_data.loc[validated_data.short_name < 5.][["scn", "component", "rf_mf"]]
    non_hc.rename(columns={"rf_mf": "mf"}, inplace=True)

    api_data = pd.read_csv(os.path.join(CSV_DATA, "api_data.csv"))
    api_data = api_data.loc[api_data.Compound.isin(non_hc.component),
                            ["CriticalTemperature", "CriticalPressure", "AccentricFactorOmega", "Compound"]]
    api_data.rename(columns={"Compound": "component", "CriticalTemperature": "tc_k",
                             "CriticalPressure": "pc_mpa", "AccentricFactorOmega": "omega"}, inplace=True)
    non_hc = pd.merge(non_hc, api_data, on='component', how='inner')
    df = pd.concat([non_hc, hc], axis=0)
    df.reset_index(inplace=True, drop=True)
    return df


def straight_line_data(cce):
    df = cce.loc[(cce.rel_vol < 1.) & (~cce.pressure.isnull())]
    x1 = df.rel_vol.tail(3).to_list()
    x2 = df.rel_vol.head(3).to_list()
    y1 = df.pressure.tail(3).to_list()
    y2 = df.pressure.head(3).to_list()
    return x1, x2, y1, y2


def fit_straight_line(x1, x2, y1, y2):
    slope1, intercept1 = intercept_slope(np.array(x1), np.array(y1))
    slope2, intercept2 = intercept_slope(np.array(x2), np.array(y2))
    return ((slope1 * 1. + intercept1) + (slope2 * 1. + intercept2)) / 2.


def calc_bubble_point(well_conn, temperature):
    df = bubble_point_calc_input(well_conn)
    df["ind_pressure"] = df.mf * df.pc_mpa * np.exp((5.37 * (1. + df.omega) * (1. - df.tc_k / temperature)))
    return df.ind_pressure.sum() * (5.1185 * temperature ** (-0.356))


def get_bubble_point(well_conn, temperature):
    cce = fetch(well_conn, property="cce_cms_cme")
    cce = cce[cce.temperature == temperature]
    cce.sort_values(by=["rel_vol"], ascending=True)
    x1, x2, y1, y2 = straight_line_data(cce)
    separator = fetch(well_conn, property="seperator_used_sep_corrected")
    gd = fetch(well_conn, property="general_data")

    if cce.loc[cce.rel_vol == 1.].pressure.any():
        return cce.loc[cce.rel_vol == 1., "pressure"].values[0]
    elif len(x1) == 3 and len(x2) == 3 and len(y1) == 3 and len(y2) == 3:
        return fit_straight_line(x1, x2, y1, y2)
    elif separator.loc[(separator.stage == "Total at Bubble Point")
                       & ~(separator.pressure.isnull())
                       & (separator.temperature == temperature)].pressure.count() > 0:
        return separator.loc[(separator.stage == "Total at Bubble Point")
                             & ~(separator.pressure.isnull())
                             & (separator.temperature == temperature)].pressure.values[0]
    elif gd.loc[(gd.param == "pb_res_temp") & ~(gd.value.isnull()) | (gd.value == 0)].value.count() > 0\
            and gd.loc[(gd.param == "temp_res") & (gd.value == temperature)].shape[0] > 0:
        return gd.loc[(gd.param == "pb_res_temp") & ~(gd.value.isnull()) | (gd.value == 0)].value.values[0]
    else:
        return calc_bubble_point(well_conn, temperature)


if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection

    db = get_connection("AB8")
    test_user_args = pd.Series({"debug": False, "well_conn": db})
    print get_bubble_point(db, 395.372222222222)
