import numpy as np
import pandas as pd

from Scripts.pvt_new.core.constants import volume_solver_temperature as temperature, thermal_coefficient_bp
from Scripts.pvt_new.core.optimizers.poas_interpolator import monotone
from Scripts.pvt_new.core.pandas.dataframe import drop_not_in_cols
from Scripts.pvt_new.core.storage.fetch_most import validated_data
from Scripts.pvt_new.core.storage.mongo import fetch


def scales(dfplus):
    _s = {5000: [7, 8], 4500: [9, 12], 2000: [13, 16], 1000: [17, 25], 800: [26, 121]}
    for key, value in _s.items():
        dfplus.loc[(dfplus.index >= value[0]) & (dfplus.index <= value[1]), "scale"] = key


def final_vb(df):
    df["final_vb"] = df.vb
    _index = df.index.max() - 1
    if df.loc[df.index == _index, "ph_diff"].values[0] > 10 ** -12:
        x = df.loc[df.index < _index].tail(5).index.tolist()
        y = df.loc[df.index < _index].tail(5).vb.tolist()
        vb = monotone(x, y, [_index])[0]
        df.loc[df.index == _index, "final_vb"] = vb
    _index = df.index.max()
    if df.loc[df.index == _index, "ph_diff"].values[0] > 0.1:
        x = df.loc[df.index < _index - 1].tail(5).index.tolist()
        y = df.loc[df.index < _index - 1].tail(5).vb.tolist()
        vb = monotone(x, y, [_index])[0]
        vb_fcm = df.loc[df.index == _index, "vb_fcm"].values[0]
        vb = vb_fcm if vb < vb_fcm * 0.9 or vb > vb_fcm * 1.1 else vb
        df.loc[df.index == _index, "final_vb"] = vb


def createPlusFractions(df, well_conn):
    df_v, dfplus = validated_data(well_conn)
    drop_not_in_cols(dfplus, ["expdensity"])
    needed = [7, 12, 20]
    for need in needed:
        if need not in dfplus.index.tolist():
            sg = df.loc[df.scn >= need, "zimi"].sum() / df.loc[df.scn >= need, "zimibysgi"].sum()
            dfplus = pd.concat([dfplus, pd.DataFrame(data=[sg], index=[need], columns=["expdensity"])])
            dfplus.sort_index(inplace=True)
    return dfplus


def get_cm_data(well_conn):
    df = fetch(well_conn, property="pvt_output", identifier="fcm")
    needed = ["mf", "mass", "vb", "density"]
    drop_not_in_cols(df, needed)
    df.rename(columns={"vb": "vb_fcm"}, inplace=True)
    df["zimi"] = df.mass * df.mf
    df["zimibysgi"] = df.zimi / df.density
    return df


def get_poas_4_data(well_conn):
    df = fetch(well_conn, property="pvt_output", identifier="eosbp", eos="poas_4")
    needed = ["bp_final1", "vb", "ph_diff", "tc_k", "vc_final"]
    drop_not_in_cols(df, needed)
    return df


def observed(well_conn):
    df = get_cm_data(well_conn)
    poas_4 = get_poas_4_data(well_conn)
    df = pd.concat([df, poas_4], axis=1)
    df["scn"] = df.index
    df.reset_index(inplace=True)
    final_vb(df)
    df["density_bp"] = df.mass / df.final_vb
    df["thermal_expansion_bp"] = 613.97226 / (df.density_bp * 1000.) ** 2
    df["initial_density_60f"] = (
            df.density_bp * np.exp((-1. * df.thermal_expansion_bp * ((temperature - 273.15) -
                                                                     (df.bp_final1 - 273.15)) *
                                    (1. + thermal_coefficient_bp * df.thermal_expansion_bp *
                                     ((temperature - 273.15) - (df.bp_final1 - 273.15))))))
    df["initial_volume_60f"] = df.mass / df.initial_density_60f
    df["volume_slope"] = df.initial_volume_60f - df.initial_volume_60f.shift(1)
    df.loc[df.index == df.index.min(), "volume_slope"] = \
        df.loc[df.index == df.index.min() + 1, "volume_slope"].values[0]
    df["volume_correction"] = (4.81668632961298 * np.exp(0.0140674955555049 * df.scn))
    # create 7, 12 and 20
    dfplus = createPlusFractions(df, well_conn)
    scales(dfplus)
    return df, dfplus


"""
return one df with data input for volume solver tuning
"""

if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection

    well_name = "AB8"
    db = get_connection(well_name)
    test_df = observed(db)
    print test_df
