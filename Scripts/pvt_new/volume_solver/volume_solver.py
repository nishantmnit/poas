import numpy as np
from autograd import grad as gd

from Scripts.pvt_new.core.get_logger import get_logger
from Scripts.pvt_new.core.optimizers.ncg import ncg as ncg_solver
from calculated import calculated
from observed import observed
from process_output import process_output
from start_point import start_point
from var_names import var_names
from var_range import var_range
from Scripts.pvt_new.core.storage.save import save

logger = get_logger("pvt volume solver")


def _cost(df_o, dfplus_o, iter_vars):
    df = df_o.copy(deep=True)
    dfplus = dfplus_o.copy(deep=True)
    for i in range(len(iter_vars)):
        df[var_names[i]] = iter_vars[i]
    calculated_df, calculated_df_plus = calculated(df, dfplus)
    cost = calculated_df_plus.cost.sum()
    return cost


def _cost_for_ncg(iter_vars, args, only_cost=False):
    df, dfplus = args[0], args[1]
    cost = _cost(df, dfplus, iter_vars)
    if only_cost:
        return cost
    grad = gd(_cost, 2)
    derivative = np.array([i.item(0) for i in grad(df, dfplus, iter_vars)])
    return cost, derivative


def run_solver(df, dfplus, user_args):
    logger.info("Volume Solver started | start point = %s" % (",".join([str(x) for x in start_point])))
    ncg = ncg_solver()
    var_calc = ncg.minimize(start_point, var_range, _cost_for_ncg, max_iter=150,
                            debug=user_args.debug, args=(df, dfplus))
    logger.info("Volume Solver completed | end point = %s" % (",".join([str(x) for x in var_calc])))
    return var_calc


def volume_solver(user_args):
    observed_df, observed_df_plus = observed(user_args.well_conn)
    iter_vars = run_solver(observed_df, observed_df_plus, user_args)
    for i in range(len(iter_vars)):
        observed_df[var_names[i]] = iter_vars[i]
    calculated_df, calculated_df_plus = calculated(observed_df, observed_df_plus)
    save(process_output(calculated_df), user_args, identifier="volume_solver")


"""
save one df with volume solver tuned data
"""

if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection
    from Scripts.pvt_new.settings import DEBUG_DIR
    import os
    import pandas as pd

    well_name = "AB8"

    debug_dir = os.path.join(DEBUG_DIR, well_name)

    db = get_connection(well_name)
    test_user_args = pd.Series({"debug": True, "well_conn": db, "bias": "general",
                                "well_name": well_name, "debug_dir": debug_dir})
    volume_solver(test_user_args)
