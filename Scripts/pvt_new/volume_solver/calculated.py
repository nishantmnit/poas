def calculated(df, dfplus):
    df["volume_slope"] = df.volume_slope * df.volume_slope_multiplier
    df["volume_correction"] = df.volume_correction * df.volume_correction_multiplier
    df["volume_corrected"] = df.initial_volume_60f * (1. + df.volume_correction / 100.)
    vol_slope = []
    for index, row in df.loc[df.scn > 6].iterrows():
        if len(vol_slope) == 0:
            vol_slope.append(row.volume_slope + df.loc[df.scn == 6, "volume_corrected"].values[0])
        else:
            vol_slope.append(row.volume_slope + vol_slope[-1])
    df.loc[df.scn > 6, "volume_slope_for_diff"] = vol_slope
    df.loc[df.scn > 6, "volume_corrected"] = vol_slope * (1. + df.loc[df.scn > 6., "volume_correction"] / 100.)
    df.loc[df.scn > 6, "volume_corrected_final"] = df.loc[df.scn > 6].apply(
        lambda row1: 0.95 * row1.final_vb if row1.volume_corrected >= row1.final_vb else row1.volume_corrected, axis=1)
    df.loc[df.scn <= 6, "volume_corrected_final"] = df.loc[df.scn <= 6, "volume_corrected"]
    df["initial_density_60f"] = df.mass / df.volume_corrected_final
    df["sg_60f"] = df.initial_density_60f
    df["zimibysgi"] = df.zimi / df.sg_60f
    dfplus["density"] = dfplus.apply(
        lambda row1: df.loc[df.scn >= row1.name, "zimi"].sum() / df.loc[df.scn >= row1.name, "zimibysgi"].sum(),
        axis=1)
    dfplus["cost"] = dfplus.scale * ((dfplus.expdensity - dfplus.density) / dfplus.expdensity) ** 2
    return df, dfplus
