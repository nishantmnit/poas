import numpy as np

from Scripts.pvt_new.core.constants import thermal_coefficient_bp, volume_solver_temperature as temperature


def volume_bp_vc(df_o):
    df = df_o
    df["density_tc"] = df.mass / df.vc_final
    df["y1"] = np.log(df.density_tc / df.density_bp)
    df["a1"] = ((df.tc_k - 273.15) - (df.bp_final1 - 273.15))
    df["a2"] = thermal_coefficient_bp * df.a1
    df["a3"] = df.a1 * df.a2
    df["thermal_expansion_a"] = df.apply(
        lambda row: row.thermal_expansion_bp if (row.a1 ** 2 - 4. * row.a3 * row.y1) < 0 else (
                (-row.a1 + np.sqrt(row.a1 ** 2 - 4. * row.a3 * row.y1)) / (2. * row.a3)), axis=1)
    df["thermal_expansion_b"] = df.apply(
        lambda row: row.thermal_expansion_bp if (row.a1 ** 2 - 4. * row.a3 * row.y1) < 0 else (-row.a1 - np.sqrt(
            row.a1 ** 2 - 4. * row.a3 * row.y1)) / (2. * row.a3), axis=1)
    df["thermal_expansion_1"] = df[["thermal_expansion_a", "thermal_expansion_b"]].max(axis=1)
    df["thermal_expansion_2"] = df[["thermal_expansion_a", "thermal_expansion_b"]].min(axis=1)
    df["final_thermal_expansion"] = df.apply(
        lambda row: row.thermal_expansion_bp if row.thermal_expansion_a <= 0 and row.thermal_expansion_b <= 0
        else (row.thermal_expansion_1 if row.thermal_expansion_a <= 0 or row.thermal_expansion_b <= 0
              else row.thermal_expansion_2), axis=1)
    df_o["final_thermal_expansion_tc"] = df.final_thermal_expansion


def volume_below_bp(df_o):
    df = df_o
    df["density_60f"] = df.mass / df.volume_corrected_final
    df["y1"] = np.log(df.density_60f / df.density_bp)
    df["a1"] = ((temperature - 273.15) - (df.bp_final1 - 273.15))
    df["a2"] = thermal_coefficient_bp * df.a1
    df["a3"] = df.a1 * df.a2
    df["thermal_expansion_a"] = df.apply(
        lambda row: row.thermal_expansion_bp if (row.a1 ** 2 - 4. * row.a3 * row.y1) < 0 else (
                (-row.a1 + np.sqrt(row.a1 ** 2 - 4. * row.a3 * row.y1)) / (2. * row.a3)), axis=1)
    df["thermal_expansion_b"] = df.apply(
        lambda row: row.thermal_expansion_bp if (row.a1 ** 2 - 4. * row.a3 * row.y1) < 0 else (-row.a1 - np.sqrt(
            row.a1 ** 2 - 4. * row.a3 * row.y1)) / (2. * row.a3), axis=1)
    df["thermal_expansion_1"] = df[["thermal_expansion_a", "thermal_expansion_b"]].max(axis=1)
    df["thermal_expansion_2"] = df[["thermal_expansion_a", "thermal_expansion_b"]].min(axis=1)
    df["final_thermal_expansion"] = df.apply(
        lambda row: row.thermal_expansion_bp if row.thermal_expansion_2 <= 0 else row.thermal_expansion_2, axis=1)
    df.loc[df.final_thermal_expansion > 3. * df.thermal_expansion_bp, "final_thermal_expansion"] = \
        df.thermal_expansion_bp
    df_o["final_thermal_expansion_bp"] = df.final_thermal_expansion


def density(row, temp):
    if temp > row.bp_final1:
        return row.density_bp * \
               np.exp((-1 * row.final_thermal_expansion_tc *
                       ((temp - 273.15) - (row.bp_final1 - 273.15)) *
                       (1 + thermal_coefficient_bp * row.final_thermal_expansion_tc *
                        ((temp - 273.15) - (row.bp_final1 - 273.15)))))
    return row.density_bp * np.exp((-1 * row.final_thermal_expansion_bp *
                                    ((temp - 273.15) - (row.bp_final1 - 273.15)) *
                                    (1 + thermal_coefficient_bp * row.final_thermal_expansion_bp *
                                     ((temp - 273.15) - (row.bp_final1 - 273.15)))))


def volumes(df_o):
    df = df_o[["scn", "mf", "mass", "final_vb"]]
    temps = [200, 250, temperature, 300, 350, 400, 475, 550, 298.15]
    for temp in temps:
        df["density_%s" % temp] = df_o.apply(lambda row: density(row, temp), axis=1)
        df["volume_%s" % temp] = df.mass / df["density_%s" % temp]
    return df


def process_output(df):
    volume_below_bp(df)
    volume_bp_vc(df)
    df = volumes(df)
    return df
