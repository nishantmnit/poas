import numpy as np

from Scripts.pvt_new.core.get_logger import get_logger
from Scripts.pvt_new.core.optimizers.ncg import ncg
from Scripts.pvt_new.core.storage.save import save

logger = get_logger("pvt poas contingency density")

scales = [1., 5.]


def smooth(df_o, dfplus):
    df = df_o
    ls = df.index.max()
    if ls >= 80:
        fs = 40
    elif ls >= 50:
        fs = 30
    else:
        fs = 20
    ls = fs + 1
    slope = np.log(df.loc[df.index == ls, "density"].values[0]) - np.log(
        df.loc[df.index == fs, "density"].values[0])
    alphas, calphas, dalphas = list(), list(), list()
    d50 = np.log(df.loc[df.index == ls, "density"].values[0])
    for s_multiplier in [0.0001, 0.0005, 0.0025, 0.0125, 0.0625, 0.39375, 0.725, 1.05625, 1.3875, 1.71875, 2.05,
                         2.38125, 2.7125, 3.04375, 3.375, 3.70625, 4.0375, 4.36875, 4.7, 5.03125, 5.3625, 5.69375,
                         6.025, 6.35625, 6.6875, 7.01875, 7.35, 7.68125, 8.0125, 8.34375, 8.675, 9.00625, 9.3375,
                         9.66875, 10.]:
        df["density"] = df.apply(
            lambda row: row.density if row.name <= ls else np.exp(d50 + (row.name - ls) * (slope * s_multiplier)),
            axis=1)
        df["zimibysgi"] = df.zimi / df.density
        dfplus["density"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "zimi"].sum() / df.loc[
            df.index >= row.name, "zimibysgi"].sum(), axis=1)
        dfplus["error"] = (np.abs(dfplus.expdensity - dfplus.density) / dfplus.expdensity)
        alphas.append(s_multiplier)
        calphas.append(dfplus.loc[dfplus.index == dfplus.index.max(), "error"].values[0])
        dalphas.append(dfplus.loc[dfplus.index == dfplus.index.max(), "density"].values[0])

    m_index = calphas.index(min(calphas))

    if m_index == 0 or m_index == len(alphas) - 1:
        s_multiplier = alphas[m_index]
    else:
        index1 = max(0, m_index - 1)
        index2 = max(0, m_index - 2)
        index3 = min(len(alphas) - 1, m_index + 1)

        density1, density2, density3, density4 = dalphas[m_index], dalphas[index2], dalphas[index1], dalphas[index3]
        cost1, cost2, cost3, cost4 = alphas[m_index], alphas[index2], alphas[index1], alphas[index3]
        k1 = (density2 ** 3 - density1 ** 3) * (density3 - density1) - (density3 ** 3 - density1 ** 3) * (
                density2 - density1)
        k2 = (density2 ** 2 - density1 ** 2) * (density3 - density1) - (density3 ** 2 - density1 ** 2) * (
                density2 - density1)
        k3 = (density2 ** 3 - density1 ** 3) * (density4 - density1) - (density4 ** 3 - density1 ** 3) * (
                density2 - density1)
        k4 = (density2 ** 2 - density1 ** 2) * (density4 - density1) - (density4 ** 2 - density1 ** 2) * (
                density2 - density1)
        p1 = (cost2 - cost1) * (density3 - density1) - (cost3 - cost1) * (density2 - density1)
        p2 = (cost2 - cost1) * (density4 - density1) - (cost4 - cost1) * (density2 - density1)

        a = 0 if (k1 * k4 - k2 * k3) == 0 else (p1 * k4 - p2 * k2) / (k1 * k4 - k2 * k3)
        b = 0 if k2 == 0 else (p1 - a * k1) / k2
        c = 0 if (density2 - density1) == 0 else ((cost2 - cost1) - a * (density2 ** 3 - density1 ** 3) - b * (
                density2 ** 2 - density1 ** 2)) / (density2 - density1)
        d = cost1 - (a * density1 ** 3 + b * density1 ** 2 + c * density1)

        if a > 10 ** 50:
            a = 10 ** 50

        if b > 10 ** 50:
            b = 10 ** 50

        if c > 10 ** 50:
            c = 10 ** 50

        if d > 10 ** 50:
            d = 10 ** 50

        last_density = dfplus.loc[dfplus.index == dfplus.index.max(), "expdensity"].values[0]

        s_multiplier = a * last_density ** 3 + b * last_density ** 2 + c * last_density + d

    df["density"] = df.apply(
        lambda row: row.density if row.name <= ls else np.exp(d50 + (row.name - ls) * (slope * s_multiplier)),
        axis=1)
    df["zimibysgi"] = df.zimi / df.density
    dfplus["density"] = dfplus.apply(
        lambda row: df.loc[df.index >= row.name, "zimi"].sum() / df.loc[df.index >= row.name, "zimibysgi"].sum(),
        axis=1)

    return df, dfplus


def calculateDensity(iter_vars, df_o, dfplus):
    theta, a, b, c = iter_vars
    df = df_o
    df["density"] = theta - np.exp(a - b * df.index ** c)
    df["density_calc"] = df.density
    lid = df.loc[~df.expdensity.isnull()].index.max()
    lid = 19 if lid > 15 else 15
    df["error"] = df.apply(
        lambda row: 0 if row.POASDensity == 0 or row.name > lid or row.name <= lid - 3
        else ((row.POASDensity - row.density) / row.POASDensity) ** 2, axis=1)
    df["diff"] = df.apply(lambda row: 0 if row.name > lid or row.name <= lid - 2 else row.POASDensity - row.density,
                          axis=1)
    df.loc[df.index <= lid, "density"] = df.POASDensity
    df.loc[~df.expdensity.isnull(), "density"] = df.expdensity
    df["density_before_smoothing"] = df.density
    avg_diff = np.mean(df.loc[(df.index >= lid - 1) & (df.index <= lid), "diff"].tolist())
    df.loc[df.index > lid, "density"] = df.loc[df.index > lid].apply(
        lambda row: 0 if row.density == 0 else row.density + avg_diff, axis=1)
    df["zimibysgi"] = df.zimi / df.density
    dfplus["density"] = dfplus.apply(
        lambda row: df.loc[df.index >= row.name, "zimi"].sum() / df.loc[df.index >= row.name, "zimibysgi"].sum(),
        axis=1)
    dfplus["error"] = ((dfplus.expdensity - dfplus.density) / dfplus.expdensity) ** 2
    lid = lid
    return df, dfplus


def _sp():
    return [1.1, 1., 3., 0.089]


def _range():
    return [[1., 2.], [1., 4.], [1., 4.], [0.084, 1.]]


def _gradient(iter_vars, df, dfplus, lid):
    df["density_wrt1"] = df.apply(lambda row: 0 if row.name <= lid else 1, axis=1)
    df["density_wrt2"] = df.apply(
        lambda row: 0 if row.name <= lid else (
                -1. * np.exp(iter_vars[1] - iter_vars[2] * row.name ** iter_vars[3])), axis=1)
    df["density_wrt3"] = df.apply(lambda row: 0 if row.name <= lid else (
            row.name ** iter_vars[3] * np.exp(iter_vars[1] - iter_vars[2] * row.name ** iter_vars[3])), axis=1)
    df["density_wrt4"] = df.apply(lambda row: 0 if row.name <= lid else (
            iter_vars[2] * np.log(row.name) * row.name ** iter_vars[3] *
            np.exp(iter_vars[1] - iter_vars[2] * row.name ** iter_vars[3])), axis=1)

    df["zimibysgi_wrt1"] = -1. * df.zimi / df.density_before_smoothing ** 2 * df.density_wrt1
    df["zimibysgi_wrt2"] = -1. * df.zimi / df.density_before_smoothing ** 2 * df.density_wrt2
    df["zimibysgi_wrt3"] = -1. * df.zimi / df.density_before_smoothing ** 2 * df.density_wrt3
    df["zimibysgi_wrt4"] = -1. * df.zimi / df.density_before_smoothing ** 2 * df.density_wrt4

    df["obj_wrt1"] = df.apply(
        lambda row: 0 if row.POASDensity == 0 or row.name > lid or row.name <= lid - 3
        else ((2. * (row.POASDensity - row.density_calc) / row.POASDensity ** 2) * -1.), axis=1) * scales[1] / df.target
    df["obj_wrt2"] = df.apply(
        lambda row: 0 if row.POASDensity == 0 or row.name > lid or row.name <= lid - 3 else (
                (2. * (row.POASDensity - row.density_calc) / row.POASDensity ** 2) * -1. *
                (-1 * np.exp(iter_vars[1] - iter_vars[2] * row.name ** iter_vars[3]))), axis=1) * scales[1] / df.target
    df["obj_wrt3"] = df.apply(
        lambda row: 0 if row.POASDensity == 0 or row.name > lid or row.name <= lid - 3 else (
                (2. * (row.POASDensity - row.density_calc) / row.POASDensity ** 2) * -1. *
                (row.name ** iter_vars[3] * np.exp(iter_vars[1] - iter_vars[2] *
                                                   row.name ** iter_vars[3]))), axis=1) * scales[1] / df.target
    df["obj_wrt4"] = df.apply(
        lambda row: 0 if row.POASDensity == 0 or row.name > lid or row.name <= lid - 3 else (
                (2. * (row.POASDensity - row.density_calc) / row.POASDensity ** 2) * -1. *
                (iter_vars[2] * np.log(row.name) * row.name ** iter_vars[3] *
                 np.exp(iter_vars[1] - iter_vars[2] * row.name ** iter_vars[3]))), axis=1) * scales[1] / df.target

    dfplus["density_wrt1"] = dfplus.apply(lambda row: (-1. * df.loc[df.index >= row.name, "zimi"].sum() / df.loc[
        df.index >= row.name, "zimibysgi"].sum() ** 2) * df.loc[df.index >= row.name, "zimibysgi_wrt1"].sum(),
                                          axis=1)
    dfplus["density_wrt2"] = dfplus.apply(lambda row: (-1. * df.loc[df.index >= row.name, "zimi"].sum() / df.loc[
        df.index >= row.name, "zimibysgi"].sum() ** 2) * df.loc[df.index >= row.name, "zimibysgi_wrt2"].sum(),
                                          axis=1)
    dfplus["density_wrt3"] = dfplus.apply(lambda row: (-1. * df.loc[df.index >= row.name, "zimi"].sum() / df.loc[
        df.index >= row.name, "zimibysgi"].sum() ** 2) * df.loc[df.index >= row.name, "zimibysgi_wrt3"].sum(),
                                          axis=1)
    dfplus["density_wrt4"] = dfplus.apply(lambda row: (-1. * df.loc[df.index >= row.name, "zimi"].sum() / df.loc[
        df.index >= row.name, "zimibysgi"].sum() ** 2) * df.loc[df.index >= row.name, "zimibysgi_wrt4"].sum(),
                                          axis=1)

    dfplus["obj_wrt1"] = (scales[0] * (2. * (dfplus.expdensity - dfplus.density) / dfplus.expdensity ** 2) * (
        -1.) * dfplus.density_wrt1) / dfplus.target
    dfplus["obj_wrt2"] = (scales[0] * (2. * (dfplus.expdensity - dfplus.density) / dfplus.expdensity ** 2) * (
        -1.) * dfplus.density_wrt2) / dfplus.target
    dfplus["obj_wrt3"] = (scales[0] * (2. * (dfplus.expdensity - dfplus.density) / dfplus.expdensity ** 2) * (
        -1.) * dfplus.density_wrt3) / dfplus.target
    dfplus["obj_wrt4"] = (scales[0] * (2. * (dfplus.expdensity - dfplus.density) / dfplus.expdensity ** 2) * (
        -1.) * dfplus.density_wrt4) / dfplus.target

    gradients = np.array([df.obj_wrt1.sum() + dfplus.obj_wrt1.sum(), df.obj_wrt2.sum() + dfplus.obj_wrt2.sum(),
                          df.obj_wrt3.sum() + dfplus.obj_wrt3.sum(), df.obj_wrt4.sum() + dfplus.obj_wrt4.sum()])
    return gradients


def _cost(iter_vars, df_o, dfplus):
    df, dfplus = calculateDensity(iter_vars, df_o, dfplus)
    return (df.error / df.target).sum() * scales[1] + (dfplus.error / dfplus.target).sum() * scales[0], df, dfplus


def _cost_for_ncg(iter_vars, args, only_cost=False):
    df_o, dfplus, lid = args[0], args[1], args[2]
    df = df_o.copy(deep=True)
    cost, df, dfplus = _cost(iter_vars, df, dfplus)
    if only_cost:
        return cost
    grad = _gradient(iter_vars, df, dfplus, lid)
    return cost, grad


def process_result(iter_vars, df, dfplus, user_args):
    df, dfplus = calculateDensity(iter_vars, df, dfplus)
    dfplus["diff"] = (np.abs(dfplus.expdensity - dfplus.density) / dfplus.expdensity) * 100.
    if dfplus.loc[dfplus.index == dfplus.index.max(), "diff"].values[0] > 0.5:
        df, dfplus = smooth(df, dfplus)
    save(df, user_args, identifier="density", plus_or_individual="individual", solver=1, distribution="poas_c")
    save(dfplus, user_args, identifier="density", plus_or_individual="plus", solver=1, distribution="poas_c")
    return True


def poas_c(df, dfplus, user_args, lid):
    logger.info("Executing POAS Density Contingency Finder.")
    dfplus["target"] = 0.001
    df["target"] = 0.001
    if dfplus.shape[0] > 2:
        dfplus.loc[(dfplus.index == dfplus.index.min()) | (dfplus.index == dfplus.index.max()), "target"] = 0.0001
    ncg_solver = ncg()
    iter_vars = ncg_solver.minimize(_sp(), _range(), _cost_for_ncg, max_iter=200,
                                    debug=user_args.debug, args=(df, dfplus, lid))
    logger.info(
        "POAS Density Contingency solver final variables - %s" % (",".join([str(x) for x in iter_vars])))
    return process_result(iter_vars, df, dfplus, user_args)


"""
executed from poas.py if results are not reliable
"""
