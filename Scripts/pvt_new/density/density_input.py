from Scripts.pvt_new.core.get_logger import get_logger
from Scripts.pvt_new.core.storage.fetch_most import recommended, validated_data
from Scripts.pvt_new.core.storage.mongo import fetch
from Scripts.pvt_new.validation.validate import average

logger = get_logger("pvt density input")


def density_input(well_conn):
    try:
        df_o, dfplus_o = validated_data(well_conn)
        df_o = average(df_o, df_o.index.min())

        df, dfplus = recommended(well_conn, "mass")
        df["expdensity"] = df_o.expdensity
        dfplus["expdensity"] = dfplus_o.expdensity
        df["zimi"] = df.mass * df.mf
        df["xw"] = df.zimi / df.zimi.sum()
        dfplus["mf"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "mf"].sum(), axis=1)
        dfplus["mass"] = dfplus.apply(
            lambda row: df.loc[df.index >= row.name, "zimi"].sum() / df.loc[df.index >= row.name, "mf"].sum(),
            axis=1)
        dfplus.drop(dfplus.loc[dfplus.expdensity.isnull()].index, inplace=True)
        df.drop(df.loc[df.index < 6].index, inplace=True)
    except Exception as e:
        logger.error('Error:{"Type": "Initialize", "Message": "Failed to '
                     'initialize density solver. Please run mass solver first."}')
        raise e
    return df, dfplus


if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection

    db = get_connection("ALUMM")
    print density_input(db)
