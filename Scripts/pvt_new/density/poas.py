import numpy as np
from scipy import optimize

from Scripts.pvt_new.core.get_logger import get_logger
from Scripts.pvt_new.density.density_input import density_input
from poas_c import poas_c
from Scripts.pvt_new.core.storage.save import save

logger = get_logger("pvt poas density")


def aliphaticDensity(scn):
    # from scn 6 to 121
    all_densities = [0.6497773095056900, 0.6765171765303820, 0.6970152608650540, 0.7132531517677730,
                     0.7264494970774210, 0.7373960330624230, 0.7466300599078980, 0.7545293850206410,
                     0.7613676163109280, 0.7673478940701880, 0.7726242864728250, 0.7773158179807860,
                     0.7815159342163050, 0.7852990490227780, 0.7887251735941340, 0.7918432537654240,
                     0.7946936180955970,
                     0.7973098018897410, 0.7997199255265320, 0.8019477494038700, 0.8040134908503150,
                     0.8059344635078030, 0.8077255827063110, 0.8093997685539040, 0.8109682701518690,
                     0.8124409284067970,
                     0.8138263906178320, 0.8151322868765220, 0.8163653759946470, 0.8175316669413190,
                     0.8186365204635100, 0.8196847345702580, 0.8206806167987170, 0.8216280455913880,
                     0.8225305226555910,
                     0.8233912178170610, 0.8242130075961990, 0.8249985085106260, 0.8257501059280980,
                     0.8264699791497080, 0.8271601232868710, 0.8278223684012470, 0.8284583962997460,
                     0.8290697553137550,
                     0.8296578733399000, 0.8302240693768070, 0.8307695637568260, 0.8312954872421090,
                     0.8318028891297480, 0.8322927444899020, 0.8327659606434620, 0.8332233829710510,
                     0.8336658001326770,
                     0.8340939487667980, 0.8345085177284950, 0.8349101519187600, 0.8352994557502730,
                     0.8356769962894100, 0.8360433061092730, 0.8363988858843640, 0.8367442067538340,
                     0.8370797124771030,
                     0.8374058214028500, 0.8377229282700120, 0.8380314058573000, 0.8383316064959330,
                     0.8386238634586640, 0.8389084922367630, 0.8391857917153940, 0.8394560452567010,
                     0.8397195216989740,
                     0.8399764762793980, 0.8402271514871360, 0.8404717778528010, 0.8407105746798270,
                     0.8409437507226370, 0.8411715048161190, 0.8413940264604140, 0.8416114963647060,
                     0.8418240869533260,
                     0.8420319628371830, 0.8422352812532810, 0.8424341924748080, 0.8426288401940800,
                     0.8428193618804240, 0.8430058891148750, 0.8431885479034590, 0.8433674589706100,
                     0.8435427380342070,
                     0.8437144960635450, 0.8438828395214760, 0.8440478705918410, 0.8442096873932230,
                     0.8443683841799760, 0.8445240515314150, 0.8446767765299570, 0.8448266429289770,
                     0.8449737313110580,
                     0.8451181192372660, 0.8452598813880610, 0.8453990896963560, 0.8455358134732600,
                     0.8456701195269530, 0.8458020722751300, 0.8459317338514250, 0.8460591642061800,
                     0.8461844212019150,
                     0.8463075607038090, 0.8464286366655130, 0.8465477012105560, 0.8466648047096160,
                     0.8467799958539030, 0.8468933217248660, 0.8470048278604530, 0.8471145583181170,
                     0.8472225557347470]
    return all_densities[scn - 6]


def targetCost(val, target):
    return val / target


def perChange(scn):
    return 5.29314130144534E-06 * scn ** 3 - 0.00111243172535057 * \
           scn ** 2 + 0.0772646126408345 * scn + 2.64506454446415


def baseDensity(scn, lid):
    if scn <= max(25, lid + 5):
        if scn <= 33:
            return (
                    -7.27411968529E-09 * scn ** 6 + 9.8804076628049E-07 * scn ** 5 - 0.0000544578952353303 *
                    scn ** 4 + 0.00156130415898253 * scn ** 3 - 0.0247730420179834 * scn ** 2 +
                    0.21511514278639 * scn - 0.0080429446478434)
        else:
            return 0.686069942338086 * scn ** 0.0814141102460385
    else:
        return 0


def finalDensity(df, dfplus, lid):
    df["density"] = df.apply(lambda row: row.POASDensity if np.isnan(row.expdensity) else row.expdensity, axis=1)
    points = [max(19, lid), max(19, lid) + 11, max(19, lid) + 16]
    points = [float(x) for x in points]
    densities = df.loc[df.index.isin(points), "density"].values.tolist()
    params = [1. / (points[2] - points[0]) * (
            (densities[2] - densities[1]) / (points[2] - points[1]) -
            (densities[1] - densities[0]) / (points[1] - points[0]))]
    params = params + [
        (densities[1] - densities[0]) / (points[1] - points[0]) - params[0] * (points[1] + points[0])]
    params = params + [(densities[0] - params[1] * points[0] - params[0] * points[0] ** 2)]
    df["density"] = df.apply(
        lambda row: params[0] * row.name ** 2 + params[1] * row.name + params[2]
        if points[0] < row.name < points[2] else row.density, axis=1)
    df["zimibysgi"] = df.zimi / df.density
    dfplus["density"] = dfplus.apply(
        lambda row: df.loc[df.index >= row.name, "zimi"].sum() / df.loc[df.index >= row.name, "zimibysgi"].sum(),
        axis=1)
    return df, dfplus


def density1(scn, c_multiplier, lid):
    return baseDensity(scn, lid) + perChange(scn) * c_multiplier / 100.


def avgDensity(c_multiplier, lid):
    d, l, s = list(), [], list()
    for scn in range(max(lid - 1, 19), max(25, lid + 5)):
        d.append(density1(scn, c_multiplier, lid))
        l.append(np.log(d[-1]))
        if len(d) > 1:
            s.append((l[-1] - l[-2]) / (np.log(scn) - np.log(scn - 1)))
    return np.mean(s)


def POASDensity(df, c_multiplier, s_multiplier, lid):
    density = list()
    last_scn = min(122, df.index.max() + 1)
    for scn in range(6, last_scn):
        if scn <= max(20, lid):
            density.append(density1(scn, c_multiplier, lid))
        elif scn <= max(lid + 4, 24):
            avg_density = avgDensity(c_multiplier, lid)
            density.append(np.exp(avg_density * (np.log(scn) - np.log(scn - 1)) + np.log(density[-1])))
        else:
            slope = (np.log(density[max(lid, 20) + 4 - 6]) - np.log(
                density[max(lid, 20) + 4 - 1 - 6])) * s_multiplier
            density.append(np.exp(slope + np.log(density[-1])))
        if density[-1] < aliphaticDensity(scn):
            density[-1] = aliphaticDensity(scn)
    df.loc[df.index >= 6, "POASDensity"] = density
    mr = df.loc[df.index == df.index.max(), "mass"].values[0] / df.loc[df.index == df.index.max() - 1, "mass"].values[0]
    dr = 1.02 if mr <= 1.02 else (1.05 if mr > 1.05 else mr)
    df.loc[df.index == df.index.max(), "POASDensity"] = \
        df.loc[df.index == df.index.max() - 1, "POASDensity"].values[0] * dr
    return df


def calculateDensity(iter_vars, df_o, dfplus, lid):
    df = df_o.copy(deep=True)
    df = POASDensity(df, iter_vars[0], iter_vars[1], lid)
    df, dfplus = finalDensity(df, dfplus, lid)
    return df, dfplus


def min_density_constraint(iter_vars, df_o, dfplus, lid):
    df = df_o.copy(deep=True)
    df = POASDensity(df, iter_vars[0], iter_vars[1], lid)
    df, dfplus = finalDensity(df, dfplus, lid)
    min_density = min((0.1172 * np.log(dfplus.index) + 0.3754).tolist())
    return df.loc[df.index == df.index.max(), "density"].values[0] - min_density


def max_density_constraint(iter_vars, df_o, dfplus, lid):
    df = df_o.copy(deep=True)
    df = POASDensity(df, iter_vars[0], iter_vars[1], lid)
    df, dfplus = finalDensity(df, dfplus, lid)
    max_density = max(
        (
            dfplus.apply(
                lambda row: 1.3 * ((-9.259953526186E-08 * row.name ** 4 - 5.35267342716139E-06 *
                                    row.name ** 3 + 0.000735231107221068 * row.name ** 2 -
                                    0.024352285572931 * row.name + 1.55207500562452) if row.name <= 40
                                   else (-0.111023831942787 * np.log(row.name) + 1.63490295684644)) * row.expdensity,
                axis=1)).tolist())
    return max_density - df.loc[df.index == df.index.max(), "density"].values[0]


def get_constraints(df, dfplus, lid):
    constraints = list()
    constraints.append({'type': 'ineq', 'fun': min_density_constraint, 'args': [df, dfplus, lid]})
    constraints.append({'type': 'ineq', 'fun': max_density_constraint, 'args': [df, dfplus, lid]})
    return constraints


def cost_function(iter_vars, df_o, dfplus, lid):
    df, dfplus = calculateDensity(iter_vars, df_o, dfplus, lid)
    df["error"] = np.abs(df.expdensity - df.POASDensity) / df.expdensity
    dfplus["error"] = ((dfplus.expdensity - dfplus.density) / dfplus.expdensity) ** 2
    error = targetCost(df.loc[df.index >= 7, "error"].sum(), 0.01)
    for index, row in dfplus.iterrows():
        error += targetCost(row.error, 0.001)
    if not np.isnan(lid) and lid != 0:
        error += targetCost(df.loc[df.index > lid - 3, "error"].sum(), 0.01)
    return error


def process_result(res, df, dfplus, user_args, lid):
    df, dfplus = calculateDensity(res.x, df, dfplus, lid)
    save(df, user_args, identifier="density", plus_or_individual="individual", solver=1, distribution="poas")
    save(dfplus, user_args, identifier="density", plus_or_individual="plus", solver=1, distribution="poas")
    return df, dfplus


def poas(user_args):
    df, dfplus = density_input(user_args.well_conn)
    logger.info("Executing POAS Density Finder.")
    lid = df.loc[~df.expdensity.isnull()].index.max()
    lid = 0 if np.isnan(lid) else lid
    if dfplus.index.max() <= 16:
        res = optimize.minimize(cost_function, np.array([0., 1.]), bounds=[[-3., 6.], [.0005, 6.]],
                                method="SLSQP", args=(df, dfplus, lid), constraints=get_constraints(df, dfplus, lid),
                                options={'maxiter': 500})
    else:
        res = optimize.minimize(cost_function, np.array([0., 1.]), bounds=[[-3., 6.], [.0005, 6.]],
                                method="SLSQP", args=(df, dfplus, lid), options={'maxiter': 500})
    if not res.success:
        logger.debug(res)
        print 'Error:{"Type": "Density Solver", "Message": "POAS Density Solver failed to find a ' \
              'feasible solution. Please check input data. If issue persists, ' \
              'please raise with POAS support team."}'
        raise Exception("poas density finder failed.")

    # save main solver results
    df, dfplus = process_result(res, df, dfplus, user_args, lid)
    logger.info("POAS Density solver final variables - %s" % (",".join([str(x) for x in res.x])))
    logger.info("Completed POAS Density Solver.")

    if res.x[1] <= 0.5:
        logger.info(
            "POAS Density solver results not reliable. Executing Contingency Solver. Var2 = %s" % (res.x[1]))
        df, dfplus = calculateDensity(res.x, df, dfplus, lid)
        return poas_c(df, dfplus, user_args, lid)


"""
executed for all inputs
"""

if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection
    import pandas as pd
    from Scripts.pvt_new.settings import DEBUG_DIR
    import os
    from Scripts.pvt_new.core.storage.fetch_most import set_recommended

    well_name = "ASPHALTENE10"

    debug_dir = os.path.join(DEBUG_DIR, well_name)

    db = get_connection(well_name)
    test_user_args = pd.Series({"debug": True, "well_conn": db, "bias": "general", "debug_dir": debug_dir})
    poas(test_user_args)
    set_recommended(test_user_args.well_conn, "density")
