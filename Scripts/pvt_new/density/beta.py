import numpy as np
import pandas as pd
from scipy import optimize

from Scripts.pvt_new.core.get_logger import get_logger
from Scripts.pvt_new.core.storage.mongo import fetch
from Scripts.pvt_new.core.storage.save import save
from Scripts.pvt_new.density.density_input import density_input
from Scripts.pvt_new.mass.beta.common import calcDensity

logger = get_logger("pvt beta density")


def getPoasDensity(df, dfplus, user_args):
    df_p = fetch(user_args.well_conn, identifier="density", plus_or_individual="individual", solver=1,
                 distribution=["poas", "poas_c"], poas_recommended=1)
    dfplus_p = fetch(user_args.well_conn, identifier="density", plus_or_individual="plus", solver=1,
                     distribution=["poas", "poas_c"], poas_recommended=1)
    df["poas_density"] = df_p.density
    dfplus["poas_density"] = dfplus_p.density
    return df, dfplus


def targetCost(val, target):
    return val / target


def definePlusFractions(df, dfplus):
    desired = [7, 12, 20, 25, 30, 36] + dfplus.index.tolist() + [max(7, df.index.min())]
    desired = list(set(desired))
    desired = [scn for scn in desired if scn >= max(7, df.index.min())]
    desired.sort()
    df["zimi"] = df.mass * df.mf
    df["zimibysgi"] = df.zimi / df.poas_density
    dfplus_n = pd.DataFrame(index=desired, columns=["mf", "mass", "expdensity", "poas_density"])
    dfplus_n[["mf", "mass", "expdensity", "poas_density"]] = dfplus[["mf", "mass", "expdensity", "poas_density"]]
    dfplus_n["mf"] = dfplus_n.apply(lambda row: df.loc[df.index >= row.name, "mf"].sum(), axis=1)
    dfplus_n["mass"] = dfplus_n.apply(lambda row: df.loc[df.index >= row.name, "zimi"].sum() / row.mf, axis=1)
    dfplus_n["expdensity"] = dfplus_n.apply(lambda row: df.loc[df.index >= row.name, "zimi"].sum() / df.loc[
        df.index >= row.name, "zimibysgi"].sum() if np.isnan(row.expdensity) else row.expdensity, axis=1)
    dfplus_n["poas_density"] = dfplus_n.apply(lambda row: df.loc[df.index >= row.name, "zimi"].sum() / df.loc[
        df.index >= row.name, "zimibysgi"].sum() if np.isnan(row.poas_density) else row.poas_density, axis=1)
    return dfplus_n


class DensitySolver1:
    def __init__(self):
        self.beta = None
        self.alpha = None
        self.eta = None

    def optimize(self, user_args):
        logger.info("Executing Beta Density Solver 1")
        df, dfplus = density_input(user_args.well_conn)
        df, dfplus = getPoasDensity(df, dfplus, user_args)
        sp, bounds = self.startPoint(df)
        res = optimize.minimize(self.cost_function, np.array(sp), bounds=bounds, method="SLSQP", args=(df, dfplus),
                                options={'maxiter': 500})
        if not res.success:
            logger.debug(res)
            logger.info("Beta Density Solver failed to find a feasible solution.")
            res.x = [1., 1., 1., 1.]
        logger.info("Completed Beta Density Solver 1")
        logger.info(
            "Beta Density solver final variables - %s, objective - %s" % (",".join([str(x) for x in res.x]), res.fun))
        return self.process_result(res, df, dfplus, user_args)

    def startPoint(self, df_o):
        df = df_o
        df["zimi"] = df.mass * df.mf
        df["xwi"] = df.zimi / df.zimi.sum()
        df["xwsgi"] = df.poas_density * df.xwi
        df["af"] = df.xwi * (df.xwsgi.sum() - df.poas_density) ** 2
        df["skewness"] = df.xwi * ((df.xwsgi.sum() - df.poas_density) / np.sqrt(df.af.sum())) ** 3
        variance = df.af.sum()
        s_mean = df.xwsgi.sum()
        skewness = df.skewness.sum()
        eta = min(0.6, df.loc[df.index == 6, "poas_density"].values[0] * 0.98)

        b_max = 3.
        d1 = (b_max - eta) / (s_mean - eta)
        alpha = np.abs(
            -1. / d1 ** 2 * (2. * d1 + (2. - d1) * 2. * (b_max - eta) / (np.abs(skewness) * np.sqrt(variance))))
        beta_value = alpha * (b_max - s_mean) / (s_mean - eta)

        # mass boundary, alpha, beta, eta
        self.eta, self.alpha, self.beta = eta, alpha, beta_value
        df_o["db"] = (df_o.poas_density + df_o.poas_density.shift(-1)) / 2.
        bounds = [[0.98, 1.2], [0.2, 3.], [0.2, 3], [0.4, (df_o.db.min() * 0.98 * (1. - 0.01 / 100.)) / eta]]
        return [1., 1., 1., 1.], bounds

    def process_result(self, res, df, dfplus, user_args):
        df, dfplus = self.calculateDensities(res.x, df, dfplus)
        save(df, user_args, identifier="density", plus_or_individual="individual", solver=1, distribution="beta")
        save(dfplus, user_args, identifier="density", plus_or_individual="plus", solver=1, distribution="beta")
        return True

    def cost_function(self, i_vars, df_o, dfplus_o):
        df, dfplus = df_o.copy(deep=True), dfplus_o.copy(deep=True)
        df, dfplus = self.calculateDensities(i_vars, df, dfplus)
        dfplus["error"] = np.abs((dfplus.expdensity - dfplus.density) / dfplus.expdensity) / 0.01
        error = dfplus.error.sum() + df.loc[df.index > 6].error.sum() + df.error.head(1).values[0] / 0.01
        lid = df.loc[~df.expdensity.isnull()].index.max()
        if not np.isnan(lid):
            df["exp_error"] = np.abs((df.expdensity - df.density_Beta) / df.expdensity)
            error += df.exp_error.sum() / 0.05
            error += df.loc[df.index >= lid - 2, "exp_error"].sum() / 0.01
        return error

    def calculateDensities(self, i_vars, df, dfplus):
        db_multi, alpha_multi, beta_multi, eta_multi = i_vars
        alpha, beta_value, eta = self.alpha * alpha_multi, self.beta * beta_multi, self.eta * eta_multi
        # density calculations
        df["db"] = df.db * db_multi
        df.loc[df.index == df.index.max(), "db"] = max(2.8,
                                                       df.loc[df.index == df.index.max(), "poas_density"].values[0])
        df = calcDensity(df, alpha, eta, beta_value)

        df["error"] = np.abs((df.poas_density - df.density) / df.poas_density)
        df.loc[~df.expdensity.isnull(), "density"] = df.expdensity

        # smoothing
        df["diff"] = df.expdensity - df.density
        lid = df.loc[~df.expdensity.isnull()].index.max()
        if not np.isnan(lid) and lid >= 9:
            avg_diff = np.mean(df.loc[(df.index >= lid - 1) & (df.index <= lid), "diff"].tolist())
            df.loc[df.index > lid, "density"] = df.loc[df.index > lid].apply(
                lambda row: 0 if row.density == 0 else row.density + avg_diff, axis=1)

        df["zimi"] = df.mass * df.mf
        df["zimibysgi"] = df.apply(lambda row: 0 if row.density == 0 else row.zimi / row.density, axis=1)
        dfplus["density"] = dfplus.apply(
            lambda row: df.loc[df.index >= row.name, "zimi"].sum() / df.loc[df.index >= row.name, "zimibysgi"].sum(),
            axis=1)
        return df, dfplus


def beta(user_args):
    obj = DensitySolver1()
    obj.optimize(user_args)


"""
executed for all inputs
"""

if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection
    from Scripts.pvt_new.settings import DEBUG_DIR
    import os

    well_name = "ALUMM"

    debug_dir = os.path.join(DEBUG_DIR, well_name)

    db = get_connection(well_name)
    test_user_args = pd.Series({"debug": True, "well_conn": db, "bias": "general", "debug_dir": debug_dir})
    beta(test_user_args)
