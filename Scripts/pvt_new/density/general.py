import numpy as np
from scipy import optimize

from Scripts.pvt_new.core.get_logger import get_logger
from Scripts.pvt_new.density.density_input import density_input
from Scripts.pvt_new.core.storage.save import save
from Scripts.pvt_new.core.storage.mongo import fetch
from scipy.special import gammainc, gamma

logger = get_logger("pvt general density")


def getPoasDensity(df, dfplus, user_args):
    df_p = fetch(user_args.well_conn, identifier="density", plus_or_individual="individual", solver=1,
                 distribution=["poas", "poas_c"], poas_recommended=1)
    dfplus_p = fetch(user_args.well_conn, identifier="density", plus_or_individual="plus", solver=1,
                     distribution=["poas", "poas_c"], poas_recommended=1)
    df["poas_density"] = df_p.density
    dfplus["poas_density"] = dfplus_p.density
    return df, dfplus


def targetCost(val, target):
    return val / target


def calculateA(iter_vars, df):
    avgDensity = (df.poas_density * df.xw).sum()
    pstar_avg = (avgDensity - iter_vars[1]) / iter_vars[1]
    A = iter_vars[0] * (pstar_avg / (gamma(1. + 1. / iter_vars[0]))) ** iter_vars[0]
    return A


def calculateC6Density(df_o, dfplus):
    df = df_o
    df["zimi"] = df.mass * df.mf
    dfplus["zimi"] = dfplus.mass * dfplus.mf
    df["expdensity"] = df.apply(lambda row: row.poas_density if np.isnan(
        row.expdensity) and row.name < dfplus.index.max() else row.expdensity, axis=1)
    df["zimibysgi"] = df.mass * df.mf / df.expdensity
    dfplus["zimibysgi"] = dfplus.mass * dfplus.mf / dfplus.poas_density
    return (df.loc[(df.index >= 6) & (df.index < dfplus.index.max()), "zimi"].sum() +
            dfplus.loc[dfplus.index == dfplus.index.max(), "zimi"].values[0]) / \
           (df.loc[(df.index >= 6) & (df.index < dfplus.index.max()), "zimibysgi"].sum() +
            dfplus.loc[dfplus.index == dfplus.index.max(), "zimibysgi"].values[0])


def calculateDensities(iter_vars, df_o, dfplus_o):
    b, sg0 = iter_vars
    df, dfplus = df_o.copy(deep=True), dfplus_o.copy(deep=True)
    a = calculateA(iter_vars, df)
    df["xwcumm"] = df.xw.cumsum()
    df["xwcumm_1"] = df.apply(lambda row: 10 ** -20 if 1. - row.xwcumm == 0 else 1. - row.xwcumm, axis=1)
    df["qi"] = -1. * np.log(1. - df.xwcumm)
    df["xwi_inside"] = df.xwcumm_1.shift(1) - df.xwcumm_1
    df.loc[df.index == df.index.min(), "xwi_inside"] = 1. - df.loc[df.index == df.index.min(), "xwcumm_1"].values[0]
    df["incg"] = df.apply(lambda row: gammainc(1 + 1 / b, row.qi) * gamma(1 + 1 / b), axis=1)
    df['incgdiff'] = df['incg'].sub(df['incg'].shift())
    df.incgdiff = df.incgdiff.fillna(df['incg'].iloc[0])
    df["pav"] = (1 / df.xwi_inside.values) * (a / b) ** (1 / b) * df.incgdiff.values
    df["pav"] = df.apply(lambda row: 0 if row.xwi_inside == 0 else row.pav, axis=1)
    df["density"] = df.apply(lambda row: 0 if row.pav == 0 else iter_vars[1] * (1. + row.pav), axis=1)
    df.loc[df.density <= iter_vars[1], "density"] = df.poas_density
    df["exp_error"] = np.abs(df.density - df.expdensity) / df.expdensity
    df["diff"] = df.expdensity - df.density
    df["error"] = df.apply(
        lambda row: 0 if row.name <= 6 else np.abs(row.poas_density - row.density) / row.poas_density, axis=1)
    df["density"] = df.apply(lambda row: row.expdensity if not np.isnan(row.expdensity) else row.density, axis=1)

    # poonch fix
    df["slope"] = (np.log(df.density) - np.log(df.density.shift(1))) / (np.log(df.index) - np.log(df.index - 1))
    df["slope"] = df.apply(lambda row: 10 ** -20 if row.slope == 0 or np.isnan(row.slope) else row.slope, axis=1)
    avg_slope = np.mean(df["slope"].tail(12).head(2))
    fs = df.tail(12).head(1).index.values[0]
    fd = df["density"].tail(12).head(1).values[0]
    df.loc[df.index > fs, "density"] = np.exp(
        np.log(fd) + avg_slope * (np.log(df.loc[df.index > fs].index) - np.log(fs)))
    df.loc[df.index > fs, "density"] = df.loc[df.index > fs].apply(
        lambda row: 0 if np.isnan(row.density) else row.density, axis=1)
    mr = df.loc[df.index == df.index.max(), "mass"].values[0] / df.loc[df.index == df.index.max() - 1, "mass"].values[0]
    dr = 1.03 if mr <= 1.03 else (1.1 if mr > 1.1 else mr)
    df.loc[df.index == df.index.max(), "density"] = df.loc[df.index == df.index.max() - 1, "density"].values[0] * dr
    lid = df.loc[~df.expdensity.isnull()].index.max()
    if not np.isnan(lid) and lid >= 9:
        avg_diff = np.mean(df.loc[(df.index >= lid - 1) & (df.index <= lid), "diff"].tolist())
        df.loc[df.index > lid, "density"] = df.loc[df.index > lid].apply(
            lambda row: 0 if row.density == 0 else row.density + avg_diff, axis=1)
    df["zimi"] = df.mass * df.mf
    df["zimibysgi"] = df.apply(lambda row: 0 if row.density == 0 else row.zimi / row.density, axis=1)
    dfplus["density"] = dfplus.apply(
        lambda row: df.loc[df.index >= row.name, "zimi"].sum() / df.loc[df.index >= row.name, "zimibysgi"].sum(),
        axis=1)
    dfplus["error"] = np.abs(dfplus.poas_density - dfplus.density) / dfplus.poas_density
    return df, dfplus


def cost_function(iter_vars, df_o, dfplus_o):
    df, dfplus = calculateDensities(iter_vars, df_o, dfplus_o)
    error = 0
    for index, row in dfplus.iterrows():
        error += targetCost(row.error, 0.001)
    error += targetCost(df.error.sum(), 1.)
    lid = df.loc[~df.expdensity.isnull()].index.max()
    if not np.isnan(lid):
        error += targetCost(df.exp_error.sum(), 0.05)
        error += targetCost(df.loc[df.index >= lid - 2, "exp_error"].sum(), 0.001)
    return error


def process_result(res, df, dfplus, user_args):
    df, dfplus = calculateDensities(res.x, df, dfplus)
    save(df, user_args, identifier="density", plus_or_individual="individual", solver=1, distribution="general")
    save(dfplus, user_args, identifier="density", plus_or_individual="plus", solver=1, distribution="general")
    return True


def general(user_args):
    df, dfplus = density_input(user_args.well_conn)
    df, dfplus = getPoasDensity(df, dfplus, user_args)
    logger.info("Executing General Density Solver 1")
    d6 = df.loc[df.index == 6, "expdensity"].values[0]
    res = optimize.minimize(cost_function, np.array([2.5, 0.55]),
                            bounds=[[.5, 10.], [.1, .6634 if np.isnan(d6) else d6 * (1. - 0.1 / 100)]],
                            method="SLSQP", args=(df, dfplus), options={'maxiter': 500})
    if not res.success:
        print res
        print 'Error:{"Type": "Density Solver", "Message": "Density Solver failed to find a feasible solution. ' \
              'Please check input data. If issue persists, please raise with POAS support team."}'
        return False
    logger.info("Completed General Density Solver 1")
    logger.info("General Density solver final variables - %s" % (",".join([str(x) for x in res.x])))
    return process_result(res, df, dfplus, user_args)


"""
executed for all inputs
"""

if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection
    import pandas as pd
    from Scripts.pvt_new.settings import DEBUG_DIR
    import os

    well_name = "ALUMM"

    debug_dir = os.path.join(DEBUG_DIR, well_name)

    db = get_connection(well_name)
    test_user_args = pd.Series({"debug": True, "well_conn": db, "bias": "general", "debug_dir": debug_dir})
    general(test_user_args)
