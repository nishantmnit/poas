import importlib

import autograd.numpy as np  # Thinly-wrapped version of Numpy
import pandas as pd
from autograd import grad as gd

from Scripts.pvt_new.core.constants import valid_eos
from Scripts.pvt_new.core.get_logger import get_logger
from Scripts.pvt_new.core.optimizers.ncg import ncg as ncg_solver
from Scripts.pvt_new.core.storage.save import save
from calculated import calculated
from scales import scales
from start_point import start_point
from var_names import var_names
from var_range import var_range

logger = get_logger("pvt eos tr")


def eos_cost_poas_4(row, iter_vars, eos):
    for i in range(len(iter_vars)):
        row[var_names[eos][i]] = iter_vars[i]
    cv = calculated(row, eos)
    cost = scales[eos][0] * cv.pressure_diff
    cost += scales[eos][1] * ((row["omega"] - cv["omega"]) / row["omega"]) ** 2
    cost += scales[eos][2] * ((row["lv"] - cv["lv"]) / row["lv"]) ** 2
    cost += scales[eos][3] * max(0, cv.ph_diff - 10 ** -7) ** 2
    return cost


def eos_cost_others(row, iter_vars, eos):
    for i in range(len(iter_vars)):
        row[var_names[eos][i]] = iter_vars[i]
    cv = calculated(row, eos)
    objs = ["hv_bp", "vb"]
    costs = pd.Series()
    for i in range(len(objs)):
        costs[objs[i]] = 0 if row[objs[i]] == 0 else scales[eos][i] * (
                (row[objs[i]] - cv[objs[i]]) / row[objs[i]]) ** 2
    return costs.sum() + scales[eos][2] * max(0, cv.ph_diff - 10 ** -7) ** 2


def cost_for_ncg(iter_vars, args, only_cost=False):
    row, eos = args[0], args[1]

    eos_cost = eos_cost_poas_4 if eos == "poas_4" else eos_cost_others
    cost = eos_cost(row, iter_vars, eos)
    if only_cost:
        return cost
    grad_fct = gd(eos_cost, 1)
    derivative = np.array([i.item(0) for i in grad_fct(row, iter_vars, eos)])
    return cost, derivative


def run_eos(eos, user_args):
    eos_module = importlib.import_module("Scripts.pvt_new.tr.observed.%s" % eos)
    observed_df = getattr(eos_module, "observed")(user_args.well_conn)
    observed_df.rename(columns={"omega_final": "omega", "vc_final": "vc"}, inplace=True)
    for index, row in observed_df.iterrows():
        logger.info("tr tuning for well = %s, eos = %s, scn = %s" % (user_args.well_name, eos, index))
        ncg = ncg_solver()
        iter_vars = ncg.minimize(start_point(row, eos), var_range(row, eos), cost_for_ncg, max_iter=200, f_tol=10 ** -5,
                                 debug=user_args.debug, args=(row, eos))
        for i in range(len(iter_vars)):
            observed_df.loc[observed_df.seq == row.seq, var_names[eos][i]] = iter_vars[i]
    calculated_df = calculated(observed_df, eos)
    eos_module = importlib.import_module("Scripts.pvt_new.eos.%s" % eos)
    calculated_df.rename(columns=getattr(eos_module, "var_rename"), inplace=True)
    save(calculated_df, user_args, identifier="tr", eos=eos)


def tr(user_args):
    if user_args.eos == "all":
        eos_except_poas_4 = list(valid_eos)
        eos_except_poas_4.remove("poas_4")
        logger.info("Executing all EOS %s" % (", ".join(eos for eos in eos_except_poas_4)))
        for eos in eos_except_poas_4:
            run_eos(eos, user_args)
    else:
        logger.info("Executing EOS = %s" % user_args.eos)
        run_eos(user_args.eos, user_args)


"""
save one df with eos tuned tr data
"""

if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection
    from Scripts.pvt_new.settings import DEBUG_DIR
    import os

    well_name = "NORTHALASKAG"

    debug_dir = os.path.join(DEBUG_DIR, well_name)

    db = get_connection(well_name)
    test_user_args = pd.Series({"debug": True, "well_conn": db, "bias": "general",
                                "debug_dir": debug_dir, "eos": "poas_4", "well_name": well_name})
    tr(test_user_args)
