def var_range(row, eos):
    if eos in ["sw_poas", "als_poas"]:
        bound = [[row.alpha_multiplier_min, row.alpha_multiplier_max],
                 [row.omega_multiplier_min, row.omega_multiplier_max]]
    elif eos in ["generic_poas_a", "srk_poas", "pr_poas"]:
        bound = [[row.alpha_multiplier_min, row.alpha_multiplier_max],
                 [row.om_b_multiplier_min, row.om_b_multiplier_max]]
    elif eos in ["poas_4a"]:
        bound = [[row.alpha_multiplier_min, row.alpha_multiplier_max],
                 [row.beta_multiplier_min, row.beta_multiplier_max]]
    elif eos in ["pt_poas"]:
        bound = [[row.alpha_multiplier_min, row.alpha_multiplier_max], [row.zc_multiplier_min, row.zc_multiplier_max]]
    elif eos in ["poas_4"]:
        bound = [[row.alpha_multiplier_min, row.alpha_multiplier_max],
                 [row.om_b_multiplier_min, row.om_b_multiplier_max], [row.vp_multiplier_min, row.vp_multiplier_max]]
    else:
        raise NotImplementedError
    return bound


if __name__ == "__main__":
    test_row = dict()
    print var_range(test_row, "nishant")
