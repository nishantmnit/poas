import importlib

from var_names import var_names
from Scripts.pvt_new.core.optimizers.poas_interpolator import monotone
from get_zl_zv import get_zl_zv
from Scripts.pvt_new.core.constants import gasConstant, pressure
import autograd.numpy as np  # Thinly-wrapped version of Numpy
import pandas as pd

penalty = [-1., 1.]


def get_cost_poas_4(row):
    row["t1"] = gasConstant * row.temperature * (row.ZV - row.ZL)
    row["t2"] = 10 ** 6 if ((row.b * np.sqrt(row.u ** 2 - 4 * row.w)) == 0 or (row.u ** 2 - 4 * row.w) < 0) else (
            row.ac * (row.temperature * row.diff_alpha_wrt_temp - row.alpha_eos) / (
            row.b * np.sqrt(row.u ** 2 - 4 * row.w)))
    if ((row.ZV + row.B * row.r2) == 0 or (row.ZV + row.B * row.r1) == 0 or (
            (row.ZV + row.B * row.r2) * (row.ZV + row.B * row.r1)) < 0):
        row["t3"] = -1.
    else:
        row["t3"] = (np.log((row.ZV + row.B * row.r2) / (row.ZV + row.B * row.r1)))
    if ((row.ZL + row.B * row.r2) == 0 or (row.ZL + row.B * row.r1) == 0 or (
            (row.ZL + row.B * row.r2) * (row.ZL + row.B * row.r1)) < 0):
        row["t4"] = -5.
    else:
        row["t4"] = (np.log((row.ZL + row.B * row.r2) / (row.ZL + row.B * row.r1)))
    row["del_hv"] = ((row.t1 + row.t2 * (row.t3 - row.t4)) / 1000.)
    row["lv"] = 0 if row.vapour_pressure == 0 else row.ZL * gasConstant * row.temperature / row.vapour_pressure
    row["vv"] = 0 if row.vapour_pressure == 0 else row.ZV * gasConstant * row.temperature / row.vapour_pressure
    # pressures - liquid and vapour
    row["pressure_liquid"] = 0 if (row.lv - row.b * 10 ** 6) == 0 or (
            row.lv ** 2 + row.u * row.b * 10 ** 6 * row.lv + row.w * (row.b * 10 ** 6) ** 2) == 0 or (
                                          row.ac_alpha * 10 ** 6) == 0 else (
            (gasConstant * row.temperature) / (row.lv - row.b * 10 ** 6) - (row.ac_alpha * 10 ** 6) / (
            row.lv ** 2 + row.u * row.b * 10 ** 6 * row.lv + row.w * (row.b * 10 ** 6) ** 2))
    row["pressure_vapour"] = 0 if (row.vv - row.b * 10 ** 6) == 0 or (
            row.vv ** 2 + row.u * row.b * 10 ** 6 * row.vv + row.w * (row.b * 10 ** 6) ** 2) == 0 or (
                                          row.ac_alpha * 10 ** 6) == 0 else (
            (gasConstant * row.temperature) / (row.vv - row.b * 10 ** 6) - (row.ac_alpha * 10 ** 6) / (
            row.vv ** 2 + row.u * row.b * 10 ** 6 * row.vv + row.w * (row.b * 10 ** 6) ** 2))
    row["pressure_diff"] = np.abs(row.pressure_vapour - row.pressure_liquid)
    return row


def get_cost_others(row):
    row["t1"] = gasConstant * row.temperature * (row.ZV - row.ZL)
    row["t2"] = 10 ** 6 if ((row.b * np.sqrt(row.u ** 2 - 4 * row.w)) == 0 or (row.u ** 2 - 4 * row.w) < 0) else (
            row.ac * (row.temperature * row.diff_alpha_wrt_temp - row.alpha_eos) / (
            row.b * np.sqrt(row.u ** 2 - 4 * row.w)))
    if ((row.ZV + row.B * row.r2) == 0 or (row.ZV + row.B * row.r1) == 0 or (
            (row.ZV + row.B * row.r2) * (row.ZV + row.B * row.r1)) < 0):
        row["t3"] = -1.
    else:
        row["t3"] = (np.log((row.ZV + row.B * row.r2) / (row.ZV + row.B * row.r1)))
    if ((row.ZL + row.B * row.r2) == 0 or (row.ZL + row.B * row.r1) == 0 or (
            (row.ZL + row.B * row.r2) * (row.ZL + row.B * row.r1)) < 0):
        row["t4"] = -5.
    else:
        row["t4"] = (np.log((row.ZL + row.B * row.r2) / (row.ZL + row.B * row.r1)))
    row["hv_bp"] = ((row.t1 + row.t2 * (row.t3 - row.t4)) / 1000.)
    row["vb"] = row.ZL * gasConstant * row.temperature / row.vapour_pressure
    return row


def interpolate(x, y, seq):
    return monotone(x, y, seq)[0]


def fix_negative(df, eos):
    df["marker"] = df.apply(lambda row: 0 if row.ph_diff == 0 or row.ph_diff > 10 ** -2 else row.ph_diff, axis=1)
    x = df.loc[df.marker != 0, "seq"].tolist()
    for col in ["vb", "hv_bp"] + var_names[eos]:
        y = df.loc[df.marker != 0, col].tolist()
        df[col] = df.apply(lambda row: row[col] if row.marker != 0 else interpolate(x, y, row.seq), axis=1)
    return df


def calculated_df(df_o, eos):
    df = df_o.copy(deep=True)
    for index, row in df.iterrows():
        row = calculated_row(row, eos)
        for col in row.keys():
            df.loc[df.seq == row.seq, col] = row[col]
    df = fix_negative(df, eos)
    df.rename(columns={"hv_bp": "del_hv"}, inplace=True)
    return df


def eos_specific_calc(eos, row):
    if eos == "poas_4":
        row["vapour_pressure"] = row.vp_start * row.vp_multiplier
        row["omega_final"] = 0 if row.vapour_pressure == 0 else (-1. * np.log10(row.vapour_pressure / row.pc_mpa) - 1.)
    return row


def calculated_row(row_o, eos):
    row = row_o.copy(deep=True)
    row["alpha_eos"] = row.alpha_pr * row.alpha_multiplier
    row["diff_alpha_wrt_temp"] = row.alpha_eos * (((2. * row.alpha_m - 2.) / (
            row.temperature / row.tc_k) + row.alpha_l * (-2 * row.alpha_m) * (row.temperature / row.tc_k) ** (
                                                           2 * row.alpha_m - 1)) / row.tc_k)
    eos_module = importlib.import_module("Scripts.pvt_new.eos.%s" % eos)
    row = getattr(eos_module, eos)(row)
    row = eos_specific_calc(eos, row)
    row = get_zl_zv(row, penalty)
    if eos == "poas_4":
        row = get_cost_poas_4(row)
    else:
        row = get_cost_others(row)
    return row


def calculated(data, eos):
    if not isinstance(data, pd.DataFrame):
        return calculated_row(data, eos)
    else:
        return calculated_df(data, eos)
