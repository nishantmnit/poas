import numpy as np

from Scripts.pvt_new.core.pandas.dataframe import drop_not_in_cols
from Scripts.pvt_new.core.storage.mongo import fetch
from Scripts.pvt_new.eos.fix_hv_vb import fix_hv_vb
from Scripts.pvt_new.eos.get_om_b import get_om_b


def min_max(df):
    df["alpha_bp"] = df.alpha_multiplier * df.alpha_pr
    df.loc[df.alpha_bp < 1.05, "alpha_bp"] = 1.05
    df["alpha_tc"] = 1.
    df["tr"] = 0.7
    df["slope"] = (np.log(df.alpha_tc) - np.log(df.alpha_bp)) / (1. - (df.bp_final1 / df.tc_k))
    df.loc[df.slope > 1, "slope"] = 1.
    df.loc[df.slope < -50, "slope"] = -50.
    df["alpha_0.7"] = np.exp(np.log(df.alpha_bp) + df.slope * (0.7 - (df.bp_final1 / df.tc_k)))
    df.loc[df["alpha_0.7"] > df.alpha_pr * 1.2, "alpha_0.7"] = df.loc[df[
                                                                          "alpha_0.7"] > df.alpha_pr * 1.2, "alpha_pr"] * 1.2
    df["alpha_pr_0.7"] = (
            0.7 ** (2. * (df.alpha_m - 1.)) * (np.exp(df.alpha_l * (1. - 0.7 ** (2. * df.alpha_m)))))
    df.loc[df.index == df.index.max(), "alpha_pr_0.7"] = \
        df.loc[df.index == df.index.max() - 1, "alpha_pr_0.7"].values[0] * 1.01
    df["expected_alpha"] = df["alpha_0.7"] / df["alpha_pr_0.7"]
    df["per_multiplier"] = (0.0111349117430651 * df.index ** 1.89776187835285) * 10
    df.loc[df.per_multiplier > 99, "per_multiplier"] = 99
    df["alpha_multiplier_min"] = df.expected_alpha * (1 - ((
                                                                   1.89697738551E-09 * df.index ** 5 - 7.4990046401686E-07 * df.index ** 4 + 0.00010418894957489 * df.index ** 3 - 0.00605636535447327 * df.index ** 2 + 0.203286734580314 * df.index + 1.07366170022993) * 1.25) / 100)
    df["alpha_multiplier_max"] = (df.expected_alpha * (1. + 1. * df.per_multiplier / 100.))
    df.loc[df.alpha_multiplier_max > 1.2, "alpha_multiplier_max"] = 1.2
    df["om_b_multiplier_min"] = df.om_b_multiplier * (
            -5.856486207E-11 * df.index ** 5 + 2.787514371825E-08 * df.index ** 4 - 4.90198457824066E-06 * df.index ** 3 + 0.000380349000374974 * df.index ** 2 - 0.0137231138575975 * df.index + 0.767728728170205)
    df["om_b_multiplier_max"] = df.om_b_multiplier * (1. + 0.7 * df.per_multiplier / 100.)
    df.loc[df.om_b_multiplier_max > 1.5, "om_b_multiplier_max"] = 1.5


def add_eosbp(df, well_conn):
    p4 = fetch(well_conn, property="pvt_output", identifier="tr", eos="poas_4")
    p4["hv_bp"] = p4.del_hv
    eos = fetch(well_conn, property="pvt_output", identifier="eosbp", eos="generic_poas_a")
    eos_cols = ["bp_final1", "tc_k", "zc_final", "alpha_l", "alpha_m", "alpha_multiplier", "alpha_pr", "vb",
                "om_b_multiplier"]
    p4[eos_cols] = eos[eos_cols]
    fix_hv_vb(p4, well_conn)
    df[["hv_bp", "vb", "vapour_pressure"]] = p4[["hv_bp", "vb", "vapour_pressure"]]
    df[eos_cols] = p4[eos_cols]
    min_max(df)
    df["vb"] = df.vb * (df.zc_final ** (1. - 0.7) ** (2. / 7.)) / (
            df.zc_final ** (1. - df.bp_final1 / df.tc_k) ** (2. / 7.))
    df["temperature"] = df.tc_k * 0.7
    df["a1_d"] = (3. * df.zc_final - 1.75) * -1.
    df["a2_d"] = (3. * df.zc_final ** 2 + 0.5 * (1. - 3. * df.zc_final))
    df["a3_d"] = (df.zc_final ** 3 + 0.25 * (1. - 3. * df.zc_final) ** 2) * -1.
    return get_om_b(df)


def observed(well_conn):
    df = fetch(well_conn, property="pvt_output", identifier="fcm")
    cols = ["mf", "bp_final1", "tcbytb", "vc_final", "zc_final", "pc_mpa", "vb", "hv_bp", "alpha_l", "alpha_m",
            "tc_k", "seq", "component", "omega_final"]
    df.loc[df.tcbytb < 1.0001, "tcbytb"] = 1.0001
    drop_not_in_cols(df, cols)
    df = add_eosbp(df, well_conn)
    df["max_om_b"] = df.om_b
    df["alpha_pr"] = ((df.temperature / df.tc_k) ** (2 * (df.alpha_m - 1)) * (
        np.exp(df.alpha_l * (1 - (df.temperature / df.tc_k) ** (2 * df.alpha_m)))))
    return df


"""
return one df with data input for tr tuning
"""

if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection

    well_name = "AB8"
    db = get_connection(well_name)
    test_df = observed(db)
    print test_df
