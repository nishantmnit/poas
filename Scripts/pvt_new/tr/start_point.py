import numpy as np


def start_point(row, eos):
    iter_vars = list()
    if eos in ["poas_4"]:
        iter_vars = [round(row.expected_alpha, 5), round((0.0305405900409994 * np.log(row.name) + 1.14767398454079), 5),
                     1.]
    elif eos in ["generic_poas_a"]:
        iter_vars = [round(row.expected_alpha, 8), round(row.om_b_multiplier, 8)]
    elif eos == "pt_poas":
        iter_vars = [round(row.expected_alpha, 8), round(row.zc_multiplier, 8)]
    elif eos in ["sw_poas"]:
        iter_vars = [round(row.expected_alpha, 8), round(row.omega_multiplier, 8)]
    elif eos in ["als_poas"]:
        iter_vars = [round(row.expected_alpha, 8), round(row.omega_multiplier, 8)]
    elif eos in ["poas_4a"]:
        iter_vars = [round(row.expected_alpha, 8), round(row.beta_multiplier, 8)]
    elif eos in ["pr_poas"]:
        iter_vars = [round(row.expected_alpha, 8), round(row.om_b_multiplier, 8)]
    elif eos in ["srk_poas"]:
        iter_vars = [round(row.expected_alpha, 8), round(row.om_b_multiplier, 8)]
    iter_vars = [round(x, 8) for x in iter_vars]
    return iter_vars
