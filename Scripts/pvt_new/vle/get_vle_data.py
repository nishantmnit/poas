import os.path

import numpy as np
import pandas as pd
from scipy.interpolate import PchipInterpolator

from Scripts.pvt_new.core.constants import gasConstant
from Scripts.pvt_new.settings import CSV_DATA
from Scripts.pvt_new.vle.generic import find_min, find_max, tune_eos_multiplier, calc_alpha_eos


def read_vle_input(compound, temperature, pressure):
    df = pd.read_csv(os.path.join(CSV_DATA, "vle", "%s.csv" % compound))
    df = df.head(1)[["tc_k", "pc_mpa", "zc_final", "vc", "omega", "alpha_l", "alpha_m"]]
    df["alpha_pr"] = ((temperature / df.tc_k) ** (2 * (df.alpha_m - 1)) *
                      (np.exp(df.alpha_l * (1 - (temperature / df.tc_k) ** (2 * df.alpha_m)))))
    df["temperature"] = temperature
    df["pressure"] = pressure
    return df


def predict_alpha(required_temperature, compound):
    vle_output = os.path.join(CSV_DATA, "vle", "%s.csv" % compound)
    df = pd.read_csv(
        vle_output
    )[["tc_k", "pc_mpa", "zc_final", "vc", "omega", "m", "n", "tou"]].drop_duplicates().reset_index()

    alpha_eos = np.exp((df.m * (1. - required_temperature / df.tc_k) * (np.abs(1. - required_temperature / df.tc_k)) ** (
                df.tou - 1.) + df.n * (df.tc_k / required_temperature - 1.)))
    return alpha_eos.values[0]


def predict_volume(required_temperature, required_pressure, compound):
    vle_output = os.path.join(CSV_DATA, "kij", compound, "volume.csv")
    df = pd.read_csv(vle_output, index_col=0)
    df.sort_index(inplace=True)
    temperatures, volumes = np.array(df.columns.to_list()), list()
    x = df.index.values  # pressure
    if required_temperature in temperatures.astype(np.float) and required_pressure in x:
        column_index = np.where(temperatures.astype(np.float) == required_temperature)
        return df.loc[df.index == required_pressure, temperatures[column_index]].values[0]
    for temperature in temperatures:
        y = df[temperature].values
        volumes.append(PchipInterpolator(x, y)(required_pressure))
    return PchipInterpolator(temperatures, np.array(volumes))(required_temperature)


def calc_alpha_eos_2(df):
    u, w = 2., -1.
    m = ((df.volume ** 2 + u * df.b * 10 ** 6 * df.volume + w * (df.b * 10 ** 6) ** 2) / (
            df.volume ** 2 + df.u * df.b * 10 ** 6 * df.volume + df.w * (df.b * 10 ** 6) ** 2))
    ac_alpha = df.ac_alpha * m
    df["alpha_eos"] = ac_alpha / df.ac
    return df


def vle_data(compound, temperature, pressure, eos):
    df = read_vle_input(compound, temperature, pressure)
    df["alpha_eos"] = predict_alpha(temperature, compound)
    df["volume"] = predict_volume(temperature, pressure, compound)
    df["multiplier_minimum"] = find_min(df.head(1).squeeze(), eos)
    max_multiplier, min_multiplier = find_max(df.head(1).squeeze(), eos)
    df["multiplier_minimum"] = min_multiplier
    df["multiplier_maximum"] = max_multiplier
    df["z"] = df.pressure * df.volume / (gasConstant * df.temperature)

    df = tune_eos_multiplier(df, eos)
    df = calc_alpha_eos(df, eos)
    df = calc_alpha_eos_2(df)
    cols = ["tc_k", "pc_mpa", "zc_final", "vc", "omega", "volume"]
    return df[["om_b", "b", "ac", "alpha_eos"] + cols]


if __name__ == "__main__":
    out_df = vle_data("HYDROGEN", 395.3722222, 12.18368613, "poas_4")
    print out_df.T
