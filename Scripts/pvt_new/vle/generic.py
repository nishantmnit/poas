import importlib

import numpy as np
import pandas as pd

from Lib.Utils import getLogger
from Scripts.pvt_new.core.constants import gasConstant
from Scripts.pvt_new.core.optimizers.poas_interpolator import power, exponential, calc_extrapolated_point
from Scripts.pvt_new.vle.max_multiplier import find_max
from Scripts.pvt_new.vle.min_multiplier import find_min

logger = getLogger("vle generic")


def get_multiplier(row):
    multipliers = list()
    if row.multiplier_minimum == row.multiplier_maximum:
        logger.exception("minimum and maximum multipliers are same for row =\n%s" % row)
        raise ValueError
    var_range = [row.multiplier_minimum, row.multiplier_maximum]
    steps = 50.
    interval = (var_range[1] - var_range[0]) / steps
    var = var_range[0]
    while round(var, 2) <= var_range[1]:
        multipliers.append(var)
        var = var + interval
    return multipliers


def pre_processing(df):
    df["volume"] = df.v_liq
    df.loc[df.v_liq == 0, "volume"] = df.v_vapour
    df["alpha_eos"] = df.alpha_poas
    df["diff_alpha_wrt_temp"] = df.alpha_eos * (((2. * df.alpha_m - 2.) / (
            df.temperature / df.tc_k) + df.alpha_l * (-2 * df.alpha_m) * (df.temperature / df.tc_k) ** (
                                                         2 * df.alpha_m - 1)) / df.tc_k)
    df["z"] = df.pressure * df.volume / (gasConstant * df.temperature)
    return df


def get_objective(row):
    term = (row.b * 10 ** 6 * row.pressure / (gasConstant * row.temperature))
    row["objective"] = (row.z ** 3 + (row.u * term - term - 1) * row.z ** 2 +
                        ((row.ac * row.alpha_eos * 10 ** 6 * row.pressure /
                          (gasConstant ** 2 * row.temperature ** 2)) + row.w * term ** 2 -
                         row.u * term - row.u * term ** 2) * row.z -
                        ((row.ac * row.alpha_eos * 10 ** 6 * row.pressure / (
                                gasConstant ** 2 * row.temperature ** 2))
                         * term + row.w * (row.b * 10 ** 6 * row.pressure / (gasConstant * row.temperature)) ** 2 +
                         row.w * term ** 3))
    return row


def tune_eos_multiplier(df, eos):
    eos_method = get_eos_method(eos)
    for index, row in df.iterrows():
        logger.debug("Executing for index = %s" % index)
        objectives = list()
        multipliers = get_multiplier(row)
        for multiplier in multipliers:
            row["multiplier"] = multiplier
            row = eos_method(row)
            row = get_objective(row)
            objectives.append(row.objective)
        df.loc[df.index == index, "multiplier"] = tune_multiplier(row, objectives, multipliers, eos)
    return df


def find_minima(objectives):
    min_negative_objective = max(objectives[objectives < 0]) if np.any(objectives < 0) else 500
    min_positive_objective = min(objectives[objectives >= 0]) if np.any(objectives >= 0) else 500
    min_negative_objective_index = np.where(objectives == min_negative_objective)[0][0] if np.any(
        objectives < 0) else 500
    min_positive_objective_index = np.where(objectives == min_positive_objective)[0][0] if np.any(
        objectives >= 0) else 500
    min_objective_index = min(min_negative_objective_index, min_positive_objective_index)
    try:
        min_objective = objectives[min_objective_index]
    except Exception as e:
        logger.debug("All objectives are nan = %s" % objectives)
        raise ValueError(e)
    return min_objective, min_objective_index


def tune_multiplier(row, objectives, multipliers, eos):
    eos_method = get_eos_method(eos)

    df = pd.DataFrame(columns=["objective", "multiplier"], data=list(zip(objectives, multipliers)))
    df["difference"] = df.objective - df.objective.shift(-1)
    df = df.loc[df.difference != 0].reset_index(drop=True)
    objectives = df.objective.values
    multipliers = df.multiplier.tolist()
    min_objective, min_objective_index = find_minima(objectives)
    indexes = [min_objective_index + i for i in range(-2, 2)]

    if np.any(np.array(indexes) < 0):
        indexes = list(np.array(indexes) + np.abs(min(indexes)))
    elif min_objective_index == len(multipliers) - 1:
        indexes = list(np.array(indexes) + ((len(multipliers) - 1) - max(indexes)))

    costs = [list(objectives)[i] for i in indexes]
    alphas = [multipliers[i] for i in indexes]

    k1 = (costs[1] ** 3 - costs[0] ** 3) * (costs[2] - costs[0]) - (costs[2] ** 3 - costs[0] ** 3) * (
            costs[1] - costs[0])
    k2 = (costs[1] ** 2 - costs[0] ** 2) * (costs[2] - costs[0]) - (costs[2] ** 2 - costs[0] ** 2) * (
            costs[1] - costs[0])
    k3 = (costs[1] ** 3 - costs[0] ** 3) * (costs[3] - costs[0]) - (costs[3] ** 3 - costs[0] ** 3) * (
            costs[1] - costs[0])
    k4 = (costs[1] ** 2 - costs[0] ** 2) * (costs[3] - costs[0]) - (costs[3] ** 2 - costs[0] ** 2) * (
            costs[1] - costs[0])
    p1 = (alphas[1] - alphas[0]) * (costs[2] - costs[0]) - (alphas[2] - alphas[0]) * (costs[1] - costs[0])
    p2 = (alphas[1] - alphas[0]) * (costs[3] - costs[0]) - (alphas[3] - alphas[0]) * (costs[1] - costs[0])

    a = 0 if (k1 * k4 - k2 * k3) == 0 else (p1 * k4 - p2 * k2) / (k1 * k4 - k2 * k3)
    if np.abs(a) > 10 ** 50:
        a = 10 ** 50
    b = 0 if k2 == 0 else (p1 - a * k1) / k2
    if np.abs(b) > 10 ** 50:
        b = 10 ** 50

    c = 0 if (costs[1] - costs[0]) == 0 else ((alphas[1] - alphas[0]) - a * (costs[1] ** 3 - costs[0] ** 3) - b * (
            costs[1] ** 2 - costs[0] ** 2)) / (costs[1] - costs[0])
    if np.abs(c) > 10 ** 50:
        c = 10 ** 50

    d = alphas[0] - (a * costs[0] ** 3 + b * costs[0] ** 2 + c * costs[0])
    if np.abs(d) > 10 ** 50:
        d = 10 ** 50

    row["multiplier"] = max(multipliers) if d >= max(multipliers) else (
        min(multipliers) if d <= min(multipliers) else d)
    row = eos_method(row)
    row = get_objective(row)
    multiplier = row.multiplier if np.abs(row.objective) <= np.abs(min_objective) else multipliers[
        min_objective_index]
    return multiplier


def error(df):
    df["error"] = np.abs((df.y - df.y_calc) / df.y)
    return df.error.sum()


def curve_fit(df_o, curve):
    df = df_o.copy(deep=True)
    coefficients = power(df) if curve == "power" else exponential(df)
    df["y_calc"] = calc_extrapolated_point(curve, coefficients, df.xr.values)
    offset = df.loc[df.xr == df.xr.min(), "y"].values[0] / df.loc[df.xr == df.xr.min(), "y_calc"].values[0]
    df["y_calc"] = df.y_calc * offset
    fit_error = error(df)
    logger.info(
        "curve == %s | error = %s | offset =%s | coefficients = %s" % (curve, fit_error, offset, coefficients))
    return coefficients, fit_error, offset


def best_curve(df):
    df.rename(columns={"temperature": "xr", "alpha_eos": "y"}, inplace=True)
    power_coefficients, power_error, power_offset = curve_fit(df, "power")
    exp_coefficients, exp_error, exp_offset = curve_fit(df, "exponential")
    if power_error < exp_error:
        df["y_calc"] = calc_extrapolated_point("power", power_coefficients, df.xr.values)
        df["y_calc"] = df.y_calc * power_offset
        df["curve"] = "power"
        df["coefficients"] = ",".join([str(i) for i in power_coefficients])
        df["offset"] = power_offset
    else:
        df["y_calc"] = calc_extrapolated_point("exponential", exp_coefficients, df.xr.values)
        df["y_calc"] = df.y_calc * exp_offset
        df["curve"] = "exponential"
        df["coefficients"] = ",".join([str(i) for i in exp_coefficients])
        df["offset"] = exp_offset
    df.rename(columns={"xr": "temperature", "y_calc": "alpha_eos"}, inplace=True)
    return df[["temperature", "pressure", "alpha_eos", "curve", "coefficients", "offset", "multiplier_minimum",
               "multiplier_maximum"]]


def smooth(df):
    dict_dfs = dict()
    pressures = df.pressure.unique()
    for pressure in pressures:
        logger.debug("Tuning for pressure = %s" % pressure)
        df_pressure = df.loc[df.pressure == pressure].reset_index(drop=True)
        dict_dfs[pressure] = best_curve(df_pressure)

    df = pd.concat(dict_dfs.values(), ignore_index=True)
    return df


def get_eos_method(eos):
    eos_module = importlib.import_module("Scripts.pvt_new.eos.%s" % eos)
    eos_method = getattr(eos_module, eos)
    return eos_method


def run_workflow(df, eos):
    df = pre_processing(df)

    df["multiplier_minimum"] = find_min(df.head(1).squeeze(), eos)
    for index, row in df.iterrows():
        max_multiplier, min_multiplier = find_max(row, eos)
        df.loc[df.index == index, "multiplier_maximum"] = max_multiplier
        df.loc[df.index == index, "multiplier_minimum"] = min_multiplier

    logger.debug("Tuning eos multiplier initial")
    df = tune_eos_multiplier(df, eos)
    df = calc_alpha_eos_initial(df, eos)
    logger.debug("Tuning eos multiplier final")
    df = tune_eos_multiplier(df, eos)
    df = calc_alpha_eos(df, eos)
    logger.debug("smoothing the output")
    df = smooth(df)
    return df


def calc_alpha_eos_initial(df, eos):
    eos_method = get_eos_method(eos)
    df = eos_method(df)
    df["sigma"] = df.apply(
        lambda row: 0 if (row.u ** 2 - 4 * row.w) <= 0 else (row.u - np.sqrt(row.u ** 2 - 4 * row.w)) / 2., axis=1)
    df["epsilon"] = df.apply(
        lambda row: 0 if (row.u ** 2 - 4 * row.w) <= 0 else (row.u + np.sqrt(row.u ** 2 - 4 * row.w)) / 2., axis=1)
    df["t1"] = gasConstant * df.temperature * (df.z - 1.)
    df["t2"] = df.del_h_fluid * 1000. - df.t1
    df["f1"] = df.t2 * (df.epsilon - df.sigma) * df.b / (
        np.log((df.volume + df.sigma * df.b * 10 ** 6) / (df.volume + df.epsilon * df.b * 10 ** 6)))
    df["alpha_eos_multiplier"] = (df.f1 / (df.ac * (df.alpha_eos - df.temperature * df.diff_alpha_wrt_temp)))
    df.loc[df.alpha_eos_multiplier < 0.2, "alpha_eos_multiplier"] = 1.1
    df.loc[df.alpha_eos_multiplier > 10., "alpha_eos_multiplier"] = 10.
    df["alpha_eos"] = df.alpha_eos_multiplier * df.alpha_eos
    return df


def calc_alpha_eos(df, eos):
    eos_method = get_eos_method(eos)
    df = eos_method(df)
    df["sigma"] = df.apply(
        lambda row: 0 if (row.u ** 2 - 4 * row.w) <= 0 else (row.u - np.sqrt(row.u ** 2 - 4 * row.w)) / 2, axis=1)
    df["epsilon"] = df.apply(
        lambda row: 0 if (row.u ** 2 - 4 * row.w) <= 0 else (row.u + np.sqrt(row.u ** 2 - 4 * row.w)) / 2, axis=1)
    df["alpha_eos_min"] = 0.65 * df.alpha_eos
    df["alpha_eos_max"] = 1.3 * df.alpha_eos

    df["alpha_eos"] = (((gasConstant * df.temperature / (df.volume - df.b * 10 ** 6)) - df.pressure) * (
            df.volume ** 2 + df.u * df.b * 10 ** 6 * df.volume + df.w * (df.b * 10 ** 6) ** 2) / (df.ac * 10 ** 6))
    df.loc[df.alpha_eos < df.alpha_eos_min, "alpha_eos"] = df.alpha_eos_min
    df.loc[df.alpha_eos > df.alpha_eos_max, "alpha_eos"] = df.alpha_eos_max
    return df


if __name__ == "__main__":
    import os
    from Scripts.pvt_new.settings import CSV_DATA

    test_eos = "poas_4"
    compound = "CARBON DIOXIDE"
    observed_data_file = os.path.join(CSV_DATA, "vle", "%s.csv" % compound)
    ov = pd.read_csv(observed_data_file)
    ov = ov.loc[ov.index == 15]
    print run_workflow(ov, test_eos).columns
