import argparse
import os.path

import pandas as pd

from Lib.Utils import getLogger
from Scripts.pvt_new.core.storage.excel import write_csv
from Scripts.pvt_new.settings import CSV_DATA
from Scripts.pvt_new.vle.generic import run_workflow
from Scripts.pvt_new.vle.volume import standardize_volume


def read_options():
    parser = argparse.ArgumentParser()
    parser.add_argument('--eos', dest='eos', default='poas_4', help='EOS to run.')
    parser.add_argument('--compound', dest='compound', default='HYDROGEN SULFIDE', help='EOS to run.')
    parser.add_argument('--volume', dest='volume', default=False, action='store_true',
                        help='Run volume standardization')
    args = parser.parse_args()
    return args


def observed_data(compound):
    observed_data_file = os.path.join(CSV_DATA, "vle", "%s.csv" % compound)
    ov = pd.read_csv(observed_data_file)
    return ov


def run(eos, compound, volume):
    df = observed_data(compound)
    logger.info("Started VLE tuning for compound = %s | eos = %s. Total rows = %s" % (compound, eos, df.shape[0]))

    if volume:
        df_volume = standardize_volume(df)
        write_csv(os.path.join(CSV_DATA, "kij", compound), filename=["volume"], df=df_volume)

    # df = df.loc[df.index.isin([0])]

    df = run_workflow(df, eos)
    df = df.sort_values(by=["pressure", "temperature"]).reset_index(drop=True)
    write_csv(os.path.join(CSV_DATA, "kij", compound), [eos], df)

    logger.info("Completed VLE tuning for compound = %s | eos = %s" % (compound, eos))


if __name__ == "__main__":
    user_args = read_options()
    logger = getLogger("vle_%s_%s" % (user_args.eos, user_args.compound), master_module=True)
    try:
        run(user_args.eos, user_args.compound, user_args.volume)
    except Exception as e:
        logger.exception('EXCEPTION IN MAIN == %s | %s!' % (user_args.compound, user_args.eos))
        raise Exception(e)
