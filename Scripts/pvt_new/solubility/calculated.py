import importlib

from config import cost_penalty
from get_zl_zv import get_zl_zv
from Scripts.pvt_new.core.constants import gasConstant, pressure
import autograd.numpy as np  # Thinly-wrapped version of Numpy
import pandas as pd

penalty = [-1.5, 1.]


def get_cost(eos, row, final):
    cp = cost_penalty(eos)
    row["v_liq"] = row.ZL * gasConstant * row.temperature / pressure
    row["v_vapour"] = row.ZV * gasConstant * row.temperature / pressure
    row["t1"] = 0 if row.v_liq == 0 or (row.b * (row.r2 - row.r1)) == 0 else ((1. / row.v_liq) * (
            (row.ac_alpha - row.temperature * row.ac * row.diff_alpha_wrt_temp) / (row.b * (row.r2 - row.r1))))
    if row.v_vapour == 0:
        row["t2"] = 0
    elif (row.v_vapour + row.r1 * row.b * 10 ** 6) <= 0 or (row.v_vapour + row.r2 * row.b * 10 ** 6) <= 0 or (
            (row.v_vapour + row.r1 * row.b * 10 ** 6) / (row.v_vapour + row.r2 * row.b * 10 ** 6)) < 0:
        row["t2"] = cp[0] if not final else 0
    else:
        row["t2"] = (
            np.log((row.v_vapour + row.r1 * row.b * 10 ** 6) / (row.v_vapour + row.r2 * row.b * 10 ** 6)))
    if row.v_liq == 0:
        row["t3"] = 0
    elif (row.v_liq + row.r1 * row.b * 10 ** 6) <= 0 or (row.v_liq + row.r2 * row.b * 10 ** 6) <= 0 or (
            (row.v_liq + row.r1 * row.b * 10 ** 6) / (row.v_liq + row.r2 * row.b * 10 ** 6)) < 0:
        row["t3"] = cp[1] if not final else 0
    else:
        row["t3"] = (np.log((row.v_liq + row.r1 * row.b * 10 ** 6) / (row.v_liq + row.r2 * row.b * 10 ** 6)))
    row["del_ui_by_vi"] = row.t1 * (row.t2 - row.t3)
    row["solubility"] = 100. if row.del_ui_by_vi == 0 else np.sqrt(row.del_ui_by_vi)
    return row


def calculated_df(df_o, eos):
    df = df_o.copy(deep=True)
    for index, row in df.iterrows():
        row = calculated_row(row, eos, final=True)
        for col in row.keys():
            df.loc[df.seq == row.seq, col] = row[col]
    df["solubility_observed"] = df_o.solubility
    return df


def calculated_row(row_o, eos, final):
    row = row_o.copy(deep=True)
    row["alpha_eos"] = row.alpha_pr * row.alpha_multiplier
    row["diff_alpha_wrt_temp"] = row.alpha_eos * (((2. * row.alpha_m - 2.) / (
            row.temperature / row.tc_k) + row.alpha_l * (-2 * row.alpha_m) * (row.temperature / row.tc_k) ** (
                                                           2 * row.alpha_m - 1)) / row.tc_k)
    eos_module = importlib.import_module("Scripts.pvt_new.eos.%s" % eos)
    row = getattr(eos_module, eos)(row)
    row = get_zl_zv(row, penalty)
    row = get_cost(eos, row, final)
    return row


def calculated(data, eos):
    if not isinstance(data, pd.DataFrame):
        return calculated_row(data, eos, final=False)
    else:
        return calculated_df(data, eos)
