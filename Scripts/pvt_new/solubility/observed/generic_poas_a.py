import numpy as np
import pandas as pd

from Scripts.pvt_new.cm.predict_om_b import predict_om_b
from Scripts.pvt_new.core.storage.mongo import fetch
from Scripts.pvt_new.eos.get_om_b import get_om_b
from generic import get_generic_input_data


def get_volume_data(df, well_conn, temperature):
    poas_4 = fetch(well_conn, property="pvt_output", identifier="solubility", eos="poas_4", temperature=temperature)
    df["scn"] = df.index
    df.reset_index(inplace=True, drop=True)
    poas_4.reset_index(inplace=True, drop=True)
    df[["v_liq", "solubility"]] = poas_4[["v_liq", "solubility"]]
    return df


def get_input_data(eos, well_conn, temperature):
    df = get_generic_input_data(eos, well_conn)
    df = get_volume_data(df, well_conn, temperature)
    return df


def eos_specific_iter_vars(df_o, well_conn, temperature):
    df = df_o.copy(deep=True)
    df = pd.concat([df, predict_om_b(well_conn, temperature)[["slope", "intercept"]]], axis=1)
    om_b_multiplier_based_slope_intercept = np.exp(df.slope * df.bp_final1 + df.intercept).values

    df["om_b_max"] = fetch(well_conn, property="pvt_output", identifier="eosbp", eos="poas_4").om_b.values

    df["om_b_eos"] = ((0.9129 * df.zc_final ** 2 - 0.0551 * df.zc_final + 0.0368) * df.om_b_multiplier)
    df.loc[df.om_b_eos > df.om_b_max, "om_b_eos"] = df.om_b_max

    poas_4_vb = fetch(well_conn, property="pvt_output", identifier="eosbp", eos="poas_4").vb.values
    eos_vb = fetch(well_conn, property="pvt_output", identifier="eosbp", eos="srk_poas").vb.values

    df["smooth_om_b_eos"] = (df.om_b_eos * (poas_4_vb / eos_vb))
    df.loc[df.smooth_om_b_eos > 1.1, "smooth_om_b_eos"] = 1.1
    df["smooth_om_b_eos_multiplier"] = df.smooth_om_b_eos / (
            0.9129 * df.zc_final ** 2 - 0.0551 * df.zc_final + 0.0368)
    df["multiplier"] = df.smooth_om_b_eos_multiplier / om_b_multiplier_based_slope_intercept

    df_o["om_b_multiplier"] = df.multiplier * np.exp(df.slope * temperature + df.intercept)
    df_o["om_b_multiplier_min"] = df_o.om_b_multiplier * 0.8
    df_o["om_b_multiplier_max"] = df_o.om_b_multiplier * 1.2
    return df_o


def alpha(df_o, temperature, well_conn):
    df = df_o.copy(deep=True)
    df["tbr"] = df.bp_final1 / df.tc_k
    df["tr"] = 0.7
    df["tcr"] = 1.
    df["t1"] = df[['tbr', 'tr']].min(axis=1)
    df["t2"] = df[['tbr', 'tr']].max(axis=1)
    df["t3"] = df.tcr
    df["alpha_eos1"] = df.apply(lambda row: row.alpha_eos_bp if row.t1 == row.tbr else row.alpha_eos_tr, axis=1)
    df["alpha_eos2"] = df.apply(lambda row: row.alpha_eos_tr if row.t2 == row.tr else row.alpha_eos_bp, axis=1)
    df["alpha_eos3"] = 1.
    for col in ["alpha_eos1", "alpha_eos2", "alpha_eos3"]:
        df["ln_%s" % col] = np.log(df[col])
    df["y1"] = np.log(df.alpha_eos1)
    df["y2"] = np.log(df.alpha_eos2)
    df["y3"] = np.log(df.alpha_eos3)
    df["xi"] = df[["t1", "t2", "t3"]].sum(axis=1)
    df["yi"] = df[["ln_alpha_eos1", "ln_alpha_eos2", "ln_alpha_eos3"]].sum(axis=1)
    df["xiyi"] = df.t1 * df.ln_alpha_eos1 + df.t2 * df.ln_alpha_eos2 + df.t3 * df.ln_alpha_eos3
    df["xi2"] = df.t1 ** 2 + df.t2 ** 2 + df.t3 ** 2
    df["intercept"] = (df.xi * df.xiyi - df.yi * df.xi2) / (df.xi ** 2 - 3. * df.xi2)
    df["slope"] = (df.yi - 3. * df.intercept) / df.xi
    df["alpha_pr_req_tr"] = ((temperature / df.tc_k) ** (2. * (df.alpha_m - 1.)) * (
        np.exp(df.alpha_l * (1. - (temperature / df.tc_k) ** (2 * df.alpha_m)))))
    df["expected_alpha"] = np.exp(df.slope * (temperature / df.tc_k) + df.intercept)
    df["expected_alpha_multiplier"] = df.expected_alpha / df.alpha_pr_req_tr
    df["alpha_multiplier_min"] = df.expected_alpha * 0.3 / df.alpha_pr_req_tr
    df["alpha_multiplier_max"] = df.expected_alpha * 15. / df.alpha_pr_req_tr
    df_o[["alpha_multiplier_min", "alpha_multiplier_max", "expected_alpha_multiplier"]] = df[
        ["alpha_multiplier_min", "alpha_multiplier_max", "expected_alpha_multiplier"]]
    df_o["alpha_pr"] = df.alpha_pr_req_tr
    df_o = eos_specific_iter_vars(df_o, well_conn, temperature)
    df_o["temperature"] = temperature
    return df_o


def observed(eos, well_conn, temperature):
    df = get_input_data(eos, well_conn, temperature)
    df = alpha(df, temperature, well_conn)
    df["a1_d"] = (3. * df.zc_final - 1.75) * -1.
    df["a2_d"] = (3. * df.zc_final ** 2 + 0.5 * (1. - 3. * df.zc_final))
    df["a3_d"] = (df.zc_final ** 3 + 0.25 * (1. - 3. * df.zc_final) ** 2) * -1.
    df = get_om_b(df)
    df["max_om_b"] = df.om_b
    return df


"""
return one df with data input for generic_poas_a solubility tuning
"""

if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection

    well_name = "NORTHALASKAG"
    db = get_connection(well_name)
    print observed("generic_poas_a", db, 400)
