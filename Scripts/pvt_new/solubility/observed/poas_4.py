from Scripts.pvt_new.core.storage.mongo import fetch
import pandas as pd
import numpy as np
from Scripts.pvt_new.eos.get_om_b import get_om_b
from generic import get_generic_input_data
from Scripts.pvt_new.pt_matching.kij.input.solubility_data import solubility


def get_volume_data(df, well_conn, temperature):
    vs = fetch(well_conn, property="pvt_output", identifier="volume_solver")
    df["scn"] = df.index
    df.reset_index(inplace=True, drop=True)
    df = pd.concat([df, solubility(well_conn)], axis=1)
    df["v_liq"] = vs["volume_%s" % temperature]
    df["solubility"] = np.sqrt(((df.sol_dispersion / ((vs["volume_298.15"] / df.v_liq) **
                                                      (-1.25))) ** 2 +
                                (df.sol_polar / ((vs["volume_298.15"] / df.v_liq) ** (-0.5))) ** 2 +
                                (df.sol_h_bonding / (np.exp(-1 * 1.32 * 10 ** -3 * (298.15 - temperature) -
                                                            np.log((vs["volume_298.15"] /
                                                                    df.v_liq) ** 0.5)))) ** 2).values)
    return df


def get_input_data(eos, well_conn, temperature):
    df = get_generic_input_data(eos, well_conn)
    df = get_volume_data(df, well_conn, temperature)
    return df


def alpha(df_o, temperature):
    df = df_o.copy(deep=True)
    df["tbr"] = df.bp_final1 / df.tc_k
    df["tr"] = 0.7
    df["tcr"] = 1.
    df["t1"] = df[['tbr', 'tr']].min(axis=1)
    df["t2"] = df[['tbr', 'tr']].max(axis=1)
    df["t3"] = df.tcr
    df["alpha_eos1"] = df.apply(lambda row: row.alpha_eos_bp if row.t1 == row.tbr else row.alpha_eos_tr, axis=1)
    df["alpha_eos2"] = df.apply(lambda row: row.alpha_eos_tr if row.t2 == row.tr else row.alpha_eos_bp, axis=1)
    df["alpha_eos3"] = 1.
    for col in ["alpha_eos1", "alpha_eos2", "alpha_eos3"]:
        df["ln_%s" % col] = np.log(df[col])
    df["y1"] = np.log(df.alpha_eos1)
    df["y2"] = np.log(df.alpha_eos2)
    df["y3"] = np.log(df.alpha_eos3)
    df["xi"] = df[["t1", "t2", "t3"]].sum(axis=1)
    df["yi"] = df[["ln_alpha_eos1", "ln_alpha_eos2", "ln_alpha_eos3"]].sum(axis=1)
    df["xiyi"] = df.t1 * df.ln_alpha_eos1 + df.t2 * df.ln_alpha_eos2 + df.t3 * df.ln_alpha_eos3
    df["xi2"] = df.t1 ** 2 + df.t2 ** 2 + df.t3 ** 2
    df["intercept"] = (df.xi * df.xiyi - df.yi * df.xi2) / (df.xi ** 2 - 3. * df.xi2)
    df["slope"] = (df.yi - 3. * df.intercept) / df.xi
    df["alpha_pr_req_tr"] = ((temperature / df.tc_k) ** (2. * (df.alpha_m - 1.)) * (
        np.exp(df.alpha_l * (1. - (temperature / df.tc_k) ** (2 * df.alpha_m)))))
    df["expected_alpha"] = np.exp(df.slope * (temperature / df.tc_k) + df.intercept)
    df["expected_alpha_multiplier"] = df.expected_alpha / df.alpha_pr_req_tr
    df["alpha_multiplier_min"] = df.expected_alpha * 0.5 / df.alpha_pr_req_tr
    df["alpha_multiplier_max"] = df.expected_alpha * 5. / df.alpha_pr_req_tr
    df_o[["alpha_multiplier_min", "alpha_multiplier_max", "expected_alpha_multiplier"]] = df[
        ["alpha_multiplier_min", "alpha_multiplier_max", "expected_alpha_multiplier"]]
    df_o["alpha_pr"] = df.alpha_pr_req_tr
    df_o["om_b_multiplier_min"] = df_o.om_b_multiplier * 0.4
    df_o["om_b_multiplier_max"] = 1.25
    df_o["temperature"] = temperature
    return df_o


def observed(eos, well_conn, temperature):
    df = get_input_data(eos, well_conn, temperature)
    df = alpha(df, temperature)
    df["a1_d"] = (3. * df.zc_final - 1.75) * -1.
    df["a2_d"] = (3. * df.zc_final ** 2 + 0.5 * (1. - 3. * df.zc_final))
    df["a3_d"] = (df.zc_final ** 3 + 0.25 * (1. - 3. * df.zc_final) ** 2) * -1.
    df = get_om_b(df)
    df["max_om_b"] = df.om_b
    return df


"""
return one df with data input for als solubility tuning
"""

if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection

    well_name = "NORTHALASKAG"
    db = get_connection(well_name)
    print observed("poas_4", db, 400)
