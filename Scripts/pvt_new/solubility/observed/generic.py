import pandas as pd

from Scripts.pvt_new.core.pandas.dataframe import drop_not_in_cols
from Scripts.pvt_new.core.storage.mongo import fetch


def get_omega_tuning_data(i_eos, well_conn):
    eos = fetch(well_conn, property="pvt_output", identifier="tr", eos=i_eos)
    eos["alpha_eos_tr"] = eos.alpha_multiplier * eos.alpha_pr
    cols = ["omega_final", "alpha_eos_tr", "vb"]
    drop_not_in_cols(eos, cols)
    eos.rename(columns={"alpha_multiplier": "alpha_multiplier_tr", "vb": "vb_%s" % i_eos}, inplace=True)
    return eos


def get_cm_data(well_conn):
    df = fetch(well_conn, property="pvt_output", identifier="fcm")
    df.loc[df.tcbytb < 1.0001, "tcbytb"] = 1.0001
    cols = ["mf", "component", "tcbytb", "pc_mpa", "zc_final", "vc_final"]
    drop_not_in_cols(df, cols)
    return df


def get_eos_data(eos, well_conn):
    eos = fetch(well_conn, property="pvt_output", identifier="eosbp", eos=eos)
    eos["alpha_eos_bp"] = eos.alpha_multiplier * eos.alpha_pr
    cols = ["seq", "bp_final1", "tc_k", "alpha_l", "alpha_m", "alpha_multiplier", "tc_k", "alpha_eos_bp",
            "omega_multiplier", "beta_multiplier", "zc_multiplier", "om_b_multiplier", "vb"]
    drop_not_in_cols(eos, cols)
    eos.rename(columns={"alpha_multiplier": "alpha_multiplier_bp"}, inplace=True)
    return eos


def get_generic_input_data(eos, well_conn):
    cm = get_cm_data(well_conn)
    eos_df = get_eos_data(eos, well_conn)
    eos_tr = get_omega_tuning_data(eos, well_conn)
    poas4_tr = get_omega_tuning_data("poas_4", well_conn)
    df = pd.concat([cm, eos_df, eos_tr], axis=1)
    df["omega_final"] = poas4_tr["omega_final"]
    df["vb_poas_4"] = poas4_tr["vb_poas_4"]
    return df
