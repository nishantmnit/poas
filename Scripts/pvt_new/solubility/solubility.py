import importlib

import autograd.numpy as np  # Thinly-wrapped version of Numpy
import pandas as pd
from autograd import grad as gd

from Scripts.pvt_new.core.constants import valid_eos
from Scripts.pvt_new.core.get_logger import get_logger
from Scripts.pvt_new.core.optimizers.ncg import ncg as ncg_solver
from Scripts.pvt_new.core.storage.save import save
from calculated import calculated
from config import scales, start_point, var_range, var_names
from Scripts.pvt_new.core.constants import solubility_temperatures
from Scripts.pvt_new.cm.predict_om_b import predict_om_b
from Scripts.pvt_new.cm.predict_alpha import predict_alpha


logger = get_logger("pvt eos solubility")


def eos_cost(row_o, iter_vars, eos):
    row = row_o.copy(deep=True)
    for i in range(len(iter_vars)):
        row[var_names[i]] = iter_vars[i]
    cv = calculated(row, eos)
    objs = ["solubility", "v_liq"]
    costs = pd.Series()
    eos_scales = scales(eos)
    for i in range(len(objs)):
        costs[objs[i]] = 0 if row[objs[i]] == 0 else eos_scales[i] * (
                (row[objs[i]] - cv[objs[i]]) / row[objs[i]]) ** 2
    return costs.sum() + eos_scales[2] * max(0, cv.ph_diff - 10 ** -7) ** 2


def cost_for_ncg(iter_vars, args, only_cost=False):
    row, eos = args[0], args[1]
    cost = eos_cost(row, iter_vars, eos)
    if only_cost:
        return cost
    grad_fct = gd(eos_cost, 1)
    derivative = np.array([i.item(0) for i in grad_fct(row, iter_vars, eos)])
    return cost, derivative


def run_eos(eos, user_args, temperature):
    eos_module = importlib.import_module("Scripts.pvt_new.solubility.observed.%s" % eos)
    observed_df = getattr(eos_module, "observed")(eos, user_args.well_conn, temperature)
    observed_df.rename(columns={"omega_final": "omega", "vc_final": "vc"}, inplace=True)
    observed_df.set_index(observed_df.scn, drop=True, inplace=True)
    if eos == "poas_4" and temperature > 300:
        observed_df["multiplier"] = predict_om_b(user_args.well_conn, temperature)["om_b_multiplier"].values
        observed_df["alpha_multiplier"] = predict_alpha(user_args.well_conn, temperature)["alpha_multiplier"].values
    else:
        for index, row in observed_df.iterrows():
            logger.info("solubility tuning for well = %s, temperature = %s, eos = %s, scn = %s" %
                        (user_args.well_name, temperature, eos, index))
            ncg = ncg_solver()
            iter_vars = ncg.minimize(start_point(row, eos), var_range(row, eos), cost_for_ncg, max_iter=200,
                                     f_tol=10 ** -5, debug=user_args.debug, args=(row, eos))
            for i in range(len(iter_vars)):
                observed_df.loc[observed_df.seq == row.seq, var_names[i]] = iter_vars[i]
    calculated_df = calculated(observed_df, eos)
    eos_module = importlib.import_module("Scripts.pvt_new.eos.%s" % eos)
    calculated_df.rename(columns=getattr(eos_module, "var_rename"), inplace=True)
    save(calculated_df, user_args, identifier="solubility", eos=eos, temperature=temperature)


def run_eos_all_temperatures(eos, user_args):
    for temperature in solubility_temperatures:
        run_eos(eos, user_args, temperature)


def solubility(user_args):
    if user_args.eos == "all":
        eos_except_poas_4 = list(valid_eos)
        eos_except_poas_4.remove("poas_4")
        logger.info("Executing all EOS %s" % (", ".join(eos for eos in eos_except_poas_4)))
        for eos in eos_except_poas_4:
            run_eos_all_temperatures(eos, user_args)
    else:
        logger.info("Executing EOS = %s" % user_args.eos)
        run_eos_all_temperatures(user_args.eos, user_args)


"""
save one df with eos tuned solubility data
"""

if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection
    from Scripts.pvt_new.settings import DEBUG_DIR
    import os

    well_name = "AB8"

    debug_dir = os.path.join(DEBUG_DIR, well_name)

    db = get_connection(well_name)
    test_user_args = pd.Series({"debug": True, "well_conn": db, "bias": "general",
                                "debug_dir": debug_dir, "eos": "poas_4", "well_name": well_name})

    run_eos("poas_4",test_user_args, 350)
    # solubility(test_user_args)
