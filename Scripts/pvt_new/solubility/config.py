var_names = ["alpha_multiplier", "multiplier"]


def scales(eos):
    if eos != "poas_4":
        return [30., 200., 0.0000001]
    return [1., 200., 0.0000001]


def start_point(row, eos):
    if eos in ["srk_poas", "pr_poas", "generic_poas_a", "poas_4"]:
        return [row.expected_alpha_multiplier, row.om_b_multiplier]
    elif eos in ["sw_poas", "als_poas"]:
        return [row.expected_alpha_multiplier, row.omega_multiplier]
    elif eos == "pt_poas":
        return [row.expected_alpha_multiplier, row.zc_multiplier]
    elif eos == "poas_4a":
        return [row.expected_alpha_multiplier, row.beta_multiplier]


def var_range(row, eos):
    if eos in ["srk_poas", "pr_poas", "generic_poas_a", "poas_4"]:
        return [[row.alpha_multiplier_min, row.alpha_multiplier_max],
                [row.om_b_multiplier_min, row.om_b_multiplier_max]]
    elif eos in ["sw_poas", "als_poas"]:
        return [[row.alpha_multiplier_min, row.alpha_multiplier_max],
                [row.omega_multiplier_min, row.omega_multiplier_max]]
    elif eos == "pt_poas":
        return [[row.alpha_multiplier_min, row.alpha_multiplier_max], [row.zc_multiplier_min, row.zc_multiplier_max]]
    elif eos == "poas_4a":
        return [[row.alpha_multiplier_min, row.alpha_multiplier_max],
                [row.beta_multiplier_min, row.beta_multiplier_max]]


def cost_penalty(eos):
    if eos in ["srk_poas", "generic_poas_a", "poas_4", "als_poas"]:
        return [-75., -150.]
    elif eos in ["sw_poas", "pr_poas", "pt_poas", "poas_4a"]:
        return [-200., -1000.]
