import autograd.numpy as np  # Thinly-wrapped version of Numpy
import pandas as pd
from Scripts.pvt_new.core.constants import gasConstant, pressure


def get_zl_zv(data, penalty):
    if isinstance(data, pd.DataFrame):
        raise NotImplementedError("Not implemented")
    return zl_zv_row(data, penalty)


def zl_zv_row(row, penalty):
    row["r1"] = 0 if (row.u ** 2 - 4 * row.w) <= 0 else (row.u - np.sqrt(row.u ** 2 - 4 * row.w)) / 2
    row["r2"] = 0 if (row.u ** 2 - 4 * row.w) <= 0 else (row.u + np.sqrt(row.u ** 2 - 4 * row.w)) / 2
    row["A"] = row.ac_alpha * pressure * 10 ** 6 / (gasConstant ** 2 * row.temperature ** 2)
    row["B"] = (row.b * pressure * 10 ** 6 / (gasConstant * row.temperature))
    row["a1"] = (row.u * row.B - row.B - 1)
    row["a2"] = (row.A + row.w * row.B ** 2 - row.u * row.B - row.u * row.B ** 2)
    row["a3"] = -1. * (row.A * row.B + row.w * row.B ** 2 + row.w * row.B ** 3)
    row["Q"] = (3 * row.a2 - row.a1 ** 2) / 9
    row["L"] = (9 * row.a1 * row.a2 - 27 * row.a3 - 2 * row.a1 ** 3) / 54
    row["D"] = row.Q ** 3 + row.L ** 2
    row["S1"] = (-1. * (-1. * (row.L + np.sqrt(row.D))) ** (1. / 3.)) if (row.L + np.sqrt(row.D)) < 0 else (
                                                                                                                   row.L + np.sqrt(
                                                                                                               row.D)) ** (
                                                                                                                   1. / 3.)
    row["S2"] = (-1. * (-1. * (row.L - np.sqrt(row.D))) ** (1. / 3.)) if (row.L - np.sqrt(row.D)) < 0 else (
                                                                                                                   row.L - np.sqrt(
                                                                                                               row.D)) ** (
                                                                                                                   1. / 3.)
    row["Z1"] = 0 if row.Q >= 0 else 2. * np.sqrt(-row.Q) * np.cos(1. / 3. * (np.arccos(
        1. if np.abs(row.L / np.sqrt(-row.Q ** 3)) > 1 else row.L / np.sqrt(
            -row.Q ** 3))) + 2 * np.pi / 3.) - row.a1 / 3.
    row["Z2"] = 0 if row.Q >= 0 else 2. * np.sqrt(-row.Q) * np.cos(1. / 3. * (np.arccos(
        1. if np.abs(row.L / np.sqrt(-row.Q ** 3)) > 1 else row.L / np.sqrt(
            -row.Q ** 3))) + 4 * np.pi / 3.) - row.a1 / 3.
    row["Z3"] = 0 if row.Q >= 0 else 2. * np.sqrt(-row.Q) * np.cos(1. / 3. * (
        np.arccos(1. if np.abs(row.L / np.sqrt(-row.Q ** 3)) > 1 else row.L / np.sqrt(-row.Q ** 3)))) - row.a1 / 3.
    row["Z1_d"] = row.S1 + row.S2 - row.a1 / 3.
    row["Z1_dd"] = ((-1. * (-1. * row.L) ** 1. / 3.) if row.L < 0 else row.L ** (1. / 3.)) * 2. - row.a1 / 3.
    row["Z2_dd"] = (((-1. * (-1. * row.L) ** 1. / 3.) if row.L < 0 else row.L ** (1. / 3.)) + row.a1 / 3.) * -1
    row["Z3_dd"] = row.Z2_dd
    row["ZL"] = _min([row.Z1, row.Z2, row.Z3]) if row.D < 0 else (
        _min([row.Z1_dd, row.Z2_dd, row.Z3_dd]) if row.D == 0 else row.Z1_d)
    row["ZV"] = _max([row.Z1, row.Z2, row.Z3]) if row.D < 0 else (
        _max([row.Z1_dd, row.Z2_dd, row.Z3_dd]) if row.D == 0 else 0)
    row["phl"] = _fugacity(row.r1, row.r2, row.A, row.B, row.ZL, penalty[0])
    row["phv"] = _fugacity(row.r1, row.r2, row.A, row.B, row.ZV, penalty[1])
    row["ph_diff"] = np.abs(row.phl - row.phv)
    return row


def _max(data):
    data = [x for x in data if x > 0]
    if len(data) > 0:
        return max(data)
    return 0


def _min(data):
    data = [x for x in data if x > 0]
    if len(data) > 0:
        return min(data)
    return 0


def _fugacity(r1, r2, A, B, root, penalty):
    if (root - B) <= 0 or (B * (r2 - r1)) == 0 or (root + r1 * B) == 0 or (root + r2 * B) == 0 or (
            (root + r2 * B) * (root + r1 * B)) < 0:
        return penalty
    else:
        return (root - 1) - np.log(root - B) - (A / (B * (r2 - r1))) * np.log((root + r2 * B) / (root + r1 * B))
