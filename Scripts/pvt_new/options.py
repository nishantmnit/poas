import argparse
import os

from Scripts.pvt_new.core.constants import valid_eos
from Scripts.pvt_new.core.storage.mongo import get_connection
from Scripts.pvt_new.settings import DEBUG_DIR

# sequence is important
available_options = ("validate", "mass", "density", "bp", "ri", "cm", "ptmd", "eosbp", "tr", "solubility")


def user_options():
    parser = argparse.ArgumentParser()
    parser.add_argument('--well', dest='well_name', default='AB8', help='Well name AB8/ALUMM')
    parser.add_argument('--validate', dest='validate', default=False, action='store_true',
                        help='Provide this flag to just validate data.')
    parser.add_argument('--bias', dest='bias', default='general',
                        help='Bias - general/gamma/beta. Used in MDBR analysis')
    parser.add_argument('--mass', dest='mass', default=False, action='store_true',
                        help='Provide this flag for mass calculations.')
    parser.add_argument('--density', dest='density', default=False, action='store_true',
                        help='Provide this flag for density calculations.')
    parser.add_argument('--bp', dest='bp', default=False, action='store_true',
                        help='Provide this flag for bp calculations.')
    parser.add_argument('--ri', dest='ri', default=False, action='store_true',
                        help='Provide this flag for Refractivity calculations.')
    parser.add_argument('--cm', dest='cm', default=False, action='store_true',
                        help='Provide this flag for Component Modeling.')
    parser.add_argument('--ptmd', dest='ptmd', default=False, action='store_true',
                        help='Provide this flag for PT Matching data run after cm.')
    parser.add_argument('--eosbp', dest='eosbp', default=False, action='store_true',
                        help='Provide this flag for EOS at BP.')
    parser.add_argument('--tr', dest='tr', default=False, action='store_true',
                        help='Provide this flag for EOS at TR.')
    parser.add_argument('--solubility', dest='solubility', default=False, action='store_true',
                        help='Provide this flag for EOS Solubility.')
    parser.add_argument('--eos', dest='eos', default='all',
                        help='EOS to run after CM. POAS-4 is run as part of CM.')
    parser.add_argument('--temp', dest='utemp', default=380, help='Reservoir or reference temperature')
    parser.add_argument('--gas', dest='gas', default=False, action='store_true',
                        help='Pass this flag if the well is Gas Condensate')
    parser.add_argument('--debug', dest='debug', default=False, action='store_true', help='Run in debug mode')

    args = parser.parse_args()

    if not (args.mass or args.density or args.bp or args.ri or args.cm or args.eosbp or args.tr or args.solubility
            or args.validate or args.pvt_status or args.ptmd):
        print "Please provide at-least one flag to run the program"
        quit()
    if args.bias not in ["general", "gamma", "beta"]:
        print "Valid bias values are general/gamma and beta. %s" % args.bias
        quit()
    if (args.eosbp or args.tr or args.solubility) and (
            args.eos not in valid_eos and args.eos != "all"):
        print "Valid EOS values are all, %s" % (", ".join(eos for eos in valid_eos))
        quit()

    args.utemp = float(args.utemp)
    args.debug_dir = os.path.join(DEBUG_DIR, args.well_name, args.bias)
    args.well_conn = get_connection(args.well_name)

    return args
