import numpy as np

from generic import load_user_data


def calc_bo_fvf(dfs, gor):
    df = dfs["separator"]
    bo_fvf = None
    if df.loc[df.stage == "Total at Bubble Point"].shape[0] > 0:
        bo_fvf = df.loc[df.stage == "Total at Bubble Point", "bo_fvf"].values[0]
    if bo_fvf is None or np.isnan(bo_fvf):
        liq_density = df.loc[df.stage == "Total at Bubble Point", "liq_density"].values[0]
        liq_density = liq_density / 0.0160185
        api_gravity = float(dfs["general_data"]['value']['api_gravity'])
        specific_gravity_crude_oil = 141.5 / (131.5 + api_gravity)
        gas_specific_gravity = float(dfs["general_data"]['value']['gas_sp_gravity'])
        bo_fvf = (62.42797 * specific_gravity_crude_oil + 0.01357 * gor * gas_specific_gravity) / liq_density

    return bo_fvf


def calc_gor(dfs):
    df = dfs["separator"]
    gor = None
    if df.loc[df.stage == "Total at Bubble Point"].shape[0] > 0:
        gor = df.loc[df.stage == "Total at Bubble Point", "gor"].values[0]
    if gor is None or np.isnan(gor):
        gor = df.loc[df.stage != "Total at Bubble Point", "gor"].sum()
    return gor


def interpret_inputs(dfs):
    gor = calc_gor(dfs)
    bo_fvf = calc_bo_fvf(dfs, gor)


def separator_corrected(well_conn):
    user_inputs = load_user_data(well_conn)
    interpreted_inputs = interpret_inputs(user_inputs)


if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection

    db = get_connection("ALUMM")
    print separator_corrected(db)
