import numpy as np

from Scripts.pvt_new.core.get_logger import get_logger
from Scripts.pvt_new.core.optimizers.poas_interpolator import monotone
from generic import load_user_data, input_data, get_gor_data, multiple_bubble_points

logger = get_logger("gor case 1")


def calc_rsb(interpreted_inputs):
    if interpreted_inputs.stock_tank_oil_api <= 30:
        return (0.0362 * interpreted_inputs.average_gas_gravity * (interpreted_inputs.bubble_point_pressure ** 1.0937)
                * np.exp(25.724 * (interpreted_inputs.stock_tank_oil_api /
                                   (interpreted_inputs.reservoir_temp + 459.67))))
    else:
        return (0.0178 * interpreted_inputs.average_gas_gravity * (interpreted_inputs.bubble_point_pressure ** 1.187)
                * np.exp(23.931 * (interpreted_inputs.stock_tank_oil_api /
                                   (interpreted_inputs.reservoir_temp + 459.67))))


def temperature_pressure(well_conn):
    user_inputs = load_user_data(well_conn)
    interpreted_inputs = input_data(user_inputs)
    fluid_code = user_inputs["general_data"]["value"]["fluid_type"]
    logger.info("fluid_code = %s | RSB = %s" % (fluid_code, interpreted_inputs.rsb))

    if np.isnan(interpreted_inputs.rsb):
        interpreted_inputs.rsb = calc_rsb(interpreted_inputs)

    bppAtRsb = get_gor_data(interpreted_inputs.rsb, fluid_code)

    temp1 = np.arange(0, 10. * (len(bppAtRsb)), 10.)
    bbpAtRefTemp = monotone(temp1.tolist(), bppAtRsb.tolist(), [interpreted_inputs.reservoir_temp])[0]
    calibration_Mul = interpreted_inputs.bubble_point_pressure / bbpAtRefTemp

    bpp = bppAtRsb * calibration_Mul
    temp = np.arange(10., 5. * 101, 5.)  # 10 to 500 dg f
    bpp_calculated = np.array(monotone(temp1.tolist(), bpp.tolist(), temp.tolist()))
    if not user_inputs["bubble_points"].empty:
        bpp_calculated = multiple_bubble_points(temp, bpp_calculated, user_inputs, interpreted_inputs)
    return (temp - 32.) * 5. / 9. + 273.15, bpp_calculated / 145.038


if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection

    db = get_connection("ALUMM")
    print temperature_pressure(db)
