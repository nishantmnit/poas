from math import copysign

import autograd.numpy as np  # Thinly-wrapped version of Numpy
import pandas as pd
from autograd import grad as gd

from Scripts.pvt_new.core.get_logger import get_logger
from Scripts.pvt_new.core.optimizers.ncg import ncg
from Scripts.pvt_new.core.optimizers.poas_interpolator import monotone
from generic import load_user_data, input_data, get_gor_data, multiple_bubble_points

logger = get_logger("gor case 1")


def cost_for_ncg(iter_vars, args, only_cost=False):
    inputs, calculated_values = args
    cost = cost_function(iter_vars, inputs, calculated_values)
    if only_cost:
        return cost
    grad_fct = gd(cost_function, 0)
    derivative = np.array([i.item(0) for i in grad_fct(iter_vars, inputs, calculated_values)])
    return cost, derivative


def prepare_cost_inputs(inputs):
    ini_vars = np.array(
        [0.0006, 0.856, 0.351, 1.829, 1.462, -2.116, 3.867, -0.306, -0.083, -0.306, -0.288, 0.525, 0.01])
    pressure_observed = np.arange(inputs.pressure[0], inputs.pressure[-1], -10. if inputs.pressure[0] <= 150 else -20)
    pressure_observed = np.append(pressure_observed, [inputs.pressure[-1]])
    rs_observed = monotone(inputs.pressure.tolist(), inputs.rs.tolist(), pressure_observed.tolist())
    if 0 in rs_observed:
        zero_index = rs_observed.index(0)
    else:
        zero_index = len(rs_observed) + 1
    pr = pressure_observed / pressure_observed[0]
    objFunMul = [0.01 if (pr[i] <= 0.05) else 1. if (pr[i] <= 0.1) else 100. if (pr[i] > 0.2) else 50. for i in
                 range(len(pr))]
    calculated_data = pd.Series()
    calculated_data["ini_vars"] = ini_vars
    calculated_data["zero_index"] = zero_index
    calculated_data["pressure"] = pressure_observed
    calculated_data["pr"] = pr
    calculated_data["rs"] = rs_observed
    calculated_data["multiplier"] = objFunMul
    return calculated_data


def cost_function(iter_vars, inputs, cv, final=False):
    cv = cv.copy(deep=True)
    if not final:
        slice_cols = ["pressure", "pr", "rs", "multiplier"]
    else:
        slice_cols = ["rs"]
    for col in slice_cols:
        cv[col] = cv[col][:cv.zero_index]
    # objective
    As = np.array(iter_vars) * cv.ini_vars
    if iter_vars[12] == 0:
        term = (10. ** -3. * cv.ini_vars[12])
    elif abs(iter_vars[12]) <= 10. ** -3.:
        term = copysign(1., iter_vars[12]) * 10. ** -3. * cv.ini_vars[12]
    else:
        term = iter_vars[12] * cv.ini_vars[12]

    multiplier_factor = cv.pr ** term
    if (inputs.bubble_point_pressure ** As[1] > 10. ** 25 or inputs.average_gas_gravity ** As[
        2] > 10. ** 25 or inputs.reservoir_temp ** As[3] > 10. ** 25 or inputs.rsb ** As[
        5] > 10. ** 25 or inputs.stock_tank_oil_api ** As[4] > 10. ** 25 or (
            As[6] * inputs.bubble_point_pressure ** As[7] * inputs.average_gas_gravity **
            As[8] * inputs.reservoir_temp ** As[9] * inputs.stock_tank_oil_api ** As[10] * inputs.rsb ** As[11]) > 50.):
        rs_factor1 = -1000.
    else:
        rs_factor1 = As[0] * inputs.bubble_point_pressure ** As[1] * inputs.average_gas_gravity ** As[2] \
                     * inputs.reservoir_temp ** As[3] * inputs.stock_tank_oil_api ** As[4] * inputs.rsb ** As[5] \
                     * cv.pressure ** (As[6] * inputs.bubble_point_pressure ** As[7] * inputs.average_gas_gravity
                                       ** As[8] * inputs.reservoir_temp ** As[9] * inputs.stock_tank_oil_api
                                       ** As[10] * inputs.rsb ** As[11])

    rs_calc = rs_factor1 * multiplier_factor
    rs_len = len(cv.rs)
    obj_fun = [cv.multiplier[i] * (10. ** 50 if rs_calc[i] > 10. ** 50 else ((cv.rs[i] - rs_calc[i]) / cv.rs[i]) ** 2)
               for i in
               range(rs_len)]
    obj = sum(obj_fun)
    if final:
        return obj, rs_calc
    return obj


def solve(inputs):
    start_vars = np.ones(13)
    bounds = [[-3., 3.] for i in range(12)]
    bounds.append([-10., 5000.])
    calculated_values = prepare_cost_inputs(inputs)
    var_calc = ncg().minimize(start_vars, bounds, cost_for_ncg, max_iter=300, args=(inputs, calculated_values))
    obj, rs_calc = cost_function(calculated_values.ini_vars, inputs, calculated_values, final=True)
    rs_multiplier = calculated_values.rs / rs_calc
    rs_final = rs_calc * rs_multiplier
    f_vars = calculated_values.ini_vars * var_calc
    return rs_final, f_vars


def result_solver(inputs, f_vars):
    temp_deg_f_range = 5.
    temp_deg_f = np.arange(10., 1205., temp_deg_f_range)
    rsb = np.repeat(inputs.rsb, len(temp_deg_f))
    k1 = f_vars[0] * inputs.bubble_point_pressure ** f_vars[1] * inputs.average_gas_gravity ** f_vars[
        2] * temp_deg_f ** f_vars[3] * inputs.stock_tank_oil_api ** f_vars[4] * inputs.rsb ** f_vars[5]
    k2 = f_vars[6] * inputs.bubble_point_pressure ** f_vars[7] * inputs.average_gas_gravity ** f_vars[
        8] * temp_deg_f ** f_vars[9] * inputs.stock_tank_oil_api ** f_vars[10] * inputs.rsb ** f_vars[11]
    pb = np.exp((np.log(rsb) - np.log(k1)) / k2)
    pb_multiplier = inputs.bubble_point_pressure / monotone(temp_deg_f.tolist(), pb.tolist(),
                                                            [inputs.reservoir_temp])[0]
    final_bp_pressure = pb * pb_multiplier
    return final_bp_pressure


def get_bpp_using_multiplier(error_perc, bpp_interpolated_db, temp2, bp_pressure):
    error_perc_marker = np.array([0 if i >= 0.8 else 1 for i in error_perc])
    ones = np.where(error_perc_marker == 1)[0]
    if len(ones) == 0:
        final_bp_pressure = bpp_interpolated_db
    else:
        seOfOnes = np.split(ones, np.where(np.diff(ones) != 1)[0] + 1)[0]  # array of index of consecutive 1
        if len(seOfOnes) < 2:
            final_bp_pressure = bpp_interpolated_db
        elif 2 <= len(seOfOnes) <= 6:
            m1, mat1B = get_mat_b(seOfOnes[0:5], bp_pressure, bpp_interpolated_db, temp2)
            multiplier = np.array(
                [0.8 if ((mat1B[0] + mat1B[1] * i) < 0.8) else (mat1B[0] + mat1B[1] * i) for i in temp2])
            multiplier[seOfOnes[0]] = m1[0]
            multiplier[seOfOnes[1]:] = 1.
            final_bp_pressure = bpp_interpolated_db * multiplier
            final_bp_pressure[seOfOnes[1:-1]] = final_bp_pressure[seOfOnes[1:-1]]
        else:
            m1, mat1B = get_mat_b(seOfOnes[0:5], bp_pressure, bpp_interpolated_db, temp2)
            multiplier = np.array(
                [0.8 if ((mat1B[0] + mat1B[1] * i) < 0.8) else (mat1B[0] + mat1B[1] * i) for i in temp2])
            multiplier[seOfOnes[0]] = m1[0]
            multiplier[seOfOnes[1:-1]] = 1.
            m2, mat2B = get_mat_b(seOfOnes[5:][-5:], bp_pressure, bpp_interpolated_db, temp2)
            multiplier[seOfOnes[-1]] = m2[-1]
            if len(temp2) > (seOfOnes[-1] + 1):
                multiplier[seOfOnes[-1] + 1:] = np.array(
                    [0.8 if ((mat2B[0] + mat2B[1] * i) < 0.8) else (mat2B[0] + mat2B[1] * i) for i in
                     temp2[seOfOnes[-1] + 1:]])

            final_bp_pressure = bpp_interpolated_db * multiplier
            final_bp_pressure[seOfOnes[1:-1]] = final_bp_pressure[seOfOnes[1:-1]]
    return final_bp_pressure


def get_mat_b(seOfOnes, finalBP_pressure, bppInterpolated_db, temp2):
    m1 = finalBP_pressure[seOfOnes] / bppInterpolated_db[seOfOnes]
    t1 = np.array([np.ones(len(m1)), temp2[seOfOnes]]).T
    matA = t1.T.dot(t1)
    matB = np.linalg.inv(matA).dot(t1.T.dot(m1))
    return m1, matB


def final_bubble_point_pressure(inputs, final_bp_pressure, fluid_code):
    bpp_at_rsb = get_gor_data(inputs.rsb, fluid_code)[1:]
    temp1 = np.arange(10., 10. * (len(bpp_at_rsb) + 1), 10.)
    bpp_at_ref_temp = monotone(temp1.tolist(), bpp_at_rsb.tolist(), [inputs.reservoir_temp])[0]
    calibration_multiplier = inputs.bubble_point_pressure / bpp_at_ref_temp
    bpp = bpp_at_rsb * calibration_multiplier
    temp2 = np.arange(10., 5. * 101, 5.)  # 10 to 500 dg f
    bpp_interpolated_db = np.array(
        monotone(temp1.tolist(), bpp.tolist(), temp2.tolist()))
    final_bp_pressure = final_bp_pressure[0:len(bpp_interpolated_db)]
    error_perc = np.abs((bpp_interpolated_db - final_bp_pressure) / bpp_interpolated_db) * 100.
    bpp_calculated = get_bpp_using_multiplier(error_perc, bpp_interpolated_db, temp2, final_bp_pressure)
    return temp2, bpp_interpolated_db, bpp_calculated


def temperature_pressure(well_conn):
    user_inputs = load_user_data(well_conn)
    interpreted_inputs = input_data(user_inputs)
    fluid_code = user_inputs["general_data"]["value"]["fluid_type"]
    logger.info("fluid_code = %s | RSB = %s" % (fluid_code, interpreted_inputs.rsb))
    rs, my_vars = solve(interpreted_inputs)
    bubble_point_pressure = result_solver(interpreted_inputs, my_vars)
    temp, bppDb, bpp_calculated = final_bubble_point_pressure(interpreted_inputs, bubble_point_pressure, fluid_code)
    if not user_inputs["bubble_points"].empty:
        bpp_calculated = multiple_bubble_points(temp, bpp_calculated, user_inputs, interpreted_inputs)

    return (temp - 32.) * 5. / 9. + 273.15, bpp_calculated / 145.038


if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection

    db = get_connection("ALUMM")
    print temperature_pressure(db)
