import os

import numpy as np
import pandas as pd

from Scripts.pvt_new.core.get_logger import get_logger
from Scripts.pvt_new.core.optimizers.poas_interpolator import monotone
from Scripts.pvt_new.core.storage.mongo import fetch_all
from Scripts.pvt_new.settings import CSV_DATA

logger = get_logger("gor generic")

gor_data = os.path.join(CSV_DATA, "gor")


def get_gor_data(rsb, fluid_code):
    df = pd.read_csv(os.path.join(gor_data, "%s.csv" % fluid_code))
    if rsb in df.gor.to_list():
        return df.loc[df.gor == rsb, "pressure"].values
    else:
        all_gor = df.gor.unique()
        all_gor.sort()
        if np.where(all_gor > rsb)[0].shape[0] == 0:
            mx = all_gor[-1]
            mn = all_gor[-2]
        else:
            mx = min(all_gor[np.where(all_gor > rsb)])
            mn = all_gor[np.where(all_gor < rsb)]
            if mn.shape[0] == 0:
                mn = 0
            else:
                mn = max(mn)
        bppMx = df.loc[df.gor == mx, "pressure"].values
        if mn == 0:
            bppMn = np.zeros(len(bppMx))
        else:
            bppMn = df.loc[df.gor == mn, "pressure"].values
    return bppMn + (bppMx - bppMn) / (mx - mn) * (rsb - mn)


def enrich_general_data(df):
    if not df.empty:
        df.set_index(["param"], inplace=True, drop=True)
        df["value"]["temp_res"] = (df["value"]["temp_res"] - 273.15) * 9. / 5. + 32.
        df["value"]["pb_res_temp"] = (df["value"]["pb_res_temp"] * 145.038)
        df["value"]["gor_pb"] = (df["value"]["gor_pb"] / 0.178105620841002)
    return df


def enrich_bubble_points(df):
    if not df.empty:
        df["temperature"] = (df["temperature"] - 273.15) * 9. / 5. + 32.
        df["bubble_point"] = df["bubble_point"] * 145.038
    return df


def enrich_separator(df):
    if not df.empty:
        df["temperature"] = (df["temperature"] - 273.15) * 9. / 5. + 32.
        df["pressure"] = df["pressure"] * 145.038
        df["gor"] = df["gor"] / 0.178105620841002
    return df


def enrich_separator_corrected(df):
    if not df.empty:
        df["pressure"] = df["pressure"] * 145.038
        df["gor"] = df["gor"] / 0.178105620841002
    return df


def enrich_user_data(dfs):
    dfs["general_data"] = enrich_general_data(dfs["general_data"])
    dfs["bubble_points"] = enrich_bubble_points(dfs["bubble_points"])
    dfs["separator"] = enrich_separator(dfs["separator"])
    dfs["separator_corrected"] = enrich_separator_corrected(dfs["separator_corrected"])
    return dfs


def load_user_data(well_conn):
    dfs = dict()
    inputs = ["general_data", "separator", "separator_corrected", "dv_oil", "bubble_points", "cce", "cvd"]
    for poas_property in inputs:
        try:
            dfs[poas_property] = fetch_all(well_conn, property=poas_property)
        except:
            dfs[poas_property] = pd.DataFrame()
    dfs = enrich_user_data(dfs)
    return dfs


def separator_test_data(dfs, inputs):
    gas_sp_gravity_calc, api_gravity_calc = None, None
    try:
        df_separator1 = dfs["separator"][1:]
        if len(df_separator1) <= 2 and df_separator1['gas_gravity'].isnull().values.any():
            logger.warn("Require min 2 values to compute missing gas_gravity")
            return
        if len(df_separator1) > 2 and df_separator1['gas_gravity'].isnull().values.any():
            if df_separator1['gas_gravity'][0:2].isnull().values.any():
                logger.warn("Require initial 2 values to compute missing gas_gravity")
                return
            else:
                data_indices = []
                for index, row in df_separator1.iterrows():
                    if np.isnan(row['gas_gravity']):
                        break
                    data_indices.append(index)
                lnx = np.log(
                    np.array(df_separator1['pressure'][data_indices] / df_separator1['temperature'][data_indices]))
                lny = np.log(np.array(df_separator1['gas_gravity'][data_indices]))
                mata = np.array([np.ones(len(lnx)), lnx]).T
                matA = mata.T.dot(mata)
                matB = np.linalg.inv(matA).dot(mata.T.dot(lny))
                for index, row in df_separator1.iterrows():
                    if np.isnan(row['gas_gravity']): df_separator1.at[index, 'gas_gravity'] = np.exp(
                        matB[0] + matB[1] * np.log(
                            df_separator1['pressure'][index] / df_separator1['temperature'][index]))
        gas_sp_gravity_calc = (df_separator1['gor'] * df_separator1['gas_gravity']).sum() / df_separator1[
            'gor'].sum()
        gas_sp_gravity_calc = inputs.average_gas_gravity if (
                (gas_sp_gravity_calc * 0.99) <= inputs.average_gas_gravity <= (
                 gas_sp_gravity_calc * 1.01)) else gas_sp_gravity_calc
        api_gravity_calc = inputs.stock_tank_oil_api if (((141.5 / ((df_separator1['liq_density'].iloc[
            -1]) * 0.9991026) - 131.5) * 0.99) <= inputs.stock_tank_oil_api <= ((141.5 / (
                (df_separator1['liq_density'].iloc[-1]) * 0.9991026) - 131.5) * 1.01)) else (
                141.5 / ((df_separator1['liq_density'].iloc[-1]) * 0.9991026) - 131.5)
    except Exception as e:
        print "Error in calculation of gas_sp_gravity_calc, api_gravity_calc in SolutionGOR::input_solver , Error:%s" % (
            e.message)
    return gas_sp_gravity_calc, api_gravity_calc


def multiple_bubble_points(temp, bppCalculated, user_inputs, interpreted_inputs):
    logger.info("Found multipleBubblePoints")
    temp_res = {'temperature': interpreted_inputs.reservoir_temp,
                'bubble_point': interpreted_inputs.bubble_point_pressure}
    df_mbp_sorted = user_inputs["bubble_points"].append(temp_res, ignore_index=True)
    df_mbp_sorted.sort_values('temperature', inplace=True)
    df_mbp_sorted = df_mbp_sorted.reset_index(drop=True)
    df_mbp_sorted['calculated_pb'] = monotone(temp.tolist(), bppCalculated.tolist(),
                                              df_mbp_sorted['temperature'].tolist())
    df_mbp_sorted['multiplier'] = df_mbp_sorted['bubble_point'] / df_mbp_sorted['calculated_pb']
    df_mbp_sorted['multiplier_ln'] = np.log(df_mbp_sorted['multiplier'])
    mata = np.array([np.ones(len(df_mbp_sorted['temperature'])), df_mbp_sorted['temperature']]).T
    matA = mata.T.dot(mata)
    matB = np.linalg.inv(matA).dot(mata.T.dot(df_mbp_sorted['multiplier_ln']))
    temp_multiplier = [0.75 if (np.exp(matB[0] + matB[1] * i) < 0.75) else np.exp(matB[0] + matB[1] * i) for i in
                       temp]
    obs_multiplier = df_mbp_sorted['multiplier'][
        df_mbp_sorted.index[df_mbp_sorted['temperature'] == interpreted_inputs.reservoir_temp].tolist()[0]]
    ratio_correction = monotone(temp.tolist(), temp_multiplier, [interpreted_inputs.reservoir_temp])[
                           0] / obs_multiplier
    final_bubble_point_corrected = temp_multiplier * bppCalculated * ratio_correction
    return final_bubble_point_corrected


def input_data(dfs):
    inputs = pd.Series()
    # take from MongoDB user input. If not available then compute
    inputs["average_gas_gravity"] = float(dfs["general_data"]['value']['gas_sp_gravity'])
    inputs["reservoir_temp"] = float(dfs["general_data"]['value']['temp_res'])
    inputs["bubble_point_pressure"] = float(dfs["general_data"]['value']['pb_res_temp'])
    inputs["stock_tank_oil_api"] = float(dfs["general_data"]['value']['api_gravity'])
    inputs["rsb"] = float(dfs["general_data"]['value']['gor_pb'])
    if not dfs["separator_corrected"].empty:
        bubblePointPressureIdx = \
            (dfs["separator_corrected"]['pressure'] - inputs["bubble_point_pressure"]).abs().argsort()[0]
        inputs["pressure"] = np.array(
            dfs["separator_corrected"]['pressure'].iloc[bubblePointPressureIdx:].astype(float).tolist())
        inputs["rs"] = np.array(dfs["separator_corrected"]['gor'].iloc[bubblePointPressureIdx:].astype(float).tolist())
        gas_sp_gravity_calc, api_gravity_calc = separator_test_data(dfs, inputs)
        # if gas_sp_gravity_calc is not None and api_gravity_calc is not None and (averageGasGravity!=
        # gas_sp_gravity_calc or stockTankOilAPI!=api_gravity_calc) then show warning on UI.
        inputs["average_gas_gravity"] = gas_sp_gravity_calc
        inputs["stock_tank_oil_api"] = api_gravity_calc
    return inputs


if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection

    db = get_connection("AB8")
    ui = load_user_data(db)
    print input_data(ui)
