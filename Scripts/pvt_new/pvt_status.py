import json
import argparse
from Scripts.pvt_new.core.get_logger import get_logger
import os
import time
from Scripts.pvt_new.options import available_options
from collections import OrderedDict
from Lib.DBLib import getDbCon
from Scripts.pvt_new.core.storage.mongo import db, get_connection

logger = get_logger("pvt status")

# possible status values => pending, running, success, error, warning.

keys = available_options
default_status = {"status": "pending", "message": None, "end_time": 0, "start_time": 0, "start_button": "no"}


def user_options():
    parser = argparse.ArgumentParser()
    parser.add_argument('--well', dest='well_name', default=None, help='Well name like AB8/ALUMM')
    parser.add_argument('--module', dest='module', default=None, help='PVT module name')
    parser.add_argument('--status', dest='status', default=None, help='Set PVT Module status')
    parser.add_argument('--message', dest='message', default=None, help='Message')
    args = parser.parse_args()
    return args


def pvt_status(well_conn):
    print json.dumps(get_status(well_conn), indent=4)


def set_start_button(status):
    for module in keys:
        if status[module]["status"] == "running":
            return status
        if status[module]["status"] == "pending":
            status[module]["start_button"] = "yes"
            return status
        if status[module]["status"] == "error":
            status[module]["start_button"] = "yes"
            return status
    return status


def reset_start_button(status):
    for module in keys:
        status[module]["start_button"] = "no"
    return status


def get_status(well_conn):
    status_in_db = well_conn.find({"property": "pvt_status"})
    status = OrderedDict(zip(keys, [default_status.copy() for i in range(len(keys))]))
    if status_in_db.count() == 0:
        status = set_start_button(status)
        return status
    else:
        status_in_db = status_in_db[0]
        for module in keys:
            if module in status_in_db:
                status[module] = status_in_db[module]
    status = set_start_button(status)
    return status


def set_status(well_conn, module, status, message=None, json_style=True):
    current_status = get_status(well_conn)
    current_status = reset_start_button(current_status)
    if status == "running":  # reset end time and set start time and log file location.
        current_status[module]["start_time"] = time.time()
        current_status[module]["end_time"] = 0
        current_status[module]["log_file"] = os.environ['PyLogfile']

    if status in ["success", "error"]:  # set completion time.
        current_status[module]["end_time"] = time.time()

    if message is not None and json_style:  # set error message when error.
        try:
            msg = json.loads("{" + message.message.replace("Error:", "\"Error\":") + "}")["Error"]["Message"]
        except:
            msg = message
    else:
        msg = message

    if status == "pending":  # reset start and end time and log file.
        current_status[module]["start_time"] = 0
        current_status[module]["end_time"] = 0
        current_status[module]["log_file"] = None

    current_status[module]["status"] = status
    current_status[module]["message"] = msg

    # reset status of all modules after this module.
    module_index = keys.index(module)
    for i in range(module_index + 1, len(keys)):
        current_status[keys[i]] = default_status
    current_status = {'$set': current_status}
    well_conn.update({"property": "pvt_status"}, current_status, upsert=True)
    return get_status(well_conn)


def get_all_wells_status():
    pvt_conn = getDbCon(db)
    output = OrderedDict()
    wells = pvt_conn.list_collection_names()
    wells.sort()
    for collection in wells:
        output[collection] = get_status(get_connection(collection))
    return json.dumps(output, indent=4)


def run_pvt_status(args):
    if args.well_name is None:
        return get_all_wells_status()
    if args.module is not None and args.status is not None:
        well_conn = get_connection(args.well_name)
        set_status(well_conn, args.module, args.status, args.message, json_style=False)
    else:
        well_conn = get_connection(args.well_name)
        return pvt_status(well_conn)


if __name__ == "__main__":
    user_args = user_options()
    run_pvt_status(user_args)
