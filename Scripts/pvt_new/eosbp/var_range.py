def var_range(row, eos):
    if eos in ["sw_poas"]:
        bound = [[1.000001 / row.alpha_pr, 1.25], [0.01, 10.]]
    elif eos in ["als_poas"]:
        bound = [[1.000001 / row.alpha_pr, 1.25], [0.2, 5.]]
    elif eos in ["generic_poas_a"]:
        bound = [[1.000001 / row.alpha_pr, 1.25], [0.01, 20.]]
    elif eos in ["poas_4a"]:
        bound = [[1.000001 / row.alpha_pr, 1.25], [0.5, 1.7]]
    elif eos in ["srk_poas"]:
        bound = [[1.000001 / row.alpha_pr, 2.5], [0.1, 2.]]
    elif eos in ["pr_poas"]:
        bound = [[1.000001 / row.alpha_pr, 2.5], [0.1, 1.5]]
    elif eos in ["poas_4", "pt_poas"]:
        bound = [[1.000001 / row.alpha_pr, 1.25], [0.01, 4.]]
    else:
        bound = [[0.2, 3.], [0.01, 4.]]
    return bound
