from Scripts.pvt_new.core.constants import valid_eos


def define_var_names(my_eos):
    if my_eos in ["poas_4", "generic_poas_a", "pr_poas", "srk_poas"]:
        return ["alpha_multiplier", "multiplier"]
    elif my_eos in ["pt_poas"]:
        return ["alpha_multiplier", "multiplier"]
    elif my_eos in ["sw_poas", "als_poas"]:
        return ["alpha_multiplier", "multiplier"]
    elif my_eos in ["poas_4a"]:
        return ["alpha_multiplier", "multiplier"]


var_names = dict()

for eos in valid_eos:
    var_names[eos] = define_var_names(eos)


if __name__ == "__main__":
    print var_names
