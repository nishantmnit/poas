from Scripts.pvt_new.core.constants import valid_eos


def define_scales(my_eos):
    if my_eos == "poas_4":
        return [0.01, 1., 500.]
    elif my_eos == "pt_poas":
        return [0.14, 1., 150.]
    elif my_eos in ["sw_poas"]:
        return [0.14, 25., 200.]
    elif my_eos in ["als_poas"]:
        return [1., 10., 200.]
    elif my_eos in ["generic_poas_a"]:
        return [0.14, 1., 300.]
    elif my_eos in ["poas_4a"]:
        return [1., 1.5, 200.]
    elif my_eos in ["srk_poas"]:
        return [1., 1., 275.]
    elif my_eos in ["pr_poas"]:
        return [1., 15., 250.]


scales = dict()

for eos in valid_eos:
    scales[eos] = define_scales(eos)

if __name__ == "__main__":
    print scales
