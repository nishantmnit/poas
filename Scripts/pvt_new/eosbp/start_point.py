import numpy as np


def start_point(row, eos):
    iter_vars = list()
    if eos in ["poas_4"]:
        iter_vars = [(1.000001 / row.alpha_pr * 1.1) if (-0.0996900019304187 * np.log(
            row.name) + 1.18306731835084) < 1.000001 / row.alpha_pr * 1.1 else (
                -0.0996900019304187 * np.log(row.name) + 1.18306731835084),
                     0.0305405900409994 * np.log(row.name) + 1.14767398454079]
    elif eos in ["generic_poas_a"]:
        iter_vars = [(1.000001 / row.alpha_pr * 1.1) if (-0.0996900019304187 * np.log(
            row.name) + 1.18306731835084) < 1.000001 / row.alpha_pr * 1.1 else (
                -0.0996900019304187 * np.log(row.name) + 1.18306731835084),
                     0.0305405900409994 * np.log(row.name) + 1.14767398454079]
    elif eos == "pt_poas":
        iter_vars = [(1.000001 / row.alpha_pr * 1.1) if (-0.0996900019304187 * np.log(
            row.name) + 1.18306731835084) * 1.1 < 1.000001 / row.alpha_pr * 1.1 else (-0.0996900019304187 * np.log(
            row.name) + 1.18306731835084) * 1.1, 0.0483600098446223 * np.log(row.name) + 0.975636286416085]
    elif eos in ["sw_poas"]:
        iter_vars = [(1.000001 / row.alpha_pr * 1.05) if (-0.0996900019304187 * np.log(
            row.name) + 1.18306731835084) * 1.1 < 1.000001 / row.alpha_pr * 1.05 else (-0.0996900019304187 * np.log(
            row.name) + 1.18306731835084) * 1.1, 1.06134115451027 * np.exp(0.000488020527107394 * row.name)]
    elif eos in ["als_poas"]:
        iter_vars = [(1.000001 / row.alpha_pr * 1.1) if (-0.0996900019304187 * np.log(
            row.name) + 1.18306731835084) < 1.000001 / row.alpha_pr * 1.1 else (
                -0.0996900019304187 * np.log(row.name) + 1.18306731835084),
                     1.14437586198642 * np.exp(0.000613668469982046 * row.name) * 1.25]
    elif eos in ["poas_4a"]:
        iter_vars = [(1.000001 / row.alpha_pr * 1.1) if (-0.0996900019304187 * np.log(
            row.name) + 1.18306731835084) < 1.000001 / row.alpha_pr * 1.1 else (
                -0.0996900019304187 * np.log(row.name) + 1.18306731835084),
                     1.02676600315626 * np.exp(0.000572446998568102 * row.name) * 0.96]
    elif eos in ["pr_poas"]:
        iter_vars = [(1.000001 / row.alpha_pr * 1.1) if (0.053332778363657 * np.log(
            row.name) + 1.0903237635827) < 1.000001 / row.alpha_pr * 1.1 else (
                0.053332778363657 * np.log(row.name) + 1.0903237635827),
                     (0.994224780507426 * np.exp(-0.00304016543633199 * row.name))]
    elif eos in ["srk_poas"]:
        iter_vars = [(1.000001 / row.alpha_pr * 1.1) if (0.053332778363657 * np.log(
            row.name) + 1.0903237635827) * 1.05 < 1.000001 / row.alpha_pr * 1.1 else (0.053332778363657 * np.log(
            row.name) + 1.0903237635827) * 1.05, (0.994224780507426 * np.exp(-0.00304016543633199 * row.name))]
    iter_vars = [round(x, 8) for x in iter_vars]
    return iter_vars
