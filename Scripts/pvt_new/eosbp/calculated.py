import importlib

from var_names import var_names
from Scripts.pvt_new.core.optimizers.poas_interpolator import monotone
from Scripts.pvt_new.eos.get_zl_zv import get_zl_zv
from Scripts.pvt_new.core.constants import gasConstant, pressure
import autograd.numpy as np  # Thinly-wrapped version of Numpy
import pandas as pd

penalty = [-1., 1.]


def get_cost(row):
    row["t1"] = gasConstant * row.bp_final1 * (row.ZV - row.ZL)
    row["t2"] = 10 ** 6 if ((row.b * np.sqrt(row.u ** 2 - 4 * row.w)) == 0 or (row.u ** 2 - 4 * row.w) < 0) \
        else (row.ac * (row.bp_final1 * row.diff_alpha_wrt_temp - row.alpha_eos) /
              (row.b * np.sqrt(row.u ** 2 - 4 * row.w)))
    if (row.ZV + row.B * row.r2) == 0 or (row.ZV + row.B * row.r1) == 0 \
            or ((row.ZV + row.B * row.r2) * (row.ZV + row.B * row.r1)) < 0:
        row["t3"] = -1.
    else:
        row["t3"] = (np.log((row.ZV + row.B * row.r2) / (row.ZV + row.B * row.r1)))
    if ((row.ZL + row.B * row.r2) == 0 or (row.ZL + row.B * row.r1) == 0 or (
            (row.ZL + row.B * row.r2) * (row.ZL + row.B * row.r1)) < 0):
        row["t4"] = -5.
    else:
        row["t4"] = (np.log((row.ZL + row.B * row.r2) / (row.ZL + row.B * row.r1)))
    row["hv_bp"] = ((row.t1 + row.t2 * (row.t3 - row.t4)) / 1000.)
    row["vb"] = row.ZL * gasConstant * row.bp_final1 / pressure
    return row


def interpolate(x, y, row, col, ov, eos):
    val_c = monotone(x, y, row.seq)[0]
    if col not in var_names[eos] and eos != "poas_4":
        val_o = ov.loc[ov.seq == row.seq, col].values[0]
        if val_c > val_o * 1.05 or val_c < val_o * 0.95:
            val_c = val_o
    return val_c


def fix_negative(df, ov, eos):
    df["marker"] = df.apply(lambda row: 0 if row.ph_diff == 0 or row.ph_diff > 10 ** -2 else row.ph_diff, axis=1)
    x = df.loc[df.marker != 0, "seq"].tolist()
    for col in ["vb", "hv_bp"] + var_names[eos]:
        y = df.loc[df.marker != 0, col].tolist()
        df[col] = df.apply(lambda row: row[col] if row.marker != 0 else interpolate(x, y, row, col, ov, eos),
                           axis=1)
    return df


def calculated_df(df_o, eos):
    df = df_o.copy(deep=True)
    for index, row in df.iterrows():
        row = calculated_row(row, eos)
        for col in ["hv_bp", "vb", "Z1", "Z2", "Z3", "ZL", "ZV", "phl", "phv", "ph_diff"]:
            df.loc[df.seq == row.seq, col] = row[col]
    df = fix_negative(df, df_o, eos)
    return df


def calculated_row(row_o, eos):
    row = row_o.copy(deep=True)
    row["alpha_eos"] = row.alpha_pr * row.alpha_multiplier
    row["diff_alpha_wrt_temp"] = row.alpha_eos * (((2. * row.alpha_m - 2.) / (
            row.bp_final1 / row.tc_k) + row.alpha_l * (-2 * row.alpha_m) * (row.bp_final1 / row.tc_k) ** (
                                                           2 * row.alpha_m - 1)) / row.tc_k)

    eos_module = importlib.import_module("Scripts.pvt_new.eos.%s" % eos)
    row = getattr(eos_module, eos)(row)
    row = get_zl_zv(row, pressure, penalty)
    row = get_cost(row)
    return row


def calculated(data, eos):
    if not isinstance(data, pd.DataFrame):
        return calculated_row(data, eos)
    else:
        return calculated_df(data, eos)
