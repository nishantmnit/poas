import numpy as np

from Scripts.pvt_new.core.pandas.dataframe import drop_not_in_cols
from Scripts.pvt_new.core.storage.mongo import fetch
from Scripts.pvt_new.eos.get_om_b import get_om_b
from Scripts.pvt_new.eos.fix_hv_vb import fix_hv_vb


def observed(eos, well_conn):
    df = fetch(well_conn, property="pvt_output", identifier="fcm")
    cols = ["mf", "bp_final1", "tcbytb", "vc_final", "zc_final", "pc_mpa", "vb", "hv_bp", "alpha_l", "alpha_m",
            "tc_k", "seq", "component", "omega_final"]
    drop_not_in_cols(df, cols)
    df.loc[df.tcbytb < 1.0001, "tcbytb"] = 1.0001
    df["tc_k"] = df.bp_final1 * df.tcbytb
    df["alpha_pr"] = (
            (df.bp_final1 / df.tc_k) ** (2 * (df.alpha_m - 1))
            * (np.exp(df.alpha_l * (1 - (df.bp_final1 / df.tc_k) ** (2 * df.alpha_m))))
    )
    globals()[eos](df, well_conn)
    return df


def poas_4(*args):
    df = args[0]
    df["a1_d"] = (3. * df.zc_final - 1.75) * -1.
    df["a2_d"] = (3. * df.zc_final ** 2 + 0.5 * (1. - 3. * df.zc_final))
    df["a3_d"] = (df.zc_final ** 3 + 0.25 * (1. - 3. * df.zc_final) ** 2) * -1.
    get_om_b(df)
    df["max_om_b"] = df.om_b


def generic(df, well_conn):
    p4 = fetch(well_conn, property="pvt_output", identifier="eosbp", eos="poas_4")
    fix_hv_vb(p4, well_conn)
    df[["hv_bp", "vb"]] = p4[["hv_bp", "vb"]]
    df.rename(columns={"omega_final": "omega", "vc_final": "vc"}, inplace=True)


def srk_poas(df, well_conn):
    generic(df, well_conn)


def pr_poas(df, well_conn):
    generic(df, well_conn)


def poas_4a(df, well_conn):
    generic(df, well_conn)
    df["beta_est"] = 0.79132 * df.zc_final - 0.02207
    df["min_om_b"] = (0.02207 + 0.20868 * df.zc_final) * 0.215


def generic_poas_a(df, well_conn):
    generic(df, well_conn)
    df["a1_d"] = (3. * df.zc_final - 1.75) * -1.
    df["a2_d"] = (3. * df.zc_final ** 2 + 0.5 * (1. - 3. * df.zc_final))
    df["a3_d"] = (df.zc_final ** 3 + 0.25 * (1. - 3. * df.zc_final) ** 2) * -1.
    get_om_b(df)
    df["max_om_b"] = df.om_b


def als_poas(df, well_conn):
    generic(df, well_conn)


def sw_poas(df, well_conn):
    generic(df, well_conn)


def pt_poas(df, well_conn):
    generic(df, well_conn)


"""
return one df with data input for eosbp tuning
"""

if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection
    well_name = "AB8"
    test_eos = "pt_poas"
    db = get_connection(well_name)
    test_df = observed(test_eos, db)
    print test_df
