import importlib

from Scripts.pvt_new.core.get_logger import get_logger
from Scripts.pvt_new.core.storage.mongo import delete_all
from Scripts.pvt_new.core.constants import valid_eos

logger = get_logger("pvt eosbp run")

modules = ["eosbp"]


def delete_previous_data(user_args):
    if user_args.eos == "all":
        logger.info("Deleting all previous eosbp outputs")
        eos_except_poas_4 = list(valid_eos)
        eos_except_poas_4.remove("poas_4")
        for eos in eos_except_poas_4:
            delete_all(user_args.well_conn, property="pvt_output", identifier="eosbp", eos=eos)
    else:
        eos = user_args.eos
        logger.info("Deleting previous eos = %s outputs" % eos)
        delete_all(user_args.well_conn, property="pvt_output", identifier="eosbp", eos=eos)


def eosbp(user_args):
    delete_previous_data(user_args)
    for solver_module in modules:
        solver = importlib.import_module("Scripts.pvt_new.eosbp.%s" % solver_module)
        getattr(solver, solver_module)(user_args)


if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection
    import pandas as pd
    from Scripts.pvt_new.settings import DEBUG_DIR
    import os

    well_name = "AB8"

    debug_dir = os.path.join(DEBUG_DIR, well_name)

    db = get_connection(well_name)
    test_user_args = pd.Series({"debug": True, "well_conn": db, "bias": "general",
                                "debug_dir": debug_dir, "eos": "all", "well_name": well_name})
    eosbp(test_user_args)
