from Scripts.pvt_new.eosbp.eosbp import run_eos as eosbp
from Scripts.pvt_new.tr.tr import run_eos as tr
from Scripts.pvt_new.core.storage.mongo import delete_all
from Scripts.pvt_new.volume_solver.volume_solver import volume_solver
from Scripts.pvt_new.solubility.solubility import run_eos_all_temperatures as solubility
from Scripts.pvt_new.core.get_logger import get_logger

logger = get_logger("pvt mains pt matching data")

eos = "poas_4"


def ptmd(user_args):
    logger.info("starting pt matching data run.")
    delete_all(user_args.well_conn, property="pvt_output", identifier="eosbp", eos=eos)
    delete_all(user_args.well_conn, property="pvt_output", identifier="tr", eos=eos)
    delete_all(user_args.well_conn, property="pvt_output", identifier="solubility", eos=eos)
    delete_all(user_args.well_conn, property="pvt_output", identifier="volume_solver")
    logger.info("EOS BP Tuning POAS_4")
    eosbp(eos, user_args)
    logger.info("Volume Solver")
    volume_solver(user_args)
    logger.info("TR Tuning POAS_4")
    tr(eos, user_args)
    logger.info("Solubility Tuning POAS_4")
    solubility(eos, user_args)
    logger.info("completed pt matching data run.")


if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection
    from Scripts.pvt_new.settings import DEBUG_DIR
    import os
    import pandas as pd

    well_name = "AB8"

    debug_dir = os.path.join(DEBUG_DIR, well_name)

    db = get_connection(well_name)
    test_user_args = pd.Series({"debug": True, "well_conn": db, "bias": "general",
                                "well_name": well_name, "debug_dir": debug_dir, "eos": "all"})

    ptmd(test_user_args)
