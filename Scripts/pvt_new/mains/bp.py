import importlib

from Scripts.pvt_new.core.get_logger import get_logger
from Scripts.pvt_new.core.storage.fetch_most import set_recommended
from Scripts.pvt_new.core.storage.mongo import delete_all

logger = get_logger("pvt bp run")

modules = ["poas", "general", "gamma", "beta"]


def bp(user_args):
    logger.info("Deleting all previous bp outputs")
    delete_all(user_args.well_conn, property="pvt_output", identifier="bp")
    for solver_module in modules:
        solver = importlib.import_module("Scripts.pvt_new.bp.%s" % solver_module)
        getattr(solver, solver_module)(user_args)
        set_recommended(user_args.well_conn, "bp")


if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection
    import pandas as pd
    from Scripts.pvt_new.settings import DEBUG_DIR
    import os

    well_name = "ALUMM"

    debug_dir = os.path.join(DEBUG_DIR, well_name)

    db = get_connection(well_name)
    test_user_args = pd.Series({"debug": True, "well_conn": db, "bias": "general", "debug_dir": debug_dir})
    bp(test_user_args)
