import importlib
from options import user_options, available_options
from Scripts.pvt_new.core.get_logger import get_logger
from pvt_status import pvt_status, set_status


def run_pvt(user_args):
    for option in available_options:
        if getattr(args, option):
            module = importlib.import_module("Scripts.pvt_new.mains.%s" % option)
            set_status(user_args.well_conn, option, "running")
            try:
                getattr(module, option)(user_args)
                set_status(user_args.well_conn, option, "success")
            except Exception as e:
                logger.exception(e)
                set_status(user_args.well_conn, option, "error", e, json_style=False)
                raise Exception(e)


if __name__ == "__main__":
    args = user_options()
    logger = get_logger("pvt main %s" % args.well_name, master_module=True)
    run_pvt(args)
