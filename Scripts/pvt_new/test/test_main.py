import argparse
import os
import shutil
import socket
from multiprocessing import Pool

import Scripts.pvt_new.settings as settings
from Scripts.pvt_new.core.send_email import send_email
from Scripts.pvt_new.core.storage.excel import write_excel
from Scripts.pvt_new.core.test.compare import compare
from Scripts.pvt_new.options import available_options

all_wells = ["AB8", "ALUMM", "ASPHALTENE10", "AustraliaOilA", "BASEA", "BUAE471", "BahnousGC", "Bakken", "Brazil",
             "C10WHITSON", "CPAX2", "DGC", "Deepwater1G", "Deepwater1o", "GC2", "GC3", "GC4", "GC5", "GC6", "GC7",
             "HIGHSOURGAS", "IRANA", "IRANB", "IRANC", "IRANW3", "IRANWELLX8", "M132", "M152", "M161", "M161A", "M59",
             "MALAYSIA2", "MALAYSIA5", "MCCLAIN45", "MCCLAINC", "MU83", "NORTHALASKAG", "NorthAlskaA", "NorthAlskaB",
             "NorthAlskaE", "NorthAlskaF", "NorthSEA34", "OMAN4", "PhDAshok", "SAB1", "SHALEGC2", "SUDANA", "TOTALF3",
             "TOTBOIL2", "TOTGASC3", "TOTHEAVY1", "TRINIDADPL511", "TRINIDADPL515", "TURWELLB60", "TURWELLBSS1",
             "TURWELLDS1", "TURWELLE2", "U137", "U137_X1", "UAE30770", "UAEWA22", "WAXOIL2", "WAXOIL3", "WELL128",
             "WELLXXBOMOLE", "WHITSONCO", "WHITSONVO", "WestVirginia", "veryheavy"]


def getoptions():
    parser = argparse.ArgumentParser()
    parser.add_argument('--well', dest='well', default='', help='Well name MC/MC1/MC2')
    parser.add_argument('--bias', dest='bias', default='general',
                        help='Bias - general/gamma/beta. Used in MDBR analysis')
    user_args = parser.parse_args()
    if user_args.well == "":
        user_args.well = all_wells
    else:
        user_args.well = [user_args.well]
    return user_args


args = getoptions()


def run_process_mbp(well):
    print("starting well = %s" % well)
    pvt_main = os.path.join(settings.PVT_DIR, "main.py")
    cmd = "python %s --well %s --bias %s --debug" % (pvt_main, well, args.bias)
    for option in available_options:
        cmd = cmd + " --%s" % option
    os.system(cmd)
    print("completed well = %s" % well)


def clear_debug(wells):
    for well in wells:
        path = os.path.join(settings.DEBUG_DIR, well)
        if os.path.exists(path):
            shutil.rmtree(path)


def compare_debug(wells, bias):
    dfs = list()
    for well in wells:
        print "Auditing %s" % well
        dfs.append(compare(settings.GOLDEN_DIR, settings.DEBUG_DIR, well, bias))
    write_excel(settings.DEBUG_TEST_DIR, ["audit"], wells, dfs)


def start_test():
    send_email(subject="POAS PVT TEST ALERT", body="PVT Test has started on %s for following wells \n%s" % (
        socket.gethostname(), "\n".join(args.well)))
    clear_debug(args.well)

    num = 6
    if len(args.well) < 6:
        num = len(args.well)

    pool = Pool(processes=num)
    pool.map(run_process_mbp, args.well)
    compare_debug(args.well, args.bias)
    send_email(attachment=os.path.join(settings.DEBUG_TEST_DIR, "audit.xlsx"), subject="POAS PVT TEST ALERT",
               body="PVT Test has completed on %s for following wells \n%s" % (
                   socket.gethostname(), "\n".join(args.well)))


if __name__ == "__main__":
    start_test()

    # usage
    # nohup python test_main.py > test.out &
    # nohup python test_main.py --well AB8 > test.out &
