var_names = ["CH3", "CH2", "CH", "'C'", "(CH3)2CH", "(CH3)3C", "CH(CH3)CH(CH3)", "CH(CH3)C(CH3)2",
             "C(CH3)2C(CH3)2", "CH2 (cyclic)", "CH (cyclic)", "C (cyclic)", "CHcyc-CH3", "CHcyc-CH2",
             "CHcyc-CH", "CHcyc-C", "aCH", "aC-CH3", "aC-CH2", "aC-CH", "aC-C",
             "aC-CH(CH3)2 (If the number of C>8)", "aC-C(CH3)3 (If the number of C>9)",
             "aC fused with aromatic ring", "aC fused with non-aromatic ring",
             "'C' -others -fused (cyclic) as CH", "CH2SH", "CHSH", "CSH", "aC-SH", "CHcyc-SH",
             "CH3S (Sulfide)", "CH2S (Sulfide)", "CHS-", "CS-", "CHcyc-S-", "aC-S-", "'-S2-'", "CHcyc-NH2",
             "aC-NH2", "aC-NH", "aC-N", "CHcyc-OH", "aC-OH", "aC-COO"]
