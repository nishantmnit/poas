import numpy as np
import pandas as pd

from Scripts.pvt_new.core.get_logger import get_logger
from Scripts.pvt_new.core.storage.save import save
from calculated import calculated
from gradients import gradients
from observed import observed
from var_names import var_names

logger = get_logger("pvt cm fcm")


def _sp(row):
    return [3., row.ali_c * 0.5, 0.9, 0.2, 0, 0, 0, 0, 0, row.nap_c * 0.7, row.nap_c * 0.1,
            row.nap_c * 0.1, 0, 0, 0, 0, row.ar_ach_alk * 0.8, row.ar_ach_alk * 0.2, 0, 0, 0,
            0, 0, row.ar_cc * 0.8, (row.ar_cc * 0.2 + row.nap_cc * 0.8), row.nap_cc * 0.2, row.S / 12.,
            row.S / 12., row.S / 12., row.S / 5., row.S / 12., row.S / 12., row.S / 12., row.S / 12., row.S / 12.,
            row.S / 12., row.S / 6., row.S / 12., row.N / 4., row.N / 4., row.N / 4., row.N / 4., row.O / 3.,
            row.O / 3., row.O / 3.]


def _converged(pcost, ccost, iteration):
    cd = np.abs(pcost - ccost)
    # logger.info("Previous cost = %s, Current Cost = %s, Difference = %s" %(pcost, ccost, cd))
    if cd <= 10 ** -3 and iteration <= 50:
        return True
    elif cd <= 10 ** -2 and iteration > 50:
        return True
    return False


def regress(iter_vars, grad, alpha):
    niter_vars = np.array(iter_vars) - alpha * np.array(grad)
    niter_vars = [0 if x < 10 ** -5 else x for x in niter_vars]
    return niter_vars


def _grad(row, cv, scales, pbetaT):
    grad = gradients(row, cv, scales)
    betaT = np.sum(np.array(grad) ** 2)
    beta = betaT / pbetaT if pbetaT is not None else 0
    # logger.info("Gradients = %s | beta = %s" %(",".join([str(x) for x in grad]), beta))
    grad = np.array(grad) - beta
    return grad, betaT


def _cost(row, iter_vars):
    for i in range(len(iter_vars)):
        row[var_names[i]] = iter_vars[i]
    cv = calculated(row, False)
    objs = ["C", "H", "hbyc", "arc_ring", "nap_c", "ar_cc", "cc", "S", "O", "N", "mass_hc", "mass", "v60", "v20_wc",
            "v25_wc", "d60", "d20_wc", "rll_wc", "mnd20_wc", "rgd_wc", "lbmv_wc", "bp_cons", "bp_final", "nhc25",
            "par_wc", "bp_final1", "tc_cons", "pc_cons"]
    scales = [1500., 300., 100., 1., 1., 1., 1., 0.00001, 0.00001, 0.00001, 500., 1600., 300., 100., 100., 500.,
              100., 50., 300., 300., 300., 1., 1., 50., 400., 1., 1., 1]
    costs = pd.Series()
    for i in range(len(objs)):
        costs[objs[i]] = 0 if row[objs[i]] == 0 else scales[i] * ((row[objs[i]] - cv[objs[i]]) / row[objs[i]]) ** 2
    return scales, cv, costs.sum()


def _objective(row, iter_vars):
    if sum(iter_vars) <= 0:
        return 10 ** 150  # penalty if all variables are 0
    scales, cv, cost = _cost(row, iter_vars)
    return cost


def _alpha(row, iter_vars, grad):
    alphas, calphas, niter_vars = list(), list(), list()
    for i in [1, 4, 7, 9, 11, 13, 16, 18, 20, 22, 25, 28, 30, 32, 35, 38, 42, 45]:
        alpha = 0.75 * 3.15533101157586 * np.exp(-0.48288458350686 * i)
        alphas.append(alpha)
        new_iter_vars = np.array(iter_vars) - alpha * np.array(grad)
        new_iter_vars = [0 if x < 0 else x for x in new_iter_vars]
        niter_vars.append(new_iter_vars)
        calphas.append(_objective(row, new_iter_vars))
    m_index = calphas.index(min(calphas))
    m_alpha = alphas[m_index]
    m_cost = calphas[m_index]
    if m_index == 0 or m_index == len(alphas) - 1:
        return m_alpha
    else:
        alpha1, alpha2 = alphas[m_index - 1], alphas[m_index + 1]
        cost1, cost2 = calphas[m_index - 1], calphas[m_index + 1]
        a = (1. / (alpha1 - alpha2)) * (
                (cost1 - m_cost) / (alpha1 - m_alpha) - (m_cost - cost2) / (m_alpha - alpha2))
        b = ((cost1 - m_cost) / (alpha1 - m_alpha) - a * (alpha1 + m_alpha))
        m_alpha = (-1. * b / (2. * a))
    return m_alpha


def _solver(row):
    iter_vars = _sp(row)
    logger.info("FCM tuning for scn = %s | component = %s" % (row.name, row.component))
    _max, pcost, pbetaT = 1, 0, None
    while _max <= 100:
        scales, cv, cost = _cost(row, iter_vars)
        grad, pbetaT = _grad(row, cv, scales, pbetaT)
        alpha = _alpha(row, iter_vars, grad)
        niter_vars = regress(iter_vars, grad, alpha)
        if _converged(pcost, cost, _max):
            break
        iter_vars = niter_vars
        _max += 1
        pcost = cost
    logger.info(
        "FCM tuning for scn = %s | component = %s | iterations = %s" % (row.name, row.component, _max - 1))
    return iter_vars


def fcm(user_args):
    observed_df = observed(user_args.well_conn)
    for index, row in observed_df.iterrows():
        iter_vars = _solver(row)
        for i in range(len(iter_vars)):
            observed_df.loc[observed_df.seq == row.seq, var_names[i]] = iter_vars[i]
    calculated_df = calculated(observed_df, normalize=True)
    save(calculated_df, user_args, identifier="fcm")


"""
return one df with pcm tuned data
"""

if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection
    from Scripts.pvt_new.settings import DEBUG_DIR
    import os

    well_name = "GC3"

    debug_dir = os.path.join(DEBUG_DIR, well_name)

    db = get_connection(well_name)
    test_user_args = pd.Series({"debug": True, "well_conn": db, "bias": "general", "debug_dir": debug_dir})
    fcm(test_user_args)
