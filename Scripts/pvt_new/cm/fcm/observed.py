import numpy as np
import pandas as pd
from Scripts.pvt_new.core.storage.mongo import fetch


def _cc(df):
    df["cc"] = df.ar_cc + df.nap_cc


def _nap(df):
    df["nap_cc"] = df.nap5_cc + df.nap6_cc
    df["nap5_ring_wcc"] = df.apply(lambda row: row.nap5_ring if row.nap5_cc == 0 else (
        0 if (row.nap5_c - row.nap5_cc) < 0 else (row.nap5_c - row.nap5_cc) / 5.), axis=1)
    df["nap6_ring_wcc"] = df.apply(lambda row: row.nap6_ring if row.nap6_cc == 0 else (
        0 if (row.nap6_c - row.nap6_cc) < 0 else (row.nap6_c - row.nap6_cc) / 6.), axis=1)


def _tb3(df):
    df["tb3_ec_corr"] = df.apply(lambda row: 0 if row.ar_cc <= 1 else (0.32567 if row.ar_cc <= 2 else (
        (0.30855 * row.ar_cc + 1.26844) if row.ar_cc <= 5 else (0.30855 * row.ar_cc + 2.410036))
                                                                       ), axis=1)


def _lbmv(df):
    df["lbmv_corr"] = df.apply(lambda row: 15. * row.ar_ring if row.ar_ring <= 1 else (
            -0.00145403872692995 * row.ar_ring ** 2 + 15.7248472809213 * row.ar_ring - 0.513598790322472),
                               axis=1) * -1.


def _zeroCorrection(df):
    for col in ["arc_ring", "arh_ring", "no_of_db", "ar_cc", "ar_ach_alk", "ar_ring"]:
        df.loc[df[col] < 10 ** -5, col] = 0
    for col in ["S", "O", "N"]:
        df.loc[df[col] <= 10 ** -4, col] = 0


def _pc(df):
    df["pc"] = df.apply(lambda row: 0.1 if (row.tc / (
            6.27021835886156 * row.mass ** 1.81560190746867 * row.bp_final ** -1.23843125187169)) < 0.1 else (
            row.tc / (6.27021835886156 * row.mass ** 1.81560190746867 * row.bp_final ** -1.23843125187169)),
                        axis=1)
    df["pc_cons"] = (df.pc - 0.0519) ** -0.5 - 0.1155


def _tc(df):
    df["tc"] = df.apply(lambda row: (189.833 + 450.56 * row.density + (0.4244 + 0.1174 * row.density) * row.bp_final + (
                0.1441 - 1.00688 * row.density) * 10 ** 5 / row.bp_final) if row.name <= 10 else row.bp_final * (
        1.001 if (7.18182523524 * row.bp_final ** -0.263520396445593) <= 1.001 else (
                    7.18182523524 * row.bp_final ** -0.263520396445593)), axis=1)
    df["tc_cons"] = np.exp(df.tc / 181.6738)


def _bp(df):
    df["bp_wc"] = df.apply(lambda row: row.bp if row.name <= 10 else (
                53.1593293703321 * row.mass ** 0.489388591497609 * row.c_fr_wc ** 0.278746719870968), axis=1)
    df["bp_correction"] = df.apply(lambda row: 0 if row.bp_wc <= 700 else ((
                                                                                       3.0457118891455E-07 * row.bp_wc ** 3 - 0.00136805880969611 * row.bp_wc ** 2 + 1.21362118681932 * row.bp_wc - 285.388271671563) if row.bp_wc <= 1700 else (
                -669.288134986441 * np.log(row.bp_wc) + 4308.65467699613)), axis=1)
    df["bp_final"] = df.bp_wc + df.bp_correction
    df["bp_final1"] = df.bp_final
    df["bp_cons"] = np.exp(df.bp_final / 244.7889)


def observed(well_conn):
    sno = fetch(well_conn, property="pvt_output", identifier="sno")
    pcm = fetch(well_conn, property="pvt_output", identifier="pcm")
    df = pd.DataFrame()
    cols = ["component", "seq", "mass", "density", "d60", "v60", "bp", "nhc25", "S", "O", "N", "mf"]
    df[cols] = sno[cols]
    cols = ["ali_mass", "nap_mass", "ar_mass", "mass_hc", "C", "H", "c_fr_wc", "par_wc", "rll_wc", "mnd20_wc",
            "rgd_wc", "lbmv_wc", "d20_wc",
            "v25_wc", "v20_wc", "arc_ring", "arh_ring", "no_of_db", "ar_cc", "ar_ach_alk", "ar_ring",
            "nap_c", "nap_h", "nap5_cc", "nap6_cc", "nap5_ring", "nap6_ring", "nap5_c", "nap6_c", "ali_c", "ali_h"]
    df[cols] = pcm[cols]
    df["hbyc"] = df.H / df.C
    _bp(df)
    _tc(df)
    _pc(df)
    _zeroCorrection(df)
    _lbmv(df)
    _tb3(df)
    _nap(df)
    _cc(df)
    return df


"""
return one df with sno and pcm data enhanced for fcm run
"""

if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection

    well_name = "GC3"
    db = get_connection(well_name)
    print observed(db)
