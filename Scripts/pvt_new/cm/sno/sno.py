import numpy as np

from Scripts.pvt_new.core.get_logger import get_logger
from Scripts.pvt_new.core.storage.save import save
from calculated import calculated
from gradients import gradients
from observed import observed
from var_names import var_names

logger = get_logger("pvt cm sno")


def get_alphas(row, iter_vars, grad):
    alphas = [1.000000000000000000000, 0.700000000000000000000, 0.329173118055010000000, 0.203103455175091000000,
              0.120153241392875000000, 0.071482572280045300000, 0.045628333839522800000, 0.029810791974004500000,
              0.018558379763264400000, 0.011553314643162600000, 0.010788582916176700000, 0.007462570342551960000,
              0.005807176078545710000, 0.003615193420952190000, 0.002250598792618160000, 0.001401085456722320000,
              0.000872230298655383000, 0.000542997352689844000, 0.000459730338027106000, 0.000378794185967992000,
              0.000235814142792338000, 0.000228360990395213000, 0.000162472704000815000, 0.000151718384971501000,
              0.000149546794856164000, 0.000146997959686608000, 0.000137267937866327000, 0.000109814350293061000,
              0.000087851480234449000, 0.000070281184187559200, 0.000042168710512535500, 0.000021084355256267800,
              0.000014759048679387400, 0.000010331334075571200, 0.000005000000000000000]
    calphas = list()
    for alpha in alphas:
        calphas.append((100. * np.abs((row.mass - ((iter_vars[0] - alpha * grad[0]) * (12.011 + 1.008 * row.hbyc) + (
                iter_vars[1] - alpha * grad[1]) * 32.065 + (iter_vars[2] - alpha * grad[2]) * 15.9999 + (
                                                           iter_vars[3] - alpha * grad[
                                                       3]) * 14.0067)) / row.mass)) + (np.abs((row.nhc25 - (
                (iter_vars[0] - alpha * grad[0]) * (420 + 100.8 * row.hbyc) + (
                iter_vars[1] - alpha * grad[1]) * 340.2 - (iter_vars[2] - alpha * grad[2]) * 180.6 - (
                        iter_vars[3] - alpha * grad[3]) * 21)) / row.nhc25)) + (100 * np.abs((row.cfraction - (
                (iter_vars[0] - alpha * grad[0]) / (
                (iter_vars[0] - alpha * grad[0]) + (iter_vars[0] - alpha * grad[0]) * row.hbyc + (
                iter_vars[1] - alpha * grad[1]) + (iter_vars[2] - alpha * grad[2]) + (
                        iter_vars[3] - alpha * grad[3])))) / row.cfraction)))
    alpha = alphas[calphas.index(min(calphas))]
    mo = (100. * np.abs((row.mass - (
            iter_vars[0] * (12.011 + 1.008 * row.hbyc) + iter_vars[1] * 32.065 + iter_vars[2] * 15.9999 + iter_vars[
        3] * 14.0067)) / row.mass))
    alpha = 0 if alpha == alphas[-1] else (alphas[0] if mo >= 5 else (
        alpha * 1000. if mo >= 10 ** -3 and alpha <= 10 ** -4 else (
            alpha * mo if mo <= 0.1 and alpha > 0.01 else alpha)))
    return alpha


def get_residual(row, iter_vars):
    _cr = 0
    _sr = (-1. * (iter_vars[1] * 32.065) / (12.011 + 1.008 * row.hbyc)) if iter_vars[1] < 0 else 0
    _or = (-1. * (iter_vars[2] * 15.9999) / (12.011 + 1.008 * row.hbyc)) if iter_vars[2] < 0 else 0
    _nr = (-1. * (iter_vars[3] * 14.0067) / (12.011 + 1.008 * row.hbyc)) if iter_vars[3] < 0 else 0
    return [_cr, _sr, _or, _nr]


def new_iter_vars(iter_vars, residual):
    _c = iter_vars[0] + sum(residual)
    _s = 0 if iter_vars[1] < 0 else iter_vars[1]
    _o = 0 if iter_vars[2] < 0 else iter_vars[2]
    _n = 0 if iter_vars[3] < 0 else iter_vars[3]
    return [_c, _s, _o, _n]


def _sp(row):
    m_c = np.mean([row.min_c, row.max_c])
    c_sp = ((row.mass * (1. - (6.86692485698784 * np.log(row.name) - 11.0518843587855) * 1.2 / 100.)) / (
            12.011 + 1.008 * row.hbyc))
    c_sp = m_c if c_sp < m_c else c_sp
    sp = [c_sp, row.min_s * 2., row.min_o * 2., row.min_n * 2.]
    return sp


def regress(row, iter_vars, grad, alpha):
    cmin = ((1. - (15.9562646275333 * np.log(row.name) - 25.6805551275745) * 0.01) * row.mass) / (
            12.011 + 1.008 * row.hbyc)
    iter_vars = iter_vars - np.array(grad) * alpha
    residual = get_residual(row, iter_vars)
    niter_vars = new_iter_vars(iter_vars, residual)
    _c = (row.mass / (12.011 + 1.008 * row.hbyc) + 10 ** -5) \
        if niter_vars[1] < 10 ** -4 and niter_vars[2] < 10 ** -4 and niter_vars[3] < 10 ** -4 \
        else niter_vars[0]
    _c = cmin if cmin >= _c else _c
    niter_vars[0] = _c
    return niter_vars


def run_solver(row):
    iter_vars = _sp(row)
    logger.info("SNO tuning for scn = %s | component = %s | start point = %s" % (
        row.name, row.component, ",".join([str(x) for x in iter_vars])))
    _max = 1
    while _max <= 500:
        grad = gradients(row, iter_vars)
        alpha = get_alphas(row, iter_vars, grad)
        if alpha == 0:
            break
        iter_vars = regress(row, iter_vars, grad, alpha)
        _max += 1
    logger.info("SNO tuning for scn = %s | component = %s | end point = %s | iterations = %s" % (
        row.name, row.component, ",".join([str(x) for x in iter_vars]), _max))
    return iter_vars


def sno(user_args):
    observed_df = observed(user_args.well_conn)
    for index, row in observed_df.iterrows():
        iter_vars = run_solver(row)
        for i in range(len(iter_vars)):
            observed_df.loc[observed_df.seq == row.seq, var_names[i]] = iter_vars[i]
    calculated_df = calculated(observed_df)
    save(calculated_df, user_args, identifier="sno")


"""
return one df with sno (sulfur, nitrogen, oxygen) tuned data
"""

if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection
    import pandas as pd
    from Scripts.pvt_new.settings import DEBUG_DIR
    import os

    well_name = "GC3"

    debug_dir = os.path.join(DEBUG_DIR, well_name)

    db = get_connection(well_name)
    test_user_args = pd.Series({"debug": True, "well_conn": db, "bias": "general", "debug_dir": debug_dir})
    sno(test_user_args)
