import numpy as np

from Scripts.pvt_new.cm.get_mdbr_data import get_mdbr_data
from var_names import var_names


def nhc25(df_o):
    df = df_o.copy(deep=True)
    df["temp"] = ((-0.216971696050033 * (
            13.7119980393449 * (1. / df.ri20) ** 0.761356864307216) ** 3 + 6.64961268109592 * (
                           13.7119980393449 * (1. / df.ri20) ** 0.761356864307216) ** 2 - 67.7531281795701 * (
                           13.7119980393449 * (1. / df.ri20) ** 0.761356864307216) + 229.487004138441) + (
                          13.7119980393449 * (1. / df.ri20) ** 0.761356864307216)) * 4.186 * df.mass
    df.loc[df.mass > 300, "temp"] = 0.
    df.drop(df.loc[df.temp == 0].index, inplace=True)
    df["x"] = np.log(df.mass)
    df["y"] = np.log(df.temp)
    df["xiyi"] = df.x * df.y
    df["xi2"] = df.x ** 2
    xi = df.x.sum()
    yi = df.y.sum()
    xiyi = df.xiyi.sum()
    xi2 = df.xi2.sum()
    samples = df.shape[0]
    b = (xi * xiyi - yi * xi2) / (xi ** 2 - samples * xi2)
    a = (yi - samples * b) / xi
    df_o.loc[df_o.mass <= 300, "nhc25"] = df.temp
    df_o.loc[df_o.mass > 300, "nhc25"] = (np.exp(a * np.log(df_o.loc[df_o.mass > 300].mass) + b))


def hbyc(df):
    df["temp"] = (df.bp / (47.6112866933562 * df.mass ** 0.509665090106662)) ** (1. / 0.26807533679463)
    df["cfraction"] = df.apply(lambda row: 0.005 if row.temp < 0.005 else (0.99 if row.temp > 0.99 else row.temp),
                               axis=1)
    df["hbyc_ar2"] = 11.9147 / (3.4707 * (
        np.exp(1.485 * 10 ** -2 * df.bp + 16.94 * df.sg - 1.2492 * 10 ** -2 * df.bp * df.sg)) * df.bp ** (
                                    -2.725) * df.sg ** (-6.798))
    df["temp"] = (-0.0238805970149254 * df.index + 2.08955223880597 + 0.029850746268657)
    df["max_hbyc"] = (2. * df.index + df.apply(lambda row: 1. if row.temp < 1. else row.temp, axis=1)) / df.index
    df["hbyc"] = df.apply(
        lambda row:
        row.hbyc_ar2 if row.name <= 9 else
        ((-74.8366274408181 * row.cfraction ** 3 + 26.2199898293658 * row.cfraction ** 2 - 0.037645231242048 *
          row.cfraction + 1.9486415829277) if row.cfraction < 0.22 else
         ((0.331284194561323 * row.cfraction ** -1.62832651872397) if row.cfraction <= 0.45 else
          ((0.202399471147768 * row.cfraction ** -2.26386197332771) if row.cfraction <= 6. else
           (-1.29373735411075 * np.log(row.cfraction) - 0.0120988776134345)))), axis=1)
    df["hbyc"] = df.apply(lambda row: row.max_hbyc if row.hbyc > row.max_hbyc else row.hbyc, axis=1)
    df.drop(columns=["temp", "max_hbyc", "hbyc_ar2"], inplace=True)


def carbon(df):
    df["min_c"] = (df.index - df.index * (df.apply(
        lambda row: 30. if row.name >= 120 else (3.83202829994525 * np.exp(0.0175863848593195 * row.name)),
        axis=1) / 100.)) * 0.97
    df["max_c"] = (df.index + df.index * (df.apply(
        lambda row: 40. if row.name >= 120 else (3.83202829994525 * np.exp(0.0175863848593195 * row.name)),
        axis=1) / 100. + 0.1)) * 0.95


def sulfur(df):
    df["min_s"] = ((df.apply(
        lambda row: 0 if row.bp < (218. + 273.15) else (10.8957731840327 * np.exp(-0.225159084234834 * row.api)),
        axis=1) * df.mass / 100.) / 32.065) * 0.1
    df["max_s"] = ((df.apply(
        lambda row: 0 if row.bp < (218. + 273.15) else (13.2848517480824 * np.exp(-0.0799729918929779 * row.api)),
        axis=1) * df.mass / 100.) / 32.065) * 6


def oxygen(df):
    df["min_o"] = df.min_n * 1.05
    df["max_o"] = df.max_n * 3.


def nitrogen(df):
    df["min_n"] = ((df.apply(
        lambda row: 0 if row.bp < (218. + 273.15) else (2.03651477044276 * np.exp(-0.153706703092679 * row.api)),
        axis=1) * df.mass / 100.) / 14.0067) * 0.08
    df["max_n"] = ((df.apply(
        lambda row: 0 if row.bp < (218. + 273.15) else (2.45276763820172 * np.exp(-0.0813879816568074 * row.api)),
        axis=1) * df.mass / 100.) / 14.0067) * 3.


def observed(well_conn):
    df = get_mdbr_data(well_conn)
    df["sg"] = df.density / 0.999016
    df["d60"] = df.density
    df["d20"] = (df.d60 * 1000. * np.exp((-1. * 613.97226 / (df.d60 * 1000.) ** 2 * (20. - 15.) * (
            1. + 0.8 * 613.97226 / (df.d60 * 1000.) ** 2 * (20. - 15.))))) / 1000.
    df["d25"] = (df.d20 * 1000. * np.exp((-1. * 613.97226 / (df.d20 * 1000.) ** 2 * (25. - 20.) * (
            1. + 0.8 * 613.97226 / (df.d20 * 1000.) ** 2 * (25. - 20.))))) / 1000.
    df["v25"] = df.mass / df.d25
    df["v60"] = df.mass / df.d60
    df["rll20"] = df.R * df.mass / df.d20
    df["ri20"] = np.sqrt((1. + 2. * df.R) / (1. - df.R))
    df["mnd20"] = df.mass * df.ri20
    df["st"] = 34.39 * (((df.d20 + 0.1674) ** 14) / df.d20) ** (1. / 8.) - 7.509
    df["para"] = (df.st ** 0.25) * (df.mass / df.d20)
    df["api"] = df.apply(
        lambda row: ((row.sg / 1.43) ** (-1. / 0.15)) if row.sg > 1. else (141.5 / row.sg - 131.5),
        axis=1)
    df["vc_parachor"] = df.para / df.apply(
        lambda row: 0.72 if (-0.0257075535439389 * np.log(row.density) + 0.747511293729167) < 0.72 else (
                -0.0257075535439389 * np.log(row.density) + 0.747511293729167), axis=1)
    df["lbmv"] = 0.3445 * df.vc_parachor ** 1.0135
    nitrogen(df)
    oxygen(df)
    sulfur(df)
    carbon(df)
    hbyc(df)
    nhc25(df)
    for col in var_names:
        df[col] = np.nan
    df["seq"] = range(df.shape[0])
    return df


"""
return one df with all recommended mass, density, bp and ri data
"""

if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection

    well_name = "ALUMM"

    db = get_connection(well_name)
    print observed(db)
