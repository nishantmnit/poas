import numpy as np


def grad_0(row, iter_vars):
    return 100. * (np.sign((row.mass - (
            iter_vars[0] * (12.011 + 1.008 * row.hbyc) + iter_vars[1] * 32.065 + iter_vars[2] * 15.9999 + iter_vars[
        3] * 14.0067)) / row.mass) * (-1 / row.mass) * (12.011 + 1.008 * row.hbyc)) + (np.sign((row.nhc25 - (
            iter_vars[0] * (420 + 100.8 * row.hbyc) + iter_vars[1] * 340.2 - iter_vars[2] * 180.6 - iter_vars[
        3] * 21)) / row.nhc25) * (-1 / row.nhc25) * (420 + 100.8 * row.hbyc)) + 100 * (np.sign((row.cfraction - (
            iter_vars[0] / (iter_vars[0] + iter_vars[0] * row.hbyc + iter_vars[1] + iter_vars[2] + iter_vars[
        3]))) / row.cfraction) * (-1 / row.cfraction) * (iter_vars[1] + iter_vars[2] + iter_vars[3]) / (
                                                                                               iter_vars[0] +
                                                                                               iter_vars[
                                                                                                   0] * row.hbyc +
                                                                                               iter_vars[1] +
                                                                                               iter_vars[2] +
                                                                                               iter_vars[
                                                                                                   3]) ** 2)


def grad_1(row, iter_vars):
    return 100. * (np.sign((row.mass - (
            iter_vars[0] * (12.011 + 1.008 * row.hbyc) + iter_vars[1] * 32.065 + iter_vars[2] * 15.9999 + iter_vars[
        3] * 14.0067)) / row.mass) * (-1 / row.mass) * 32.065) + (np.sign((row.nhc25 - (
            iter_vars[0] * (420 + 100.8 * row.hbyc) + iter_vars[1] * 340.2 - iter_vars[2] * 180.6 - iter_vars[
        3] * 21)) / row.nhc25) * (-1 / row.nhc25) * 340.2) + 100 * (np.sign((row.cfraction - (iter_vars[0] / (
            iter_vars[0] + iter_vars[0] * row.hbyc + iter_vars[1] + iter_vars[2] + iter_vars[
        3]))) / row.cfraction) * (-1 / row.cfraction) * (-1 * iter_vars[0]) / (iter_vars[0] + iter_vars[
        0] * row.hbyc + iter_vars[1] + iter_vars[2] + iter_vars[3]) ** 2)


def grad_2(row, iter_vars):
    return 100. * (np.sign((row.mass - (
            iter_vars[0] * (12.011 + 1.008 * row.hbyc) + iter_vars[1] * 32.065 + iter_vars[2] * 15.9999 + iter_vars[
        3] * 14.0067)) / row.mass) * (-1 / row.mass) * 15.9999) + (np.sign((row.nhc25 - (
            iter_vars[0] * (420 + 100.8 * row.hbyc) + iter_vars[1] * 340.2 - iter_vars[2] * 180.6 - iter_vars[
        3] * 21)) / row.nhc25) * (1 / row.nhc25) * 180.6) + 100 * (np.sign((row.cfraction - (iter_vars[0] / (
            iter_vars[0] + iter_vars[0] * row.hbyc + iter_vars[1] + iter_vars[2] + iter_vars[
        3]))) / row.cfraction) * (-1 / row.cfraction) * (-1 * iter_vars[0]) / ((
            iter_vars[0] + iter_vars[0] * row.hbyc + iter_vars[1] + iter_vars[2] + iter_vars[3])) ** 2)


def grad_3(row, iter_vars):
    return 100. * (np.sign((row.mass - (
            iter_vars[0] * (12.011 + 1.008 * row.hbyc) + iter_vars[1] * 32.065 + iter_vars[2] * 15.9999 + iter_vars[
        3] * 14.0067)) / row.mass) * (-1 / row.mass) * 14.0067) + (np.sign((row.nhc25 - (
            iter_vars[0] * (420 + 100.8 * row.hbyc) + iter_vars[1] * 340.2 - iter_vars[2] * 180.6 - iter_vars[
        3] * 21)) / row.nhc25) * (1 / row.nhc25) * 21) + 100 * (np.sign((row.cfraction - (iter_vars[0] / (
            iter_vars[0] + iter_vars[0] * row.hbyc + iter_vars[1] + iter_vars[2] + iter_vars[
        3]))) / row.cfraction) * (-1 / row.cfraction) * (-1 * iter_vars[0]) / ((
            iter_vars[0] + iter_vars[0] * row.hbyc + iter_vars[1] + iter_vars[2] + iter_vars[3])) ** 2)


def gradients(row, iter_vars):
    grad0 = grad_0(row, iter_vars)
    grad1 = grad_1(row, iter_vars)
    grad2 = grad_2(row, iter_vars)
    grad3 = grad_3(row, iter_vars)
    return [grad0, grad1, grad2, grad3]
