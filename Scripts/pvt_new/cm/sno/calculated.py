import numpy as np


def d20_hc(df_o):
    df = df_o.copy(deep=True)
    df["wf_s"] = df.apply(lambda row: 0 if row.S == 0 else (row.S * 32.065) / row.mass, axis=1)
    df["wf_o"] = df.apply(lambda row: 0 if row.O == 0 else (row.O * 15.9999) / row.mass, axis=1)
    df["wf_n"] = df.apply(lambda row: 0 if row.N == 0 else (row.N * 14.0067) / row.mass, axis=1)
    df["wf_hc"] = 1. - (df.wf_s + df.wf_n + df.wf_o)
    df["min_d20_hc"] = df.apply(
        lambda row:
        (-8.82519803114407E-06 * row.C ** 2 + 0.00331822386370742 * row.C + 0.977013330741513) *
        ((2.8868630945E-10 * row.C ** 6 + 2.0729620662553E-07 * row.C ** 5 - 0.0000225661467711281 *
          row.C ** 4 + 0.000901638009900008 * row.C ** 3 - 0.0173333070960121 * row.C ** 2 +
          0.166058512570425 * row.C + 0.115680916261106)
         if row.C <= 30 else (0.0604102877761363 * np.log(row.C) + 0.606186312956607))
        if row.C > 10 else (2.8868630945E-10 * row.C ** 6 + 2.0729620662553E-07 * row.C ** 5 -
                            0.0000225661467711281 * row.C ** 4 + 0.000901638009900008 * row.C ** 3 -
                            0.0173333070960121 * row.C ** 2 + 0.166058512570425 * row.C + 0.115680916261106), axis=1)
    df["d20_hc"] = df.apply(lambda row: row.d20 if row.C <= 6 else ((row.min_d20_hc * 1.06) if (row.wf_hc / (
            1. / row.d20 - row.wf_s / 2.5 - row.wf_o / 1.62 - row.wf_n / 1.542)) < row.min_d20_hc else (
            row.wf_hc / (1. / row.d20 - row.wf_s / 2.5 - row.wf_o / 1.62 - row.wf_n / 1.542))), axis=1)
    df["d20_wc"] = df.apply(
        lambda row: row.d20 if row.C <= 6
        else ((1. / (row.wf_hc / row.min_d20_hc + row.wf_s / 2.5 + row.wf_o / 1.62 + row.wf_n / 1.542))
              if row.d20 < (1. / (row.wf_hc / row.min_d20_hc + row.wf_s / 2.5 + row.wf_o / 1.62 + row.wf_n / 1.542))
              else row.d20), axis=1)
    df_o[["d20_hc", "d20_wc"]] = df[["d20_hc", "d20_wc"]]


def fix_iter_vars(df, obs):
    df["S"] = df.apply(lambda row: row.min_s if row.S < row.min_s else row.S, axis=1)
    df["O"] = df.apply(lambda row: row.min_o if row.O < row.min_o else row.O, axis=1)
    df["N"] = df.apply(lambda row: row.min_n if row.N < row.min_n else row.N, axis=1)
    df["C"] = obs.C - ((df.S - obs.S) * 32.065 - (df.O - obs.O) * 15.9999 - (df.N - obs.N) * 14.0067) / 12.011


def _refractivity(row):
    if row.d20_hc <= 0.65:
        return -0.771988205690377 * row.d20_hc ** 2 + 1.25683786884901 * row.d20_hc - 0.263582391020546
    elif row.d20_hc <= 0.685:
        return 0.323066708963071 * row.d20_hc ** 0.825367199519558
    elif row.d20_hc <= 0.694:
        return 9.33747573594155 * row.d20_hc ** 2 - 12.6643155256813 * row.d20_hc + 4.52953399612808
    elif row.d20_hc <= 0.7:
        return 45.2446673429513 * row.d20_hc ** 2 - 62.3150182598514 * row.d20_hc + 21.6929685696088
    elif row.d20_hc <= 0.74:
        return 0.310905890442151 * row.d20_hc ** 0.697222127007676
    elif row.d20_hc <= 0.9:
        return 0.131843282949603 * row.d20_hc ** 2 + 0.100833492150919 * row.d20_hc + 0.100895494309563 + 0.004
    elif row.d20_hc <= 1.17:
        return 0.280866083905693 * row.d20_hc ** 2 - 0.209842167791342 * row.d20_hc + 0.254487058045577 + 0.009
    elif row.d20_hc <= 2.8:
        return -0.126906413488938 * row.d20_hc ** 2 + 0.651119502049113 * row.d20_hc - 0.185838323984245
    else:
        return 0.247678193996086 * np.log(row.d20_hc) + 0.375251277321885 + 0.012580930886738


def _d20_por(row):
    if row.C <= 6:
        return 0.867
    elif row.C <= 15:
        return -0.0003 * row.C ** 3 + 0.0149 * row.C ** 2 - 0.1661 * row.C + 1.3977 + 0.004179
    else:
        return 0.300637954533737 * np.log(row.C) + 0.426327227650635


def _d20_ali(row):
    if row.C <= 30:
        return 2.8868630945E-10 * row.C ** 6 + 2.0729620662553E-07 * row.C ** 5 - 0.0000225661467711281 * \
               row.C ** 4 + 0.000901638009900008 * row.C ** 3 - 0.0173333070960121 * row.C ** 2 + 0.166058512570425 * \
               row.C + 0.115680916261106
    else:
        return 0.0604102877761363 * np.log(row.C) + 0.606186312956607


def _d20_nap(row):
    if row.C <= 15:
        return -0.00012079625145045 * row.C ** 4 + 0.00519493513767344 * row.C ** 3 - 0.0808917600746053 * \
               row.C ** 2 + 0.554624548015966 * row.C - 0.579097973264385
    else:
        return 0.173440291796096 * np.log(row.C) + 0.487535073065162


def calculated(observed_df):
    df = observed_df
    fix_iter_vars(df, observed_df)
    df["H"] = (df.mass - df.C * 12.011 - df.S * 32.065 - df.O * 15.9999 - df.N * 14.0067) / 1.008
    df["cfraction"] = df.C / (df.S + df.O + df.N + df.H)
    df["hbyc"] = df.H / df.C
    df["bp"] = df.apply(lambda row: row.bp if row.name <= 6 else (
            47.6112866933562 * row.mass ** 0.509665090106662 * row.cfraction ** 0.26807533679463), axis=1)
    df["nhc25"] = df.C * (420 + 0.24 * 420 * df.hbyc) + 340.2 * df.S - 180.6 * df.O - 21 * df.N
    df["lbmv"] = df.lbmv - (25.6 * df.S + 7.4 * df.O + 11.25 * df.N)
    df["mass_hc"] = 12.011 * df.C + 1.008 * df.H
    df["cfraction_hc"] = df.C / (df.C + df.H)
    df["bp_hc"] = df.apply(lambda row: row.bp if row.name <= 6 else (
            47.6112866933562 * row.mass_hc ** 0.509665090106662 * row.cfraction_hc ** 0.26807533679463), axis=1)
    d20_hc(df)
    df["d25_hc"] = ((df.d20_hc * 1000. * np.exp((-1. * (613.97226 / (df.d20_hc * 1000.) ** 2) * (25. - 20.) * (
            1. + 0.8 * (613.97226 / (df.d20_hc * 1000.) ** 2) * (25. - 20.))))) / 1000.)
    df["R_20_hc"] = df.apply(lambda row: _refractivity(row), axis=1)
    df["ri_hc"] = np.sqrt((1. + 2. * df.R_20_hc) / (1. - df.R_20_hc))
    df["mbynd20_hc"] = df.mass_hc / df.ri_hc
    df["rll_hc"] = df.mass_hc / df.d20_hc * df.R_20_hc
    df["mnd20_hc"] = df.mass_hc * df.ri_hc
    df["st_hc"] = 34.39 * (((df.d20_hc + 0.1674) ** 14) / df.d20_hc) ** (1. / 8.) - 7.509
    df["para_hc"] = (df.st_hc ** 0.25) * (df.mass_hc / df.d20_hc)
    df["d20_por"] = df.apply(lambda row: _d20_por(row), axis=1)
    df["d20_ali"] = df.apply(lambda row: _d20_ali(row), axis=1)
    df["d20_nap"] = df.apply(lambda row: _d20_nap(row), axis=1)
    df["v25"] = df.mass_hc / df.d25_hc
    df["mbynd20_dep"] = df.apply(
        lambda row: 0 if (row.mbynd20_hc - (9.5013636627887 * row.C + 5.8626784355479)) > 0 else (
                row.mbynd20_hc - (9.5013636627887 * row.C + 5.8626784355479)), axis=1)
    return df
