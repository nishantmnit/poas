import pandas as pd

from Scripts.pvt_new.core.get_logger import get_logger
from Scripts.pvt_new.core.storage.fetch_most import validated_data, recommended

logger = get_logger("get mdbr data")


def get_mdbr_data(well_conn):
    df, dfplus_o = validated_data(well_conn)
    df.drop(df.loc[df.index > 5].index, inplace=True)
    df.drop(columns=["expbp", "expri", "scn"], inplace=True)
    df.rename(columns={'expmass': 'mass', "expdensity": "density"}, inplace=True)
    dfmass, dfplusmass = recommended(well_conn, "mass")
    dfdensity, dfplusdensity = recommended(well_conn, "density")
    df_bp = recommended(well_conn, "bp")
    df_ri = recommended(well_conn, "ri")
    dfmass_bkp = dfmass.copy(deep=True)
    dfmass.drop(dfmass.loc[dfmass.index <= 5].index, inplace=True)
    dfdensity.drop(dfdensity.loc[dfdensity.index <= 5].index, inplace=True)
    df = pd.concat([df, dfmass], sort=False)
    df.loc[df.index > 5, "density"] = dfdensity["density"]
    df["mass"] = dfmass_bkp["mass"]
    df = make_plus(df, df_bp)
    df["bp"] = df_bp["bp"]
    df["R"] = df_ri["ri"]  # self is refractivity at 20 deg c
    df.drop(df.loc[df.index < 5].index, inplace=True)
    return df


def make_plus(df_o, df_bp):
    df = df_o.copy(deep=True)
    bp_m = df_bp.index.max()
    m_m = df.index.max()
    if bp_m < m_m:
        df.drop(df.loc[df.index > bp_m].index, inplace=True)
        df.loc[df.index == bp_m, "mf"] = df_o.loc[df_o.index >= bp_m, "mf"].sum()
        df_o["zimi"] = df_o.mass * df_o.mf
        df.loc[df.index == bp_m, "mass"] = df_o.loc[df_o.index >= bp_m, "zimi"].sum() / df_o.loc[
            df_o.index >= bp_m, "mf"].sum()
        df_o["zimibysgi"] = df_o.zimi / df_o.density
        df.loc[df.index == bp_m, "density"] = df_o.loc[df_o.index >= bp_m, "zimi"].sum() / df_o.loc[
            df_o.index >= bp_m, "zimibysgi"].sum()
    return df


"""
return one df with all recommended mass, density, bp and ri data
"""

if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection

    well_name = "GC3"

    db = get_connection(well_name)
    print get_mdbr_data(db)
