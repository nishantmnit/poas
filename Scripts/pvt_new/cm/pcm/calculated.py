import numpy as np


def fix_iter_vars(df, obs):
    obs.loc[obs.C <= 5.5, "Ar_wf"] = 0
    df["Ali_wf"] = obs.Ali_wf / (obs.Ali_wf + obs.Nap_wf + obs.Ar_wf)
    df["Nap_wf"] = obs.Nap_wf / (obs.Ali_wf + obs.Nap_wf + obs.Ar_wf)
    df["Ar_wf"] = obs.Ar_wf / (obs.Ali_wf + obs.Nap_wf + obs.Ar_wf)


def mass(df):
    df["ali_mass"] = df.mass_hc * df.Ali_wf
    df["nap_mass"] = df.mass_hc * df.Nap_wf
    df["ar_mass"] = df.mass_hc * df.Ar_wf
    df["nap5_mass"] = df.nap_mass * df.Nap5_wf
    df["nap6_mass"] = df.apply(
        lambda row: 0 if (row.nap_mass - row.nap5_mass) < 0 else (row.nap_mass - row.nap5_mass), axis=1)
    df["mass_hc"] = df.ali_mass + df.nap_mass + df.ar_mass


def _nap5_cc(row):
    if (2 * row.nap5_ring - 2) >= (0 if row.C <= 8 else ((0.25 * row.nap5_c) if row.nap5_c <= 8 else (0.666666666666667 * row.nap5_c - 3.33333333333333))):
        return 2 * row.nap5_ring - 2
    else:
        return (0 if row.C <= 8 else (
            (0.25 * row.nap5_c) if row.nap5_c <= 8 else (0.666666666666667 * row.nap5_c - 3.33333333333333)))


def _nap6_cc(row):
    if (2 * row.nap6_ring - 2) >= (0 if row.C <= 10 else ((0.2 * row.nap6_c) if row.nap6_c <= 10 else (0.496539162112933 * row.nap6_c - 2.7344262295082))):
        return 2 * row.nap6_ring - 2
    elif row.C <= 10:
        return 0
    else:
        return (0.2 * row.nap6_c) if row.nap6_c <= 10 else (0.496539162112933 * row.nap6_c - 2.7344262295082)


def _nap6_c(row):
    if (row.mass_hc * row.Nap_wf - row.mass_hc * row.Nap_wf * row.Nap5_wf) <= 85:
        return (row.mass_hc * row.Nap_wf - row.mass_hc * row.Nap_wf * row.Nap5_wf) * 0.0712910814857061
    elif ((row.mass_hc * row.Nap_wf - row.mass_hc * row.Nap_wf * row.Nap5_wf) * 0.0712910814857061) >= (0.0681542447698304 * (0 if (row.mass_hc * row.Nap_wf - row.mass_hc * row.Nap_wf * row.Nap5_wf) <= 0 else (row.mass_hc * row.Nap_wf - row.mass_hc * row.Nap_wf * row.Nap5_wf)) ** 1.01250029139992):
        return (row.mass_hc * row.Nap_wf - row.mass_hc * row.Nap_wf * row.Nap5_wf) * 0.0712910814857061
    else:
        return (0.0681542447698304 * (
            0 if (row.mass_hc * row.Nap_wf - row.mass_hc * row.Nap_wf * row.Nap5_wf) <= 0 else (
                        row.mass_hc * row.Nap_wf - row.mass_hc * row.Nap_wf * row.Nap5_wf)) ** 1.01250029139992)


def _mbynd20_dep_ar(row, iter_vars):
    mbynd20_dep_ar = (-0.138691077327068 * (iter_vars[2] * row.mass_hc)) if (iter_vars[2] * row.mass_hc) <= 78.114 else ((-2.5767E-16 * (iter_vars[2] * row.mass_hc) ** 6 + 1.25570849E-12 * (iter_vars[2] * row.mass_hc) ** 5 - 2.33339465412E-09 * (iter_vars[2] * row.mass_hc) ** 4 + 2.05922029150513E-06 * (iter_vars[2] * row.mass_hc) ** 3 - 0.000903990644090139 * (iter_vars[2] * row.mass_hc) ** 2 - 0.15864057854991 * (iter_vars[2] * row.mass_hc) + 10.4586452565478) if (iter_vars[2] * row.mass_hc) <= 1350 else (-0.0000112642961817011 * (iter_vars[2] * row.mass_hc) ** 2 - 0.362846148407561 * (iter_vars[2] * row.mass_hc) + 45.8178034639216))
    return mbynd20_dep_ar


def _mbynd20_dep_nap(row, iter_vars):
    mbynd20_dep_nap = ((-0.0499691144951728 * (row.mass_hc * iter_vars[1] * iter_vars[3])) if (row.mass_hc * iter_vars[1] * iter_vars[3]) <= 70.135 else ((6.64124123196186E-06 * (row.mass_hc * iter_vars[1] * iter_vars[3]) ** 3 - 0.00163014843927023 * (row.mass_hc * iter_vars[1] * iter_vars[3]) ** 2 + 0.0316936237605425 * (row.mass_hc * iter_vars[1] * iter_vars[3]) - 1.15107923E-12) if (row.mass_hc * iter_vars[1] * iter_vars[3]) <= 150 else (-0.0139845478076861 * (0 if (row.mass_hc * iter_vars[1] * iter_vars[3]) < 0 else (row.mass_hc * iter_vars[1] * iter_vars[3])) ** 1.30734582329863))) + (-0.0000916000485151024 * (row.mass_hc * iter_vars[1] - row.mass_hc * iter_vars[1] * iter_vars[3]) ** 2 - 0.0381575593603518 * (row.mass_hc * iter_vars[1] - row.mass_hc * iter_vars[1] * iter_vars[3])) if (row.mass_hc * iter_vars[1] - row.mass_hc * iter_vars[1] * iter_vars[3]) <= 220 else (-0.0118553726879464 * (0 if (row.mass_hc * iter_vars[1] - row.mass_hc * iter_vars[1] * iter_vars[3]) < 0 else (row.mass_hc * iter_vars[1] - row.mass_hc * iter_vars[1] * iter_vars[3])) ** 1.31929385202031)
    return mbynd20_dep_nap


def _lbmv_ar(row, iter_vars):
    lbmv_ar = (1.22897303940395 * (iter_vars[2] * row.mass_hc)) if (iter_vars[2] * row.mass_hc) <= 78.114 else ((-0.0000000000000003456 * (iter_vars[2] * row.mass_hc) ** 6 + 1.88579496E-12 * (iter_vars[2] * row.mass_hc) ** 5 - 4.09056994439E-09 * (iter_vars[2] * row.mass_hc) ** 4 + 4.48623820651704E-06 * (iter_vars[2] * row.mass_hc) ** 3 - 0.00262651390002428 * (iter_vars[2] * row.mass_hc) ** 2 + 1.50855371168788 * (iter_vars[2] * row.mass_hc) - 8.75690977467315) if (iter_vars[2] * row.mass_hc) <= 1350 else (1.61128548674228 * (0 if (iter_vars[2] * row.mass_hc) <= 0 else (iter_vars[2] * row.mass_hc)) ** 0.899697806202926))
    return lbmv_ar


def _lbmv_nap(row, iter_vars):
    lbmv_nap = ((0.0000137764595451451 * (row.mass_hc * iter_vars[1] * iter_vars[3]) ** 3 - 0.00455449752561954 * (row.mass_hc * iter_vars[1] * iter_vars[3]) ** 2 + 1.67035692759237 * (row.mass_hc * iter_vars[1] * iter_vars[3]) - 2.626165951E-11) if (row.mass_hc * iter_vars[1] * iter_vars[3]) <= 150 else ((- 8.43E-18 * (row.mass_hc * iter_vars[1] * iter_vars[3]) ** 3 + 7.6452E-16 * (row.mass_hc * iter_vars[1] * iter_vars[3]) ** 2 + 1.19056533134165 * (row.mass_hc * iter_vars[1] * iter_vars[3]) + 15.9997004846206) if (row.mass_hc * iter_vars[1] * iter_vars[3]) <= 950 else (1.34009804098614 * (0 if (row.mass_hc * iter_vars[1] * iter_vars[3]) <= 0 else (row.mass_hc * iter_vars[1] * iter_vars[3])) ** 0.984803528800257))) + ((-2.699290111937E-08 * (row.mass_hc * iter_vars[1] - row.mass_hc * iter_vars[1] * iter_vars[3]) ** 4 + 0.0000178477062199311 * (row.mass_hc * iter_vars[1] - row.mass_hc * iter_vars[1] * iter_vars[3]) ** 3 - 0.00422788943328101 * (row.mass_hc * iter_vars[1] - row.mass_hc * iter_vars[1] * iter_vars[3]) ** 2 + 1.64993385270191 * (row.mass_hc * iter_vars[1] - row.mass_hc * iter_vars[1] * iter_vars[3]) - 2.74712874671E-09) if (row.mass_hc * iter_vars[1] - row.mass_hc * iter_vars[1] * iter_vars[3]) <= 245 else ((-8.0182407936E-10 * (row.mass_hc * iter_vars[1] - row.mass_hc * iter_vars[1] * iter_vars[3]) ** 3 - 1.93901517697986E-06 * (row.mass_hc * iter_vars[1] - row.mass_hc * iter_vars[1] * iter_vars[3]) ** 2 + 1.23418932687637 * (row.mass_hc * iter_vars[1] - row.mass_hc * iter_vars[1] * iter_vars[3]) + 11.2968615678507) if (row.mass_hc * iter_vars[1] - row.mass_hc * iter_vars[1] * iter_vars[3]) <= 1350 else (1.22753826813575 * (row.mass_hc * iter_vars[1] - row.mass_hc * iter_vars[1] * iter_vars[3]) + 14.8879242773457)))
    return lbmv_nap


def _lbmv_ali(row, iter_vars):
    lbmv_ali = ((row.mass_hc * iter_vars[0]) * 1.58266200898268 + 2.10467669494546 * row.factor_f)
    return lbmv_ali


def _mnd20_ar(row, iter_vars):
    mnd20_ar = (1.50112 * (iter_vars[2] * row.mass_hc)) if (iter_vars[2] * row.mass_hc) <= 78.114 else ((-5.84206450751114E-06 * (iter_vars[2] * row.mass_hc) ** 3 + 0.00751213531409437 * (iter_vars[2] * row.mass_hc) ** 2 + 0.144073628503315 * (iter_vars[2] * row.mass_hc) + 68.4532599149825) if (iter_vars[2] * row.mass_hc) <= 500 else ((-8.41693144913E-09 * (iter_vars[2] * row.mass_hc) ** 3 + 0.000161108611351861 * (iter_vars[2] * row.mass_hc) ** 2 + 2.12248219255403 * (iter_vars[2] * row.mass_hc) - 63.9862717815407) if (iter_vars[2] * row.mass_hc) <= 1350 else (1.18286661434459 * (0 if (iter_vars[2] * row.mass_hc) <= 0 else (iter_vars[2] * row.mass_hc)) ** 1.09094887407156)))
    return mnd20_ar


def _mnd20_nap(row, iter_vars):
    mnd20_nap = (1.42058467041269 * (row.mass_hc * iter_vars[1])) if (row.mass_hc * iter_vars[1]) <= 70.135 else ((1.447284293541 * (row.mass_hc * iter_vars[1]) - 0.37610425029041) if (row.mass_hc * iter_vars[1]) <= 150 else ((-6.624138640713E-08 * (row.mass_hc * iter_vars[1]) ** 3 + 0.000275943845907455 * (row.mass_hc * iter_vars[1]) ** 2 + 1.60710605833917 * (row.mass_hc * iter_vars[1]) - 21.4347121998645) if (row.mass_hc * iter_vars[1]) <= 1350 else (1.02283899923738 * (0 if (row.mass_hc * iter_vars[1]) <= 0 else (row.mass_hc * iter_vars[1])) ** 1.0815776154784)))
    return mnd20_nap


def _mnd20_ali(row, iter_vars):
    mnd20_ali = ((row.mass_hc * iter_vars[0]) * 1.47073501105012 - 4.04250089113852 * row.factor_f)
    return mnd20_ali


def _rll_ar(row, iter_vars):
    rll_ar = (0.335239449951955 * (iter_vars[2] * row.mass_hc)) if (iter_vars[2] * row.mass_hc) <= 78.114 else ((-1.87116097323482E-06 * (iter_vars[2] * row.mass_hc) ** 3 + 0.0018792988594211 * (iter_vars[2] * row.mass_hc) ** 2 - 0.0939803177037452 * (iter_vars[2] * row.mass_hc) + 24.2899661854656) if (iter_vars[2] * row.mass_hc) <= 500 else ((2.452083914396E-08 * (iter_vars[2] * row.mass_hc) ** 3 - 0.000079209630494905 * (iter_vars[2] * row.mass_hc) ** 2 + 0.381092016734914 * (iter_vars[2] * row.mass_hc) - 3.73279981787174) if (iter_vars[2] * row.mass_hc) <= 1350 else (0.528496537381154 * (0 if (iter_vars[2] * row.mass_hc) <= 0 else (iter_vars[2] * row.mass_hc)) ** 0.928884019445315)))
    return rll_ar


def _rll_nap(row, iter_vars):
    rll_nap = (0.339821860964231 * (row.mass_hc * iter_vars[1])) if (row.mass_hc * iter_vars[1]) <= 70.135 else ((0.33771553289526 * (row.mass_hc * iter_vars[1]) + 0.0306062107298265) if (row.mass_hc * iter_vars[1]) <= 150 else ((-2.293E-17 * (row.mass_hc * iter_vars[1]) ** 6 + 1.0414983E-13 * (row.mass_hc * iter_vars[1]) ** 5 - 1.5843345177E-10 * (row.mass_hc * iter_vars[1]) ** 4 + 6.998936592203E-08 * (row.mass_hc * iter_vars[1]) ** 3 + 0.0000329446336797695 * (row.mass_hc * iter_vars[1]) ** 2 + 0.316604983947332 * (row.mass_hc * iter_vars[1]) + 1.74679158038743) if (row.mass_hc * iter_vars[1]) <= 1350 else (0.366228631794776 * (0 if (row.mass_hc * iter_vars[1]) <= 0 else (row.mass_hc * iter_vars[1])) ** 0.991034715216667)))
    return rll_nap


def _rll_ali(row, iter_vars):
    rll_ali = ((row.mass_hc * iter_vars[0]) * 0.329222214300991 + 0.768144007984601 * row.factor_f)
    return rll_ali


def _v25_ar(row, iter_vars):
    v_ar = (1.14219998676821 * (iter_vars[2] * row.mass_hc)) if (iter_vars[2] * row.mass_hc) <= 78.114 else ((-3.0818872804607E-07 * (iter_vars[2] * row.mass_hc) ** 3 + 0.000157473969023783 * (iter_vars[2] * row.mass_hc) ** 2 + 0.562071271755067 * (iter_vars[2] * row.mass_hc) + 45.1777143670648) if (iter_vars[2] * row.mass_hc) <= 500 else ((-1.336E-17 * (iter_vars[2] * row.mass_hc) ** 6 + 8.597456E-14 * (iter_vars[2] * row.mass_hc) ** 5 - 2.3321333134E-10 * (iter_vars[2] * row.mass_hc) ** 4 + 3.5394846103436E-07 * (iter_vars[2] * row.mass_hc) ** 3 - 0.000354659417799298 * (iter_vars[2] * row.mass_hc) ** 2 + 0.702633249568294 * (iter_vars[2] * row.mass_hc) + 30.8475817662467) if (iter_vars[2] * row.mass_hc) <= 1350 else (1.78553362642803 * (0 if (iter_vars[2] * row.mass_hc) <= 0 else (iter_vars[2] * row.mass_hc)) ** 0.835009655244618)))
    return v_ar


def _v25_nap(row, iter_vars):
    v_nap = (1.34866139998192 * (row.mass_hc * iter_vars[1])) if (row.mass_hc * iter_vars[1]) <= 70.135 else ((1.28366284131307 * (row.mass_hc * iter_vars[1]) + 0.651255737588963) if (row.mass_hc * iter_vars[1]) <= 150 else ((-3.280281826E-11 * (row.mass_hc * iter_vars[1]) ** 4 + 1.5036964504228E-07 * (row.mass_hc * iter_vars[1]) ** 3 - 0.000287245765992295 * (row.mass_hc * iter_vars[1]) ** 2 + 0.946749304356398 * (row.mass_hc * iter_vars[1]) + 31.5857375708661) if (row.mass_hc * iter_vars[1]) <= 1350 else (2.08318821509476 * (0 if (row.mass_hc * iter_vars[1]) <= 0 else (row.mass_hc * iter_vars[1])) ** 0.862908532042399)))
    return v_nap


def _v25_ali(row, iter_vars):
    v_ali = (-2.09149718E-12 * (row.mass_hc * iter_vars[0]) ** 6 + 2.56273813802E-09 * (row.mass_hc * iter_vars[0]) ** 5 - 1.23907173033011E-06 * (row.mass_hc * iter_vars[0]) ** 4 + 0.000296187570285156 * (row.mass_hc * iter_vars[0]) ** 3 - 0.0355571041254734 * (row.mass_hc * iter_vars[0]) ** 2 + 3.04701929242583 * (row.mass_hc * iter_vars[0]) + 0.00439327908679843) if (row.mass_hc * iter_vars[0]) <= 325 else ((7.41289E-15 * (row.mass_hc * iter_vars[0]) ** 5 - 4.954497963E-11 * (row.mass_hc * iter_vars[0]) ** 4 + 1.4086741743278E-07 * (row.mass_hc * iter_vars[0]) ** 3 - 0.00024576304582591 * (row.mass_hc * iter_vars[0]) ** 2 + 1.25190727201385 * (row.mass_hc * iter_vars[0]) + 20.7143141299502) if (row.mass_hc * iter_vars[0]) <= 1350 else (1.87454241096822 * (0 if (row.mass_hc * iter_vars[0]) <= 0 else (row.mass_hc * iter_vars[0])) ** 0.929332334824849))
    return v_ali


def _para_ar(row, iter_vars):
    para_ar = (2.62187448054632 * (iter_vars[2] * row.mass_hc)) if (iter_vars[2] * row.mass_hc) <= 78.15 else ((-1.822524384834E-08 * (iter_vars[2] * row.mass_hc) ** 4 + 0.0000199651937544676 * (iter_vars[2] * row.mass_hc) ** 3 - 0.00688962262335124 * (iter_vars[2] * row.mass_hc) ** 2 + 2.91787913445933 * (iter_vars[2] * row.mass_hc) + 1.99335676321425) if (iter_vars[2] * row.mass_hc) <= 400. else ((2.2187371E-13 * (iter_vars[2] * row.mass_hc) ** 5 - 1.09452475889E-09 * (iter_vars[2] * row.mass_hc) ** 4 + 2.07146705611516E-06 * (iter_vars[2] * row.mass_hc) ** 3 - 0.00192220203281091 * (iter_vars[2] * row.mass_hc) ** 2 + 2.49255064334514 * (iter_vars[2] * row.mass_hc) + 3.58255134747975) if (iter_vars[2] * row.mass_hc) <= 1350. else (3.50525295532602 * (0 if (iter_vars[2] * row.mass_hc) <= 0 else (iter_vars[2] * row.mass_hc)) ** 0.901185761435326)))
    return para_ar


def _para_nap(row, iter_vars):
    para_nap = (2.89506991671068 * (row.mass_hc * iter_vars[1])) if (row.mass_hc * iter_vars[1]) <= 70.135 else ((6.069839E-14 * (row.mass_hc * iter_vars[1]) ** 5 - 0.0000000003134148489 * (row.mass_hc * iter_vars[1]) ** 4 + 6.4959169160542E-07 * (row.mass_hc * iter_vars[1]) ** 3 - 0.000750319943029562 * (row.mass_hc * iter_vars[1]) ** 2 + 2.49938707512647 * (row.mass_hc * iter_vars[1]) + 35.4336466618015) if (row.mass_hc * iter_vars[1]) <= 1350 else (3.82825010509589 * (0 if (row.mass_hc * iter_vars[1]) <= 0 else (row.mass_hc * iter_vars[1])) ** 0.918472389443328))
    return para_nap


def _para_ali(row, iter_vars):
    para_ali = ((row.mass_hc * iter_vars[0]) * 2.79461039423968 + row.factor_f * 12.1830327226064)
    return para_ali


def aliphatic(df):
    df["ali_c"] = ((df.mass_hc * df.Ali_wf - 1.008 * df.factor_f) * 0.0712910814857061)
    df["ali_h"] = (df.mass_hc * df.Ali_wf * 0.142582162971412 + 0.856277179724817 * df.factor_f)
    df["para_ali"] = df.apply(lambda row: _para_ali(row, [row.Ali_wf, row.Nap_wf, row.Ar_wf, row.Nap5_wf]),
                              axis=1)
    df["v25_ali"] = df.apply(lambda row: _v25_ali(row, [row.Ali_wf, row.Nap_wf, row.Ar_wf, row.Nap5_wf]),
                             axis=1)
    df["rll_ali"] = df.apply(lambda row: _rll_ali(row, [row.Ali_wf, row.Nap_wf, row.Ar_wf, row.Nap5_wf]),
                             axis=1)
    df["mnd20_ali"] = df.apply(lambda row: _mnd20_ali(row, [row.Ali_wf, row.Nap_wf, row.Ar_wf, row.Nap5_wf]),
                               axis=1)
    df["lbmv_ali"] = df.apply(lambda row: _lbmv_ali(row, [row.Ali_wf, row.Nap_wf, row.Ar_wf, row.Nap5_wf]),
                              axis=1)


def napthenic(df):
    df["nap_c"] = df.apply(lambda row: (row.mass_hc * row.Nap_wf * 0.0712910814857061) if (row.mass_hc * row.Nap_wf) <= 85 else ((row.mass_hc * row.Nap_wf * 0.0712910814857061) if ((row.mass_hc * row.Nap_wf * 0.0712910814857061) >= (0.0681542447698304 * (0 if (row.mass_hc * row.Nap_wf) <= 0 else (row.mass_hc * row.Nap_wf)) ** 1.01250029139992)) else (0.0681542447698304 * (0 if (row.mass_hc * row.Nap_wf) <= 0 else (row.mass_hc * row.Nap_wf)) ** 1.01250029139992)), axis=1)
    df["nap_h"] = df.apply(lambda row: (row.mass_hc * row.Nap_wf * 0.992063492063492 - 11.9156746031746 * ((row.mass_hc * row.Nap_wf * 0.0712910814857061) if (row.mass_hc * row.Nap_wf) <= 85 else ((row.mass_hc * row.Nap_wf * 0.0712910814857061) if ((row.mass_hc * row.Nap_wf * 0.0712910814857061) >= (0.0681542447698304 * (0 if (row.mass_hc * row.Nap_wf) <= 0 else (row.mass_hc * row.Nap_wf)) ** 1.01250029139992)) else (0.0681542447698304 * (0 if (row.mass_hc * row.Nap_wf) <= 0 else (row.mass_hc * row.Nap_wf)) ** 1.01250029139992)))), axis=1)
    df["para_nap"] = df.apply(lambda row: _para_nap(row, [row.Ali_wf, row.Nap_wf, row.Ar_wf, row.Nap5_wf]),
                              axis=1)
    df["v25_nap"] = df.apply(lambda row: _v25_nap(row, [row.Ali_wf, row.Nap_wf, row.Ar_wf, row.Nap5_wf]),
                             axis=1)
    df["rll_nap"] = df.apply(lambda row: _rll_nap(row, [row.Ali_wf, row.Nap_wf, row.Ar_wf, row.Nap5_wf]),
                             axis=1)
    df["mnd20_nap"] = df.apply(lambda row: _mnd20_nap(row, [row.Ali_wf, row.Nap_wf, row.Ar_wf, row.Nap5_wf]),
                               axis=1)
    df["lbmv_nap"] = df.apply(lambda row: _lbmv_nap(row, [row.Ali_wf, row.Nap_wf, row.Ar_wf, row.Nap5_wf]),
                              axis=1)
    df["mbynd20_dep_nap"] = df.apply(
        lambda row: _mbynd20_dep_nap(row, [row.Ali_wf, row.Nap_wf, row.Ar_wf, row.Nap5_wf]), axis=1)
    df["nap5_ring"] = df.apply(lambda row: (row.mass_hc * row.Nap5_wf * row.Nap_wf * 0.0142857142857143) if (row.mass_hc * row.Nap5_wf * row.Nap_wf) <= 70.135 else (0.0249594409085236 * row.mass_hc * row.Nap5_wf * row.Nap_wf - 0.750530388119291), axis=1)
    df["nap6_ring"] = df.apply(lambda row: ((row.mass_hc * row.Nap_wf - row.mass_hc * row.Nap5_wf * row.Nap_wf) * 0.0118818469142844) if (row.mass_hc * row.Nap_wf - row.mass_hc * row.Nap5_wf * row.Nap_wf) <= 84.162 else (0.0183539141721682 * (row.mass_hc * row.Nap_wf - row.mass_hc * row.Nap5_wf * row.Nap_wf) - 0.417520813955541), axis=1)
    df["nap6_c"] = df.apply(lambda row: _nap6_c(row), axis=1)
    df["nap5_c"] = df.apply(lambda row: 0 if (row.nap_c - row.nap6_c) < 0 else (row.nap_c - row.nap6_c), axis=1)
    df["nap5_cc"] = df.apply(lambda row: _nap5_cc(row), axis=1)
    df["nap6_cc"] = df.apply(lambda row: _nap6_cc(row), axis=1)
    df["nap_none_cc"] = df.apply(
        lambda row: 0 if (row.nap_c - row.nap5_cc - row.nap6_cc) < 0 else (row.nap_c - row.nap5_cc - row.nap6_cc),
        axis=1)


def aromatic(df):
    df["arc_ring"] = df.apply(lambda row: (-0.0000001354040217456 * (row.Ar_wf * row.mass_hc) ** 3 + 0.0000520658252493034 * (row.Ar_wf * row.mass_hc) ** 2 + 0.0735699529423499 * (row.Ar_wf * row.mass_hc) + 1.68398628E-12) if (row.Ar_wf * row.mass_hc) <= 178 else (0.081366952742896 * (row.Ar_wf * row.mass_hc) - 0.495292073123728), axis=1)
    df["arh_ring"] = df.apply(lambda row: (row.mass_hc * row.Ar_wf * 0.992063492063492 - 11.9156746031746 * ((-0.0000001354040217456 * (row.Ar_wf * row.mass_hc) ** 3 + 0.0000520658252493034 * (row.Ar_wf * row.mass_hc) ** 2 + 0.0735699529423499 * (row.Ar_wf * row.mass_hc) + 1.68398628E-12) if (row.Ar_wf * row.mass_hc) <= 178 else (0.081366952742896 * (row.Ar_wf * row.mass_hc) - 0.495292073123728))), axis=1)
    df["para_ar"] = df.apply(lambda row: _para_ar(row, [row.Ali_wf, row.Nap_wf, row.Ar_wf, row.Nap5_wf]), axis=1)
    df["v25_ar"] = df.apply(lambda row: _v25_ar(row, [row.Ali_wf, row.Nap_wf, row.Ar_wf, row.Nap5_wf]), axis=1)
    df["rll_ar"] = df.apply(lambda row: _rll_ar(row, [row.Ali_wf, row.Nap_wf, row.Ar_wf, row.Nap5_wf]), axis=1)
    df["mnd20_ar"] = df.apply(lambda row: _mnd20_ar(row, [row.Ali_wf, row.Nap_wf, row.Ar_wf, row.Nap5_wf]), axis=1)
    df["no_ar_ring"] = df.apply(lambda row: (0.0128018024937911 * (row.Ar_wf * row.mass_hc)) if (row.Ar_wf * row.mass_hc) <= 78.114 else ((0.0202183047870432 * (row.Ar_wf * row.mass_hc) - 0.587335140981496) if (row.Ar_wf * row.mass_hc) <= 226.3 else (0.0338676948709907 * (row.Ar_wf * row.mass_hc) - 3.66041883727174)), axis=1)
    df["lbmv_ar"] = df.apply(lambda row: _lbmv_ar(row, [row.Ali_wf, row.Nap_wf, row.Ar_wf, row.Nap5_wf]), axis=1)
    df["mbynd20_dep_ar"] = df.apply(lambda row: _mbynd20_dep_ar(row, [row.Ali_wf, row.Nap_wf, row.Ar_wf, row.Nap5_wf]), axis=1)
    df["no_of_db"] = df.apply(lambda row: (0.0384054074813734 * (row.Ar_wf * row.mass_hc)) if (row.Ar_wf * row.mass_hc) <= 78.114 else (0.0408799637586271 * (row.Ar_wf * row.mass_hc) - 0.354541730913743), axis=1)
    df["ar_cc"] = df.apply(lambda row: 0 if (row.arc_ring * (-row.arh_ring / row.arc_ring + 1)) < 10 ** -4 else (row.arc_ring * (-row.arh_ring / row.arc_ring + 1.)), axis=1)
    df["ar_ach_alk"] = df.arc_ring - df.ar_cc
    df["ar_cc_ring"] = df.apply(lambda row: 0 if row.ar_cc == 0 else (row.ar_cc + 2.) / 2., axis=1)
    df["ar_ring1"] = df.apply(lambda row: (0.0128018024937911 * (row.Ar_wf * row.mass_hc)) if (row.Ar_wf * row.mass_hc) <= 78.114 else ((0.0202183047870432 * (row.Ar_wf * row.mass_hc) - 0.587335140981496) if (row.Ar_wf * row.mass_hc) <= 226.3 else (0.0338676948709907 * (row.Ar_wf * row.mass_hc) - 3.66041883727174)), axis=1)
    df["ar_ring"] = df.apply(lambda row: max(row.ar_cc_ring, row.ar_ring1), axis=1)
    df["ar_alk_ring"] = df.apply(
        lambda row: 0 if (row.ar_ring - row.ar_cc_ring) < 0 else (row.ar_ring - row.ar_cc_ring), axis=1)


def wc(df, sno):
    df["c_fr_wc"] = df.C / (df.C + df.H + df.S + df.O + df.N)
    df["mass_wc"] = sno.mass
    df["bp_wc"] = (47.6112866933562 * df.mass_wc ** 0.509665090106662 * df.c_fr_wc ** 0.26807533679463)
    df["par_wc"] = df.para + df.S * 50. + df.O * 20. + df.N * 17.5
    df["rll_wc"] = df.rll + df.S * 7.91 + df.O * 1.643 + df.N * 2.84
    df["mnd20_wc"] = df.mnd20 + df.S * 52.86 + df.O * 22.74 + df.N * 30.23
    df["ri_wc"] = df.mnd20_wc / df.mass_wc
    df["rgd_wc"] = df.rll_wc * (df.ri_wc ** 2 + 2.) / (df.ri_wc + 1.)
    df["lbmv_wc"] = df.lbmv + 25.6 * df.S + 7.4 * df.O + 11.25 * df.N
    df["d25_wc"] = df.mass_wc / (df.mass_hc / df.d25_hc + 14. * df.S + 3.75 * df.O + 1.5 * df.N)
    df["d20_wc"] = ((df.d25_wc * 1000. * np.exp((-1. * (613.97226 / (df.d25_wc * 1000.) ** 2) * (20. - 25.) * (
                1. + 0.8 * (613.97226 / (df.d25_wc * 1000.) ** 2) * (20. - 25.))))) / 1000.)
    df["v25_wc"] = df.mass_wc / df.d25_wc
    df["v20_wc"] = df.mass_wc / df.d20_wc


def hc_params(df):
    from pcm import _cost
    for index, row in df.iterrows():
        iter_vars = [row.Ali_wf, row.Nap_wf, row.Ar_wf, row.Nap5_wf]
        cost = _cost(row, iter_vars)
        for col in ["d20", "c", "h", "hbyc", "para", "v25", "rll", "mnd20", "lbmv", "mbynd20_dep", "wf", "d20_por",
                    "d20_nap", "d20_ali"]:
            df.loc[df.seq == row.seq, col] = cost[col]
    df.drop(columns=["d20_hc", "C", "H"], inplace=True)
    df.rename(columns={"d20": "d20_hc", "d20_por": "d20_par", "c": "C", "h": "H"}, inplace=True)
    df["d25_hc"] = ((df.d20_hc * 1000. * np.exp((-1. * (613.97226 / (df.d20_hc * 1000.) ** 2) * (25. - 20.) * (
                1. + 0.8 * (613.97226 / (df.d20_hc * 1000.) ** 2) * (25. - 20.))))) / 1000.)


def calculated(ov, sno):
    df = ov.copy(deep=True)
    fix_iter_vars(df, ov)
    mass(df)
    hc_params(df)
    df[["S", "O", "N", "mf"]] = sno[["S", "N", "O", "mf"]]
    wc(df, sno)
    aromatic(df)
    napthenic(df)
    aliphatic(df)
    return df
