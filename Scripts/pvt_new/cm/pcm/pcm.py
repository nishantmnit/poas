import numpy as np
import pandas as pd

from Scripts.pvt_new.core.get_logger import get_logger
from Scripts.pvt_new.core.storage.save import save
from calculated import calculated
from gradients import gradients
from observed import observed
from var_names import var_names
from Scripts.pvt_new.core.storage.mongo import fetch

logger = get_logger("pvt cm pcm")


def regress(row, iter_vars, grad, alpha):
    niter_vars = np.array(iter_vars) - alpha * np.array(grad)
    mvar0 = 0.7 if row.name <= 5.5 else 0.5 * (359.41 * row.name ** -1.133) / 100.
    mvar1 = 0. if row.name <= 5.5 else 0.005
    mvar2 = 0. if row.name <= 5.5 else (0.01 if row.name <= 118.5 else 0.005)
    mvar3 = 0. if row.name <= 6.5 else (0.005 if row.name <= 15.5 else 0.01)
    niter_vars[0] = 1. if niter_vars[0] > 1. else (mvar0 if niter_vars[0] < mvar0 else niter_vars[0])
    niter_vars[1] = 0.95 if niter_vars[1] > 0.95 else (mvar1 if niter_vars[1] < mvar1 else niter_vars[1])
    niter_vars[2] = 1. if niter_vars[2] > 1. else (mvar2 if niter_vars[2] < mvar2 else niter_vars[2])
    niter_vars[3] = 1. if niter_vars[3] > 1. else (mvar3 if niter_vars[3] < mvar3 else niter_vars[3])
    return niter_vars


def _sp(row):
    Ali_wf = ((86.0114249492252 * np.exp(-1. * 0.0177673248982099 * row.name)) if row.name > 8. else (
        100. if (305.499331374587 * row.name ** -0.670255496913115) > 100. else (
                    305.499331374587 * row.name ** -0.670255496913115))) / 100.
    Nap_wf = ((2.82897935494434 * row.name ** 0.448514890024682) if row.name > 15. else (
        0. if (-0.617455953455017 * row.name ** 2 + 13.3922281099951 * row.name - 51.5940531155635) * 1.1 < 0 else (
                                                                                                                               -0.617455953455017 * row.name ** 2 + 13.3922281099951 * row.name - 51.5940531155635) * 1.1)) / 100.
    Ar_wf = 1. - (Ali_wf + Nap_wf)
    Nap5_wf = (
                          8.13565318377E-09 * row.name ** 6 - 3.14560346557703E-06 * row.name ** 5 + 0.000471967158649088 * row.name ** 4 - 0.0346670621679459 * row.name ** 3 + 1.2919295926371 * row.name ** 2 - 22.7746200504744 * row.name + 186.66230336393 - 1.1) / 100
    sp = [Ali_wf, Nap_wf, Ar_wf, Nap5_wf]
    return sp


def _mbynd20_dep(row, iter_vars):
    mbynd20_dep_ar = (-0.138691077327068 * (iter_vars[2] * row.mass_hc)) if (iter_vars[
                                                                                 2] * row.mass_hc) <= 78.114 else ((
                                                                                                                               -2.5767E-16 * (
                                                                                                                                   iter_vars[
                                                                                                                                       2] * row.mass_hc) ** 6 + 1.25570849E-12 * (
                                                                                                                                           iter_vars[
                                                                                                                                               2] * row.mass_hc) ** 5 - 2.33339465412E-09 * (
                                                                                                                                           iter_vars[
                                                                                                                                               2] * row.mass_hc) ** 4 + 2.05922029150513E-06 * (
                                                                                                                                           iter_vars[
                                                                                                                                               2] * row.mass_hc) ** 3 - 0.000903990644090139 * (
                                                                                                                                           iter_vars[
                                                                                                                                               2] * row.mass_hc) ** 2 - 0.15864057854991 * (
                                                                                                                                           iter_vars[
                                                                                                                                               2] * row.mass_hc) + 10.4586452565478) if (
                                                                                                                                                                                                    iter_vars[
                                                                                                                                                                                                        2] * row.mass_hc) <= 1350 else (
                -0.0000112642961817011 * (iter_vars[2] * row.mass_hc) ** 2 - 0.362846148407561 * (
                    iter_vars[2] * row.mass_hc) + 45.8178034639216))
    mbynd20_dep_nap = ((-0.0499691144951728 * (row.mass_hc * iter_vars[1] * iter_vars[3])) if (row.mass_hc * iter_vars[
        1] * iter_vars[3]) <= 70.135 else ((6.64124123196186E-06 * (
                row.mass_hc * iter_vars[1] * iter_vars[3]) ** 3 - 0.00163014843927023 * (
                                                        row.mass_hc * iter_vars[1] * iter_vars[
                                                    3]) ** 2 + 0.0316936237605425 * (
                                                        row.mass_hc * iter_vars[1] * iter_vars[
                                                    3]) - 1.15107923E-12) if (row.mass_hc * iter_vars[1] * iter_vars[
        3]) <= 150 else (-0.0139845478076861 * (0 if (row.mass_hc * iter_vars[1] * iter_vars[3]) < 0 else (
                row.mass_hc * iter_vars[1] * iter_vars[3])) ** 1.30734582329863))) + (-0.0000916000485151024 * (
                row.mass_hc * iter_vars[1] - row.mass_hc * iter_vars[1] * iter_vars[3]) ** 2 - 0.0381575593603518 * (
                                                                                                  row.mass_hc *
                                                                                                  iter_vars[
                                                                                                      1] - row.mass_hc *
                                                                                                  iter_vars[1] *
                                                                                                  iter_vars[3])) if (
                                                                                                                                row.mass_hc *
                                                                                                                                iter_vars[
                                                                                                                                    1] - row.mass_hc *
                                                                                                                                iter_vars[
                                                                                                                                    1] *
                                                                                                                                iter_vars[
                                                                                                                                    3]) <= 220 else (
                -0.0118553726879464 * (
            0 if (row.mass_hc * iter_vars[1] - row.mass_hc * iter_vars[1] * iter_vars[3]) < 0 else (
                        row.mass_hc * iter_vars[1] - row.mass_hc * iter_vars[1] * iter_vars[3])) ** 1.31929385202031)
    return mbynd20_dep_ar + mbynd20_dep_nap


def _lbmv(row, iter_vars):
    lbmv_ar = (1.22897303940395 * (iter_vars[2] * row.mass_hc)) if (iter_vars[2] * row.mass_hc) <= 78.114 else ((
                                                                                                                            -0.0000000000000003456 * (
                                                                                                                                iter_vars[
                                                                                                                                    2] * row.mass_hc) ** 6 + 1.88579496E-12 * (
                                                                                                                                        iter_vars[
                                                                                                                                            2] * row.mass_hc) ** 5 - 4.09056994439E-09 * (
                                                                                                                                        iter_vars[
                                                                                                                                            2] * row.mass_hc) ** 4 + 4.48623820651704E-06 * (
                                                                                                                                        iter_vars[
                                                                                                                                            2] * row.mass_hc) ** 3 - 0.00262651390002428 * (
                                                                                                                                        iter_vars[
                                                                                                                                            2] * row.mass_hc) ** 2 + 1.50855371168788 * (
                                                                                                                                        iter_vars[
                                                                                                                                            2] * row.mass_hc) - 8.75690977467315) if (
                                                                                                                                                                                                 iter_vars[
                                                                                                                                                                                                     2] * row.mass_hc) <= 1350 else (
                1.61128548674228 * (
            0 if (iter_vars[2] * row.mass_hc) <= 0 else (iter_vars[2] * row.mass_hc)) ** 0.899697806202926))
    lbmv_nap = ((0.0000137764595451451 * (row.mass_hc * iter_vars[1] * iter_vars[3]) ** 3 - 0.00455449752561954 * (
                row.mass_hc * iter_vars[1] * iter_vars[3]) ** 2 + 1.67035692759237 * (
                             row.mass_hc * iter_vars[1] * iter_vars[3]) - 2.626165951E-11) if (row.mass_hc * iter_vars[
        1] * iter_vars[3]) <= 150 else ((- 8.43E-18 * (row.mass_hc * iter_vars[1] * iter_vars[3]) ** 3 + 7.6452E-16 * (
                row.mass_hc * iter_vars[1] * iter_vars[3]) ** 2 + 1.19056533134165 * (
                                                     row.mass_hc * iter_vars[1] * iter_vars[3]) + 15.9997004846206) if (
                                                                                                                                   row.mass_hc *
                                                                                                                                   iter_vars[
                                                                                                                                       1] *
                                                                                                                                   iter_vars[
                                                                                                                                       3]) <= 950 else (
                1.34009804098614 * (0 if (row.mass_hc * iter_vars[1] * iter_vars[3]) <= 0 else (
                    row.mass_hc * iter_vars[1] * iter_vars[3])) ** 0.984803528800257))) + ((-2.699290111937E-08 * (
                row.mass_hc * iter_vars[1] - row.mass_hc * iter_vars[1] * iter_vars[3]) ** 4 + 0.0000178477062199311 * (
                                                                                                        row.mass_hc *
                                                                                                        iter_vars[
                                                                                                            1] - row.mass_hc *
                                                                                                        iter_vars[1] *
                                                                                                        iter_vars[
                                                                                                            3]) ** 3 - 0.00422788943328101 * (
                                                                                                        row.mass_hc *
                                                                                                        iter_vars[
                                                                                                            1] - row.mass_hc *
                                                                                                        iter_vars[1] *
                                                                                                        iter_vars[
                                                                                                            3]) ** 2 + 1.64993385270191 * (
                                                                                                        row.mass_hc *
                                                                                                        iter_vars[
                                                                                                            1] - row.mass_hc *
                                                                                                        iter_vars[1] *
                                                                                                        iter_vars[
                                                                                                            3]) - 2.74712874671E-09) if (
                                                                                                                                                    row.mass_hc *
                                                                                                                                                    iter_vars[
                                                                                                                                                        1] - row.mass_hc *
                                                                                                                                                    iter_vars[
                                                                                                                                                        1] *
                                                                                                                                                    iter_vars[
                                                                                                                                                        3]) <= 245 else (
        (-8.0182407936E-10 * (row.mass_hc * iter_vars[1] - row.mass_hc * iter_vars[1] * iter_vars[
            3]) ** 3 - 1.93901517697986E-06 * (
                     row.mass_hc * iter_vars[1] - row.mass_hc * iter_vars[1] * iter_vars[3]) ** 2 + 1.23418932687637 * (
                     row.mass_hc * iter_vars[1] - row.mass_hc * iter_vars[1] * iter_vars[3]) + 11.2968615678507) if (
                                                                                                                                row.mass_hc *
                                                                                                                                iter_vars[
                                                                                                                                    1] - row.mass_hc *
                                                                                                                                iter_vars[
                                                                                                                                    1] *
                                                                                                                                iter_vars[
                                                                                                                                    3]) <= 1350 else (
                    1.22753826813575 * (
                        row.mass_hc * iter_vars[1] - row.mass_hc * iter_vars[1] * iter_vars[3]) + 14.8879242773457)))
    lbmv_ali = ((row.mass_hc * iter_vars[0]) * 1.58266200898268 + 2.10467669494546 * row.factor_f)
    return lbmv_ar + lbmv_nap + lbmv_ali


def _mnd20(row, iter_vars):
    mnd20_ar = (1.50112 * (iter_vars[2] * row.mass_hc)) if (iter_vars[2] * row.mass_hc) <= 78.114 else ((
                                                                                                                    -5.84206450751114E-06 * (
                                                                                                                        iter_vars[
                                                                                                                            2] * row.mass_hc) ** 3 + 0.00751213531409437 * (
                                                                                                                                iter_vars[
                                                                                                                                    2] * row.mass_hc) ** 2 + 0.144073628503315 * (
                                                                                                                                iter_vars[
                                                                                                                                    2] * row.mass_hc) + 68.4532599149825) if (
                                                                                                                                                                                         iter_vars[
                                                                                                                                                                                             2] * row.mass_hc) <= 500 else (
        (-8.41693144913E-09 * (iter_vars[2] * row.mass_hc) ** 3 + 0.000161108611351861 * (
                    iter_vars[2] * row.mass_hc) ** 2 + 2.12248219255403 * (
                     iter_vars[2] * row.mass_hc) - 63.9862717815407) if (iter_vars[2] * row.mass_hc) <= 1350 else (
                    1.18286661434459 * (
                0 if (iter_vars[2] * row.mass_hc) <= 0 else (iter_vars[2] * row.mass_hc)) ** 1.09094887407156)))
    mnd20_nap = (1.42058467041269 * (row.mass_hc * iter_vars[1])) if (row.mass_hc * iter_vars[1]) <= 70.135 else (
        (1.447284293541 * (row.mass_hc * iter_vars[1]) - 0.37610425029041) if (row.mass_hc * iter_vars[1]) <= 150 else (
            (-6.624138640713E-08 * (row.mass_hc * iter_vars[1]) ** 3 + 0.000275943845907455 * (
                        row.mass_hc * iter_vars[1]) ** 2 + 1.60710605833917 * (
                         row.mass_hc * iter_vars[1]) - 21.4347121998645) if (row.mass_hc * iter_vars[1]) <= 1350 else (
                        1.02283899923738 * (
                    0 if (row.mass_hc * iter_vars[1]) <= 0 else (row.mass_hc * iter_vars[1])) ** 1.0815776154784)))
    mnd20_ali = ((row.mass_hc * iter_vars[0]) * 1.47073501105012 - 4.04250089113852 * row.factor_f)
    return mnd20_ar + mnd20_nap + mnd20_ali


def _rll(row, iter_vars):
    rll_ar = (0.335239449951955 * (iter_vars[2] * row.mass_hc)) if (iter_vars[2] * row.mass_hc) <= 78.114 else ((
                                                                                                                            -1.87116097323482E-06 * (
                                                                                                                                iter_vars[
                                                                                                                                    2] * row.mass_hc) ** 3 + 0.0018792988594211 * (
                                                                                                                                        iter_vars[
                                                                                                                                            2] * row.mass_hc) ** 2 - 0.0939803177037452 * (
                                                                                                                                        iter_vars[
                                                                                                                                            2] * row.mass_hc) + 24.2899661854656) if (
                                                                                                                                                                                                 iter_vars[
                                                                                                                                                                                                     2] * row.mass_hc) <= 500 else (
        (2.452083914396E-08 * (iter_vars[2] * row.mass_hc) ** 3 - 0.000079209630494905 * (
                    iter_vars[2] * row.mass_hc) ** 2 + 0.381092016734914 * (
                     iter_vars[2] * row.mass_hc) - 3.73279981787174) if (iter_vars[2] * row.mass_hc) <= 1350 else (
                    0.528496537381154 * (
                0 if (iter_vars[2] * row.mass_hc) <= 0 else (iter_vars[2] * row.mass_hc)) ** 0.928884019445315)))
    rll_nap = (0.339821860964231 * (row.mass_hc * iter_vars[1])) if (row.mass_hc * iter_vars[1]) <= 70.135 else (
        (0.33771553289526 * (row.mass_hc * iter_vars[1]) + 0.0306062107298265) if (row.mass_hc * iter_vars[
            1]) <= 150 else ((-2.293E-17 * (row.mass_hc * iter_vars[1]) ** 6 + 1.0414983E-13 * (
                    row.mass_hc * iter_vars[1]) ** 5 - 1.5843345177E-10 * (
                                          row.mass_hc * iter_vars[1]) ** 4 + 6.998936592203E-08 * (
                                          row.mass_hc * iter_vars[1]) ** 3 + 0.0000329446336797695 * (
                                          row.mass_hc * iter_vars[1]) ** 2 + 0.316604983947332 * (
                                          row.mass_hc * iter_vars[1]) + 1.74679158038743) if (row.mass_hc * iter_vars[
            1]) <= 1350 else (0.366228631794776 * (
            0 if (row.mass_hc * iter_vars[1]) <= 0 else (row.mass_hc * iter_vars[1])) ** 0.991034715216667)))
    rll_ali = ((row.mass_hc * iter_vars[0]) * 0.329222214300991 + 0.768144007984601 * row.factor_f)
    return rll_ar + rll_nap + rll_ali


def _v25(row, iter_vars):
    v_ar = (1.14219998676821 * (iter_vars[2] * row.mass_hc)) if (iter_vars[2] * row.mass_hc) <= 78.114 else ((
                                                                                                                         -3.0818872804607E-07 * (
                                                                                                                             iter_vars[
                                                                                                                                 2] * row.mass_hc) ** 3 + 0.000157473969023783 * (
                                                                                                                                     iter_vars[
                                                                                                                                         2] * row.mass_hc) ** 2 + 0.562071271755067 * (
                                                                                                                                     iter_vars[
                                                                                                                                         2] * row.mass_hc) + 45.1777143670648) if (
                                                                                                                                                                                              iter_vars[
                                                                                                                                                                                                  2] * row.mass_hc) <= 500 else (
        (-1.336E-17 * (iter_vars[2] * row.mass_hc) ** 6 + 8.597456E-14 * (
                    iter_vars[2] * row.mass_hc) ** 5 - 2.3321333134E-10 * (
                     iter_vars[2] * row.mass_hc) ** 4 + 3.5394846103436E-07 * (
                     iter_vars[2] * row.mass_hc) ** 3 - 0.000354659417799298 * (
                     iter_vars[2] * row.mass_hc) ** 2 + 0.702633249568294 * (
                     iter_vars[2] * row.mass_hc) + 30.8475817662467) if (iter_vars[2] * row.mass_hc) <= 1350 else (
                    1.78553362642803 * (
                0 if (iter_vars[2] * row.mass_hc) <= 0 else (iter_vars[2] * row.mass_hc)) ** 0.835009655244618)))
    v_nap = (1.34866139998192 * (row.mass_hc * iter_vars[1])) if (row.mass_hc * iter_vars[1]) <= 70.135 else (
        (1.28366284131307 * (row.mass_hc * iter_vars[1]) + 0.651255737588963) if (row.mass_hc * iter_vars[
            1]) <= 150 else ((-3.280281826E-11 * (row.mass_hc * iter_vars[1]) ** 4 + 1.5036964504228E-07 * (
                    row.mass_hc * iter_vars[1]) ** 3 - 0.000287245765992295 * (
                                          row.mass_hc * iter_vars[1]) ** 2 + 0.946749304356398 * (
                                          row.mass_hc * iter_vars[1]) + 31.5857375708661) if (row.mass_hc * iter_vars[
            1]) <= 1350 else (2.08318821509476 * (
            0 if (row.mass_hc * iter_vars[1]) <= 0 else (row.mass_hc * iter_vars[1])) ** 0.862908532042399)))
    v_ali = (-2.09149718E-12 * (row.mass_hc * iter_vars[0]) ** 6 + 2.56273813802E-09 * (
                row.mass_hc * iter_vars[0]) ** 5 - 1.23907173033011E-06 * (
                         row.mass_hc * iter_vars[0]) ** 4 + 0.000296187570285156 * (
                         row.mass_hc * iter_vars[0]) ** 3 - 0.0355571041254734 * (
                         row.mass_hc * iter_vars[0]) ** 2 + 3.04701929242583 * (
                         row.mass_hc * iter_vars[0]) + 0.00439327908679843) if (row.mass_hc * iter_vars[
        0]) <= 325 else ((7.41289E-15 * (row.mass_hc * iter_vars[0]) ** 5 - 4.954497963E-11 * (
                row.mass_hc * iter_vars[0]) ** 4 + 1.4086741743278E-07 * (
                                      row.mass_hc * iter_vars[0]) ** 3 - 0.00024576304582591 * (
                                      row.mass_hc * iter_vars[0]) ** 2 + 1.25190727201385 * (
                                      row.mass_hc * iter_vars[0]) + 20.7143141299502) if (row.mass_hc * iter_vars[
        0]) <= 1350 else (1.87454241096822 * (
        0 if (row.mass_hc * iter_vars[0]) <= 0 else (row.mass_hc * iter_vars[0])) ** 0.929332334824849))
    return v_ar + v_nap + v_ali


def _para(row, iter_vars):
    para_ar = (2.62187448054632 * (iter_vars[2] * row.mass_hc)) if (iter_vars[2] * row.mass_hc) <= 78.15 else ((
                                                                                                                           -1.822524384834E-08 * (
                                                                                                                               iter_vars[
                                                                                                                                   2] * row.mass_hc) ** 4 + 0.0000199651937544676 * (
                                                                                                                                       iter_vars[
                                                                                                                                           2] * row.mass_hc) ** 3 - 0.00688962262335124 * (
                                                                                                                                       iter_vars[
                                                                                                                                           2] * row.mass_hc) ** 2 + 2.91787913445933 * (
                                                                                                                                       iter_vars[
                                                                                                                                           2] * row.mass_hc) + 1.99335676321425) if (
                                                                                                                                                                                                iter_vars[
                                                                                                                                                                                                    2] * row.mass_hc) <= 400. else (
        (2.2187371E-13 * (iter_vars[2] * row.mass_hc) ** 5 - 1.09452475889E-09 * (
                    iter_vars[2] * row.mass_hc) ** 4 + 2.07146705611516E-06 * (
                     iter_vars[2] * row.mass_hc) ** 3 - 0.00192220203281091 * (
                     iter_vars[2] * row.mass_hc) ** 2 + 2.49255064334514 * (
                     iter_vars[2] * row.mass_hc) + 3.58255134747975) if (iter_vars[2] * row.mass_hc) <= 1350. else (
                    3.50525295532602 * (
                0 if (iter_vars[2] * row.mass_hc) <= 0 else (iter_vars[2] * row.mass_hc)) ** 0.901185761435326)))
    para_nap = (2.89506991671068 * (row.mass_hc * iter_vars[1])) if (row.mass_hc * iter_vars[1]) <= 70.135 else ((
                                                                                                                             6.069839E-14 * (
                                                                                                                                 row.mass_hc *
                                                                                                                                 iter_vars[
                                                                                                                                     1]) ** 5 - 0.0000000003134148489 * (
                                                                                                                                         row.mass_hc *
                                                                                                                                         iter_vars[
                                                                                                                                             1]) ** 4 + 6.4959169160542E-07 * (
                                                                                                                                         row.mass_hc *
                                                                                                                                         iter_vars[
                                                                                                                                             1]) ** 3 - 0.000750319943029562 * (
                                                                                                                                         row.mass_hc *
                                                                                                                                         iter_vars[
                                                                                                                                             1]) ** 2 + 2.49938707512647 * (
                                                                                                                                         row.mass_hc *
                                                                                                                                         iter_vars[
                                                                                                                                             1]) + 35.4336466618015) if (
                                                                                                                                                                                    row.mass_hc *
                                                                                                                                                                                    iter_vars[
                                                                                                                                                                                        1]) <= 1350 else (
                3.82825010509589 * (
            0 if (row.mass_hc * iter_vars[1]) <= 0 else (row.mass_hc * iter_vars[1])) ** 0.918472389443328))
    para_ali = ((row.mass_hc * iter_vars[0]) * 2.79461039423968 + row.factor_f * 12.1830327226064)
    return para_ar + para_nap + para_ali


def _h(row, iter_vars):
    h = (row.mass_hc * iter_vars[2] * 0.992063492063492 - 11.9156746031746 * ((-0.0000001354040217456 * (
                iter_vars[2] * row.mass_hc) ** 3 + 0.0000520658252493034 * (iter_vars[
                                                                                2] * row.mass_hc) ** 2 + 0.0735699529423499 * (
                                                                                           iter_vars[
                                                                                               2] * row.mass_hc) + 1.68398628E-12) if (
                                                                                                                                                  iter_vars[
                                                                                                                                                      2] * row.mass_hc) <= 178 else (
                0.081366952742896 * (iter_vars[2] * row.mass_hc) - 0.495292073123728)))
    h1 = (row.mass_hc * iter_vars[1] * 0.992063492063492 - 11.9156746031746 * (
        (row.mass_hc * iter_vars[1] * 0.0712910814857061) if (row.mass_hc * iter_vars[1]) <= 85 else (
            (row.mass_hc * iter_vars[1] * 0.0712910814857061) if ((row.mass_hc * iter_vars[1] * 0.0712910814857061) >= (
                        0.0681542447698304 * (0 if (row.mass_hc * iter_vars[1]) <= 0 else (
                            row.mass_hc * iter_vars[1])) ** 1.01250029139992)) else (0.0681542447698304 * (
                0 if (row.mass_hc * iter_vars[1]) <= 0 else (row.mass_hc * iter_vars[1])) ** 1.01250029139992))))
    h2 = (row.mass_hc * iter_vars[0] * 0.142582162971412 + 0.856277179724817 * row.factor_f)
    return h + h1 + h2


def _d20(iter_vars, c):
    if c <= 6:
        d20_por = 0.867
    elif c <= 15:
        d20_por = (-0.0003 * c ** 3 + 0.0149 * c ** 2 - 0.1661 * c + 1.3977 + 0.004179)
    else:
        d20_por = (0.300637954533737 * np.log(10 ** -3 if c < 10 ** -3 else np.abs(c)) + 0.426327227650635)
    if c <= 15:
        d20_nap = (
                -0.00012079625145045 * c ** 4 + 0.00519493513767344 * c ** 3 - 0.0808917600746053 * c ** 2 + 0.554624548015966 * c - 0.579097973264385)
    else:
        d20_nap = (0.173440291796096 * np.log(10 ** -3 if c < 10 ** -3 else np.abs(c)) + 0.487535073065162)
    if c <= 30:
        d20_ali = (
                2.8868630945E-10 * c ** 6 + 2.0729620662553E-07 * c ** 5 - 0.0000225661467711281 * c ** 4 + 0.000901638009900008 * c ** 3 - 0.0173333070960121 * c ** 2 + 0.166058512570425 * c + 0.115680916261106)
    else:
        d20_ali = (0.0604102877761363 * np.log(10 ** -3 if c < 10 ** -3 else np.abs(c)) + 0.606186312956607)
    d20 = 0 if (iter_vars[0] / d20_ali + iter_vars[1] / d20_nap + iter_vars[
        2] / d20_por) == 0 or d20_ali == 0 or d20_nap == 0 or d20_por == 0 else (
            1. / (iter_vars[0] / d20_ali + iter_vars[1] / d20_nap + iter_vars[2] / d20_por))
    return d20_por, d20_nap, d20_ali, d20


def _c(row, iter_vars):
    if iter_vars[2] * row.mass_hc <= 178:
        c = (-0.0000001354040217456 * (iter_vars[2] * row.mass_hc) ** 3 + 0.0000520658252493034 * (
                iter_vars[2] * row.mass_hc) ** 2 + 0.0735699529423499 * (iter_vars[2] * row.mass_hc) + 1.68398628E-12)
    else:
        c = (0.081366952742896 * (iter_vars[2] * row.mass_hc) - 0.495292073123728)
    if row.mass_hc * iter_vars[1] <= 85:
        c += (row.mass_hc * iter_vars[1] * 0.0712910814857061)
    elif ((row.mass_hc * iter_vars[1] * 0.0712910814857061) >= (0.0681542447698304 * (
            0 if (row.mass_hc * iter_vars[1]) <= 0 else (row.mass_hc * iter_vars[1])) ** 1.01250029139992)):
        c += (row.mass_hc * iter_vars[1] * 0.0712910814857061)
    else:
        c += (0.0681542447698304 * (
            0 if (row.mass_hc * iter_vars[1]) <= 0 else (row.mass_hc * iter_vars[1])) ** 1.01250029139992)
    c += ((row.mass_hc * iter_vars[0] - 1.008 * row.factor_f) * 0.0712910814857061)
    return c


def _cost(row, iter_vars):
    c = _c(row, iter_vars)
    d20_po, d20_nap, d20_ali, d20 = _d20(iter_vars, c)
    h = _h(row, iter_vars)
    hbyc = 0 if h == 0 or c == 0 else h / c
    para = _para(row, iter_vars)
    v25 = _v25(row, iter_vars)
    rll = _rll(row, iter_vars)
    mnd20 = _mnd20(row, iter_vars)
    lbmv = _lbmv(row, iter_vars)
    mbynd20_dep = _mbynd20_dep(row, iter_vars)
    wf = iter_vars[0] + iter_vars[1] + iter_vars[2]
    cost = pd.Series(data=[d20, c, h, hbyc, para, v25, rll, mnd20, lbmv, mbynd20_dep, wf, d20_po, d20_nap, d20_ali],
                     index=["d20", "c", "h", "hbyc", "para", "v25", "rll", "mnd20", "lbmv", "mbynd20_dep", "wf",
                            "d20_por", "d20_nap", "d20_ali"])
    return cost


def _objective(row, iter_vars):
    cost = _cost(row, iter_vars)
    d20 = 300. * ((row.d20_hc - cost.d20) / row.d20_hc) ** 2
    hbyc = 10. * ((row.hbyc - cost.hbyc) / row.hbyc) ** 2
    par = 50. * ((row.para_hc - cost.para) / row.para_hc) ** 2
    v25 = 10. * ((row.v25_hc - cost.v25) / row.v25_hc) ** 2
    rll = 100. * ((row.rll_hc - cost.rll) / row.rll_hc) ** 2
    mnd20 = 100. * ((row.mnd20_hc - cost.mnd20) / row.mnd20_hc) ** 2
    c = 100. * ((row.C - cost.c) / row.C) ** 2
    lbmv = 500. * ((row.lbmv - cost.lbmv) / row.lbmv) ** 2
    mbynd20_dep = 500. * (
        0. if row.mbynd20_dep >= 0 else ((row.mbynd20_dep - cost.mbynd20_dep) / row.mbynd20_dep) ** 2)
    wf = 500. * (1. - cost.wf) ** 2
    return d20 + hbyc + par + v25 + rll + mnd20 + c + lbmv + mbynd20_dep + wf


def _alpha(row, iter_vars, grad):
    alphas, calphas, niter_vars = list(), list(), list()
    for i in range(1, 46):
        alpha = (0.00230808038753905 * np.exp(-0.189804192865194 * i)) * 0.9
        alphas.append(alpha)
        new_iter_vars = np.array(iter_vars) - alpha * np.array(grad)
        new_iter_vars = [0 if x < 0 else x for x in new_iter_vars]
        niter_vars.append(new_iter_vars)
        calphas.append(_objective(row, new_iter_vars))
    alpha = alphas[calphas.index(min(calphas))]
    m_cost = min(calphas)
    return alpha, m_cost


def _converged(row, iter_vars, m_cost):
    cd = np.abs(_objective(row, iter_vars) - m_cost)
    if cd < 10 ** -3:
        return True
    return False


def _solver(row):
    iter_vars = _sp(row)
    logger.info("PCM tuning for scn = %s | component = %s | start point = %s" % (
        row.name, row.component, ",".join([str(x) for x in iter_vars])))
    _max = 1
    while _max <= 200:
        cost = _cost(row, iter_vars)
        grad = gradients(row, iter_vars, cost)
        alpha, m_cost = _alpha(row, iter_vars, grad)
        niter_vars = regress(row, iter_vars, grad, alpha)
        if _converged(row, iter_vars, m_cost):
            break
        iter_vars = niter_vars
        _max += 1
    logger.info("PCM tuning for scn = %s | component = %s | end point = %s | iterations = %s" % (
        row.name, row.component, ",".join([str(x) for x in iter_vars]), _max - 1))
    return iter_vars


def pcm(user_args):
    sno = fetch(user_args.well_conn, property="pvt_output", identifier="sno")
    observed_df = observed(sno)
    for index, row in observed_df.iterrows():
        iter_vars = _solver(row)
        for i in range(len(iter_vars)):
            observed_df.loc[observed_df.seq == row.seq, var_names[i]] = iter_vars[i]
    calculated_df = calculated(observed_df, sno)
    save(calculated_df, user_args, identifier="pcm")


"""
return one df with pcm tuned data
"""

if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection
    import pandas as pd
    from Scripts.pvt_new.settings import DEBUG_DIR
    import os

    well_name = "GC3"

    debug_dir = os.path.join(DEBUG_DIR, well_name)

    db = get_connection(well_name)
    test_user_args = pd.Series({"debug": True, "well_conn": db, "bias": "general", "debug_dir": debug_dir})
    pcm(test_user_args)
