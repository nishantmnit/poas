import numpy as np
from Scripts.pvt_new.core.pandas.dataframe import drop_not_in_cols


def _mnd20(row):
    temp = row.mnd20 - row.S * 52.86 - row.O * 22.74 - row.N * 30.23
    if row.mnd20_hc <= temp * 0.995:
        return temp
    elif row.mnd20_hc >= temp * 1.005:
        return temp
    else:
        return row.mnd20_hc


def _rll(row):
    temp = row.rll20 - row.S * 7.91 - row.O * 1.643 - row.N * 2.84
    if row.rll_hc <= temp * 0.995:
        return temp
    elif row.rll_hc >= temp * 1.005:
        return temp
    else:
        return row.rll_hc


def _d20(row):
    temp = ((row.d25_hc * 1000. * np.exp((-1. * (613.97226 / (row.d25_hc * 1000.) ** 2) * (20. - 25.) * (
            1. + 0.8 * (613.97226 / (row.d25_hc * 1000.) ** 2) * (20. - 25.))))) / 1000.)
    if row.d20_hc <= temp * 0.98:
        return temp
    elif row.d20_hc >= temp * 1.02:
        return temp
    else:
        return row.d20_hc


def clear_df(df):
    needed = ["component", "seq", "C", "H", "mass_hc", "d20_hc", "mbynd20_hc", "rll_hc", "mnd20_hc", "para_hc",
              "d20_por", "d20_ali", "d20_nap", "v25_hc", "cfraction_hc", "lbmv", "factor_f", "hbyc", "mbynd20_dep",
              "S", "N", "O", "mf"]
    drop_not_in_cols(df, needed)


def observed(df):
    df["v25_hc"] = df.v25 - df.S * 14. - df.O * 3.75 - df.N * 1.5
    df["d25_hc"] = df.mass_hc / df.v25_hc
    df["d20_hc"] = df.apply(lambda row: _d20(row), axis=1)
    df["rll_hc"] = df.apply(lambda row: _rll(row), axis=1)
    df["mnd20_hc"] = df.apply(lambda row: _mnd20(row), axis=1)
    df["ri_hc"] = df.mnd20_hc / df.mass_hc
    df["R_20_hc"] = (df.ri_hc ** 2 - 1.) / (df.ri_hc ** 2 + 2.)
    df["mbynd20_hc"] = df.mass_hc / df.ri_hc
    df["mbynd20_dep"] = df.apply(
        lambda row: 0 if (row.mbynd20_hc - (9.5013636627887 * row.C + 5.8626784355479)) > 0 else (
                row.mbynd20_hc - (9.5013636627887 * row.C + 5.8626784355479)), axis=1)
    df["st_hc"] = 34.39 * (((df.d20_hc + 0.1674) ** 14) / df.d20_hc) ** (1. / 8.) - 7.509
    df["para_hc"] = (df.st_hc ** 0.25) * (df.mass_hc / df.d20_hc)
    df["factor_f"] = df.apply(
        lambda row: 1. if (-0.0238805970149254 * row.name + 2.08955223880597 + 0.029850746268657) < 1 else (
                -0.0238805970149254 * row.name + 2.08955223880597 + 0.029850746268657), axis=1)
    df["hbyc"] = df.H / df.C
    # clear_df(df)
    return df


"""
return one df with sno data enhanced for pcm run
"""

if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection
    from Scripts.pvt_new.core.storage.mongo import fetch
    well_name = "GC3"
    db = get_connection(well_name)
    sno = fetch(db, property="pvt_output", identifier="sno")
    print observed(sno)
