import os
from collections import OrderedDict

import pandas as pd


def compare(golden_data_dir, debug_data_dir, well, bias):
    golden_data_dir = os.path.join(golden_data_dir, well, bias)
    debug_data_dir = os.path.join(debug_data_dir, well, bias)

    uniq_csvs, debug_csvs, golden_csvs = sorted_files(debug_data_dir, golden_data_dir)

    output = OrderedDict()
    cols = ["csv", "golden_csv", "debug_csv", "match"]
    for key in cols:
        output[key] = list()

    for csv in uniq_csvs:
        output["csv"].append(csv)

        output["golden_csv"].append(True if csv in golden_csvs else False)

        output["debug_csv"].append(True if csv in debug_csvs else False)

        if csv in golden_csvs and csv in debug_csvs:
            golden_df = pd.read_csv(os.path.join(golden_data_dir, csv))
            debug_df = pd.read_csv(os.path.join(debug_data_dir, csv))
            output["match"].append(golden_df.equals(debug_df))
        else:
            output["match"].append("False")

    df = pd.DataFrame(output)
    return df


def list_files(my_dir):
    if os.path.exists(my_dir):
        files = [dI for dI in os.listdir(my_dir) if not os.path.isdir(os.path.join(my_dir, dI))]
        return files
    return []


def sorted_files(debug_data_dir, golden_data_dir):
    debug_csvs = list_files(debug_data_dir)
    golden_csvs = list_files(golden_data_dir)
    files = list(set(debug_csvs + golden_csvs))
    files.sort(
        key=lambda x: os.path.getmtime(os.path.join(debug_data_dir, x))
        if x in debug_csvs else os.path.getmtime(os.path.join(golden_data_dir, x))
    )
    return files, debug_csvs, golden_csvs


if __name__ == "__main__":
    test_golden_data_dir = "../../test/golden_data"
    test_debug_data_dir = "../../debug"
    test_well = "NORTHALASKAG"
    test_bias = "general"
    test_output = compare(test_golden_data_dir, test_debug_data_dir, test_well, test_bias)
    print test_output
