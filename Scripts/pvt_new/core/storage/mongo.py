import time

import pandas as pd
from Lib.DBLib import getDbCon
from Scripts.pvt_new.core.get_logger import get_logger
from Scripts.pvt_new.core.pandas.dataframe import drop_cols

logger = get_logger("pvt mongo storage")

db = "MBP_NEW"


def write(df_o, well_conn, **kwargs):
    df = df_o.copy(deep=True)
    df["original_index"] = df.index  # preserve index to be set again in fetch
    df.reset_index(inplace=True, drop=True)  # there can be duplicates in index
    df.index = df.index.map(str)
    df["seq"] = range(0, df.shape[0])
    data = df.to_dict("list")
    for key, value in kwargs.items():  # add kwargs to data
        data[key] = value
    data["created_at"] = time.time()
    data["created_by"] = 'pvt mongo'
    delete_one(well_conn, **kwargs)
    r = well_conn.insert(data, check_keys=False)
    logger.info('PVT_WRITE:{%s, "id": "%s"}' % (kwargs, r))


def delete_one(well_conn, **kwargs):
    try:
        prev_data = find_one(well_conn, **kwargs)
        prev_doc_id = prev_data["_id"]
        logger.info('PVT_DELETE:{%s, "id": "%s"}' % (kwargs, prev_doc_id))
    except:
        logger.info('PVT_DELETE:{%s, "id": "%s"}' % (kwargs, "Not Found"))
    well_conn.remove(kwargs)


def delete_all(well_conn, **kwargs):
    logger.info('PVT_DELETE:{%s}' % kwargs)
    well_conn.remove(kwargs)


def fetch(well_conn, **kwargs):
    gen = find_one(well_conn, **kwargs)
    df = pd.DataFrame(gen)
    df.sort_values(by=["seq"], inplace=True)
    if "original_index" in df.columns:
        df.set_index(df.original_index.values, inplace=True, drop=True)
    cols_to_drop = ["_id", "created_at", "created_by", "original_index"] + kwargs.keys()
    cols_to_drop = cols_to_drop + ["distribution", "error"]
    drop_cols(df, cols_to_drop)
    return df


def fetch_all(well_conn, **kwargs):
    gen = find_all(well_conn, **kwargs)
    df = pd.DataFrame(gen)
    df.sort_values(by=["seq"], inplace=True)
    df.reset_index(drop=True, inplace=True)
    cols_to_drop = ["_id", "created_at", "created_by", "original_index"] + kwargs.keys()
    cols_to_drop = cols_to_drop + ["distribution", "error"]
    drop_cols(df, cols_to_drop)
    return df


def find_all(well_conn, **kwargs):
    data = list()
    for key, value in kwargs.items():
        if isinstance(value, list):
            data.append({key: {"$in": value}})
        else:
            data.append({key: value})
    gen = well_conn.find({'$and': data}) if len(data) > 0 else well_conn.find()
    if gen.count() == 0:
        raise RuntimeError("Found no document = %s. Please check and re-run." % data)
    return gen


def find_one(well_conn, **kwargs):
    data = list()
    for key, value in kwargs.items():
        if isinstance(value, list):
            data.append({key: {"$in": value}})
        else:
            data.append({key: value})
    gen = well_conn.find({'$and': data}) if len(data) > 0 else well_conn.find()
    if gen.count() > 1:
        raise RuntimeError("Found more than 1 document = %s. Please check and re-run." % data)
    if gen.count() == 0:
        raise RuntimeError("Found no document = %s. Please check and re-run." % data)
    return gen[0]


def get_connection(well, conn_db="MBP_NEW"):
    db_con = getDbCon(conn_db)
    conn = db_con[well]
    return conn
