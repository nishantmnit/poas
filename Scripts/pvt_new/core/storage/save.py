from Scripts.pvt_new.core.storage.mongo import write
from Scripts.pvt_new.core.storage.excel import write_csv


def save(df, user_args, **kwargs):
    kwargs["property"] = "pvt_output"
    calc_error(df, kwargs["identifier"])
    write(df, user_args.well_conn, **kwargs)
    if user_args.debug:
        write_csv(user_args.debug_dir, kwargs.values(), df)


def calc_error(df, identifier):
    if identifier == "mass":
        df["error"] = ((df["expmass"] - df["mass"]) / df["expmass"]) ** 2
        if "expzi" in df.columns:
            df["mf_error"] = ((df["expzi"] - df["mf"]) / df["expzi"]) ** 2
    elif identifier == "density":
        df["error"] = ((df["expdensity"] - df["density"]) / df["expdensity"]) ** 2
    elif identifier == "bp":
        df["error"] = ((df["expbp"] - df["bp"]) / df["expbp"]) ** 2
    elif identifier == "ri":
        df["error"] = ((df["expri"] - df["ri"]) / df["expri"]) ** 2