import pandas as pd

from Scripts.pvt_new.core.get_logger import get_logger
from Scripts.pvt_new.core.storage.mongo import fetch

logger = get_logger("pvt fetch most")


def validated_data(well_conn):
    df = fetch(well_conn, property="pvt_output", identifier="validated_averaged_data", plus_or_individual="individual")
    dfplus = fetch(well_conn, property="pvt_output", identifier="validated_averaged_data", plus_or_individual="plus")
    return df, dfplus


def input_validated_data(well_conn):
    df = fetch(well_conn, property="pvt_output", identifier="validated_data", plus_or_individual="individual")
    dfplus = fetch(well_conn, property="pvt_output", identifier="validated_data", plus_or_individual="plus")
    return df, dfplus


def recommended(well_conn, what):
    df = fetch(well_conn, property="pvt_output", identifier=what, plus_or_individual="individual", recommended=1)
    if what in ["bp", "ri"]:
        return df
    dfplus = fetch(well_conn, property="pvt_output", identifier=what, plus_or_individual="plus", recommended=1)
    return df, dfplus


def set_recommended(well_conn, identifier):
    if identifier in ("bp", "ri"):
        well_conn.update({'$and': [{'property': 'pvt_output'}, {'identifier': identifier}]},
                         {"$set": {"recommended": 0}}, **{'upsert': False, 'multi': True})
        well_conn.update(
            {'$and': [{'property': 'pvt_output'}, {'identifier': identifier}, {"solver": 1}, {"distribution": "poas"}]},
            {"$set": {"recommended": 1}}, **{'upsert': False, 'multi': True})
        logger.info("PVT_RECOMMENDATION: %s - Distribution = %s, Solver = 1" % (identifier.upper(), "POAS"))
        return 0

    df = pd.DataFrame()
    for r in well_conn.find(
            {'$and': [{'property': 'pvt_output'}, {'identifier': identifier}, {'plus_or_individual': 'plus'}]}):
        df = pd.concat([df, pd.DataFrame(r)[["distribution", "solver", "error"]]])
    df["plus_or_ind"] = "plus"

    df1 = pd.DataFrame()
    for r in well_conn.find(
            {'$and': [{'property': 'pvt_output'}, {'identifier': identifier}, {'plus_or_individual': 'individual'}]}):
        df1 = pd.concat([df1, pd.DataFrame(r)[["distribution", "solver", "error"]]])

    df1["plus_or_ind"] = "ind"
    df = pd.concat([df, df1], axis=0, sort=False)
    df_all = df.groupby(["distribution", "solver"])['error'].agg('sum').reset_index()
    df_all = df_all.loc[df_all.error == df_all.error.min()]
    df_poas = df.loc[df.distribution.isin(["poas", "poas_c"])].groupby(["distribution", "solver"])['error'].agg(
        'sum').reset_index()
    if identifier == "density" and df_poas.loc[df_poas.distribution == "poas_c"].shape[0] > 0:
        df_poas = df_poas.loc[df_poas.distribution == "poas_c"]
    else:
        df_poas = df_poas.loc[df_poas.error == df_poas.error.min()]

    well_conn.update({'$and': [{'property': 'pvt_output'}, {'identifier': identifier}]},
                     {"$set": {"recommended": 0, "poas_recommended": 0}}, **{'upsert': False, 'multi': True})
    well_conn.update({'$and': [{'property': 'pvt_output'}, {'identifier': identifier},
                               {"solver": float(df_all.solver.values[0])},
                               {"distribution": df_all.distribution.values[0]}]},
                     {"$set": {"recommended": 1}}, **{'upsert': False, 'multi': True})
    logger.info("PVT_RECOMMENDATION: %s - Distribution = %s, Solver = %s" % (
        identifier.upper(), df_all.distribution.values[0], df_all.solver.values[0]))

    if df_poas.shape[0] == 1:
        well_conn.update({'$and': [{'property': 'pvt_output'}, {'identifier': identifier},
                                   {"solver": float(df_poas.solver.values[0])},
                                   {"distribution": df_poas.distribution.values[0]}]},
                         {"$set": {"poas_recommended": 1}}, **{'upsert': False, 'multi': True})
        logger.info("PVT_POAS_RECOMMENDATION: %s - Distribution = %s, Solver = %s" % (
            identifier.upper(), df_poas.distribution.values[0], df_poas.solver.values[0]))
