def intercept_slope(x, y):
    samples = len(x)
    xi = sum(x)
    yi = sum(y)
    xiyi = sum(x * y)
    xi2 = sum(x ** 2)
    intercept = (xi * xiyi - yi * xi2) / (xi ** 2 - samples * xi2)
    slope = (yi - samples * intercept) / xi
    return intercept, slope


def quadratic(x, y):
    k1 = (x[0] * x[1] ** 2 - x[1] * x[0] ** 2)
    k2 = (x[0] * x[2] ** 2 - x[2] * x[0] ** 2)
    k3 = (x[1] ** 2 - x[0] ** 2)
    k4 = (x[2] ** 2 - x[0] ** 2)
    d1 = y[0] * x[1] ** 2 - y[1] * x[0] ** 2
    d2 = y[0] * x[2] ** 2 - y[2] * x[0] ** 2
    c = (d1 * k2 - d2 * k1) / (k2 * k3 - k4 * k1)
    b = (d1 - c * k3) / k1
    a = (y[0] - c - b * x[0]) / x[0] ** 2
    return a, b, c
