gasConstant = 8.314472
vapour_pressure = pressure = 0.101325
solubility_temperatures = (200, 250, 288.7056, 300, 350, 400, 475, 550)
valid_eos = ("poas_4", "pt_poas", "generic_poas_a", "als_poas", "poas_4a", "sw_poas", "pr_poas", "srk_poas")
volume_solver_temperature = 288.7056
thermal_coefficient_bp = 0.8
cm_groups = ["group%s" % i for i in range(1, 34)]
