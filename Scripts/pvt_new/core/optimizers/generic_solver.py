from math import sqrt
from Scripts.pvt_new.core.get_logger import get_logger

logger = get_logger("pvt generic solver")


def GenericSolver(fun, boundsOfVars, func, debug=False, t=0.01, tstep=0.01, tmax=0.01, boundsMultiplier=None,
                  maxIter=3000, dtol=10 ** -8, etol=10 ** -6, **kwargs):
    return runSolver(fun, boundsOfVars, func, debug, t, tstep, tmax, boundsMultiplier, maxIter, dtol, etol, **kwargs)


def runSolver(fun_input, boundsOfVars, func, debug, t, tstep, tmax, boundsMultiplier, maxIter_input, dtol, etol,
              **kwargs):
    iter_num = 0
    if "result" not in kwargs:
        kwargs['result'] = list()
    if "r2" not in kwargs: kwargs['r2'] = list()
    if "values" not in kwargs:
        kwargs['values'] = list()
    msg, success = None, True
    while t <= tmax:
        maxIter, fun = maxIter_input, fun_input[:]
        prevMomentum = list()
        for i in range(0, len(fun)):
            prevMomentum.append([None, None])
        for iter_num in range(0, maxIter + 1):
            iter_vars = list()
            for i in range(0, len(fun)):
                iter_vars.append(boundsOfVars[i][0] + fun[i] * (boundsOfVars[i][1] - boundsOfVars[i][0]))
            if min(fun) < 0:
                msg = "function is negative. Terminating Solver!"
                logger.info("%s - %s" % (msg, fun))
                break
            initErr, differentiations, r2 = func(iter_vars) if "args" not in kwargs else func(iter_vars, kwargs["args"])
            kwargs["result"].append(abs(initErr))
            kwargs["values"].append(iter_vars)
            kwargs["r2"].append(r2)
            # for mass & bp 10**-8, for density 10**-10
            if all(abs(x) <= dtol for x in differentiations):
                msg = "All differentiations are less than %s. Terminating Solver!" % (dtol)
                break
            # for mass & bp 10**-6, for density 10**-10
            if iter_num > 1000 and abs(kwargs["result"][-1] - kwargs["result"][-2]) < etol:
                msg = "No change in error functions. Terminating Solver!"
                break
            subtract = list()
            prevMomentumNew = list()
            for i in range(0, len(fun)):
                subtract_um_term, momentum1, momentum2 = get_subtraction_term(
                    differentiations[i] * (boundsOfVars[i][1] - boundsOfVars[i][0]) * (
                        boundsMultiplier if boundsMultiplier is not None and any(
                            d < 10 ** -3 for d in differentiations) else 1.), fun[i], prevMomentum[i])
                subtract.append(subtract_um_term)
                prevMomentumNew.append([momentum1, momentum2])
            prevMomentum = prevMomentumNew
            # debug statement
            # if debug:
            #     print iter_num, \
            #         t, \
            #         boundsMultiplier if boundsMultiplier is not None and any(d < 10**-2 for d in differentiations) \
            #         else 1., fun, iter_vars, initErr, differentiations, subtract, min(kwargs["result"])
            for i in range(0, len(fun)):
                fun[i] = fun[i] - subtract[i] * t * 1.
        t += tstep

    minima = kwargs["result"].index(min(kwargs["result"]))
    if msg is None:
        msg = "Completed %s iterations!" % maxIter_input
    return {"ErrFunc": min(kwargs["result"]), "Values": kwargs["values"][minima], "Regression": kwargs["r2"][minima],
            "Iterations": iter_num, "Message": msg, "success": success}


def get_subtraction_term(error, fun, prevMomentum, Beta=0.9, Beta1=0.99):
    prevMomentum1, prevMomentum2 = [None, None] if len(prevMomentum) == 0 else prevMomentum
    dcr = min([0 - fun, 1 - fun]) if error == 0 else (1 - fun)
    momentum1 = (1 - Beta) * error if prevMomentum1 is None else Beta * prevMomentum1 + (1 - Beta) * error
    momentum2 = (1 - Beta1) * error ** 2 if prevMomentum2 is None else Beta1 * prevMomentum2 + (1 - Beta1) * error ** 2
    subtract_um_term = (1 / ((sqrt(momentum2)) + 10 ** -8)) * momentum1 * dcr if prevMomentum2 is None else (1 / (
            (sqrt(max([momentum2, prevMomentum2]))) + 10 ** -8)) * momentum1 * dcr
    return subtract_um_term, momentum1, momentum2
