import numpy as np
from Scripts.pvt_new.core.get_logger import get_logger

logger = get_logger("ncp solver")


def normalize_grad(grads):
    new_grads = list()
    for grad in grads:
        if np.abs(grad) >= 10 ** 100:
            grad = 10 ** 10
        elif np.abs(grad) >= 10 ** 25:
            for i in range(90, 20, -5):
                if np.abs(grad) >= 10 ** i:
                    grad = grad * 10 ** (-1. * (i - 5.))
        elif np.abs(grad) >= 10 ** 20:
            grad = grad * 10 ** -12
        elif np.abs(grad) >= 10 ** 15:
            grad = grad * 10 ** -5
        elif np.abs(grad) >= 10 ** 10:
            grad = grad * 10 ** -4
        else:
            grad = grad
        new_grads.append(grad)
    return np.array(new_grads)


def define_region(iter_vars, bound, mk_iter_vars):
    lb, ub = list(), list()
    for i in range(len(iter_vars)):
        lb.append(max(bound[i][0], (iter_vars[i] - 4. * iter_vars[i])))
        ub.append(min(bound[i][1], (iter_vars[i] + 4. * iter_vars[i])))
    a_max_l = list()
    for i in range(len(iter_vars)):
        a_max_l.append(
            np.abs(0 if mk_iter_vars[i] == 0
                   else (((iter_vars[i] - ub[i]) / mk_iter_vars[i]) if mk_iter_vars[i] < 0
                         else ((iter_vars[i] - lb[i]) / mk_iter_vars[i]))))

    if 1.5 * min(a_max_l) < 10 ** -4:
        if max(a_max_l) < 5:
            alpha_max = max(a_max_l)
        else:
            alpha_max = 10 ** -4
    else:
        if 1.5 * min(a_max_l) > 50:
            alpha_max = 50.
        else:
            alpha_max = 1.5 * min(a_max_l)

    if alpha_max * 0.0000001 < 10 ** -13:
        alpha_min = 10 ** -13
    else:
        if alpha_max * 0.0000001 > 10 ** -8:
            alpha_min = 10 ** -8
        else:
            alpha_min = alpha_max * 0.0000001
    return alpha_max, alpha_min


def converged(debug, all_vars, all_costs, all_gradients, f_tol):
    # rule 0 - converge if cost is  <= f_tol
    if f_tol is not None:
        if all_costs[-1] <= f_tol:
            if debug:
                logger.info("converged by rule 0 - cost (%s) is <= f_tol (%s)" % (all_costs[-1], f_tol))
            return True

    # rule 1 - if sqrt of sum of squared gradient is <= 10**-3 then exit.
    if np.sqrt(np.sum(np.square(all_gradients[-1]), axis=0)) <= 10 ** -3:
        if debug:
            logger.info("converged by rule 1 - sqrt of sum of squared gradient (%s) is <= 10**-3" % np.sqrt(
                np.sum(np.square(all_gradients[-1]), axis=0)))
        return True
    else:
        # we need to run at-least 4 iterations before declaring it converged
        if len(all_costs) < 4:
            return False
        else:
            # rule 2 - if change in cost for consecutive 3 iterations is less than or equal to 10**-5 then exit.
            cost_diff = np.abs(np.ediff1d(np.array(all_costs))[-3:])
            if np.all(cost_diff <= 10 ** -5):
                if debug:
                    logger.info("converged by rule 2 - change in cost for consecutive 3 "
                                "iterations is less than or equal to 10**-5")
                return True
            # rule 3 - change in all variables <= 10**-10 for consecutive 3 iterations.
            var_diff1 = np.abs(np.array(all_vars[-1]) - np.array(all_vars[-2]))
            var_diff2 = np.abs(np.array(all_vars[-2]) - np.array(all_vars[-3]))
            if np.all(var_diff1 <= 10 ** -10) and np.all(var_diff2 <= 10 ** -10):
                if debug:
                    logger.info("converged by rule 3 - change in all variables <= 10**-10 for consecutive 3 iterations")
                return True
            if len(all_costs) >= 20:
                # rule 4 - if change in cost for consecutive 10 iterations is less than or equal to 10**-2 then exit.
                cost_diff = np.abs(np.ediff1d(np.array(all_costs))[-10:])
                if np.all(cost_diff <= 10 ** -2):
                    if debug:
                        logger.info("converged by rule 4 - change in cost for consecutive 10 "
                                    "iterations is less than or equal to 10**-2")
                    return True
    return False


class ncg(object):
    def __init__(self):
        self.pgrad = None
        self.piter_vars = None
        self.all_costs, self.all_vars, self.all_gradients = list(), list(), list()  # needed for convergence function
        self.region_f = None
        self.converge_f = None
        self.yk = None
        self.get_alphas_f = None

    def minimize(self, s_iter_vars, bound, cost_f, max_iter=200, debug=False, region_f=None, converge_f=None,
                 f_tol=None, get_alphas_f=None, alpha_multiplier=1., **kwargs):
        curr_iteration = 1
        iter_vars = s_iter_vars
        self.region_f = define_region if region_f is None else region_f
        self.converge_f = converged if converge_f is None else converge_f
        self.get_alphas_f = self.get_alphas if get_alphas_f is None else get_alphas_f

        while curr_iteration <= max_iter:
            cost, grad = cost_f(iter_vars) if "args" not in kwargs else cost_f(iter_vars, kwargs["args"])
            self.all_vars.append(iter_vars)
            self.all_costs.append(cost)
            grad = normalize_grad(grad)
            self.all_gradients.append(grad)
            mk_iter_vars = self.mk_iter_vars(iter_vars, grad)
            min_alpha = self.min_alpha(iter_vars, bound, mk_iter_vars, cost_f, alpha_multiplier, **kwargs)
            if debug:
                logger.info(
                    "iteration == %s, cost == %s, gradients == %s, mk_iter_vars == %s, "
                    "alpha == %s, iter_vars == %s" % (
                        curr_iteration, cost, grad, mk_iter_vars, min_alpha, iter_vars))
            if self.converge_f(debug, self.all_vars, self.all_costs, self.all_gradients, f_tol):
                break
            self.pgrad, self.piter_vars = grad, iter_vars
            iter_vars = self.regress(iter_vars, bound, grad, mk_iter_vars, min_alpha, curr_iteration)
            curr_iteration += 1
        minima = self.all_costs.index(min(self.all_costs))
        return self.all_vars[minima]

    def mk_iter_vars(self, iter_vars, grad):
        if len(self.all_gradients) > 1:
            yk = np.array(self.all_gradients[-1]) - np.array(self.all_gradients[-2])
            sk = np.array(self.all_vars[-1]) - np.array(self.all_vars[-2])
        else:
            yk = np.zeros(grad.size)
            sk = np.zeros(grad.size)
        multi1 = yk.dot(sk.T)
        multi1 = 10 ** -8 if multi1 == 0 else (np.sign(multi1) * 10 ** -8 if abs(multi1) <= 10 ** -8 else multi1)
        multi2 = ((yk.dot(grad.T)) / multi1 - 2. * ((yk.dot(yk.T)) * (sk.dot(grad.T))) / multi1 ** 2)
        multi3 = sk.dot(grad.T) / multi1
        mk_iter_vars = list()
        for i in range(len(iter_vars)):
            mk_iter_vars.append(grad[i] - multi2 * sk[i] - multi3 * yk[i])
        self.yk = yk  # patching to pass yk to regress function.
        return np.array(mk_iter_vars)

    def min_alpha(self, iter_vars, bound, mk_iter_vars, cost_f, alpha_multiplier, **kwargs):
        calphas, niter_vars = list(), list()
        alphas = self.get_alphas_f(iter_vars, bound, mk_iter_vars)
        for alpha in alphas:
            new_iter_vars = np.array(iter_vars) - alpha * np.array(mk_iter_vars)
            for i in range(len(new_iter_vars)):
                new_iter_vars[i] = min(bound[i][1], max(bound[i][0], new_iter_vars[i]))
            # new_iter_vars = [0 if x < 0 else x for x in new_iter_vars]
            niter_vars.append(new_iter_vars)
            cost = cost_f(new_iter_vars, only_cost=True) if "args" not in kwargs \
                else cost_f(new_iter_vars, kwargs["args"], only_cost=True)
            calphas.append(cost)
        m_index = calphas.index(min(calphas))
        m_alpha = alphas[m_index]
        m_cost = calphas[m_index]
        if m_index == 0 or m_index == len(alphas)-1:
            return m_alpha
        else:
            alpha1, alpha2, alpha3 = alphas[m_index - 1], alphas[m_index - 2], alphas[m_index + 1]
            cost1, cost2, cost3 = calphas[m_index - 1], calphas[m_index - 2], calphas[m_index + 1]
            k1 = (alpha1 ** 3 - alpha2 ** 3) * (m_alpha - alpha2) - (m_alpha ** 3 - alpha2 ** 3) * (alpha1 - alpha2)
            k2 = (alpha1 ** 2 - alpha2 ** 2) * (m_alpha - alpha2) - (m_alpha ** 2 - alpha2 ** 2) * (alpha1 - alpha2)
            k3 = (alpha1 ** 3 - alpha2 ** 3) * (alpha3 - alpha2) - (alpha3 ** 3 - alpha2 ** 3) * (alpha1 - alpha2)
            k4 = (alpha1 ** 2 - alpha2 ** 2) * (alpha3 - alpha2) - (alpha3 ** 2 - alpha2 ** 2) * (alpha1 - alpha2)
            p1 = (cost1 - cost2) * (m_alpha - alpha2) - (m_cost - cost2) * (alpha1 - alpha2)
            p2 = (cost1 - cost2) * (alpha3 - alpha2) - (cost3 - cost2) * (alpha1 - alpha2)

            a = 0 if (k1 * k4 - k2 * k3) == 0 else (p1 * k4 - p2 * k2) / (k1 * k4 - k2 * k3)
            if np.abs(a) > 10 ** 50:
                a = 10 ** 50

            b = 0 if k2 == 0 else (p1 - a * k1) / k2
            if np.abs(b) > 10 ** 50:
                b = 10 ** 50

            c = 0 if (alpha1 - alpha2) == 0 else ((cost1 - cost2) - a * (alpha1 ** 3 - alpha2 ** 3) - b * (
                    alpha1 ** 2 - alpha2 ** 2)) / (alpha1 - alpha2)
            if np.abs(c) > 10 ** 50:
                c = 10 ** 50

            d = cost2 - (a * alpha2 ** 3 + b * alpha2 ** 2 + c * alpha2)
            if np.abs(d) > 10 ** 50:
                d = 10 ** 50

            a1 = m_alpha if (4 * b ** 2 - 12 * a * c) < 0 or a == 0 else (
                    (-2 * b + np.sqrt(4 * b ** 2 - 12 * a * c)) / (6 * a))
            a2 = m_alpha if (4 * b ** 2 - 12 * a * c) < 0 or a == 0 else (-2 * b - np.sqrt(4 * b ** 2 - 12 * a * c)) / (
                    6 * a)

            a1_f = a * a1 ** 3 + b * a1 ** 2 + c * a1 + d
            a2_f = a * a2 ** 3 + b * a2 ** 2 + c * a2 + d
            alpha = a1 if a1_f <= a2_f else a2

        # find cost on interpolated alpha, compare it with previous minimum cost and select the alpha which gives
        # real low cost
        new_iter_vars = np.array(iter_vars) - alpha * np.array(mk_iter_vars)
        for i in range(len(new_iter_vars)):
            new_iter_vars[i] = min(bound[i][1], max(bound[i][0], new_iter_vars[i]))
        # new_iter_vars = [0 if x < 0 else x for x in new_iter_vars]
        cost = cost_f(new_iter_vars, only_cost=True) if "args" not in kwargs \
            else cost_f(new_iter_vars, kwargs["args"], only_cost=True)

        if cost > m_cost:
            return m_alpha
        return alpha * alpha_multiplier

    def get_alphas(self, iter_vars, bound, mk_iter_vars):
        try:
            a_max, a_min = self.region_f(iter_vars, bound, mk_iter_vars)
        except:
            a_max, a_min = define_region(iter_vars, bound, mk_iter_vars)
        alphas = [a_min]
        for i in range(7):
            alphas.append(
                alphas[-1] * 4. if a_min == 0 or a_max == 0 else (alphas[-1] * (0.6 * a_max / a_min) ** (1. / 8.)))
        for i in range(7):
            alphas.append(
                alphas[-1] * 2. if alphas[7] == 0 or a_max == 0 else (alphas[-1] * (a_max / alphas[7]) ** (1. / 7.)))
        pull_back = list()
        for i in range(len(alphas)):
            if i % 2 == 0 and 4 <= i <= 12:
                multi = -1. * 0.8 if i == 12 else -1.
                pull_back.append((alphas[i] + alphas[i + 1]) / 2. * multi)
        pull_back.reverse()
        alphas = pull_back + alphas
        return alphas

    def regress(self, iter_vars, bound, grad, mk_iter_vars, alpha, iteration):
        niter_vars = np.array(iter_vars) - alpha * mk_iter_vars
        for i in range(len(iter_vars)):
            if np.all(np.abs(grad) > 1) and np.all(np.abs(self.yk) <= 10 ** -50) and iteration > 1:
                niter_vars[i] = (bound[i][0] + bound[i][1]) / 2
            else:
                niter_vars[i] = min(bound[i][1], max(bound[i][0], niter_vars[i]))
        return niter_vars
