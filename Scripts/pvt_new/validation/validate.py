import pandas as pd

from Scripts.pvt_new.core.get_logger import get_logger
from Scripts.pvt_new.core.storage.save import save
from average_input import average
from fix_mass import fix_mass
from fix_mf import fix_mf
from fix_sg import fix_sg
from read_user_input import individuals, plus, whole
from rf_mf import rf_mf
from validate_wc import validate_wc
from wf_to_mf import wf_to_mf
from other_data import other_data_validation

logger = get_logger("pvt validation")


def consistency(df_plus, df_ind):
    ind_max = df_ind.short_name.max()
    plus_max = df_plus.short_name.max()
    if ind_max + 1 != plus_max:
        msg = 'Error:{"Type": "Validation", "Message": "Consistency check failed: Maximum individual short name ' \
              'should be one(1) less than Maximum plus fraction."}'
        raise Exception(msg)


def validate(user_args):
    df_ind, df_plus, df_wc = individuals(user_args.well_conn), plus(user_args.well_conn), whole(user_args.well_conn)
    consistency(df_plus, df_ind)
    df_ind, df_plus, df_wc = validate_wc(df_ind, df_plus, df_wc)
    if not df_ind.fl_wf.isnull().values.any():
        df_ind, df_plus = wf_to_mf(df_ind, df_plus, df_wc)
    df_ind, df_plus = fix_mf(df_ind, df_plus)
    df_ind, df_plus = fix_mass(df_ind, df_plus, df_wc)
    df_ind, df_plus = rf_mf(df_ind, df_plus, df_wc)
    # commented after discussion on 20th June 2022 as AustraliaOilA was not proceeding further.
    # df_ind, df_plus = fix_sg(df_ind, df_plus, df_wc)
    save(df_ind, user_args, plus_or_individual="individual", identifier="validated_data")
    save(df_plus, user_args, plus_or_individual="plus", identifier="validated_data")
    prepare_data(df_ind, df_plus, user_args)
    other_data_validation(user_args)
    logger.info("validation completed")


def prepare_data(df_ind_o, df_plus_o, user_args):
    df_ind_o.loc[df_ind_o.rf_mf == 0, "rf_mf"] = 10 ** -20
    df_ind, df_plus = pd.DataFrame(), pd.DataFrame()
    df_ind[["scn", "component", "mf", "expmass", "expdensity", "expbp", "expri"]] = df_ind_o.loc[
        df_ind_o.short_name >= 4, ["scn", "component", "rf_mf", "mw", "sg", "bp", "ri"]]
    df_plus[["scn", "mf", "expmass", "expdensity"]] = df_plus_o[["scn", "rf_mf", "rf_mw", "rf_sg"]]
    df_ind.index = df_ind_o.loc[df_ind_o.short_name >= 4, "short_name"].astype(int)
    df_plus.index = df_plus_o["short_name"].astype(int)
    df_ind = average(df_ind)
    save(df_ind, user_args, plus_or_individual="individual", identifier="validated_averaged_data")
    save(df_plus, user_args, plus_or_individual="plus", identifier="validated_averaged_data")
    logger.info("max plus fraction = %s+" % df_plus.index.max())


if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection

    db = get_connection("AustraliaOilA")
    test_user_args = pd.Series({"debug": False, "well_conn": db})
    validate(test_user_args)
