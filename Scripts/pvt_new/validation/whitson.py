import numpy as np
import pandas as pd
from scipy import optimize

from Scripts.pvt_new.core.get_logger import get_logger

logger = get_logger("pvt validate whitson")

added_7_plus = False


def define_plus_fractions(df, dfplus_o, desired_plus_fractions=None):
    dfplus = None
    if desired_plus_fractions is None:
        desired_plus_fractions = [4, 5, 7, 12, 20, 25, 30, 36] + \
                                 [dfplus_o.index.max()] + dfplus_o.loc[~dfplus_o.expdensity.isnull()].index.tolist()
        desired_plus_fractions = list(set(desired_plus_fractions))
        desired_plus_fractions.sort()
    for plus_fraction in desired_plus_fractions:
        if plus_fraction > dfplus_o.index.max():
            break
        last_mass, last_mf = dfplus_o.loc[dfplus_o.index == dfplus_o.index.max(), ["expmass", "mf"]].values[0]
        df_s = df.loc[((df.index >= plus_fraction) & (df.index < dfplus_o.index.max()))]
        data = [df_s.mf.sum() + last_mf,
                ((df_s.mf * df_s.expmass).sum() + last_mass * last_mf) / (df_s.mf.sum() + last_mf)]
        temp_df = pd.DataFrame(data=[data], columns=["mf", "expmass"], index=[plus_fraction])
        dfplus = temp_df if dfplus is None else pd.concat([dfplus, temp_df], axis=0)
    dfplus["scn"] = dfplus.apply(lambda row: "C%s" % row.name, axis=1)
    dfplus["expdensity"] = dfplus_o["expdensity"]
    # if "user_mf" in dfplus_o.columns: dfplus["user_mf"] = dfplus_o["user_mf"]
    return dfplus


def cost_function2(iter_vars, df_o, c7mf):
    df = df_o.copy(deep=True)
    df.loc[df.index >= 7, "mf"] = df.loc[df.index >= 7, "mf"] * iter_vars[0]
    return np.abs(df.loc[df.index >= 7, "mf"].sum() - c7mf)


def exponentialFit(df, dfplus):
    start_point = df.index.max() - 7
    ln_zi = np.log(df.loc[df.index >= start_point, "mf"]).tolist()
    diffs = list()
    for i in range(1, len(ln_zi)):
        diffs.append((ln_zi[i] - ln_zi[i - 1]))
    avg_slope = np.average(diffs)
    scns = [x for x in range(df.index.max() + 1, 122)]
    df = pd.concat([df, pd.DataFrame(data=["C%s" % x for x in scns], columns=["scn"], index=scns)], axis=0,
                   sort=True)
    df.loc[df.index == start_point, "mf"] = np.log(df.loc[df.index == start_point, "mf"])
    df.loc[df.index > start_point, "mf"] = avg_slope
    df.loc[df.index >= start_point, "mf"] = df.loc[df.index >= start_point, "mf"].cumsum()
    df.loc[df.index >= start_point, "mf"] = np.exp(df.loc[df.index >= start_point, "mf"])
    df.loc[df.index == 121, "mf"] = df.loc[df.index == 120, "mf"].values[0] * 5
    res = optimize.minimize(cost_function2, np.asarray(1.), bounds=[[0.001, 500.]], method="SLSQP",
                            args=(df.copy(deep=True), dfplus.loc[dfplus.index == 7, "mf"].values[0]),
                            options={'maxiter': 500})
    if not res.success:
        msg = 'Error:{"Type": "Validation", "Message": "Failed to Extend MFs. ' \
              'Please check input data or reach out to POAS support team."}'
        raise Exception(msg)
    print "Exponential MF extender result -->", res
    df.loc[df.index >= 7, "mf"] = df.loc[df.index >= 7, "mf"] * res.x[0]
    last = df.loc[(df.mf < 10 ** -6) & (df.index >= min(scns))].index.min() - 1
    df.loc[df.index == last, "mf"] = df.loc[df.index >= last, "mf"].sum()
    df.drop(df.loc[df.index > last].index, inplace=True)
    dfplus["mf"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "mf"].sum(), axis=1)
    return df, dfplus


def find_last_zi(df, after):
    last = df.loc[(df.final_zi < 10 ** -6) & (df.index >= after)].index.min() - 1
    df.loc[df.index == last, "final_zi"] = df.loc[df.index >= last, "final_zi"].sum()
    df.drop(df.loc[df.index > last].index, inplace=True)
    return df


def calc_mf(df, dfplus, iter_vars):
    a, b, n = iter_vars
    df["expzi"] = df["mf"]
    dfplus["expzi"] = dfplus["mf"]
    df["scn"] = df.index
    last_exp_value = df.index.max()
    scns = [x for x in range(df.index.max() + 1, 122)]
    df = pd.concat([df, pd.DataFrame(data=scns, columns=["scn"], index=scns)], axis=0, sort=True)
    df.index = df.scn
    df.loc[df.index >= 15, "zi"] = a * df.loc[df.index >= 15].index ** b
    df.loc[df.index == 121, "zi"] = df.loc[df.index == 121, "zi"] * n
    df["final_zi"] = df.apply(lambda row: row.expzi if row.name <= (last_exp_value - 4) else row.zi, axis=1)
    df.loc[df.index == last_exp_value - 3, "final_zi"] = (df.loc[df.index == last_exp_value - 4, "final_zi"].values[
                                                              0] +
                                                          df.loc[df.index == last_exp_value - 2, "final_zi"].values[
                                                              0]) / 2.
    df = find_last_zi(df, min(scns))
    dfplus["zi"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "final_zi"].sum(), axis=1)
    return df, dfplus


def zi_error(iter_vars, df, dfplus):
    df, dfplus = calc_mf(df, dfplus, iter_vars)
    df["error"] = np.abs(df.zi - df.expzi)
    dfplus["error"] = np.abs(dfplus.zi - dfplus.expzi)
    return df, dfplus


def zi_constraint(iter_vars, df, dfplus, scn):
    df, dfplus = zi_error(iter_vars, df, dfplus)
    return 10 ** -5 - dfplus.loc[dfplus.index == scn, "error"].values[0]


def constraints(df, dfplus):
    constraints_list = list()
    constraints_list.append({'type': 'ineq', 'fun': zi_constraint, 'args': [df, dfplus, 7]})
    constraints_list.append({'type': 'ineq', 'fun': zi_constraint, 'args': [df, dfplus, dfplus.index.max()]})
    return constraints_list


def cost_function(iter_vars, df, dfplus):
    df, dfplus = zi_error(iter_vars, df, dfplus)
    return df.error.sum() + dfplus.error.sum()


def extend(df, dfplus):
    global added_7_plus
    logger.info("Extending MFs.")
    if 7 not in dfplus.index:
        added_7_plus = True
        dfplus = define_plus_fractions(df, dfplus, [7] + dfplus.index.tolist())
    res = optimize.minimize(cost_function, np.array([.5, -0.5, 1.1]), bounds=[[0.1, 50000.], [-50., -0.1], [1.1, 5.]],
                            method="SLSQP", constraints=constraints(df.copy(deep=True), dfplus.copy(deep=True)),
                            args=(df.copy(deep=True), dfplus.copy(deep=True)), options={'maxiter': 500})
    if not res.success:
        logger.info("Solver to extend MF 1 failed. Extending MFs using exponential fit.")
        df, dfplus = exponentialFit(df, dfplus)
    else:
        logger.info("MF Extender slope result - %s" % res)
        df, dfplus = calc_mf(df, dfplus, res.x)
        df.drop(columns=["expzi", "zi", "mf", "scn"], inplace=True)
        dfplus.drop(columns=["mf", "expzi"], inplace=True)
        df.rename(columns={'final_zi': 'mf'}, inplace=True)
        dfplus.rename(columns={'zi': 'mf'}, inplace=True)
    if added_7_plus:
        dfplus.drop([7], inplace=True)
    logger.info("Extended MFs. till %s" % (df.index.max()))
    return df, dfplus


if __name__ == "__main__":
    pass
