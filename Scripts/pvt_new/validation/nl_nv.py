import numpy as np
from scipy import optimize

from Scripts.pvt_new.core.get_logger import get_logger

logger = get_logger("pvt validate find nl nv")


def cost_function(ivars, df_ind_o, df_plus_o, df_wc):
    df_ind, df_plus = df_ind_o.copy(deep=True), df_plus_o.copy(deep=True)
    nl = ivars[0]
    nv = 1. - nl
    df_ind["wc_mf"] = df_ind.fl_mf * nl + df_ind.fg_mf * nv
    df_plus["wc_mf"] = df_plus.fl_mf * nl + df_plus.fg_mf * nv
    df_ind["zimi"] = df_ind.wc_mf * df_ind.mw
    df_plus["zimi"] = df_plus.wc_mf * df_plus.rf_mw
    mw = df_ind.zimi.sum() + df_plus.loc[df_plus.short_name == df_plus.short_name.max(), "zimi"].sum()
    mw = mw / (df_ind.wc_mf.sum() + df_plus.loc[df_plus.short_name == df_plus.short_name.max(), "wc_mf"].sum())
    return np.abs(df_wc.rf_mw.values[0] - mw)


def find_nl_nv(df_ind, df_plus, df_wc):
    nl, nv = None, None
    logger.info("NL and NV not provided. Trying to find NL and NV.")
    if not df_wc.fl_mw.isnull().values.any() and not df_wc.fg_mw.isnull().values.any() \
            and not df_wc.rf_mw.isnull().values.any():
        logger.info("Calculating NL and NV from Whole crude Properties.")
        nl = ((df_wc.rf_mw - df_wc.fg_mw) / (df_wc.fl_mw - df_wc.fg_mw)).values[0]
        nv = 1. - nl
    elif df_ind.fl_mf.isnull().values.any() or df_ind.fg_mf.isnull().values.any() \
            or df_plus.fl_mf.isnull().values.any() or df_plus.fg_mf.isnull().values.any() \
            or df_plus.rf_mw.isnull().values.any() or df_wc.rf_mw.isnull().values.any():
        msg = 'Error:{"Type": "Validation", "Message": "Please provide Monophasic liquid mf or flashed liquid, ' \
              'gas mf and whole crude Monophasic molecular weight."}'
        raise Exception(msg)
    else:
        res = optimize.shgo(cost_function, bounds=[[0.001, 1.]], args=(df_ind, df_plus, df_wc), iters=5)
        if not res.success:
            msg = 'Error:{"Type": "Validation", "Message": "Failed to find optimal NL and NV values. ' \
                  'Please provide nl and nv values or Monophasic fluid mf."}'
            raise Exception(msg)
        nl = res.x[0]
        nv = 1. - nl
    logger.info("Found NL and NV.")
    return nl, nv


if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection
    from read_user_input import individuals, plus, whole

    db = get_connection("ALUMM")
    test_df_ind, test_df_plus, test_df_wc = individuals(db), plus(db), whole(db)
    test_nl, test_nv = find_nl_nv(test_df_ind, test_df_plus, test_df_wc)
    print test_nl, test_nv
