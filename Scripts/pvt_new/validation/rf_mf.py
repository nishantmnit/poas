from Scripts.pvt_new.core.get_logger import get_logger
from nl_nv import find_nl_nv

logger = get_logger("pvt validate rf mf")


def nl_available(df_ind, df_plus, df_wc):
    if df_wc.nl.isnull().values.any() and df_wc.nv.isnull().values.any():
        nl, nv = find_nl_nv(df_ind, df_plus, df_wc)
    else:
        nl, nv = df_wc.nl.values[0], df_wc.nv.values[0]
    return nl, nv


def plus_mw(df_ind, df_plus, mf_column, mw_column):
    df_plus[mw_column] = df_plus.apply(lambda row: ((df_ind.loc[df_ind.short_name >= row.short_name, mf_column] *
                                                     df_ind.loc[df_ind.short_name >= row.short_name, "mw"]).sum()
                                                    + get_last_plus(df_plus, mw_column) *
                                                    get_last_plus(df_plus, mf_column)) /
                                                   (df_ind.loc[df_ind.short_name >= row.short_name,
                                                               mf_column].sum() +
                                                    get_last_plus(df_plus, mf_column)), axis=1)
    df_plus[mw_column].fillna(0, inplace=True)
    return df_plus


def get_last_plus(df_plus, column):
    return df_plus.loc[df_plus.short_name == df_plus.short_name.max(), column].values[0]


def rf_mf(df_ind, df_plus, df_wc):
    if df_ind.rf_mf.isnull().values.any() or df_plus.rf_mf.isnull().values.any():
        logger.info("Monophasic fluid mf not provided.")
        nl, nv = nl_available(df_ind, df_plus, df_wc)
        if df_ind.fl_mf.isnull().values.any() or df_ind.fg_mf.isnull().values.any() \
                or df_plus.fl_mf.isnull().values.any() or df_plus.fg_mf.isnull().values.any():
            msg = 'Error:{"Type": "Validation", "Message": "Insufficient data to run calculations. ' \
                  'Please provide monophasic fluid MFs or other data to derive the values."}'
            raise Exception(msg)
        df_ind["rf_mf"] = df_ind.fl_mf * nl + df_ind.fg_mf * nv
        df_plus["rf_mf"] = df_plus.fl_mf * nl + df_plus.fg_mf * nv

        if not df_plus.fl_mw.isnull().values.any() and not df_plus.fl_mf.isnull().values.any() \
                and not df_plus.fg_mw.isnull().values.any() and not df_plus.fg_mf.isnull().values.any() \
                and not df_plus.rf_mf.isnull().values.any() and df_plus.rf_mw.isnull().values.any():
            df_plus["rf_mw"] = (df_plus.fl_mw * df_plus.fl_mf * nl +
                                df_plus.fg_mw * df_plus.fg_mf * nv) / df_plus.rf_mf
        elif df_plus.rf_mw.isnull().values.any():
            msg = 'Error:{"Type": "Validation", "Message": "Insufficient data to run calculations. ' \
                  'Please provide monophasic fluid MWs or other data to derive the values."}'
            raise Exception(msg)
        df_plus = plus_mw(df_ind, df_plus, "rf_mf", "rf_mw")
    return df_ind, df_plus


if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection
    from read_user_input import individuals, plus, whole

    db = get_connection("ALUMM")
    test_df_ind, test_df_plus, test_df_wc = individuals(db), plus(db), whole(db)
    test_df_ind, test_df_plus = rf_mf(test_df_ind, test_df_plus, test_df_wc)
    print test_df_ind
    print test_df_plus
