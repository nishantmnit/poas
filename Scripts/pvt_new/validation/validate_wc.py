import numpy as np
from scipy import optimize

from Scripts.pvt_new.core.get_logger import get_logger

logger = get_logger("pvt validate wc")

columns = ["fl", "fg", "rf"]


def validate_wc(df_ind, df_plus, df_wc):
    for col in columns:
        if missing_mass(df_wc, col) and can_be_calculated(df_ind, df_plus, col):
            logger.info("Trying to find wc %s_mw" % col)
            df_ind, df_plus, df_wc = calculate_mw(df_ind, df_plus, df_wc, col)
        if missing_sg(df_wc, col) and can_be_calculated_sg(df_ind, df_plus, col):
            logger.info("Trying to find wc %s_sg" % col)
            df_wc = calculate_sg(df_ind, df_plus, df_wc, col)
    return df_ind, df_plus, df_wc


def missing_mass(df_wc, col):
    if df_wc["%s_mw" % col].isnull().values.any():
        return True
    return False


def missing_sg(df_wc, col):
    if df_wc["%s_sg" % col].isnull().values.any():
        return True
    return False


def can_be_calculated(df_ind, df_plus, col):
    if (not df_ind["%s_mf" % col].isnull().values.any() or not df_ind[
        "%s_wf" % col].isnull().values.any()) and not df_ind.mw.isnull().values.any() and (
            not df_plus["%s_mf" % col].isnull().values.any()
            or not df_plus["%s_wf" % col].isnull().values.any()) \
            and not df_plus["%s_mw" % col].isnull().values.any():
        return True
    return False


def can_be_calculated_sg(df_ind, df_plus, col):
    if not df_ind["%s_mf" % col].isnull().values.any() and not df_ind.sg.isnull().values.any() \
            and not df_plus["%s_mf" % col].isnull().values.any() \
            and not df_plus["%s_sg" % col].isnull().values.any():
        return True
    return False


def calculate_sg(df_ind, df_plus, df_wc, col):
    zimi = (df_ind["%s_mf" % col] * df_ind.mw).sum() + (
            df_plus.loc[df_plus.short_name == df_plus.short_name.max(), "%s_mf" % col] *
            df_plus.loc[df_plus.short_name == df_plus.short_name.max(), "%s_mw" % col]).sum()
    zimibysgi = (df_ind["%s_mf" % col] * df_ind.mw / df_ind.sg).sum() + (
            df_plus.loc[df_plus.short_name == df_plus.short_name.max(), "%s_mf" % col] *
            df_plus.loc[df_plus.short_name == df_plus.short_name.max(), "%s_mw" % col] /
            df_plus.loc[df_plus.short_name == df_plus.short_name.max(), "%s_sg" % col]).sum()
    df_wc["%s_sg" % col] = zimi / zimibysgi
    return df_wc


def calculate_mw(df_ind, df_plus, df_wc, col):
    if not df_ind["%s_mf" % col].isnull().values.any() and not df_plus["%s_mf" % col].isnull().values.any():
        logger.info("Executing Method 1 to find out WC MW")
        zimi = (df_ind["%s_mf" % col] * df_ind.mw).sum() + (
                df_plus.loc[df_plus.short_name == df_plus.short_name.max(), "%s_mf" % col] *
                df_plus.loc[df_plus.short_name == df_plus.short_name.max(), "%s_mw" % col]).sum()
        zi = df_ind["%s_mf" % col].sum() + df_plus.loc[
            df_plus.short_name == df_plus.short_name.max(), "%s_mf" % col].sum()
        df_wc["%s_mw" % col] = zimi / zi
    else:
        logger.info("Executing Method 2 to find out WC MW")
        res = optimize.shgo(cost_function, bounds=[[1., 10000.]], args=(df_ind, df_plus, col), iters=5)
        if not res.success:
            msg = 'Error:{"Type": "Validation", "Message": "Failed to derive whole crude molecular weights. ' \
                  'Please check input data."}'
            raise Exception(msg)
        df_wc["%s_mw" % col] = res.x[0]
        df_ind, df_plus = calc_zi(res.x, df_ind, df_plus, col)
        df_ind["%s_mf" % col] = df_ind.zi
        df_plus["%s_mf" % col] = df_plus.calc_zi
        df_ind.drop(columns=["zimi", "zi"], inplace=True)
        df_plus.drop(columns=["zimi", "exp_zi", "calc_zi", "error"], inplace=True)
    return df_ind, df_plus, df_wc


def cost_function(ivars, df_ind_o, df_plus_o, col):
    df_ind, df_plus = calc_zi(ivars, df_ind_o, df_plus_o, col)
    return df_plus.error.sum()


def calc_zi(ivars, df_ind_o, df_plus_o, col):
    df_ind, df_plus = df_ind_o.copy(deep=True), df_plus_o.copy(deep=True)
    multiplier = 100. / (df_ind["%s_wf" % col].sum() + df_plus.loc[
        df_plus.short_name == df_plus.short_name.max(), "%s_wf" % col].sum())
    df_ind["%s_wf" % col] = df_ind["%s_wf" % col] * multiplier
    df_ind["zimi"] = df_ind["%s_wf" % col] * ivars[0] / 100.
    df_ind["zi"] = df_ind.zimi / df_ind.mw
    df_plus["zimi"] = df_plus["%s_wf" % col] * ivars[0] / 100.
    df_plus["exp_zi"] = df_plus.apply(lambda row: exp_zi(df_ind, row), axis=1)
    df_plus["calc_zi"] = df_plus.zimi / df_plus["%s_mw" % col]
    df_plus["calc_zi"].fillna(0, inplace=True)
    df_plus["error"] = np.abs(df_plus.exp_zi - df_plus.calc_zi)
    return df_ind, df_plus


def exp_zi(df, row):
    index = df.loc[df.short_name <= row.short_name - 1].index.max()
    return 1. - df.loc[df.index < index, "zi"].sum()


if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection
    from read_user_input import individuals, plus, whole

    db = get_connection("ALUMM")
    test_df_ind, test_df_plus, test_df_wc = individuals(db), plus(db), whole(db)
    test_df_ind, test_df_plus, test_df_wc = validate_wc(test_df_ind, test_df_plus, test_df_wc)
    print test_df_ind
    print test_df_plus
    print test_df_wc
