import numpy as np
from scipy import optimize

from Scripts.pvt_new.core.get_logger import get_logger

logger = get_logger("pvt validate wf to mf")


def nl_nv(df_ind, df_plus, df_wc, nl, nv):
    if not df_ind.fl_wf.isnull().values.any() and not df_ind.fg_wf.isnull().values.any() \
            and not df_plus.fl_wf.isnull().values.any() and not df_plus.fg_wf.isnull().values.any() \
            and not df_wc.fg_mw.isnull().values.any() and not df_wc.fl_mw.isnull().values.any():
        df_ind["fl_mf"] = (df_ind.fl_wf * df_wc.fl_mw.values[0] / df_ind.mw) / 100.
        df_ind["fg_mf"] = (df_ind.fg_wf * df_wc.fg_mw.values[0] / df_ind.mw) / 100.
        df_plus["fl_mf"] = (df_plus.fl_wf * df_wc.fl_mw.values[0] / df_plus.fl_mw) / 100.
        df_plus["fg_mf"] = (df_plus.fg_wf * df_wc.fg_mw.values[0] / df_plus.fg_mw) / 100.
        df_plus.fl_mf.fillna(0, inplace=True)
        df_plus.fg_mf.fillna(0, inplace=True)
        df_ind["rf_mf"] = df_ind.fl_mf * nl + df_ind.fg_mf * nv
        df_plus["rf_mf"] = df_plus.fl_mf * nl + df_plus.fg_mf * nv
    else:
        msg = 'Error:{"Type": "Validation", "Message": "Insufficient data to calculate Monophasic MF."}'
        raise Exception(msg)
    return df_ind, df_plus


def cost_function(ivars, df_ind_o, df_plus_o, df_wc):
    df_ind, df_plus = df_ind_o.copy(deep=True), df_plus_o.copy(deep=True)
    nl = ivars[0]
    nv = 1. - nl
    df_ind, df_plus = nl_nv(df_ind, df_plus, df_wc, nl, nv)
    if not df_ind.rf_wf.isnull().values.any():
        df_ind["zimi"] = df_ind.rf_mf * df_ind.mw
        df_plus["zimi"] = df_plus.rf_mf * df_plus.rf_mw
        total = df_ind["zimi"].sum() + df_plus.loc[df_plus.short_name == df_plus.short_name.max(), "zimi"].sum()
        df_ind["error"] = np.abs(df_ind.rf_wf / 100. - df_ind.rf_mf / total)
        df_plus["error"] = np.abs(df_plus.rf_wf / 100. - df_plus.rf_mf / total)
        return df_ind.error.sum() + df_plus.error.sum()
    else:
        rf_mw = df_wc.fl_mw * nl + df_wc.fg_mw * nv
        df_plus["rf_mf"] = (rf_mw * df_plus.rf_wf / 100.) / df_plus.rf_mw
        df_plus["error"] = df_plus.apply(lambda row: np.abs(row.rf_mf - (1. - df_ind.loc[
            df_ind.index <= df_ind.loc[df_ind.short_name == row.short_name - 1].index.max(), "rf_mf"].sum())),
                                         axis=1)
        return df_plus.error.sum()


def wf_to_mf(df_ind, df_plus, df_wc):
    if (
            not df_ind.rf_wf.isnull().values.any() and not df_wc.rf_mw.isnull().values.any()
            and not df_ind.mw.isnull().values.any()) and (
            not df_plus.rf_wf.isnull().values.any() and not df_plus.rf_mw.isnull().values.any()):
        logger.info("Executing WF to MF Method 1 to determine mf.")
        df_ind["rf_mf"] = (df_ind.rf_wf * df_wc.rf_mw.values[0] / df_ind.mw) / 100.
        df_plus["rf_mf"] = (df_plus.rf_wf * df_wc.rf_mw.values[0] / df_plus.rf_mw) / 100.
    elif not df_wc.nl.isnull().values.any() and not df_wc.nv.isnull().values.any():
        logger.info("Executing WF to MF Method 2 to determine mf.")
        df_ind, df_plus = nl_nv(df_ind, df_plus, df_wc, df_wc.nl.values[0], df_wc.nv.values[0])
    elif not df_wc.fl_mw.isnull().values.any() and not df_wc.fg_mw.isnull().values.any() \
            and not df_wc.rf_mw.isnull().values.any():
        logger.info("Executing WF to MF Method 3 to determine mf.")
        nl = ((df_wc.rf_mw - df_wc.fg_mw) / (df_wc.fl_mw - df_wc.fg_mw)).values[0]
        nv = 1. - nl
        df_ind, df_plus = nl_nv(df_ind, df_plus, df_wc, nl, nv)
    else:
        logger.info("Executing WF to MF Method 4 to determine mf.")
        res = optimize.shgo(cost_function, bounds=[[0.001, 1.]], args=(df_ind, df_plus, df_wc), iters=5)
        if not res.success:
            msg = 'Error:{"Type": "Validation", "Message": "Failed to find optimal NL and NV values. ' \
                  'Please provide nl and nv values or Monophasic fluid mf."}'
            raise Exception(msg)
        nl = res.x[0]
        nv = 1. - nl
        df_ind, df_plus = nl_nv(df_ind, df_plus, df_wc, nl, nv)
    return df_ind, df_plus


if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection
    from read_user_input import individuals, plus, whole

    db = get_connection("ALUMM")
    test_df_ind, test_df_plus, test_df_wc = individuals(db), plus(db), whole(db)
    test_df_ind, test_df_plus = wf_to_mf(test_df_ind, test_df_plus, test_df_wc)
    print test_df_ind
    print test_df_plus
