import numpy as np
from scipy import optimize

from Scripts.pvt_new.core.get_logger import get_logger

logger = get_logger("pvt validate fix sg")

mf_columns = ["rf_mf", "fl_mf", "fg_mf"]
sg_columns = ["rf_sg", "fl_sg", "fg_sg"]
mw_columns = ["rf_mw", "fl_mw", "fg_mw"]


def get_last_plus(df_plus, column):
    return df_plus.loc[df_plus.short_name == df_plus.short_name.max(), column].values[0]


def plus_sg(df_ind, df_plus, mf_column, sg_column, mw_column):
    df_ind["zimi"] = df_ind[mf_column] * df_ind.mw
    df_plus["zimi"] = df_plus[mf_column] * df_plus[mw_column]
    df_plus[sg_column] = df_plus.apply(lambda row: (df_ind.loc[
                                                        df_ind.short_name >= row.short_name, "zimi"].sum() +
                                                    get_last_plus(df_plus, "zimi")) /
                                                   ((df_ind.loc[df_ind.short_name >= row.short_name, "zimi"] /
                                                     df_ind.loc[df_ind.short_name >= row.short_name, "sg"]).sum() +
                                                    get_last_plus(df_plus, "zimi") /
                                                    get_last_plus(df_plus, sg_column)), axis=1)
    df_plus[sg_column].fillna(0, inplace=True)
    return df_plus


def total_sg(df_ind, df_plus, mf_column, sg_column, mw_column):
    df_ind["zimi"] = df_ind[mf_column] * df_ind.mw
    df_plus["zimi"] = df_plus[mf_column] * df_plus[mw_column]
    return (df_ind.zimi.sum() + get_last_plus(df_plus, "zimi")) / \
           ((df_ind.zimi / df_ind.sg).sum() + get_last_plus(df_plus, "zimi") /
            get_last_plus(df_plus, sg_column))


def calc_sg(ivars, df_ind, df_plus, mf_column, sg_column, mw_column):
    df_ind.loc[df_ind.short_name >= 6, "sg"] = df_ind.loc[df_ind.short_name >= 6, "sg"] * ivars[0]
    df_plus[sg_column] = df_plus[sg_column] * ivars[0]
    df_plus = plus_sg(df_ind, df_plus, mf_column, sg_column, mw_column)
    return df_ind, df_plus


def is_one(df_ind, df_plus, df_wc, mf_column, sg_column, mw_column):
    sg = total_sg(df_ind, df_plus, mf_column, sg_column, mw_column)
    logger.info("Given SG = %s, Calculated SG = %s. Error = %s" % (
        df_wc[sg_column].values[0], sg, np.abs(100. * (df_wc[sg_column] - sg) / df_wc[sg_column]).values[0]))
    if np.abs(100. * (df_wc[sg_column] - sg) / df_wc[sg_column]).values[0] <= 25.:
        return True
    return False


def wc_sg(ivars, df_ind_o, df_plus_o, mf_column, sg_column, mw_column):
    df_ind, df_plus = df_ind_o.copy(deep=True), df_plus_o.copy(deep=True)
    df_ind, df_plus = calc_sg(ivars, df_ind, df_plus, mf_column, sg_column, mw_column)
    sg = total_sg(df_ind, df_plus, mf_column, sg_column, mw_column)
    return sg


def cost_function(ivars, df_ind_o, df_plus_o, df_wc, mf_column, sg_column, mw_column):
    sg = wc_sg(ivars, df_ind_o, df_plus_o, mf_column, sg_column, mw_column)
    return np.abs(np.abs(100. * (df_wc[sg_column] - sg) / df_wc[sg_column])).values[0]


def fix_sg(df_ind, df_plus, df_wc):
    for i in range(len(mf_columns)):
        mf_column = mf_columns[i]
        sg_column = sg_columns[i]
        mw_column = mw_columns[i]
        if not df_ind[mf_column].isnull().values.any() and not df_plus[mf_column].isnull().values.any() and not \
                df_plus[sg_column].isnull().values.any() and not df_plus[mw_column].isnull().values.any() and not \
                df_wc[
                    sg_column].isnull().values.any():
            if not is_one(df_ind, df_plus, df_wc, mf_column, sg_column, mw_column):
                logger.info(
                    "Individual, plus fractions and whole crude specific gravity do not "
                    "tally (%s, %s, %s). Trying to fix." % (
                        mf_column, sg_column, mw_column))
                res = optimize.shgo(cost_function, bounds=[[0.97, 1.1]],
                                    args=(df_ind, df_plus, df_wc, mf_column, sg_column, mw_column), iters=5)
                if not res.success:
                    lb = wc_sg([0.97], df_ind, df_plus, mf_column, sg_column, mw_column)
                    ub = wc_sg([1.1], df_ind, df_plus, mf_column, sg_column, mw_column)
                    msg = 'Error:{"Type": "Validation", "Message": "Individual, plus fractions and whole ' \
                          'crude specific gravity do not tally. Please check and fix input data (%s, %s, %s). ' \
                          'whole crude SG should be between %s and %s."}' \
                          % (mf_column, sg_column, mw_column, lb, ub)
                    raise Exception(msg)
                df_ind, df_plus = calc_sg(res.x, df_ind, df_plus, mf_column, sg_column, mw_column)
                if not is_one(df_ind, df_plus, df_wc, mf_column, sg_column, mw_column):
                    lb = wc_sg([0.97], df_ind, df_plus, mf_column, sg_column, mw_column)
                    ub = wc_sg([1.1], df_ind, df_plus, mf_column, sg_column, mw_column)
                    msg = 'Error:{"Type": "Validation", "Message": "Individual, plus fractions and ' \
                          'whole crude specific gravity do not tally. Please check and fix input ' \
                          'data (%s, %s, %s). whole crude SG should be between %s and %s."}' \
                          % (mf_column, sg_column, mw_column, lb, ub)
                    raise Exception(msg)
                logger.info(
                    "Individual, plus fractions and whole crude specific gravity fixed (%s, %s, %s)." % (
                        mf_column, sg_column, mw_column))
            else:
                df_ind, df_plus = calc_sg([1.], df_ind, df_plus, mf_column, sg_column, mw_column)
            break
    return df_ind, df_plus


if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection
    from read_user_input import individuals, plus, whole

    db = get_connection("ALUMM")
    test_df_ind, test_df_plus, test_df_wc = individuals(db), plus(db), whole(db)
    test_df_ind, test_df_plus = fix_sg(test_df_ind, test_df_plus, test_df_wc)
    print test_df_ind
    print test_df_plus
