import numpy as np
import pandas as pd

from Scripts.pvt_new.core.get_logger import get_logger
from Scripts.pvt_new.core.storage.mongo import fetch
from Scripts.pvt_new.core.storage.save import save

logger = get_logger("pvt other validation")


def min_data(well_conn):
    logger.info("checking reservoir temperature and fluid type are provided.")
    msg = 'Error:{"Type": "Validation", "Message": "Min data check failed: ' \
          'Reservoir Temperature and Fluid Type should be provided in General Data."}'
    gd = fetch(well_conn, property="general_data")
    try:
        res_temp = gd.loc[gd.param == "temp_res", "value"].values[0]
        fluid_type = gd.loc[gd.param == "fluid_type", "value"].values[0]

        if not (res_temp or fluid_type):
            raise Exception(msg)
    except Exception as e:
        raise Exception(msg)
    return res_temp, fluid_type


def validate_res_temp(gd_res_temp, well_conn):
    logger.info("checking reservoir temperature consistency")
    separator = fetch(well_conn, property="seperator_used_sep_corrected")
    sep = separator.loc[separator.stage == "Total at Bubble Point"]
    if sep.temperature.count() > 0:
        sep_res_temp = separator.loc[separator.stage == "Total at Bubble Point", "temperature"].values[0]
        error = ((gd_res_temp - sep_res_temp) / gd_res_temp) * 100.
        if abs(error) > 0.5:
            msg = 'Error:{"Type": "Validation", "Message": "reservoir temperature in general data and temperature in ' \
                  'separator corrected data at stage total at bubble point does not tally. Tolerance is +/-0.5%")'
            raise Exception(msg)
    return True


def validate_api_gravity(user_args):
    well_conn = user_args.well_conn
    logger.info("checking api gravity consistency")
    gd = fetch(well_conn, property="general_data")
    separator = fetch(well_conn, property="seperator_used_sep_corrected")
    gd_api_gravity = gd.loc[gd.param == "api_gravity", "value"].values[0]
    sep_api_gravity = separator.loc[separator.stage == "Stock Tank Condition"]
    if gd_api_gravity is not None and gd_api_gravity != 0 and sep_api_gravity.shape[0] > 0:
        sep_api_gravity = (141.5 / (sep_api_gravity.liq_density.values[0] / 0.999016)) - 131.5
        logger.info("api_gravity in general data == %s. sep_api_gravity == %s" % (gd_api_gravity, sep_api_gravity))
        if gd_api_gravity != sep_api_gravity:
            logger.info("replacing api gravity in general data")
            gd.loc[gd.param == "api_gravity", "value"] = sep_api_gravity
    save(gd, user_args, identifier="validated_general_data")


def validate_gas_gravity(user_args):
    well_conn = user_args.well_conn
    logger.info("checking gas gravity consistency")
    gd = fetch(well_conn, property="general_data")
    separator = fetch(well_conn, property="seperator_used_sep_corrected")
    gd_gas_gravity = gd.loc[gd.param == "gas_sp_gravity", "value"].values[0]
    sep_gas_gravity = separator.loc[separator.stage.str.startswith("Stage")]
    if gd_gas_gravity is not None and gd_gas_gravity != 0 and sep_gas_gravity.shape[
        0] > 0 and not sep_gas_gravity.gas_gravity.isnull().any() and not sep_gas_gravity.gor.isnull().any():
        sep_gas_gravity = (sep_gas_gravity.gor * sep_gas_gravity.gas_gravity).sum() / sep_gas_gravity.gor.sum()
        logger.info("gas_gravity in general data == %s. sep_gas_gravity == %s" % (gd_gas_gravity, sep_gas_gravity))
        if abs((gd_gas_gravity - sep_gas_gravity) / gd_gas_gravity) >= 0.2:
            logger.info("replacing gas gravity in general data")
            gd.loc[gd.param == "gas_sp_gravity", "value"] = sep_gas_gravity
    save(gd, user_args, identifier="validated_general_data")


def validate_gor_pb(user_args):
    well_conn = user_args.well_conn
    logger.info("checking gor pb consistency")
    gd = fetch(well_conn, property="general_data")
    separator = fetch(well_conn, property="seperator_used_sep_corrected")
    gd_gor_pb = gd.loc[gd.param == "gor_pb", "value"].values[0]
    sep_gor_pb = separator.loc[separator.stage.str.startswith("Stage")]
    if gd_gor_pb is not None and gd_gor_pb != 0 and sep_gor_pb.shape[
        0] > 0 and not sep_gor_pb.gor.isnull().any():
        sep_gor_pb = sep_gor_pb.gor.sum()
        logger.info("gor_pb in general data == %s. sep_gor_pb == %s" % (gd_gor_pb, sep_gor_pb))
        if abs((gd_gor_pb - sep_gor_pb) / gd_gor_pb) >= 0.2:
            logger.info("replacing gor pb in general data")
            gd.loc[gd.param == "gor_pb", "value"] = sep_gor_pb
    save(gd, user_args, identifier="validated_general_data")


def validate_separator(user_args):
    well_conn = user_args.well_conn
    logger.info("checking separator corrected consistency")
    dv_oil = fetch(well_conn, property="dv_oil")
    separator = fetch(well_conn, property="seperator_used_sep_corrected")
    separator_corrected = fetch(well_conn, property="separator_corrected")
    dv_oil.sort_values(by=["temperature", "pressure"], ascending=False, inplace=True)
    dv_oil.reset_index(inplace=True)
    separator_corrected.sort_values(by=["temperature", "pressure"], ascending=False, inplace=True)
    separator_corrected.reset_index(inplace=True)
    try:
        bo_fvf = separator.loc[separator.stage == "Total at Bubble Point", "bo_fvf"].values[0]
        gor = separator.loc[separator.stage == "Total at Bubble Point", "gor"].values[0]
    except:
        logger.info("missing total at bubble point in separator data. skipping validation.")
        save(separator_corrected, user_args, identifier="validated_separator_corrected")
        return 0
    if (bo_fvf is None or bo_fvf == 0) or (gor is None or gor == 0) or dv_oil.shape[0]== 0:
        logger.info("Missing bo_fvf_pb and/or gor_pb in separator data. Skipping validation.")
        save(separator_corrected, user_args, identifier="validated_separator_corrected")
    else:
        max_pressure = dv_oil.loc[~dv_oil.gor.isnull()].pressure.max()
        fvf_bod = dv_oil.loc[dv_oil.pressure == max_pressure, "bo_fvf"].values[0]
        gor_pb_dv = dv_oil.loc[dv_oil.pressure == max_pressure, "gor"].values[0]
        dv_oil["bo_fvf"] = (bo_fvf * (dv_oil.bo_fvf / fvf_bod))
        dv_oil.loc[dv_oil.bo_fvf < 1, "bo_fvf"] = 1.
        dv_oil["gor"] = (gor - (gor_pb_dv - dv_oil.gor) * (bo_fvf / fvf_bod))
        dv_oil.loc[dv_oil.gor <= 0, "gor"] = 0
        if separator_corrected.shape[0] != dv_oil.shape[0]:
            save(dv_oil[["pressure", "temperature", "gor", "liq_density", "bo_fvf"]],
                 user_args, identifier="validated_separator_corrected")
        else:
            dv_oil["gor_error"] = (np.abs((dv_oil.gor - separator_corrected.gor)/separator_corrected.gor))
            dv_oil["bo_fvf_error"] = (np.abs((dv_oil.bo_fvf - separator_corrected.bo_fvf)/separator_corrected.bo_fvf))
            error = (dv_oil[["gor_error", "bo_fvf_error"]].sum()*100.).sum()
            if error > 5.:
                logger.info("Total error==%s is greater than threshold 5, replacing separator corrected data" % error)
                save(dv_oil[["pressure", "temperature", "gor", "liq_density", "bo_fvf"]],
                     user_args, identifier="validated_separator_corrected")
            else:
                save(separator_corrected, user_args, identifier="validated_separator_corrected")


def other_data_validation(user_args):
    res_temp, fluid_type = min_data(user_args.well_conn)
    validate_res_temp(res_temp, user_args.well_conn)
    validate_api_gravity(user_args)
    validate_gas_gravity(user_args)
    validate_gor_pb(user_args)
    validate_separator(user_args)


if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection

    db = get_connection("ALUMM")
    test_user_args = pd.Series({"debug": False, "well_conn": db})
    other_data_validation(test_user_args)
