import numpy as np
from scipy import optimize

from Scripts.pvt_new.core.get_logger import get_logger

logger = get_logger("pvt validate fix mf")

mf_columns = ["rf_mf", "fl_mf", "fg_mf"]


def getLastPlus(df_plus, column):
    return df_plus.loc[df_plus.short_name == df_plus.short_name.max(), column].values[0]


def plusMF(df_ind, df_plus, mf_column):
    df_plus[mf_column] = df_plus.apply(
        lambda row: df_ind.loc[df_ind.short_name >= row.short_name, mf_column].sum() + getLastPlus(df_plus,
                                                                                                   mf_column),
        axis=1)
    return df_plus


def totalMF(df_ind, df_plus, mf_column):
    return df_ind[mf_column].sum() + getLastPlus(df_plus, mf_column)


def mf(i_vars, df_ind, df_plus, mf_column):
    df_ind[mf_column] = df_ind[mf_column] * i_vars[0]
    df_plus[mf_column] = df_plus[mf_column] * i_vars[0]
    df_plus = plusMF(df_ind, df_plus, mf_column)
    return df_ind, df_plus


def cost_function(i_vars, df_ind_o, df_plus_o, mf_column):
    df_ind, df_plus = df_ind_o.copy(deep=True), df_plus_o.copy(deep=True)
    df_ind, df_plus = mf(i_vars, df_ind, df_plus, mf_column)
    total = totalMF(df_ind, df_plus, mf_column)
    return np.abs(1. - total)


def is_one(df_ind, df_plus, mf_column):
    total = totalMF(df_ind, df_plus, mf_column)
    if 0.9999 <= total <= 1.0001:
        return True
    return False


def fix_mf(df_ind, df_plus):
    for i in range(len(mf_columns)):
        mf_column = mf_columns[i]
        if not df_ind[mf_column].isnull().values.any() and not df_plus[mf_column].isnull().values.any():
            if not is_one(df_ind, df_plus, mf_column):
                logger.info("individual Mole Fractions sum is not 1 (%s). Trying to fix." % mf_column)
                res = optimize.shgo(cost_function, bounds=[[0.8, 1.2]], args=(df_ind, df_plus, mf_column),
                                    iters=5)
                if not res.success:
                    msg = 'Error:{"Type": "Validation", "Message": "individual and plus Mole Fractions sum ' \
                          'should be 1. Please check and fix input data (%s) before proceeding further."}' % (
                              mf_column)
                    raise Exception(msg)
                df_ind, df_plus = mf(res.x, df_ind, df_plus, mf_column)
                if not is_one(df_ind, df_plus, mf_column):
                    msg = 'Error:{"Type": "Validation", "Message": "individual and plus Mole Fractions sum ' \
                          'should be 1. Please check and fix input data (%s) before proceeding further."}' % (
                              mf_column)
                    raise Exception(msg)
                logger.info("individual Mole Fractions sum fixed (%s)." % mf_column)
            else:
                df_ind, df_plus = mf([1.], df_ind, df_plus, mf_column)
    return df_ind, df_plus


if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection
    from read_user_input import individuals, plus, whole

    db = get_connection("ALUMM")
    test_df_ind, test_df_plus, test_df_wc = individuals(db), plus(db), whole(db)
    test_df_ind, test_df_plus = fix_mf(test_df_ind, test_df_plus)
    print test_df_ind
    print test_df_plus
