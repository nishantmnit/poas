import numpy as np
from scipy import optimize

from Scripts.pvt_new.core.get_logger import get_logger

logger = get_logger("pvt validate fix mass")

mf_columns = ["rf_mf", "fl_mf", "fg_mf"]
mw_columns = ["rf_mw", "fl_mw", "fg_mw"]


def get_last_plus(df_plus, column):
    return df_plus.loc[df_plus.short_name == df_plus.short_name.max(), column].values[0]


def plus_mw(df_ind, df_plus, mf_column, mw_column):
    df_plus[mw_column] = df_plus.apply(lambda row: ((df_ind.loc[df_ind.short_name >= row.short_name, mf_column] *
                                                     df_ind.loc[df_ind.short_name >= row.short_name, "mw"]).sum()
                                                    + get_last_plus(df_plus, mw_column) *
                                                    get_last_plus(df_plus, mf_column)) /
                                                   (df_ind.loc[df_ind.short_name >= row.short_name,
                                                               mf_column].sum() +
                                                    get_last_plus(df_plus, mf_column)), axis=1)
    df_plus[mw_column].fillna(0, inplace=True)
    return df_plus


def total_mw(df_ind, df_plus, mf_column, mw_column):
    return (df_ind[mf_column] * df_ind.mw).sum() + get_last_plus(df_plus, mw_column) * get_last_plus(df_plus,
                                                                                                     mf_column) \
           / (df_ind[mf_column].sum() + get_last_plus(df_plus, mf_column))


def calc_mw(ivars, df_ind, df_plus, mf_column, mw_column):
    df_ind.loc[df_ind.short_name >= 6, "mw"] = df_ind.loc[df_ind.short_name >= 6, "mw"] * ivars[0]
    df_plus[mw_column] = df_plus[mw_column] * ivars[0]
    df_plus = plus_mw(df_ind, df_plus, mf_column, mw_column)
    return df_ind, df_plus


def is_one(df_ind, df_plus, df_wc, mf_column, mw_column):
    mw = total_mw(df_ind, df_plus, mf_column, mw_column)
    logger.info("Given MW = %s, Calculated MW = %s. Error = %s" % (
        df_wc[mw_column].values[0], mw, np.abs(100. * (df_wc[mw_column] - mw) / df_wc[mw_column]).values[0]))
    if np.abs(100. * (df_wc[mw_column] - mw) / df_wc[mw_column]).values[0] <= 5.:
        return True
    return False


def wc_mw(ivars, df_ind_o, df_plus_o, mf_column, mw_column):
    df_ind, df_plus = df_ind_o.copy(deep=True), df_plus_o.copy(deep=True)
    df_ind, df_plus = calc_mw(ivars, df_ind, df_plus, mf_column, mw_column)
    mw = total_mw(df_ind, df_plus, mf_column, mw_column)
    return mw


def cost_function(ivars, df_ind_o, df_plus_o, df_wc, mf_column, mw_column):
    mw = wc_mw(ivars, df_ind_o, df_plus_o, mf_column, mw_column)
    return np.abs(np.abs(100. * (df_wc[mw_column] - mw) / df_wc[mw_column])).values[0]


def fix_mass(df_ind, df_plus, df_wc):
    for i in range(len(mf_columns)):
        mf_column = mf_columns[i]
        mw_column = mw_columns[i]
        if not df_ind[mf_column].isnull().values.any() and not df_plus[mf_column].isnull().values.any() and not \
                df_plus[mw_column].isnull().values.any() and not df_wc[mw_column].isnull().values.any():
            if not is_one(df_ind, df_plus, df_wc, mf_column, mw_column):
                logger.info(
                    "Individual, plus fractions and whole crude molecular weights do not tally "
                    "(%s, %s). Trying to fix." % (
                        mf_column, mw_column))
                res = optimize.shgo(cost_function, bounds=[[0.97, 1.5]],
                                    args=(df_ind, df_plus, df_wc, mf_column, mw_column), iters=5)
                if not res.success:
                    lb = wc_mw([0.97], df_ind, df_plus, mf_column, mw_column)
                    ub = wc_mw([1.2], df_ind, df_plus, mf_column, mw_column)
                    logger.info("Fixing the masses failed.")
                    msg = 'Error:{"Type": "Validation", "Message": "Individual, plus fractions and whole ' \
                          'crude molecular weights do not tally. Please check and fix input data (%s, %s) ' \
                          'before proceeding further. Whole Crude mass should be between %s and %s."}' \
                          % (mf_column, mw_column, lb, ub)
                    raise Exception(msg)
                df_ind, df_plus = calc_mw(res.x, df_ind, df_plus, mf_column, mw_column)
                if not is_one(df_ind, df_plus, df_wc, mf_column, mw_column):
                    logger.info("Solver converged, still the masses are not tallying.")
                    lb = wc_mw([0.97], df_ind, df_plus, mf_column, mw_column)
                    ub = wc_mw([1.2], df_ind, df_plus, mf_column, mw_column)
                    msg = 'Error:{"Type": "Validation", "Message": "Individual, plus fractions and ' \
                          'whole crude molecular weights do not tally. Please check and fix input data (%s, %s)' \
                          ' before proceeding further. Whole Crude mass should be between %s and %s."}' \
                          % (mf_column, mw_column, lb, ub)
                    raise Exception(msg)
                logger.info("Individual, plus fractions and whole crude molecular weights fixed (%s, %s)." % (
                    mf_column, mw_column))
            else:
                df_ind, df_plus = calc_mw([1.], df_ind, df_plus, mf_column, mw_column)
            break
    return df_ind, df_plus


if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection
    from read_user_input import individuals, plus, whole

    db = get_connection("ALUMM")
    test_df_ind, test_df_plus, test_df_wc = individuals(db), plus(db), whole(db)
    test_df_ind, test_df_plus = fix_mass(test_df_ind, test_df_plus, test_df_wc)
    print test_df_ind
    print test_df_plus
