import re

import numpy as np

from Scripts.pvt_new.core.get_logger import get_logger
from Scripts.pvt_new.core.storage.mongo import fetch

logger = get_logger("pvt validate read user input")


def individuals(well_conn):
    df = fetch(well_conn, property="pvt_individual_input")
    df.fillna(value=np.nan, inplace=True)
    df["short_name"] = df.scn.str.extract('(^c\d+)', expand=True, flags=re.IGNORECASE)
    df["short_name"] = df.short_name.str.extract('(\d+)', expand=True, flags=re.IGNORECASE)
    df["short_name"] = df.short_name.astype(float)
    df["short_name"] = df["short_name"].replace(np.nan, 0)
    df = df.sort_values(by=["seq"])
    df.reset_index(inplace=True, drop=True)
    for column in ["fl_mf", "fg_mf", "rf_mf"]:
        df[column] = df[column] / 100.
    return df


def plus(well_conn):
    df = fetch(well_conn, property="pvt_plus_input")
    df.fillna(value=np.nan, inplace=True)
    df["scn"] = df.scn.str.extract('(^c\d+)', expand=True, flags=re.IGNORECASE)
    df["short_name"] = df.scn.str.extract('(^c\d+)', expand=True, flags=re.IGNORECASE)
    df["short_name"] = df.short_name.str.extract('(\d+)', expand=True, flags=re.IGNORECASE)
    df["short_name"] = df.short_name.astype(float)
    df = df.sort_values(by=["short_name"])
    df.reset_index(inplace=True, drop=True)
    for column in ["fl_mf", "fg_mf", "rf_mf"]:
        df[column] = df[column] / 100.
    return df


def whole(well_conn):
    df = fetch(well_conn, property="pvt_wc_input")
    df.fillna(value=np.nan, inplace=True)
    return df


if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection

    db = get_connection("AB8")
    print individuals(db)
    print plus(db)
    print whole(db)
