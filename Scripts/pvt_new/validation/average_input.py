import numpy as np
import pandas as pd


def average(df_ind, start_scn=6):
    df = df_ind.loc[df_ind.index < start_scn]
    short_name, mf, mass, sg, bp, ri, user_mf = list(), list(), list(), list(), list(), list(), list()
    for index, row in df_ind.loc[df_ind.index >= start_scn].iterrows():
        if index not in short_name:
            short_name.append(index)
            mf.append(average_mf(df_ind, index))
            if "user_mf" in df_ind.columns:
                user_mf.append(average_user_mf(df_ind, index))
            mass.append(average_mw(df_ind, index))
            sg.append(average_sg(df_ind, index))
            bp.append(average_bp(df_ind, index))
            ri.append(average_ri(df_ind, index))
    scn = ["C%s" % x for x in short_name]
    if "user_mf" in df_ind.columns:
        temp_df = pd.DataFrame(data=list(zip(mf, mass, sg, bp, ri, scn, user_mf)),
                               columns=["mf", "expmass", "expdensity", "expbp", "expri", "scn", "user_mf"],
                               index=short_name)
    else:
        temp_df = pd.DataFrame(data=list(zip(mf, mass, sg, bp, ri, scn)),
                               columns=["mf", "expmass", "expdensity", "expbp", "expri", "scn"], index=short_name)
    df = pd.concat([df, temp_df], axis=0, sort=True)
    return df


def average_mf(df, short_name):
    return df.loc[df.index == short_name, "mf"].sum()


def average_user_mf(df, short_name):
    return df.loc[df.index == short_name, "user_mf"].sum()


def average_mw(df_o, short_name):
    df = df_o
    if df.loc[df.index == short_name, "expmass"].isnull().values.any():
        return np.nan
    if df.loc[df.index == short_name].shape[0] == 1:
        return df.loc[df.index == short_name, "expmass"].values[0]
    df["zimi"] = df.mf * df.expmass
    return df.loc[df.index == short_name, "zimi"].sum() / df.loc[df.index == short_name, "mf"].sum()


def average_sg(df_o, short_name):
    df = df_o
    if df.loc[df.index == short_name, "expdensity"].isnull().values.any():
        return np.nan
    if df.loc[df.index == short_name].shape[0] == 1:
        return df.loc[df.index == short_name, "expdensity"].values[0]
    df["zimi"] = df.mf * df.expmass
    df["zimibysg"] = df.zimi / df.expdensity
    return df.loc[df.index == short_name, "zimi"].sum() / df.loc[df.index == short_name, "zimibysg"].sum()


def average_bp(df_o, short_name):
    df = df_o
    if df.loc[df.index == short_name, "expbp"].isnull().values.any():
        return np.nan
    if df.loc[df.index == short_name].shape[0] == 1:
        return df.loc[df.index == short_name, "expbp"].values[0]
    df["zimi"] = df.mf * df.expmass
    df.loc[df.index == short_name, "xwi"] = df.loc[df.index == short_name, "zimi"] / df.loc[
        df.index == short_name, "zimi"].sum()
    df["xwibysg"] = df.xwi / df.expdensity
    df.loc[df.index == short_name, "xvi"] = df.loc[df.index == short_name, "xwibysg"] / df.loc[
        df.index == short_name, "xwibysg"].sum()
    df["xvibytb"] = df.xvi * df.expbp
    return df.loc[df.index == short_name, "xvibytb"].sum()


def average_ri(df_o, short_name):
    df = df_o
    if df.loc[df.index == short_name, "expri"].isnull().values.any():
        return np.nan
    if df.loc[df.index == short_name].shape[0] == 1:
        return df.loc[df.index == short_name, "expri"].values[0]
    df["zimi"] = df.mf * df.expmass
    df.loc[df.index == short_name, "xwi"] = df.loc[df.index == short_name, "zimi"] / df.loc[
        df.index == short_name, "zimi"].sum()
    df["xwibysg"] = df.xwi / df.expdensity
    df.loc[df.index == short_name, "xvi"] = df.loc[df.index == short_name, "xwibysg"] / df.loc[
        df.index == short_name, "xwibysg"].sum()
    df["xvibyri"] = df.xvi * df.expri
    return df.loc[df.index == short_name, "xvibyri"].sum()


if __name__ == "__main__":
    msg = "This module cannot be run standalone."
    raise Exception(msg)
