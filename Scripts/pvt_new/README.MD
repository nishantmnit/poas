###### Summary:

This is compositional PVT module. Code so far covers followings:

1. Mass
2. Density
3. Boiling point
4. Refractivity
5. Component modeling
6. EOS at BP
7. EOS at tr=0.7
8. Solubility
9. VLE

Followings are known but are yet to be coded

1. KIJ
2. PT Matching

###### How to set-up/run test?

1. set-up your python env using python virtual env or anaconda. If you have root access and permission to install python packages, you can skip this step.
2. Install modules given in requirements.txt.
3. Fire-up and start-up your MongoDB.
4. Go to playground/nishant/pvt_test_data folder and locate createTestData.py.
5. Apart from python script, there are total 69 excels in TestData folder. Each excel name corresponds to a test well. Pick anyone excel of your choice to start the test.
6. Load the test data into mongo using following command
`python create_test_data.py --excel <excel file of your choice> --d2s`
7. This will create a new collection with the excel name in MBP database in mongo.
8. You can now proceed with test execution as following.
`python main.py --validate --mass --density --bp --ri --cm --eosbp --tr --sol`

###### How to run an end to end test?
1. Load all test cases into mongo using command given in step 6 above.
2. go to pvt/test folder and locate main.py
3. run following command
`nohup python main.py --validate --mass --density --bp --ri --cm --ptmd --eosbp --tr --solubility --debug --clear> main.out &`

###### How to run VLE?
1. go to pvt/vle
2. run test.py as following
`nohup python test.py > vle_test.out &`
3. This will generate vle output data for all non-hydrocarbons for all EOS.

##### How to set-up API data?
1. go to playground/nishant/api_data
2. execute api_data.py script to load api data in mongo

##### Outputs - property = "pvt_output"
1. validation - 
   1. plus_or_individual="individual", identifier="validated_data"
   2. plus_or_individual="plus", identifier="validated_data"
   3. plus_or_individual="individual", identifier="validated_averaged_data"
   4. plus_or_individual="plus", identifier="validated_averaged_data"
2. mass -
   1. identifier="mass", plus_or_individual={individual/plus}, distribution={general/gamma/beta}, solver=int
3. density -
   1. identifier="density", plus_or_individual={individual/plus}, distribution={general/gamma/beta/poas/poas_c}, solver=1
4. bp -
   1. identifier="bp", plus_or_individual={individual/plus}, distribution={general/gamma/beta/poas}, solver=1
5. ri -
   1. identifier="ri", plus_or_individual={individual/plus}, distribution={general/gamma/beta/poas}, solver=1
6. cm -
   1. identifier="sno"
   2. identifier="pcm"
   3. identifier="fcm"
7. PT Data Matching -
   1. eosbp
      1. identified = "eosbp"
      2. eos = "poas_4"
   2. tr
      1. identifier = "tr"
      2. eos = "poas_4"
   3. solubility
      1. identifier = "solubility"
      2. eos = "poas_4"
      3. temperature = <any temperature from core/constants/solubility_temperatures>
8. tr
   1. identifier = "tr"
   2. eos = <any eos from core/constants/valid_eos>
9. solubility
   1. identifier = "solubility"
   2. eos = <any eos from core/constants/valid_eos>
   3. temperature = <any temperature from core/constants/solubility_temperatures>


### PVT FLow
1. user enters individual, plus and whole crude data from UI.
2. User can choose any units from UI but UI will save data in uniform units (as expected by backend). 
3. UI can save user entered unit system somewhere for later reference. However, unit is no where referred in backend code.
4. User can now run PVT in 2 modes as detailed below
   1. POAS Recommended mode - In this mode, everything can be executed at once. that means, UI can call backend program with all flags at once as following.
   `python main.py --well <XYZ> --validate --mass --density --bp --ri --cm --ptmd --eosbp --tr --solubility --kij --eos=poas_4`
   2. Please note - eos="poas_4" should be passed if user have chosen to execute "poas recommended mode".
   3. In manual mode - user will run one module at a time, review its result and adjust the recommendations for mass, density, bp and ri before proceeding with next module.
   4. eosbp, tr, solubility, kij etc. require user to choose an EOS before executing these modules. List of valid EOS are available in core/constants/valid_eos.
5. execution flow 
   1. validate
   2. mass
   3. density
   4. bp
   5. ri
   6. cm
   7. ptmd
   8. eosbp
   9. tr
   10. solubility
   11. kij

### Recommendation engine
mass, density, bp and ri have multiple outputs. Only one, in each module, is marked as recommended=1. 
On UI, we need to show all results, with one marked as recommended in each module. 
User will have the option to change the recommended. When user changes the recommendation, do followings from UI.

1. Mark the default/previous recommendation as 0. i.e. set recommended = 0.
2. Mark the user selection as 1. i.e. recommended = 1