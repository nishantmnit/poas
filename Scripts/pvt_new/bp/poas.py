import numpy as np

from Scripts.pvt_new.core.get_logger import get_logger
from Scripts.pvt_new.core.storage.save import save
from bp_input import bp_input

logger = get_logger("pvt poas bp")


def get_intercept(df, bracket):
    xi = df.loc[df.index.isin(bracket), "xi"].sum()
    yi = df.loc[df.index.isin(bracket), "yi"].sum()
    xiyi = (df.loc[df.index.isin(bracket), "xi"] * df.loc[df.index.isin(bracket), "yi"]).sum()
    xi2 = (df.loc[df.index.isin(bracket), "xi"] ** 2).sum()
    intercept = (xi * xiyi - yi * xi2) / (xi ** 2 - len(bracket) * xi2)
    slope = (yi - len(bracket) * intercept) / xi
    return intercept, slope


def process_result(df, user_args):
    df["bp"] = df.poas_bp
    save(df, user_args, identifier="bp", plus_or_individual="individual", solver=1, distribution="poas")
    return True


def poas(user_args):
    logger.info("Executing POAS Bp Finder.")
    df = bp_input(user_args.well_conn)

    df["xi"] = df.apply(lambda row: 0 if row.mass == 0 or row.density == 0 else np.log(row.mass / row.density),
                        axis=1)
    df["yi"] = df.apply(lambda row: 0 if row.expbp == 0 or row.density == 0 else np.log(row.expbp / row.density),
                        axis=1)

    # divide into two brackets, Bracket 1 (SCN6-10) and Bracket 2 (SCN 10-21). In case the Input data terminates
    # before SCN 21, the 21 will be replaced by Last Terminating SCN-1
    ls = df.loc[~df.expbp.isnull()].index.max()
    intercepts, slopes = list(), list()
    for b in [6, 10], [10, min(21, ls)]:
        bracket = range(b[0], b[1] + 1)
        intercept, slope = get_intercept(df, bracket)
        intercepts.append(intercept)
        slopes.append(slope)
    df["poas_bp"] = df.apply(lambda row: row.expbp if row.name <= 6 else (
        (np.exp(slopes[0] * row.xi + intercepts[0]) * row.density) if row.name <= 10 else (
                np.exp(slopes[1] * row.xi + intercepts[1]) * row.density)), axis=1)
    return process_result(df, user_args)


"""
executed for all inputs
"""

if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection
    import pandas as pd
    from Scripts.pvt_new.settings import DEBUG_DIR
    import os

    well_name = "ALUMM"

    debug_dir = os.path.join(DEBUG_DIR, well_name)

    db = get_connection(well_name)
    test_user_args = pd.Series({"debug": True, "well_conn": db, "bias": "general", "debug_dir": debug_dir})
    poas(test_user_args)
