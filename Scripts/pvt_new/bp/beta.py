import numpy as np
from scipy import optimize
from scipy.stats import beta as betaInbuilt

from Scripts.pvt_new.core.get_logger import get_logger
from Scripts.pvt_new.core.storage.mongo import fetch
from Scripts.pvt_new.core.storage.save import save
from bp_input import bp_input

logger = get_logger("pvt beta bp")


def getPoasBp(df, user_args):
    df_p = fetch(user_args.well_conn, property="pvt_output", identifier="bp",
                 plus_or_individual="individual", distribution="poas", solver=1)
    df["poas_bp"] = df_p.bp
    return df


class BpSolver1:
    def __init__(self):
        self.beta = None
        self.alpha = None
        self.eta = None

    def optimize(self, user_args):
        logger.info("Executing Beta BP Solver 1")
        df = bp_input(user_args.well_conn)
        df = getPoasBp(df, user_args)
        sp, bounds = self.startPoint(df)
        res = optimize.minimize(self.cost_function, np.array(sp), bounds=bounds, method="SLSQP", args=df,
                                options={'maxiter': 500})
        if not res.success:
            print res
            logger.info("Beta BP Solver failed to find a feasible solution.")
            res.x = [1., 1., 1., 1.]
        logger.info("Completed Beta BP Solver 1")
        logger.info(
            "Beta BP solver final variables - %s, objective - %s" % (",".join([str(x) for x in res.x]), res.fun))
        return self.process_result(res, df, user_args)

    def startPoint(self, df_o):
        df = df_o.copy(deep=True)
        df["xvibp"] = df.xvi * df.poas_bp
        smean = df.xvibp.sum()
        df["variance"] = df.xvi * (smean - df.poas_bp) ** 2
        variance = df.variance.sum()
        df["skewness"] = df.xvi * ((smean - df.poas_bp) / np.sqrt(variance)) ** 3
        skewness = df.skewness.sum()

        eta = min(255., df.poas_bp.head(1).values[0] * 0.98)
        b_max = max(3000., 1.25 * df.poas_bp.tail(1).values[0])
        d1 = (b_max - eta) / (smean - eta)
        alpha = np.abs(
            -1. / d1 ** 2 * (2. * d1 + (2. - d1) * 2. * (b_max - eta) / (np.abs(skewness) * np.sqrt(variance))))
        beta_value = alpha * (b_max - smean) / (smean - eta)
        # mass boundary, alpha, beta, eta
        self.eta, self.alpha, self.beta = eta, alpha, beta_value
        bounds = [[0.98, 1.2], [0.2, 3.], [0.2, 3],
                  [0.5, (np.mean(df.poas_bp.head(2).values) * 0.98 * (1. - 0.01 / 100.)) / eta]]
        df_o["bb"] = (df_o.poas_bp + df_o.poas_bp.shift(-1)) / 2.
        df.loc[df.index == df.index.max(), "bb"] = max(2800.,
                                                       df.loc[df.index == df.index.max(), "poas_bp"].values[0] * 1.1)
        return [1., 1., 1., 1.], bounds

    def process_result(self, res, df, user_args):
        df = self.calculateBp(res.x, df)
        save(df, user_args, identifier="bp", plus_or_individual="individual", solver=1, distribution="beta")
        return True

    def cost_function(self, i_vars, df_o):
        df = df_o.copy(deep=True)
        df = self.calculateBp(i_vars, df)
        error = df.exp_error.sum() / 0.01 + df.poas_error.sum() / 0.1
        return error

    def calculateBp(self, i_vars, df):
        bb_multi, alpha_multi, beta_multi, eta_multi = i_vars
        alpha, beta_value, eta = self.alpha * alpha_multi, self.beta * beta_multi, self.eta * eta_multi
        # bp calculations
        df["bb"] = df.bb * bb_multi

        b_max = max(3000., 1.25 * df.poas_bp.tail(1).values[0])
        df["y"] = (df.bb - eta) / (b_max - eta)
        df.loc[df.y > 1., "y"] = 0.9999999999
        df["p0"] = df.apply(lambda row1: betaInbuilt.cdf(row1.y, alpha, beta_value), axis=1)
        df.loc[df.p0 > 1., "p0"] = 0.9999999999
        df["p1"] = df.apply(lambda row1: betaInbuilt.cdf(row1.y, alpha + 1, beta_value), axis=1)
        df.loc[df.p1 > 1., "p1"] = 0.9999999999
        df['p0diff'] = df['p0'].sub(df['p0'].shift())
        df['p0diff'] = df['p0diff'].fillna(df['p0'].iloc[0])
        df['p1diff'] = df['p1'].sub(df['p1'].shift())
        df['p1diff'] = df['p1diff'].fillna(df['p1'].iloc[0])
        df.loc[df.p0diff == 0, "p0diff"] = 1.
        df["bp"] = eta + (alpha / (alpha + beta_value)) * (b_max - eta) * df.p1diff.values / df.p0diff.values
        df["bp_Beta"] = df.bp

        df["seq"] = range(0, df.shape[0])

        for scn, row in df.iterrows():
            if (row.seq - 1.) in df.seq.tolist():
                prow = df.loc[df.seq == row.seq - 1].squeeze()
                if row.bp < prow.bp:
                    df.loc[df.seq == row.seq, "bp"] = row.poas_bp
        for scn, row in df.iterrows():
            if (row.seq + 1.) in df.seq.tolist():
                n_row = df.loc[df.seq == row.seq + 1].squeeze()
                if row.bp > n_row.bp:
                    df.loc[df.seq == row.seq, "bp"] = row.poas_bp

        df["exp_error"] = ((df.expbp - df.bp) / df.expbp) ** 2
        df["poas_error"] = ((df.poas_bp - df.bp) / df.poas_bp) ** 2
        # df.scn = df.index
        # df.index = df.seq
        df.loc[df.index <= 5, "bp"] = df.loc[df.index <= 5, "expbp"]
        # df.index = df.scn
        return df


def beta(user_args):
    obj = BpSolver1()
    obj.optimize(user_args)


"""
executed for all inputs
"""

if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection
    import pandas as pd
    from Scripts.pvt_new.settings import DEBUG_DIR
    import os

    well_name = "ALUMM"

    debug_dir = os.path.join(DEBUG_DIR, well_name)

    db = get_connection(well_name)
    test_user_args = pd.Series({"debug": True, "well_conn": db, "bias": "general", "debug_dir": debug_dir})
    beta(test_user_args)
