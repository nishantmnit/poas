import math

import numpy as np
import pandas as pd
from numpy import exp

from Scripts.pvt_new.core.get_logger import get_logger
from Scripts.pvt_new.core.storage.fetch_most import recommended, validated_data

logger = get_logger("pvt bp input")


def calExpBPInDegK(mw, density, bp=float('NaN')):
    if not math.isnan(float(bp)) and not bp == 0:
        return float(bp)
    else:
        sg = density/0.999016
        return ((5.0 / 9.0) * (1928.3 - 1.695 * 10.0 ** 5.0 * mw ** (-0.03522) * sg ** 3.266 *
                               (exp(-4.922 * 10.0 ** -3.0 * mw - 4.7685 * sg + 3.462 * 10.0 ** -3.0 * mw * sg)))) \
            if mw < 300 \
            else (9.3369 * (exp(1.6514 * 10.0 ** -4.0 * mw + 1.4103 * sg - 7.5152 * 10.0 ** -4.0 * mw * sg)) *
                  mw ** 0.5369 * sg ** -0.7276)


def createLastFraction(df_o, well_conn):
    df_v, dfplus_v = validated_data(well_conn)
    ls = df_v.index.max()
    # create plus fraction where mf < 10**-8
    df_o["zimibysgi"] = df_o.zimi / df_o.density
    mp = df_o.loc[(df_o.mf < 10 ** -8) & (df_o.index > ls)].index.min() - 1
    if np.isnan(mp):
        return df_o
    else:
        mp = int(mp)
    mf, mass, zimi = df_o.loc[df_o.index >= mp, "mf"].sum(), df_o.loc[df_o.index >= mp, "zimi"].sum() / df_o.loc[
        df_o.index >= mp, "mf"].sum(), df_o.loc[df_o.index >= mp, "zimi"].sum()
    density = zimi / df_o.loc[df_o.index >= mp, "zimibysgi"].sum()
    temp_df = pd.DataFrame(data=[[mf, mass, zimi, "C%s" % mp, density]],
                           columns=["mf", "mass", "zimi", "scn", "density"], index=[mp])
    df_o.drop(df_o.loc[df_o.index >= mp].index, inplace=True)
    df_o = pd.concat([df_o, temp_df], sort=True, axis=0)
    return df_o


def bp_input(well_conn):
    try:
        df, dfplus = validated_data(well_conn)
        dfmass, dfplusmass = recommended(well_conn, "mass")
        dfdensity, dfplusdensity = recommended(well_conn, "density")
        df = pd.concat([df, dfmass.loc[dfmass.index > df.index.max()]], axis=0, sort=True)
        df["mass"] = dfmass["mass"]
        df["density"] = dfdensity["density"]
        df.loc[df.index <= 5, "mass"] = df.loc[df.index <= 5, "expmass"]
        df.loc[df.index <= 5, "density"] = df.loc[df.index <= 5, "expdensity"]
        df["scn"] = df.apply(lambda row: "C%s" % row.name, axis=1)
        df["zimi"] = df.mass * df.mf
        df = createLastFraction(df, well_conn)
        df["xwi"] = df.zimi / df.zimi.sum()
        df["xwibysgi"] = df.xwi / df.density
        df["xvi"] = df.xwibysgi / df.xwibysgi.sum()
        df["cummxvi"] = df.xvi.cumsum()
        df["expbp"] = df.apply(
            lambda row: np.NaN if row.mass > 300 and math.isnan(float(row.expbp)) else calExpBPInDegK(row.mass,
                                                                                                      row.density,
                                                                                                      row.expbp),
            axis=1)
    except Exception as e:
        logger.info('Error:{"Type": "Initialize", "Message": "Failed to initialize BP solver.'
                    ' Please run mass and density solvers first."}')
        raise Exception(e)
    return df


if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection

    db = get_connection("ALUMM")
    print bp_input(db)
