from numpy import log
from scipy.special import gammainc, gamma


def calcBp(args, df):
    a, b, eta = args
    df["qi"] = df.apply(lambda row: log(1 / (1 - (row.cummxvi if row.cummxvi < 0.99999999 else 0.99999999))),
                        axis=1)
    df["incg"] = df.apply(lambda row: gammainc(1 + 1 / b, row.qi) * gamma(1 + 1 / b), axis=1)
    df['incgdiff'] = df['incg'].sub(df['incg'].shift())
    df.incgdiff = df.incgdiff.fillna(df['incg'].iloc[0])
    df["pav"] = (1 / df.xvi.values) * (a / b) ** (1 / b) * df.incgdiff.values
    df["bp"] = eta * (1 + df.pav.values)
    df["error"] = ((df.expbp - df.bp) / df.expbp) ** 2
    return df
