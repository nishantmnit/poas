from numpy import log

from Scripts.pvt_new.core.get_logger import get_logger
from Scripts.pvt_new.core.optimizers.generic_solver import GenericSolver
from Scripts.pvt_new.core.storage.save import save
from Scripts.pvt_new.mass.general.common import Differentiate
from bp_input import bp_input
from common import calcBp

logger = get_logger("pvt general bp")


def get_bounds():
    bounds = dict()
    A, B, eta = 15.0, 15.0, 200.
    bounds["A"] = [0.5, 30.]
    bounds["B"] = [0.5, 30.]
    bounds["Eta"] = [100., 300.]
    initialEtaFn = (eta - bounds["Eta"][0]) / (bounds["Eta"][1] - bounds["Eta"][0])
    initialAFn = (A - bounds["A"][0]) / (bounds["A"][1] - bounds["A"][0])
    initialBFn = (B - bounds["B"][0]) / (bounds["B"][1] - bounds["B"][0])
    return ([initialAFn, initialBFn, initialEtaFn],
            [[bounds["A"][0], bounds["A"][1]], [bounds["B"][0], bounds["B"][1]],
             [bounds["Eta"][0], bounds["Eta"][1]]])


def cost_function(iter_vars, args):
    a, b, eta = iter_vars
    df = calcBp(iter_vars, args)
    df.drop(df.loc[df.mass > 300].index, inplace=True)
    df["diffincg"] = df.apply(lambda row: Differentiate(b, row.qi, row.incg), axis=1)
    df["diffincgdiff"] = df['diffincg'].sub(df['diffincg'].shift())
    df.diffincgdiff = df.diffincgdiff.fillna(df['diffincg'].iloc[0])
    df["diffbpwrta"] = df.pav.values * eta / (a * b)
    df["diffbpwrteta"] = 1 + df.pav.values
    df["diffbpwrtb"] = (1 / df.xvi.values) * (a / b) ** (1 / b) * (
            df.incgdiff.values * (-(log(a / b) + 1) / b ** 2) + df.diffincgdiff.values) * eta
    df.loc[df.xvi == 0, "diffbpwrtb"] = 0.
    df["differrorwrta"] = df.diffbpwrta.values * 2 * ((df.bp.values - df.expbp.values) / df.expbp.values ** 2)
    df["differrorwrtb"] = df.diffbpwrtb.values * 2 * ((df.bp.values - df.expbp.values) / df.expbp.values ** 2)
    df["differrorwrteta"] = df.diffbpwrteta.values * 2 * ((df.bp.values - df.expbp.values) / df.expbp.values ** 2)
    df["derivativea"] = df.differrorwrta.values * df.error.values
    df["derivativeb"] = df.differrorwrtb.values * df.error.values
    df["derivativeeta"] = df.differrorwrteta.values * df.error.values
    avg = df.expbp.mean()
    df["r21"] = (df.bp.values - df.expbp.values) ** 2
    df["r22"] = (df.bp.values - avg) ** 2
    r2 = 1 - df.r21.sum() / df.r22.sum()
    df.drop(["r21", "r22"], axis=1, inplace=True)
    return df["error"].sum(), [df["derivativea"].sum(), df["derivativeb"].sum(), df["derivativeeta"].sum()], r2


def solver1(start_points, bounds, df, user_args, printname):
    logger.info("Executing General %s Solver 1" % printname)
    res = GenericSolver(start_points, bounds, cost_function, debug=user_args.debug, args=(df.copy(deep=True)))
    logger.info("Result -- %s" % res)
    logger.info("Completed General %s Solver 1" % printname)
    return res


def process_result(res, df, user_args):
    df = calcBp(res["Values"], df)
    df.loc[df.index <= 5, "bp"] = df.loc[df.index <= 5, "expbp"]
    save(df, user_args, identifier="bp", plus_or_individual="individual", solver=1, distribution="general")
    return True


def general(user_args):
    df = bp_input(user_args.well_conn)
    start_points, bounds = get_bounds()
    res = solver1(start_points, bounds, df, user_args, "BP")
    if not res["success"]:
        print 'Error:{"Type": "BP Solver", "Message": "BP Solver failed to find a feasible solution. ' \
              'Please check input data. If issue persists, please raise with POAS support team."}'
        raise Exception("General BP Solver failed.")
    process_result(res, df, user_args)


"""
executed for all inputs
"""

if __name__ == "__main__":
    from Scripts.pvt_new.core.storage.mongo import get_connection
    import pandas as pd
    from Scripts.pvt_new.settings import DEBUG_DIR
    import os

    well_name = "ALUMM"

    debug_dir = os.path.join(DEBUG_DIR, well_name)

    db = get_connection(well_name)
    test_user_args = pd.Series({"debug": True, "well_conn": db, "bias": "general", "debug_dir": debug_dir})
    general(test_user_args)
