import importlib
import numpy as np


def eos_multiplier(df, eos):
    eos_module = importlib.import_module("Scripts.pvt_new.eos.%s" % eos)
    eos_var = getattr(eos_module, "var_rename")["multiplier"]
    multiplier_converter = importlib.import_module("Scripts.pvt_new.eos.om_b_to_eos_multipliers")
    df = getattr(multiplier_converter, eos_var)(df, eos)
    df = getattr(eos_module, eos)(df)
    if eos == "sw_poas":
        df["w"] = -3. * df.omega_final
    return df


def om_b_multiplier(*args):
    df, eos = args
    df_bkp = df.copy(deep=True)
    df["multiplier"] = 1.
    eos_module = importlib.import_module("Scripts.pvt_new.eos.%s" % eos)
    df = getattr(eos_module, eos)(df)
    df_bkp["multiplier"] = df_bkp.om_b / df.om_b
    return df_bkp


def beta_multiplier(*args):
    df, eos = args
    df_bkp = df.copy(deep=True)
    df["beta_est"] = 0.79132 * df.zc_final - 0.02207
    df["min_om_b"] = (0.02207 + 0.20868 * df.zc_final) * 0.215
    df_bkp["multiplier"] = (df.zc_final - df.om_b) / df.beta_est
    return df_bkp


def zc_multiplier(*args):
    df_o, eos = args
    df = df_o.copy(deep=True)
    df["a1_d"] = -1 * 3 * df.om_b
    df["a2_d"] = 3 * df.om_b ** 2
    df["a3_d"] = -1 * (df.om_b ** 3 + 2 * df.om_b ** 2)
    om_b_module = importlib.import_module("Scripts.pvt_new.eos.get_om_b")
    df = getattr(om_b_module, "get_om_b_df")(df, 1.)
    df_o["multiplier"] = df.om_b / df.zc_final
    return df_o


def omega_multiplier(*args):
    df, eos = args
    if eos == "als_poas":
        return als_poas(df)
    elif eos == "sw_poas":
        return sw_poas(df)
    else:
        raise NotImplementedError("Multiplier conversion Logic for eos = %s is not coded." % eos)


def als_poas(df_o):
    df = df_o.copy(deep=True)
    df["term1"] = ((0.03452 - np.sqrt(0.03452 ** 2 - 4 * 0.0033 * (0.08974 - df.om_b))) / (2 * 0.0033))
    df["term2"] = ((0.03452 + np.sqrt(0.03452 ** 2 - 4 * 0.0033 * (0.08974 - df.om_b))) / (2 * 0.0033))
    df["term3"] = ((0.03452 - np.sqrt(0.03452 ** 2 - 4 * 0.0033 * (0.08974 - df.om_b))) / (2 * 0.0033))
    df["term4"] = ((0.03452 + np.sqrt(0.03452 ** 2 - 4 * 0.0033 * (0.08974 - df.om_b))) / (2 * 0.0033))

    df.loc[(df.term1 < 0.001) & (df.term2 < 0.001), "multiplier"] = 0.001
    df.loc[np.abs(df.term3) < np.abs(df.term4), "multiplier"] = df.term3
    df.loc[np.abs(df.term3) >= np.abs(df.term4), "multiplier"] = df.term4
    df_o["multiplier"] = df.multiplier / df.omega
    return df_o


def sw_poas(df_o):
    df = df_o.copy(deep=True)
    df["a1_d"] = (81 * df.om_b ** 3 - 27 * df.om_b ** 2) / (27 * df.om_b ** 3)
    df["a2_d"] = (81 * df.om_b ** 3 - 54 * df.om_b ** 2 + 9 * df.om_b) / (27 * df.om_b ** 3)
    df["a3_d"] = (27 * df.om_b ** 3 + 27 * df.om_b ** 2 + 9 * df.om_b - 1) / (27 * df.om_b ** 3)
    om_b_module = importlib.import_module("Scripts.pvt_new.eos.get_om_b")
    df = getattr(om_b_module, "get_om_b_df")(df, 1.)
    df_o["multiplier"] = df.om_b / df.omega
    return df_o
