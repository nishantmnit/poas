import pandas as pd


def apply_max(row, column):
    if isinstance(row, pd.DataFrame):
        if "max_%s" % column in row.columns:
            row[column] = row.apply(lambda x: min(x[column], x["max_%s" % column]), axis=1)
    else:
        if "max_%s" % column in row.index:
            row[column] = min(row[column], row["max_%s" % column])
    return row


def apply_min(row, column):
    if isinstance(row, pd.DataFrame):
        if "min_%s" % column in row.columns:
            row[column] = row.apply(lambda x: max(x[column], x["min_%s" % column]), axis=1)
    else:
        if "min_%s" % column in row.index:
            row[column] = max(row[column], row["min_%s" % column])
    return row
