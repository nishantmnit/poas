import numpy as np
from Scripts.pvt_new.core.storage.mongo import fetch


def fix_hv_vb(df_o, well_conn):
    df = df_o.copy(deep=True)
    ls = df.index.max()
    cm = fetch(well_conn, property="pvt_output", identifier="fcm")
    df["mass"] = cm.mass
    df = df.loc[df.index < ls]
    df["xi"] = np.log(df.mass)
    df["yi"] = np.log(df.hv_bp)
    df["xiyi"] = df.xi * df.yi
    df["xi2"] = df.xi ** 2

    xi = df.xi.sum()
    yi = df.yi.sum()
    xiyi = df.xiyi.sum()
    xi2 = df.xi2.sum()
    samples = df.shape[0]
    b = (xi * xiyi - yi * xi2) / (xi ** 2 - samples * xi2)
    a = (yi - samples * b) / xi

    df_o.loc[df_o.index == ls, "hv_bp"] = np.exp(np.log(cm.loc[cm.index == ls, "mass"].values[0]) * a + b)
    if df_o.loc[df_o.index == ls, "hv_bp"].values[0] <= 0:
        df_o.loc[df_o.index == ls, "hv_bp"] = 1.
    df_o.loc[df_o.index == ls, "vb"] = cm.loc[cm.index == ls, "vb"].values[0]
