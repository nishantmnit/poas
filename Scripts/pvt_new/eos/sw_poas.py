import autograd.numpy as np  # Thinly-wrapped version of Numpy
import pandas as pd

from Scripts.pvt_new.core.constants import gasConstant

var_rename = {"multiplier": "omega_multiplier"}


def sw_poas(row):
    row["omega_final"] = row.omega * row.multiplier
    row["a1_d"] = 3. / (1. + 6. * row.omega_final)
    row["a2_d"] = 3. / (1. + 6. * row.omega_final)
    row["a3_d"] = -1. / (1. + 6. * row.omega_final)
    if isinstance(row, pd.DataFrame):
        row["om_b"] = row.apply(lambda row1: _om_b(row1), axis=1)
    else:
        row["om_b"] = _om_b(row)
    row["final_q"] = row.om_b
    row["eta"] = 1. / (3. * (1. + row.final_q * row.omega_final))
    row["om_b"] = row.final_q * row.eta
    row["om_a"] = (1. - row.eta * (1. - row.final_q)) ** 3
    row["b"] = row.om_b * gasConstant * row.tc_k / (row.pc_mpa * 10 ** 6)
    row["ac"] = (row.om_a * gasConstant ** 2 * row.tc_k ** 2 / (row.pc_mpa * 10 ** 6))
    row["ac_alpha"] = (row.ac * row.alpha_eos)
    row["u"] = 1. + 3. * row.omega_final
    if isinstance(row, pd.DataFrame):
        row["w"] = row.apply(lambda row1: -1 if row1.omega_final < 0 else -3. * row1.omega_final, axis=1)
    else:
        row["w"] = -1 if row.omega_final < 0 else -3. * row.omega_final
    return row


def _om_b(row):
    row["Q_d"] = (3. * row.a2_d - row.a1_d ** 2) / 9.
    row["L_d"] = (9. * row.a1_d * row.a2_d - 27. * row.a3_d - 2. * row.a1_d ** 3) / 54.
    row["D_d"] = row.Q_d ** 3 + row.L_d ** 2
    row["S1_d"] = (-1. * (-1. * (row.L_d + np.sqrt(row.D_d))) ** (1. / 3.)) if (row.L_d + np.sqrt(
        row.D_d)) < 0 else (row.L_d + np.sqrt(row.D_d)) ** (1. / 3.)
    row["S2_d"] = (-1. * (-1. * (row.L_d - np.sqrt(row.D_d))) ** (1. / 3.)) if (row.L_d - np.sqrt(
        row.D_d)) < 0 else (row.L_d - np.sqrt(row.D_d)) ** (1. / 3.)
    row["theta"] = np.degrees(
        np.arccos(1. if np.abs(row.L_d / np.sqrt(-row.Q_d ** 3)) > 1 else row.L_d / np.sqrt(-row.Q_d ** 3)))
    row["Z1_ov"] = 0 if row.Q_d >= 0 else 2. * np.sqrt(-row.Q_d) * np.cos(
        np.radians(row.theta * 1. / 3. + 120)) - row.a1_d / 3.
    row["Z2_ov"] = 0 if row.Q_d >= 0 else 2. * np.sqrt(-row.Q_d) * np.cos(
        np.radians(row.theta * 1. / 3. + 240)) - row.a1_d / 3.
    row["Z3_ov"] = 0 if row.Q_d >= 0 else 2. * np.sqrt(-row.Q_d) * np.cos(
        np.radians(row.theta * 1. / 3.)) - row.a1_d / 3.
    row["Z1_ov_d"] = row.S1_d + row.S2_d - row.a1_d / 3.
    row["Z1_ov_dd"] = ((-1. * (-1. * row.L_d) ** 1. / 3.) if row.L_d < 0 else row.L_d ** (
            1. / 3.)) * 2. - row.a1_d / 3.
    row["Z2_ov_dd"] = (((-1. * (-1. * row.L_d) ** 1. / 3.) if row.L_d < 0 else row.L_d ** (
            1. / 3.)) + row.a1_d / 3.) * -1
    row["Z3_ov_dd"] = row.Z2_ov_dd
    row["om_b1"] = _min([row.Z1_ov, row.Z2_ov, row.Z3_ov])
    row["om_b2"] = _min([row.Z1_ov_dd, row.Z2_ov_dd, row.Z3_ov_dd])
    row["om_b"] = row.om_b1 if row.D_d < 0 else (row.Z1_ov_d if row.D_d > 0 else row.om_b2)
    return row["om_b"]


def _min(data):
    data = [x for x in data if x > 0]
    if len(data) > 0:
        return min(data)
    return 0


def _max(data):
    data = [x for x in data if x > 0]
    if len(data) > 0:
        return max(data)
    return 0
