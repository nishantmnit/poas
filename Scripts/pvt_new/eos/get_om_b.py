import numpy as np
import pandas as pd


def get_om_b(data, multiplier=1.12):
    if isinstance(data, pd.DataFrame):
        return get_om_b_df(data, multiplier)
    return get_om_b_row(data, multiplier)


def get_om_b_row(row, multiplier):
    row["Q_d"] = (3. * row.a2_d - row.a1_d ** 2) / 9.
    row["L_d"] = (9. * row.a1_d * row.a2_d - 27. * row.a3_d - 2. * row.a1_d ** 3) / 54.
    row["D"] = row.Q_d ** 3 + row.L_d ** 2
    row["S1"] = (-1. * (-1. * (row.L_d + np.sqrt(row.D))) ** (1. / 3.)) \
        if (row.L_d + np.sqrt(row.D)) < 0 \
        else (row.L_d + np.sqrt(row.D)) ** (1. / 3.)
    row["S2"] = (-1. * (-1. * (row.L_d - np.sqrt(row.D))) ** (1. / 3.)) \
        if (row.L_d - np.sqrt(row.D)) < 0 else (row.L_d - np.sqrt(row.D)) ** (1. / 3.)
    row["theta"] = np.degrees(np.arccos(1. if np.abs(row.L_d / np.sqrt(-row.Q_d ** 3)) > 1
                                        else row.L_d / np.sqrt(-row.Q_d ** 3)))
    row["Z1"] = 2. * np.sqrt(-row.Q_d) * np.cos(np.radians(row.theta * 1. / 3. + 120)) - row.a1_d / 3.
    row["Z2"] = 2. * np.sqrt(-row.Q_d) * np.cos(np.radians(row.theta * 1. / 3. + 240)) - row.a1_d / 3.
    row["Z3"] = 2. * np.sqrt(-row.Q_d) * np.cos(np.radians(row.theta * 1. / 3.)) - row.a1_d / 3.
    row["Z1_d"] = row.S1 + row.S2 - row.a1_d / 3.
    row["Z1_dd"] = ((-1. * (-1. * row.L_d) ** 1. / 3.) if row.L_d < 0 else row.L_d ** (1. / 3.)) * 2. - row.a1_d / 3.
    row["Z2_dd"] = (((-1. * (-1. * row.L_d) ** 1. / 3.) if row.L_d < 0 else row.L_d ** (1. / 3.)) + row.a1_d / 3.) * -1
    row["Z3_dd"] = row.Z2_dd
    row["om_b1"] = _min([row.Z1, row.Z2, row.Z3])
    row["om_b2"] = _min([row.Z1_dd, row.Z2_dd, row.Z3_dd])
    row["om_b"] = (row.om_b1 if row.D < 0 else (row.Z1_d if row.D > 0 else row.om_b2)) * multiplier
    return row


def get_om_b_df(df, multiplier):
    df["Q_d"] = (3. * df.a2_d - df.a1_d ** 2) / 9.
    df["L_d"] = (9. * df.a1_d * df.a2_d - 27. * df.a3_d - 2. * df.a1_d ** 3) / 54.
    df["D"] = df.Q_d ** 3 + df.L_d ** 2
    df["S1"] = df.apply(
        lambda row: (-1. * (-1. * (row.L_d + np.sqrt(row.D))) ** (1. / 3.))
        if (row.L_d + np.sqrt(row.D)) < 0
        else (row.L_d + np.sqrt(row.D)) ** (1. / 3.), axis=1)
    df["S2"] = df.apply(
        lambda row: (-1. * (-1. * (row.L_d - np.sqrt(row.D))) ** (1. / 3.))
        if (row.L_d - np.sqrt(row.D)) < 0 else (row.L_d - np.sqrt(row.D)) ** (1. / 3.), axis=1)
    df["theta"] = df.apply(
        lambda row: np.degrees(np.arccos(1. if np.abs(row.L_d / np.sqrt(-row.Q_d ** 3)) > 1
                                         else row.L_d / np.sqrt(-row.Q_d ** 3))), axis=1)
    df["Z1"] = 2. * np.sqrt(-df.Q_d) * np.cos(np.radians(df.theta * 1. / 3. + 120)) - df.a1_d / 3.
    df["Z2"] = 2. * np.sqrt(-df.Q_d) * np.cos(np.radians(df.theta * 1. / 3. + 240)) - df.a1_d / 3.
    df["Z3"] = 2. * np.sqrt(-df.Q_d) * np.cos(np.radians(df.theta * 1. / 3.)) - df.a1_d / 3.
    df["Z1_d"] = df.S1 + df.S2 - df.a1_d / 3.
    df["Z1_dd"] = df.apply(
        lambda row: (-1. * (-1. * row.L_d) ** 1. / 3.) if row.L_d < 0
        else row.L_d ** (1. / 3.), axis=1) * 2. - df.a1_d / 3.
    df["Z2_dd"] = (df.apply(
        lambda row: (-1. * (-1. * row.L_d) ** 1. / 3.) if row.L_d < 0
        else row.L_d ** (1. / 3.), axis=1) + df.a1_d / 3.) * -1
    df["Z3_dd"] = df.Z2_dd
    df["om_b1"] = df.apply(lambda row: _min([row.Z1, row.Z2, row.Z3]), axis=1)
    df["om_b2"] = df.apply(lambda row: _min([row.Z1_dd, row.Z2_dd, row.Z3_dd]), axis=1)
    df["om_b"] = df.apply(lambda row: row.om_b1 if row.D < 0 else (row.Z1_d if row.D > 0 else row.om_b2), axis=1) * multiplier
    return df


def _min(data):
    data = [x for x in data if x > 0]
    if len(data) > 0:
        return min(data)
    return 0
