#!/usr/bin/python 
from Crypto.Hash import SHA256
from Crypto.Cipher import AES
import os, random, sys, pkg_resources
import argparse

def encrypt(key, filename):
	chunksize = 64 * 1024
	outFile = os.path.join(os.path.dirname(filename), os.path.basename(filename)+'.e')
	filesize = str(os.path.getsize(filename)).zfill(16)
	IV = ''

	for i in range(16):
		IV += chr(random.randint(0, 0xFF))
	
	encryptor = AES.new(key, AES.MODE_CBC, IV)

	with open(filename, "rb") as infile:
		with open(outFile, "wb") as outfile:
			outfile.write(filesize)
			outfile.write(IV)
			while True:
				chunk = infile.read(chunksize)
				
				if len(chunk) == 0:
					break

				elif len(chunk) % 16 !=0:
					chunk += ' ' *  (16 - (len(chunk) % 16))

				outfile.write(encryptor.encrypt(chunk))
	os.chmod(outfile.name, 0755)
	return outfile

def decrypt(key, filename):
	outFile = os.path.join(os.path.dirname(filename), os.path.basename(filename)[:-2])
	chunksize = 64 * 1024
	with open(filename, "rb") as infile:
		filesize = infile.read(16)
		IV = infile.read(16)

		decryptor = AES.new(key, AES.MODE_CBC, IV)
		
		with open(outFile, "wb") as outfile:
			while True:
				chunk = infile.read(chunksize)
				if len(chunk) == 0:
					break

				outfile.write(decryptor.decrypt(chunk))

			outfile.truncate(int(filesize))
	os.chmod(outfile.name, 0755)
	return outFile
	
def allfiles():
	allFiles = []
	#for root, subfiles, files in os.walk(os.getcwd()):
	for root, subfiles, files in os.walk('~/POAS/Python/Scripts/dist/main/'):
		for names in files:
			allFiles.append(os.path.join(root, names))

	return allFiles

def main():
	args= getoptions()
	if args.action == "e":
		filename = args.file
		if not os.path.exists(filename):
			print "The file does not exist"
			sys.exit(0)
		elif os.path.basename(filename).endswith(".e"):
			print "%s is already encrypted" %str(filename)
			pass
		elif filename == os.path.join(os.getcwd(), sys.argv[0]):
			pass 
		else:
			outfile= encrypt(SHA256.new(args.key).digest(), str(filename))
			if args.outfile:
				os.rename(outfile.name, args.outfile)
				outfile= args.outfile
			else:
				outfile= outfile.name
			print "Done encrypting %s" %str(outfile)
			os.remove(filename)

	elif args.action == "d":
		filename = args.file
		if not os.path.exists(filename):
			print "The file does not exist"
			sys.exit(0)
		elif not filename.endswith(".e"):
			print "%s is already not encrypted" %filename
			sys.exit()
		else:
			outfile= decrypt(SHA256.new(args.key).digest(), filename)
			if args.outfile:
				os.rename(outfile, args.outfile)
				outfile= args.outfile
			else:
				outfile= outfile
			print "Done decrypting %s" %outfile
			#os.remove(filename)

	else:
		print "Please choose a valid command."
		sys.exit()

def getoptions():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--action",   dest='action',      default="d", 						help='(e)ncrypt/(d)ecrypt')
    parser.add_argument("-k", "--key",      dest="key",         default='!)~M4r8<O^}m$S@-i;r{"',    help="key to e/d")
    parser.add_argument("-f", "--file",     dest='file',                     						help='filename to e/d')
    parser.add_argument("-o", "--outfile",  dest='outfile',                     					help='output filename')
    args = parser.parse_args()
    if (not args.file):
        usage()
        sys.exit(1)
    return args

def usage():
    print "\nUsage:\n%s -f <filename> [-a <e/d, default d> -k <key to encrypt/decrypt, default !)~M4r8<O^}m$S@-i;r{\">, -o <output filename>]\n" %(sys.argv[0])

main()


#pyinstaller SpecFiles/Mac/main.spec --clean

# Original:

# Ganeshs-MacBook-Air:~/POAS/Python/Scripts$ file  ./dist/main/main 
# ./dist/main/main: Mach-O 64-bit executable x86_64
# Ganeshs-MacBook-Air:~/POAS/Python/Scripts$ wc -l   ./dist/main/main 
#    40798 ./dist/main/main
# Ganeshs-MacBook-Air:~/POAS/Python/Scripts$ 

# Encrypted:

# Ganeshs-MacBook-Air:~/POAS/Python/Scripts$ python encryptdecrypt.py -a e -f dist/main/main
# Done encrypting dist/main/main
# Ganeshs-MacBook-Air:~/POAS/Python/Scripts$

# Ganeshs-MacBook-Air:~/POAS/Python/Scripts$ file dist/main/main.e 
# dist/main/main.e: data
# Ganeshs-MacBook-Air:~/POAS/Python/Scripts$ wc -l  dist/main/main.e 
#    40884 dist/main/main.e
# Ganeshs-MacBook-Air:~/POAS/Python/Scripts$

# Decrypted:

# Ganeshs-MacBook-Air:~/POAS/Python/Scripts$ python encryptdecrypt.py -f dist/main/main.e
# Done decrypting dist/main/main
# Ganeshs-MacBook-Air:~/POAS/Python/Scripts$

# Ganeshs-MacBook-Air:~/POAS/Python/Scripts$ wc -l ./dist/main/main
#    40798 ./dist/main/main
# Ganeshs-MacBook-Air:~/POAS/Python/Scripts$ file ./dist/main/main
# ./dist/main/main: Mach-O 64-bit executable x86_64
# Ganeshs-MacBook-Air:~/POAS/Python/Scripts$ 
