import autograd.numpy as np  # Thinly-wrapped version of Numpy
from autograd import grad as gd

from Scripts.pvt.core.constants import gasConstant
from Scripts.pvt.vle_old.generic import zl_zv, cost

my_vars = ["alpha_multiplier", "omega_multiplier"]
scales = [0.0001, 75., 175, 175., 0.0000001, 200.]

output = ["alpha_eos"]


def start_point(row):
    return [row.start_alpha_multiplier, row.start_omega_multiplier]


def pre_processing(df):
    return df


def var_range(row):
    return [row.alpha_multiplier_min, row.alpha_multiplier_max], [row.omega_multiplier_min, row.omega_multiplier_max]


def final_calculated_values(df):
    df = df.copy(deep=True)
    for index, row in df.iterrows():
        row = calculated_values(row)
        for col in row.keys():
            df.loc[df.seq == row.seq, col] = row[col]
    return df


def _calc(row):
    row["omega"] = row.omega * row.omega_multiplier
    row["om_b"] = 0.001 if (0.08974 - 0.03452 * row.omega + 0.0033 * row.omega ** 2) < 0.001 else (
            0.08974 - 0.03452 * row.omega + 0.0033 * row.omega ** 2)
    row["om_c"] = 0.001 if (
                                   0.03686 + 0.00405 * row.omega - 0.01073 * row.omega ** 2 + 0.00157 * row.omega ** 3) < 0.001 else (
            0.03686 + 0.00405 * row.omega - 0.01073 * row.omega ** 2 + 0.00157 * row.omega ** 3)
    row["om_d"] = 0.001 if (
                                   0.154 + 0.14122 * row.omega - 0.00272 * row.omega ** 2 - 0.00484 * row.omega ** 3) < 0.001 else (
            0.154 + 0.14122 * row.omega - 0.00272 * row.omega ** 2 - 0.00484 * row.omega ** 3)
    row["om_a"] = 0.001 if (
                                   0.44869 + 0.04024 * row.omega + 0.01111 * row.omega ** 2 - 0.00576 * row.omega ** 3) < 0.001 else (
            0.44869 + 0.04024 * row.omega + 0.01111 * row.omega ** 2 - 0.00576 * row.omega ** 3)
    row["b"] = row.om_b * gasConstant * row.tc_k / (row.pc_mpa * 10 ** 6)
    row["c"] = row.om_c * gasConstant * row.tc_k / (row.pc_mpa * 10 ** 6)
    row["d"] = row.om_d * gasConstant * row.tc_k / (row.pc_mpa * 10 ** 6)
    row["ac"] = (row.om_a * gasConstant ** 2 * row.tc_k ** 2 / (row.pc_mpa * 10 ** 6))
    row["ac_alpha"] = (row.ac * row.alpha_eos)
    row["u"] = 0. if row.b == 0 else ((row.d - row.c) / row.b)
    row["w"] = 0. if row.b == 0 else (-1. * row.d * row.c / row.b ** 2)
    return row


def calculated_values(row_o):
    row = row_o.copy(deep=True)
    row["alpha_eos"] = row.alpha_pr * row.alpha_multiplier
    row["diff_alpha_wrt_temp"] = row.alpha_eos * (((2. * row.alpha_m - 2.) / (
            row.temperature / row.tc_k) + row.alpha_l * (-2 * row.alpha_m) * (row.temperature / row.tc_k) ** (
                                                           2 * row.alpha_m - 1)) / row.tc_k)
    row = _calc(row)
    row = zl_zv(row)
    row = cost(row, scales)
    return row


def objective(row_o, iter_vars):
    iter_row = row_o.copy(deep=True)
    for i in range(len(iter_vars)):
        iter_row[my_vars[i]] = iter_vars[i]
    return calculated_values(iter_row).total_objective


def cost_for_ncg(iter_vars, row_o):
    iter_row = row_o.copy(deep=True)
    obj_cost = objective(iter_row, iter_vars)
    grad_fct = gd(objective, 1)
    derivative = np.array([i.item(0) for i in grad_fct(iter_row, iter_vars)])
    return obj_cost, derivative
