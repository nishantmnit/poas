import autograd.numpy as np   # Thinly-wrapped version of Numpy
from Scripts.pvt.core.constants import gasConstant


def om_b(df):
    df = root_finder(df)
    df["om_b1"] = df.apply(lambda row: min([row.Z1, row.Z2, row.Z3]), axis=1)
    df["om_b2"] = df.apply(lambda row: min([row.Z1_dd, row.Z2_dd, row.Z3_dd]), axis=1)
    df["om_b"] = df.apply(lambda row: row.om_b1 if row.D < 0 else (row.Z1_d if row.D > 0 else row.om_b2),
                          axis=1) * 1.12
    return df


def root_finder(df):
    df["Q_d"] = (3. * df.a2_d - df.a1_d ** 2) / 9.
    df["L_d"] = (9. * df.a1_d * df.a2_d - 27. * df.a3_d - 2. * df.a1_d ** 3) / 54.
    df["D"] = df.Q_d ** 3 + df.L_d ** 2
    df["S1"] = df.apply(
        lambda row: (-1. * (-1. * (row.L_d + np.sqrt(row.D))) ** (1. / 3.)) if (row.L_d + np.sqrt(row.D)) < 0 else (
                                                                                                                           row.L_d + np.sqrt(
                                                                                                                       row.D)) ** (
                                                                                                                           1. / 3.),
        axis=1)
    df["S2"] = df.apply(
        lambda row: (-1. * (-1. * (row.L_d - np.sqrt(row.D))) ** (1. / 3.)) if (row.L_d - np.sqrt(row.D)) < 0 else (
                                                                                                                           row.L_d - np.sqrt(
                                                                                                                       row.D)) ** (
                                                                                                                           1. / 3.),
        axis=1)
    df["theta"] = df.apply(lambda row: np.degrees(
        np.arccos(1. if np.abs(row.L_d / np.sqrt(-row.Q_d ** 3)) > 1 else row.L_d / np.sqrt(-row.Q_d ** 3))), axis=1)
    df["Z1"] = 2. * np.sqrt(-df.Q_d) * np.cos(np.radians(df.theta * 1. / 3. + 120)) - df.a1_d / 3.
    df["Z2"] = 2. * np.sqrt(-df.Q_d) * np.cos(np.radians(df.theta * 1. / 3. + 240)) - df.a1_d / 3.
    df["Z3"] = 2. * np.sqrt(-df.Q_d) * np.cos(np.radians(df.theta * 1. / 3.)) - df.a1_d / 3.
    df["Z1_d"] = df.S1 + df.S2 - df.a1_d / 3.
    df["Z1_dd"] = df.apply(lambda row: (-1. * (-1. * row.L_d) ** 1. / 3.) if row.L_d < 0 else row.L_d ** (1. / 3.),
                           axis=1) * 2. - df.a1_d / 3.
    df["Z2_dd"] = (df.apply(lambda row: (-1. * (-1. * row.L_d) ** 1. / 3.) if row.L_d < 0 else row.L_d ** (1. / 3.),
                            axis=1) + df.a1_d / 3.) * -1
    df["Z3_dd"] = df.Z2_dd
    return df


def _min(data):
    data = [x for x in data if x > 0]
    if len(data) > 0:
        return min(data)
    return 0


def zl_zv(row):
    row["r1"] = 0 if (row.u ** 2 - 4 * row.w) <= 0 else (row.u - np.sqrt(row.u ** 2 - 4 * row.w)) / 2
    row["r2"] = 0 if (row.u ** 2 - 4 * row.w) <= 0 else (row.u + np.sqrt(row.u ** 2 - 4 * row.w)) / 2
    row["A"] = row.ac_alpha * row.pressure * 10 ** 6 / (gasConstant ** 2 * row.temperature ** 2)
    row["B"] = (row.b * row.pressure * 10 ** 6 / (gasConstant * row.temperature))
    row["a1"] = (row.u * row.B - row.B - 1)
    row["a2"] = (row.A + row.w * row.B ** 2 - row.u * row.B - row.u * row.B ** 2)
    row["a3"] = -1. * (row.A * row.B + row.w * row.B ** 2 + row.w * row.B ** 3)
    row["Q"] = (3 * row.a2 - row.a1 ** 2) / 9
    row["L"] = (9 * row.a1 * row.a2 - 27 * row.a3 - 2 * row.a1 ** 3) / 54
    row["D"] = row.Q ** 3 + row.L ** 2
    row["S1"] = (-1. * (-1. * (row.L + np.sqrt(row.D))) ** (1. / 3.)) if (row.L + np.sqrt(row.D)) < 0 \
        else (row.L + np.sqrt(row.D)) ** (1. / 3.)
    row["S2"] = (-1. * (-1. * (row.L - np.sqrt(row.D))) ** (1. / 3.)) if (row.L - np.sqrt(row.D)) < 0 \
        else (row.L - np.sqrt(row.D)) ** (1. / 3.)
    row["Z1"] = 0 if row.Q >= 0 else 2. * np.sqrt(-row.Q) * np.cos(1. / 3. * (np.arccos(
        1. if np.abs(row.L / np.sqrt(-row.Q ** 3)) > 1 else row.L / np.sqrt(
            -row.Q ** 3))) + 2 * np.pi / 3.) - row.a1 / 3.
    row["Z2"] = 0 if row.Q >= 0 else 2. * np.sqrt(-row.Q) * np.cos(1. / 3. * (np.arccos(
        1. if np.abs(row.L / np.sqrt(-row.Q ** 3)) > 1 else row.L / np.sqrt(
            -row.Q ** 3))) + 4 * np.pi / 3.) - row.a1 / 3.
    row["Z3"] = 0 if row.Q >= 0 else 2. * np.sqrt(-row.Q) * np.cos(1. / 3. * (
        np.arccos(1. if np.abs(row.L / np.sqrt(-row.Q ** 3)) > 1 else row.L / np.sqrt(-row.Q ** 3)))) - row.a1 / 3.
    row["Z1_d"] = row.S1 + row.S2 - row.a1 / 3.
    row["Z1_dd"] = ((-1. * (-1. * row.L) ** 1. / 3.) if row.L < 0 else row.L ** (1. / 3.)) * 2. - row.a1 / 3.
    row["Z2_dd"] = (((-1. * (-1. * row.L) ** 1. / 3.) if row.L < 0 else row.L ** (1. / 3.)) + row.a1 / 3.) * -1
    row["Z3_dd"] = row.Z2_dd
    row["ZL"] = _min([row.Z1, row.Z2, row.Z3]) if row.D < 0 \
        else (_min([row.Z1_dd, row.Z2_dd, row.Z3_dd]) if row.D == 0 else row.Z1_d)
    row["ZV"] = _max([row.Z1, row.Z2, row.Z3]) if row.D < 0 \
        else (_max([row.Z1_dd, row.Z2_dd, row.Z3_dd]) if row.D == 0 else 0)
    row["phl"] = _fugacity(row.r1, row.r2, row.A, row.B, row.ZL, -1.5)
    row["phv"] = _fugacity(row.r1, row.r2, row.A, row.B, row.ZV, 1.)
    row["ph_diff"] = np.abs(row.phl - row.phv) if row.row_index == 5 else 0
    return row


def _max(data):
    data = [x for x in data if x > 0]
    if len(data) > 0:
        return max(data)
    return 0


def _fugacity(r1, r2, A, B, root, penalty):
    if (root - B) <= 0 or (B * (r2 - r1)) == 0 or (root + r1 * B) == 0 or (root + r2 * B) == 0 or (
            (root + r2 * B) * (root + r1 * B)) < 0:
        return penalty
    else:
        return (root - 1) - np.log(root - B) - (A / (B * (r2 - r1))) * np.log((root + r2 * B) / (root + r1 * B))


def cost(row_o, scales):
    row = row_o.copy(deep=True)
    row["v_liq"] = row.ZL * gasConstant * row.temperature / row.pressure
    row["v_vapour"] = row.ZV * gasConstant * row.temperature / row.pressure
    row["t1"] = 0 if row.v_liq == 0 or (row.b * (row.r2 - row.r1)) == 0 else ((1. / row.v_liq) * (
            (row.ac_alpha - row.temperature * row.ac * row.diff_alpha_wrt_temp) / (row.b * (row.r2 - row.r1))))
    if row.v_vapour == 0:
        row["t2"] = 0
    elif (row.v_vapour + row.r1 * row.b * 10 ** 6) <= 0 or (row.v_vapour + row.r2 * row.b * 10 ** 6) <= 0 or (
            (row.v_vapour + row.r1 * row.b * 10 ** 6) / (row.v_vapour + row.r2 * row.b * 10 ** 6)) < 0:
        row["t2"] = -200.
    else:
        row["t2"] = (
            np.log((row.v_vapour + row.r1 * row.b * 10 ** 6) / (row.v_vapour + row.r2 * row.b * 10 ** 6)))
    if row.v_liq == 0:
        row["t3"] = 0
    elif (row.v_liq + row.r1 * row.b * 10 ** 6) <= 0 or (row.v_liq + row.r2 * row.b * 10 ** 6) <= 0 or (
            (row.v_liq + row.r1 * row.b * 10 ** 6) / (row.v_liq + row.r2 * row.b * 10 ** 6)) < 0:
        row["t3"] = -1000.
    else:
        row["t3"] = (np.log((row.v_liq + row.r1 * row.b * 10 ** 6) / (row.v_liq + row.r2 * row.b * 10 ** 6)))
    row["del_ui_by_vi"] = row.t1 * (row.t2 - row.t3)
    row["solubility"] = 100. if row.del_ui_by_vi == 0 else np.sqrt(row.del_ui_by_vi)
    row["t1_del_h_liq"] = (gasConstant * row.temperature * (row.ZL - 1.))
    row["t1_del_h_vapour"] = (gasConstant * row.temperature * (row.ZV - 1.))
    row["t2_del_h"] = 10 ** 100 if row.b == 0 or (row.r2 - row.r1) == 0 \
        else (row.ac * (row.alpha_eos - row.temperature * row.diff_alpha_wrt_temp) / (row.b * (row.r2 - row.r1)))
    row["t3_del_h"] = -20. if (row.ZV + row.B * row.r2) == 0 or (row.ZV + row.B * row.r1) == 0 or \
                              ((row.ZV + row.B * row.r2) * (row.ZV + row.B * row.r1)) < 0 \
        else (np.log((row.ZV + row.B * row.r1) / (row.ZV + row.B * row.r2)))
    row["t4_del_h"] = 50000. if (row.ZL + row.B * row.r2) == 0 or (row.ZL + row.B * row.r1) == 0 or \
                                ((row.ZL + row.B * row.r2) * (row.ZL + row.B * row.r1)) < 0 \
        else (np.log((row.ZL + row.B * row.r1) / (row.ZL + row.B * row.r2)))
    row["del_h_liq"] = 0 if row_o.v_liq == 0 else ((row.t1_del_h_liq + row.t2_del_h * row.t4_del_h) / 1000.)
    row["del_h_vapour"] = 0 if row_o.v_vapour == 0 else ((row.t1_del_h_vapour + row.t2_del_h * row.t3_del_h) / 1000.)
    row["del_h"] = (row.del_h_vapour - row.del_h_liq) if row.row_index == 5 \
        else (row.del_h_vapour if row_o.v_liq == 0 else row.del_h_liq)
    row["obj_sol"] = 0 if row_o.solubility == 0 else ((row_o.solubility - row.solubility) / row_o.solubility) ** 2
    row["obj_del_h"] = 0 if row_o.del_h_fluid == 0 else ((row_o.del_h_fluid - row.del_h) / row_o.del_h_fluid) ** 2
    if (row.t2_del_h == 10 ** 100 or row.t3_del_h == -20. or row.t4_del_h == 50000.) and row.row_index != 5:
        row["obj_del_h"] = row["obj_del_h"] * 100.
    row["obj_v_liq"] = 0 if row_o.v_liq == 0 else ((row_o.v_liq - row.v_liq) / row_o.v_liq) ** 2
    row["obj_v_vapour"] = 0 if row_o.v_vapour == 0 else ((row_o.v_vapour - row.v_vapour) / row_o.v_vapour) ** 2
    row["total_objective"] = row.obj_sol * scales[0] + row.obj_del_h * scales[1] + row.obj_v_liq * scales[2] + row.obj_v_vapour * scales[3] + (scales[5] if row.row_index == 5 else scales[4]) * max(0, row_o.ph_diff - 10 ** -7) ** 2
    return row

