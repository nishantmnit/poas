import argparse
import importlib
import os.path

import numpy as np
import pandas as pd

from Lib.Utils import getLogger
from Scripts.pvt.core.optimizers.ncg import ncg
from Scripts.pvt.core.storage.excel import write_csv, write_excel
from Scripts.pvt.settings import CSV_DATA, LOG_DIR, DEBUG_DIR
from volume import standardize_volume
from del_h import standardize_delh
from standardize import standardize

solver = None


def read_options():
    parser = argparse.ArgumentParser()
    parser.add_argument('--eos', dest='eos', default='poas_4', help='EOS to run.')
    parser.add_argument('--compound', dest='compound', default='HYDROGEN SULFIDE', help='EOS to run.')
    parser.add_argument('--volume', dest='volume', default=False, action='store_true',
                        help='Run volume standardization')
    parser.add_argument('--delh', dest='delh', default=False, action='store_true',
                        help='Run del_h standardization')
    args = parser.parse_args()
    return args


def observed_data(eos, compound):
    observed_data_file = os.path.join(CSV_DATA, "vle_old", compound, "%s.csv" % eos)
    ov = pd.read_csv(observed_data_file)
    ov.dropna(inplace=True, axis=1, how='all')
    ov.dropna(inplace=True, axis=0, how='all')
    ov.drop(ov.loc[ov.row_index == 5].index, inplace=True)
    ov.reset_index(inplace=True, drop=True)
    return ov


def converged(debug, s_logger, iteration, grad):
    global solver
    total_grad = np.sqrt(np.sum(np.square(grad), axis=0))
    cd = solver.cost_diff
    if solver.pvars is not None and solver.curr_vars is not None:
        var_diff = np.array(solver.pvars) - np.array(solver.curr_vars)
        if np.all(np.abs(var_diff) <= 10 ** -16):
            if debug:
                s_logger.info("Converged by rule 0 - %s" % np.abs(var_diff))
            return True
    if iteration < 3:
        return False
    elif total_grad <= 10 ** -3 and iteration <= 15:
        if debug:
            s_logger.info("Converged by rule 1 - %s" % total_grad)
        return True
    elif iteration > 15 and cd <= 10 ** -4:
        if debug:
            s_logger.info("Converged by rule 2 - %s" % cd)
        return True
    return False


def run(eos, compound, volume, delh):
    vle_logger = getLogger(compound)
    eos_module = importlib.import_module("Scripts.pvt.vle_old.%s" % eos)
    df = observed_data(eos, compound)

    if volume:
        df_volume = standardize_volume(df)
        filename = os.path.join(CSV_DATA, "kij", compound)
        write_csv(True, save_to_dir=filename, filename=["volume"], df=df_volume)

    if delh:
        df_delh = standardize_delh(df)
        filename = os.path.join(CSV_DATA, "kij", compound)
        write_csv(True, save_to_dir=filename, filename=["del_h"], df=df_delh)

    df = eos_module.pre_processing(df)
    vle_logger.info("Started VLE tuning for compound = %s | eos = %s" % (compound, eos))

    total_rows = df.shape[0]
    logger.info("Total rows == %s" % total_rows)
    for index, row in df.iterrows():
        done_per = round(((index + 1.) / total_rows) * 100., 2)
        vars_range = eos_module.var_range(row)
        global solver
        solver = ncg()
        vle_logger.info("Executing vle for temperature = %s | Completed %s percent" % (row.temperature, done_per))

        end_vars = solver.minimize(eos_module.start_point(row), vars_range,
                                   eos_module.cost_for_ncg, max_iter=50,
                                   converge_f=converged, args=row, debug=True, logger=vle_logger)
        for i in range(len(end_vars)):
            df.loc[df.seq == row.seq, eos_module.my_vars[i]] = end_vars[i]

    cv = eos_module.final_calculated_values(df)

    # cv = pd.read_excel(os.path.join(DEBUG_DIR, "vle", eos, "%s.xlsx" % compound), sheet_name="output")

    debug_dir = os.path.join(DEBUG_DIR, "vle", eos)
    write_excel(True, debug_dir, [compound], ["input", "output"], [df, cv])

    standardize_input(compound, eos=eos, df=cv, columns=eos_module.output)
    vle_logger.info("Completed VLE tuning for compound = %s | eos = %s" % (compound, eos))


def standardize_input(compound, eos, df, columns):
    for column in columns:
        new_df = standardize(df.copy(deep=True), column, compound, eos)
        filename = os.path.join(CSV_DATA, "kij", compound)
        write_csv(True, save_to_dir=filename, filename=[eos, column], df=new_df)


if __name__ == "__main__":
    user_args = read_options()
    logger = getLogger(user_args.compound, os.path.join(LOG_DIR, "vle", user_args.eos))
    try:
        run(user_args.eos, user_args.compound, user_args.volume, user_args.delh)
    except Exception as e:
        logger.exception('EXCEPTION IN MAIN == %s | %s!' % (user_args.compound, user_args.eos))
        raise Exception(e)
