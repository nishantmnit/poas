import autograd.numpy as np  # Thinly-wrapped version of Numpy
from autograd import grad as gd

from Scripts.pvt.core.constants import gasConstant
from Scripts.pvt.vle_old.generic import zl_zv, cost

my_vars = ["alpha_multiplier", "zc_multiplier"]
scales = [0.0001, 125., 175., 175., 0.0000001, 200.]

output = ["alpha_eos"]


def start_point(row):
    return [row.start_alpha_multiplier, row.start_zc_multiplier]


def pre_processing(df):
    return df


def var_range(row):
    return [row.alpha_multiplier_min, row.alpha_multiplier_max], [row.zc_multiplier_min, row.zc_multiplier_max]


def final_calculated_values(df):
    df = df.copy(deep=True)
    for index, row in df.iterrows():
        row = calculated_values(row)
        for col in row.keys():
            df.loc[df.seq == row.seq, col] = row[col]
    return df


def _calc(row):
    row["ziac"] = row.zc * row.zc_multiplier
    row["om_c"] = 1. - 3. * row.ziac
    row["a1_d"] = 2. - 3. * row.ziac
    row["a2_d"] = 3. * row.ziac ** 2
    row["a3_d"] = -1. * row.ziac ** 3
    row = _om_b(row)
    row["om_a"] = 3 * row.ziac ** 2 + 3 * (1 - 2 * row.ziac) * row.om_b + row.om_b ** 2 + 1 - 3 * row.ziac
    row["b"] = row.om_b * gasConstant * row.tc_k / (row.pc_mpa * 10 ** 6)
    row["c"] = row.om_c * gasConstant * row.tc_k / (row.pc_mpa * 10 ** 6)
    row["ac"] = (row.om_a * gasConstant ** 2 * row.tc_k ** 2 / (row.pc_mpa * 10 ** 6))
    row["ac_alpha"] = (row.ac * row.alpha_eos)
    row["u"] = 0 if row.b == 0 else ((row.c + row.b) / row.b)
    row["w"] = 0 if row.b == 0 else (-1 * (row.c / row.b))
    return row


def _om_b(row):
    row["Q_d"] = (3. * row.a2_d - row.a1_d ** 2) / 9.
    row["L_d"] = (9. * row.a1_d * row.a2_d - 27. * row.a3_d - 2. * row.a1_d ** 3) / 54.
    row["D_d"] = row.Q_d ** 3 + row.L_d ** 2
    row["S1_d"] = (-1. * (-1. * (row.L_d + np.sqrt(row.D_d))) ** (1. / 3.)) if (row.L_d + np.sqrt(
        row.D_d)) < 0 else (row.L_d + np.sqrt(row.D_d)) ** (1. / 3.)
    row["S2_d"] = (-1. * (-1. * (row.L_d - np.sqrt(row.D_d))) ** (1. / 3.)) if (row.L_d - np.sqrt(
        row.D_d)) < 0 else (row.L_d - np.sqrt(row.D_d)) ** (1. / 3.)
    row["theta"] = np.degrees(
        np.arccos(1. if np.abs(row.L_d / np.sqrt(-row.Q_d ** 3)) > 1 else row.L_d / np.sqrt(-row.Q_d ** 3)))
    row["Z1_ov"] = 0 if row.Q_d >= 0 else 2. * np.sqrt(-row.Q_d) * np.cos(
        np.radians(row.theta * 1. / 3. + 120)) - row.a1_d / 3.
    row["Z2_ov"] = 0 if row.Q_d >= 0 else 2. * np.sqrt(-row.Q_d) * np.cos(
        np.radians(row.theta * 1. / 3. + 240)) - row.a1_d / 3.
    row["Z3_ov"] = 0 if row.Q_d >= 0 else 2. * np.sqrt(-row.Q_d) * np.cos(
        np.radians(row.theta * 1. / 3.)) - row.a1_d / 3.
    row["Z1_ov_d"] = row.S1_d + row.S2_d - row.a1_d / 3.
    row["Z1_ov_dd"] = ((-1. * (-1. * row.L_d) ** 1. / 3.) if row.L_d < 0 else row.L_d ** (
                1. / 3.)) * 2. - row.a1_d / 3.
    row["Z2_ov_dd"] = (((-1. * (-1. * row.L_d) ** 1. / 3.) if row.L_d < 0 else row.L_d ** (
                1. / 3.)) + row.a1_d / 3.) * -1
    row["Z3_ov_dd"] = row.Z2_ov_dd
    row["om_b1"] = _min([row.Z1_ov, row.Z2_ov, row.Z3_ov])
    row["om_b2"] = _min([row.Z1_ov_dd, row.Z2_ov_dd, row.Z3_ov_dd])
    row["om_b"] = row.om_b1 if row.D_d < 0 else (row.Z1_ov_d if row.D_d > 0 else row.om_b2)
    return row


def _min(data):
    data = [x for x in data if x > 0]
    if len(data) > 0:
        return min(data)
    return 0


def _max(data):
    data = [x for x in data if x > 0]
    if len(data) > 0:
        return max(data)
    return 0


def calculated_values(row_o):
    row = row_o.copy(deep=True)
    row["alpha_eos"] = row.alpha_pr * row.alpha_multiplier
    row["diff_alpha_wrt_temp"] = row.alpha_eos * (((2. * row.alpha_m - 2.) / (
            row.temperature / row.tc_k) + row.alpha_l * (-2 * row.alpha_m) * (row.temperature / row.tc_k) ** (
                                                           2 * row.alpha_m - 1)) / row.tc_k)
    row = _calc(row)
    row = zl_zv(row)
    row = cost(row, scales)
    return row


def objective(row_o, iter_vars):
    iter_row = row_o.copy(deep=True)
    for i in range(len(iter_vars)):
        iter_row[my_vars[i]] = iter_vars[i]
    return calculated_values(iter_row).total_objective


def cost_for_ncg(iter_vars, row_o):
    iter_row = row_o.copy(deep=True)
    obj_cost = objective(iter_row, iter_vars)
    grad_fct = gd(objective, 1)
    derivative = np.array([i.item(0) for i in grad_fct(iter_row, iter_vars)])
    return obj_cost, derivative

