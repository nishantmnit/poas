import numpy as np
import pandas as pd
from Lib.Utils import getLogger

from Scripts.pvt.core.optimizers.poas_interpolator import monotone
from Scripts.pvt.core.optimizers.poas_interpolator import power, exponential, calc_extrapolated_point
from Scripts.pvt.settings import LOG_DIR

logger = getLogger(qualname='vle standardize')

standard_temperatures = [180, 200, 220, 240, 260, 280, 298.15, 300, 320, 340, 360, 380, 400, 420, 440, 460, 480, 500,
                         520, 540, 560, 580, 600, 620, 640, 660, 680, 700, 720, 740, 760, 780, 800, 850, 900, 950, 1000,
                         1050, 1100, 1150, 1200, 1250, 1300, 1350, 1400, 1450, 1500, 1550, 1600, 1650, 1700, 1750, 1800,
                         1850, 1900, 1950, 2000, 2050, 2100, 2150, 2200, 2250, 2300, 2350, 2400, 2450, 2500]
standard_pressures = [300, 250, 200, 150, 125, 100, 90, 80, 70, 60, 50, 45, 40, 35, 30, 25, 20, 15, 10, 8, 6, 4, 3, 2,
                      1, 0.5, 0.10132501]


def standardize(df, column, compound, eos):
    logger.info("Standardizing vle data %s for temperatures and pressures." % column)
    df = df[["temperature", "pressure", column]]
    df.sort_values("temperature", inplace=True)
    df.reset_index(inplace=True)
    df.rename(columns={"temperature": "xr", column: "y"}, inplace=True)
    new_df = standardize_temperatures(df, column, compound)
    new_df = standardize_pressures(new_df)
    return new_df


def standardize_temperatures_3(df):
    dict_of_df = dict()
    for pressure in df.pressure.unique():
        logger.info("Executing for pressure = %s" % pressure)
        # part 1 - top rows till 500 temp
        df_subset = df.loc[(df.pressure == pressure) & (df.xr <= 300)]
        df_subset.reset_index(inplace=True)
        new_df = best_curve(df_subset)
        min_temp = df_subset.xr.min()

        # part 2 - bottom 5 rows
        df_subset = df.loc[df.pressure == pressure].tail(5)
        df_subset.reset_index(inplace=True)
        new_df1 = best_curve(df_subset)
        max_temp = df_subset.xr.max()

        # combine part and 2
        new_df.loc[new_df.xr > max_temp, "y_calc"] = new_df1.loc[new_df1.xr > max_temp, "y_calc"]

        # interpolate if pressure is within the available range
        x = df.loc[df.pressure == pressure].xr.tolist()
        y = df.loc[df.pressure == pressure].y.tolist()
        find_x = new_df.loc[(new_df.xr <= max_temp) & (new_df.xr >= min_temp), "xr"].tolist()
        new_df.loc[(new_df.xr <= max_temp) & (new_df.xr >= min_temp), "y_calc"] = monotone(x, y, find_x)
        dict_of_df[pressure] = new_df
    new_df = pd.concat(dict_of_df.values())
    new_df.sort_values(["pressure", "xr"], inplace=True)
    new_df.reset_index(inplace=True, drop=True)
    return new_df


def exp_fit(df):
    df["slope"] = (df.y - df.y.shift(-1))/(df.xr - df.xr.shift(-1))
    df.loc[df.slope > 0, "slope"] = 0
    slopes = df.slope.values
    slopes = slopes[:-1]
    if np.all(slopes >= 0):
        slope = -0.0001
    elif len(slopes[slopes < 0]) == 1:
        slope = slopes[slopes < 0][0]*0.9
    else:
        slope = slopes[slopes < 0].sum()/len(slopes[slopes < 0])

    exp_b = (1./df.y.head(1)*slope).values[0]
    exp_a = (np.log(df.y.head(1)) - exp_b * df.xr.head(1)).values[0]

    new_df = pd.DataFrame(columns=["xr"], data=standard_temperatures)
    new_df["y_calc"] = np.exp(exp_a + exp_b * new_df.xr)
    return new_df


def power_fit(df):
    df["slope"] = (df.y - df.y.shift(-1)) / (df.xr - df.xr.shift(-1))
    df.loc[df.slope > 0, "slope"] = 0
    slopes = df.slope.values
    slopes = slopes[:-1]
    if np.all(slopes >= 0):
        slope = -0.0001
    elif len(slopes[slopes < 0]) == 1:
        slope = slopes[slopes < 0][0] * 0.9
    else:
        slope = slopes[slopes < 0].sum() / len(slopes[slopes < 0])

    power_b = (1. / df.y.tail(1) * slope * df.xr.tail(1)).values[0]
    power_a = (np.log(df.y.tail(1)) - power_b * np.log(df.xr.tail(1))).values[0]

    new_df = pd.DataFrame(columns=["xr"], data=standard_temperatures)
    new_df["y_calc"] = np.exp(power_a + power_b * np.log(new_df.xr))
    return new_df


def standardize_temperatures_4(df):
    dict_of_df = dict()
    for pressure in df.pressure.unique():
        logger.info("Executing for pressure = %s" % pressure)
        # part 1 - top 3 rows
        df_subset = df.loc[df.pressure == pressure].head(4)
        df_subset.reset_index(inplace=True)
        new_df = exp_fit(df_subset)
        min_temp = df_subset.xr.min()

        # part 2 - bottom 3 rows
        df_subset = df.loc[df.pressure == pressure].tail(4)
        df_subset.reset_index(inplace=True)
        new_df1 = power_fit(df_subset)
        max_temp = df_subset.xr.max()

        # combine part and 2
        new_df.loc[new_df.xr > df_subset.xr.max(), "y_calc"] = new_df1.loc[new_df1.xr > df_subset.xr.max(), "y_calc"]

        # interpolate if pressure is within the available range
        x = df.loc[df.pressure == pressure].xr.tolist()
        y = df.loc[df.pressure == pressure].y.tolist()
        find_x = new_df.loc[(new_df.xr <= max_temp) & (new_df.xr >= min_temp), "xr"].tolist()
        new_df.loc[(new_df.xr <= max_temp) & (new_df.xr >= min_temp), "y_calc"] = monotone(x, y, find_x)
        new_df["pressure"] = pressure
        dict_of_df[pressure] = new_df
    new_df = pd.concat(dict_of_df.values())
    new_df.sort_values(["pressure", "xr"], inplace=True)
    new_df.reset_index(inplace=True, drop=True)
    return new_df


def standardize_temperatures(df, column, compound):
    if compound in ["NITROGEN"] and column == "alpha_eos":
        return standardize_temperatures_3(df)
    elif compound in ["HYDROGEN"] and column == "alpha_eos":
        return standardize_temperatures_2(df)
    elif column == "alpha_eos":
        return standardize_temperatures_1(df)
    else:
        return standardize_temperatures_4(df)


def standardize_temperatures_1(df):
    dict_of_df = dict()
    for pressure in df.pressure.unique():
        logger.info("Executing for pressure = %s" % pressure)
        df_subset = df.loc[(df.pressure == pressure) & (df.xr <= 500)]
        df_subset.reset_index(inplace=True)
        new_df = best_curve(df_subset)
        dict_of_df[pressure] = new_df
    new_df = pd.concat(dict_of_df.values())
    new_df.sort_values(["pressure", "xr"], inplace=True)
    new_df.reset_index(inplace=True, drop=True)
    return new_df


def standardize_temperatures_2(df):
    dict_of_df = dict()
    for pressure in df.pressure.unique():
        logger.info("Executing for pressure = %s" % pressure)
        df_subset = df.loc[df.pressure == pressure].tail(5)
        df_subset.reset_index(inplace=True)
        # extrapolate if pressure within available range
        new_df = best_curve(df_subset)
        # interpolate if pressure is outside the available range
        x = df.loc[df.pressure == pressure].xr.tolist()
        y = df.loc[df.pressure == pressure].y.tolist()
        find_x = new_df.loc[new_df.xr <= df_subset.xr.max(), "xr"].tolist()
        new_df.loc[new_df.xr <= df_subset.xr.max(), "y_calc"] = monotone(x, y, find_x)
        dict_of_df[pressure] = new_df
    new_df = pd.concat(dict_of_df.values())
    new_df.sort_values(["pressure", "xr"], inplace=True)
    new_df.reset_index(inplace=True, drop=True)
    return new_df


def standardize_pressures(df):
    new_df = pd.DataFrame(index=standard_pressures, columns=standard_temperatures)
    x = list(df.pressure.unique())
    for i in range(len(standard_temperatures)):
        temperature = standard_temperatures[i]
        y = df.loc[df.xr == temperature, "y_calc"].tolist()
        if temperature == standard_temperatures[0]:
            a_log = (y[-1] - y[-2]) / (np.log(x[-1]) - np.log(x[-2]))
            b_log = y[-2] - a_log * np.log(x[-2])
            b_power = (np.log(y[-1]) - np.log(y[-2])) / (np.log(x[-1]) - np.log(x[-2]))
            a_power = np.exp(np.log(y[-2]) - b_power * np.log(x[-2]))
            find_pressures = new_df.loc[(new_df.index > max(x)) | (new_df.index < min(x))].index.tolist()
            if y[-1] > y[-2]:
                new_df.loc[(new_df.index > max(x)) | (new_df.index < min(x)), temperature] = (
                        a_log * np.log(find_pressures) + b_log)
            else:
                new_df.loc[
                    (new_df.index > max(x)) | (
                            new_df.index < min(x)), temperature] = a_power * find_pressures ** b_power
        else:
            y_prev = df.loc[df.xr == standard_temperatures[i - 1], "y_calc"].tolist()
            multiplier = y[-1] / y_prev[-1]
            new_df.loc[(new_df.index > max(x)) | (new_df.index < min(x)), temperature] = new_df.loc[
                                                                                             (new_df.index > max(x)) | (
                                                                                                     new_df.index < min(
                                                                                                 x)),
                                                                                             standard_temperatures[
                                                                                                 i - 1]] * multiplier

        find_xr = new_df.loc[(new_df.index <= max(x)) & (new_df.index >= min(x))].index.tolist()
        new_df.loc[(new_df.index <= max(x)) & (new_df.index >= min(x)), temperature] = monotone(x, y, find_xr)
    return new_df


def error(df):
    df["error"] = np.abs((df.y - df.y_calc) / df.y)
    return df.error.sum()


def curve_fit(df_o, curve):
    df = df_o.copy(deep=True)
    coefficients = power(df) if curve == "power" else exponential(df)
    df["y_calc"] = calc_extrapolated_point(curve, coefficients, df.xr.values)
    offset = df.loc[df.xr == df.xr.min(), "y"].values[0] / \
             df.loc[df.xr == df.xr.min(), "y_calc"].values[0]
    df["y_calc"] = df.y_calc * offset
    power_error = error(df)
    logger.info(
        "curve == %s | error = %s | offset =%s | coefficients = %s" % (curve, power_error, offset, coefficients))
    return coefficients, power_error, offset


def best_curve(df):
    power_coefficients, power_error, power_offset = curve_fit(df, "power")
    exp_coefficients, exp_error, exp_offset = curve_fit(df, "exponential")
    new_df = pd.DataFrame(columns=["xr"], data=standard_temperatures)
    if power_error < exp_error:
        new_df["y_calc"] = calc_extrapolated_point("power", power_coefficients, new_df.xr.values)
        new_df["y_calc"] = new_df.y_calc * power_offset
    else:
        new_df["y_calc"] = calc_extrapolated_point("exponential", exp_coefficients, new_df.xr.values)
        new_df["y_calc"] = new_df.y_calc * exp_offset
    new_df["pressure"] = df.pressure.unique()[0]
    return new_df
