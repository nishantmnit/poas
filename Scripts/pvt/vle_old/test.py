import os
import socket
from itertools import product
from multiprocessing import Pool

import Scripts.pvt.settings as settings
from Scripts.pvt.core.send_email import send_email


def run_process_vle(args):
    eos, compound = args[0], args[1]
    print("starting eos = %s | compound = %s" % (eos, compound))
    vle_main = os.path.join(settings.PVT_DIR, "vle", "vle.py")
    cmd = "python %s --eos '%s' --compound '%s'" % (vle_main, eos, compound)
    if eos == "poas_4":
        cmd += "--volume --delh"
    os.system(cmd)
    print("completed eos = %s | compound = %s" % (eos, compound))


def run_test(all_compounds, all_eos):
    send_email(subject="POAS VLE TEST ALERT", body="VLE Test has started on %s" % socket.gethostname())
    cartesian = [i for i in product(*[all_eos, all_compounds])]
    num = 10
    if len(cartesian) < 10:
        num = len(cartesian)

    pool = Pool(processes=num)
    pool.map(run_process_vle, cartesian)
    send_email(subject="POAS VLE TEST ALERT", body="VLE Test has completed on %s" % socket.gethostname())


if __name__ == "__main__":
    all_test_compounds = ["CARBON DIOXIDE", "ETHANE", "HYDROGEN SULFIDE", "HYDROGEN", "ISOBUTANE", "METHANE",
                          "N-BUTANE", "NITROGEN", "PROPANE", "WATER"]
    all_test_eos = ["pr", "srk", "generic_poas_a", "poas_4", "als", "poas_4a", "sw_poas", "pt"]
    run_test(all_test_compounds, all_test_eos)
    # usage
    # nohup python test.py > vle_test.out &
