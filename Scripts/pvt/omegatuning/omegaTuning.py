from importlib import import_module

from Scripts.pvt.core.storage.excel import write_excel
from Scripts.pvt.core.storage.mongo import write


class omegaTuning(object):
    def __init__(self, parent):
        self.__dict__ = parent.__dict__.copy()

    def runOmegaTuningAnalysis(self, temp):
        for eos in self.getEOS():
            self.runOmegaTuning(eos, temp)

    def getEOS(self):
        validEOS = self.validEOS
        if self.args.eos == "all":
            for eos in validEOS:
                yield eos
        else:
            yield self.args.eos

    def runOmegaTuning(self, e, temp):
        eos_omega = import_module("omegatuning.%s" % (e))
        om = getattr(eos_omega, "%s" % (e))(self)
        om.optimize(temp)
        write_excel(self.args.debug, self.debugDir, ["omegaTuning_eos", e, temp],
                    ["observed", "calculated"], [om.ov, om.cv])
        write(om.cv, "MBP", self.args.collection, logger=self.logger, Property="MCOutput", Identifier="OmegaTuning",
              Eos=e, Temperature=temp)
