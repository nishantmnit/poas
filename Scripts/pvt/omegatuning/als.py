import pandas as pd
from Scripts.pvt.core.optimizers.ncg import *

from Scripts.pvt.core.optimizers.poas_interpolator import monotone
from Scripts.pvt.core.storage.fetch_most import cm_data, eosbp_data, omega_data

gasConstant = 8.314472
pressure = 0.101325


class als(object):
    def __init__(self, parent):
        self.__dict__ = parent.__dict__.copy()

    def optimize(self, temperature):
        self.temperature = temperature
        self._setNVar()
        ov = ObservedValues(self)
        for index, row in ov.df.iterrows():
            self.logger.info("well = %s | EOS = %s | Omega tuning for scn = %s | component = %s" % (
            self.args.collection, "als", row.name, row.component))
            self.ncg = ncg()
            vars = self.ncg.minimize(self._sp(row), self._range(row), self._cost_for_ncg, self.logger, max_iter=200,
                                     debug=self.args.debug, converge_f=self._converged, args=(row))
            for i in range(len(vars)):
                ov.df.loc[ov.df.seq == row.seq, self._nvar[i]] = vars[i]
        cv = CalculatedValues(self, ov.df, normalize=True)
        self.ov = ov.df
        self.cv = cv.df

    def _setNVar(self):
        self._nvar = ["alpha_multiplier", "omega_multiplier"]

    def _cost_for_ncg(self, vars, row):
        cv, cost = self._cost(row, vars)
        gd = Gradients(self)
        grad = gd.gradient(row, cv)
        return cost, grad

    def _cost(self, row, vars):
        for i in range(len(vars)): row[self._nvar[i]] = vars[i]
        calc = CalculatedValues(self, row)
        cv = calc.row
        objs = ["hv_bp", "vb"]
        self._scales()
        costs = pd.Series()
        for i in range(len(objs)):
            costs[objs[i]] = 0 if row[objs[i]] == 0 else self.scales[i] * (
                        (row[objs[i]] - cv[objs[i]]) / row[objs[i]]) ** 2
        return cv, costs.sum() + self.scales[2] * max(0, cv.ph_diff - 10 ** -7) ** 2

    def _scales(self):
        if self.temperature == 0.7:
            self.scales = [1., 1., 300.]

    def _range(self, row):
        return [[row.alpha_multiplier_min, row.alpha_multiplier_max],
                [row.omega_multiplier_min, row.omega_multiplier_max]]

    def _converged(self, debug, logger, iteration, grad):
        row = self.ncg.curr_args
        cv, cost = self._cost(row, self.ncg.curr_vars)
        cd = cv.ph_diff
        if iteration < 3:
            return False
        elif cd <= 10 ** -4 and iteration <= 50:
            self.logger.info("Converged by rule 1 - %s" % (cd))
            return True
        elif cd <= 10 ** -3 and iteration > 50 and iteration <= 75:
            self.logger.info("Converged by rule 2 - %s" % (cd))
            return True
        elif cd <= 10 ** -2 and iteration > 75 and iteration <= 100:
            self.logger.info("Converged by rule 3 - %s" % (cd))
            return True
        elif cd <= 10 ** -1 and iteration > 100:
            self.logger.info("Converged by rule 4 - %s" % (cd))
            return True
        if iteration > 5 and np.all(grad - self.ncg.pgrad == 0):
            self.logger.info("Converged by rule 5 - %s" % (cd))
            return True
        return False

    def _sp(self, row):
        return [round(row.expected_alpha, 8), round(row.omega_multiplier, 8)]


class Gradients(object):
    def __init__(self, parent):
        self.__dict__ = parent.__dict__.copy()

    def gradient(self, ov, cv):
        return self._grad(ov, cv)

    def _grad(self, ov, cv):
        diff_alpha_wrt1 = ov.alpha_pr
        diff_alpha_wrt2 = 0

        diff_diff_alpha_wrt1 = ov.alpha_pr * (((2 * ov.alpha_m - 2) / (ov.temperature / ov.tc_k) + ov.alpha_l * (
                    -2 * ov.alpha_m) * (ov.temperature / ov.tc_k) ** (2 * ov.alpha_m - 1)) / ov.tc_k)
        diff_diff_alpha_wrt2 = 0

        diff_omega_wrt1 = 0.
        diff_omega_wrt2 = ov.omega_final

        diff_om_b_wrt1 = 0
        diff_om_b_wrt2 = 0 if (0.08974 - 0.03452 * cv.omega_final + 0.0033 * cv.omega_final ** 2) < 0.001 < 0.001 else (
                    -0.03452 * diff_omega_wrt2 + 0.0033 * 2 * diff_omega_wrt2 * cv.omega_final)

        diff_om_c_wrt1 = 0
        diff_om_c_wrt2 = 0 if (
                                          0.03686 + 0.00405 * cv.omega_final - 0.01073 * cv.omega_final ** 2 + 0.00157 * cv.omega_final ** 3) < 0.001 else (
                    0.00405 * diff_omega_wrt2 - 0.01073 * 2 * diff_omega_wrt2 * cv.omega_final + 0.00157 * 3 * diff_omega_wrt2 * cv.omega_final ** 2)

        diff_om_d_wrt1 = 0
        diff_om_d_wrt2 = 0 if (
                                          0.154 + 0.14122 * cv.omega_final - 0.00272 * cv.omega_final ** 2 - 0.00484 * cv.omega_final ** 3) < 0.001 else (
                    0.14122 * diff_omega_wrt2 - 0.00272 * 2 * diff_omega_wrt2 * cv.omega_final - 0.00484 * 3 * diff_omega_wrt2 * cv.omega_final ** 2)

        diff_om_a_wrt1 = 0
        diff_om_a_wrt2 = 0 if (
                                          0.44869 + 0.04024 * cv.omega_final + 0.01111 * cv.omega_final ** 2 - 0.00576 * cv.omega_final ** 3) < 0.001 else (
                    0.04024 * diff_omega_wrt2 + 0.01111 * 2 * diff_omega_wrt2 * cv.omega_final - 0.00576 * 3 * diff_omega_wrt2 * cv.omega_final ** 2)

        diff_b_wrt1 = diff_om_b_wrt1 * gasConstant * ov.tc_k / (ov.pc_mpa * 10 ** 6)
        diff_b_wrt2 = diff_om_b_wrt2 * gasConstant * ov.tc_k / (ov.pc_mpa * 10 ** 6)

        diff_c_wrt1 = diff_om_c_wrt1 * gasConstant * ov.tc_k / (ov.pc_mpa * 10 ** 6)
        diff_c_wrt2 = diff_om_c_wrt2 * gasConstant * ov.tc_k / (ov.pc_mpa * 10 ** 6)

        diff_d_wrt1 = diff_om_d_wrt1 * gasConstant * ov.tc_k / (ov.pc_mpa * 10 ** 6)
        diff_d_wrt2 = diff_om_d_wrt2 * gasConstant * ov.tc_k / (ov.pc_mpa * 10 ** 6)

        diff_ac_wrt1 = diff_om_a_wrt1 * gasConstant ** 2 * ov.tc_k ** 2 / (ov.pc_mpa * 10 ** 6)
        diff_ac_wrt2 = diff_om_a_wrt2 * gasConstant ** 2 * ov.tc_k ** 2 / (ov.pc_mpa * 10 ** 6)

        diff_ac_alpha_wrt1 = (cv.ac * diff_alpha_wrt1 + diff_ac_wrt1 * cv.alpha_eos)
        diff_ac_alpha_wrt2 = (cv.ac * diff_alpha_wrt2 + diff_ac_wrt2 * cv.alpha_eos)

        diff_u_wrt1 = 0. if cv.b == 0 else (
                    ((diff_d_wrt1 - diff_c_wrt1) * cv.b - (cv.d - cv.c) * diff_b_wrt1) / cv.b ** 2)
        diff_u_wrt2 = 0. if cv.b == 0 else (
                    ((diff_d_wrt2 - diff_c_wrt2) * cv.b - (cv.d - cv.c) * diff_b_wrt2) / cv.b ** 2)

        diff_w_wrt1 = 0. if cv.b == 0 else (-1. * ((
                                                               diff_d_wrt1 * cv.c + cv.d * diff_c_wrt1) * cv.b ** 2 - cv.d * cv.c * 2. * diff_b_wrt1 * cv.b) / cv.b ** 4)
        diff_w_wrt2 = 0. if cv.b == 0 else (-1. * ((
                                                               diff_d_wrt2 * cv.c + cv.d * diff_c_wrt2) * cv.b ** 2 - cv.d * cv.c * 2. * diff_b_wrt2 * cv.b) / cv.b ** 4)

        diff_r2_wrt1 = 0
        diff_r2_wrt2 = 0 if (cv.u ** 2 - 4 * cv.w) <= 0 else (diff_u_wrt2 + 0.5 * (
                    2 * cv.u * diff_u_wrt2 - 4 * diff_w_wrt2) * (cv.u ** 2 - 4 * cv.w) ** -0.5) / 2

        diff_r1_wrt1 = 0
        diff_r1_wrt2 = 0 if (cv.u ** 2 - 4 * cv.w) <= 0 else (diff_u_wrt2 - 0.5 * (
                    2 * cv.u * diff_u_wrt2 - 4 * diff_w_wrt2) * (cv.u ** 2 - 4 * cv.w) ** -0.5) / 2

        diff_A_wrt1 = diff_ac_alpha_wrt1 * ov.vapour_pressure * 10 ** 6 / (gasConstant ** 2 * ov.temperature ** 2)
        diff_A_wrt2 = diff_ac_alpha_wrt2 * ov.vapour_pressure * 10 ** 6 / (gasConstant ** 2 * ov.temperature ** 2)

        diff_B_wrt1 = 0
        diff_B_wrt2 = diff_b_wrt2 * ov.vapour_pressure * 10 ** 6 / (gasConstant * ov.temperature)

        diff_a1_wrt1 = 0
        diff_a1_wrt2 = ((cv.u * diff_B_wrt2 + diff_u_wrt2 * cv.B) - diff_B_wrt2)

        diff_a2_wrt1 = diff_A_wrt1 + (cv.w * 2 * cv.B * diff_B_wrt1 + diff_w_wrt1 * cv.B ** 2) - (
                    cv.u * diff_B_wrt1 + diff_u_wrt1 * cv.B) - (cv.u * 2 * cv.B * diff_B_wrt1 + diff_u_wrt1 * cv.B ** 2)
        diff_a2_wrt2 = diff_A_wrt2 + (cv.w * 2 * cv.B * diff_B_wrt2 + diff_w_wrt2 * cv.B ** 2) - (
                    cv.u * diff_B_wrt2 + diff_u_wrt2 * cv.B) - (cv.u * 2 * cv.B * diff_B_wrt2 + diff_u_wrt2 * cv.B ** 2)

        diff_a3_wrt1 = -1 * ((cv.A * diff_B_wrt1 + diff_A_wrt1 * cv.B) + (
                    cv.w * 2 * cv.B * diff_B_wrt1 + diff_w_wrt1 * cv.B ** 2) + (
                                         cv.w * 3 * cv.B ** 2 * diff_B_wrt1 + diff_w_wrt1 * cv.B ** 3))
        diff_a3_wrt2 = -1 * ((cv.A * diff_B_wrt2 + diff_A_wrt2 * cv.B) + (
                    cv.w * 2 * cv.B * diff_B_wrt2 + diff_w_wrt2 * cv.B ** 2) + (
                                         cv.w * 3 * cv.B ** 2 * diff_B_wrt2 + diff_w_wrt2 * cv.B ** 3))

        diff_Q_wrt1 = (3 * diff_a2_wrt1 - 2 * cv.a1 * diff_a1_wrt1) / 9.
        diff_Q_wrt2 = (3 * diff_a2_wrt2 - 2 * cv.a1 * diff_a1_wrt2) / 9.

        diff_L_wrt1 = (9 * (
                    cv.a1 * diff_a2_wrt1 + diff_a1_wrt1 * cv.a2) - 27 * diff_a3_wrt1 - 2 * 3 * diff_a1_wrt1 * cv.a1 ** 2) / 54.
        diff_L_wrt2 = (9 * (
                    cv.a1 * diff_a2_wrt2 + diff_a1_wrt2 * cv.a2) - 27 * diff_a3_wrt2 - 2 * 3 * diff_a1_wrt2 * cv.a1 ** 2) / 54.

        diff_D_wrt1 = 3 * diff_Q_wrt1 * cv.Q ** 2 + 2 * diff_L_wrt1 * cv.L
        diff_D_wrt2 = 3 * diff_Q_wrt2 * cv.Q ** 2 + 2 * diff_L_wrt2 * cv.L

        diff_S1_wrt1 = 0 if cv.L + np.sqrt(cv.D) == 0 else ((-1 * (1. / 3.) * (
                    -1 * (diff_L_wrt1 + 0.5 * diff_D_wrt1 / np.sqrt(cv.D))) * (-1 * (cv.L + np.sqrt(cv.D))) ** (
                                                                         1. / 3. - 1)) if (cv.L + np.sqrt(
            cv.D)) < 0 else ((1. / 3.) * (diff_L_wrt1 + 0.5 * diff_D_wrt1 / np.sqrt(cv.D)) * (cv.L + np.sqrt(cv.D)) ** (
                    1. / 3. - 1)))
        diff_S1_wrt2 = 0 if cv.L + np.sqrt(cv.D) == 0 else ((-1 * (1. / 3.) * (
                    -1 * (diff_L_wrt2 + 0.5 * diff_D_wrt2 / np.sqrt(cv.D))) * (-1 * (cv.L + np.sqrt(cv.D))) ** (
                                                                         1. / 3. - 1)) if (cv.L + np.sqrt(
            cv.D)) < 0 else ((1. / 3.) * (diff_L_wrt2 + 0.5 * diff_D_wrt2 / np.sqrt(cv.D)) * (cv.L + np.sqrt(cv.D)) ** (
                    1. / 3. - 1)))

        diff_S2_wrt1 = 0 if cv.L - np.sqrt(cv.D) == 0 else ((-1 * (1. / 3.) * (
                    -1 * (diff_L_wrt1 - 0.5 * diff_D_wrt1 / np.sqrt(cv.D))) * (-1 * (cv.L - np.sqrt(cv.D))) ** (
                                                                         1. / 3. - 1)) if (cv.L - np.sqrt(
            cv.D)) < 0 else ((1. / 3.) * (diff_L_wrt1 - 0.5 * diff_D_wrt1 / np.sqrt(cv.D)) * (cv.L - np.sqrt(cv.D)) ** (
                    1. / 3. - 1)))
        diff_S2_wrt2 = 0 if cv.L - np.sqrt(cv.D) == 0 else ((-1 * (1. / 3.) * (
                    -1 * (diff_L_wrt2 - 0.5 * diff_D_wrt2 / np.sqrt(cv.D))) * (-1 * (cv.L - np.sqrt(cv.D))) ** (
                                                                         1. / 3. - 1)) if (cv.L - np.sqrt(
            cv.D)) < 0 else ((1. / 3.) * (diff_L_wrt2 - 0.5 * diff_D_wrt2 / np.sqrt(cv.D)) * (cv.L - np.sqrt(cv.D)) ** (
                    1. / 3. - 1)))

        diff_Z1_d_wrt1 = diff_S1_wrt1 + diff_S2_wrt1 - diff_a1_wrt1 / 3
        diff_Z1_d_wrt2 = diff_S1_wrt2 + diff_S2_wrt2 - diff_a1_wrt2 / 3

        diff_Z1_dd_wrt1 = ((-1 * (1. / 3.) * (-1 * diff_L_wrt1) * (-1 * cv.L) ** (1. / 3. - 1)) if cv.L < 0 else (
                    1. / 3. * diff_L_wrt1 * cv.L ** (1. / 3. - 1))) * 2 - diff_a1_wrt1 / 3
        diff_Z1_dd_wrt2 = ((-1 * (1. / 3.) * (-1 * diff_L_wrt2) * (-1 * cv.L) ** (1. / 3. - 1)) if cv.L < 0 else (
                    1. / 3. * diff_L_wrt2 * cv.L ** (1. / 3. - 1))) * 2 - diff_a1_wrt2 / 3

        diff_Z2_dd_wrt1 = (((-1 * (1. / 3.) * (-1 * diff_L_wrt1) * (-1 * cv.L) ** (1. / 3. - 1)) if cv.L < 0 else (
                    1. / 3. * diff_L_wrt1 * cv.L ** (1. / 3. - 1))) + diff_a1_wrt1 / 3) * -1.
        diff_Z2_dd_wrt2 = (((-1 * (1. / 3.) * (-1 * diff_L_wrt2) * (-1 * cv.L) ** (1. / 3. - 1)) if cv.L < 0 else (
                    1. / 3. * diff_L_wrt2 * cv.L ** (1. / 3. - 1))) + diff_a1_wrt2 / 3) * -1.

        diff_Z3_dd_wrt1 = diff_Z2_dd_wrt1
        diff_Z3_dd_wrt2 = diff_Z2_dd_wrt2

        diff_Z1_wrt1 = 0 if cv.D >= 0 or cv.Q >= 0 else (2 * (-0.5 * diff_Q_wrt1 / np.sqrt(-1 * cv.Q) * np.cos(
            1. / 3. * (np.arccos(1. if np.abs(cv.L / np.sqrt(-cv.Q ** 3)) > 1 else cv.L / np.sqrt(
                -cv.Q ** 3))) + 2 * np.pi / 3) + np.sqrt(-1 * cv.Q) * (-1) * (1. / 3. * -1 / np.sqrt(
            1 - (1. if np.abs(cv.L / np.sqrt(-cv.Q ** 3)) > 1 else cv.L / np.sqrt(-cv.Q ** 3)) ** 2)) * ((np.sqrt(
            -cv.Q ** 3) * diff_L_wrt1 - (0.5 * -1 * 3 * diff_Q_wrt1 * cv.Q ** 2) / np.sqrt(-cv.Q ** 3) * cv.L) / (
                                                                                                             -cv.Q ** 3)) * np.sin(
            1. / 3. * (np.arccos(1. if np.abs(cv.L / np.sqrt(-cv.Q ** 3)) > 1 else cv.L / np.sqrt(
                -cv.Q ** 3))) + 2 * np.pi / 3)) - diff_a1_wrt1 / 3)
        diff_Z1_wrt2 = 0 if cv.D >= 0 or cv.Q >= 0 else (2 * (-0.5 * diff_Q_wrt2 / np.sqrt(-1 * cv.Q) * np.cos(
            1. / 3. * (np.arccos(1. if np.abs(cv.L / np.sqrt(-cv.Q ** 3)) > 1 else cv.L / np.sqrt(
                -cv.Q ** 3))) + 2 * np.pi / 3) + np.sqrt(-1 * cv.Q) * (-1) * (1. / 3. * -1 / np.sqrt(
            1 - (1. if np.abs(cv.L / np.sqrt(-cv.Q ** 3)) > 1 else cv.L / np.sqrt(-cv.Q ** 3)) ** 2)) * ((np.sqrt(
            -cv.Q ** 3) * diff_L_wrt2 - (0.5 * -1 * 3 * diff_Q_wrt2 * cv.Q ** 2) / np.sqrt(-cv.Q ** 3) * cv.L) / (
                                                                                                             -cv.Q ** 3)) * np.sin(
            1. / 3. * (np.arccos(1. if np.abs(cv.L / np.sqrt(-cv.Q ** 3)) > 1 else cv.L / np.sqrt(
                -cv.Q ** 3))) + 2 * np.pi / 3)) - diff_a1_wrt2 / 3)

        diff_Z2_wrt1 = 0 if cv.D >= 0 or cv.Q >= 0 else (2 * (-0.5 * diff_Q_wrt1 / np.sqrt(-1 * cv.Q) * np.cos(
            1. / 3. * (np.arccos(1. if np.abs(cv.L / np.sqrt(-cv.Q ** 3)) > 1 else cv.L / np.sqrt(
                -cv.Q ** 3))) + 4 * np.pi / 3) + np.sqrt(-1 * cv.Q) * (-1) * (1. / 3. * -1 / np.sqrt(
            1 - (1. if np.abs(cv.L / np.sqrt(-cv.Q ** 3)) > 1 else cv.L / np.sqrt(-cv.Q ** 3)) ** 2)) * ((np.sqrt(
            -cv.Q ** 3) * diff_L_wrt1 - (0.5 * -1 * 3 * diff_Q_wrt1 * cv.Q ** 2) / np.sqrt(-cv.Q ** 3) * cv.L) / (
                                                                                                             -cv.Q ** 3)) * np.sin(
            1. / 3. * (np.arccos(1. if np.abs(cv.L / np.sqrt(-cv.Q ** 3)) > 1 else cv.L / np.sqrt(
                -cv.Q ** 3))) + 4 * np.pi / 3)) - diff_a1_wrt1 / 3)
        diff_Z2_wrt2 = 0 if cv.D >= 0 or cv.Q >= 0 else (2 * (-0.5 * diff_Q_wrt2 / np.sqrt(-1 * cv.Q) * np.cos(
            1. / 3. * (np.arccos(1. if np.abs(cv.L / np.sqrt(-cv.Q ** 3)) > 1 else cv.L / np.sqrt(
                -cv.Q ** 3))) + 4 * np.pi / 3) + np.sqrt(-1 * cv.Q) * (-1) * (1. / 3. * -1 / np.sqrt(
            1 - (1. if np.abs(cv.L / np.sqrt(-cv.Q ** 3)) > 1 else cv.L / np.sqrt(-cv.Q ** 3)) ** 2)) * ((np.sqrt(
            -cv.Q ** 3) * diff_L_wrt2 - (0.5 * -1 * 3 * diff_Q_wrt2 * cv.Q ** 2) / np.sqrt(-cv.Q ** 3) * cv.L) / (
                                                                                                             -cv.Q ** 3)) * np.sin(
            1. / 3. * (np.arccos(1. if np.abs(cv.L / np.sqrt(-cv.Q ** 3)) > 1 else cv.L / np.sqrt(
                -cv.Q ** 3))) + 4 * np.pi / 3)) - diff_a1_wrt2 / 3)

        diff_Z3_wrt1 = 0 if cv.D >= 0 or cv.Q >= 0 else (2 * (-0.5 * diff_Q_wrt1 / np.sqrt(-1 * cv.Q) * np.cos(
            1. / 3. * (
                np.arccos(1. if np.abs(cv.L / np.sqrt(-cv.Q ** 3)) > 1 else cv.L / np.sqrt(-cv.Q ** 3)))) + np.sqrt(
            -1 * cv.Q) * (-1) * (1. / 3. * -1 / np.sqrt(
            1 - (1. if np.abs(cv.L / np.sqrt(-cv.Q ** 3)) > 1 else cv.L / np.sqrt(-cv.Q ** 3)) ** 2)) * ((np.sqrt(
            -cv.Q ** 3) * diff_L_wrt1 - (0.5 * -1 * 3 * diff_Q_wrt1 * cv.Q ** 2) / np.sqrt(-cv.Q ** 3) * cv.L) / (
                                                                                                             -cv.Q ** 3)) * np.sin(
            1. / 3. * (np.arccos(
                1. if np.abs(cv.L / np.sqrt(-cv.Q ** 3)) > 1 else cv.L / np.sqrt(-cv.Q ** 3))))) - diff_a1_wrt1 / 3)
        diff_Z3_wrt2 = 0 if cv.D >= 0 or cv.Q >= 0 else (2 * (-0.5 * diff_Q_wrt2 / np.sqrt(-1 * cv.Q) * np.cos(
            1. / 3. * (
                np.arccos(1. if np.abs(cv.L / np.sqrt(-cv.Q ** 3)) > 1 else cv.L / np.sqrt(-cv.Q ** 3)))) + np.sqrt(
            -1 * cv.Q) * (-1) * (1. / 3. * -1 / np.sqrt(
            1 - (1. if np.abs(cv.L / np.sqrt(-cv.Q ** 3)) > 1 else cv.L / np.sqrt(-cv.Q ** 3)) ** 2)) * ((np.sqrt(
            -cv.Q ** 3) * diff_L_wrt2 - (0.5 * -1 * 3 * diff_Q_wrt2 * cv.Q ** 2) / np.sqrt(-cv.Q ** 3) * cv.L) / (
                                                                                                             -cv.Q ** 3)) * np.sin(
            1. / 3. * (np.arccos(
                1. if np.abs(cv.L / np.sqrt(-cv.Q ** 3)) > 1 else cv.L / np.sqrt(-cv.Q ** 3))))) - diff_a1_wrt2 / 3)

        diff_ZL_wrt1 = (
            diff_Z1_wrt1 if cv.ZL == cv.Z1 else (diff_Z2_wrt1 if cv.ZL == cv.Z2 else diff_Z3_wrt1)) if cv.D < 0 else ((
                                                                                                                          diff_Z1_dd_wrt1 if cv.ZL == cv.Z1_dd else (
                                                                                                                              diff_Z2_dd_wrt1 if cv.ZL == cv.Z2_dd else diff_Z3_dd_wrt1)) if cv.D == 0 else diff_Z1_d_wrt1)
        diff_ZL_wrt2 = (
            diff_Z1_wrt2 if cv.ZL == cv.Z1 else (diff_Z2_wrt2 if cv.ZL == cv.Z2 else diff_Z3_wrt2)) if cv.D < 0 else ((
                                                                                                                          diff_Z1_dd_wrt2 if cv.ZL == cv.Z1_dd else (
                                                                                                                              diff_Z2_dd_wrt2 if cv.ZL == cv.Z2_dd else diff_Z3_dd_wrt2)) if cv.D == 0 else diff_Z1_d_wrt2)

        diff_ZV_wrt1 = (
            diff_Z1_wrt1 if cv.ZV == cv.Z1 else (diff_Z2_wrt1 if cv.ZV == cv.Z2 else diff_Z3_wrt1)) if cv.D < 0 else ((
                                                                                                                          diff_Z1_dd_wrt1 if cv.ZV == cv.Z1_dd else (
                                                                                                                              diff_Z2_dd_wrt1 if cv.ZV == cv.Z2_dd else diff_Z3_dd_wrt1)) if cv.D == 0 else 0)
        diff_ZV_wrt2 = (
            diff_Z1_wrt2 if cv.ZV == cv.Z1 else (diff_Z2_wrt2 if cv.ZV == cv.Z2 else diff_Z3_wrt2)) if cv.D < 0 else ((
                                                                                                                          diff_Z1_dd_wrt2 if cv.ZV == cv.Z1_dd else (
                                                                                                                              diff_Z2_dd_wrt2 if cv.ZV == cv.Z2_dd else diff_Z3_dd_wrt2)) if cv.D == 0 else 0)

        diff_phl_wrt1 = 0 if (cv.ZL - cv.B) <= 0 or cv.B * (cv.r2 - cv.r1) == 0 or (cv.ZL + cv.r1 * cv.B) == 0 or (
                    cv.ZL + cv.r2 * cv.B) == 0 or (cv.ZL + cv.r2 * cv.B) * (cv.ZL + cv.r1 * cv.B) < 0 else (
                    (diff_ZL_wrt1) - (diff_ZL_wrt1 - diff_B_wrt1) / (cv.ZL - cv.B) - ((((cv.B * (
                        cv.r2 - cv.r1)) * diff_A_wrt1 - (diff_B_wrt1 * (cv.r2 - cv.r1) + cv.B * (
                        diff_r2_wrt1 - diff_r1_wrt1)) * cv.A) / (cv.B * (cv.r2 - cv.r1)) ** 2) * np.log(
                (cv.ZL + cv.r2 * cv.B) / (cv.ZL + cv.r1 * cv.B)) + (cv.A / (cv.B * (cv.r2 - cv.r1))) * 1 / ((
                                                                                                                        cv.ZL + cv.r2 * cv.B) / (
                                                                                                                        cv.ZL + cv.r1 * cv.B)) * (
                                                                                                  ((
                                                                                                               cv.ZL + cv.r1 * cv.B) * (
                                                                                                               diff_ZL_wrt1 + diff_r2_wrt1 * cv.B + cv.r2 * diff_B_wrt1) - (
                                                                                                               cv.ZL + cv.r2 * cv.B) * (
                                                                                                               diff_ZL_wrt1 + diff_r1_wrt1 * cv.B + cv.r1 * diff_B_wrt1)) / (
                                                                                                              cv.ZL + cv.r1 * cv.B) ** 2)))
        diff_phl_wrt2 = 0 if (cv.ZL - cv.B) <= 0 or cv.B * (cv.r2 - cv.r1) == 0 or (cv.ZL + cv.r1 * cv.B) == 0 or (
                    cv.ZL + cv.r2 * cv.B) == 0 or (cv.ZL + cv.r2 * cv.B) * (cv.ZL + cv.r1 * cv.B) < 0 else (
                    (diff_ZL_wrt2) - (diff_ZL_wrt2 - diff_B_wrt2) / (cv.ZL - cv.B) - ((((cv.B * (
                        cv.r2 - cv.r1)) * diff_A_wrt2 - (diff_B_wrt2 * (cv.r2 - cv.r1) + cv.B * (
                        diff_r2_wrt2 - diff_r1_wrt2)) * cv.A) / (cv.B * (cv.r2 - cv.r1)) ** 2) * np.log(
                (cv.ZL + cv.r2 * cv.B) / (cv.ZL + cv.r1 * cv.B)) + (cv.A / (cv.B * (cv.r2 - cv.r1))) * 1 / ((
                                                                                                                        cv.ZL + cv.r2 * cv.B) / (
                                                                                                                        cv.ZL + cv.r1 * cv.B)) * (
                                                                                                  ((
                                                                                                               cv.ZL + cv.r1 * cv.B) * (
                                                                                                               diff_ZL_wrt2 + diff_r2_wrt2 * cv.B + cv.r2 * diff_B_wrt2) - (
                                                                                                               cv.ZL + cv.r2 * cv.B) * (
                                                                                                               diff_ZL_wrt2 + diff_r1_wrt2 * cv.B + cv.r1 * diff_B_wrt2)) / (
                                                                                                              cv.ZL + cv.r1 * cv.B) ** 2)))

        diff_phv_wrt1 = 0 if (cv.ZV - cv.B) <= 0 or cv.B * (cv.r2 - cv.r1) == 0 or (cv.ZV + cv.r1 * cv.B) == 0 or (
                    cv.ZV + cv.r2 * cv.B) == 0 or (cv.ZV + cv.r2 * cv.B) * (cv.ZV + cv.r1 * cv.B) < 0 else (
                    (diff_ZV_wrt1) - (diff_ZV_wrt1 - diff_B_wrt1) / (cv.ZV - cv.B) - ((((cv.B * (
                        cv.r2 - cv.r1)) * diff_A_wrt1 - (diff_B_wrt1 * (cv.r2 - cv.r1) + cv.B * (
                        diff_r2_wrt1 - diff_r1_wrt1)) * cv.A) / (cv.B * (cv.r2 - cv.r1)) ** 2) * np.log(
                (cv.ZV + cv.r2 * cv.B) / (cv.ZV + cv.r1 * cv.B)) + (cv.A / (cv.B * (cv.r2 - cv.r1))) * 1 / ((
                                                                                                                        cv.ZV + cv.r2 * cv.B) / (
                                                                                                                        cv.ZV + cv.r1 * cv.B)) * (
                                                                                                  ((
                                                                                                               cv.ZV + cv.r1 * cv.B) * (
                                                                                                               diff_ZV_wrt1 + diff_r2_wrt1 * cv.B + cv.r2 * diff_B_wrt1) - (
                                                                                                               cv.ZV + cv.r2 * cv.B) * (
                                                                                                               diff_ZV_wrt1 + diff_r1_wrt1 * cv.B + cv.r1 * diff_B_wrt1)) / (
                                                                                                              cv.ZV + cv.r1 * cv.B) ** 2)))
        diff_phv_wrt2 = 0 if (cv.ZV - cv.B) <= 0 or cv.B * (cv.r2 - cv.r1) == 0 or (cv.ZV + cv.r1 * cv.B) == 0 or (
                    cv.ZV + cv.r2 * cv.B) == 0 or (cv.ZV + cv.r2 * cv.B) * (cv.ZV + cv.r1 * cv.B) < 0 else (
                    (diff_ZV_wrt2) - (diff_ZV_wrt2 - diff_B_wrt2) / (cv.ZV - cv.B) - ((((cv.B * (
                        cv.r2 - cv.r1)) * diff_A_wrt2 - (diff_B_wrt2 * (cv.r2 - cv.r1) + cv.B * (
                        diff_r2_wrt2 - diff_r1_wrt2)) * cv.A) / (cv.B * (cv.r2 - cv.r1)) ** 2) * np.log(
                (cv.ZV + cv.r2 * cv.B) / (cv.ZV + cv.r1 * cv.B)) + (cv.A / (cv.B * (cv.r2 - cv.r1))) * 1 / ((
                                                                                                                        cv.ZV + cv.r2 * cv.B) / (
                                                                                                                        cv.ZV + cv.r1 * cv.B)) * (
                                                                                                  ((
                                                                                                               cv.ZV + cv.r1 * cv.B) * (
                                                                                                               diff_ZV_wrt2 + diff_r2_wrt2 * cv.B + cv.r2 * diff_B_wrt2) - (
                                                                                                               cv.ZV + cv.r2 * cv.B) * (
                                                                                                               diff_ZV_wrt2 + diff_r1_wrt2 * cv.B + cv.r1 * diff_B_wrt2)) / (
                                                                                                              cv.ZV + cv.r1 * cv.B) ** 2)))

        diff_ph_diff_wrt1 = np.sign(cv.phl - cv.phv) * (diff_phl_wrt1 - diff_phv_wrt1)
        diff_ph_diff_wrt2 = np.sign(cv.phl - cv.phv) * (diff_phl_wrt2 - diff_phv_wrt2)

        diff_t1_wrt1 = gasConstant * ov.temperature * (diff_ZV_wrt1 - diff_ZL_wrt1)
        diff_t1_wrt2 = gasConstant * ov.temperature * (diff_ZV_wrt2 - diff_ZL_wrt2)

        diff_t2_wrt1 = 0 if cv.b == 0 or (cv.u ** 2 - 4 * cv.w) <= 0 else ((cv.b * np.sqrt(cv.u ** 2 - 4 * cv.w)) * (
                    diff_ac_wrt1 * (ov.temperature * cv.diff_alpha_wrt_temp - cv.alpha_eos) + cv.ac * (
                        ov.temperature * diff_diff_alpha_wrt1 - diff_alpha_wrt1)) - (diff_b_wrt1 * np.sqrt(
            cv.u ** 2 - 4 * cv.w) + cv.b * 0.5 * (2 * cv.u * diff_u_wrt1 - 4 * diff_w_wrt1) / np.sqrt(
            cv.u ** 2 - 4 * cv.w)) * (cv.ac * (ov.temperature * cv.diff_alpha_wrt_temp - cv.alpha_eos))) / (
                                                                                      cv.b * np.sqrt(
                                                                                  cv.u ** 2 - 4 * cv.w)) ** 2
        diff_t2_wrt2 = 0 if cv.b == 0 or (cv.u ** 2 - 4 * cv.w) <= 0 else ((cv.b * np.sqrt(cv.u ** 2 - 4 * cv.w)) * (
                    diff_ac_wrt2 * (ov.temperature * cv.diff_alpha_wrt_temp - cv.alpha_eos) + cv.ac * (
                        ov.temperature * diff_diff_alpha_wrt2 - diff_alpha_wrt2)) - (diff_b_wrt2 * np.sqrt(
            cv.u ** 2 - 4 * cv.w) + cv.b * 0.5 * (2 * cv.u * diff_u_wrt2 - 4 * diff_w_wrt2) / np.sqrt(
            cv.u ** 2 - 4 * cv.w)) * (cv.ac * (ov.temperature * cv.diff_alpha_wrt_temp - cv.alpha_eos))) / (
                                                                                      cv.b * np.sqrt(
                                                                                  cv.u ** 2 - 4 * cv.w)) ** 2

        diff_t3_wrt1 = 0 if (cv.ZV + cv.B * cv.r2) == 0 or (cv.ZV + cv.B * cv.r1) == 0 or (cv.ZV + cv.B * cv.r2) * (
                    cv.ZV + cv.B * cv.r1) < 0 else (((cv.ZV + cv.B * cv.r1) / (cv.ZV + cv.B * cv.r2)) * (
                    (cv.ZV + cv.B * cv.r1) * (diff_ZV_wrt1 + diff_B_wrt1 * cv.r2 + cv.B * diff_r2_wrt1) - (
                        diff_ZV_wrt1 + diff_B_wrt1 * cv.r1 + cv.B * diff_r1_wrt1) * (cv.ZV + cv.B * cv.r2)) / (
                                                                cv.ZV + cv.B * cv.r1) ** 2)
        diff_t3_wrt2 = 0 if (cv.ZV + cv.B * cv.r2) == 0 or (cv.ZV + cv.B * cv.r1) == 0 or (cv.ZV + cv.B * cv.r2) * (
                    cv.ZV + cv.B * cv.r1) < 0 else (((cv.ZV + cv.B * cv.r1) / (cv.ZV + cv.B * cv.r2)) * (
                    (cv.ZV + cv.B * cv.r1) * (diff_ZV_wrt2 + diff_B_wrt2 * cv.r2 + cv.B * diff_r2_wrt2) - (
                        diff_ZV_wrt2 + diff_B_wrt2 * cv.r1 + cv.B * diff_r1_wrt2) * (cv.ZV + cv.B * cv.r2)) / (
                                                                cv.ZV + cv.B * cv.r1) ** 2)

        diff_t4_wrt1 = 0 if (cv.ZL + cv.B * cv.r2) == 0 or (cv.ZL + cv.B * cv.r1) == 0 or (cv.ZL + cv.B * cv.r2) * (
                    cv.ZL + cv.B * cv.r1) < 0 else (((cv.ZL + cv.B * cv.r1) / (cv.ZL + cv.B * cv.r2)) * (
                    (cv.ZL + cv.B * cv.r1) * (diff_ZL_wrt1 + diff_B_wrt1 * cv.r2 + cv.B * diff_r2_wrt1) - (
                        diff_ZL_wrt1 + diff_B_wrt1 * cv.r1 + cv.B * diff_r1_wrt1) * (cv.ZL + cv.B * cv.r2)) / (
                                                                cv.ZL + cv.B * cv.r1) ** 2)
        diff_t4_wrt2 = 0 if (cv.ZL + cv.B * cv.r2) == 0 or (cv.ZL + cv.B * cv.r1) == 0 or (cv.ZL + cv.B * cv.r2) * (
                    cv.ZL + cv.B * cv.r1) < 0 else (((cv.ZL + cv.B * cv.r1) / (cv.ZL + cv.B * cv.r2)) * (
                    (cv.ZL + cv.B * cv.r1) * (diff_ZL_wrt2 + diff_B_wrt2 * cv.r2 + cv.B * diff_r2_wrt2) - (
                        diff_ZL_wrt2 + diff_B_wrt2 * cv.r1 + cv.B * diff_r1_wrt2) * (cv.ZL + cv.B * cv.r2)) / (
                                                                cv.ZL + cv.B * cv.r1) ** 2)

        diff_hv_bp_wrt1 = (diff_t1_wrt1 + diff_t2_wrt1 * (cv.t3 - cv.t4) + cv.t2 * (
                    diff_t3_wrt1 - diff_t4_wrt1)) / 1000.
        diff_hv_bp_wrt2 = (diff_t1_wrt2 + diff_t2_wrt2 * (cv.t3 - cv.t4) + cv.t2 * (
                    diff_t3_wrt2 - diff_t4_wrt2)) / 1000.

        diff_vb_wrt1 = diff_ZL_wrt1 * gasConstant * ov.temperature / ov.vapour_pressure
        diff_vb_wrt2 = diff_ZL_wrt2 * gasConstant * ov.temperature / ov.vapour_pressure

        diff_wrt1 = self.scales[0] * (2 * (ov.hv_bp - cv.hv_bp) / ov.hv_bp ** 2) * (-1.) * diff_hv_bp_wrt1 + \
                    self.scales[1] * (2 * (ov.vb - cv.vb) / ov.vb ** 2) * (-1) * diff_vb_wrt1 + self.scales[2] * 2 * (
                        max(0, (cv.ph_diff - 10 ** -7))) * (0 if (cv.ph_diff - 10 ** -7) <= 0 else diff_ph_diff_wrt1)
        diff_wrt2 = self.scales[0] * (2 * (ov.hv_bp - cv.hv_bp) / ov.hv_bp ** 2) * (-1.) * diff_hv_bp_wrt2 + \
                    self.scales[1] * (2 * (ov.vb - cv.vb) / ov.vb ** 2) * (-1) * diff_vb_wrt2 + self.scales[2] * 2 * (
                        max(0, (cv.ph_diff - 10 ** -7))) * (0 if (cv.ph_diff - 10 ** -7) <= 0 else diff_ph_diff_wrt2)

        diff_wrt1 = (np.sign(diff_wrt1) * 10 ** 7) if np.abs(diff_wrt1) > 10 ** 7 else diff_wrt1
        diff_wrt2 = (np.sign(diff_wrt2) * 10 ** 7) if np.abs(diff_wrt2) > 10 ** 7 else diff_wrt2
        return np.array([diff_wrt1, diff_wrt2])


class CalculatedValues(object):
    def __init__(self, parent, df, normalize=False):
        self.__dict__ = parent.__dict__.copy()
        self._prepare(df, normalize)

    def _prepare(self, df, normalize):
        if not normalize:
            row = self._intermediate(df)
            self.row = row
        else:
            df = self._final(df)
            self.df = df

    def _final(self, df_o):
        df = df_o.copy(deep=True)
        for index, row in df.iterrows():
            row = self._intermediate(row)
            for col in row.keys():
                df.loc[df.seq == row.seq, col] = row[col]
        df = self._fixNegative(df, df_o)
        df.rename(columns={"hv_bp": "del_hv"}, inplace=True)
        return df

    def _fixNegative(self, df, ov):
        df["marker"] = df.apply(lambda row: 0 if row.ph_diff == 0 or row.ph_diff > 10 ** -2 else row.ph_diff, axis=1)
        x = df.loc[df.marker != 0, "seq"].tolist()
        for col in ["vb", "hv_bp"] + self._nvar:
            y = df.loc[df.marker != 0, col].tolist()
            df[col] = df.apply(lambda row: row[col] if row.marker != 0 else self._interpolate(x, y, row, col, ov),
                               axis=1)
        return df

    def _interpolate(self, x, y, row, col, ov):
        val_c = monotone(x, y, row.seq)[0]
        if col not in self._nvar:
            val_o = ov.loc[ov.seq == row.seq, col].values[0]
            if val_c > val_o * 1.05 or val_c < val_o * 0.95:
                val_c = val_o
        return val_c

    def _intermediate(self, row_o):
        row = row_o.copy(deep=True)
        row["alpha_eos"] = row.alpha_pr * row.alpha_multiplier
        row["diff_alpha_wrt_temp"] = row.alpha_eos * (((2. * row.alpha_m - 2.) / (
                    row.temperature / row.tc_k) + row.alpha_l * (-2 * row.alpha_m) * (row.temperature / row.tc_k) ** (
                                                                   2 * row.alpha_m - 1)) / row.tc_k)
        row = self._calc(row)
        row = self._zlzv(row)
        row = self._cost(row, row_o)
        return row

    def _calc(self, row):
        row["omega_final"] = row.omega_final * row.omega_multiplier
        row["om_b"] = 0.001 if (0.08974 - 0.03452 * row.omega_final + 0.0033 * row.omega_final ** 2) < 0.001 else (
                    0.08974 - 0.03452 * row.omega_final + 0.0033 * row.omega_final ** 2)
        row["om_c"] = 0.001 if (
                                           0.03686 + 0.00405 * row.omega_final - 0.01073 * row.omega_final ** 2 + 0.00157 * row.omega_final ** 3) < 0.001 else (
                    0.03686 + 0.00405 * row.omega_final - 0.01073 * row.omega_final ** 2 + 0.00157 * row.omega_final ** 3)
        row["om_d"] = 0.001 if (
                                           0.154 + 0.14122 * row.omega_final - 0.00272 * row.omega_final ** 2 - 0.00484 * row.omega_final ** 3) < 0.001 else (
                    0.154 + 0.14122 * row.omega_final - 0.00272 * row.omega_final ** 2 - 0.00484 * row.omega_final ** 3)
        row["om_a"] = 0.001 if (
                                           0.44869 + 0.04024 * row.omega_final + 0.01111 * row.omega_final ** 2 - 0.00576 * row.omega_final ** 3) < 0.001 else (
                    0.44869 + 0.04024 * row.omega_final + 0.01111 * row.omega_final ** 2 - 0.00576 * row.omega_final ** 3)
        row["b"] = row.om_b * gasConstant * row.tc_k / (row.pc_mpa * 10 ** 6)
        row["c"] = row.om_c * gasConstant * row.tc_k / (row.pc_mpa * 10 ** 6)
        row["d"] = row.om_d * gasConstant * row.tc_k / (row.pc_mpa * 10 ** 6)
        row["ac"] = (row.om_a * gasConstant ** 2 * row.tc_k ** 2 / (row.pc_mpa * 10 ** 6))
        row["ac_alpha"] = (row.ac * row.alpha_eos)
        row["u"] = 0. if row.b == 0 else ((row.d - row.c) / row.b)
        row["w"] = 0. if row.b == 0 else (-1. * row.d * row.c / row.b ** 2)
        return row

    def _cost(self, row, ov):
        row["t1"] = gasConstant * row.temperature * (row.ZV - row.ZL)
        row["t2"] = 10 ** 6 if ((row.b * np.sqrt(row.u ** 2 - 4 * row.w)) == 0 or (row.u ** 2 - 4 * row.w) < 0) else (
                    row.ac * (row.temperature * row.diff_alpha_wrt_temp - row.alpha_eos) / (
                        row.b * np.sqrt(row.u ** 2 - 4 * row.w)))
        if ((row.ZV + row.B * row.r2) == 0 or (row.ZV + row.B * row.r1) == 0 or (
                (row.ZV + row.B * row.r2) * (row.ZV + row.B * row.r1)) < 0):
            row["t3"] = -1.
        else:
            row["t3"] = (np.log((row.ZV + row.B * row.r2) / (row.ZV + row.B * row.r1)))
        if ((row.ZL + row.B * row.r2) == 0 or (row.ZL + row.B * row.r1) == 0 or (
                (row.ZL + row.B * row.r2) * (row.ZL + row.B * row.r1)) < 0):
            row["t4"] = -5.
        else:
            row["t4"] = (np.log((row.ZL + row.B * row.r2) / (row.ZL + row.B * row.r1)))
        row["hv_bp"] = ((row.t1 + row.t2 * (row.t3 - row.t4)) / 1000.)
        row["vb"] = row.ZL * gasConstant * row.temperature / row.vapour_pressure
        return row

    def _zlzv(self, row):
        row["r1"] = 0 if (row.u ** 2 - 4 * row.w) <= 0 else (row.u - np.sqrt(row.u ** 2 - 4 * row.w)) / 2
        row["r2"] = 0 if (row.u ** 2 - 4 * row.w) <= 0 else (row.u + np.sqrt(row.u ** 2 - 4 * row.w)) / 2
        row["A"] = row.ac_alpha * row.vapour_pressure * 10 ** 6 / (gasConstant ** 2 * row.temperature ** 2)
        row["B"] = (row.b * row.vapour_pressure * 10 ** 6 / (gasConstant * row.temperature))
        row["a1"] = (row.u * row.B - row.B - 1)
        row["a2"] = (row.A + row.w * row.B ** 2 - row.u * row.B - row.u * row.B ** 2)
        row["a3"] = -1. * (row.A * row.B + row.w * row.B ** 2 + row.w * row.B ** 3)
        row["Q"] = (3 * row.a2 - row.a1 ** 2) / 9
        row["L"] = (9 * row.a1 * row.a2 - 27 * row.a3 - 2 * row.a1 ** 3) / 54
        row["D"] = row.Q ** 3 + row.L ** 2
        row["S1"] = (-1. * (-1. * (row.L + np.sqrt(row.D))) ** (1. / 3.)) if (row.L + np.sqrt(row.D)) < 0 else (
                                                                                                                           row.L + np.sqrt(
                                                                                                                       row.D)) ** (
                                                                                                                           1. / 3.)
        row["S2"] = (-1. * (-1. * (row.L - np.sqrt(row.D))) ** (1. / 3.)) if (row.L - np.sqrt(row.D)) < 0 else (
                                                                                                                           row.L - np.sqrt(
                                                                                                                       row.D)) ** (
                                                                                                                           1. / 3.)
        row["Z1"] = 0 if row.Q >= 0 else 2. * np.sqrt(-row.Q) * np.cos(1. / 3. * (np.arccos(
            1. if np.abs(row.L / np.sqrt(-row.Q ** 3)) > 1 else row.L / np.sqrt(
                -row.Q ** 3))) + 2 * np.pi / 3.) - row.a1 / 3.
        row["Z2"] = 0 if row.Q >= 0 else 2. * np.sqrt(-row.Q) * np.cos(1. / 3. * (np.arccos(
            1. if np.abs(row.L / np.sqrt(-row.Q ** 3)) > 1 else row.L / np.sqrt(
                -row.Q ** 3))) + 4 * np.pi / 3.) - row.a1 / 3.
        row["Z3"] = 0 if row.Q >= 0 else 2. * np.sqrt(-row.Q) * np.cos(1. / 3. * (
            np.arccos(1. if np.abs(row.L / np.sqrt(-row.Q ** 3)) > 1 else row.L / np.sqrt(-row.Q ** 3)))) - row.a1 / 3.
        row["Z1_d"] = row.S1 + row.S2 - row.a1 / 3.
        row["Z1_dd"] = ((-1. * (-1. * row.L) ** 1. / 3.) if row.L < 0 else row.L ** (1. / 3.)) * 2. - row.a1 / 3.
        row["Z2_dd"] = (((-1. * (-1. * row.L) ** 1. / 3.) if row.L < 0 else row.L ** (1. / 3.)) + row.a1 / 3.) * -1
        row["Z3_dd"] = row.Z2_dd
        row["ZL"] = self._min([row.Z1, row.Z2, row.Z3]) if row.D < 0 else (
            self._min([row.Z1_dd, row.Z2_dd, row.Z3_dd]) if row.D == 0 else row.Z1_d)
        row["ZV"] = self._max([row.Z1, row.Z2, row.Z3]) if row.D < 0 else (
            self._max([row.Z1_dd, row.Z2_dd, row.Z3_dd]) if row.D == 0 else 0)
        row["phl"] = self._fugacity(row.r1, row.r2, row.A, row.B, row.ZL, -1.5)
        row["phv"] = self._fugacity(row.r1, row.r2, row.A, row.B, row.ZV, 1.)
        row["ph_diff"] = np.abs(row.phl - row.phv)
        return row

    def _omb(self, row):
        row["Q_d"] = (3. * row.a2_d - row.a1_d ** 2) / 9.
        row["L_d"] = (9. * row.a1_d * row.a2_d - 27. * row.a3_d - 2. * row.a1_d ** 3) / 54.
        row["D_d"] = row.Q_d ** 3 + row.L_d ** 2
        row["S1_d"] = (-1. * (-1. * (row.L_d + np.sqrt(row.D_d))) ** (1. / 3.)) if (row.L_d + np.sqrt(
            row.D_d)) < 0 else (row.L_d + np.sqrt(row.D_d)) ** (1. / 3.)
        row["S2_d"] = (-1. * (-1. * (row.L_d - np.sqrt(row.D_d))) ** (1. / 3.)) if (row.L_d - np.sqrt(
            row.D_d)) < 0 else (row.L_d - np.sqrt(row.D_d)) ** (1. / 3.)
        row["theta"] = np.degrees(
            np.arccos(1. if np.abs(row.L_d / np.sqrt(-row.Q_d ** 3)) > 1 else row.L_d / np.sqrt(-row.Q_d ** 3)))
        row["Z1_ov"] = 0 if row.Q_d >= 0 else 2. * np.sqrt(-row.Q_d) * np.cos(
            np.radians(row.theta * 1. / 3. + 120)) - row.a1_d / 3.
        row["Z2_ov"] = 0 if row.Q_d >= 0 else 2. * np.sqrt(-row.Q_d) * np.cos(
            np.radians(row.theta * 1. / 3. + 240)) - row.a1_d / 3.
        row["Z3_ov"] = 0 if row.Q_d >= 0 else 2. * np.sqrt(-row.Q_d) * np.cos(
            np.radians(row.theta * 1. / 3.)) - row.a1_d / 3.
        row["Z1_ov_d"] = row.S1_d + row.S2_d - row.a1_d / 3.
        row["Z1_ov_dd"] = ((-1. * (-1. * row.L_d) ** 1. / 3.) if row.L_d < 0 else row.L_d ** (
                    1. / 3.)) * 2. - row.a1_d / 3.
        row["Z2_ov_dd"] = (((-1. * (-1. * row.L_d) ** 1. / 3.) if row.L_d < 0 else row.L_d ** (
                    1. / 3.)) + row.a1_d / 3.) * -1
        row["Z3_ov_dd"] = row.Z2_ov_dd
        row["om_b1"] = self._min([row.Z1_ov, row.Z2_ov, row.Z3_ov])
        row["om_b2"] = self._min([row.Z1_ov_dd, row.Z2_ov_dd, row.Z3_ov_dd])
        row["om_b"] = row.om_b1 if row.D_d < 0 else (row.Z1_ov_d if row.D_d > 0 else row.om_b2)
        return row

    def _fugacity(self, r1, r2, A, B, root, penalty):
        if (root - B) <= 0 or (B * (r2 - r1)) == 0 or (root + r1 * B) == 0 or (root + r2 * B) == 0 or (
                (root + r2 * B) * (root + r1 * B)) < 0:
            return penalty
        else:
            return ((root - 1) - np.log(root - B) - (A / (B * (r2 - r1))) * np.log((root + r2 * B) / (root + r1 * B)))

    def _min(self, data):
        data = [x for x in data if x > 0]
        if len(data) > 0:
            return min(data)
        return 0

    def _max(self, data):
        data = [x for x in data if x > 0]
        if len(data) > 0:
            return max(data)
        return 0


class ObservedValues(object):
    def __init__(self, parent):
        self.__dict__ = parent.__dict__.copy()
        self._prepare()

    def _prepare(self):
        df = cm_data(self.args.collection, self.logger)
        cols = ["mf", "bp_final1", "tcbytb", "vc_final", "zc_final", "pc_mpa", "vb", "hv_bp", "alpha_l", "alpha_m",
                "tc_k", "seq", "component", "omega_final"]
        droplist = [col for col in df.columns if col not in cols]
        df.loc[df.tcbytb < 1.0001, "tcbytb"] = 1.0001
        df.drop(columns=droplist, inplace=True)
        self._temperature(df)
        df["alpha_pr"] = ((df.temperature / df.tc_k) ** (2 * (df.alpha_m - 1)) * (
            np.exp(df.alpha_l * (1 - (df.temperature / df.tc_k) ** (2 * df.alpha_m)))))
        self.df = df

    def _temperature(self, df):
        if self.temperature == 0.7:
            self._temp7(df)

    def _temp7(self, df):
        p4 = omega_data(self.args.collection, self.logger, "poas_4")
        p4["hv_bp"] = p4.del_hv
        eos = eosbp_data(self.args.collection, self.logger, "als")
        eos_cols = ["bp_final1", "tc_k", "zc_final", "alpha_l", "alpha_m", "alpha_multiplier", "omega_multiplier",
                    "alpha_pr", "vb"]
        p4[eos_cols] = eos[eos_cols]
        self._fixhvvb(p4)
        df[["hv_bp", "vb", "vapour_pressure"]] = p4[["hv_bp", "vb", "vapour_pressure"]]
        df[eos_cols] = p4[eos_cols]
        self._minmax7(df)
        df["vb"] = df.vb * (df.zc_final ** (1. - 0.7) ** (2. / 7.)) / (
                    df.zc_final ** (1. - df.bp_final1 / df.tc_k) ** (2. / 7.))
        df["temperature"] = df.tc_k * 0.7

    def _minmax7(self, df):
        df["alpha_bp"] = df.alpha_multiplier * df.alpha_pr
        df.loc[df.alpha_bp < 1.05, "alpha_bp"] = 1.05
        df["alpha_tc"] = 1.
        df["tr"] = 0.7
        df["slope"] = (np.log(df.alpha_tc) - np.log(df.alpha_bp)) / (1. - (df.bp_final1 / df.tc_k))
        df.loc[df.slope > 1, "slope"] = 1.
        df.loc[df.slope < -50, "slope"] = -50.
        df["alpha_0.7"] = np.exp(np.log(df.alpha_bp) + df.slope * (0.7 - (df.bp_final1 / df.tc_k)))
        df.loc[df["alpha_0.7"] > df.alpha_pr * 1.2, "alpha_0.7"] = df.loc[df[
                                                                              "alpha_0.7"] > df.alpha_pr * 1.2, "alpha_pr"] * 1.2
        df["alpha_pr_0.7"] = (
                    (0.7) ** (2. * (df.alpha_m - 1.)) * (np.exp(df.alpha_l * (1. - (0.7) ** (2. * df.alpha_m)))))
        df.loc[df.index == df.index.max(), "alpha_pr_0.7"] = \
        df.loc[df.index == df.index.max() - 1, "alpha_pr_0.7"].values[0] * 1.01
        df["expected_alpha"] = df["alpha_0.7"] / df["alpha_pr_0.7"]
        df["per_multiplier"] = (0.0111349117430651 * df.index ** 1.89776187835285) * 10
        df.loc[df.per_multiplier > 99, "per_multiplier"] = 99
        df["alpha_multiplier_min"] = df.expected_alpha * (1 - ((
                                                                           1.89697738551E-09 * df.index ** 5 - 7.4990046401686E-07 * df.index ** 4 + 0.00010418894957489 * df.index ** 3 - 0.00605636535447327 * df.index ** 2 + 0.203286734580314 * df.index + 1.07366170022993) * 1.25) / 100)
        df["alpha_multiplier_max"] = (df.expected_alpha * (1. + 1. * df.per_multiplier / 100.))
        df.loc[df.alpha_multiplier_max > 1.2, "alpha_multiplier_max"] = 1.2
        df["omega_multiplier_min"] = df.omega_multiplier * (
                    -5.856486207E-11 * df.index ** 5 + 2.787514371825E-08 * df.index ** 4 - 4.90198457824066E-06 * df.index ** 3 + 0.000380349000374974 * df.index ** 2 - 0.0137231138575975 * df.index + 0.767728728170205)
        df["omega_multiplier_max"] = df.omega_multiplier * (1. + 0.7 * df.per_multiplier / 100.)
        df.loc[df.omega_multiplier_max > 4, "omega_multiplier_max"] = 4.

    def _fixhvvb(self, df_o):
        df = df_o.copy(deep=True)
        ls = df.index.max()

        cm = cm_data(self.args.collection, self.logger)
        df["mass"] = cm.mass
        df = df.loc[df.index < ls].tail(11)
        df["xi"] = np.log(df.mass)
        df["yi"] = np.log(df.hv_bp)
        df["xiyi"] = df.xi * df.yi
        df["xi2"] = df.xi ** 2
        xi = df.xi.sum()
        yi = df.yi.sum()
        xiyi = df.xiyi.sum()
        xi2 = df.xi2.sum()
        samples = df.shape[0]
        b = (xi * xiyi - yi * xi2) / (xi ** 2 - samples * xi2)
        a = (yi - samples * b) / xi
        df_o.loc[df_o.index == ls, "hv_bp"] = np.exp(np.log(cm.loc[cm.index == ls, "mass"].values[0]) * a + b)
        if df_o.loc[df_o.index == ls, "hv_bp"].values[0] <= 0:
            df_o.loc[df_o.index == ls, "hv_bp"] = 1.
        df_o.loc[df_o.index == ls, "vb"] = cm.loc[cm.index == ls, "vb"].values[0]
