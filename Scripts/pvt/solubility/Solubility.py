from importlib import import_module

from Scripts.pvt.core.constants import solubility_temperatures
from Scripts.pvt.core.storage.excel import write_excel
from Scripts.pvt.core.storage.mongo import write


class Solubility(object):
    def __init__(self, parent):
        self.__dict__ = parent.__dict__.copy()

    def runSolubilityAnalysis(self):
        for eos in self.getEOS():
            self.runSolubilityTuning(eos)

    def getEOS(self):
        validEOS = self.validEOS
        if self.args.eos == "all":
            for eos in validEOS:
                yield eos
        else:
            yield self.args.eos

    def runSolubilityTuning(self, eos):
        eos_sol = import_module("solubility.%s_solubility" % (eos))
        obj = getattr(eos_sol, "%s_solubility" % (eos))(self)
        for temp in solubility_temperatures:
            obj.optimize(temp, eos)
            write_excel(self.args.debug, self.debugDir, ["Solubility_eos", eos, temp],
                        ["observed", "calculated"], [obj.ov, obj.cv])
            write(obj.cv, "MBP", self.args.collection, logger=self.logger, Property="MCOutput",
                  Identifier="SolubilityTuning", Eos=eos, Temperature=temp)
