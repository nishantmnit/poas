import pandas as pd
from Scripts.pvt.core.optimizers.ncg import *
from Scripts.pvt.core.pandas.dataframe import drop_not_in_cols
from Scripts.pvt.core.storage.fetch_most import cm_data, eosbp_data, omega_data
from Scripts.pvt.core.storage.mongo import fetch

gasConstant = 8.314472
pressure = 0.101325


class poas_4a_solubility(object):
    def __init__(self, parent):
        self.__dict__ = parent.__dict__.copy()
        self.cost_diff = list()

    def optimize(self, temperature, eos):
        self.temperature = temperature
        self.eos = eos
        self._setNVar()
        ov = ObservedValues(self)

        for index, row in ov.df.iterrows():
            self.logger.info("EOS = %s | temperature = %s | solubility tuning for scn = %s | component = %s" % (
            self.eos, self.temperature, row.scn, row.component))
            self.ncg = ncg()
            vars = self.ncg.minimize(self._sp(row), self._range(row), self._cost_for_ncg, self.logger, max_iter=150,
                                     debug=self.args.debug, converge_f=self._converged, args=(row))
            for i in range(len(vars)):
                ov.df.loc[ov.df.seq == row.seq, self._nvar[i]] = vars[i]
        cv = CalculatedValues(self, ov.df, normalize=True)
        self.ov = ov.df
        self.cv = cv.df

    def _setNVar(self):
        self._nvar = ["alpha_multiplier", "beta_multiplier"]

    def _cost_for_ncg(self, vars, row):
        cv, cost = self._cost(row, vars)
        gd = Gradients(self)
        grad = gd.gradient(row, cv)
        return cost, grad

    def _cost(self, row_o, vars):
        row = row_o.copy(deep=True)
        for i in range(len(vars)): row[self._nvar[i]] = vars[i]
        calc = CalculatedValues(self, row)
        cv = calc.row
        objs = ["solubility", "v_liq"]
        self._scales()
        costs = pd.Series()
        for i in range(len(objs)):
            costs[objs[i]] = 0 if row[objs[i]] == 0 else self.scales[i] * (
                        (row[objs[i]] - cv[objs[i]]) / row[objs[i]]) ** 2
        return cv, costs.sum() + self.scales[2] * max(0, cv.ph_diff - 10 ** -7) ** 2

    def _scales(self):
        if self.temperature >= 400.:
            self.scales = [30., 200., 0.0000001]
        else:
            self.scales = [30., 200., 0.0000001]

    def _range(self, row):
        return [[row.alpha_multiplier_min, row.alpha_multiplier_max],
                [row.beta_multiplier_min, row.beta_multiplier_max]]

    def _converged(self, debug, logger, iteration, grad):
        cd = np.sqrt(np.sum(np.square(grad), axis=0))
        self.cost_diff.append(self.ncg.cost_diff)
        # if previous 2 costs difference is in same order then converge
        if iteration < 3:
            return False
        elif iteration <= 8 and cd <= 10 ** -3:
            if debug: logger.info("Converged by rule 1 - %s" % (cd))
            return True
        elif (iteration > 8 and iteration <= 50) and (
                self.cost_diff[-1] <= 10 ** -4 and self.cost_diff[-2] <= 10 ** -4):
            if debug: logger.info("Converged by rule 2 - %s - %s" % (self.cost_diff[-1], self.cost_diff[-2]))
            return True
        elif (iteration > 50 and iteration <= 75) and (
                self.cost_diff[-1] <= 10 ** -3 and self.cost_diff[-2] <= 10 ** -3):
            if debug: logger.info("Converged by rule 3 - %s - %s" % (self.cost_diff[-1], self.cost_diff[-2]))
            return True
        elif (iteration > 75 and iteration <= 100) and (
                self.cost_diff[-1] <= 10 ** -2 and self.cost_diff[-2] <= 10 ** -2):
            if debug: logger.info("Converged by rule 4 - %s - %s" % (self.cost_diff[-1], self.cost_diff[-2]))
            return True
        elif (iteration > 100 and iteration <= 150) and (
                self.cost_diff[-1] <= 10 ** -1 and self.cost_diff[-2] <= 10 ** -1):
            if debug: logger.info("Converged by rule 5 - %s - %s" % (self.cost_diff[-1], self.cost_diff[-2]))
            return True
        return False

    def _sp(self, row):
        return [row.expected_alpha_multiplier, row.beta_multiplier]


class Gradients(object):
    def __init__(self, parent):
        self.__dict__ = parent.__dict__.copy()

    def gradient(self, ov, cv):
        return self._grad(ov, cv)

    def _grad(self, ov, cv):
        diff_alpha_wrt1 = ov.alpha_pr
        diff_alpha_wrt2 = 0

        diff_diff_alpha_wrt1 = ov.alpha_pr * (((2 * ov.alpha_m - 2) / (ov.temperature / ov.tc_k) + ov.alpha_l * (
                    -2 * ov.alpha_m) * (ov.temperature / ov.tc_k) ** (2 * ov.alpha_m - 1)) / ov.tc_k)
        diff_diff_alpha_wrt2 = 0

        diff_beta_wrt1 = 0
        diff_beta_wrt2 = ov.beta_est

        diff_om_b_wrt1 = 0
        diff_om_b_wrt2 = 0 if (ov.zc_final - cv.beta) < ov.om_b_min else -1. * diff_beta_wrt2

        diff_om_c_wrt1 = 0
        diff_om_c_wrt2 = (
                    -2. * (1. - cv.beta) * diff_beta_wrt2 * (cv.beta - 0.25) + (1. - cv.beta) ** 2 * diff_beta_wrt2)

        diff_om_d_wrt1 = 0
        diff_om_d_wrt2 = 0.5 * diff_beta_wrt2

        diff_om_a_wrt1 = -3. * diff_beta_wrt1 * (1. - cv.beta) ** 2
        diff_om_a_wrt2 = -3. * diff_beta_wrt2 * (1. - cv.beta) ** 2

        diff_b_wrt1 = diff_om_b_wrt1 * gasConstant * ov.tc_k / (ov.pc_mpa * 10 ** 6)
        diff_b_wrt2 = diff_om_b_wrt2 * gasConstant * ov.tc_k / (ov.pc_mpa * 10 ** 6)

        diff_c_wrt1 = diff_om_c_wrt1 * (gasConstant * ov.tc_k / (ov.pc_mpa * 10 ** 6)) ** 2
        diff_c_wrt2 = diff_om_c_wrt2 * (gasConstant * ov.tc_k / (ov.pc_mpa * 10 ** 6)) ** 2

        diff_d_wrt1 = diff_om_d_wrt1 * gasConstant * ov.tc_k / (ov.pc_mpa * 10 ** 6)
        diff_d_wrt2 = diff_om_d_wrt2 * gasConstant * ov.tc_k / (ov.pc_mpa * 10 ** 6)

        diff_ac_wrt1 = 0
        diff_ac_wrt2 = diff_om_a_wrt2 * gasConstant ** 2 * ov.tc_k ** 2 / (ov.pc_mpa * 10 ** 6)

        diff_ac_alpha_wrt1 = (cv.ac * diff_alpha_wrt1 + diff_ac_wrt1 * cv.alpha_eos)
        diff_ac_alpha_wrt2 = (cv.ac * diff_alpha_wrt2 + diff_ac_wrt2 * cv.alpha_eos)

        diff_u_wrt1 = 0 if cv.b == 0 else ((-2. * (diff_d_wrt1 * cv.b - cv.d * diff_b_wrt1)) / cv.b ** 2)
        diff_u_wrt2 = 0 if cv.b == 0 else ((-2. * (diff_d_wrt2 * cv.b - cv.d * diff_b_wrt2)) / cv.b ** 2)

        diff_w_wrt1 = 0 if cv.b == 0 else (((2. * diff_d_wrt1 * cv.d + diff_c_wrt1) * cv.b ** 2 - (
                    cv.d ** 2 + cv.c) * 2. * diff_b_wrt1 * cv.b) / cv.b ** 4)
        diff_w_wrt2 = 0 if cv.b == 0 else (((2. * diff_d_wrt2 * cv.d + diff_c_wrt2) * cv.b ** 2 - (
                    cv.d ** 2 + cv.c) * 2. * diff_b_wrt2 * cv.b) / cv.b ** 4)

        diff_r2_wrt1 = 0
        diff_r2_wrt2 = 0 if (cv.u ** 2 - 4 * cv.w) <= 0 else (diff_u_wrt2 + 0.5 * (
                    2 * cv.u * diff_u_wrt2 - 4 * diff_w_wrt2) * (cv.u ** 2 - 4 * cv.w) ** -0.5) / 2

        diff_r1_wrt1 = 0
        diff_r1_wrt2 = 0 if (cv.u ** 2 - 4 * cv.w) <= 0 else (diff_u_wrt2 - 0.5 * (
                    2 * cv.u * diff_u_wrt2 - 4 * diff_w_wrt2) * (cv.u ** 2 - 4 * cv.w) ** -0.5) / 2

        diff_A_wrt1 = diff_ac_alpha_wrt1 * pressure * 10 ** 6 / (gasConstant ** 2 * ov.temperature ** 2)
        diff_A_wrt2 = diff_ac_alpha_wrt2 * pressure * 10 ** 6 / (gasConstant ** 2 * ov.temperature ** 2)

        diff_B_wrt1 = 0
        diff_B_wrt2 = diff_b_wrt2 * pressure * 10 ** 6 / (gasConstant * ov.temperature)

        diff_a1_wrt1 = 0
        diff_a1_wrt2 = ((cv.u * diff_B_wrt2 + diff_u_wrt2 * cv.B) - diff_B_wrt2)

        diff_a2_wrt1 = diff_A_wrt1 + (cv.w * 2 * cv.B * diff_B_wrt1 + diff_w_wrt1 * cv.B ** 2) - (
                    cv.u * diff_B_wrt1 + diff_u_wrt1 * cv.B) - (cv.u * 2 * cv.B * diff_B_wrt1 + diff_u_wrt1 * cv.B ** 2)
        diff_a2_wrt2 = diff_A_wrt2 + (cv.w * 2 * cv.B * diff_B_wrt2 + diff_w_wrt2 * cv.B ** 2) - (
                    cv.u * diff_B_wrt2 + diff_u_wrt2 * cv.B) - (cv.u * 2 * cv.B * diff_B_wrt2 + diff_u_wrt2 * cv.B ** 2)

        diff_a3_wrt1 = -1 * ((cv.A * diff_B_wrt1 + diff_A_wrt1 * cv.B) + (
                    cv.w * 2 * cv.B * diff_B_wrt1 + diff_w_wrt1 * cv.B ** 2) + (
                                         cv.w * 3 * cv.B ** 2 * diff_B_wrt1 + diff_w_wrt1 * cv.B ** 3))
        diff_a3_wrt2 = -1 * ((cv.A * diff_B_wrt2 + diff_A_wrt2 * cv.B) + (
                    cv.w * 2 * cv.B * diff_B_wrt2 + diff_w_wrt2 * cv.B ** 2) + (
                                         cv.w * 3 * cv.B ** 2 * diff_B_wrt2 + diff_w_wrt2 * cv.B ** 3))

        diff_Q_wrt1 = (3 * diff_a2_wrt1 - 2 * cv.a1 * diff_a1_wrt1) / 9.
        diff_Q_wrt2 = (3 * diff_a2_wrt2 - 2 * cv.a1 * diff_a1_wrt2) / 9.

        diff_L_wrt1 = (9 * (
                    cv.a1 * diff_a2_wrt1 + diff_a1_wrt1 * cv.a2) - 27 * diff_a3_wrt1 - 2 * 3 * diff_a1_wrt1 * cv.a1 ** 2) / 54.
        diff_L_wrt2 = (9 * (
                    cv.a1 * diff_a2_wrt2 + diff_a1_wrt2 * cv.a2) - 27 * diff_a3_wrt2 - 2 * 3 * diff_a1_wrt2 * cv.a1 ** 2) / 54.

        diff_D_wrt1 = 3 * diff_Q_wrt1 * cv.Q ** 2 + 2 * diff_L_wrt1 * cv.L
        diff_D_wrt2 = 3 * diff_Q_wrt2 * cv.Q ** 2 + 2 * diff_L_wrt2 * cv.L

        diff_S1_wrt1 = (-1 * (1. / 3.) * (-1 * (diff_L_wrt1 + 0.5 * diff_D_wrt1 / np.sqrt(cv.D))) * (
                    -1 * (cv.L + np.sqrt(cv.D))) ** (1. / 3. - 1)) if (cv.L + np.sqrt(cv.D)) < 0 else (
                    (1. / 3.) * (diff_L_wrt1 + 0.5 * diff_D_wrt1 / np.sqrt(cv.D)) * (cv.L + np.sqrt(cv.D)) ** (
                        1. / 3. - 1))
        diff_S1_wrt2 = (-1 * (1. / 3.) * (-1 * (diff_L_wrt2 + 0.5 * diff_D_wrt2 / np.sqrt(cv.D))) * (
                    -1 * (cv.L + np.sqrt(cv.D))) ** (1. / 3. - 1)) if (cv.L + np.sqrt(cv.D)) < 0 else (
                    (1. / 3.) * (diff_L_wrt2 + 0.5 * diff_D_wrt2 / np.sqrt(cv.D)) * (cv.L + np.sqrt(cv.D)) ** (
                        1. / 3. - 1))

        diff_S2_wrt1 = (-1 * (1. / 3.) * (-1 * (diff_L_wrt1 - 0.5 * diff_D_wrt1 / np.sqrt(cv.D))) * (
                    -1 * (cv.L - np.sqrt(cv.D))) ** (1. / 3. - 1)) if (cv.L - np.sqrt(cv.D)) < 0 else (
                    (1. / 3.) * (diff_L_wrt1 - 0.5 * diff_D_wrt1 / np.sqrt(cv.D)) * (cv.L - np.sqrt(cv.D)) ** (
                        1. / 3. - 1))
        diff_S2_wrt2 = (-1 * (1. / 3.) * (-1 * (diff_L_wrt2 - 0.5 * diff_D_wrt2 / np.sqrt(cv.D))) * (
                    -1 * (cv.L - np.sqrt(cv.D))) ** (1. / 3. - 1)) if (cv.L - np.sqrt(cv.D)) < 0 else (
                    (1. / 3.) * (diff_L_wrt2 - 0.5 * diff_D_wrt2 / np.sqrt(cv.D)) * (cv.L - np.sqrt(cv.D)) ** (
                        1. / 3. - 1))

        diff_Z1_d_wrt1 = diff_S1_wrt1 + diff_S2_wrt1 - diff_a1_wrt1 / 3
        diff_Z1_d_wrt2 = diff_S1_wrt2 + diff_S2_wrt2 - diff_a1_wrt2 / 3

        diff_Z1_dd_wrt1 = ((-1 * (1. / 3.) * (-1 * diff_L_wrt1) * (-1 * cv.L) ** (1. / 3. - 1)) if cv.L < 0 else (
                    1. / 3. * diff_L_wrt1 * cv.L ** (1. / 3. - 1))) * 2 - diff_a1_wrt1 / 3
        diff_Z1_dd_wrt2 = ((-1 * (1. / 3.) * (-1 * diff_L_wrt2) * (-1 * cv.L) ** (1. / 3. - 1)) if cv.L < 0 else (
                    1. / 3. * diff_L_wrt2 * cv.L ** (1. / 3. - 1))) * 2 - diff_a1_wrt2 / 3

        diff_Z2_dd_wrt1 = (((-1 * (1. / 3.) * (-1 * diff_L_wrt1) * (-1 * cv.L) ** (1. / 3. - 1)) if cv.L < 0 else (
                    1. / 3. * diff_L_wrt1 * cv.L ** (1. / 3. - 1))) + diff_a1_wrt1 / 3) * -1.
        diff_Z2_dd_wrt2 = (((-1 * (1. / 3.) * (-1 * diff_L_wrt2) * (-1 * cv.L) ** (1. / 3. - 1)) if cv.L < 0 else (
                    1. / 3. * diff_L_wrt2 * cv.L ** (1. / 3. - 1))) + diff_a1_wrt2 / 3) * -1.

        diff_Z3_dd_wrt1 = diff_Z2_dd_wrt1
        diff_Z3_dd_wrt2 = diff_Z2_dd_wrt2

        diff_Z1_wrt1 = 0 if cv.D >= 0 or cv.Q >= 0 else (2 * (-0.5 * diff_Q_wrt1 / np.sqrt(-1 * cv.Q) * np.cos(
            1. / 3. * (np.arccos(1. if np.abs(cv.L / np.sqrt(-cv.Q ** 3)) > 1 else cv.L / np.sqrt(
                -cv.Q ** 3))) + 2 * np.pi / 3) + np.sqrt(-1 * cv.Q) * (-1) * (1. / 3. * -1 / np.sqrt(
            1 - (1. if np.abs(cv.L / np.sqrt(-cv.Q ** 3)) > 1 else cv.L / np.sqrt(-cv.Q ** 3)) ** 2)) * ((np.sqrt(
            -cv.Q ** 3) * diff_L_wrt1 - (0.5 * -1 * 3 * diff_Q_wrt1 * cv.Q ** 2) / np.sqrt(-cv.Q ** 3) * cv.L) / (
                                                                                                             -cv.Q ** 3)) * np.sin(
            1. / 3. * (np.arccos(1. if np.abs(cv.L / np.sqrt(-cv.Q ** 3)) > 1 else cv.L / np.sqrt(
                -cv.Q ** 3))) + 2 * np.pi / 3)) - diff_a1_wrt1 / 3)
        diff_Z1_wrt2 = 0 if cv.D >= 0 or cv.Q >= 0 else (2 * (-0.5 * diff_Q_wrt2 / np.sqrt(-1 * cv.Q) * np.cos(
            1. / 3. * (np.arccos(1. if np.abs(cv.L / np.sqrt(-cv.Q ** 3)) > 1 else cv.L / np.sqrt(
                -cv.Q ** 3))) + 2 * np.pi / 3) + np.sqrt(-1 * cv.Q) * (-1) * (1. / 3. * -1 / np.sqrt(
            1 - (1. if np.abs(cv.L / np.sqrt(-cv.Q ** 3)) > 1 else cv.L / np.sqrt(-cv.Q ** 3)) ** 2)) * ((np.sqrt(
            -cv.Q ** 3) * diff_L_wrt2 - (0.5 * -1 * 3 * diff_Q_wrt2 * cv.Q ** 2) / np.sqrt(-cv.Q ** 3) * cv.L) / (
                                                                                                             -cv.Q ** 3)) * np.sin(
            1. / 3. * (np.arccos(1. if np.abs(cv.L / np.sqrt(-cv.Q ** 3)) > 1 else cv.L / np.sqrt(
                -cv.Q ** 3))) + 2 * np.pi / 3)) - diff_a1_wrt2 / 3)

        diff_Z2_wrt1 = 0 if cv.D >= 0 or cv.Q >= 0 else (2 * (-0.5 * diff_Q_wrt1 / np.sqrt(-1 * cv.Q) * np.cos(
            1. / 3. * (np.arccos(1. if np.abs(cv.L / np.sqrt(-cv.Q ** 3)) > 1 else cv.L / np.sqrt(
                -cv.Q ** 3))) + 4 * np.pi / 3) + np.sqrt(-1 * cv.Q) * (-1) * (1. / 3. * -1 / np.sqrt(
            1 - (1. if np.abs(cv.L / np.sqrt(-cv.Q ** 3)) > 1 else cv.L / np.sqrt(-cv.Q ** 3)) ** 2)) * ((np.sqrt(
            -cv.Q ** 3) * diff_L_wrt1 - (0.5 * -1 * 3 * diff_Q_wrt1 * cv.Q ** 2) / np.sqrt(-cv.Q ** 3) * cv.L) / (
                                                                                                             -cv.Q ** 3)) * np.sin(
            1. / 3. * (np.arccos(1. if np.abs(cv.L / np.sqrt(-cv.Q ** 3)) > 1 else cv.L / np.sqrt(
                -cv.Q ** 3))) + 4 * np.pi / 3)) - diff_a1_wrt1 / 3)
        diff_Z2_wrt2 = 0 if cv.D >= 0 or cv.Q >= 0 else (2 * (-0.5 * diff_Q_wrt2 / np.sqrt(-1 * cv.Q) * np.cos(
            1. / 3. * (np.arccos(1. if np.abs(cv.L / np.sqrt(-cv.Q ** 3)) > 1 else cv.L / np.sqrt(
                -cv.Q ** 3))) + 4 * np.pi / 3) + np.sqrt(-1 * cv.Q) * (-1) * (1. / 3. * -1 / np.sqrt(
            1 - (1. if np.abs(cv.L / np.sqrt(-cv.Q ** 3)) > 1 else cv.L / np.sqrt(-cv.Q ** 3)) ** 2)) * ((np.sqrt(
            -cv.Q ** 3) * diff_L_wrt2 - (0.5 * -1 * 3 * diff_Q_wrt2 * cv.Q ** 2) / np.sqrt(-cv.Q ** 3) * cv.L) / (
                                                                                                             -cv.Q ** 3)) * np.sin(
            1. / 3. * (np.arccos(1. if np.abs(cv.L / np.sqrt(-cv.Q ** 3)) > 1 else cv.L / np.sqrt(
                -cv.Q ** 3))) + 4 * np.pi / 3)) - diff_a1_wrt2 / 3)

        diff_Z3_wrt1 = 0 if cv.D >= 0 or cv.Q >= 0 else (2 * (-0.5 * diff_Q_wrt1 / np.sqrt(-1 * cv.Q) * np.cos(
            1. / 3. * (
                np.arccos(1. if np.abs(cv.L / np.sqrt(-cv.Q ** 3)) > 1 else cv.L / np.sqrt(-cv.Q ** 3)))) + np.sqrt(
            -1 * cv.Q) * (-1) * (1. / 3. * -1 / np.sqrt(
            1 - (1. if np.abs(cv.L / np.sqrt(-cv.Q ** 3)) > 1 else cv.L / np.sqrt(-cv.Q ** 3)) ** 2)) * ((np.sqrt(
            -cv.Q ** 3) * diff_L_wrt1 - (0.5 * -1 * 3 * diff_Q_wrt1 * cv.Q ** 2) / np.sqrt(-cv.Q ** 3) * cv.L) / (
                                                                                                             -cv.Q ** 3)) * np.sin(
            1. / 3. * (np.arccos(
                1. if np.abs(cv.L / np.sqrt(-cv.Q ** 3)) > 1 else cv.L / np.sqrt(-cv.Q ** 3))))) - diff_a1_wrt1 / 3)
        diff_Z3_wrt2 = 0 if cv.D >= 0 or cv.Q >= 0 else (2 * (-0.5 * diff_Q_wrt2 / np.sqrt(-1 * cv.Q) * np.cos(
            1. / 3. * (
                np.arccos(1. if np.abs(cv.L / np.sqrt(-cv.Q ** 3)) > 1 else cv.L / np.sqrt(-cv.Q ** 3)))) + np.sqrt(
            -1 * cv.Q) * (-1) * (1. / 3. * -1 / np.sqrt(
            1 - (1. if np.abs(cv.L / np.sqrt(-cv.Q ** 3)) > 1 else cv.L / np.sqrt(-cv.Q ** 3)) ** 2)) * ((np.sqrt(
            -cv.Q ** 3) * diff_L_wrt2 - (0.5 * -1 * 3 * diff_Q_wrt2 * cv.Q ** 2) / np.sqrt(-cv.Q ** 3) * cv.L) / (
                                                                                                             -cv.Q ** 3)) * np.sin(
            1. / 3. * (np.arccos(
                1. if np.abs(cv.L / np.sqrt(-cv.Q ** 3)) > 1 else cv.L / np.sqrt(-cv.Q ** 3))))) - diff_a1_wrt2 / 3)

        diff_ZL_wrt1 = (
            diff_Z1_wrt1 if cv.ZL == cv.Z1 else (diff_Z2_wrt1 if cv.ZL == cv.Z2 else diff_Z3_wrt1)) if cv.D < 0 else ((
                                                                                                                          diff_Z1_dd_wrt1 if cv.ZL == cv.Z1_dd else (
                                                                                                                              diff_Z2_dd_wrt1 if cv.ZL == cv.Z2_dd else diff_Z3_dd_wrt1)) if cv.D == 0 else diff_Z1_d_wrt1)
        diff_ZL_wrt2 = (
            diff_Z1_wrt2 if cv.ZL == cv.Z1 else (diff_Z2_wrt2 if cv.ZL == cv.Z2 else diff_Z3_wrt2)) if cv.D < 0 else ((
                                                                                                                          diff_Z1_dd_wrt2 if cv.ZL == cv.Z1_dd else (
                                                                                                                              diff_Z2_dd_wrt2 if cv.ZL == cv.Z2_dd else diff_Z3_dd_wrt2)) if cv.D == 0 else diff_Z1_d_wrt2)

        diff_ZV_wrt1 = (
            diff_Z1_wrt1 if cv.ZV == cv.Z1 else (diff_Z2_wrt1 if cv.ZV == cv.Z2 else diff_Z3_wrt1)) if cv.D < 0 else ((
                                                                                                                          diff_Z1_dd_wrt1 if cv.ZV == cv.Z1_dd else (
                                                                                                                              diff_Z2_dd_wrt1 if cv.ZV == cv.Z2_dd else diff_Z3_dd_wrt1)) if cv.D == 0 else 0)
        diff_ZV_wrt2 = (
            diff_Z1_wrt2 if cv.ZV == cv.Z1 else (diff_Z2_wrt2 if cv.ZV == cv.Z2 else diff_Z3_wrt2)) if cv.D < 0 else ((
                                                                                                                          diff_Z1_dd_wrt2 if cv.ZV == cv.Z1_dd else (
                                                                                                                              diff_Z2_dd_wrt2 if cv.ZV == cv.Z2_dd else diff_Z3_dd_wrt2)) if cv.D == 0 else 0)

        diff_phl_wrt1 = 0 if (cv.ZL - cv.B) <= 0 or cv.B * (cv.r2 - cv.r1) == 0 or (cv.ZL + cv.r1 * cv.B) == 0 or (
                    cv.ZL + cv.r2 * cv.B) == 0 or (cv.ZL + cv.r2 * cv.B) * (cv.ZL + cv.r1 * cv.B) < 0 else (
                    (diff_ZL_wrt1) - (diff_ZL_wrt1 - diff_B_wrt1) / (cv.ZL - cv.B) - ((((cv.B * (
                        cv.r2 - cv.r1)) * diff_A_wrt1 - (diff_B_wrt1 * (cv.r2 - cv.r1) + cv.B * (
                        diff_r2_wrt1 - diff_r1_wrt1)) * cv.A) / (cv.B * (cv.r2 - cv.r1)) ** 2) * np.log(
                (cv.ZL + cv.r2 * cv.B) / (cv.ZL + cv.r1 * cv.B)) + (cv.A / (cv.B * (cv.r2 - cv.r1))) * 1 / ((
                                                                                                                        cv.ZL + cv.r2 * cv.B) / (
                                                                                                                        cv.ZL + cv.r1 * cv.B)) * (
                                                                                                  ((
                                                                                                               cv.ZL + cv.r1 * cv.B) * (
                                                                                                               diff_ZL_wrt1 + diff_r2_wrt1 * cv.B + cv.r2 * diff_B_wrt1) - (
                                                                                                               cv.ZL + cv.r2 * cv.B) * (
                                                                                                               diff_ZL_wrt1 + diff_r1_wrt1 * cv.B + cv.r1 * diff_B_wrt1)) / (
                                                                                                              cv.ZL + cv.r1 * cv.B) ** 2)))
        diff_phl_wrt2 = 0 if (cv.ZL - cv.B) <= 0 or cv.B * (cv.r2 - cv.r1) == 0 or (cv.ZL + cv.r1 * cv.B) == 0 or (
                    cv.ZL + cv.r2 * cv.B) == 0 or (cv.ZL + cv.r2 * cv.B) * (cv.ZL + cv.r1 * cv.B) < 0 else (
                    (diff_ZL_wrt2) - (diff_ZL_wrt2 - diff_B_wrt2) / (cv.ZL - cv.B) - ((((cv.B * (
                        cv.r2 - cv.r1)) * diff_A_wrt2 - (diff_B_wrt2 * (cv.r2 - cv.r1) + cv.B * (
                        diff_r2_wrt2 - diff_r1_wrt2)) * cv.A) / (cv.B * (cv.r2 - cv.r1)) ** 2) * np.log(
                (cv.ZL + cv.r2 * cv.B) / (cv.ZL + cv.r1 * cv.B)) + (cv.A / (cv.B * (cv.r2 - cv.r1))) * 1 / ((
                                                                                                                        cv.ZL + cv.r2 * cv.B) / (
                                                                                                                        cv.ZL + cv.r1 * cv.B)) * (
                                                                                                  ((
                                                                                                               cv.ZL + cv.r1 * cv.B) * (
                                                                                                               diff_ZL_wrt2 + diff_r2_wrt2 * cv.B + cv.r2 * diff_B_wrt2) - (
                                                                                                               cv.ZL + cv.r2 * cv.B) * (
                                                                                                               diff_ZL_wrt2 + diff_r1_wrt2 * cv.B + cv.r1 * diff_B_wrt2)) / (
                                                                                                              cv.ZL + cv.r1 * cv.B) ** 2)))

        diff_phv_wrt1 = 0 if (cv.ZV - cv.B) <= 0 or cv.B * (cv.r2 - cv.r1) == 0 or (cv.ZV + cv.r1 * cv.B) == 0 or (
                    cv.ZV + cv.r2 * cv.B) == 0 or (cv.ZV + cv.r2 * cv.B) * (cv.ZV + cv.r1 * cv.B) < 0 else (
                    (diff_ZV_wrt1) - (diff_ZV_wrt1 - diff_B_wrt1) / (cv.ZV - cv.B) - ((((cv.B * (
                        cv.r2 - cv.r1)) * diff_A_wrt1 - (diff_B_wrt1 * (cv.r2 - cv.r1) + cv.B * (
                        diff_r2_wrt1 - diff_r1_wrt1)) * cv.A) / (cv.B * (cv.r2 - cv.r1)) ** 2) * np.log(
                (cv.ZV + cv.r2 * cv.B) / (cv.ZV + cv.r1 * cv.B)) + (cv.A / (cv.B * (cv.r2 - cv.r1))) * 1 / ((
                                                                                                                        cv.ZV + cv.r2 * cv.B) / (
                                                                                                                        cv.ZV + cv.r1 * cv.B)) * (
                                                                                                  ((
                                                                                                               cv.ZV + cv.r1 * cv.B) * (
                                                                                                               diff_ZV_wrt1 + diff_r2_wrt1 * cv.B + cv.r2 * diff_B_wrt1) - (
                                                                                                               cv.ZV + cv.r2 * cv.B) * (
                                                                                                               diff_ZV_wrt1 + diff_r1_wrt1 * cv.B + cv.r1 * diff_B_wrt1)) / (
                                                                                                              cv.ZV + cv.r1 * cv.B) ** 2)))
        diff_phv_wrt2 = 0 if (cv.ZV - cv.B) <= 0 or cv.B * (cv.r2 - cv.r1) == 0 or (cv.ZV + cv.r1 * cv.B) == 0 or (
                    cv.ZV + cv.r2 * cv.B) == 0 or (cv.ZV + cv.r2 * cv.B) * (cv.ZV + cv.r1 * cv.B) < 0 else (
                    (diff_ZV_wrt2) - (diff_ZV_wrt2 - diff_B_wrt2) / (cv.ZV - cv.B) - ((((cv.B * (
                        cv.r2 - cv.r1)) * diff_A_wrt2 - (diff_B_wrt2 * (cv.r2 - cv.r1) + cv.B * (
                        diff_r2_wrt2 - diff_r1_wrt2)) * cv.A) / (cv.B * (cv.r2 - cv.r1)) ** 2) * np.log(
                (cv.ZV + cv.r2 * cv.B) / (cv.ZV + cv.r1 * cv.B)) + (cv.A / (cv.B * (cv.r2 - cv.r1))) * 1 / ((
                                                                                                                        cv.ZV + cv.r2 * cv.B) / (
                                                                                                                        cv.ZV + cv.r1 * cv.B)) * (
                                                                                                  ((
                                                                                                               cv.ZV + cv.r1 * cv.B) * (
                                                                                                               diff_ZV_wrt2 + diff_r2_wrt2 * cv.B + cv.r2 * diff_B_wrt2) - (
                                                                                                               cv.ZV + cv.r2 * cv.B) * (
                                                                                                               diff_ZV_wrt2 + diff_r1_wrt2 * cv.B + cv.r1 * diff_B_wrt2)) / (
                                                                                                              cv.ZV + cv.r1 * cv.B) ** 2)))

        diff_ph_diff_wrt1 = 0 if cv.ZV == 0 else np.sign(cv.phl - cv.phv) * (diff_phl_wrt1 - diff_phv_wrt1)
        diff_ph_diff_wrt2 = 0 if cv.ZV == 0 else np.sign(cv.phl - cv.phv) * (diff_phl_wrt2 - diff_phv_wrt2)

        diff_vl_wrt1 = diff_ZL_wrt1 * gasConstant * ov.temperature / pressure
        diff_vl_wrt2 = diff_ZL_wrt2 * gasConstant * ov.temperature / pressure

        diff_vv_wrt1 = diff_ZV_wrt1 * gasConstant * ov.temperature / pressure
        diff_vv_wrt2 = diff_ZV_wrt2 * gasConstant * ov.temperature / pressure

        diff_t1_wrt1 = 0 if cv.v_liq == 0 else (-1. * (1. / cv.v_liq ** 2) * diff_vl_wrt1)
        diff_t1_wrt2 = 0 if cv.v_liq == 0 else (-1. * (1. / cv.v_liq ** 2) * diff_vl_wrt2)

        diff_t2_wrt1 = 0 if (cv.b * (cv.r2 - cv.r1)) == 0 else ((cv.b * (cv.r2 - cv.r1) * (
                    cv.ac * (diff_alpha_wrt1 - cv.temperature * diff_diff_alpha_wrt1) + diff_ac_wrt1 * (
                        cv.alpha_eos - cv.temperature * cv.diff_alpha_wrt_temp)) - (cv.ac * (
                    cv.alpha_eos - cv.temperature * cv.diff_alpha_wrt_temp)) * (cv.b * (
                    diff_r2_wrt1 - diff_r1_wrt1) + diff_b_wrt1 * (cv.r2 - cv.r1))) / (cv.b * (cv.r2 - cv.r1)) ** 2)
        diff_t2_wrt2 = 0 if (cv.b * (cv.r2 - cv.r1)) == 0 else ((cv.b * (cv.r2 - cv.r1) * (
                    cv.ac * (diff_alpha_wrt2 - cv.temperature * diff_diff_alpha_wrt2) + diff_ac_wrt2 * (
                        cv.alpha_eos - cv.temperature * cv.diff_alpha_wrt_temp)) - (cv.ac * (
                    cv.alpha_eos - cv.temperature * cv.diff_alpha_wrt_temp)) * (cv.b * (
                    diff_r2_wrt2 - diff_r1_wrt2) + diff_b_wrt2 * (cv.r2 - cv.r1))) / (cv.b * (cv.r2 - cv.r1)) ** 2)

        diff_t1_sol_wrt1 = 0 if (cv.b * (cv.r2 - cv.r1)) == 0 else ((1. / cv.v_liq) * diff_t2_wrt1 + (
                    (cv.ac_alpha - cv.temperature * cv.ac * cv.diff_alpha_wrt_temp) / (
                        cv.b * (cv.r2 - cv.r1))) * diff_t1_wrt1)
        diff_t1_sol_wrt2 = 0 if (cv.b * (cv.r2 - cv.r1)) == 0 else ((1. / cv.v_liq) * diff_t2_wrt2 + (
                    (cv.ac_alpha - cv.temperature * cv.ac * cv.diff_alpha_wrt_temp) / (
                        cv.b * (cv.r2 - cv.r1))) * diff_t1_wrt2)

        if cv.v_vapour == 0:
            diff_t2_sol_wrt1 = 0
            diff_t2_sol_wrt2 = 0
        elif (cv.v_vapour + cv.r1 * cv.b * 10 ** 6) <= 0 or (cv.v_vapour + cv.r2 * cv.b * 10 ** 6) <= 0 or (
                (cv.v_vapour + cv.r1 * cv.b * 10 ** 6) / ((cv.v_vapour + cv.r2 * cv.b * 10 ** 6))) < 0:
            diff_t2_sol_wrt1 = 0.
            diff_t2_sol_wrt2 = 0.
        else:
            diff_t2_sol_wrt1 = (1. / (cv.v_vapour + cv.r1 * cv.b * 10 ** 6) * (
                        diff_vv_wrt1 + diff_r1_wrt1 * cv.b * 10 ** 6 + cv.r1 * diff_b_wrt1 * 10 ** 6) - 1 / (
                                            cv.v_vapour + cv.r2 * cv.b * 10 ** 6) * (
                                            diff_vv_wrt1 + diff_r2_wrt1 * cv.b * 10 ** 6 + cv.r2 * diff_b_wrt1 * 10 ** 6))
            diff_t2_sol_wrt2 = (1. / (cv.v_vapour + cv.r1 * cv.b * 10 ** 6) * (
                        diff_vv_wrt2 + diff_r1_wrt2 * cv.b * 10 ** 6 + cv.r1 * diff_b_wrt2 * 10 ** 6) - 1 / (
                                            cv.v_vapour + cv.r2 * cv.b * 10 ** 6) * (
                                            diff_vv_wrt2 + diff_r2_wrt2 * cv.b * 10 ** 6 + cv.r2 * diff_b_wrt2 * 10 ** 6))

        if cv.v_liq == 0:
            diff_t3_sol_wrt1 = 0
            diff_t3_sol_wrt2 = 0
        elif (cv.v_liq + cv.r1 * cv.b * 10 ** 6) <= 0 or (cv.v_liq + cv.r2 * cv.b * 10 ** 6) <= 0 or (
                (cv.v_liq + cv.r1 * cv.b * 10 ** 6) / ((cv.v_liq + cv.r2 * cv.b * 10 ** 6))) < 0:
            diff_t3_sol_wrt1 = 0.
            diff_t3_sol_wrt2 = 0.
        else:
            diff_t3_sol_wrt1 = (1. / (cv.v_liq + cv.r1 * cv.b * 10 ** 6) * (
                        diff_vl_wrt1 + diff_r1_wrt1 * cv.b * 10 ** 6 + cv.r1 * diff_b_wrt1 * 10 ** 6) - 1 / (
                                            cv.v_liq + cv.r2 * cv.b * 10 ** 6) * (
                                            diff_vl_wrt1 + diff_r2_wrt1 * cv.b * 10 ** 6 + cv.r2 * diff_b_wrt1 * 10 ** 6))
            diff_t3_sol_wrt2 = (1. / (cv.v_liq + cv.r1 * cv.b * 10 ** 6) * (
                        diff_vl_wrt2 + diff_r1_wrt2 * cv.b * 10 ** 6 + cv.r1 * diff_b_wrt2 * 10 ** 6) - 1 / (
                                            cv.v_liq + cv.r2 * cv.b * 10 ** 6) * (
                                            diff_vl_wrt2 + diff_r2_wrt2 * cv.b * 10 ** 6 + cv.r2 * diff_b_wrt2 * 10 ** 6))

        diff_del_wrt1 = diff_t1_sol_wrt1 * (cv.t2 - cv.t3) + cv.t1 * (diff_t2_sol_wrt1 - diff_t3_sol_wrt1)
        diff_del_wrt2 = diff_t1_sol_wrt2 * (cv.t2 - cv.t3) + cv.t1 * (diff_t2_sol_wrt2 - diff_t3_sol_wrt2)

        diff_sol_wrt1 = 0. if cv.del_ui_by_vi == 0 else (
                    0.5 * np.sign(cv.del_ui_by_vi) * diff_del_wrt1 * (np.abs(cv.del_ui_by_vi)) ** (-0.5))
        diff_sol_wrt2 = 0. if cv.del_ui_by_vi == 0 else (
                    0.5 * np.sign(cv.del_ui_by_vi) * diff_del_wrt2 * (np.abs(cv.del_ui_by_vi)) ** (-0.5))

        diff_wrt1 = self.scales[0] * (2 * (ov.solubility - cv.solubility) / ov.solubility ** 2) * (-1) * diff_sol_wrt1 + \
                    self.scales[1] * (2 * (ov.v_liq - cv.v_liq) / ov.v_liq ** 2) * (-1) * diff_vl_wrt1 + self.scales[
                        2] * 2 * (max(0, (cv.ph_diff - 10 ** -7))) * (
                        0 if (cv.ph_diff - 10 ** -7) <= 0 else diff_ph_diff_wrt1)
        diff_wrt2 = self.scales[0] * (2 * (ov.solubility - cv.solubility) / ov.solubility ** 2) * (-1) * diff_sol_wrt2 + \
                    self.scales[1] * (2 * (ov.v_liq - cv.v_liq) / ov.v_liq ** 2) * (-1) * diff_vl_wrt2 + self.scales[
                        2] * 2 * (max(0, (cv.ph_diff - 10 ** -7))) * (
                        0 if (cv.ph_diff - 10 ** -7) <= 0 else diff_ph_diff_wrt2)

        diff_wrt1 = (np.sign(diff_wrt1) * 10 ** 7) if np.abs(diff_wrt1) > 10 ** 7 else diff_wrt1
        diff_wrt2 = (np.sign(diff_wrt2) * 10 ** 7) if np.abs(diff_wrt2) > 10 ** 7 else diff_wrt2

        return np.array([diff_wrt1, diff_wrt2])


class CalculatedValues(object):
    def __init__(self, parent, df, normalize=False):
        self.__dict__ = parent.__dict__.copy()
        self._prepare(df, normalize)

    def _prepare(self, df, normalize):
        if not normalize:
            row = self._intermediate(df)
            self.row = row
        else:
            df = self._final(df)
            self.df = df

    def _final(self, df_o):
        df = df_o.copy(deep=True)
        for index, row in df.iterrows():
            row = self._intermediate(row)
            for col in row.keys():
                df.loc[df.seq == row.seq, col] = row[col]
        df["solubility_observed"] = df_o.solubility
        return df

    def _intermediate(self, row_o):
        row = row_o.copy(deep=True)
        row["alpha_eos"] = row.alpha_pr * row.alpha_multiplier
        row["diff_alpha_wrt_temp"] = row.alpha_eos * (((2. * row.alpha_m - 2.) / (
                    row.temperature / row.tc_k) + row.alpha_l * (-2 * row.alpha_m) * (row.temperature / row.tc_k) ** (
                                                                   2 * row.alpha_m - 1)) / row.tc_k)
        row = self._calc(row)
        row = self._zlzv(row)
        row = self._cost(row, row_o)
        return row

    def _calc(self, row):
        row["beta"] = row.beta_est * row.beta_multiplier
        row["om_b"] = row.om_b_min if (row.zc_final - row.beta) < row.om_b_min else (row.zc_final - row.beta)
        row["om_c"] = (1. - row.beta) ** 2 * (row.beta - 0.25)
        row["om_d"] = row.zc_final - (1. - row.beta) / 2.
        row["om_a"] = (1. - row.beta) ** 3
        row["b"] = row.om_b * gasConstant * row.tc_k / (row.pc_mpa * 10 ** 6)
        row["c"] = row.om_c * (gasConstant * row.tc_k / (row.pc_mpa * 10 ** 6)) ** 2
        row["d"] = row.om_d * gasConstant * row.tc_k / (row.pc_mpa * 10 ** 6)
        row["ac"] = (row.om_a * gasConstant ** 2 * row.tc_k ** 2 / (row.pc_mpa * 10 ** 6))
        row["ac_alpha"] = (row.ac * row.alpha_eos)
        row["u"] = 0 if row.b == 0 else ((-2. * row.d) / row.b)
        row["w"] = 0 if row.b == 0 else ((row.d ** 2 + row.c) / row.b ** 2)
        return row

    def _cost(self, row, ov):
        row["v_liq"] = row.ZL * gasConstant * row.temperature / pressure
        row["v_vapour"] = row.ZV * gasConstant * row.temperature / pressure
        row["t1"] = 0 if row.v_liq == 0 or (row.b * (row.r2 - row.r1)) == 0 else ((1. / row.v_liq) * (
                    (row.ac_alpha - row.temperature * row.ac * row.diff_alpha_wrt_temp) / (row.b * (row.r2 - row.r1))))
        if row.v_vapour == 0:
            row["t2"] = 0
        elif (row.v_vapour + row.r1 * row.b * 10 ** 6) <= 0 or (row.v_vapour + row.r2 * row.b * 10 ** 6) <= 0 or (
                (row.v_vapour + row.r1 * row.b * 10 ** 6) / ((row.v_vapour + row.r2 * row.b * 10 ** 6))) < 0:
            row["t2"] = -200.
        else:
            row["t2"] = (
                np.log((row.v_vapour + row.r1 * row.b * 10 ** 6) / ((row.v_vapour + row.r2 * row.b * 10 ** 6))))

        if row.v_liq == 0:
            row["t3"] = 0
        elif (row.v_liq + row.r1 * row.b * 10 ** 6) <= 0 or (row.v_liq + row.r2 * row.b * 10 ** 6) <= 0 or (
                (row.v_liq + row.r1 * row.b * 10 ** 6) / ((row.v_liq + row.r2 * row.b * 10 ** 6))) < 0:
            row["t3"] = -1000.
        else:
            row["t3"] = (np.log((row.v_liq + row.r1 * row.b * 10 ** 6) / ((row.v_liq + row.r2 * row.b * 10 ** 6))))

        row["del_ui_by_vi"] = row.t1 * (row.t2 - row.t3)
        row["solubility"] = 100. if row.del_ui_by_vi == 0 else np.sqrt(row.del_ui_by_vi)
        return row

    def _zlzv(self, row):
        row["r1"] = 0 if (row.u ** 2 - 4 * row.w) <= 0 else (row.u - np.sqrt(row.u ** 2 - 4 * row.w)) / 2
        row["r2"] = 0 if (row.u ** 2 - 4 * row.w) <= 0 else (row.u + np.sqrt(row.u ** 2 - 4 * row.w)) / 2
        row["A"] = row.ac_alpha * pressure * 10 ** 6 / (gasConstant ** 2 * row.temperature ** 2)
        row["B"] = (row.b * pressure * 10 ** 6 / (gasConstant * row.temperature))
        row["a1"] = (row.u * row.B - row.B - 1)
        row["a2"] = (row.A + row.w * row.B ** 2 - row.u * row.B - row.u * row.B ** 2)
        row["a3"] = -1. * (row.A * row.B + row.w * row.B ** 2 + row.w * row.B ** 3)
        row["Q"] = (3 * row.a2 - row.a1 ** 2) / 9
        row["L"] = (9 * row.a1 * row.a2 - 27 * row.a3 - 2 * row.a1 ** 3) / 54
        row["D"] = row.Q ** 3 + row.L ** 2
        row["S1"] = (-1. * (-1. * (row.L + np.sqrt(row.D))) ** (1. / 3.)) if (row.L + np.sqrt(row.D)) < 0 else (
                                                                                                                           row.L + np.sqrt(
                                                                                                                       row.D)) ** (
                                                                                                                           1. / 3.)
        row["S2"] = (-1. * (-1. * (row.L - np.sqrt(row.D))) ** (1. / 3.)) if (row.L - np.sqrt(row.D)) < 0 else (
                                                                                                                           row.L - np.sqrt(
                                                                                                                       row.D)) ** (
                                                                                                                           1. / 3.)
        row["Z1"] = 0 if row.Q >= 0 else 2. * np.sqrt(-row.Q) * np.cos(1. / 3. * (np.arccos(
            1. if np.abs(row.L / np.sqrt(-row.Q ** 3)) > 1 else row.L / np.sqrt(
                -row.Q ** 3))) + 2 * np.pi / 3.) - row.a1 / 3.
        row["Z2"] = 0 if row.Q >= 0 else 2. * np.sqrt(-row.Q) * np.cos(1. / 3. * (np.arccos(
            1. if np.abs(row.L / np.sqrt(-row.Q ** 3)) > 1 else row.L / np.sqrt(
                -row.Q ** 3))) + 4 * np.pi / 3.) - row.a1 / 3.
        row["Z3"] = 0 if row.Q >= 0 else 2. * np.sqrt(-row.Q) * np.cos(1. / 3. * (
            np.arccos(1. if np.abs(row.L / np.sqrt(-row.Q ** 3)) > 1 else row.L / np.sqrt(-row.Q ** 3)))) - row.a1 / 3.
        row["Z1_d"] = row.S1 + row.S2 - row.a1 / 3.
        row["Z1_dd"] = ((-1. * (-1. * row.L) ** 1. / 3.) if row.L < 0 else row.L ** (1. / 3.)) * 2. - row.a1 / 3.
        row["Z2_dd"] = (((-1. * (-1. * row.L) ** 1. / 3.) if row.L < 0 else row.L ** (1. / 3.)) + row.a1 / 3.) * -1
        row["Z3_dd"] = row.Z2_dd
        row["ZL"] = self._min([row.Z1, row.Z2, row.Z3]) if row.D < 0 else (
            self._min([row.Z1_dd, row.Z2_dd, row.Z3_dd]) if row.D == 0 else row.Z1_d)
        row["ZV"] = self._max([row.Z1, row.Z2, row.Z3]) if row.D < 0 else (
            self._max([row.Z1_dd, row.Z2_dd, row.Z3_dd]) if row.D == 0 else 0)
        row["phl"] = self._fugacity(row.r1, row.r2, row.A, row.B, row.ZL, -1.5)
        row["phv"] = self._fugacity(row.r1, row.r2, row.A, row.B, row.ZV, 1.)
        row["ph_diff"] = np.abs(row.phl - row.phv)
        return row

    def _fugacity(self, r1, r2, A, B, root, penalty):
        if (root - B) <= 0 or (B * (r2 - r1)) == 0 or (root + r1 * B) == 0 or (root + r2 * B) == 0 or (
                (root + r2 * B) * (root + r1 * B)) < 0:
            return penalty
        else:
            return ((root - 1) - np.log(root - B) - (A / (B * (r2 - r1))) * np.log((root + r2 * B) / (root + r1 * B)))

    def _min(self, data):
        data = [x for x in data if x > 0]
        if len(data) > 0:
            return min(data)
        return 0

    def _max(self, data):
        data = [x for x in data if x > 0]
        if len(data) > 0:
            return max(data)
        return 0


class ObservedValues(object):
    def __init__(self, parent):
        self.__dict__ = parent.__dict__.copy()
        self._prepare()

    def _prepare(self):
        df = self.get_input_data()
        df = self.alpha(df)
        self.df = df

    def alpha(self, df_o):
        df = df_o.copy(deep=True)
        df["tbr"] = df.bp_final1 / df.tc_k
        df["tr"] = 0.7
        df["tcr"] = 1.
        df["t1"] = df[['tbr', 'tr']].min(axis=1)
        df["t2"] = df[['tbr', 'tr']].max(axis=1)
        df["t3"] = df.tcr
        df["alpha_eos1"] = df.apply(lambda row: row.alpha_eos_bp if row.t1 == row.tbr else row.alpha_eos_tr, axis=1)
        df["alpha_eos2"] = df.apply(lambda row: row.alpha_eos_tr if row.t2 == row.tr else row.alpha_eos_bp, axis=1)
        df["alpha_eos3"] = 1.
        for col in ["alpha_eos1", "alpha_eos2", "alpha_eos3"]:
            df["ln_%s" % (col)] = np.log(df[col])
        df["y1"] = np.log(df.alpha_eos1)
        df["y2"] = np.log(df.alpha_eos2)
        df["y3"] = np.log(df.alpha_eos3)
        df["xi"] = df[["t1", "t2", "t3"]].sum(axis=1)
        df["yi"] = df[["ln_alpha_eos1", "ln_alpha_eos2", "ln_alpha_eos3"]].sum(axis=1)
        df["xiyi"] = df.t1 * df.ln_alpha_eos1 + df.t2 * df.ln_alpha_eos2 + df.t3 * df.ln_alpha_eos3
        df["xi2"] = df.t1 ** 2 + df.t2 ** 2 + df.t3 ** 2
        df["intercept"] = (df.xi * df.xiyi - df.yi * df.xi2) / (df.xi ** 2 - 3. * df.xi2)
        df["slope"] = (df.yi - 3. * df.intercept) / df.xi
        df["alpha_pr_req_tr"] = ((self.temperature / df.tc_k) ** (2. * (df.alpha_m - 1.)) * (
            np.exp(df.alpha_l * (1. - (self.temperature / df.tc_k) ** (2 * df.alpha_m)))))
        df["expected_alpha"] = np.exp(df.slope * (self.temperature / df.tc_k) + df.intercept)
        df["expected_alpha_multiplier"] = df.expected_alpha / df.alpha_pr_req_tr
        df["alpha_multiplier_min"] = df.expected_alpha * 0.92 / df.alpha_pr_req_tr
        df["alpha_multiplier_max"] = df.expected_alpha * 6. / df.alpha_pr_req_tr
        df_o[["alpha_multiplier_min", "alpha_multiplier_max", "expected_alpha_multiplier"]] = df[
            ["alpha_multiplier_min", "alpha_multiplier_max", "expected_alpha_multiplier"]]
        df_o["alpha_pr"] = df.alpha_pr_req_tr
        self.eos_specific_vars(df_o)
        df_o["temperature"] = self.temperature
        return df_o

    def eos_specific_vars(self, df):
        per_change = (df.vb_poas_4 - df.vb) / df.vb_poas_4 * 100.
        df["beta_multiplier"] = df.beta_multiplier * (1. + per_change / 100.)
        df["beta_multiplier_min"] = df.beta_multiplier * 0.7
        df["beta_multiplier_max"] = df.beta_multiplier * 1.5
        df["beta_est"] = 0.79132 * df.zc_final - 0.02207
        df["om_b_min"] = (0.02207 + 0.20868 * df.zc_final) * 0.215

    def get_input_data(self):
        cm = self.get_cm_data()
        eos = self.get_eos_data(self.eos)
        eos_tr = self.get_omega_tuning_data(self.eos)
        df = pd.concat([cm, eos, eos_tr], axis=1)

        poas4_tr = self.get_omega_tuning_data("poas_4")
        df["omega_final"] = poas4_tr["omega_final"]
        df["vb_poas_4"] = poas4_tr["vb_poas_4"]
        df = self.get_volume_data(df)
        return df

    def get_volume_data(self, df):
        poas_4 = fetch("MBP", self.args.collection, logger=self.logger, Property="MCOutput",
                       Identifier="SolubilityTuning", Eos="poas_4", Temperature=self.temperature)
        df["scn"] = df.index
        df.reset_index(inplace=True, drop=True)
        df[["v_liq", "solubility"]] = poas_4[["v_liq", "solubility"]]
        return df

    def get_eos_data(self, eos):
        eos = eosbp_data(self.args.collection, self.logger, eos)
        eos["alpha_eos_bp"] = eos.alpha_multiplier * eos.alpha_pr
        cols = ["seq", "bp_final1", "tc_k", "alpha_l", "alpha_m", "alpha_multiplier", "tc_k", "beta_multiplier",
                "alpha_eos_bp", "vb"]
        drop_not_in_cols(eos, cols)
        eos.rename(columns={"alpha_multiplier": "alpha_multiplier_bp"}, inplace=True)
        return eos

    def get_cm_data(self):
        df = cm_data(self.args.collection, self.logger)
        df.loc[df.tcbytb < 1.0001, "tcbytb"] = 1.0001
        cols = ["mf", "component", "tcbytb", "pc_mpa", "zc_final", "vc_final"]
        drop_not_in_cols(df, cols)
        return df

    def get_omega_tuning_data(self, i_eos):
        eos = omega_data(self.args.collection, self.logger, i_eos)
        eos["alpha_eos_tr"] = eos.alpha_multiplier * eos.alpha_pr
        cols = ["omega_final", "alpha_eos_tr", "vb"]
        drop_not_in_cols(eos, cols)
        eos.rename(columns={"alpha_multiplier": "alpha_multiplier_tr", "vb": "vb_%s" % i_eos}, inplace=True)
        return eos
