import os
from collections import OrderedDict

import pandas as pd


def compare(golden_data_dir, debug_data_dir, well, bias):
    golden_data_dir = os.path.join(golden_data_dir, well, bias)
    debug_data_dir = os.path.join(debug_data_dir, well, bias)

    debug_modules = list_dirs(debug_data_dir)
    golden_modules = list_dirs(golden_data_dir)
    uniq_modules = list(set(debug_modules + golden_modules))

    output = OrderedDict()
    cols = ["module", "excel", "sheet", "golden_module", "golden_excel", "debug_module", "debug_excel", "match"]
    for key in cols:
        output[key] = list()

    for module in uniq_modules:
        debug_excels = list_files(os.path.join(debug_data_dir, module))
        golden_excels = list_files(os.path.join(golden_data_dir, module))
        uniq_excels = list(set(debug_excels + golden_excels))
        for excel in uniq_excels:
            debug_dfs = read_excel(os.path.join(debug_data_dir, module, excel))
            golden_dfs = read_excel(os.path.join(golden_data_dir, module, excel))
            unique_sheets = list(set(golden_dfs.keys() + debug_dfs.keys()))

            for sheet in unique_sheets:

                output["module"].append(module)
                output["excel"].append(excel)
                output["sheet"].append(sheet)

                output["golden_module"].append(True if module in golden_modules else False)
                output["golden_excel"].append(True if excel in golden_excels else False)

                output["debug_module"].append(True if module in debug_modules else False)
                output["debug_excel"].append(True if excel in debug_excels else False)

                if sheet in golden_dfs.keys() and sheet in debug_dfs.keys():
                    output["match"].append((golden_dfs[sheet]).equals(debug_dfs[sheet]))
                else:
                    output["match"].append("False")

    df = pd.DataFrame(output)
    return df


def read_excel(file):
    if os.path.exists(file):
        return pd.read_excel(file, None)
    return {}


def list_dirs(dir):
    if os.path.exists(dir):
        return [dI for dI in os.listdir(dir) if os.path.isdir(os.path.join(dir, dI))]
    return []


def list_files(dir):
    if os.path.exists(dir):
        return [dI for dI in os.listdir(dir) if not os.path.isdir(os.path.join(dir, dI))]
    return []


if __name__ == "__main__":
    golden_data_dir = "../../test/golden_data"
    debug_data_dir = "../../debug"
    well = "ALUMM"
    bias = "general"
    print compare(golden_data_dir, debug_data_dir, well, bias)
