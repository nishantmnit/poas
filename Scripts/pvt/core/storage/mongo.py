import datetime
import pandas as pd
from Lib.DBLib import getDbCon


def write(df_o, db, well, logger=None, **kwargs):
    df = df_o.copy(deep=True)
    if "Identifier" in kwargs.keys():
        calc_error(df, kwargs["Identifier"])
    df["index"] = df.index  # preserve index to be set again in fetch
    df.reset_index(inplace=True, drop=True)  # there can be duplicates in index
    df.index = df.index.map(str)
    df["seq"] = range(0, df.shape[0])
    data = df.to_dict()
    for key, value in kwargs.items():
        data[key] = value
    for k, v in data.items():
        k = k.encode('ascii', 'ignore')
        data[k] = v
    data["CreatedAt"] = datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
    data["CreatedBy"] = 'main'
    conn = get_connection(db, well)
    delete_one(db, well, logger, **kwargs)
    r = conn.insert(data, check_keys=False)
    if logger is not None:
        logger.info('PVT_WRITE:{%s, "id": "%s"}' % (kwargs, r))


def calc_error(df, Identifier):
    if Identifier == "mass":
        df["error"] = ((df["expmass"] - df["mass"]) / df["expmass"]) ** 2
    elif Identifier == "density":
        df["error"] = ((df["expdensity"] - df["density"]) / df["expdensity"]) ** 2
    elif Identifier == "bp":
        df["error"] = ((df["expbp"] - df["bp"]) / df["expbp"]) ** 2
    elif Identifier == "ri":
        df["error"] = ((df["expri"] - df["ri"]) / df["expri"]) ** 2


def delete_one(db, well, logger, **kwargs):
    try:
        prev_data = find_one(db, well, **kwargs)
        prev_doc_id = prev_data["_id"]
        if logger is not None:
            logger.info('PVT_DELETE:{%s, "id": "%s"}' % (kwargs, prev_doc_id))
    except:
        if logger is not None:
            logger.info('PVT_DELETE:{%s, "id": "%s"}' % (kwargs, "Not Found"))
    conn = get_connection(db, well)
    conn.remove(kwargs)


def delete_all(db, well, logger, **kwargs):
    if logger is not None:
        logger.info('PVT_DELETE:{%s}' % (kwargs))
    conn = get_connection(db, well)
    conn.remove(kwargs)


def fetch(db, well, logger, **kwargs):
    gen = find_one(db, well, **kwargs)
    df = pd.DataFrame(index=gen["index"].values())
    for key in gen.keys():
        if key not in ["CreatedAt", "CreatedBy", "_id", "index"] + kwargs.keys():
            if type(gen[key]) is dict:
                df[key] = gen[key].values()
            else:
                df[key] = gen[key]
    df.sort_values(by=["seq"], inplace=True)
    # logger.info("Returning single df with %s rows" % df.shape[0])
    return df


def find_one(db, well, **kwargs):
    conn = get_connection(db, well)
    data = list()
    for key, value in kwargs.items():
        data.append({key: value})
    gen = conn.find({'$and': data}) if len(data) > 0 else conn.find()
    if gen.count() > 1:
        raise RuntimeError("Found more than 1 document = %s. Please check and re-run." % data)
    if gen.count() == 0:
        raise RuntimeError("Found no document = %s. Please check and re-run." % data)
    return gen[0]


def get_connection(db, well):
    db_con = getDbCon(db)
    conn = db_con[well]
    return conn
