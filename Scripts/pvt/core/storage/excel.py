import os
import pandas as pd
from Lib.Utils import getLogger
import Scripts.pvt.settings

logger = getLogger(qualname='excel writer')


def write_excel(debug, save_to_dir, filename, sheets, dfs):
    if not debug:
        return True
    ensure_dir(save_to_dir)
    filename = get_file(save_to_dir, filename)
    writer = pd.ExcelWriter(filename, engine="openpyxl")
    for i in range(len(sheets)):
        dfs[i].to_excel(writer, sheet_name=sheets[i])
    writer.save()
    logger.info("PVT_DEBUG: Saved debug excel - %s" % filename)


def write_csv(debug, save_to_dir, filename, df, index=True):
    if not debug:
        return True
    ensure_dir(save_to_dir)
    filename = get_file(save_to_dir, filename, csv=True)
    df.to_csv(filename, index=index)
    logger.info("PVT_DEBUG: Saved debug csv - %s" % filename)


def get_file(save_to_dir, filename, csv=False):
    filename = [str(i) for i in filename]
    filename = os.path.join(save_to_dir, "_".join(filename))
    filename = "%s.xlsx" % filename if not csv else "%s.csv" % filename
    return filename


def ensure_dir(debug_dir):
    if not os.path.isdir(debug_dir):
        os.makedirs(debug_dir)
