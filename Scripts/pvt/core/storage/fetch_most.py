from Scripts.pvt.core.storage.mongo import fetch, get_connection
import pandas as pd
import math
from Lib.Utils import getLogger

logger = getLogger("fetch most")


def validated_data(well, logger):
    df = fetch("MBP", well, logger=logger, Property="MCOutput", Identifier="ValidatedData",
               PlusOrIndividual="Individual")
    dfplus = fetch("MBP", well, logger=logger, Property="MCOutput", Identifier="ValidatedData", PlusOrIndividual="Plus")
    return df, dfplus


def input_validated_data(well, logger):
    df = fetch("MBP", well, logger=logger, Property="MCOutput", Identifier="InputValidatedData",
               PlusOrIndividual="Individual")
    dfplus = fetch("MBP", well, logger=logger, Property="MCOutput", Identifier="InputValidatedData",
                   PlusOrIndividual="Plus")
    return df, dfplus


def cm_data(well, logger):
    df = fetch("MBP", well, logger=logger, Property="MCOutput", Identifier="ComponentModeling")
    return df


def eosbp_data(well, logger, eos):
    df = fetch("MBP", well, logger=logger, Property="MCOutput", Identifier="EOS", Eos=eos)
    return df


def omega_data(well, logger, eos):
    df = fetch("MBP", well, logger=logger, Property="MCOutput", Identifier="OmegaTuning", Eos=eos, Temperature=0.7)
    return df


def solubility_data(well, logger, eos, temperature):
    df = fetch("MBP", well, logger=logger, Property="MCOutput", Identifier="SolubilityTuning", Eos=eos,
               Temperature=temperature)
    return df


def poas_recommended_density(well, logger):
    conn = get_connection("MBP", well)
    gen = conn.find({'$and': [{'Property': 'MCOutput'}, {'Identifier': "density"}, {'PlusOrIndividual': "Individual"},
                              {"POASRecommended": 1}, {'Distribution': {'$in': ['POAS', 'POASc']}}]})[0]
    df = pd.DataFrame(index=gen["index"].values())
    for key in gen.keys():
        if key not in ["CreatedAt", "CreatedBy", "_id", "index", "Property", "Identifier", "PlusOrIndividual",
                       "POASRecommended", "Distribution", "Recommended", "Solver"]:
            if type(gen[key]) is dict:
                df[key] = gen[key].values()
            else:
                df[key] = gen[key]
    df.sort_values(by=["seq"], inplace=True)
    gen = conn.find({'$and': [{'Property': 'MCOutput'}, {'Identifier': "density"}, {'PlusOrIndividual': "Plus"},
                              {"POASRecommended": 1}, {'Distribution': {'$in': ['POAS', 'POASc']}}]})[0]
    dfplus = pd.DataFrame(index=gen["index"].values())
    for key in gen.keys():
        if key not in ["CreatedAt", "CreatedBy", "_id", "index", "Property", "Identifier", "PlusOrIndividual",
                       "POASRecommended", "Distribution", "Recommended", "Solver"]:
            if type(gen[key]) is dict:
                dfplus[key] = gen[key].values()
            else:
                dfplus[key] = gen[key]
    dfplus.sort_values(by=["seq"], inplace=True)
    return df, dfplus


def recommended(well, logger, what):
    df = fetch("MBP", well, logger=logger, Property="MCOutput", Identifier=what, PlusOrIndividual="Individual",
               Recommended=1)
    if what not in ["bp", "ri"]:
        dfplus = fetch("MBP", well, logger=logger, Property="MCOutput", Identifier=what, PlusOrIndividual="Plus",
                       Recommended=1)
        return df, dfplus
    return df


def set_recommended(well, logger, Identifier):
    conn = get_connection("MBP", well)
    if Identifier in ("bp", "ri"):
        conn.update({'$and': [{'Property': 'MCOutput'}, {'Identifier': Identifier}]}, {"$set": {"Recommended": 0}},
                    **{'upsert': False, 'multi': True})
        conn.update(
            {'$and': [{'Property': 'MCOutput'}, {'Identifier': Identifier}, {"Solver": 1}, {"Distribution": "POAS"}]},
            {"$set": {"Recommended": 1}}, **{'upsert': False, 'multi': True})
        logger.info("PVT_RECOMMENDATION: %s - Distribution = %s, Solver = 1" % (Identifier.upper(), "POAS"))
        return 0

    distributions, solvers, errors, plus_or_ind = list(), list(), list(), list()
    for r in conn.find({'$and': [{'Property': 'MCOutput'}, {'Identifier': Identifier}, {'PlusOrIndividual': 'Plus'}]}):
        distributions.append(r["Distribution"])
        solvers.append(r["Solver"])
        error = 0
        for (k, val) in r["error"].items():
            error += 0 if math.isnan(val) else val
        errors.append(error)
    df = pd.DataFrame(columns=["distribution", "solver", "error"], data=list(zip(distributions, solvers, errors)))
    df["plus_or_ind"] = "plus"

    distributions, solvers, errors, plus_or_ind = list(), list(), list(), list()
    for r in conn.find(
            {'$and': [{'Property': 'MCOutput'}, {'Identifier': Identifier}, {'PlusOrIndividual': 'Individual'}]}):
        distributions.append(r["Distribution"])
        solvers.append(r["Solver"])
        error = 0
        for (k, val) in r["error"].items():
            error += 0 if math.isnan(val) else val
        errors.append(error)
    df1 = pd.DataFrame(columns=["distribution", "solver", "error"], data=list(zip(distributions, solvers, errors)))
    df1["plus_or_ind"] = "ind"
    df = pd.concat([df, df1], axis=0, sort=False)
    df_all = df.groupby(["distribution", "solver"])['error'].agg('sum').reset_index()
    df_all = df_all.loc[df_all.error == df_all.error.min()]
    df_poas = df.loc[df.distribution.isin(["POAS", "POASc"])].groupby(["distribution", "solver"])['error'].agg(
        'sum').reset_index()
    if Identifier == "density" and df_poas.loc[df_poas.distribution == "POASc"].shape[0] > 0:
        df_poas = df_poas.loc[df_poas.distribution == "POASc"]
    else:
        df_poas = df_poas.loc[df_poas.error == df_poas.error.min()]

    conn.update({'$and': [{'Property': 'MCOutput'}, {'Identifier': Identifier}]},
                {"$set": {"Recommended": 0, "POASRecommended": 0}}, **{'upsert': False, 'multi': True})
    conn.update({'$and': [{'Property': 'MCOutput'}, {'Identifier': Identifier},
                          {"Solver": float(df_all.solver.values[0])}, {"Distribution": df_all.distribution.values[0]}]},
                {"$set": {"Recommended": 1}}, **{'upsert': False, 'multi': True})
    logger.info("PVT_RECOMMENDATAION: %s - Distribution = %s, Solver = %s" % (
        Identifier.upper(), df_all.distribution.values[0], df_all.solver.values[0]))

    if df_poas.shape[0] == 1:
        conn.update({'$and': [{'Property': 'MCOutput'}, {'Identifier': Identifier},
                              {"Solver": float(df_poas.solver.values[0])},
                              {"Distribution": df_poas.distribution.values[0]}]},
                    {"$set": {"POASRecommended": 1}}, **{'upsert': False, 'multi': True})
        logger.info("PVT_POAS_RECOMMENDATION: %s - Distribution = %s, Solver = %s" % (
            Identifier.upper(), df_poas.distribution.values[0], df_poas.solver.values[0]))


def get_api_data():
    df = fetch("Central", "APIDATABOOKVALUES", logger=logger)
    return df
