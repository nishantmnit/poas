import numpy as np


def not_in_list(df, cols):
    return [col for col in df.columns if col not in cols]


def drop_not_in_cols(df, cols):
    drop_cols(df, not_in_list(df, cols))


def drop_cols(df, cols):
    df.drop(columns=cols, inplace=True)
