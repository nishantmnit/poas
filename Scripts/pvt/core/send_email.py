import smtplib
import os

from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

default_receivers = ["nishantmnit@gmail.com", "omprakashdas7642@gmail.com"]


def send_email(subject="POAS ALERTS", receiver_email=default_receivers, attachment=None,
               body="This is an automated email from POAS"):
    sender_email = "poas.alerts@gmail.com"
    password = "Chunnu31"

    # Create a multipart message and set headers
    message = MIMEMultipart()
    message["From"] = sender_email
    message["To"] = ", ".join(receiver_email)
    message["Subject"] = subject
    # message["Bcc"] = receiver_email  # Recommended for mass emails

    # Add body to email
    message.attach(MIMEText(body, "plain"))
    if attachment is not None:
        filename = attachment
        # Open PDF file in binary mode
        with open(filename, "rb") as attachment:
            # Add file as application/octet-stream
            # Email client can usually download this automatically as attachment
            part = MIMEBase("application", "octet-stream")
            part.set_payload(attachment.read())

        # Encode file in ASCII characters to send by email    
        encoders.encode_base64(part)

        # Add header as key/value pair to attachment part
        head, tail = os.path.split(filename)
        part.add_header(
            "Content-Disposition",
            "attachment; filename= %s" % tail,
        )

        # Add attachment to message and convert message to string
        message.attach(part)

    text = message.as_string()

    # Log in to server using secure context and send email

    server = smtplib.SMTP_SSL("smtp.gmail.com", 465)
    # server.set_debuglevel(1)
    server.ehlo()
    server.login(sender_email, password)
    server.sendmail(sender_email, receiver_email, text)
    server.close()


if __name__ == "__main__":
    send_email(subject="POAS ALERTS - Test email. Ignore", receiver_email=default_receivers, attachment=None,
               body="This is an automated test email from POAS. Please ignore.")
