import numpy as np
import pandas as pd
from scipy import optimize
from multiprocessing import Pool
from functools import partial

sin = False
high_cubic = False


def calc_interpolate(row, t):
    h00 = (1. + 2. * t) * (1. - t) ** 2
    h10 = t * (1. - t) ** 2
    h01 = t ** 2 * (3. - 2. * t)
    h11 = t ** 2 * (t - 1.)
    y = row.y * h00 + row.x_diff * row.mkf * h10 + row.y1 * h01 + row.x_diff * row.mk1f * h11
    return y


def calc_extrapolated_point(method, coeff, xr, w0=None):
    if method == "order4":
        return coeff[0] + coeff[1] * xr + coeff[2] * xr ** 2 + coeff[3] * xr ** 3 + coeff[4] * xr ** 4
    elif method == "exponential":
        return np.exp(coeff[0] + coeff[1] * xr) * coeff[2]
    elif method == "cubic":
        return coeff[0] + coeff[1] * xr + coeff[2] * xr ** 2 + coeff[3] * xr ** 3
    elif method == "quad":
        return coeff[0] + coeff[1] * xr + coeff[2] * xr ** 2
    elif method == "linear":
        return coeff[0] + coeff[1] * xr
    elif method == "power":
        return np.exp(coeff[0] + coeff[1] * np.log(xr)) * coeff[2]
    elif method == "logrithmic":
        return coeff[0] + coeff[1] * np.log(xr)
    elif method == "sin":
        return coeff[0] + coeff[1] * np.cos(w0 * xr) + coeff[2] * np.sin(w0 * xr)


def order4(df):
    mat1 = pd.DataFrame([1 for x in range(df.shape[0])], columns=["dummy"])
    mat1["x"] = df.xr
    mat1["x2"] = df.xr ** 2
    mat1["x3"] = df.xr ** 3
    mat1["x4"] = df.xr ** 4
    mat2 = mat1.T.dot(mat1)
    try:
        mat2_inv = pd.DataFrame(np.linalg.inv(mat2.values), mat2.columns, mat2.index)
    except:
        mat2_inv = pd.DataFrame(np.linalg.pinv(mat2.values), mat2.columns, mat2.index)
    mat3 = mat1.T.dot(df.y)
    mat4 = mat2_inv.dot(mat3).tolist()
    return mat4


def cubic(df):
    mat1 = pd.DataFrame([1 for x in range(df.shape[0])], columns=["dummy"])
    mat1["x"] = df.xr
    mat1["x2"] = df.xr ** 2
    mat1["x3"] = df.xr ** 3
    mat2 = mat1.T.dot(mat1)
    try:
        mat2_inv = pd.DataFrame(np.linalg.inv(mat2.values), mat2.columns, mat2.index)
    except:
        mat2_inv = pd.DataFrame(np.linalg.pinv(mat2.values), mat2.columns, mat2.index)
    mat3 = mat1.T.dot(df.y)
    mat4 = mat2_inv.dot(mat3).tolist()
    return mat4


def quad(df):
    mat1 = pd.DataFrame([1 for x in range(df.shape[0])], columns=["dummy"])
    mat1["x"] = df.xr
    mat1["x2"] = df.xr ** 2
    mat2 = mat1.T.dot(mat1)
    try:
        mat2_inv = pd.DataFrame(np.linalg.inv(mat2.values), mat2.columns, mat2.index)
    except:
        mat2_inv = pd.DataFrame(np.linalg.pinv(mat2.values), mat2.columns, mat2.index)
    mat3 = mat1.T.dot(df.y)
    mat4 = mat2_inv.dot(mat3).tolist()
    return mat4


def linear(df):
    mat1 = pd.DataFrame([1 for x in range(df.shape[0])], columns=["dummy"])
    mat1["x"] = df.xr
    mat2 = mat1.T.dot(mat1)
    try:
        mat2_inv = pd.DataFrame(np.linalg.inv(mat2.values), mat2.columns, mat2.index)
    except:
        mat2_inv = pd.DataFrame(np.linalg.pinv(mat2.values), mat2.columns, mat2.index)
    mat3 = mat1.T.dot(df.y)
    mat4 = mat2_inv.dot(mat3).tolist()
    return mat4


def power(df):
    mat1 = pd.DataFrame([1 for x in range(df.shape[0])], columns=["dummy"])
    mat1["x"] = np.log(df.xr)
    mat2 = mat1.T.dot(mat1)
    try:
        mat2_inv = pd.DataFrame(np.linalg.inv(mat2.values), mat2.columns, mat2.index)
    except:
        mat2_inv = pd.DataFrame(np.linalg.pinv(mat2.values), mat2.columns, mat2.index)

    multiplier = 1.
    if np.all(df.y < 0):
        multiplier = -1.
        df["y"] = -1. * df.y

    df["log_y"] = np.log(df.y)
    mat3 = mat1.T.dot(df.log_y)
    mat4 = mat2_inv.dot(mat3).tolist()
    mat4.append(multiplier)
    return mat4


def logrithmic(df):
    mat1 = pd.DataFrame([1 for x in range(df.shape[0])], columns=["dummy"])
    mat1["x"] = np.log(df.xr)
    mat2 = mat1.T.dot(mat1)
    try:
        mat2_inv = pd.DataFrame(np.linalg.inv(mat2.values), mat2.columns, mat2.index)
    except:
        mat2_inv = pd.DataFrame(np.linalg.pinv(mat2.values), mat2.columns, mat2.index)
    mat3 = mat1.T.dot(df.y)
    mat4 = mat2_inv.dot(mat3).tolist()

    return mat4


def exponential(df):
    mat1 = pd.DataFrame([1 for x in range(df.shape[0])], columns=["dummy"])
    mat1["x"] = df.xr
    mat2 = mat1.T.dot(mat1)
    try:
        mat2_inv = pd.DataFrame(np.linalg.inv(mat2.values), mat2.columns, mat2.index)
    except:
        mat2_inv = pd.DataFrame(np.linalg.pinv(mat2.values), mat2.columns, mat2.index)

    multiplier = 1.
    if np.all(df.y < 0):
        multiplier = -1.
        df["y"] = -1. * df.y

    df["log_y"] = np.log(df.y)
    mat3 = mat1.T.dot(df.log_y)
    mat4 = mat2_inv.dot(mat3).tolist()
    mat4.append(multiplier)
    return mat4


def divide_by_non_zero(a, b):
    c = np.abs(a)
    return np.divide(c, b, out=np.zeros_like(c), where=b != 0)


def _cost(vars, df, final=False):
    if type(df) is list: df = df[0]
    df = df.copy(deep=True)
    t = vars[0]
    f0 = 1. / t
    w0 = 2. * np.pi * f0
    df["z1"] = df.y_cubic * np.cos(w0 * df.xr)
    df["z2"] = df.y_cubic * np.sin(w0 * df.xr)
    coeff = [df.y_cubic.sum() / df.shape[0], 2. * df.z1.sum() / df.shape[0], 2. * df.z2.sum() / df.shape[0]]
    df["y_calc"] = calc_extrapolated_point("sin", coeff, df.xr.values, w0)
    df["error"] = divide_by_non_zero(df.y_cubic - df.y_calc, df.y_cubic)
    if final: return coeff, w0
    return df.error.sum()


def calc_linear(df_sin):
    y_max = df_sin.loc[np.isnan(df_sin.y_cubic)].y_sin.max()
    y_max_index = df_sin.loc[np.isnan(df_sin.y_cubic) & (df_sin.y_sin == y_max)].index.min()
    xr_max = df_sin.loc[df_sin.index == y_max_index].xr.values[0]

    xr_last = df_sin.loc[~np.isnan(df_sin.y_cubic)].xr.max()
    y_last = df_sin.loc[df_sin.xr == xr_last, "y_cubic"].values[0]

    df_sin["y_linear"] = y_last + (y_max - y_last) / (xr_max - xr_last) * (df_sin.xr - xr_last)
    df_sin["y_rough"] = df_sin.y_sin * 0.7 + df_sin.y_linear * 0.3
    df_sin.loc[df_sin.xr == xr_last, "y_rough"] = y_last
    return df_sin


def validate(x, y):
    if type(x) is not list or type(y) is not list:
        raise ValueError("x and y should be a list")
    if len(x) != len(y):
        raise ValueError("x and y should be of same length")
    if len(x) < 3:
        raise ValueError("Minimum 3 data points are needed for interpolation.")


def calc_params(df):
    df["x1"] = df.x.shift(-1)
    df["y1"] = df.y.shift(-1)
    df.loc[df.x == df.x1, "del_j"] = 0
    df.loc[df.x != df.x1, "del_j"] = (df.loc[df.x != df.x1].y1 - df.loc[df.x != df.x1].y) / (
            df.loc[df.x != df.x1].x1 - df.loc[df.x != df.x1].x)
    df.loc[df.index == df.index.max(), "del_j"] = df.loc[df.index == df.index.max() - 1, "del_j"].values[0]
    df["del_j1"] = df.del_j.shift(1)
    df["mk"] = df.apply(
        lambda row: 0 if row.del_j * row.del_j1 < 0 or row.del_j == 0 else (row.del_j + row.del_j1) / 2., axis=1)
    df.loc[df.index == df.index.min(), "mk"] = df.loc[df.index == df.index.min()].del_j
    df["mk1"] = df.mk.shift(-1)
    df.loc[df.del_j == 0, "mk1"] = 0
    df.loc[df.index == df.index.max(), "mk1"] = 0
    df["alpha_k"] = df.apply(lambda row: 0 if row.mk == 0 or row.del_j == 0 else row.mk / row.del_j, axis=1)
    df["beta_k"] = df.apply(lambda row: 0 if row.mk1 == 0 or row.del_j == 0 else row.mk1 / row.del_j, axis=1)
    df["mdk"] = df.apply(lambda row: 0 if row.alpha_k < 0 else row.mk, axis=1)
    df["mdk1"] = df.apply(lambda row: 0 if row.beta_k < 0 else row.mk1, axis=1)
    df["touk"] = df.apply(lambda row: 0 if (row.alpha_k ** 2 + row.beta_k ** 2) == 0 else 3. / np.sqrt(
        row.alpha_k ** 2 + row.beta_k ** 2), axis=1)
    df["mkf"] = df.apply(
        lambda row: row.touk * row.alpha_k * row.del_j if (row.alpha_k ** 2 + row.beta_k ** 2) > 9 else row.mk,
        axis=1)
    df["mk1f"] = df.apply(
        lambda row: row.touk * row.beta_k * row.del_j if (row.beta_k ** 2 + row.beta_k ** 2) > 9 else row.mk1,
        axis=1)
    df["x_diff"] = df.x1 - df.x
    df.loc[df.index == df.index.max(), "x_diff"] = df.loc[df.index == df.index.max() - 1, "x_diff"].values[0]
    df["x_diff_avg"] = ((df.x_diff + df.x_diff.shift(1)) / 2.).shift(1)


def create_table_1(df):
    calc_params(df)
    last_x = (df.x.tolist())[-2]
    last_x_diff = (df.x_diff.tolist())[-1]
    new_x = [last_x]
    ts = [0]
    new_y = [(df.y.tolist())[-2]]
    for i in range(1, 6):
        t = i * 0.2
        new_x.append(last_x + last_x_diff * t)
        ts.append(t)
        new_y.append(calc_interpolate(df.loc[df.index == df.index.max() - 1].squeeze(), t))

    last_x_diff = new_x[1] - new_x[0]
    last_x = (df.x.tolist())[-3]

    max_points = 1
    while abs(new_x[0] - last_x_diff) <= abs(last_x) and max_points <= 2:
        max_points += 1
        new_x = [new_x[0] - last_x_diff] + new_x
        t = (new_x[0] - (df.x.tolist())[-3]) / (df.x_diff.tolist())[-3]
        ts = [t] + ts
        new_y = [calc_interpolate(df.loc[df.index == df.index.max() - 2].squeeze(), t)] + new_y

    df_out = pd.DataFrame(columns=["x", "y", "t"], data=list(zip(new_x, new_y, ts)))

    df_out["x_diff"] = df_out.x - df_out.x.shift(1)
    m_index = df_out.loc[~np.isnan(df_out.x_diff)].index.min()
    m_x_diff = df_out.loc[df_out.index == m_index].x_diff.values[0]
    df_out["step"] = df_out.x_diff / m_x_diff
    df_out.loc[df_out.index == m_index - 1, "step"] = 1.
    df_out["xr"] = np.round(df_out.step.cumsum(), 2)
    return df_out


def calc_sin(df):
    res = optimize.shgo(_cost, bounds=[[.1, 60.]], args=([df]), iters=5)
    coeff, w0 = _cost(res.x, df, final=True)
    data = [df.xr.max() + 1.]
    while (data[-1]) < 41.999:
        data.append(data[-1] + 1.)
    df = pd.concat([df, pd.DataFrame(columns=["xr"], data=data)], sort=False, ignore_index=True)
    df["y_sin"] = calc_extrapolated_point("sin", coeff, df.xr.values, w0)
    return df


def extrapolate_sin(df, x, table1=False):
    if not table1:
        df = create_table_1(df)

    last_x_index = df.loc[~np.isnan(df.x)].index.max()
    last_row = df.loc[df.index == last_x_index].squeeze()

    find_xr = (x - last_row.x) / last_row.x_diff + last_row.xr

    df_7point = df.loc[df.index > df.index.min()].copy(deep=True)
    df_7point.reset_index(inplace=True, drop=True)
    coeff_7point = cubic(df_7point)
    df_7point["y_calc"] = calc_extrapolated_point("cubic", coeff_7point, df_7point.xr.values)
    df_7point["error"] = divide_by_non_zero((df_7point.y - df_7point.y_calc), df_7point.y)

    df_5point = df.loc[df.index > df.index.min() + 2].copy(deep=True)
    df_5point.reset_index(inplace=True, drop=True)
    coeff_5point = cubic(df_5point)
    df_5point["y_calc"] = calc_extrapolated_point("cubic", coeff_5point, df_5point.xr.values)
    df_5point["error"] = divide_by_non_zero((df_5point.y - df_5point.y_calc), df_5point.y)

    coeff = coeff_5point if df_7point.error.sum() > df_5point.error.sum() else coeff_7point

    new_xr = [df.xr.max() + 1]
    while new_xr[-1] < 10.99:
        new_xr.append(new_xr[-1] + 1)

    if find_xr < 10.99:
        return calc_extrapolated_point("cubic", coeff, find_xr)

    new_y = calc_extrapolated_point("cubic", coeff, np.array(new_xr))

    df_sin = pd.concat([df, pd.DataFrame(columns=["xr", "y_cubic"], data=list(zip(new_xr, new_y)))], sort=False,
                       ignore_index=True)
    df_sin.loc[np.isnan(df_sin.y_cubic), "y_cubic"] = df_sin.loc[np.isnan(df_sin.y_cubic), "y"]

    df_sin = calc_sin(df_sin)

    df_lin = calc_linear(df_sin)

    df_order4 = df_lin.copy(deep=True)

    last_xr_cubic = df_order4.loc[~np.isnan(df_order4.y_cubic)].xr.max()
    df_order4.drop(df_order4.loc[df_order4.xr < last_xr_cubic].index, inplace=True)
    df_order4.reset_index(inplace=True, drop=True)
    df_order4["y"] = df_order4.y_rough

    coeff_order4 = order4(df_order4.copy(deep=True))

    df_order4["y_order4"] = calc_extrapolated_point("order4", coeff_order4, df_order4.xr.values)
    offset = (df_order4.loc[df_order4.index == df_order4.index.min()].y - df_order4.loc[
        df_order4.index == df_order4.index.min()].y_order4).values[0]
    df_order4["y_order4"] = df_order4.y_order4 + offset

    if find_xr < 25.:
        return calc_extrapolated_point("order4", coeff_order4, find_xr) + offset

    df_log = df_order4.copy(deep=True)
    df_log.drop(df_log.loc[df_log.index < df_log.index.max() - 17].index, inplace=True)
    df_log.reset_index(inplace=True, drop=True)
    df_log["y"] = df_log.y_order4

    coeff_log = logrithmic(df_log.copy(deep=True))

    df_log["y_log"] = calc_extrapolated_point("logrithmic", coeff_log, df_log.xr.values)
    offset = (df_log.loc[df_log.index == df_log.index.min()].y_log - df_log.loc[
        df_log.index == df_log.index.min()].y_order4).values[0]
    df_log["Y_log"] = df_log.y_log - offset

    df_log.loc[np.isnan(df_log.x), "x"] = (df_log.xr - last_row.xr) * last_row.x_diff + last_row.x

    y = calc_extrapolated_point("logrithmic", coeff_log, find_xr)

    y = y + offset
    return y


def extrapolate_cubic(df, x):
    last_x_index = df.loc[~np.isnan(df.x)].index.max()
    last_row = df.loc[df.index == last_x_index].squeeze()

    find_xr = (x - (last_row.x)) / last_row.x_diff + last_row.xr

    coeff = cubic(df)
    df["y_calc"] = calc_extrapolated_point("cubic", coeff, df.xr.values)

    new_xr = [df.xr.max() + 1]
    while new_xr[-1] < 9.99:
        new_xr.append(new_xr[-1] + 1)

    if find_xr < 9.99:
        return calc_extrapolated_point("cubic", coeff, find_xr)

    new_y = calc_extrapolated_point("cubic", coeff, np.array(new_xr))

    df = pd.concat([df, pd.DataFrame(columns=["xr", "y_cubic"], data=list(zip(new_xr, new_y)))], sort=False,
                   ignore_index=True)
    df.loc[np.isnan(df.y_cubic), "y_cubic"] = df.loc[np.isnan(df.y_cubic), "y"]

    df.drop(df.loc[df.index < df.index.max() - 7].index, inplace=True)
    df.reset_index(inplace=True, drop=True)

    if (np.any(df.y_cubic < 0) and np.any(df.y_cubic > 0)) or np.any(df.y_cubic == 0):
        method = "logrithmic"
        df["y"] = df.y_cubic
        coeff = logrithmic(df)
        df["y_calc"] = calc_extrapolated_point("logrithmic", coeff, df.xr.values)
    else:
        method = "power"
        df["y"] = df.y_cubic
        coeff = power(df)
        df["y_calc"] = calc_extrapolated_point("power", coeff, df.xr.values)
    offset = (df.loc[df.index == df.index.max()].y - df.loc[df.index == df.index.max()].y_calc).values[0]
    y = calc_extrapolated_point(method, coeff, find_xr)
    y = y + offset
    return y


def extrapolate_high_cubic(df, x):
    last_x_index = df.loc[~np.isnan(df.x)].index.max()
    last_row = df.loc[df.index == last_x_index].squeeze()
    find_xr = (x - last_row.x) / last_row.x_diff + last_row.xr

    coeff = cubic(df)
    df["y_calc"] = calc_extrapolated_point("cubic", coeff, df.xr.values)

    new_xr = [df.xr.max() + 1]
    while new_xr[-1] < 9.99:
        new_xr.append(new_xr[-1] + 1)

    if find_xr < 9.99:
        return calc_extrapolated_point("cubic", coeff, find_xr)

    new_y = calc_extrapolated_point("cubic", coeff, np.array(new_xr))

    df = pd.concat([df, pd.DataFrame(columns=["xr", "y_cubic"], data=list(zip(new_xr, new_y)))], sort=False,
                   ignore_index=True)
    df.loc[np.isnan(df.y_cubic), "y_cubic"] = df.loc[np.isnan(df.y_cubic), "y"]

    new_xr = [df.xr.max() + 1]
    new_y = list()
    y_cubics = df.y_cubic.tolist()

    while new_xr[-1] < 29.99:
        new_xr.append(new_xr[-1] + 1)
        a0 = (1. / 35.) * (-3. * y_cubics[-5] + 12. * y_cubics[-4] + 17. * y_cubics[-3] + 12. * y_cubics[-2] - 3. *
                           y_cubics[-1])
        a1 = (1. / 12.) * (y_cubics[-5] - 8. * y_cubics[-4] + 8. * y_cubics[-2] - y_cubics[-1])
        a2 = (1. / 14.) * (2. * y_cubics[-5] - y_cubics[-4] - 2. * y_cubics[-3] - y_cubics[-2] + 2. * y_cubics[-1])
        a3 = (1. / 12.) * (-1. * y_cubics[-5] + 2. * y_cubics[-4] - 2 * y_cubics[-2] + y_cubics[-1])
        z = 3
        y = a0 + a1 * z + a2 * z ** 2 + a3 * z ** 3
        new_y.append(y)
        y_cubics.pop(0)
        y_cubics.append(y)

    del new_xr[-1]

    df = pd.concat([df, pd.DataFrame(columns=["xr", "y_cubic"], data=list(zip(new_xr, new_y)))], sort=False,
                   ignore_index=True)

    # check
    D = (4. * coeff[2] ** 2 - 12. * coeff[1] * coeff[3])
    if D >= 0:
        extremum_xr = max(
            ((-2 * coeff[2] + np.sqrt(4 * coeff[2] ** 2 - 12 * coeff[3] * coeff[1])) / (6 * coeff[3])),
            ((-2 * coeff[2] - np.sqrt(4 * coeff[2] ** 2 - 12 * coeff[3] * coeff[1])) / (6 * coeff[3])))
        if int(extremum_xr) <= 29.99 or int(extremum_xr) > 8:
            extremum_xr = int(extremum_xr) + 1
            if find_xr > extremum_xr:
                df.drop(df.loc[(df.xr < extremum_xr - 4) | (df.xr.astype(int) > extremum_xr)].index, inplace=True)
                df["y"] = df.y_cubic
                df.reset_index(inplace=True, drop=True)
                coeff = logrithmic(df)
                df["y_calc"] = calc_extrapolated_point("logrithmic", coeff, df.xr.values)
                offset = (df.loc[df.index == df.index.max()].y - df.loc[df.index == df.index.max()].y_calc).values[
                    0]
                y = calc_extrapolated_point("logrithmic", coeff, find_xr)
                y = y + offset
                return y

    if find_xr < 29.99:
        df.loc[np.isnan(df.y), "y"] = df.loc[np.isnan(df.y)].y_cubic
        df.loc[np.isnan(df.x), "x"] = (df.xr - last_row.xr) * last_row.x_diff + last_row.x
        calc_params(df)
        df = df.sort_values(by=["x"], ascending=True)
        df.reset_index(inplace=True)
        s_index = df.loc[df.x < x].index.values[-1]
        row = df.loc[df.index == s_index].squeeze()
        t = (x - row.x) / row.x_diff
        return calc_interpolate(row, t)

    df.drop(df.loc[df.index < df.index.max() - 10].index, inplace=True)
    df.reset_index(inplace=True, drop=True)
    if (np.any(df.y_cubic < 0) and np.any(df.y_cubic > 0)) or np.any(df.y_cubic == 0):
        method = "logrithmic"
        df["y"] = df.y_cubic
        coeff = logrithmic(df)
        df["y_calc"] = calc_extrapolated_point("logrithmic", coeff, df.xr.values)
        offset = (df.loc[df.index == df.index.max()].y - df.loc[df.index == df.index.max()].y_calc).values[0]
        df["y_calc"] = df.y_calc + offset
    else:
        method = "power"
        df["y"] = df.y_cubic
        coeff = power(df)
        df["y_calc"] = calc_extrapolated_point("power", coeff, df.xr.values)
        if np.all(df.y < 0):
            df["y_calc"] = df.y_calc * -1.
        offset = (df.loc[df.index == df.index.max()].y - df.loc[df.index == df.index.max()].y_calc).values[0]
        df["y_calc"] = df.y_calc + offset

    y = calc_extrapolated_point(method, coeff, find_xr)
    y = y + offset
    return y


def extrapolate_cubic_8point(df, x):
    last_x_index = df.loc[~np.isnan(df.x)].index.max()
    last_row = df.loc[df.index == last_x_index].squeeze()

    find_xr = (x - last_row.x) / last_row.x_diff + last_row.xr

    df_7point = df.loc[df.index > df.index.min()].copy(deep=True)
    df_7point.reset_index(inplace=True, drop=True)
    coeff_7point = cubic(df_7point)
    df_7point["y_calc"] = calc_extrapolated_point("cubic", coeff_7point, df_7point.xr.values)
    df_7point["error"] = divide_by_non_zero((df_7point.y - df_7point.y_calc), df_7point.y)

    df_5point = df.loc[df.index > df.index.min() + 2].copy(deep=True)
    df_5point.reset_index(inplace=True, drop=True)
    coeff_5point = cubic(df_5point)
    df_5point["y_calc"] = calc_extrapolated_point("cubic", coeff_5point, df_5point.xr.values)
    df_5point["error"] = divide_by_non_zero((df_5point.y - df_5point.y_calc), df_5point.y)

    coeff = coeff_5point if df_7point.error.sum() > df_5point.error.sum() else coeff_7point
    new_xr = [df.xr.max() + 1]
    while new_xr[-1] < 10.99:
        new_xr.append(new_xr[-1] + 1)

    if find_xr < 10.99:
        return calc_extrapolated_point("cubic", coeff, find_xr)

    new_y = calc_extrapolated_point("cubic", coeff, np.array(new_xr))

    df_cubic = pd.concat([df, pd.DataFrame(columns=["xr", "y_cubic"], data=list(zip(new_xr, new_y)))], sort=False,
                         ignore_index=True)
    df_cubic.loc[np.isnan(df_cubic.y_cubic), "y_cubic"] = df_cubic.loc[np.isnan(df_cubic.y_cubic), "y"]

    new_xr = [df_cubic.xr.max() + 1]
    new_y = list()
    y_cubics = df_cubic.y_cubic.tolist()

    # kachra code above

    while new_xr[-1] < 20.99:
        new_xr.append(new_xr[-1] + 1)
        a0 = (1. / 35.) * (-3. * y_cubics[-5] + 12. * y_cubics[-4] + 17. * y_cubics[-3] + 12. * y_cubics[-2] - 3. *
                           y_cubics[-1])
        a1 = (1. / 12.) * (y_cubics[-5] - 8. * y_cubics[-4] + 8. * y_cubics[-2] - y_cubics[-1])
        a2 = (1. / 14.) * (2. * y_cubics[-5] - y_cubics[-4] - 2. * y_cubics[-3] - y_cubics[-2] + 2. * y_cubics[-1])
        a3 = (1. / 12.) * (-1. * y_cubics[-5] + 2. * y_cubics[-4] - 2 * y_cubics[-2] + y_cubics[-1])
        z = 3
        y = a0 + a1 * z + a2 * z ** 2 + a3 * z ** 3
        new_y.append(y)
        y_cubics.pop(0)
        y_cubics.append(y)

    del new_xr[-1]

    df_cubic = pd.concat([df_cubic, pd.DataFrame(columns=["xr", "y_extr_cubic"], data=list(zip(new_xr, new_y)))],
                         sort=False, ignore_index=True)

    # check
    D = (4. * coeff[2] ** 2 - 12. * coeff[1] * coeff[3])
    if D >= 0:
        extremum_xr = None
        if coeff[3] == 0 and coeff[2] == 0:
            extremum_xr = 1.
        elif coeff[3] == 0:
            extremum_xr = -coeff[1] / (2. * coeff[2])
        else:
            extremum_xr = max(
                ((-2 * coeff[2] + np.sqrt(4 * coeff[2] ** 2 - 12 * coeff[3] * coeff[1])) / (6 * coeff[3])),
                ((-2 * coeff[2] - np.sqrt(4 * coeff[2] ** 2 - 12 * coeff[3] * coeff[1])) / (6 * coeff[3])))

        if 20.99 >= int(extremum_xr) > 8:
            extremum_xr = int(extremum_xr) + 1
            if find_xr > extremum_xr:
                df_cubic.drop(
                    df_cubic.loc[(df_cubic.xr < extremum_xr - 4) | (df_cubic.xr.astype(int) > extremum_xr)].index,
                    inplace=True)
                df_cubic.loc[np.isnan(df_cubic.y_cubic), "y_cubic"] = df_cubic.loc[
                    np.isnan(df_cubic.y_cubic), "y_extr_cubic"]
                df_cubic["y"] = df_cubic.y_cubic
                df_cubic.reset_index(inplace=True, drop=True)
                coeff = logrithmic(df_cubic)
                df_cubic["y_calc"] = calc_extrapolated_point("logrithmic", coeff, df_cubic.xr.values)
                offset = (df_cubic.loc[df_cubic.index == df_cubic.index.max()].y - df_cubic.loc[
                    df_cubic.index == df_cubic.index.max()].y_calc).values[0]
                y = calc_extrapolated_point("logrithmic", coeff, find_xr)
                y = y + offset
                return y

    # test this. might be an issue with missing x and hv0 values
    if find_xr < 20.99:
        df_cubic.loc[np.isnan(df_cubic.y), "y"] = df_cubic.loc[np.isnan(df_cubic.y)].y_cubic
        df_cubic.loc[np.isnan(df_cubic.y), "y"] = df_cubic.loc[np.isnan(df_cubic.y)].y_extr_cubic
        df_cubic.loc[np.isnan(df_cubic.x), "x"] = (df_cubic.xr - last_row.xr) * last_row.x_diff + last_row.x
        calc_params(df_cubic)
        df_cubic = df_cubic.sort_values(by=["x"], ascending=True)
        df_cubic.reset_index(inplace=True)
        s_index = df_cubic.loc[df_cubic.x < x].index.values[-1]
        row = df_cubic.loc[df_cubic.index == s_index].squeeze()
        t = (x - row.x) / row.x_diff
        return calc_interpolate(row, t)

    df_8point = df_cubic.loc[df_cubic.index > df_cubic.index.max() - 8].copy(deep=True)
    df_8point.reset_index(inplace=True, drop=True)

    if (np.any(df_8point.y_extr_cubic < 0) and np.any(df_8point.y_extr_cubic > 0)) or np.any(
            df_8point.y_extr_cubic == 0):
        method = "logrithmic"
        df_8point["y"] = df_8point.y_extr_cubic
        coeff = logrithmic(df_8point)
        df_8point["y_calc"] = calc_extrapolated_point("logrithmic", coeff, df_8point.xr.values)
        offset = (df_8point.loc[df_8point.index == df_8point.index.max()].y - df_8point.loc[
            df_8point.index == df_8point.index.max()].y_calc).values[0]
        df_8point["y_calc"] = df_8point.y_calc + offset
    else:
        method = "power"
        df_8point["y"] = df_8point.y_extr_cubic
        coeff = power(df_8point)
        df_8point["y_calc"] = calc_extrapolated_point("power", coeff, df_8point.xr.values)
        if np.all(df.y < 0):
            df_8point["y_calc"] = df_8point.y_calc * -1.
        offset = (df_8point.loc[df_8point.index == df_8point.index.max()].y - df_8point.loc[
            df_8point.index == df_8point.index.max()].y_calc).values[0]
        df_8point["y_calc"] = df_8point.y_calc + offset

    y = calc_extrapolated_point(method, coeff, find_xr)
    y = y + offset
    return y


def extrapolate_high_cubic_8point(df, x):
    last_x_index = df.loc[~np.isnan(df.x)].index.max()
    last_row = df.loc[df.index == last_x_index].squeeze()

    find_xr = (x - last_row.x) / last_row.x_diff + last_row.xr

    df_7point = df.loc[df.index > df.index.min()].copy(deep=True)
    df_7point.reset_index(inplace=True, drop=True)
    coeff_7point = cubic(df_7point)
    df_7point["y_calc"] = calc_extrapolated_point("cubic", coeff_7point, df_7point.xr.values)
    df_7point["error"] = divide_by_non_zero((df_7point.y - df_7point.y_calc), df_7point.y)

    df_5point = df.loc[df.index > df.index.min() + 2].copy(deep=True)
    df_5point.reset_index(inplace=True, drop=True)
    coeff_5point = cubic(df_5point)
    df_5point["y_calc"] = calc_extrapolated_point("cubic", coeff_5point, df_5point.xr.values)
    df_5point["error"] = divide_by_non_zero((df_5point.y - df_5point.y_calc), df_5point.y)

    coeff = coeff_5point if df_7point.error.sum() > df_5point.error.sum() else coeff_7point

    new_xr = [df.xr.max() + 1]
    while new_xr[-1] < 10.99:
        new_xr.append(new_xr[-1] + 1)

    if find_xr < 10.99:
        return calc_extrapolated_point("cubic", coeff, find_xr)

    new_y = calc_extrapolated_point("cubic", coeff, np.array(new_xr))

    df_cubic = pd.concat([df, pd.DataFrame(columns=["xr", "y_cubic"], data=list(zip(new_xr, new_y)))], sort=False,
                         ignore_index=True)
    df_cubic.loc[np.isnan(df_cubic.y_cubic), "y_cubic"] = df_cubic.loc[np.isnan(df_cubic.y_cubic), "y"]

    new_xr = [df_cubic.xr.max() + 1]
    new_y = list()
    y_cubics = df_cubic.y_cubic.tolist()

    while new_xr[-1] < 40.99:
        new_xr.append(new_xr[-1] + 1)
        a0 = (1. / 35.) * (-3. * y_cubics[-5] + 12. * y_cubics[-4] + 17. * y_cubics[-3] + 12. * y_cubics[-2] - 3. *
                           y_cubics[-1])
        a1 = (1. / 12.) * (y_cubics[-5] - 8. * y_cubics[-4] + 8. * y_cubics[-2] - y_cubics[-1])
        a2 = (1. / 14.) * (2. * y_cubics[-5] - y_cubics[-4] - 2. * y_cubics[-3] - y_cubics[-2] + 2. * y_cubics[-1])
        a3 = (1. / 12.) * (-1. * y_cubics[-5] + 2. * y_cubics[-4] - 2 * y_cubics[-2] + y_cubics[-1])
        z = 3
        y = a0 + a1 * z + a2 * z ** 2 + a3 * z ** 3
        new_y.append(y)
        y_cubics.pop(0)
        y_cubics.append(y)

    del new_xr[-1]

    df_cubic = pd.concat([df_cubic, pd.DataFrame(columns=["xr", "y_extr_cubic"], data=list(zip(new_xr, new_y)))],
                         sort=False, ignore_index=True)

    # check
    D = (4. * coeff[2] ** 2 - 12. * coeff[1] * coeff[3])
    if D >= 0:
        extremum_xr = max(
            ((-2 * coeff[2] + np.sqrt(4 * coeff[2] ** 2 - 12 * coeff[3] * coeff[1])) / (6 * coeff[3])),
            ((-2 * coeff[2] - np.sqrt(4 * coeff[2] ** 2 - 12 * coeff[3] * coeff[1])) / (6 * coeff[3])))
        if int(extremum_xr) <= 40.99 or int(extremum_xr) > 8:
            extremum_xr = int(extremum_xr) + 1
            if find_xr > extremum_xr:
                df_cubic.drop(
                    df_cubic.loc[(df_cubic.xr < extremum_xr - 4) | (df_cubic.xr.astype(int) > extremum_xr)].index,
                    inplace=True)
                df_cubic.loc[np.isnan(df_cubic.y_cubic), "y_cubic"] = df_cubic.loc[
                    np.isnan(df_cubic.y_cubic), "y_extr_cubic"]
                df_cubic["y"] = df_cubic.y_cubic
                df_cubic.reset_index(inplace=True, drop=True)
                coeff = logrithmic(df_cubic)
                df_cubic["y_calc"] = calc_extrapolated_point("logrithmic", coeff, df_cubic.xr.values)
                offset = (df_cubic.loc[df_cubic.index == df_cubic.index.max()].y - df_cubic.loc[
                    df_cubic.index == df_cubic.index.max()].y_calc).values[0]
                y = calc_extrapolated_point("logrithmic", coeff, find_xr)
                y = y + offset
                return y

    # test self. might be an issue with missing x and hv0 values
    if find_xr < 40.99:
        df_cubic.loc[np.isnan(df_cubic.y), "y"] = df_cubic.loc[np.isnan(df_cubic.y)].y_extr_cubic
        df_cubic.loc[np.isnan(df_cubic.x), "x"] = (df_cubic.xr - last_row.xr) * last_row.x_diff + last_row.x
        calc_params(df_cubic)
        df_cubic = df_cubic.sort_values(by=["x"], ascending=True)
        df_cubic.reset_index(inplace=True)
        s_index = df_cubic.loc[df_cubic.x < x].index.values[-1]
        row = df_cubic.loc[df_cubic.index == s_index].squeeze()
        t = (x - row.x) / row.x_diff
        return calc_interpolate(row, t)

    df_8point = df_cubic.loc[df_cubic.index > df_cubic.index.max() - 11].copy(deep=True)
    df_8point.reset_index(inplace=True, drop=True)

    if (np.any(df_8point.y_extr_cubic < 0) and np.any(df_8point.y_extr_cubic > 0)) or np.any(
            df_8point.y_extr_cubic == 0):
        method = "logrithmic"
        df_8point["y"] = df_8point.y_extr_cubic
        coeff = logrithmic(df_8point)
        df_8point["y_calc"] = calc_extrapolated_point("logrithmic", coeff, df_8point.xr.values)
        offset = (df_8point.loc[df_8point.index == df_8point.index.max()].y - df_8point.loc[
            df_8point.index == df_8point.index.max()].y_calc).values[0]
        df_8point["y_calc"] = df_8point.y_calc + offset
    else:
        method = "power"
        df_8point["y"] = df_8point.y_extr_cubic
        coeff = power(df_8point)
        df_8point["y_calc"] = calc_extrapolated_point("power", coeff, df_8point.xr.values)
        if np.all(df.y < 0):
            df_8point["y_calc"] = df_8point.y_calc * -1.
        offset = (df_8point.loc[df_8point.index == df_8point.index.max()].y - df_8point.loc[
            df_8point.index == df_8point.index.max()].y_calc).values[0]
        df_8point["y_calc"] = df_8point.y_calc + offset

    y = calc_extrapolated_point(method, coeff, find_xr)
    y = y + offset
    return y


def extrapolate_8point(df, x):
    global sin, high_cubic
    df = create_table_1(df)
    methods, method_name = None, None
    if (df['y'] == 0).any():
        methods = [order4, cubic, quad, linear, logrithmic]
        method_name = ["order4", "cubic", "quad", "linear", "logrithmic"]
    else:
        methods = [order4, cubic, quad, linear, power, logrithmic, exponential]
        method_name = ["order4", "cubic", "quad", "linear", "power", "logrithmic", "exponential"]

    results = dict()
    for i in range(len(methods)):
        if df.shape[0] < 5 and method_name[i] == "cubic":
            continue
        if (np.any(df.y < 0) and np.any(df.y > 0)) and method_name[i] in ["power", "exponential"]:
            continue
        coeff = methods[i](df.copy(deep=True))
        df["y_calc"] = calc_extrapolated_point(method_name[i], coeff, df.xr.values)
        df["error"] = divide_by_non_zero((df.y - df.y_calc), df.y)
        results[method_name[i]] = [df.error.sum(), df.loc[df.index == df.index.max(), "error"].values[0], coeff]
    df_out = pd.DataFrame(results, index=["error", "last_err", "coeff"])
    df_out = df_out.T

    # minimum error
    method = df_out.loc[df_out.error == df_out.error.min()].index.values[0]
    if method in ["linear", "logrithmic", "exponential", "power"]:
        coeff = df_out.loc[df_out.index == method].coeff.values[0]
        last_x_index = df.loc[~np.isnan(df.x)].index.max()
        last_row = df.loc[df.index == last_x_index].squeeze()
        find_xr = (x - last_row.x) / last_row.x_diff + last_row.xr
        return calc_extrapolated_point(method, coeff, find_xr)
    else:
        # execute extrapolate further logic
        if sin:
            return extrapolate_sin(df, x, table1=True)
        else:
            if high_cubic:  # add self as input
                return extrapolate_high_cubic_8point(df, x)
            else:
                return extrapolate_cubic_8point(df, x)


def calc_extrapolate(df, x):
    global sin, high_cubic
    find_xr = df.loc[df.index == df.index.max(), "xr"].values[0]
    df.drop(df.loc[df.index == df.index.max()].index, inplace=True)

    xr_del_ij = ((df.y - df.y.shift(1)) / (df.xr - df.xr.shift(1))).values
    xr_del_ij = np.delete(xr_del_ij, 0)

    if np.all(xr_del_ij > 0) or np.all(xr_del_ij < 0) or np.all(np.abs(xr_del_ij) <= 5):
        sin = False
    else:
        sin = True

    methods, method_name = None, None

    if (df['y'] == 0).any():
        method_name = ["cubic", "quad", "linear", "logrithmic"]
        methods = [cubic, quad, linear, logrithmic]
    else:
        method_name = ["cubic", "quad", "linear", "power", "logrithmic", "exponential"]
        methods = [cubic, quad, linear, power, logrithmic, exponential]

    results = dict()
    for i in range(len(methods)):
        if df.shape[0] < 5 and method_name[i] == "cubic":
            continue
        if (np.any(df.y < 0) and np.any(df.y > 0)) and method_name[i] in ["power", "exponential"]:
            continue
        coeff = methods[i](df.copy(deep=True))
        df["y_calc"] = calc_extrapolated_point(method_name[i], coeff, df.xr.values)
        df["error"] = divide_by_non_zero((df.y - df.y_calc), df.y)
        results[method_name[i]] = [df.error.sum(), df.loc[df.index == df.index.max(), "error"].values[0], coeff]
    df_out = pd.DataFrame(results, index=["error", "last_err", "coeff"])
    df_out = df_out.T

    # minimum error
    method = df_out.loc[df_out.error == df_out.error.min()].index.values[0]

    if df_out.loc[df_out.index == method, "error"].values[0] < 0.1:
        if method in ["linear", "logrithmic", "exponential", "power"]:
            coeff = df_out.loc[df_out.index == method].coeff.values[0]
            return calc_extrapolated_point(method, coeff, find_xr)
        else:
            if sin:
                return extrapolate_sin(df, x)
            else:
                if high_cubic:  # add self as input
                    return extrapolate_high_cubic(df, x)
                else:
                    return extrapolate_cubic(df, x)
    else:
        # execute 8 point extrapolate
        return extrapolate_8point(df, x)


def interpolate(df, find_x):
    pool = Pool()
    partial_func = partial(interpolate_extrapolate, df)
    result = pool.map(partial_func, find_x)
    pool.close()
    pool.join()
    return result
    # df_out = pd.DataFrame(data=find_x, columns=["x"], index=[i for i in range(1, len(find_x) + 1)])
    # df_out["y"] = df_out.apply(lambda row: interpolate_extrapolate(df.copy(deep=True), row.x), axis=1)
    # return df_out.y.tolist()


def interpolate_extrapolate(df, x):
    # if value is greater than maximum available value or less than minimum available value
    if x > df.x.max() or x < df.x.min():
        return extrapolate(df, x)
    # if there an exact match
    elif x in df.x.values:
        return df.loc[df.x == x, "y"].values[0]
    # else interpolate
    else:
        s_index = df.loc[df.x < x].index.values[-1]
        row = df.loc[df.index == s_index].squeeze()
        t = (x - row.x) / row.x_diff
        return calc_interpolate(row, t)


def monotone(x, y, find, input_high_cubic=False):
    global sin, high_cubic
    high_cubic = input_high_cubic
    validate(x, y)
    df = pd.DataFrame(list(zip(x, y)), index=[i for i in range(1, len(x) + 1)], columns=["x", "y"])
    df = df.groupby(['x'], as_index=False).mean()
    df = df.sort_values(by=["x"], ascending=True)
    calc_params(df)
    if np.isscalar(find):
        find = [find]
    return interpolate(df, find)


def extrapolate(df, x):
    # if x is after UB
    if x > df.x.max():
        df.drop(df.loc[(df.index < df.index.max() - 4)].index, inplace=True)
    # if x is before lb
    elif x < df.x.min():
        df.drop(df.loc[df.index > df.index.min() + 4].index, inplace=True)
        df = df.iloc[::-1]

    df_out = pd.DataFrame(data=[x], columns=["x"])
    df_out = pd.concat([df[["x", "y"]], df_out], ignore_index=True, sort=False)
    df_out.reset_index(inplace=True, drop=True)
    df_out["x_diff"] = df_out.x - df_out.x.shift(1)
    m_index = df_out.loc[~np.isnan(df_out.x_diff)].index.min()
    m_x_diff = df_out.loc[df_out.index == m_index].x_diff.values[0]
    df_out["step"] = df_out.x_diff / m_x_diff
    df_out.loc[df_out.index == m_index - 1, "step"] = 1.
    df_out["xr"] = df_out.step.cumsum()
    return calc_extrapolate(df_out, x)


if __name__ == "__main__":
    input_x = [27.2128593, 47.2128593, 67.2128593, 87.2128593, 107.2128593, 147.2128593, 187.2128593, 207.2128593,
               267.2128593, 287.2128593, 307.2128593, 327.2128593, 347.2128593, 367.2128593, 387.2128593, 407.2128593,
               427.2128593, 447.2128593, 467.2128593, 487.2128593, 507.2128593, 527.2128593, 547.2128593, 567.2128593,
               587.2128593, 607.2128593, 627.2128593, 647.2128593, 667.2128593, 687.2128593, 707.2128593, 727.2128593,
               747.2128593, 767.2128593, 787.2128593, 807.2128593, 827.2128593, 847.2128593, 867.2128593, 887.2128593,
               907.2128593, 927.2128593, 947.2128593, 967.2128593, 987.2128593, 1007.212859, 1027.212859, 1047.212859,
               1067.212859, 1087.212859, 1107.212859, 1127.212859, 1147.212859, 1167.212859, 1187.212859, 1207.212859,
               1227.212859, 1247.212859, 1267.212859, 1287.212859, 1307.212859, 1327.212859, 1347.212859, 1367.212859,
               1387.212859, 1407.212859, 1427.212859, 1447.212859, 1467.212859, 1487.212859, 1507.212859, 1527.212859,
               1547.212859, 1567.212859, 1587.212859, 1607.212859, 1627.212859, 1647.212859, 1667.212859, 1687.212859,
               1707.212859, 1727.212859, 1747.212859, 1767.212859, 1787.212859, 1807.212859, 1827.212859, 1847.212859,
               1867.212859, 1887.212859, 1907.212859, 1927.212859, 1947.212859, 1967.212859, 1987.212859, 2007.212859,
               2027.212859, 2047.212859, 2067.212859, 2087.212859, 2107.212859, 2127.212859, 2147.212859, 2167.212859,
               2187.212859, 2207.212859, 2227.212859, 2247.212859, 2267.212859, 2287.212859, 2307.212859, 2327.212859,
               2347.212859, 2367.212859, 2387.212859, 2407.212859, 2427.212859, 2447.212859, 2467.212859, 2487.212859,
               2507.212859, 2527.212859, 2547.212859, 2567.212859, 2587.212859, 2607.212859, 2627.212859, 2647.212859,
               2667.212859, 2687.212859, 2707.212859, 2727.212859, 2747.212859, 2767.212859, 2787.212859, 2807.212859,
               2827.212859, 2847.212859]
    input_y = [-2.227358446, 0.670081934, 4.080307309, 7.208141973, 14.91884303, 13.96616035, 14.3115991, 16.42708849,
               19.38981307, 20.01946182, 21.39192694, 22.78644403, 24.17953735, 25.56771626, 26.99836285, 27.3431663,
               27.83995156, 28.76220045, 29.6164027, 30.44488505, 31.2539984, 32.04983381, 32.83813777, 33.62421492,
               34.41286402, 35.21066734, 35.99805367, 36.66936535, 37.51635624, 38.37785847, 39.26070382, 40.16485319,
               41.08999272, 42.03551458, 43.00051644, 43.98380947, 44.98385762, 45.99946851, 47.03129918, 48.07967523,
               49.14421819, 50.22456698, 51.32035433, 52.43120574, 53.55646724, 54.69858632, 55.85826117, 57.01973517,
               58.20138948, 59.39261392, 60.5967859, 61.81340376, 63.04214656, 64.28270641, 65.53478915, 66.79811489,
               68.07241858, 69.3572113, 70.65330376, 71.95877829, 73.27465321, 74.6004143, 75.93598409, 77.28064684,
               78.63172701, 79.98751564, 81.34135695, 82.69282153, 84.05608951, 85.42106726, 86.78861251, 88.15817618,
               89.52980442, 90.90325176, 92.27836355, 93.65500672, 95.0330684, 96.41245497, 97.79309101, 99.17491846,
               100.557799, 101.942675, 103.3294239, 104.7174892, 106.1064123, 107.5000849, 108.8941731, 110.2809767,
               111.6703927, 113.0616103, 114.454717, 115.8497985, 117.2469388, 118.6462204, 120.0477244, 121.4515312,
               122.8577204, 124.2663716, 125.6775646, 127.091663, 128.5061604, 129.9129702, 131.313505, 132.7021908,
               134.081743, 135.457315, 136.8296605, 138.1999869, 139.5694957, 140.9393753, 142.310802, 143.6849379,
               145.0629208, 146.4459696, 147.8356325, 149.2300757, 150.6336421, 152.0472923, 153.4719207, 154.9084151,
               156.3583213, 157.822811, 159.3030606, 160.8002364, 162.3154725, 163.8535556, 165.3747647, 166.9435667,
               168.7538614, 170.6999666, 172.539625, 174.705637, 176.8469373, 178.9565769, 180.6278868, 181.9248992,
               183.2212965, 184.5174232]

    # print monotone(x, y, [14.7008108667172], False)
    #
    # quit()

    # find_xs = [i for i in range(2850, 5000)]
    find_xs = [14.7008108667172]
    import datetime

    print "Case 1 - finding %s X's in one go." % len(find_xs)
    print "START - %s " % datetime.datetime.now()
    result = monotone(input_x, input_y, find_xs, False)
    print result
    print "END - %s " % datetime.datetime.now()

    print "*" * 20
    print "*" * 20

    from scipy.interpolate import PchipInterpolator

    print "Case 3 - find_x using pchip."
    print "START - %s " % datetime.datetime.now()
    result = PchipInterpolator(input_x, input_y)(find_xs)
    print result
    print "END - %s " % datetime.datetime.now()
