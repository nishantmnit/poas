#!/usr/bin/env python
import pandas as pd
from math import exp, log, sqrt

def GenericSolver(fun, boundsOfVars, func, debug=False, t=0.01, tstep=0.01, tmax=0.01, boundsMultiplier=None, maxIter = 3000, dtol=10**-8, etol=10**-6, **kwargs):
    return runSolver(fun, boundsOfVars, func, debug, t, tstep, tmax, boundsMultiplier, maxIter, dtol, etol, **kwargs)

def runSolver(fun_input, boundsOfVars, func, debug, t, tstep, tmax, boundsMultiplier, maxIter_input, dtol, etol, **kwargs):
    if "result" not in kwargs: kwargs['result'] = list()
    if "r2" not in kwargs: kwargs['r2'] = list()
    if "values" not in kwargs: kwargs['values'] = list()
    msg, success = None, True
    while t <= tmax:
        maxIter, fun = maxIter_input, fun_input[:]
        prevMomentum = list()
        for i in range(0, len(fun)): prevMomentum.append([None, None])
        for iter in range(0, maxIter+1):
            vars = list()
            for i in range(0, len(fun)): vars.append(boundsOfVars[i][0] + fun[i] * (boundsOfVars[i][1] - boundsOfVars[i][0]))
            if min(fun) < 0:
                print fun
                msg = "function is negative. Terminating Solver!"
                break
            initErr, differentiations, r2 = func(vars) if "args" not in kwargs else func(vars, kwargs["args"])
            kwargs["result"].append(abs(initErr))
            kwargs["values"].append(vars)
            kwargs["r2"].append(r2)
            # for mass & bp 10**-8, for density 10**-10
            if all(abs(x) <= dtol for x in differentiations): 
                msg = "All differentiations are less than %s. Terminating Solver!" %(dtol)
                break
            # for mass & bp 10**-6, for density 10**-10
            if iter > 1000 and abs(kwargs["result"][-1] - kwargs["result"][-2]) < etol: 
                msg = "No change in error functions. Terminating Solver!"
                break
            substract = list()
            prevMomentumNew = list()
            for i in range(0, len(fun)):
                substractumterm, momentum1, momentum2 = getSubstractionTerm(differentiations[i]*(boundsOfVars[i][1] - boundsOfVars[i][0])*(boundsMultiplier if boundsMultiplier is not None and any(d < 10**-3 for d in differentiations) else 1.), fun[i], prevMomentum[i])
                substract.append(substractumterm)
                prevMomentumNew.append([momentum1, momentum2])
            prevMomentum = prevMomentumNew
            # debug statement
            # if debug: print iter, t, boundsMultiplier if boundsMultiplier is not None and any(d < 10**-2 for d in differentiations) else 1., fun, vars, initErr, differentiations, substract, min(kwargs["result"])
            for i in range(0, len(fun)): fun[i] = fun[i] - substract[i]*t*1.
        t += tstep

    minima = kwargs["result"].index(min(kwargs["result"]))
    if msg is None: msg = "Completed %s iterations!" %(maxIter_input)
    # print {"ErrFunc": min(kwargs["result"]), "Values": kwargs["values"][minima], "Regression": kwargs["r2"][minima], "Iterations": iter, "Message": msg}
    return {"ErrFunc": min(kwargs["result"]), "Values": kwargs["values"][minima], "Regression": kwargs["r2"][minima], "Iterations": iter, "Message": msg, "success": success}
    
def getSubstractionTerm(error, fun, prevMomentum, Beta=0.9, Beta1=0.99):
    prevMomentum1, prevMomentum2 = [None, None] if len(prevMomentum) == 0 else prevMomentum
    dcr = min([0-fun, 1-fun]) if error == 0 else (1 - fun)
    momentum1 = (1 - Beta) * error if prevMomentum1 is None else Beta * prevMomentum1 + (1 - Beta) * error
    momentum2 = (1-Beta1) * error**2 if prevMomentum2 is None else Beta1 * prevMomentum2 + (1-Beta1) * error**2
    substractumterm = (1/((sqrt(momentum2)) + 10**-8)) * momentum1 * dcr if prevMomentum2 is None else (1/((sqrt(max([momentum2, prevMomentum2]))) + 10**-8)) * momentum1 * dcr
    return substractumterm, momentum1, momentum2
