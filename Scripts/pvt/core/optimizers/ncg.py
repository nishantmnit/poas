import numpy as np


def normalize_grad(grads):
    new_grads = list()
    for grad in grads:
        if np.abs(grad) >= 10 ** 100:
            grad = 10 ** 10
        elif np.abs(grad) >= 10 ** 25:
            for i in range(90, 20, -5):
                if np.abs(grad) >= 10 ** i:
                    grad = grad * 10 ** (-1. * (i - 5.))
        elif np.abs(grad) >= 10 ** 20:
            grad = grad * 10 ** -12
        elif np.abs(grad) >= 10 ** 15:
            grad = grad * 10 ** -5
        elif np.abs(grad) >= 10 ** 10:
            grad = grad * 10 ** -4
        else:
            grad = grad
        new_grads.append(grad)
    return np.array(new_grads)


def define_region(vars, bound, mk_vars):
    lb, ub = list(), list()
    for i in range(len(vars)):
        lb.append(max(bound[i][0], (vars[i] - 4. * vars[i])))
        ub.append(min(bound[i][1], (vars[i] + 4. * vars[i])))
    mk_marker = mk_vars[np.argmax(np.abs(mk_vars))]
    a_max_l = list()
    for i in range(len(vars)):
        a_max_l.append(np.abs(0 if mk_vars[i] == 0 else (
            ((vars[i] - ub[i]) / mk_vars[i]) if mk_vars[i] < 0 else ((vars[i] - lb[i]) / mk_vars[i]))))

    if 1.5 * min(a_max_l) < 10 ** -4:
        if max(a_max_l) < 5:
            alpha_max = max(a_max_l)
        else:
            alpha_max = 10 ** -4
    else:
        if 1.5 * min(a_max_l) > 50:
            alpha_max = 50.
        else:
            alpha_max = 1.5 * min(a_max_l)

    if alpha_max * 0.0000001 < 10 ** -13:
        alpha_min = 10 ** -13
    else:
        if alpha_max * 0.0000001 > 10 ** -8:
            alpha_min = 10 ** -8
        else:
            alpha_min = alpha_max * 0.0000001
    return alpha_max, alpha_min


def converged(debug, logger, iteration, grad):
    cd = np.sqrt(np.sum(np.square(grad), axis=0))
    if iteration < 3:
        return False
    elif cd <= 10 ** -3 and iteration <= 50:
        if debug:
            logger.info("Converged by rule 1 - %s" % cd)
        return True
    elif cd <= 10 ** -2 and 50 < iteration <= 75:
        if debug:
            logger.info("Converged by rule 2 - %s" % cd)
        return True
    elif cd <= 10 ** -1 and 75 < iteration <= 100:
        if debug:
            logger.info("Converged by rule 3 - %s" % cd)
        return True
    elif cd <= 0.5 and 100 < iteration <= 150:
        if debug:
            logger.info("Converged by rule 4 - %s" % cd)
        return True
    elif cd <= 1. and 150 < iteration <= 175:
        if debug:
            logger.info("Converged by rule 5 - %s" % cd)
        return True
    elif cd <= 5. and 175 < iteration <= 200:
        if debug:
            logger.info("Converged by rule 6 - %s" % cd)
        return True
    # commented following as code has been modified to handle no difference in grad
    # if iteration > 5 and np.all(grad - self.pgrad == 0):
    #     if debug:
    #         logger.info("Converged by rule 7 - %s" %(cd))
    #     return True
    return False


class ncg(object):
    def __init__(self):
        self.pgrad = None
        self.pvars = None
        self.all_costs = list()  # hacks for special convergance functions
        self.cost_diff = None  # hacks for special convergance functions
        self.curr_vars = None  # hacks for special convergance functions
        self.curr_args = None  # hacks for special convergance functions

    def minimize(self, s_vars, bound, cost_f, logger=None,
                 max_iter=200, debug=False, region_f=None, converge_f=None,
                 **kwargs):
        curr_iteration = 1
        vars = s_vars
        self.region_f = define_region if region_f is None else region_f
        self.converge_f = converged if converge_f is None else converge_f
        self.curr_args = list() if "args" not in kwargs else kwargs["args"]
        while curr_iteration <= max_iter:
            self.curr_vars = vars
            cost, grad = cost_f(vars) if "args" not in kwargs else cost_f(vars, kwargs["args"])
            self.all_costs.append(cost)
            if len(self.all_costs) > 1:
                self.cost_diff = np.abs(self.all_costs[-1] - self.all_costs[-2])
            grad = normalize_grad(grad)
            mk_vars = self.mk_vars(vars, grad)
            min_alpha = self.min_alpha(vars, bound, mk_vars, cost_f, **kwargs)
            if debug:
                logger.info("iteration == %s, cost == %s, gradients == %s, mk_vars == %s, alpha == %s, vars == %s" % (
                    curr_iteration, cost, grad, mk_vars, min_alpha, vars))
            if self.converge_f(debug, logger, curr_iteration, grad):
                break
            self.pgrad, self.pvars = grad, vars
            vars = self.regress(vars, bound, grad, mk_vars, min_alpha, curr_iteration)
            curr_iteration += 1
        return vars

    def mk_vars(self, vars, grad):
        yk = np.array(grad) - np.array(grad) if self.pgrad is None else np.array(grad) - np.array(self.pgrad)
        sk = np.array(vars) - np.array(vars) if self.pvars is None else np.array(vars) - np.array(self.pvars)
        multi1 = yk.dot(sk.T)
        multi1 = 10 ** -8 if multi1 == 0 else (np.sign(multi1) * 10 ** -8 if abs(multi1) <= 10 ** -8 else multi1)
        multi2 = ((yk.dot(grad.T)) / multi1 - 2. * ((yk.dot(yk.T)) * (sk.dot(grad.T))) / multi1 ** 2)
        multi3 = sk.dot(grad.T) / multi1
        mk_vars = list()
        for i in range(len(vars)):
            mk_vars.append(grad[i] - multi2 * sk[i] - multi3 * yk[i])
        self.yk = yk  # patching to pass yk to regress function.
        return np.array(mk_vars)

    def min_alpha(self, vars, bound, mk_vars, cost_f, **kwargs):
        calphas, nvars = list(), list()
        alphas = self.get_alphas(vars, bound, mk_vars)
        for alpha in alphas:
            new_vars = np.array(vars) - alpha * np.array(mk_vars)
            for i in range(len(new_vars)):
                new_vars[i] = min(bound[i][1], max(bound[i][0], new_vars[i]))
            # new_vars = [0 if x < 0 else x for x in new_vars]
            nvars.append(new_vars)
            cost, grad = cost_f(new_vars) if "args" not in kwargs else cost_f(new_vars, kwargs["args"])
            calphas.append(cost)
        m_index = calphas.index(min(calphas))
        m_alpha = alphas[m_index]
        m_cost = calphas[m_index]
        if m_index == 0 or m_index == len(alphas) - 1:
            return m_alpha
        else:
            alpha1, alpha2, alpha3 = alphas[m_index - 1], alphas[m_index - 2], alphas[m_index + 1]
            cost1, cost2, cost3 = calphas[m_index - 1], calphas[m_index - 2], calphas[m_index + 1]
            k1 = (alpha1 ** 3 - alpha2 ** 3) * (m_alpha - alpha2) - (m_alpha ** 3 - alpha2 ** 3) * (alpha1 - alpha2)
            k2 = (alpha1 ** 2 - alpha2 ** 2) * (m_alpha - alpha2) - (m_alpha ** 2 - alpha2 ** 2) * (alpha1 - alpha2)
            k3 = (alpha1 ** 3 - alpha2 ** 3) * (alpha3 - alpha2) - (alpha3 ** 3 - alpha2 ** 3) * (alpha1 - alpha2)
            k4 = (alpha1 ** 2 - alpha2 ** 2) * (alpha3 - alpha2) - (alpha3 ** 2 - alpha2 ** 2) * (alpha1 - alpha2)
            p1 = (cost1 - cost2) * (m_alpha - alpha2) - (m_cost - cost2) * (alpha1 - alpha2)
            p2 = (cost1 - cost2) * (alpha3 - alpha2) - (cost3 - cost2) * (alpha1 - alpha2)

            a = 0 if (k1 * k4 - k2 * k3) == 0 else (p1 * k4 - p2 * k2) / (k1 * k4 - k2 * k3)
            if np.abs(a) > 10 ** 50:
                a = 10 ** 50

            b = 0 if k2 == 0 else (p1 - a * k1) / k2
            if np.abs(b) > 10 ** 50:
                b = 10 ** 50

            c = 0 if (alpha1 - alpha2) == 0 else ((cost1 - cost2) - a * (alpha1 ** 3 - alpha2 ** 3) - b * (
                    alpha1 ** 2 - alpha2 ** 2)) / (alpha1 - alpha2)
            if np.abs(c) > 10 ** 50:
                c = 10 ** 50

            d = cost2 - (a * alpha2 ** 3 + b * alpha2 ** 2 + c * alpha2)
            if np.abs(d) > 10 ** 50:
                d = 10 ** 50

            a1 = m_alpha if (4 * b ** 2 - 12 * a * c) < 0 or a == 0 else (
                    (-2 * b + np.sqrt(4 * b ** 2 - 12 * a * c)) / (6 * a))
            a2 = m_alpha if (4 * b ** 2 - 12 * a * c) < 0 or a == 0 else (-2 * b - np.sqrt(4 * b ** 2 - 12 * a * c)) / (
                    6 * a)

            a1_f = a * a1 ** 3 + b * a1 ** 2 + c * a1 + d
            a2_f = a * a2 ** 3 + b * a2 ** 2 + c * a2 + d
            alpha = a1 if a1_f <= a2_f else a2

        # find cost on interpolated alpha, compare it with previous minimum cost and select the alpha which gives
        # real low cost
        new_vars = np.array(vars) - alpha * np.array(mk_vars)
        for i in range(len(new_vars)):
            new_vars[i] = min(bound[i][1], max(bound[i][0], new_vars[i]))
        # new_vars = [0 if x < 0 else x for x in new_vars]
        cost, grad = cost_f(new_vars) if "args" not in kwargs else cost_f(new_vars, kwargs["args"])

        if cost > m_cost:
            return m_alpha
        return alpha

    def get_alphas(self, vars, bound, mk_vars):
        try:
            a_max, a_min = self.region_f(vars, bound, mk_vars)
        except:
            a_max, a_min = define_region(vars, bound, mk_vars)
        alphas = [a_min]
        for i in range(7):
            alphas.append(
                alphas[-1] * 4. if a_min == 0 or a_max == 0 else (alphas[-1] * (0.6 * a_max / a_min) ** (1. / 8.)))
        for i in range(7):
            alphas.append(
                alphas[-1] * 2. if alphas[7] == 0 or a_max == 0 else (alphas[-1] * (a_max / alphas[7]) ** (1. / 7.)))
        pull_back = list()
        for i in range(len(alphas)):
            if i % 2 == 0 and 4 <= i <= 12:
                multi = -1. * 0.8 if i == 12 else -1.
                pull_back.append((alphas[i] + alphas[i + 1]) / 2. * multi)
        pull_back.reverse()
        alphas = pull_back + alphas
        return alphas

    def regress(self, vars, bound, grad, mk_vars, alpha, iteration):
        nvars = np.array(vars) - alpha * mk_vars
        for i in range(len(vars)):
            if np.all(np.abs(grad) > 1) and np.all(np.abs(self.yk) <= 10 ** -50) and iteration > 1:
                nvars[i] = (bound[i][0] + bound[i][1]) / 2
            else:
                nvars[i] = min(bound[i][1], max(bound[i][0], nvars[i]))
        return nvars
