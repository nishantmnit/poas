import numpy as np


class Equilibrium:
    def __init__(self):
        pass

    def equilibrium(self):
        self.s_pressure = 0.101325
        self.interval = 0.5

        gd, sep, sep_cor, dv_oil, bubble_points, cce, cvd = self.read_user_input()

        bp = gd['value']['pb_res_temp']
        ref_temp = gd['value']['temp_res']

        df, dfplus = self.get_ind_plus_data()

        cp = self.convergent_pressure(df, dfplus, ref_temp)

        df["mf_tci"] = df.mf * df.tc_k
        tcr = (min(max(ref_temp / df.mf_tci.sum(), 0.7), 0.85))

        df = self.min_press_ki(df, cp, tcr)
        df = self.calc_slope(df)
        df.drop(columns=["ki_poas", "min_pressure", "min_ki_poas", "mf_tci"], inplace=True)

        return df, dfplus

    def calc_slope(self, df_o):
        df = df_o.copy(deep=True)
        a = np.log10(self.s_pressure * 145.038)
        b = a * 1.2
        df["c"] = np.log10(df.min_pressure * 145.038)
        d = np.log(a)
        e = np.log(b)
        df["f"] = np.log(df.c)
        df["g"] = np.log10(df.ki_poas) * -1.
        df["h"] = df.g * 0.85
        i = 10 ** -20
        j = 3.
        df["k"] = d + e + df.f
        df["l"] = df.g + df.h + i
        df["m"] = (d * df.g + e * df.h + df.f * i)
        df["n"] = (d ** 2 + e ** 2 + df.f ** 2)
        df_o["intercept"] = (df.k * df.m - df.l * df.n) / (df.k ** 2 - j * df.n)
        df_o["slope"] = (df.l - j * df_o.intercept) / df.k
        df["o"] = df_o.slope * d + df_o.intercept
        df["p"] = df_o.slope * e + df_o.intercept
        df["q"] = df_o.slope * df.f + df_o.intercept
        df["r"] = (df.g - df.o)
        df["s"] = (i - df.q)
        df_o["offset_slope"] = (df.s - df.r) / (df.c - a)
        df_o["offset_intercept"] = df.r - df_o.offset_slope * a
        return df_o

    def min_press_ki(self, df_o, cp, tcr):
        df = df_o.copy(deep=True)
        pressure = self.s_pressure
        pressures = list()
        while pressure <= cp:
            pressures.append(pressure)
            exp_coeff = 1. - ((pressure * 145.038 - 14.69) / (cp * 145.038 - 14.69)) ** tcr
            df[pressure] = ((df.pc_mpa / cp) ** (exp_coeff - 1.)) * (df.pc_mpa / pressure) * np.exp(
                5.37 * exp_coeff * (1. + df.omega_final) * (1. - df.tc_k / df.bp))
            pressure = min(pressure + self.interval, cp)
            if pressure in df.columns:
                break
        df_o["min_pressure"] = df[pressures].idxmin(axis=1)
        df_o["min_ki_poas"] = df[pressures].min(axis=1)
        df_o["ki_poas"] = df[self.s_pressure]
        return df_o

    def convergent_pressure(self, df, dfplus, ref_temp):
        c7index = df.loc[df.scn == "C7"].index.values[0]
        c7sg = df.loc[df.index >= c7index, "zimi"].sum() / df.loc[df.index >= c7index, "zimibysgi"].sum()
        c7mw = df.loc[df.index >= c7index, "zimi"].sum() / df.loc[df.index >= c7index, "mf"].sum()
        cp = (-2381.8542 + 46.341487 * c7mw * c7sg + 6124.3049 * (
                    c7mw * c7sg / (ref_temp * 9 / 5 - 460)) - 2753.2538 * (
                          c7mw * c7sg / (ref_temp * 9 / 5 - 460)) ** 2 + 415.42049 * (
                          c7mw * c7sg / (ref_temp * 9 / 5 - 460)) ** 3) * 0.00689476
        return cp
