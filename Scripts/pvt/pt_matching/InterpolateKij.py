


# not used
from Scripts.pvt.core.optimizers.poas_interpolator import monotone


class InterpolateKij:
    def __init__(self):
        pass

    def get_input(self, temperature, pressure):
        idf = self.well.getKijData(self.args.eos, temperature, self.args.ref_pressure[0], self.args.kij_method)
        for col in idf.columns:
            if col not in ["omega_final", "tc_k", "pc_mpa", "zc_final", "vc_final", "bp", "alpha_l", "alpha_m", "scn",
                           "component", "mf", "seq", "alpha_multiplier"]:
                idf.drop(columns=[col], inplace=True)
        self.logger.info("Reading kij data")
        kij = self.read_kij_data()
        self.interpolate("alpha_eos", idf, temperature, pressure, kij)
        self.interpolate("om_b", idf, temperature, pressure, kij)
        for index, row in idf.iterrows():
            scn = row.scn if pd.isna(row.component) else row.component
            self.interpolate(scn, idf, temperature, pressure, kij)
        self.logger.info("completed interpolation.")
        return idf

    def interpolate(self, col, idf, temperature, pressure, kij):
        self.logger.info("Interpolating %s" % col)
        for index, row in idf.iterrows():
            ys = list()
            for press in self.args.ref_pressure:
                df = kij["%s-%s" % (temperature, press)]
                ys.append(df.loc[df.index == index, col].values[0])
            idf.loc[idf.scn == row.scn, col] = monotone(self.args.ref_pressure, ys, pressure)[0]

    def read_kij_data(self):
        kij = dict()
        for temp in self.args.ref_temp:
            for press in self.args.ref_pressure:
                kij["%s-%s" % (temp, press)] = self.well.getKijData(self.args.eos, temp, press, self.args.kij_method)
        return kij
