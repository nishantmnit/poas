import sys

sys.path.insert(0, "../../../../Python")


class BubblePoint:
    def __init__(self):
        pass

    def checkBubblePoint(self):
        gd, sep, sep_cor, dv_oil, bubble_points, cce, cvd = self.read_user_input()
        bp = gd['value']['pb_res_temp']
        if not cce.empty:
            df, dfplus = self.get_ind_plus_data()
            df["zimi"] = df.mf * df.mass
            df["zimibysgi"] = df.zimi / df.sg

            cce.sort_values("pressure", axis=0, ascending=False, inplace=True, na_position='last')
            mat1 = cce.loc[cce.pressure > bp, ["rel_vol", "pressure"]].tail(5)
            mat2 = cce.loc[cce.pressure < bp, ["rel_vol", "pressure"]].head(5)
            mat1["d"] = 1
            mat2["d"] = 1

            mat1a = mat1[["d", "pressure"]].T.dot(mat1[["d", "pressure"]])
            mat2a = mat2[["d", "pressure"]].T.dot(mat2[["d", "pressure"]])

            mat1a_inv = self.mat_inv(mat1a)
            mat2a_inv = self.mat_inv(mat2a)

            mat1b = (mat1a_inv.dot(mat1[["d", "pressure"]].T.dot(mat1["rel_vol"]))).values
            mat2b = (mat2a_inv.dot(mat2[["d", "pressure"]].T.dot(mat2["rel_vol"]))).values

            calc_bp = -1. * (mat1b[0] - mat2b[0]) / (mat1b[1] - mat2b[1])

            check = True if bp >= 0.9 * calc_bp and bp <= 1.1 * calc_bp else False
            return calc_bp, check
        else:
            return bp, True
