#!/usr/bin/env python

import sys

sys.path.insert(0, "%s/../" % (cwd))  # Needed for MCOutput

from scipy.interpolate import PchipInterpolator


def alpha_omb_sol(component, what, pressure, temperature):
    # populate data for self in pt_output excel and then write to csv to use later
    df = pd.read_csv("kij_input/non-hydrocarbon-alpha-omb-sol/%s_%s" % (component, what))
    df.sort_values("pressure", axis=0, ascending=True, inplace=True, na_position='last')
    df.columns = df.columns.map(str)
    temperatures = get_temperatures(df)
    temperature = get_temperature(temperature)
    if pressure in df.pressure.tolist() and (temperature in temperatures):
        return df.loc[df.pressure == pressure, "%s" % temperature].values[0]
    # for each pressure, interpolate and find what will be the value at ref_temp
    alphas = list()
    temperatures = [x for x in df.columns if x != "pressure" and x != "302.03889"]
    for index, row in df.iterrows():
        x = [float(t) for t in temperatures]
        y = [row[t] for t in temperatures]
        # alphas.append(poas_interpolator().monotone(x, y, temperature)[0])
        alphas.append(PchipInterpolator(x, y)(temperature).item(0))

    # Now, interpolate for ref_pressure
    x = df.pressure.tolist()
    y = alphas
    # return poas_interpolator().monotone(x, y, pressure)[0]
    return PchipInterpolator(x, y)(pressure).item(0)


def get_temperatures(df):
    columns = [col.encode('ascii', 'ignore') for col in df.columns if col != "pressure"]
    fixed_columns = list()
    for col in columns:
        try:
            fixed_columns.append(int(col))
        except:
            fixed_columns.append(float(col))
    return fixed_columns


def get_temperature(temperature):
    try:
        fixed_temperature = int(temperature)
    except:
        fixed_temperature = float(temperature)
    return fixed_temperature
