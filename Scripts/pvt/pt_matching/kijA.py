import numpy as np
import pandas as pd


class kijA:
    def __init__(self):
        pass

    def run(self, df, temp, eos):
        eij = self.get_eij(df, temp, eos)
        kij = self.get_kij(df, temp, eos, eij)
        return eij, kij

    def get_groups(self, temp):
        group_values = pd.read_csv("kij_input/kij_group_data/GroupsData")
        groups = ["group%s" % i for i in range(1, 34)]
        final_data = pd.DataFrame()
        for group in groups:
            ref_group0 = group_values.loc[group_values.index % 2 == 0, group].reset_index(drop=True)
            ref_group1 = group_values.loc[group_values.index % 2 != 0, group].reset_index(drop=True)
            ref_group = ref_group0 * (298.15 / temp) ** (ref_group1 / ref_group0 - 1.)
            final_data[group] = ref_group
        final_data.set_index(final_data.columns, inplace=True)
        final_data = final_data.T
        return final_data

    def get_eij(self, df, temp, eos):
        group_values = self.get_groups(temp)
        groups = ["group%s" % i for i in range(1, 34)]
        df["ng"] = df.apply(lambda row: row[groups].sum(), axis=1)
        for group in groups:
            df[group] = df.apply(lambda row: 0 if row.ng == 0 else row[group] / row.ng, axis=1)
        eij_df = df[["scn", "component", "mf"]].copy(deep=True)
        for index, row in df.iterrows():
            difference = row[groups] - df[groups]
            products = difference.dot(group_values)
            answer = difference * products
            eij_df[row.scn if pd.isna(row.component) else row.component] = answer.apply(
                lambda row: row.sum() * -0.5 * 10 ** 6, axis=1)
        return eij_df

    def get_kij(self, df, temp, eos, eij):
        df = self.calc_aibybi(df, temp)
        kij_df = df.copy(deep=True)
        for index, row in df.iterrows():
            for_comp = row.scn if pd.isna(row.component) else row.component
            df["k1_others"] = (eij[for_comp] - (row.aibybi - df["aibybi"]) ** 2) / (row.aibybi * df["aibybi"] * 2)
            kij_df[for_comp] = (2. * (df.multiplier + row.multiplier) / 2. * df.k1_others * row.aibybi * df.aibybi + (
                        df.multiplier + row.multiplier) / 2. * (row.aibybi - df.aibybi) ** 2 - (
                                            row.AiByBi_eq - df.AiByBi_eq) ** 2) / (2. * row.AiByBi_eq * df.AiByBi_eq)
            kij_df[for_comp] = kij_df.apply(lambda row: row[for_comp] if np.abs(row[for_comp]) <= 0.4 else (
                0.6 * row[for_comp] if int(row[for_comp]) == 0 else (np.sign(row[for_comp]) * 0.5 * row[for_comp] / (
                            int(row[for_comp]) - (1 if int(row[for_comp]) < 0 else 0)))), axis=1)
        return kij_df

    def calc_aibybi(self, df, temp):
        df["k"] = df.apply(lambda row: (
                    0.379642 + 1.48503 * row.omega_final - 0.164423 * row.omega_final ** 2 + 0.016666 * row.omega_final ** 3) if row.omega_final > 0.491 else (
            (0.37464 + 1.54226 * row.omega_final - 0.26992 * row.omega_final ** 2) if row.omega_final <= 0.49099 else (
                        432.093117 * row.omega_final - 211.086595)), axis=1)
        df["alpha_for_eos"] = (1. + df.k * (1. - np.sqrt(temp / df.tc_k))) ** 2
        df["ai"] = (0.457235528921382 * ((8.314472 * df.tc_k) ** 2) / (df.pc_mpa * 10 ** 6)) * df.alpha_for_eos
        df["bi"] = 0.0777960739038885 * (8.314472 * df.tc_k / (df.pc_mpa * 10 ** 6))
        df["aibybi"] = np.sqrt(df.ai) / df.bi
        return df
