import sys

sys.path.insert(0, "%s/../" % (cwd))  # Needed for MCOutput

from AlphaFunctions import *

gasConstant = 8.314472


# self is not in use


class EOSAiByBiEquivalents(AlphaFunctions, object):
    def __init__(self):
        pass

    def calculateAiByBiEquivalentPR78(self, eos, df, temperature, alpha_i_method):
        df["ai"] = 0.45724 * (gasConstant * df.tc_calculated) ** 2 / (df.pc_calculated * 10 ** 6)
        df["bi"] = 0.0778 * gasConstant * df.tc_calculated / (df.pc_calculated * 10. ** 6)
        df["u"] = 2.
        df["w"] = -1.
        self.calculateAiByBiEquivalent(eos, df, temperature, alpha_i_method)
        return df

    def calculateUMixPR78(self, df):
        for col in ["bi"]: df["%sbyxi" % (col)] = df["%s" % (col)] * df.mf
        df["UMix"] = 2.
        df["WMix"] = -1.
        df["change_in_umnb_wrt_ni"] = 2. * df.bi
        df["change_in_wmnb_wrt_ni"] = -2. * (df.bibyxi.sum() * df.bi)

    def calculateAiByBiEquivalentMMM(self, eos, df, temperature, alpha_i_method):
        df["ai"] = 0.48748 * (gasConstant ** 2 * df.tc_calculated ** 2.5) / (df.pc_calculated * 10. ** 6)
        df["bi"] = 0.064662 * gasConstant * df.tc_calculated / (df.pc_calculated * 10. ** 6)
        df["u"] = 1.
        df["w"] = 0.
        self.calculateAiByBiEquivalent(eos, df, temperature, alpha_i_method)
        return df

    def calculateUMixMMM(self, df):
        for col in ["bi"]: df["%sbyxi" % (col)] = df["%s" % (col)] * df.mf

    def calculateAiByBiEquivalentSRK(self, eos, df, temperature, alpha_i_method):
        df["ai"] = 0.42478 * (gasConstant * df.tc_calculated) ** 2 / (df.pc_calculated * 10 ** 6)
        df["bi"] = 0.0866 * gasConstant * df.tc_calculated / (df.pc_calculated * 10. ** 6)
        df["u"] = 1.
        df["w"] = 0.
        self.calculateAiByBiEquivalent(eos, df, temperature, alpha_i_method)
        return df

    def calculateUMixSRK(self, df):
        for col in ["bi"]: df["%sbyxi" % (col)] = df["%s" % (col)] * df.mf
        df["UMix"] = 1.
        df["WMix"] = 0.
        df["change_in_umnb_wrt_ni"] = 2. * df.bi
        df["change_in_wmnb_wrt_ni"] = 0.

    def calculateAiByBiEquivalentLLS(self, eos, df, temperature, alpha_i_method, multiplier=1):
        df["omega_w"] = (0.361 / (1. + 0.0274 * df.omega_calculated))
        df.loc[df.index <= 13, ["omega_w"]] = [0.35, 0.3525, 0.325, 0.37, 0.35, 0.35, 0.325, 0.35, 0.34, 0.3525, 0.355,
                                               0.355, 0.355, 0.355]
        df.omega_w = multiplier * df.omega_w
        df["Final_om_b"] = df.zc * df.omega_w
        df["Final_om_a"] = (1. + (df.omega_w - 1.) * df.zc) ** 3
        df["ai"] = df.Final_om_a * (gasConstant * df.tc_calculated) ** 2 / (df.pc_calculated * 10 ** 6)
        df["bi"] = (df.Final_om_b * gasConstant * df.tc_calculated / (df.pc_calculated * 10. ** 6)) ** (1. / 3.)
        df["u"] = (1. + (df.omega_w - 3.) * df.zc) / (df.omega_w * df.zc)
        df["w"] = -1. * (df.zc ** 2 * (df.omega_w - 1.) ** 3 + 2. * df.omega_w ** 2 * df.zc + df.omega_w * (
                    1. - 3. * df.zc)) / (df.omega_w ** 2 * df.zc)
        self.calculateAiByBiEquivalent(eos, df, temperature, alpha_i_method)
        return df

    def calculateUMixLLS(self, df):
        for col in ["bi"]: df["%sbyxi" % (col)] = df["%s" % (col)] * df.mf
        df["alpha_mix"] = df.mf * df.u ** 0.5
        df["beta_mix"] = df.mf * (-1. * df.w) ** 0.5
        df["UMix"] = df.alpha_mix.sum() ** 2
        df["WMix"] = -df.beta_mix.sum() ** 2
        df["b_mix"] = df.bibyxi.sum() ** 3
        df["change_in_nb_wrt_ni"] = 3. * df.b_mix * ((df.bi ** 3) / df.b_mix) ** (1. / 3.)
        df["change_in_umnb_wrt_ni"] = df.UMix * 3. * df.b_mix * ((df.bi ** 3) / df.b_mix) ** (
                    1. / 3.) + df.b_mix * 2. * df.UMix * (df.u / df.UMix) ** 0.5
        df["change_in_wmnb_wrt_ni"] = df.WMix * 2. * df.b_mix ** 2 * 3. * ((df.bi ** 3) / df.b_mix) ** (
                    1. / 3.) - df.b_mix ** 2 * 2. * (-1. * df.WMix) * ((df.w * -1.) / (-df.WMix)) ** 0.5

    def calculateAiByBiEquivalentPT(self, eos, df, temperature, alpha_i_method):
        df["ziac"] = 0.329032 - 0.076799 * df.omega_calculated + 0.0211947 * df.omega_calculated ** 2
        df["a1"] = (2. - 3. * df.ziac)
        df["a2"] = 3. * df.ziac ** 2
        df["a3"] = -1. * df.ziac ** 3
        df["Q"] = (3. * df.a2 - df.a1 ** 2) / 9.
        df["L"] = (9. * df.a1 * df.a2 - 27. * df.a3 - 2. * df.a1 ** 3) / 54.
        df["D"] = df.Q ** 3 + df.L ** 2
        df["S1"] = df.apply(
            lambda row: (-1. * (-1. * (row.L + np.sqrt(row.D))) ** (1. / 3.)) if (row.L + np.sqrt(row.D)) < 0 else (
                        (row.L + np.sqrt(row.D)) ** (1. / 3.)), axis=1)
        df["S2"] = df.apply(
            lambda row: (-1. * (-1. * (row.L - np.sqrt(row.D))) ** (1. / 3.)) if (row.L - np.sqrt(row.D)) < 0 else (
                        (row.L - np.sqrt(row.D)) ** (1. / 3.)), axis=1)
        df["Z1"] = df.S1 + df.S2 - df.a1 / 3.
        df["Z2"] = df.apply(lambda row: ((-1. * (-row.L) ** (1. / 3.)) * -1. - row.a1 / 3.) if row.L < 0 else (
                    -1. * (row.L) ** (1. / 3.) - row.a1 / 3.), axis=1)
        df["cos"] = df.L / np.sqrt(-1. * df.Q ** 3)
        df["theta"] = np.degrees(np.arccos(df.cos))
        df["z1"] = 2. * np.sqrt(-df.Q) * np.cos(np.radians(df.theta / 3. + 120.)) - df.a1 / 3.
        df["z2"] = 2. * np.sqrt(-df.Q) * np.cos(np.radians(df.theta / 3. + 240.)) - df.a1 / 3.
        df["z3"] = 2. * np.sqrt(-df.Q) * np.cos(np.radians(df.theta / 3.)) - df.a1 / 3.
        df["final_Z1"] = df.apply(lambda row: row.z1 if row.D < 0 else row.Z1, axis=1)
        df["final_Z2"] = df.apply(lambda row: row.z2 if row.D < 0 else row.Z2, axis=1)
        df["final_Z3"] = df.apply(lambda row: row.z3 if row.D < 0 else row.Z2, axis=1)
        df["Final_om_b"] = df.apply(lambda row: self.calcOmBFinal(row), axis=1)
        df["Final_om_c"] = 1. - 3. * df.ziac
        df["Final_om_a"] = 3. * df.ziac ** 2 + 3. * (
                    1. - 2. * df.ziac) * df.Final_om_b + df.Final_om_b ** 2 + 1. - 3. * df.ziac
        df["ai"] = df.Final_om_a * (gasConstant * df.tc_calculated) ** 2 / (df.pc_calculated * 10 ** 6)
        df["bi"] = df.Final_om_b * gasConstant * df.tc_calculated / (df.pc_calculated * 10. ** 6)
        df["ci"] = np.abs(df.Final_om_c * gasConstant * df.tc_calculated / (df.pc_calculated * 10. ** 6))
        df["u"] = (df.bi + df.ci) / df.bi
        df["w"] = -df.ci / df.bi
        self.calculateAiByBiEquivalent(eos, df, temperature, alpha_i_method)
        return df

    def calculateUMixPT(self, df):
        for col in ["bi", "ci"]: df["%sbyxi" % (col)] = df["%s" % (col)] * df.mf
        df["UMix"] = (df.bibyxi.sum() + df.cibyxi.sum()) / df.bibyxi.sum()
        df["WMix"] = -df.cibyxi.sum() / df.bibyxi.sum()
        df["change_in_umnb_wrt_ni"] = df.bi + df.ci
        df["change_in_wmnb_wrt_ni"] = -1. * (df.cibyxi.sum() * df.bi + df.bibyxi.sum() * df.ci)

    def calculateAiByBiEquivalentTB(self, eos, df, temperature, alpha_i_method):
        df["vc"] = df.critical_volume * 10 ** -3
        df["ziac"] = 1.075 * df.zc
        df["Final_om_d"] = (0.341 * df.vc - 0.005) * (df.pc_calculated * 10 ** 3 / (gasConstant * df.tc_calculated))
        df["a1"] = (2. - 3. * df.ziac)
        df["a2"] = 3. * df.ziac ** 2
        df["a3"] = -(df.Final_om_d ** 2 + df.ziac ** 3)
        df["Q"] = (3. * df.a2 - df.a1 ** 2) / 9.
        df["L"] = (9. * df.a1 * df.a2 - 27. * df.a3 - 2. * df.a1 ** 3) / 54.
        df["D"] = df.Q ** 3 + df.L ** 2
        df["S1"] = df.apply(
            lambda row: (-1. * (-1. * (row.L + np.sqrt(row.D))) ** (1. / 3.)) if (row.L + np.sqrt(row.D)) < 0 else (
                        (row.L + np.sqrt(row.D)) ** (1. / 3.)), axis=1)
        df["S2"] = df.apply(
            lambda row: (-1. * (-1. * (row.L - np.sqrt(row.D))) ** (1. / 3.)) if (row.L - np.sqrt(row.D)) < 0 else (
                        (row.L - np.sqrt(row.D)) ** (1. / 3.)), axis=1)
        df["Z1"] = df.S1 + df.S2 - df.a1 / 3.
        df["Z2"] = df.apply(lambda row: ((-1. * (-row.L) ** (1. / 3.)) * -1. - row.a1 / 3.) if row.L < 0 else (
                    -1. * (row.L) ** (1. / 3.) - row.a1 / 3.), axis=1)
        df["cos"] = df.L / np.sqrt(-1. * df.Q ** 3)
        df["theta"] = np.degrees(np.arccos(df.cos))
        df["z1"] = 2. * np.sqrt(-df.Q) * np.cos(np.radians(df.theta / 3. + 120.)) - df.a1 / 3.
        df["z2"] = 2. * np.sqrt(-df.Q) * np.cos(np.radians(df.theta / 3. + 240.)) - df.a1 / 3.
        df["z3"] = 2. * np.sqrt(-df.Q) * np.cos(np.radians(df.theta / 3.)) - df.a1 / 3.
        df["final_Z1"] = df.apply(lambda row: row.z1 if row.D < 0 else row.Z1, axis=1)
        df["final_Z2"] = df.apply(lambda row: row.z2 if row.D < 0 else row.Z2, axis=1)
        df["final_Z3"] = df.apply(lambda row: row.z3 if row.D < 0 else row.Z2, axis=1)
        df["Final_om_b"] = df.apply(lambda row: self.calcOmBFinal(row), axis=1)
        df["Final_om_c"] = 1. - 3. * df.ziac
        df[
            "Final_om_a"] = 2. * df.Final_om_b * df.Final_om_c + df.Final_om_b + df.Final_om_c + df.Final_om_b ** 2 + df.Final_om_d ** 2 + 3. * df.ziac ** 2
        df["q2"] = df.apply(lambda row: self.calculateTB_q2(row), axis=1)
        df["Fb"] = df.apply(lambda row: 1. if temperature > row.tc_calculated else (
                    1. + row.q2 * (1. - (temperature / row.tc_calculated) + np.log(temperature / row.tc_calculated))),
                            axis=1)
        df["ai"] = df.Final_om_a * (gasConstant * df.tc_calculated) ** 2 / (df.pc_calculated * 10 ** 6)
        df["bi"] = df.Final_om_b * df.Fb * gasConstant * df.tc_calculated / (df.pc_calculated * 10. ** 6)
        df["ci"] = df.Final_om_c * gasConstant * df.tc_calculated / (df.pc_calculated * 10. ** 6)
        df["di"] = (df.Final_om_d * gasConstant * df.tc_calculated / (df.pc_calculated * 10 ** 6))
        df["u"] = (df.bi + df.ci) / df.bi
        df["w"] = -1. * (df.bi * df.ci + df.di ** 2) / df.bi ** 2
        self.calculateAiByBiEquivalent(eos, df, temperature, alpha_i_method)
        return df

    def calculateUMixTB(self, df):
        for col in ["bi", "ci", "di"]: df["%sbyxi" % (col)] = df["%s" % (col)] * df.mf
        df["UMix"] = (df.bibyxi.sum() + df.cibyxi.sum()) / df.bibyxi.sum()
        df["WMix"] = -1. * (df.bibyxi.sum() * df.cibyxi.sum() + df.dibyxi.sum() ** 2) / df.bibyxi.sum() ** 2
        df["change_in_umnb_wrt_ni"] = df.bi + df.ci
        df["change_in_wmnb_wrt_ni"] = -1. * (
                    df.cibyxi.sum() * df.bi + df.bibyxi.sum() * df.ci + 2. * df.dibyxi.sum() * df.di)

    def calculateAiByBiEquivalentTBS(self, eos, df, temperature, alpha_i_method):
        df["vc"] = df.critical_volume * 10 ** -3
        df["zc"] = 0.2918 - 0.0928 * df.omega_calculated
        df["ziac"] = 1.063 * df.zc
        df["Final_om_d"] = (1. / 3. * df.vc) * (df.pc_calculated * 10 ** 3 / (gasConstant * df.tc_calculated))
        df["a1"] = (2. - 3. * df.ziac)
        df["a2"] = 3. * df.ziac ** 2
        df["a3"] = -(df.Final_om_d ** 2 + df.ziac ** 3)
        df["Q"] = (3. * df.a2 - df.a1 ** 2) / 9.
        df["L"] = (9. * df.a1 * df.a2 - 27. * df.a3 - 2. * df.a1 ** 3) / 54.
        df["D"] = df.Q ** 3 + df.L ** 2
        df["S1"] = df.apply(
            lambda row: (-1. * (-1. * (row.L + np.sqrt(row.D))) ** (1. / 3.)) if (row.L + np.sqrt(row.D)) < 0 else (
                        (row.L + np.sqrt(row.D)) ** (1. / 3.)), axis=1)
        df["S2"] = df.apply(
            lambda row: (-1. * (-1. * (row.L - np.sqrt(row.D))) ** (1. / 3.)) if (row.L - np.sqrt(row.D)) < 0 else (
                        (row.L - np.sqrt(row.D)) ** (1. / 3.)), axis=1)
        df["Z1"] = df.S1 + df.S2 - df.a1 / 3.
        df["Z2"] = df.apply(lambda row: ((-1. * (-row.L) ** (1. / 3.)) * -1. - row.a1 / 3.) if row.L < 0 else (
                    -1. * (row.L) ** (1. / 3.) - row.a1 / 3.), axis=1)
        df["cos"] = df.L / np.sqrt(-1. * df.Q ** 3)
        df["theta"] = np.degrees(np.arccos(df.cos))
        df["z1"] = 2. * np.sqrt(-df.Q) * np.cos(np.radians(df.theta / 3. + 120.)) - df.a1 / 3.
        df["z2"] = 2. * np.sqrt(-df.Q) * np.cos(np.radians(df.theta / 3. + 240.)) - df.a1 / 3.
        df["z3"] = 2. * np.sqrt(-df.Q) * np.cos(np.radians(df.theta / 3.)) - df.a1 / 3.
        df["final_Z1"] = df.apply(lambda row: row.z1 if row.D < 0 else row.Z1, axis=1)
        df["final_Z2"] = df.apply(lambda row: row.z2 if row.D < 0 else row.Z2, axis=1)
        df["final_Z3"] = df.apply(lambda row: row.z3 if row.D < 0 else row.Z2, axis=1)
        df["Final_om_b"] = df.apply(lambda row: self.calcOmBFinal(row), axis=1)
        df["Final_om_c"] = 1. - 3. * df.ziac
        df[
            "Final_om_a"] = 2. * df.Final_om_b * df.Final_om_c + df.Final_om_b + df.Final_om_c + df.Final_om_b ** 2 + df.Final_om_d ** 2 + 3. * df.ziac ** 2
        df["q2"] = df.apply(lambda row: self.calculateTB_q2(row), axis=1)
        df["Fb"] = df.apply(lambda row: 1. if temperature > row.tc_calculated else (
                    1. + row.q2 * (1. - (temperature / row.tc_calculated) + np.log(temperature / row.tc_calculated))),
                            axis=1)
        df["ai"] = df.Final_om_a * (gasConstant * df.tc_calculated) ** 2 / (df.pc_calculated * 10 ** 6)
        df["bi"] = df.Final_om_b * df.Fb * gasConstant * df.tc_calculated / (df.pc_calculated * 10. ** 6)
        df["ci"] = df.Final_om_c * gasConstant * df.tc_calculated / (df.pc_calculated * 10. ** 6)
        df["di"] = (df.Final_om_d * gasConstant * df.tc_calculated / (df.pc_calculated * 10 ** 6))
        df["u"] = (df.bi + df.ci) / df.bi
        df["w"] = -1. * (df.bi * df.ci + df.di ** 2) / df.bi ** 2
        self.calculateAiByBiEquivalent(eos, df, temperature, alpha_i_method)
        return df

    def calculateUMixTBS(self, df):
        for col in ["bi", "ci", "di"]: df["%sbyxi" % (col)] = df["%s" % (col)] * df.mf
        df["UMix"] = (df.bibyxi.sum() + df.cibyxi.sum()) / df.bibyxi.sum()
        df["WMix"] = -1. * (df.bibyxi.sum() * df.cibyxi.sum() + df.dibyxi.sum() ** 2) / df.bibyxi.sum() ** 2
        df["change_in_umnb_wrt_ni"] = df.bi + df.ci
        df["change_in_wmnb_wrt_ni"] = -1. * (
                    df.cibyxi.sum() * df.bi + df.bibyxi.sum() * df.ci + 2. * df.dibyxi.sum() * df.di)

    def calculateTB_q2(self, row):
        if row.omega_calculated > 0.3: return (0.17959 + 0.23471 * row.omega_calculated)
        if row.omega_calculated < -0.0423: return 0
        return (
                    0.05246 + 1.15058 * row.omega_calculated - 1.99348 * row.omega_calculated ** 2 + 1.5949 * row.omega_calculated ** 3 - 1.39267 * row.omega_calculated ** 4)

    def calculateAiByBiEquivalentER(self, eos, df, temperature, alpha_i_method):
        df["tr"] = 0.789216 + 0.1585581 * df.omega_calculated - 0.133193 * df.omega_calculated ** 2
        df["ziac_temp"] = 0.3284438 - 0.0690264 * df.omega_calculated + 0.0078711 * df.omega_calculated ** 2
        df["ziac"] = df.apply(lambda row: (row.ziac_temp - (row.ziac_temp - row.zc) * (
                    (row.tr - (temperature / row.tc_calculated)) / (
                        row.tr - 1.)) ** 2) if temperature / row.tc_calculated > row.tr else row.ziac_temp, axis=1)
        df.loc[df.index <= 7, "ziac"] = df.ziac_temp
        df["a1"] = (3. * df.ziac - 5. / 8.)
        df["a2"] = (3. * df.ziac ** 2 - 3. / 4. * df.ziac)
        df["a3"] = df.ziac ** 3 - 3. / 8. * df.ziac ** 2
        df["Q"] = (3. * df.a2 - df.a1 ** 2) / 9.
        df["L"] = (9. * df.a1 * df.a2 - 27. * df.a3 - 2. * df.a1 ** 3) / 54.
        df["D"] = df.Q ** 3 + df.L ** 2
        df["S1"] = df.apply(
            lambda row: (-1. * (-1. * (row.L + np.sqrt(row.D))) ** (1. / 3.)) if (row.L + np.sqrt(row.D)) < 0 else (
                        (row.L + np.sqrt(row.D)) ** (1. / 3.)), axis=1)
        df["S2"] = df.apply(
            lambda row: (-1. * (-1. * (row.L - np.sqrt(row.D))) ** (1. / 3.)) if (row.L - np.sqrt(row.D)) < 0 else (
                        (row.L - np.sqrt(row.D)) ** (1. / 3.)), axis=1)
        df["Z1"] = df.S1 + df.S2 - df.a1 / 3.
        df["Z2"] = df.apply(lambda row: ((-1. * (-row.L) ** (1. / 3.)) * -1. - row.a1 / 3.) if row.L < 0 else (
                    -1. * (row.L) ** (1. / 3.) - row.a1 / 3.), axis=1)
        df["cos"] = df.L / np.sqrt(-1. * df.Q ** 3)
        df["theta"] = np.degrees(np.arccos(df.cos))
        df["z1"] = 2. * np.sqrt(-df.Q) * np.cos(np.radians(df.theta / 3. + 120.)) - df.a1 / 3.
        df["z2"] = 2. * np.sqrt(-df.Q) * np.cos(np.radians(df.theta / 3. + 240.)) - df.a1 / 3.
        df["z3"] = 2. * np.sqrt(-df.Q) * np.cos(np.radians(df.theta / 3.)) - df.a1 / 3.
        df["final_Z1"] = df.apply(lambda row: row.z1 if row.D < 0 else row.Z1, axis=1)
        df["final_Z2"] = df.apply(lambda row: row.z2 if row.D < 0 else row.Z2, axis=1)
        df["final_Z3"] = df.apply(lambda row: row.z3 if row.D < 0 else row.Z2, axis=1)
        df["Final_om_c"] = df.apply(lambda row: self.calcOmBFinal(row), axis=1)
        df["Final_om_b"] = 2. * df.Final_om_c - 1. + 3. * df.ziac
        df[
            "Final_om_a"] = 3. * df.ziac ** 2 + df.Final_om_c ** 2 + 2. * df.Final_om_b * df.Final_om_c + 2. * df.Final_om_c
        df["ai"] = df.Final_om_a * (gasConstant * df.tc_calculated) ** 2 / (df.pc_calculated * 10 ** 6)
        df["bi"] = df.Final_om_b * gasConstant * df.tc_calculated / (df.pc_calculated * 10. ** 6)
        df["ci"] = np.abs(df.Final_om_c * gasConstant * df.tc_calculated / (df.pc_calculated * 10. ** 6))
        df["u"] = 2. * df.ci / df.bi
        df["w"] = -1. * df.ci ** 2 / df.bi ** 2
        self.calculateAiByBiEquivalent(eos, df, temperature, alpha_i_method)
        return df

    def calculateUMixER(self, df):
        for col in ["bi", "ci"]: df["%sbyxi" % (col)] = df["%s" % (col)] * df.mf
        df["UMix"] = 2. * df.cibyxi.sum() / df.bibyxi.sum()
        df["WMix"] = -1. * df.cibyxi.sum() ** 2 / df.bibyxi.sum() ** 2
        df["change_in_umnb_wrt_ni"] = 2. * df.ci
        df["change_in_wmnb_wrt_ni"] = -2. * df.cibyxi.sum() * df.ci
        df["change_in_um_wrt_mf"] = 2. * (df.bibyxi.sum() * df.ci - df.cibyxi.sum() * df.bi) / df.bibyxi.sum() ** 2
        df["change_in_wm_wrt_mf"] = -1. * (
                    2. * df.bibyxi.sum() ** 2 * df.cibyxi.sum() * df.ci - 2. * df.cibyxi.sum() ** 2 * df.bibyxi.sum() * df.bi) / df.bibyxi.sum() ** 4
        df["change_in_nb_wrt_mf"] = 0
        df["change_in_umnb_wrt_mf"] = 0
        df["change_in_wmnb_wrt_mf"] = -2. * df.ci ** 2

    def calculateAiByBiEquivalentSW(self, eos, df, temperature, alpha_i_method):
        df["a1"] = 3. / (1. + 6. * df.omega_calculated)
        df["a2"] = df.a1
        df["a3"] = -1. / (1. + 6. * df.omega_calculated)
        df["Q"] = (3. * df.a2 - df.a1 ** 2) / 9.
        df["L"] = (9. * df.a1 * df.a2 - 27. * df.a3 - 2. * df.a1 ** 3) / 54.
        df["D"] = df.Q ** 3 + df.L ** 2
        df["S1"] = df.apply(
            lambda row: (-1. * (-1. * (row.L + np.sqrt(row.D))) ** (1. / 3.)) if (row.L + np.sqrt(row.D)) < 0 else (
                        (row.L + np.sqrt(row.D)) ** (1. / 3.)), axis=1)
        df["S2"] = df.apply(
            lambda row: (-1. * (-1. * (row.L - np.sqrt(row.D))) ** (1. / 3.)) if (row.L - np.sqrt(row.D)) < 0 else (
                        (row.L - np.sqrt(row.D)) ** (1. / 3.)), axis=1)
        df["Z1"] = df.S1 + df.S2 - df.a1 / 3.
        df["Z2"] = df.apply(lambda row: ((-1. * (-row.L) ** (1. / 3.)) * -1. - row.a1 / 3.) if row.L < 0 else (
                    -1. * (row.L) ** (1. / 3.) - row.a1 / 3.), axis=1)
        df["cos"] = df.L / np.sqrt(-1. * df.Q ** 3)
        df["theta"] = np.degrees(np.arccos(df.cos))
        df["z1"] = 2. * np.sqrt(-df.Q) * np.cos(np.radians(df.theta / 3. + 120.)) - df.a1 / 3.
        df["z2"] = 2. * np.sqrt(-df.Q) * np.cos(np.radians(df.theta / 3. + 240.)) - df.a1 / 3.
        df["z3"] = 2. * np.sqrt(-df.Q) * np.cos(np.radians(df.theta / 3.)) - df.a1 / 3.
        df["final_Z1"] = df.apply(lambda row: row.z1 if row.D < 0 else row.Z1, axis=1)
        df["final_Z2"] = df.apply(lambda row: row.z2 if row.D < 0 else row.Z2, axis=1)
        df["final_Z3"] = df.apply(lambda row: row.z3 if row.D < 0 else row.Z2, axis=1)
        df["q"] = df.apply(lambda row: self.calcOmBFinal(row), axis=1)
        df["eta"] = 1. / (3. * (1. + df.q * df.omega_calculated))
        df["Final_om_b"] = df.eta * df.q
        df["Final_om_a"] = (1. - df.eta * (1. - df.q)) ** 3
        df["ai"] = df.Final_om_a * (gasConstant * df.tc_calculated) ** 2 / (df.pc_calculated * 10 ** 6)
        df["bi"] = df.Final_om_b * gasConstant * df.tc_calculated / (df.pc_calculated * 10. ** 6)
        df["u"] = (1. + 3. * df.omega_calculated)
        df["w"] = df.apply(lambda row: -1 if row.omega_calculated < 0 else -3 * row.omega_calculated, axis=1)
        self.calculateAiByBiEquivalent(eos, df, temperature, alpha_i_method)
        return df

    def calculateUMixSW(self, df):
        for col in ["bi"]: df["%sbyxi" % (col)] = df["%s" % (col)] * df.mf
        df["UMix"] = sum(df.u * df.mf)
        df["WMix"] = sum(df.w * df.mf)
        df["change_in_umnb_wrt_ni"] = df.bi * df.UMix
        df["change_in_wmnb_wrt_ni"] = df.WMix * 2. * df.bibyxi.sum() * df.bi

    def calculateAiByBiEquivalentPOAS2(self, eos, df, temperature, alpha_i_method):
        df["vc"] = df.critical_volume * 10 ** -3
        df["u"] = (1.7574 + 4.0951 * df.omega_calculated + 1.7176 * df.omega_calculated ** 2)
        df["w"] = 0.01871 * df.u ** 2 - 1.42111 * df.u + 1.6165
        df["Final_om_d"] = (1. / 3. * df.vc) * (df.pc_calculated * 10 ** 3 / (gasConstant * df.tc_calculated))
        df["a1"] = 3. * (9. * df.w + 4. * df.u + 5) / (df.u + 2.) ** 3
        df["a2"] = 3. * (df.u + 2.) / (df.u + 2.) ** 3
        df["a3"] = -1. / (df.u + 2.) ** 3
        df["Q"] = (3. * df.a2 - df.a1 ** 2) / 9.
        df["L"] = (9. * df.a1 * df.a2 - 27. * df.a3 - 2. * df.a1 ** 3) / 54.
        df["D"] = df.Q ** 3 + df.L ** 2
        df["S1"] = df.apply(
            lambda row: (-1. * (-1. * (row.L + np.sqrt(row.D))) ** (1. / 3.)) if (row.L + np.sqrt(row.D)) < 0 else (
                        (row.L + np.sqrt(row.D)) ** (1. / 3.)), axis=1)
        df["S2"] = df.apply(
            lambda row: (-1. * (-1. * (row.L - np.sqrt(row.D))) ** (1. / 3.)) if (row.L - np.sqrt(row.D)) < 0 else (
                        (row.L - np.sqrt(row.D)) ** (1. / 3.)), axis=1)
        df["Z1"] = df.S1 + df.S2 - df.a1 / 3.
        df["Z2"] = df.apply(lambda row: ((-1. * (-row.L) ** (1. / 3.)) * -1. - row.a1 / 3.) if row.L < 0 else (
                    -1. * (row.L) ** (1. / 3.) - row.a1 / 3.), axis=1)
        df["cos"] = df.L / np.sqrt(-1. * df.Q ** 3)
        df["theta"] = np.degrees(np.arccos(df.cos))
        df["z1"] = 2. * np.sqrt(-df.Q) * np.cos(np.radians(df.theta / 3. + 120.)) - df.a1 / 3.
        df["z2"] = 2. * np.sqrt(-df.Q) * np.cos(np.radians(df.theta / 3. + 240.)) - df.a1 / 3.
        df["z3"] = 2. * np.sqrt(-df.Q) * np.cos(np.radians(df.theta / 3.)) - df.a1 / 3.
        df["final_Z1"] = df.apply(lambda row: row.z1 if row.D < 0 else row.Z1, axis=1)
        df["final_Z2"] = df.apply(lambda row: row.z2 if row.D < 0 else row.Z2, axis=1)
        df["final_Z3"] = df.apply(lambda row: row.z3 if row.D < 0 else row.Z2, axis=1)
        df["Final_om_b"] = df.apply(lambda row: self.calcOmBFinal(row), axis=1)
        df["zc"] = 1. / 3. * (1. + (1. - df.u) * df.Final_om_b)
        df["Final_om_a"] = 3. * df.zc ** 2 + (df.u - df.w) * df.Final_om_b ** 2 + df.u * df.Final_om_b
        df["ai"] = df.Final_om_a * (gasConstant * df.tc_calculated) ** 2 / (df.pc_calculated * 10 ** 6)
        df["bi"] = df.Final_om_b * gasConstant * df.tc_calculated / (df.pc_calculated * 10. ** 6)
        df["w_bkp"] = df.w
        df["w"] = df.apply(lambda row: -1 if row.w > 0 else row.w, axis=1)
        self.calculateAiByBiEquivalent(eos, df, temperature, alpha_i_method)
        return df

    def calculateUMixPOAS2(self, df):
        for col in ["bi"]: df["%sbyxi" % (col)] = df["%s" % (col)] * df.mf
        df["UMix"] = sum(df.u * df.mf)
        df["WMix"] = sum(df.w_bkp * df.mf)
        df["change_in_umnb_wrt_ni"] = df.UMix * df.bi + df.bibyxi.sum() * df.u
        df["change_in_wmnb_wrt_ni"] = (df.WMix * 2. * df.bibyxi.sum() * df.bi + df.bibyxi.sum() ** 2 * df.w_bkp)

    def calculateAiByBiEquivalentPOAS3VT(self, eos, df, temperature, alpha_i_method):
        df["vc"] = df.critical_volume * 10 ** -3
        df["tc"] = (gasConstant * df.tc_calculated / (df.pc_calculated * 10 ** 6)) * (0.3074 - df.zc)
        df["beta"] = -10.2447 - 28.6312 * df.omega_calculated
        df["t0"] = (gasConstant * df.tc_calculated / (df.pc_calculated * 10 ** 6)) * (
                    -0.014471 + 0.067498 * df.omega_calculated - 0.084852 * df.omega_calculated ** 2 + 0.067298 * df.omega_calculated ** 3 - 0.017366 * df.omega_calculated ** 4)
        df["t"] = df.t0 + (df.tc - df.t0) * np.exp((df.beta * np.abs(1. - temperature / df.tc_calculated)))
        df["bpr"] = 0.0777960739038885 * gasConstant * df.tc_calculated / (df.pc_calculated * 10 ** 6)
        df["d"] = df.bpr - df.t
        df["ai"] = 0.457235529 * (gasConstant * df.tc_calculated) ** 2 / (df.pc_calculated * 10 ** 6)
        df["bi"] = df.bpr
        df["u"] = 2. * (df.bi + df.t) / (df.bi - df.t)
        df["w"] = ((df.bi + df.t) ** 2 - 2. * df.bi ** 2) / (df.bi - df.t) ** 2
        self.calculateAiByBiEquivalent(eos, df, temperature, alpha_i_method)
        return df

    def calculateUMixPOAS3VT(self, df):
        for col in ["bi", "t"]: df["%sbyxi" % (col)] = df["%s" % (col)] * df.mf
        df["dmix"] = df.bibyxi - df.tbyxi
        df["UMix"] = 2. * (df.bibyxi.sum() + df.tbyxi.sum()) / (df.bibyxi.sum() - df.tbyxi.sum())
        df["WMix"] = ((df.tbyxi.sum() + df.AiByBi.sum()) ** 2 - 2. * df.bibyxi.sum() ** 2) / (
                    df.bibyxi.sum() - df.tbyxi.sum()) ** 2
        df["change_in_nb_wrt_ni"] = df.d
        df["change_in_umnb_wrt_ni"] = 2. * (df.bi + df.t)
        df["change_in_wmnb_wrt_ni"] = 2. * (df.bibyxi.sum() + df.tbyxi.sum()) * (
                    df.t + df.bi) - 2. * df.bibyxi.sum() * df.bi

    def calculateAiByBiEquivalentPOASD(self, eos, df, temperature, alpha_i_method, multiplier=1):
        df["vc"] = df.critical_volume * 10 ** -3
        df["ziac"] = multiplier * df.zc
        df["Final_om_b_1"] = 0.9129 * df.ziac ** 2 - 0.0551 * df.ziac + 0.0368
        df["Final_om_b_2"] = 0.0778 - 0.03452 * df.omega_calculated + 0.0033 * df.omega_calculated ** 2
        df["Final_om_b_3"] = 0.08974 - 0.03452 * df.omega_calculated + 0.0033 * df.omega_calculated ** 2
        df["a1"] = (3. * df.ziac - 1.75) * -1.
        df["a2"] = (3. * df.ziac ** 2 + 0.5 * (1. - 3. * df.ziac))
        df["a3"] = (df.ziac ** 3 + 0.25 * (1. - 3. * df.ziac) ** 2) * -1.
        df["Q"] = (3. * df.a2 - df.a1 ** 2) / 9.
        df["L"] = (9. * df.a1 * df.a2 - 27. * df.a3 - 2. * df.a1 ** 3) / 54.
        df["D"] = df.Q ** 3 + df.L ** 2
        df["S1"] = df.apply(
            lambda row: (-1. * (-1. * (row.L + np.sqrt(row.D))) ** (1. / 3.)) if (row.L + np.sqrt(row.D)) < 0 else (
                        (row.L + np.sqrt(row.D)) ** (1. / 3.)), axis=1)
        df["S2"] = df.apply(
            lambda row: (-1. * (-1. * (row.L - np.sqrt(row.D))) ** (1. / 3.)) if (row.L - np.sqrt(row.D)) < 0 else (
                        (row.L - np.sqrt(row.D)) ** (1. / 3.)), axis=1)
        df["Z1"] = df.S1 + df.S2 - df.a1 / 3.
        df["Z2"] = df.apply(lambda row: ((-1. * (-row.L) ** (1. / 3.)) * -1. - row.a1 / 3.) if row.L < 0 else (
                    -1. * (row.L) ** (1. / 3.) - row.a1 / 3.), axis=1)
        df["cos"] = df.L / np.sqrt(-1. * df.Q ** 3)
        df["theta"] = np.degrees(np.arccos(df.cos))
        df["z1"] = 2. * np.sqrt(-df.Q) * np.cos(np.radians(df.theta / 3. + 120.)) - df.a1 / 3.
        df["z2"] = 2. * np.sqrt(-df.Q) * np.cos(np.radians(df.theta / 3. + 240.)) - df.a1 / 3.
        df["z3"] = 2. * np.sqrt(-df.Q) * np.cos(np.radians(df.theta / 3.)) - df.a1 / 3.
        df["final_Z1"] = df.apply(lambda row: row.z1 if row.D < 0 else row.Z1, axis=1)
        df["final_Z2"] = df.apply(lambda row: row.z2 if row.D < 0 else row.Z2, axis=1)
        df["final_Z3"] = df.apply(lambda row: row.z3 if row.D < 0 else row.Z2, axis=1)
        df["Final_om_b"] = df.apply(
            lambda row: self.calcOmBFinal(row) if self.calcOmBFinal(row) < row.Final_om_b_1 else row.Final_om_b_1,
            axis=1)

        df["del_b"] = df.apply(lambda row: 0 if ((row.ziac - row.Final_om_b) ** 3 + 0.25 * (
                    1. - 3. * row.ziac + row.Final_om_b) ** 2 + 3. * row.ziac * row.Final_om_b - row.Final_om_b * (
                                                             1. + 2. * row.Final_om_b)) < 0 else (
                    (row.ziac - row.Final_om_b) ** 3 + 0.25 * (
                        1. - 3. * row.ziac + row.Final_om_b) ** 2 + 3. * row.ziac * row.Final_om_b - row.Final_om_b * (
                                1. + 2. * row.Final_om_b)), axis=1)
        df["Final_om_c"] = (df.Final_om_b + 1. - 3. * df.ziac + (4. * df.del_b) ** 0.5) / 2.
        df["Final_om_d"] = (df.Final_om_b + 1. - 3. * df.ziac - (4. * df.del_b) ** 0.5) / 2.
        df[
            "Final_om_a"] = 3. * df.ziac ** 2 + df.Final_om_b * df.Final_om_c + df.Final_om_b * df.Final_om_d + df.Final_om_c * df.Final_om_d + df.Final_om_c + df.Final_om_d
        df["ai"] = df.Final_om_a * (gasConstant * df.tc_calculated) ** 2 / (df.pc_calculated * 10 ** 6)
        df["bi"] = df.Final_om_b * gasConstant * df.tc_calculated / (df.pc_calculated * 10. ** 6)
        df["ci"] = df.Final_om_c * gasConstant * df.tc_calculated / (df.pc_calculated * 10. ** 6)
        df["di"] = (df.Final_om_d * gasConstant * df.tc_calculated / (df.pc_calculated * 10 ** 6))
        df["u"] = (df.di + df.ci) / df.bi
        df["w"] = -1. * (df.ci * df.di) / df.bi ** 2
        self.calculateAiByBiEquivalent(eos, df, temperature, alpha_i_method)
        return df

    def calculateUMixPOASD(self, df):
        for col in ["bi", "ci", "di"]: df["%sbyxi" % (col)] = df["%s" % (col)] * df.mf
        df["UMix"] = (df.dibyxi.sum() + df.cibyxi.sum()) / df.bibyxi.sum()
        df["WMix"] = -1. * (df.cibyxi.sum() * df.dibyxi.sum()) / df.bibyxi.sum() ** 2
        df["change_in_umnb_wrt_ni"] = df.di + df.ci
        df["change_in_wmnb_wrt_ni"] = -1. * (df.cibyxi.sum() * df.di + df.dibyxi.sum() * df.ci)

    def calculateAiByBiEquivalentPOASD2(self, eos, df, temperature, alpha_i_method, multiplier=1):
        df["vc"] = df.critical_volume * 10 ** -3
        df["ziac"] = multiplier * df.zc
        df["A"] = 0.27 * df.omega_calculated ** 2 - 0.1883 * df.omega_calculated - 0.0799
        df["B"] = -0.2634 * df.omega_calculated ** 2 + 0.1786 * df.omega_calculated + 0.028
        df["C"] = 0.0832 * df.omega_calculated ** 2 - 0.0787 * df.omega_calculated + 0.0994
        df["Final_om_b_1"] = df.apply(
            lambda row: np.abs(row.C - row.B ** 2 / (4. * row.A)) if (temperature / row.tc_calculated) > np.abs(
                1. + row.B / (2. * row.A)) else np.abs(row.A * (1. - (temperature / row.tc_calculated)) ** 2 + row.B * (
                        1. - (temperature / row.tc_calculated)) + row.C), axis=1)
        df["a1"] = (3. * df.ziac - 1.75) * -1.
        df["a2"] = (3. * df.ziac ** 2 + 0.5 * (1. - 3. * df.ziac))
        df["a3"] = (df.ziac ** 3 + 0.25 * (1. - 3. * df.ziac) ** 2) * -1.
        df["Q"] = (3. * df.a2 - df.a1 ** 2) / 9.
        df["L"] = (9. * df.a1 * df.a2 - 27. * df.a3 - 2. * df.a1 ** 3) / 54.
        df["D"] = df.Q ** 3 + df.L ** 2
        df["S1"] = df.apply(
            lambda row: (-1. * (-1. * (row.L + np.sqrt(row.D))) ** (1. / 3.)) if (row.L + np.sqrt(row.D)) < 0 else (
                        (row.L + np.sqrt(row.D)) ** (1. / 3.)), axis=1)
        df["S2"] = df.apply(
            lambda row: (-1. * (-1. * (row.L - np.sqrt(row.D))) ** (1. / 3.)) if (row.L - np.sqrt(row.D)) < 0 else (
                        (row.L - np.sqrt(row.D)) ** (1. / 3.)), axis=1)
        df["Z1"] = df.S1 + df.S2 - df.a1 / 3.
        df["Z2"] = df.apply(lambda row: ((-1. * (-row.L) ** (1. / 3.)) * -1. - row.a1 / 3.) if row.L < 0 else (
                    -1. * (row.L) ** (1. / 3.) - row.a1 / 3.), axis=1)
        df["cos"] = df.L / np.sqrt(-1. * df.Q ** 3)
        df["theta"] = np.degrees(np.arccos(df.cos))
        df["z1"] = 2. * np.sqrt(-df.Q) * np.cos(np.radians(df.theta / 3. + 120.)) - df.a1 / 3.
        df["z2"] = 2. * np.sqrt(-df.Q) * np.cos(np.radians(df.theta / 3. + 240.)) - df.a1 / 3.
        df["z3"] = 2. * np.sqrt(-df.Q) * np.cos(np.radians(df.theta / 3.)) - df.a1 / 3.
        df["final_Z1"] = df.apply(lambda row: row.z1 if row.D < 0 else row.Z1, axis=1)
        df["final_Z2"] = df.apply(lambda row: row.z2 if row.D < 0 else row.Z2, axis=1)
        df["final_Z3"] = df.apply(lambda row: row.z3 if row.D < 0 else row.Z2, axis=1)
        df["Final_om_b"] = df.apply(
            lambda row: self.calcOmBFinal(row) if self.calcOmBFinal(row) < row.Final_om_b_1 else row.Final_om_b_1,
            axis=1)
        df["del_b"] = df.apply(lambda row: 0 if ((row.ziac - row.Final_om_b) ** 3 + 0.25 * (
                    1. - 3. * row.ziac + row.Final_om_b) ** 2 + 3. * row.ziac * row.Final_om_b - row.Final_om_b * (
                                                             1. + 2. * row.Final_om_b)) < 0 else (
                    (row.ziac - row.Final_om_b) ** 3 + 0.25 * (
                        1. - 3. * row.ziac + row.Final_om_b) ** 2 + 3. * row.ziac * row.Final_om_b - row.Final_om_b * (
                                1. + 2. * row.Final_om_b)), axis=1)
        df["Final_om_c"] = (df.Final_om_b + 1. - 3. * df.ziac + (4. * df.del_b) ** 0.5) / 2.
        df["Final_om_d"] = (df.Final_om_b + 1. - 3. * df.ziac - (4. * df.del_b) ** 0.5) / 2.
        df[
            "Final_om_a"] = 3. * df.ziac ** 2 + df.Final_om_b * df.Final_om_c + df.Final_om_b * df.Final_om_d + df.Final_om_c * df.Final_om_d + df.Final_om_c + df.Final_om_d
        df["ai"] = df.Final_om_a * (gasConstant * df.tc_calculated) ** 2 / (df.pc_calculated * 10 ** 6)
        df["bi"] = df.Final_om_b * gasConstant * df.tc_calculated / (df.pc_calculated * 10. ** 6)
        df["ci"] = df.Final_om_c * gasConstant * df.tc_calculated / (df.pc_calculated * 10. ** 6)
        df["di"] = (df.Final_om_d * gasConstant * df.tc_calculated / (df.pc_calculated * 10 ** 6))
        df["u"] = (df.di + df.ci) / df.bi
        df["w"] = -1. * (df.ci * df.di) / df.bi ** 2
        self.calculateAiByBiEquivalent(eos, df, temperature, alpha_i_method)
        return df

    def calculateUMixPOASD2(self, df):
        for col in ["bi", "ci", "di"]: df["%sbyxi" % (col)] = df["%s" % (col)] * df.mf
        df["UMix"] = (df.dibyxi.sum() + df.cibyxi.sum()) / df.bibyxi.sum()
        df["WMix"] = -1. * (df.cibyxi.sum() * df.dibyxi.sum()) / df.bibyxi.sum() ** 2
        df["change_in_umnb_wrt_ni"] = df.di + df.ci
        df["change_in_wmnb_wrt_ni"] = -1. * (df.cibyxi.sum() * df.di + df.dibyxi.sum() * df.ci)

    def calculateAiByBiEquivalentPOAS1(self, eos, df, temperature, alpha_i_method, multiplier=1):
        df["a1"] = (1. - 3. * df.zc * multiplier)
        df["a2"] = 3. * (df.zc * multiplier) ** 2
        df["a3"] = -(df.zc * multiplier) ** 3
        df["Q"] = (3. * df.a2 - df.a1 ** 2) / 9.
        df["L"] = (9. * df.a1 * df.a2 - 27. * df.a3 - 2. * df.a1 ** 3) / 54.
        df["D"] = df.Q ** 3 + df.L ** 2
        df["S1"] = df.apply(
            lambda row: (-1. * (-1. * (row.L + np.sqrt(row.D))) ** (1. / 3.)) if (row.L + np.sqrt(row.D)) < 0 else (
                        (row.L + np.sqrt(row.D)) ** (1. / 3.)), axis=1)
        df["S2"] = df.apply(
            lambda row: (-1. * (-1. * (row.L - np.sqrt(row.D))) ** (1. / 3.)) if (row.L - np.sqrt(row.D)) < 0 else (
                        (row.L - np.sqrt(row.D)) ** (1. / 3.)), axis=1)
        df["Z1"] = df.S1 + df.S2 - df.a1 / 3.
        df["Z2"] = df.apply(lambda row: ((-1. * (-row.L) ** (1. / 3.)) * -1. - row.a1 / 3.) if row.L < 0 else (
                    -1. * (row.L) ** (1. / 3.) - row.a1 / 3.), axis=1)
        df["cos"] = df.L / np.sqrt(-1. * df.Q ** 3)
        df["theta"] = np.degrees(np.arccos(df.cos))
        df["z1"] = 2. * np.sqrt(-df.Q) * np.cos(np.radians(df.theta / 3. + 120.)) - df.a1 / 3.
        df["z2"] = 2. * np.sqrt(-df.Q) * np.cos(np.radians(df.theta / 3. + 240.)) - df.a1 / 3.
        df["z3"] = 2. * np.sqrt(-df.Q) * np.cos(np.radians(df.theta / 3.)) - df.a1 / 3.
        df["final_Z1"] = df.apply(lambda row: row.z1 if row.D < 0 else row.Z1, axis=1)
        df["final_Z2"] = df.apply(lambda row: row.z2 if row.D < 0 else row.Z2, axis=1)
        df["final_Z3"] = df.apply(lambda row: row.z3 if row.D < 0 else row.Z2, axis=1)
        df["Final_om_b"] = df.apply(lambda row: self.calcOmBFinal(row), axis=1)
        df["Final_om_c"] = 1. - 3. * df.zc * multiplier
        df["Final_om_a"] = 1. - 3. * df.zc * multiplier * (1. - df.zc * multiplier) + 3. * (
                    1. - 2. * df.zc * multiplier) * df.Final_om_b + 2. * df.Final_om_b ** 2
        df["ai"] = df.Final_om_a * (gasConstant * df.tc_calculated) ** 2 / (df.pc_calculated * 10 ** 6)
        df["bi"] = df.Final_om_b * gasConstant * df.tc_calculated / (df.pc_calculated * 10. ** 6)
        df["ci"] = np.abs(df.Final_om_c * gasConstant * df.tc_calculated / (df.pc_calculated * 10. ** 6))
        df["u"] = (df.bi + df.ci) / df.bi
        df["w"] = -1. * df.u
        self.calculateAiByBiEquivalent(eos, df, temperature, alpha_i_method)
        return df

    def calculateUMixPOAS1(self, df):
        for col in ["bi", "ci"]: df["%sbyxi" % (col)] = df["%s" % (col)] * df.mf
        df["UMix"] = (df.bibyxi.sum() + df.cibyxi.sum()) / df.bibyxi.sum()
        df["WMix"] = -1. * df.UMix
        df["change_in_umnb_wrt_ni"] = df.bi + df.ci
        df["change_in_wmnb_wrt_ni"] = -1. * (
                    2. * df.bibyxi.sum() * df.bi + df.ci * df.bibyxi.sum() + df.bi * df.cibyxi.sum())

    def calculateAiByBiEquivalentASL(self, eos, df, temperature, alpha_i_method):
        df[
            "Final_om_c"] = 0.0506 + 0.04184 * df.omega_calculated + 0.16413 * df.omega_calculated ** 2 - 0.03975 * df.omega_calculated ** 3
        df[
            "Final_om_b"] = 0.08779 - 0.02181 * df.omega_calculated - 0.06708 * df.omega_calculated ** 2 + 0.10617 * df.omega_calculated ** 3
        df[
            "Final_om_a"] = 0.43711 + 0.02366 * df.omega_calculated + 0.10538 * df.omega_calculated ** 2 + 0.10164 * df.omega_calculated ** 3
        df["ai"] = df.Final_om_a * (gasConstant * df.tc_calculated) ** 2 / (df.pc_calculated * 10 ** 6)
        df["bi"] = df.Final_om_b * gasConstant * df.tc_calculated / (df.pc_calculated * 10. ** 6)
        df["ci"] = np.abs(df.Final_om_c * gasConstant * df.tc_calculated / (df.pc_calculated * 10 ** 6))
        df["u"] = 2. * df.ci / df.bi
        df["w"] = -1. * df.ci ** 2 / df.bi ** 2
        self.calculateAiByBiEquivalent(eos, df, temperature, alpha_i_method)
        return df

    def calculateUMixASL(self, df):
        for col in ["bi", "ci"]: df["%sbyxi" % (col)] = df["%s" % (col)] * df.mf
        df["UMix"] = 2. * df.cibyxi.sum() / df.bibyxi.sum()
        df["WMix"] = -1. * df.cibyxi.sum() ** 2 / df.bibyxi.sum() ** 2
        df["change_in_umnb_wrt_ni"] = 2. * df.ci
        df["change_in_wmnb_wrt_ni"] = -1. * 2. * (df.cibyxi.sum() * df.ci)

    def calculateAiByBiEquivalentPTVC(self, eos, df, temperature, alpha_i_method):
        df["Final_om_c"] = (0.5775 - 1.89841 * df.zc)
        df["Final_om_b"] = 0.025987 + 0.18075 * df.zc + 0.06126 * df.zc ** 2
        df[
            "Final_om_a"] = 0.69368 - 1.06344 * df.zc + 0.6829 * df.zc ** 2 - 0.21044 * df.zc ** 3 + 0.0037527 * df.zc ** 4
        df["ai"] = df.Final_om_a * (gasConstant * df.tc_calculated) ** 2 / (df.pc_calculated * 10 ** 6)
        df["bi"] = df.Final_om_b * gasConstant * df.tc_calculated / (df.pc_calculated * 10. ** 6)
        df["ci"] = np.abs(df.Final_om_c * gasConstant * df.tc_calculated / (df.pc_calculated * 10 ** 6))
        df["u"] = (df.bi + df.ci) / df.bi
        df["w"] = -1. * df.ci / df.bi
        self.calculateAiByBiEquivalent(eos, df, temperature, alpha_i_method)
        return df

    def calculateUMixPTVC(self, df):
        for col in ["bi", "ci"]: df["%sbyxi" % (col)] = df["%s" % (col)] * df.mf
        df["UMix"] = (df.bibyxi.sum() + df.cibyxi.sum()) / df.bibyxi.sum()
        df["WMix"] = -1. * df.cibyxi.sum() / df.bibyxi.sum()
        df["change_in_umnb_wrt_ni"] = df.bi + df.ci
        df["change_in_wmnb_wrt_ni"] = -1. * (df.cibyxi.sum() * df.bi + df.bibyxi.sum() * df.ci)

    def calculateAiByBiEquivalentPTV(self, eos, df, temperature, alpha_i_method):
        df["Final_om_c"] = (0.57765 - 1.8708 * df.zc)
        df["Final_om_b"] = (0.02207 + 0.20868 * df.zc)
        df["Final_om_a"] = 0.66121 - 0.76105 * df.zc
        df["ai"] = df.Final_om_a * (gasConstant * df.tc_calculated) ** 2 / (df.pc_calculated * 10 ** 6)
        df["bi"] = df.Final_om_b * gasConstant * df.tc_calculated / (df.pc_calculated * 10. ** 6)
        df["ci"] = np.abs(df.Final_om_c * gasConstant * df.tc_calculated / (df.pc_calculated * 10 ** 6))
        df["u"] = (df.bi + df.ci) / df.bi
        df["w"] = -1. * df.ci / df.bi
        self.calculateAiByBiEquivalent(eos, df, temperature, alpha_i_method)
        return df

    def calculateUMixPTV(self, df):
        for col in ["bi", "ci"]: df["%sbyxi" % (col)] = df["%s" % (col)] * df.mf
        df["UMix"] = (df.bibyxi.sum() + df.cibyxi.sum()) / df.bibyxi.sum()
        df["WMix"] = -1. * df.cibyxi.sum() / df.bibyxi.sum()
        df["change_in_umnb_wrt_ni"] = df.bi + df.ci
        df["change_in_wmnb_wrt_ni"] = -1. * (df.cibyxi.sum() * df.bi + df.bibyxi.sum() * df.ci)

    def calculateAiByBiEquivalent(self, eos, df, temperature, alpha_i_method):
        getattr(self, "calculateUMix%s" % (eos))(df)
        df["r1"] = (-1. * df.u + np.sqrt(df.u ** 2 - 4. * df.w)) / 2.
        df["r2"] = (-1 * df.u - np.sqrt(df.u ** 2 - 4. * df.w)) / 2.
        df["x"] = df.apply(lambda row: self.calcX(row), axis=1)
        df["om_a_eq"] = (1. - df.r1 * df.x) * (1. - df.r2 * df.x) * (2. - (df.r1 + df.r2) * df.x) / (
                    (1. - df.x) * (3. - df.x * (1. + df.r1 + df.r2)) ** 2)
        df["om_b_eq"] = df.x / (3. - df.x * (1. + df.r1 + df.r2))
        df["c_eos"] = df.apply(lambda row: (1. / (1. - row.r1)) if (row.r1 == row.r2) else (
                    1. / (row.r1 - row.r2) * np.log(np.abs((1. - row.r2) / (1. - row.r1)))), axis=1)
        cpr_EOS, om_b = 0.623225240140231, 0.0777960739038885
        df["multiplier"] = cpr_EOS * om_b / (df.c_eos * df.om_b_eq)
        self.getAlphaI(eos, df, temperature, alpha_i_method)
        df["ai_eq"] = (df.om_a_eq * (gasConstant ** 2 * df.tc_calculated ** 2.5) / (
                    df.pc_calculated * 10. ** 6)) * df.alpha if eos == "MMM" else (df.om_a_eq * (
                    (gasConstant * df.tc_calculated) ** 2) / (df.pc_calculated * 10. ** 6)) * df.alpha
        df["bi_eq"] = df.om_b_eq * (gasConstant * df.tc_calculated / (df.pc_calculated * 10. ** 6))
        df["AiByBi_eq"] = np.sqrt(df.ai_eq) / df.bi_eq
        # self.getAlphaDifference(df)

    def getAlphaDifference(self, df):
        df["vc_from_eos"] = ((df.bi * (df.u - 1.) - gasConstant * df.tc_calculated / (df.pc_calculated * 10 ** 6)) / (
            -3.)) * 10 ** 6
        df["vc"] = df.critical_volume * 10 ** -3
        df["alpha1_term"] = (df.vc * 10 ** -6) ** 3 - (df.w * df.bi ** 3 + df.w * df.bi ** 2 * (
                    gasConstant * df.tc_calculated / (df.pc_calculated * 10 ** 6)))
        df["alpha1_term1"] = df.ai * df.bi / (df.pc_calculated * 10 ** 6)
        df["alpha1"] = df.alpha1_term / df.alpha1_term1
        df["alpha2_term"] = 3. * (df.vc * 10 ** -6) ** 2 - (df.bi ** 2 * (df.w - df.u) - df.bi * df.u * (
                    gasConstant * df.tc_calculated / (df.pc_calculated * 10 ** 6)))
        df["alpha2_term1"] = df.ai / (df.pc_calculated * 10 ** 6)
        df["alpha2"] = df.alpha2_term / df.alpha2_term1
        df["alpha_difference"] = np.abs(df.alpha1 - 1.) + np.abs(df.alpha2 - 1.)

    def calcX(self, row):
        if row.r1 > 1 and row.r2 > 1:
            return (1. + -1. * (-1. * ((1. - row.r1) * (1. - row.r2) ** 2)) ** (1. / 3.) + -1. * (
                        -1. * ((1. - row.r2) * (1. - row.r1)) ** 2) ** (1. / 3.)) ** -1.
        elif row.r1 > 1:
            return (1. + -1. * (-1. * ((1. - row.r1) * (1. - row.r2) ** 2)) ** (1. / 3.) + (
                        (1. - row.r2) * (1. - row.r1) ** 2) ** (1. / 3.)) ** -1.
        elif row.r2 > 1:
            return (1. + ((1. - row.r1) * (1. - row.r2) ** 2) ** (1. / 3.) + -1. * (
                        -1. * ((1. - row.r2) * (1. - row.r1)) ** 2) ** (1. / 3.)) ** -1.
        return (1. + ((1. - row.r1) * (1. - row.r2) ** 2) ** (1. / 3.) + ((1. - row.r2) * (1. - row.r1) ** 2) ** (
                    1. / 3.)) ** -1.

    def calcOmBFinal(self, row):
        pos = list()
        for val in [row.final_Z1, row.final_Z2, row.final_Z3]:
            if val > 0: pos.append(val)
        return min(pos)
