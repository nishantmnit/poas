from Lib.DBLib import *
from Lib.Utils import *
from Scripts.pvt.core.optimizers.ncg import *

from Scripts.pvt.core.optimizers.poas_interpolator import monotone


class SolutionGORBubblePoint(object):
    def __init__(self, dbName=None):
        self.fluid_codes = ['bo', 'vh', 'vl', 'gc']
        self.dbname = dbName
        self.db = getDbCon(self.dbname)
        self.cenDb = getDbCon('Central')  # default is RTS
        self.cenDbPBO = self.cenDb[dbName]
        self.dateYmdHMS = datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')

    def input_data(self):
        self.averageGasGravity = float(self.df_general_data['value'][
                                           'gas_sp_gravity'])  # take from MongoDB user input. If not available then compute
        self.reservoirTemp = float(self.df_general_data['value']['temp_res'])
        self.bubblePointPressure = float(self.df_general_data['value']['pb_res_temp'])
        self.stockTankOilAPI = float(self.df_general_data['value']['api_gravity'])
        self.rsb = float(self.df_general_data['value']['gor_pb'])
        if not self.df_separator_corrected.empty:
            bubblePointPressureIdx = \
            (self.df_separator_corrected['pressure'] - self.bubblePointPressure).abs().argsort()[0]
            self.pressure = np.array(
                self.df_separator_corrected['pressure'].iloc[bubblePointPressureIdx:].astype(float).tolist())
            self.rs = np.array(self.df_separator_corrected['gor'].iloc[bubblePointPressureIdx:].astype(float).tolist())
            gas_sp_gravity_calc, api_gravity_calc = self.input_solver()
            # if gas_sp_gravity_calc is not None and api_gravity_calc is not None and (self.averageGasGravity!= gas_sp_gravity_calc or self.stockTankOilAPI!=api_gravity_calc) then show warning on UI.
            self.averageGasGravity = gas_sp_gravity_calc
            self.stockTankOilAPI = api_gravity_calc

    def input_solver(self):  # Seperator Test DATA
        gas_sp_gravity_calc, api_gravity_calc = None, None
        try:
            df_separator1 = self.df_separator[1:]
            if len(df_separator1) <= 2 and df_separator1['gas_gravity'].isnull().values.any():
                self.logger.warn("Require min 2 values to compute missing gas_gravity")
                return
            if len(df_separator1) > 2 and df_separator1['gas_gravity'].isnull().values.any():
                if df_separator1['gas_gravity'][0:2].isnull().values.any():
                    self.logger.warn("Require initial 2 values to compute missing gas_gravity")
                    return
                else:
                    dataIdxes = []
                    for index, row in df_separator1.iterrows():
                        if np.isnan(row['gas_gravity']): break
                        dataIdxes.append(index)
                    lnx = np.log(
                        np.array(df_separator1['pressure'][dataIdxes] / df_separator1['temperature'][dataIdxes]))
                    lny = np.log(np.array(df_separator1['gas_gravity'][dataIdxes]))
                    mata = np.array([np.ones(len(lnx)), lnx]).T
                    matA = mata.T.dot(mata)
                    matB = np.linalg.inv(matA).dot(mata.T.dot(lny))
                    for index, row in df_separator1.iterrows():
                        if np.isnan(row['gas_gravity']): df_separator1.at[index, 'gas_gravity'] = math.exp(
                            matB[0] + matB[1] * math.log(
                                df_separator1['pressure'][index] / df_separator1['temperature'][index]))
            gas_sp_gravity_calc = (df_separator1['gor'] * df_separator1['gas_gravity']).sum() / df_separator1[
                'gor'].sum()
            gas_sp_gravity_calc = self.averageGasGravity if ((gas_sp_gravity_calc * 0.99) <= self.averageGasGravity <= (
                    gas_sp_gravity_calc * 1.01)) else gas_sp_gravity_calc
            api_gravity_calc = self.stockTankOilAPI if (((141.5 / ((df_separator1['liq_density'].iloc[
                -1]) * 0.9991026) - 131.5) * 0.99) <= self.stockTankOilAPI and self.stockTankOilAPI <= ((141.5 / (
                    (df_separator1['liq_density'].iloc[-1]) * 0.9991026) - 131.5) * 1.01)) else (
                    141.5 / ((df_separator1['liq_density'].iloc[-1]) * 0.9991026) - 131.5)
        except Exception as e:
            print "Error in calculation of gas_sp_gravity_calc, api_gravity_calc in SolutionGOR::input_solver , Error:%s" % (
                e.message)
        return gas_sp_gravity_calc, api_gravity_calc

    def define_region(self, vars, bound, mk_vars):
        lb, ub = list(), list()
        for i in range(len(vars)):
            lb.append(max(bound[i][0], (vars[i] - 4. * vars[i])))
            ub.append(min(bound[i][1], (vars[i] + 4. * vars[i])))
        a_max_l = list()
        for i in range(len(vars)):
            a_max_l.append(0 if mk_vars[i] == 0 else (
                ((vars[i] - ub[i]) / mk_vars[i]) if mk_vars[i] < 0 else ((vars[i] - lb[i]) / mk_vars[i])))
        alpha_max = None
        if 1.5 * min(a_max_l) < 10. ** -5:
            alpha_max = 10. ** -5
        elif 1.5 * min(a_max_l) > 50.:
            alpha_max = 50.
        else:
            alpha_max = 1.5 * min(a_max_l)

        alpha_min = None
        if ((alpha_max * 0.0000001) < 10. ** -13):
            alpha_min = 10. ** -13,
        elif ((alpha_max * 0.0000001) > 10. ** -8):
            alpha_min = 10 ** -8
        else:
            alpha_min = alpha_max * 0.0000001
        return alpha_max, alpha_min

    def converge_f(self, debug, logger, curr_iteration, grad):
        combined_grad = math.sqrt(
            grad[0] ** 2 + grad[1] ** 2 + grad[2] ** 2 + grad[3] ** 2 + grad[4] ** 2 + grad[5] ** 2 + grad[6] ** 2 +
            grad[7] ** 2 + grad[8] ** 2 + grad[9] ** 2 + grad[10] ** 2 + grad[11] ** 2 + grad[12] ** 2)
        # print "curr_iteration:", curr_iteration, "combined_grad:", combined_grad, "Done."
        if combined_grad <= 10 ** -3:
            if debug: self.logger.info("Rs converged by rule 1 combined gradient- %s" % (combined_grad))
            return True
        return False

    def objective(self, _svars, _iniVars, pressure, pr, rs, objFunMul):
        # objective
        A0, A1, A2, A3 = _svars[0] * _iniVars[0], _svars[1] * _iniVars[1], _svars[2] * _iniVars[2], _svars[3] * \
                         _iniVars[3]
        A4, A5, A6, A7 = _svars[4] * _iniVars[4], _svars[5] * _iniVars[5], _svars[6] * _iniVars[6], _svars[7] * \
                         _iniVars[7]
        A8, A9, A10, A11 = _svars[8] * _iniVars[8], _svars[9] * _iniVars[9], _svars[10] * _iniVars[10], _svars[11] * \
                           _iniVars[11]
        A12 = (10. ** -3. * _iniVars[12]) if _svars[12] == 0 else (copysign(1., _svars[12]) * 10. ** -3. if abs(
            _svars[12]) <= 10. ** -3. else _svars[12]) * _iniVars[12]
        # A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12=0.000594214,0.800195371,0.351155927,1.648604444,1.383684448,-2.415446295,3.945618857,-0.290979816,-0.083018364,-0.29536467,-0.281598998,0.563825399,0.010010279
        mulFactor = pr ** A12
        rsFactor1 = A0 * self.bubblePointPressure ** A1 * self.averageGasGravity ** A2 * self.reservoirTemp ** A3 * self.stockTankOilAPI ** A4 * self.rsb ** A5 * pressure ** (
                A6 * self.bubblePointPressure ** A7 * self.averageGasGravity ** A8 * self.reservoirTemp ** A9 * self.stockTankOilAPI ** A10 * self.rsb ** A11)
        rs_calc = rsFactor1 * mulFactor
        rslen = len(rs)
        objFun = [objFunMul[i] * (10. ** 50 if rs_calc[i] > 10. ** 50 else ((rs[i] - rs_calc[i]) / rs[i]) ** 2) for i in
                  range(rslen)]
        obj = sum(objFun)
        return obj, rs_calc

    def normDiffVars(self, d):
        r = None
        if (d >= 10. ** 100):
            r = 10. ** 10
        elif (d >= 10. ** 90):
            r = (d * 10. ** -85)
        elif (d >= 10. ** 85):
            r = (d * 10. ** -80)
        elif (d >= 10. ** 80):
            r = (d * 10. ** -75)
        elif (d >= 10. ** 75):
            r = (d * 10. ** -70)
        elif (d >= 10. ** 70):
            r = (d * 10. ** -65)
        elif (d >= 10. ** 65):
            r = (d * 10. ** -60)
        elif (d >= 10. ** 60):
            r = (d * 10. ** -55)
        elif (d >= 10. ** 55):
            r = (d * 10. ** -50)
        elif (d >= 10. ** 50):
            r = (d * 10. ** -45)
        elif (d >= 10. ** 45):
            r = (d * 10. ** -40)
        elif (d >= 10. ** 40):
            r = (d * 10. ** -35)
        elif (d >= 10. ** 35):
            r = (d * 10. ** -30)
        elif (d >= 10. ** 30):
            r = (d * 10. ** -25)
        elif (d >= 10. ** 25):
            r = (d * 10. ** -20)
        elif (d >= 10. ** 20):
            r = (d * 10. ** -12)
        elif (d >= 10. ** 15):
            r = (d * 10. ** -5)
        elif (d >= 10. ** 10):
            r = (d * 10. ** -4)
        else:
            r = d
        return r

    def _cost(self, vars, kwargs, print_r=False):
        _svars, _iniVars, pressure, pr, rs, objFunMul = vars, kwargs[0], kwargs[1], kwargs[2], kwargs[3], kwargs[4]
        # objective
        A0, A1, A2, A3 = _svars[0] * _iniVars[0], _svars[1] * _iniVars[1], _svars[2] * _iniVars[2], _svars[3] * \
                         _iniVars[3]
        A4, A5, A6, A7 = _svars[4] * _iniVars[4], _svars[5] * _iniVars[5], _svars[6] * _iniVars[6], _svars[7] * \
                         _iniVars[7]
        A8, A9, A10, A11 = _svars[8] * _iniVars[8], _svars[9] * _iniVars[9], _svars[10] * _iniVars[10], _svars[11] * \
                           _iniVars[11]
        A12 = (10. ** -3. * _iniVars[12]) if _svars[12] == 0 else (copysign(1., _svars[12]) * 10. ** -3. if abs(
            _svars[12]) <= 10. ** -3. else _svars[12]) * _iniVars[12]
        mulFactor = pr ** A12
        rsFactor1 = -1000. if (
                self.bubblePointPressure ** A1 > 10. ** 25 or self.averageGasGravity ** A2 > 10. ** 25 or self.reservoirTemp ** A3 > 10. ** 25 or self.rsb ** A5 > 10. ** 25 or self.stockTankOilAPI ** A4 > 10. ** 25 or (
                A6 * self.bubblePointPressure ** A7 * self.averageGasGravity ** A8 * self.reservoirTemp ** A9 * self.stockTankOilAPI ** A10 * self.rsb ** A11) > 50.) else (
                A0 * self.bubblePointPressure ** A1 * self.averageGasGravity ** A2 * self.reservoirTemp ** A3 * self.stockTankOilAPI ** A4 * self.rsb ** A5 * pressure ** (
                A6 * self.bubblePointPressure ** A7 * self.averageGasGravity ** A8 * self.reservoirTemp ** A9 * self.stockTankOilAPI ** A10 * self.rsb ** A11))
        rs_calc = rsFactor1 * mulFactor
        rslen = len(rs_calc)
        objFun = [objFunMul[i] * (10. ** 50 if rs_calc[i] > 10. ** 50 else ((rs[i] - rs_calc[i]) / rs[i]) ** 2) for i in
                  range(rslen)]
        obj = sum(objFun)
        # print "Total Obj:", obj
        # objective
        tmp3 = self.bubblePointPressure ** A1 > 10. ** 25 or self.averageGasGravity ** A2 > 10. ** 25 or self.reservoirTemp ** A3 > 10. ** 25 or self.rsb ** A5 > 10. ** 25 or self.stockTankOilAPI ** A4 > 10. ** 25 or (
                A6 * self.bubblePointPressure ** A7 * self.averageGasGravity ** A8 * self.reservoirTemp ** A9 * self.stockTankOilAPI ** A10 * self.rsb ** A11) > 50.
        Diff_rs_factor_var1 = -10000. if (tmp3) else (_iniVars[
                                                          0] * self.bubblePointPressure ** A1 * self.averageGasGravity ** A2 * self.reservoirTemp ** A3 * self.stockTankOilAPI ** A4 * self.rsb ** A5 * pressure ** (
                                                              A6 * self.bubblePointPressure ** A7 * self.averageGasGravity ** A8 * self.reservoirTemp ** A9 * self.stockTankOilAPI ** A10 * self.rsb ** A11))

        tmp = 0. if (tmp3) else (
                A0 * self.bubblePointPressure ** A1 * self.averageGasGravity ** A2 * self.reservoirTemp ** A3 * self.stockTankOilAPI ** A4 * self.rsb ** A5 * pressure ** (
                A6 * self.bubblePointPressure ** A7 * self.averageGasGravity ** A8 * self.reservoirTemp ** A9 * self.stockTankOilAPI ** A10 * self.rsb ** A11))
        Diff_rs_factor_var2 = -10000. if (tmp3) else tmp * (_iniVars[1] * log(self.bubblePointPressure))
        Diff_rs_factor_var3 = -10000. if (tmp3) else tmp * (_iniVars[2] * log(self.averageGasGravity))
        Diff_rs_factor_var4 = -10000. if (tmp3) else tmp * (_iniVars[3] * log(self.reservoirTemp))
        Diff_rs_factor_var5 = -10000. if (tmp3) else tmp * (_iniVars[4] * log(self.stockTankOilAPI))
        Diff_rs_factor_var6 = -10000. if (tmp3) else tmp * (_iniVars[5] * log(self.rsb))
        Diff_rs_factor_var7 = -10000. if (tmp3) else tmp * (np.log(pressure) * (
                self.bubblePointPressure ** A7 * self.averageGasGravity ** A8 * self.reservoirTemp ** A9 * self.stockTankOilAPI ** A10 * self.rsb ** A11) *
                                                            _iniVars[6])
        tmp1 = (
                A6 * self.bubblePointPressure ** A7 * self.averageGasGravity ** A8 * self.reservoirTemp ** A9 * self.stockTankOilAPI ** A10 * self.rsb ** A11)
        Diff_rs_factor_var8 = -10000. if (tmp3) else tmp * (
                np.log(pressure) * tmp1 * _iniVars[7] * log(self.bubblePointPressure))
        Diff_rs_factor_var9 = -10000. if (tmp3) else tmp * (
                np.log(pressure) * tmp1 * _iniVars[8] * log(self.averageGasGravity))
        Diff_rs_factor_var10 = -10000. if (tmp3) else tmp * (
                np.log(pressure) * tmp1 * _iniVars[9] * log(self.reservoirTemp))
        Diff_rs_factor_var11 = -10000. if (tmp3) else tmp * (
                np.log(pressure) * tmp1 * _iniVars[10] * log(self.stockTankOilAPI))
        Diff_rs_factor_var12 = -10000. if (tmp3) else tmp * (np.log(pressure) * tmp1 * _iniVars[11] * log(self.rsb))
        Diff_rs_factor_var13 = np.zeros(rslen)
        Diff_mul_factor_var1, Diff_mul_factor_var2, Diff_mul_factor_var3, Diff_mul_factor_var4, Diff_mul_factor_var5, Diff_mul_factor_var6 = np.zeros(
            rslen), np.zeros(rslen), np.zeros(rslen), np.zeros(rslen), np.zeros(rslen), np.zeros(rslen)
        Diff_mul_factor_var7, Diff_mul_factor_var8, Diff_mul_factor_var9, Diff_mul_factor_var10, Diff_mul_factor_var11, Diff_mul_factor_var12 = np.zeros(
            rslen), np.zeros(rslen), np.zeros(rslen), np.zeros(rslen), np.zeros(rslen), np.zeros(rslen)
        Diff_mul_factor_var13 = (pr ** A12) * np.log(pr) * A12
        Diff_Rs_var1 = mulFactor * Diff_rs_factor_var1 + rsFactor1 * Diff_mul_factor_var1
        Diff_Rs_var2 = mulFactor * Diff_rs_factor_var2 + rsFactor1 * Diff_mul_factor_var2
        Diff_Rs_var3 = mulFactor * Diff_rs_factor_var3 + rsFactor1 * Diff_mul_factor_var3
        Diff_Rs_var4 = mulFactor * Diff_rs_factor_var4 + rsFactor1 * Diff_mul_factor_var4
        Diff_Rs_var5 = mulFactor * Diff_rs_factor_var5 + rsFactor1 * Diff_mul_factor_var5
        Diff_Rs_var6 = mulFactor * Diff_rs_factor_var6 + rsFactor1 * Diff_mul_factor_var6
        Diff_Rs_var7 = mulFactor * Diff_rs_factor_var7 + rsFactor1 * Diff_mul_factor_var7
        Diff_Rs_var8 = mulFactor * Diff_rs_factor_var8 + rsFactor1 * Diff_mul_factor_var8
        Diff_Rs_var9 = mulFactor * Diff_rs_factor_var9 + rsFactor1 * Diff_mul_factor_var9
        Diff_Rs_var10 = mulFactor * Diff_rs_factor_var10 + rsFactor1 * Diff_mul_factor_var10
        Diff_Rs_var11 = mulFactor * Diff_rs_factor_var11 + rsFactor1 * Diff_mul_factor_var11
        Diff_Rs_var12 = mulFactor * Diff_rs_factor_var12 + rsFactor1 * Diff_mul_factor_var12
        Diff_Rs_var13 = mulFactor * Diff_rs_factor_var13 + rsFactor1 * Diff_mul_factor_var13
        tmp = (2. * (np.array(rs) - rs_calc) / np.array(rs) ** 2)
        diff_total_obj_var1, diff_total_obj_var2, diff_total_obj_var3 = self.normDiffVars(
            np.sum(tmp * (-1) * Diff_Rs_var1 * objFunMul)), self.normDiffVars(
            np.sum(tmp * (-1) * Diff_Rs_var2 * objFunMul)), self.normDiffVars(
            np.sum(tmp * (-1) * Diff_Rs_var3 * objFunMul))
        diff_total_obj_var4, diff_total_obj_var5, diff_total_obj_var6 = self.normDiffVars(
            np.sum(tmp * (-1) * Diff_Rs_var4 * objFunMul)), self.normDiffVars(
            np.sum(tmp * (-1) * Diff_Rs_var5 * objFunMul)), self.normDiffVars(
            np.sum(tmp * (-1) * Diff_Rs_var6 * objFunMul))
        diff_total_obj_var7, diff_total_obj_var8, diff_total_obj_var9 = self.normDiffVars(
            np.sum(tmp * (-1) * Diff_Rs_var7 * objFunMul)), self.normDiffVars(
            np.sum(tmp * (-1) * Diff_Rs_var8 * objFunMul)), self.normDiffVars(
            np.sum(tmp * (-1) * Diff_Rs_var9 * objFunMul))
        diff_total_obj_var10, diff_total_obj_var11, diff_total_obj_var12 = self.normDiffVars(
            np.sum(tmp * (-1) * Diff_Rs_var10 * objFunMul)), self.normDiffVars(
            np.sum(tmp * (-1) * Diff_Rs_var11 * objFunMul)), self.normDiffVars(
            np.sum(tmp * (-1) * Diff_Rs_var12 * objFunMul))
        diff_total_obj_var13 = self.normDiffVars(np.sum(tmp * (-1) * Diff_Rs_var13 * objFunMul))
        return obj, np.array(
            [diff_total_obj_var1, diff_total_obj_var2, diff_total_obj_var3, diff_total_obj_var4, diff_total_obj_var5,
             diff_total_obj_var6, diff_total_obj_var7, diff_total_obj_var8, diff_total_obj_var9, diff_total_obj_var10,
             diff_total_obj_var11, diff_total_obj_var12, diff_total_obj_var13])

    def solve(self):
        self.vars = np.ones(13)  # 13 variables.
        self.bounds = [[-3., 3.] for i in range(12)]  # 13 vars ini values from excel/gui
        self.bounds.append([-10., 5000.])
        self.bounds = np.array(self.bounds)
        self.iniVars = np.array(
            [0.0006, 0.856, 0.351, 1.829, 1.462, -2.116, 3.867, -0.306, -0.083, -0.306, -0.288, 0.525, 0.01])
        pressure_obsv = np.arange(self.pressure[0], self.pressure[-1], -10. if self.pressure[0] <= 150 else -20)
        pressure_obsv = np.append(pressure_obsv, [self.pressure[-1]])
        rs_obsv = monotone(self.pressure.tolist(), self.rs.tolist(), pressure_obsv.tolist())
        if 0 in rs_obsv:
            zero_index = rs_obsv.index(0)
        else:
            zero_index = len(rs_obsv) + 1
        pr = pressure_obsv / pressure_obsv[0]
        objFunMul = [0.01 if (pr[i] <= 0.05) else 1. if (pr[i] <= 0.1) else 100. if (pr[i] > 0.2) else 50. for i in
                     range(len(pr))]
        var_calc = ncg().minimize(self.vars, self.bounds, self._cost, self.logger, max_iter=300, debug=self.debug,
                                  region_f=self.define_region, converge_f=self.converge_f, args=(
                self.iniVars, pressure_obsv[:zero_index], pr[:zero_index], rs_obsv[:zero_index],
                objFunMul[:zero_index]))
        # obj, rs_calc = self.objective(self.iniVars, var_calc, pressure_obsv[:zero_index], pr[:zero_index], rs_obsv[:zero_index], objFunMul[:zero_index])
        obj, rs_calc = self.objective(self.iniVars, var_calc, pressure_obsv, pr, rs_obsv[:zero_index], objFunMul)
        rs_multiplier = rs_obsv / rs_calc
        rs_final = rs_calc * rs_multiplier
        self.compVars = self.iniVars * var_calc

    def result_solver(self):
        self.tempDegFRange = 5.
        self.tempDegF = np.arange(10., 1205., self.tempDegFRange)
        rsb = np.repeat(self.rsb, len(self.tempDegF))
        k1 = self.compVars[0] * self.bubblePointPressure ** self.compVars[1] * self.averageGasGravity ** self.compVars[
            2] * self.tempDegF ** self.compVars[3] * self.stockTankOilAPI ** self.compVars[4] * self.rsb ** \
             self.compVars[5]
        k2 = self.compVars[6] * self.bubblePointPressure ** self.compVars[7] * self.averageGasGravity ** self.compVars[
            8] * self.tempDegF ** self.compVars[9] * self.stockTankOilAPI ** self.compVars[10] * self.rsb ** \
             self.compVars[11]
        pb = np.exp((np.log(rsb) - np.log(k1)) / k2)
        pbMiltiplier = self.bubblePointPressure / \
                       monotone(self.tempDegF.tolist(), pb.tolist(), [self.reservoirTemp])[0]
        self.finalBPpressure = pb * pbMiltiplier

    # rateOfChangePb= (self.finalBPpressure[1:]-self.finalBPpressure[0:-1])/(self.tempDegF[1:]-self.tempDegF[0:-1])
    # self.markerIdx= len(rateOfChangePb)
    # for i in range(len(rateOfChangePb)):
    #     if rateOfChangePb[i] <= 0.085:
    #         self.markerIdx= i
    #         break
    # self.finalMaxPointTemp= self.reservoirTemp if(self.tempDegF[self.markerIdx] <= self.reservoirTemp) else self.tempDegF[self.markerIdx]
    # self.finalMaxPointPressure= self.bubblePointPressure if(self.tempDegF[self.markerIdx] <= self.reservoirTemp) else self.finalBPpressure[self.markerIdx]

    def pIdx(self, n):
        return 0 if (self.markerIdx - n) < 0 else (self.markerIdx - n)

    def nIdx(self, n):
        return len(self.tempDegF) if ((self.markerIdx + n) > len(self.tempDegF)) else self.markerIdx + n

    def setMinMaxIdx(self):
        self.minIdx = 10  # -50  degree
        self.maxIdx = 20  # +100 degree

    def presssureAtMaxTemp(self):
        tempDegFL50 = self.tempDegF[self.pIdx(self.minIdx):self.markerIdx + 1]
        pressureL50 = self.finalBPpressure[self.pIdx(self.minIdx):self.markerIdx + 1]
        mata = np.array([np.ones(len(tempDegFL50)), tempDegFL50, tempDegFL50 ** 2]).T
        matA = mata.T.dot(mata)
        matB = np.linalg.inv(matA).dot(mata.T.dot(pressureL50))

        temp = self.tempDegF[self.pIdx(self.minIdx):self.nIdx(self.maxIdx) + 1]
        pressure1 = np.array([matB[0] + matB[1] * temp[i] + matB[2] * temp[i] ** 2 for i in
                              range(len(temp))])  # matB will have 3 elements always?
        t1 = self.tempDegF[self.markerIdx]
        t2 = self.tempDegF[self.nIdx(self.maxIdx)]
        p1 = pressure1[temp.tolist().index(t1)]
        p2 = pressure1[temp.tolist().index(t2)]
        p2 = p2 if (p2 < (p1 / 1.25)) else (p1 / 1.25)
        b2 = (t2 ** 2 * p1 ** 2 - t1 ** 2 * p2 ** 2) / (t2 ** 2 - t1 ** 2)
        a2 = t1 ** 2 / (1. - p1 ** 2 / b2)
        temp2, factorA, pressure2, diffPresswrtT = [], [], [], []
        t = t2
        offset = None
        while 1:
            fA = (1. - t ** 2 / a2) * b2
            print fA
            if fA < 0: break
            p = np.sqrt(fA)
            pressure2.append(p)
            if offset is None: offset = p
            diffPresswrtT.append((b2 / a2) * (-1. * t / p))
            temp2.append(t)
            t += 5.
        self.offset = offset
        self.temp2 = temp2
        self.pressure2 = pressure2

        self.diff_at_90 = monotone(temp2, diffPresswrtT, [-90.])[0]
        self.pressureMaxTemp = 0. if (((1. - self.diff_at_90 ** 2 / a2) * b2) < 0) else sqrt(
            (1. - self.diff_at_90 ** 2 / a2) * b2)

        self.finalQuadraticSetT = np.array([temp[0], t1, t2])
        self.finalQuadraticSetP = np.array([pressure1[0], p1, p2])

    def finalSmoothedPressure(self):
        mata = np.array(
            [np.ones(len(self.finalQuadraticSetT)), self.finalQuadraticSetT, self.finalQuadraticSetT ** 2]).T
        matA = mata.T.dot(mata)
        matB = np.linalg.inv(matA).dot(mata.T.dot(self.finalQuadraticSetP))

        temp = self.tempDegF[self.pIdx(self.minIdx):self.nIdx(self.maxIdx) + 1]
        pressure = np.array([matB[0] + matB[1] * temp[i] + matB[2] * temp[i] ** 2 for i in
                             range(len(temp))])  # matB will have 3 elements always?
        pressureOffset = pressure[-1] - self.offset

        temp1 = np.append(temp, self.temp2[1:])
        temp1 = np.append(temp1, [temp1[-1] + self.tempDegFRange])
        pressure_quad = np.append(pressure, np.zeros(len(self.pressure2) - 1))
        pressure_quad = np.append(pressure_quad, [0.])
        pressure_ellip = np.append(np.zeros(len(pressure)), self.pressure2[1:])
        pressure_ellip = np.append(pressure_ellip, [0.])

        temp2 = [temp1[0]]
        for i in range(1, len(temp1)): temp2.append(
            (temp2[-1] + 5.) if ((temp2[-1] + 5.) <= self.diff_at_90) else self.diff_at_90)
        pressure_corr = [pressure_quad[i] if (temp1[i] <= self.temp2[0]) else self.pressureMaxTemp if (
                temp2[i] == self.diff_at_90) else (pressure_ellip[i] + pressureOffset) for i in range(len(temp1))]

        temp_final = np.append(self.tempDegF[0:self.markerIdx + 1],
                               temp2[temp.tolist().index(self.tempDegF[self.markerIdx]) + 1:])
        pressure_final = np.append(self.finalBPpressure[0:self.markerIdx + 1],
                                   pressure_corr[temp.tolist().index(self.tempDegF[self.markerIdx]) + 1:])
        degree50idx = 0
        for i in range(len(temp_final)):
            if temp_final[i] >= 50.:
                degree50idx = i
                break
        temp4 = temp_final[degree50idx:]
        pressure4 = pressure_final[degree50idx:]
        mata = np.array([np.ones(len(temp4)), temp4, temp4 ** 2, temp4 ** 3, temp4 ** 4, temp4 ** 5]).T
        matA = mata.T.dot(mata)
        matB = np.linalg.inv(matA).dot(mata.T.dot(pressure4))
        pressureSmooth = []
        # temp4= np.append(temp4, [0.])
        # temp4= np.append(temp4, [0.])
        for i in np.append(self.tempDegF[0:self.markerIdx + 1], temp2[temp.tolist().index(
                self.tempDegF[self.markerIdx]) + 1:]):  # temp_final with first 50 degree
            ps = matB[0]
            for j in range(1, len(matB)): ps += matB[j] * i ** j
            pressureSmooth.append(ps)

        offset = pressure_final[self.markerIdx] - pressureSmooth[self.markerIdx]
        pressure_final_smooth_1 = [0. if ((p + offset) < 0.) else (p + offset) for p in pressureSmooth]
        temp_final_smooth = temp_final
        pressure_final_smooth = np.append(pressure_final[0:self.markerIdx + 1],
                                          pressure_final_smooth_1[self.markerIdx + 1:])
        print np.array([temp_final_smooth, pressure_final_smooth])
        return temp_final_smooth, pressure_final_smooth

    def fTOs(self, f):
        s = ('%s' % (float(f))).replace('.', '_')
        return s

    def getMatB(self, seOfOnes):
        m1 = self.finalBPpressure[seOfOnes] / self.bppInterpolated_db[seOfOnes]
        t1 = np.array([np.ones(len(m1)), self.temp2[seOfOnes]]).T
        matA = t1.T.dot(t1)
        matB = np.linalg.inv(matA).dot(t1.T.dot(m1))
        return m1, matB

    def getBPPusingMiltipler(self):
        s, e = None, None
        self.error_perc_marker = np.array([0 if i >= 0.8 else 1 for i in self.error_perc])
        ones = np.where(self.error_perc_marker == 1)[0]
        mat1B, mat2B, multiplier = None, None, None
        finalBPpressure = None
        if (len(ones) == 0):
            # multiplier= np.ones(len(temp2))
            finalBPpressure = self.bppInterpolated_db
        else:
            seOfOnes = np.split(ones, np.where(np.diff(ones) != 1)[0] + 1)[0]  # array of index of consecutiive 1
            if len(seOfOnes) < 2:
                finalBPpressure = self.bppInterpolated_db
            elif (len(seOfOnes) >= 2 and len(seOfOnes) <= 6):
                m1, mat1B = self.getMatB(seOfOnes[0:5])
                multiplier = np.array(
                    [0.8 if ((mat1B[0] + mat1B[1] * i) < 0.8) else (mat1B[0] + mat1B[1] * i) for i in self.temp2])
                multiplier[seOfOnes[0]] = m1[0]
                multiplier[seOfOnes[1]:] = 1.
                finalBPpressure = self.bppInterpolated_db * multiplier
                finalBPpressure[seOfOnes[1:-1]] = self.finalBPpressure[seOfOnes[1:-1]]
            else:
                m1, mat1B = self.getMatB(seOfOnes[0:5])
                multiplier = np.array(
                    [0.8 if ((mat1B[0] + mat1B[1] * i) < 0.8) else (mat1B[0] + mat1B[1] * i) for i in self.temp2])
                multiplier[seOfOnes[0]] = m1[0]
                multiplier[seOfOnes[1:-1]] = 1.
                m2, mat2B = self.getMatB(seOfOnes[5:][-5:])
                multiplier[seOfOnes[-1]] = m2[-1]
                if len(self.temp2) > (seOfOnes[-1] + 1):
                    multiplier[seOfOnes[-1] + 1:] = np.array(
                        [0.8 if ((mat2B[0] + mat2B[1] * i) < 0.8) else (mat2B[0] + mat2B[1] * i) for i in
                         self.temp2[seOfOnes[-1] + 1:]])

                finalBPpressure = self.bppInterpolated_db * multiplier
                finalBPpressure[seOfOnes[1:-1]] = self.finalBPpressure[seOfOnes[1:-1]]
        return finalBPpressure

    def finalBubblePointPressure(self):
        bppAtRsb = self.get_gor_data()[1:]
        temp1 = np.arange(10., 10. * (len(bppAtRsb) + 1), 10.)
        bbpAtRefTemp = monotone(temp1.tolist(), bppAtRsb.tolist(), [self.reservoirTemp])[0]
        calibration_Mul = self.bubblePointPressure / bbpAtRefTemp
        bpp = bppAtRsb * calibration_Mul
        self.temp2 = np.arange(10., 5. * 101, 5.)  # 10 to 500 dg f
        self.bppInterpolated_db = np.array(
            monotone(temp1.tolist(), bpp.tolist(), self.temp2.tolist()))
        self.finalBPpressure = self.finalBPpressure[0:len(self.bppInterpolated_db)]
        self.error_perc = np.abs((self.bppInterpolated_db - self.finalBPpressure) / self.bppInterpolated_db) * 100.
        self.bppCalculated = self.getBPPusingMiltipler()
        return self.temp2, self.bppInterpolated_db, self.bppCalculated

    def multipleBubblePoints(self, temp, bppCalculated):
        self.logger.info("Found multipleBubblePoints")
        temp_res = {'temperature': self.reservoirTemp, 'bubble_point': self.bubblePointPressure}
        df_mbp_sorted = self.df_multiple_bubble_points.append(temp_res, ignore_index=True)
        df_mbp_sorted.sort_values('temperature', inplace=True)
        df_mbp_sorted = df_mbp_sorted.reset_index(drop=True)
        df_mbp_sorted['calculated_pb'] = monotone(temp.tolist(), bppCalculated.tolist(),
                                                                      df_mbp_sorted['temperature'].tolist())
        df_mbp_sorted['multipler'] = df_mbp_sorted['bubble_point'] / df_mbp_sorted['calculated_pb']
        df_mbp_sorted['multipler_ln'] = np.log(df_mbp_sorted['multipler'])
        mata = np.array([np.ones(len(df_mbp_sorted['temperature'])), df_mbp_sorted['temperature']]).T
        matA = mata.T.dot(mata)
        matB = np.linalg.inv(matA).dot(mata.T.dot(df_mbp_sorted['multipler_ln']))
        temp_Multipler = [0.75 if (math.exp(matB[0] + matB[1] * i) < 0.75) else math.exp(matB[0] + matB[1] * i) for i in
                          temp]
        obs_multiplier = df_mbp_sorted['multipler'][
            df_mbp_sorted.index[df_mbp_sorted['temperature'] == self.reservoirTemp].tolist()[0]]
        ratio_correction = monotone(temp.tolist(), temp_Multipler, [self.reservoirTemp])[
                               0] / obs_multiplier
        final_bubble_point_corrected = temp_Multipler * bppCalculated * ratio_correction
        return final_bubble_point_corrected

    def readDf(self, df_general_data, df_separator, df_separator_corrected, df_multiple_bubble_points):
        self.df_general_data = df_general_data
        self.df_separator = df_separator
        self.df_separator_corrected = df_separator_corrected
        self.df_multiple_bubble_points = df_multiple_bubble_points

        if not self.df_general_data.empty:
            self.df_general_data["value"]["temp_res"] = (self.df_general_data["value"][
                                                             "temp_res"] - 273.15) * 9. / 5. + 32.
            self.df_general_data["value"]["pb_res_temp"] = (self.df_general_data["value"]["pb_res_temp"] * 145.038)
            self.df_general_data["value"]["gor_pb"] = (self.df_general_data["value"]["gor_pb"] / 0.178105620841002)

        if not self.df_multiple_bubble_points.empty:
            self.df_multiple_bubble_points["temperature"] = (self.df_multiple_bubble_points[
                                                                 "temperature"] - 273.15) * 9. / 5. + 32.
            self.df_multiple_bubble_points["bubble_point"] = self.df_multiple_bubble_points["bubble_point"] * 145.038

        if not self.df_separator.empty:
            self.df_separator["temperature"] = (self.df_separator["temperature"] - 273.15) * 9. / 5. + 32.
            self.df_separator["pressure"] = self.df_separator["pressure"] * 145.038
            self.df_separator["gor"] = self.df_separator["gor"] / 0.178105620841002

        if not self.df_separator_corrected.empty:
            self.df_separator_corrected["pressure"] = self.df_separator_corrected["pressure"] * 145.038
            self.df_separator_corrected["gor"] = self.df_separator_corrected["gor"] / 0.178105620841002

    def solve_df(self, df_general_data, df_separator, df_separator_corrected, df_multiple_bubble_points, fluid_code,
                 logger=None, debug=False):
        self.debug = debug
        self.logger = logger
        self.fluid_code = fluid_code
        self.readDf(df_general_data, df_separator, df_separator_corrected,
                    df_multiple_bubble_points)  # df inputs from PVT
        self.input_data()
        self.solve()
        # self.calculate()
        self.result_solver()
        temp, bppDb, bppCalculated = self.finalBubblePointPressure()
        # bppCalculated=
        if not self.df_multiple_bubble_points.empty:
            bppCalculated = self.multipleBubblePoints(temp, bppCalculated)

        return (temp - 32.) * 5. / 9. + 273.15, bppCalculated / 145.038

    def case2_3(self, df_general_data, df_separator, df_separator_corrected, df_multiple_bubble_points, fluid_code,
                logger=None, debug=False):
        self.debug = debug
        self.logger = logger
        self.fluid_code = fluid_code
        self.readDf(df_general_data, df_separator, df_separator_corrected,
                    df_multiple_bubble_points)  # df inputs from PVT
        self.input_data()
        if math.isnan(self.rsb):
            self.rsb = self.calc_gor()

        bppAtRsb = self.get_gor_data()

        temp1 = np.arange(0, 10. * (len(bppAtRsb)), 10.)
        bbpAtRefTemp = monotone(temp1.tolist(), bppAtRsb.tolist(), [self.reservoirTemp])[0]
        calibration_Mul = self.bubblePointPressure / bbpAtRefTemp

        bpp = bppAtRsb * calibration_Mul
        temp = np.arange(10., 5. * 101, 5.)  # 10 to 500 dg f
        bppCalculated = np.array(monotone(temp1.tolist(), bpp.tolist(), temp.tolist()))
        if not self.df_multiple_bubble_points.empty:
            bppCalculated = self.multipleBubblePoints(temp, bppCalculated)
        return (temp - 32.) * 5. / 9. + 273.15, bppCalculated / 145.038

    def calc_gor(self):
        if self.stockTankOilAPI <= 30:
            return (0.0362 * self.averageGasGravity * (self.bubblePointPressure ** 1.0937) * np.exp(
                25.724 * (self.stockTankOilAPI / (self.reservoirTemp + 459.67))))
        else:
            return (0.0178 * self.averageGasGravity * (self.bubblePointPressure ** 1.187) * np.exp(
                23.931 * (self.stockTankOilAPI / (self.reservoirTemp + 459.67))))

    def get_gor_data(self):
        # bubble point pressure at rsb or rs[0] from Mongo
        fluid_codes = {}
        for data in self.cenDbPBO.find({'Property': 'SolutionGOR'}):
            fluid_codes[data['OilType']] = {'GOR': data['GOR']}
            for g in data['GOR']: fluid_codes[data['OilType']][float(g)] = data[self.fTOs(g)]
        if self.rsb in fluid_codes[self.fluid_code]['GOR']:
            return np.array(fluid_codes[self.fluid_code][self.rsb])
        else:
            mx = [i for i in fluid_codes[self.fluid_code]['GOR'] if i > self.rsb]
            if len(mx) == 0:
                mx = fluid_codes[self.fluid_code]['GOR'][-1]
                mn = fluid_codes[self.fluid_code]['GOR'][-2]
            else:
                mx = min(mx)
                mn = [i for i in fluid_codes[self.fluid_code]['GOR'] if i < self.rsb]
                if len(mn) == 0:
                    mn = 0
                else:
                    mn = max(mn)
            bppMx = np.array(fluid_codes[self.fluid_code][mx])
            if mn == 0:
                bppMn = np.zeros(len(bppMx))
            else:
                bppMn = np.array(fluid_codes[self.fluid_code][mn])
        return bppMn + (bppMx - bppMn) / (mx - mn) * (self.rsb - mn)
