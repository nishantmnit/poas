#!/usr/bin/env python
import os
import sys

from openpyxl import load_workbook
import numpy as np
import pandas as pd


uni_pressure = [300, 250, 200, 150, 125, 100, 90, 80, 70, 60, 50, 45, 40, 35, 30, 25, 20, 15, 10, 8, 6, 4, 3, 2, 1, 0.5,
                0.10132501]


def get_method(method_name):
    possibles = globals().copy()
    possibles.update(locals())
    method_name = method_name.replace(".", "_")
    method = possibles.get(method_name)
    if not method:
        raise NotImplementedError("Method %s not implemented" % method_name)
    return method


def read_data(component):
    idf = pd.read_excel("%s/kij_input/input_data.xlsx" % (cwd), component)
    formulaes = pd.read_excel("%s/kij_input/formulaes.xlsx" % (cwd), component)
    return idf, formulaes


def get_alpha_omb(df, formulaes, ref_temp, ref_pressure=None):
    alpha_eos = get_alpha_eos(df, formulaes, ref_temp, ref_pressure)
    omb = get_omb(df, formulaes, ref_temp, ref_pressure)
    return alpha_eos, omb


def expand_equation(equation, coeff, ref_temp):
    if equation != "constant": coeff = [float(x.encode('ascii', 'ignore')) for x in coeff.split(",")]
    if equation == "exponential":
        val = coeff[0] * np.exp(coeff[1] * ref_temp)
        if len(coeff) > 2:
            if val < coeff[2]: return coeff[2]
            return val
        return val
    elif equation == "cubic":
        return coeff[0] * ref_temp ** 3 + coeff[1] * ref_temp ** 2 + coeff[2] * ref_temp + coeff[3]
    elif equation == "binomial":
        return coeff[0] * ref_temp ** 2 + coeff[1] * ref_temp + coeff[2]
    elif equation == "power":
        val = coeff[0] * ref_temp ** coeff[1]
        if len(coeff) > 2:
            if val < coeff[2]: return coeff[2]
            return val
        return val
    elif equation == "log":
        return coeff[0] * np.log(ref_temp) + coeff[1]
    elif equation == "constant":
        return coeff
    elif equation == "nothing":
        raise NotImplementedError("Method not implemented")


def get_alpha_eos(df, formulaes_o, ref_temp, ref_pressure):
    what = "alpha_eos"
    alpha_eos = list()
    formulaes = formulaes_o.loc[formulaes_o.what == what].copy(deep=True)

    for index, row in formulaes.iterrows():
        if ref_temp < row.max_temperature or ref_temp > row.min_temperature:
            res = expand_equation(row.equation, row.coeff, ref_temp)
        else:
            res = interpolate(df, formulaes, ref_temp, row.pressure, "temperature", what)
        alpha_eos.append(res)
    x = formulaes["pressure"].tolist()
    y = alpha_eos
    return interpolate_pressure(x, y, ref_pressure)


def interpolate_pressure(x, y, ref_pressure):
    obj = poas_interpolator()
    data = list()
    for pressure in uni_pressure:
        ival = monotone(x, y, pressure)[0]
        if ival <= 0.95 * y[0] and pressure > x[0]:
            val = 0.98 * y[0]
        elif ival > 1.05 * y[0] and pressure > x[0]:
            val = 1.05 * y[0]
        else:
            val = ival
        data.append(val)
    if ref_pressure is None: return data
    return monotone(uni_pressure, data, ref_pressure)[0]


def get_omb(df, formulaes_o, ref_temp, ref_pressure):
    what = "omb"
    omb = list()
    formulaes = formulaes_o.loc[formulaes_o.what == what].copy(deep=True)
    for index, row in formulaes.iterrows():
        if ref_temp < row.max_temperature or ref_temp > row.min_temperature:
            res = expand_equation(row.equation, row.coeff, ref_temp)
        else:
            res = interpolate(df, formulaes, ref_temp, row.pressure, "temperature", what)
        omb.append(res)
    x = formulaes["pressure"].tolist()
    y = omb
    return interpolate_pressure(x, y, ref_pressure)


def interpolate(df_o, formulaes, ref_temp, pressure, x, y):
    df = df_o.loc[df_o.for_pressure == pressure, [x, y]].copy(deep=True)
    obj = poas_interpolator()
    x = df[x].tolist()
    y = df[y].tolist()
    return monotone(x, y, ref_temp)[0]


def for_pt(debug=False, ref_temp=302.03889):
    if debug:
        output_file = "Debug/pt_output.xlsx"
        if os.path.exists(output_file):
            os.remove(output_file)
        exists = False
    for component in ["CARBON DIOXIDE", "ETHANE", "HYDROGEN SULFIDE", "HYDROGEN", "ISOBUTANE", "METHANE", "N-BUTANE",
                      "NITROGEN", "PROPANE", "WATER"]:
        print "executing for component = %s" % component
        df, formulaes = read_data(component)
        alpha_df, omb_df = pd.DataFrame(), pd.DataFrame()
        alpha_df["pressure"] = uni_pressure
        omb_df["pressure"] = uni_pressure
        for temp in [180, 200, 220, 240, 260, 280, 298.15, 300, 320, 340, 360, 380, 400, 420, 440, 460, 480, 500, 520,
                     540, 560, 580, 600, 620, 640, 660, 680, 700, 720, 740, 760, 780, 800, 850, 900, 950, 1000,
                     ref_temp]:
            print "executing for temperature = %s" % temp
            alpha_eos, omb = get_alpha_omb(df, formulaes, temp)
            alpha_df["%s" % (temp)] = alpha_eos
            omb_df["%s" % (temp)] = omb
        if debug:
            if os.path.exists(output_file):
                book = load_workbook(output_file)
                exists = True
            with pd.ExcelWriter(output_file, engine="openpyxl") as writer:
                if exists: writer.book = book
                alpha_df.to_excel(writer, sheet_name="%s_alpha" % component, index=False)
                omb_df.to_excel(writer, sheet_name="%s_omb" % component, index=False)
    return alpha_df, omb_df

# usage

# use 1 for pt diagram
# for_pt(debug=True)

# use 2 for specific temp and pressure
# df, formulaes = read_data("N-BUTANE")
# alpha_eos, omb = get_alpha_omb(df, formulaes, 180, 0.10132501)
# print alpha_eos, omb
