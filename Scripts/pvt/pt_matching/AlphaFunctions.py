import numpy as np

gasConstant = 8.314472


class AlphaFunctions:
    def __init__(self):
        pass

    def pr(self, df):
        df["om_a"] = 0.457235528921382
        df["b"] = df.om_b * gasConstant * df.tc_k / (df.pc_mpa * 10 ** 6)
        df["pf"] = df.b * 10 ** 6 / df.vc_final
        df["ac"] = (df.om_a * gasConstant ** 2 * df.tc_k ** 2 / (df.pc_mpa * 10 ** 6))
        df["ac_alpha"] = (df.ac * df.alpha_eos)
        df["u"] = df.apply(lambda row: 0 if row.pf == 0 else (1. + 1. / row.pf * (1. / row.zc_final - 3.)), axis=1)
        df["w"] = df.apply(lambda row: 0 if ((row.u - 1) * row.pf ** 3 + 3 * row.pf ** 2) == 0 else (
                    (1 - 3 * row.pf - row.u ** 2 * row.pf ** 3 - 3 * row.u * row.pf ** 2) / (
                        (row.u - 1) * row.pf ** 3 + 3 * row.pf ** 2)), axis=1)
        df["epsilon"] = df.apply(
            lambda row: 0 if (row.u ** 2 - 4. * row.w) < 0 else (row.u + np.sqrt(row.u ** 2 - 4. * row.w)) / 2., axis=1)
        df["sigma"] = df.apply(
            lambda row: 0 if (row.u ** 2 - 4. * row.w) < 0 else (row.u - np.sqrt(row.u ** 2 - 4. * row.w)) / 2., axis=1)
        return df
