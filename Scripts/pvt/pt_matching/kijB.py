import numpy as np
import pandas as pd


class kijB:
    def __init__(self):
        pass

    def run(self, df, temp, eos):
        eij = self.get_eij(df, temp, eos)
        kij = self.get_kij(df, temp, eos, eij)
        return eij, kij

    def get_eij(self, df, temp, eos):
        df["delta"] = np.sqrt(df.ac_alpha * 10 ** 6) / (df.b * 10 ** 6)
        df["t1"] = (df.diff_alpha_wrt_temp * df.ac * 10 ** 6) / (df.b * 10 ** 6) ** 2
        df["t2"] = (df.diff_alpha_wrt_temp * df.ac * 10 ** 6) / (df.ac_alpha * 10 ** 6)
        eij_df = df[["scn", "component", "mf"]].copy(deep=True)
        for index, row in df.iterrows():
            eij_df[row.scn if pd.isna(row.component) else row.component] = ((df.cpar + row.cpar) / 2.) * (
                        (row.delta - df.delta) ** 2 - temp * ((row.t1 + df.t1) - (
                            np.sqrt(row.ac_alpha * 10 ** 6 * df.ac_alpha * 10 ** 6) / (
                                row.b * 10 ** 6 * df.b * 10 ** 6)) * (row.t2 + df.t2)))
            if pd.isna(row.component):
                eij_df.loc[eij_df.scn == row.scn, row.scn] = 0
            else:
                eij_df.loc[eij_df.component == row.component, row.component] = 0
        return eij_df

    def get_kij(self, df, temp, eos, eij):
        df["delta"] = np.sqrt(df.ac_alpha * 10 ** 6) / (df.b * 10 ** 6)
        kij_df = df.copy(deep=True)
        for index, row in df.iterrows():
            for_comp = row.scn if pd.isna(row.component) else row.component
            df["k1_others"] = 1. - (eij[for_comp] - row.delta ** 2 - df.delta ** 2) / (-2. * row.delta * df.delta)
            kij_df[
                for_comp] = df.k1_others  # (2. * df.multiplier * df.k1_others * row.aibybi * df.aibybi + df.multiplier * (row.aibybi - df.aibybi)**2 - (row.AiByBi_eq - df.AiByBi_eq)**2)/(2. * row.AiByBi_eq*df.AiByBi_eq)
            if pd.isna(row.component):
                kij_df.loc[kij_df.scn == row.scn, row.scn] = 0
            else:
                kij_df.loc[kij_df.component == row.component, row.component] = 0
            kij_df[for_comp] = kij_df.apply(lambda row: row[for_comp] if np.abs(row[for_comp]) <= 0.4 else (
                0.6 * row[for_comp] if (int(row[for_comp]) - (1 if np.sign(row[for_comp]) < 0 else 0)) == 0 else (
                            np.sign(row[for_comp]) * 0.5 * row[for_comp] / (
                                int(row[for_comp]) - (1 if np.sign(row[for_comp]) < 0 else 0)))), axis=1)
        return kij_df
