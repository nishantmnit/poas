from AlphaFunctions import *
from alpha_omb_121 import *
from kijA import *
from kijB import *
from kijC import *
from non_hydrocarbon_alpha_omb_sol import *

gasConstant = 8.314472


class kij:
    def __init__(self):
        pass

    def handle_last_alpha_omb(self, df):
        row = df.loc[df.index == df.index.max()].squeeze()
        row_1 = df.loc[df.index == df.index.max() - 1].squeeze()
        row_2 = df.loc[df.index == df.index.max() - 2].squeeze()

        alpha_eos = row.alpha_pr * row.alpha_multiplier
        if alpha_eos < (row_1.alpha_eos ** 2 / row_2.alpha_eos) * 0.9 or alpha_eos > (
                row_1.alpha_eos ** 2 / row_2.alpha_eos) * 1.1:
            alpha_eos = (row_1.alpha_eos ** 2 / row_2.alpha_eos) * 1.05

        om_b = row.om_b
        if om_b < (row_1.om_b ** 2 / row_2.om_b) * 0.9 or om_b > (row_1.om_b ** 2 / row_2.om_b) * 1.1:
            om_b = (row_1.om_b ** 2 / row_2.om_b) * 1.05
        return alpha_eos, om_b

    def calculateAiByBiEquivalent(self, df):
        df["r1"] = (-1. * df.u + np.sqrt(df.u ** 2 - 4. * df.w)) / 2.
        df["r2"] = (-1 * df.u - np.sqrt(df.u ** 2 - 4. * df.w)) / 2.
        df["x"] = (1. + ((1. - df.r1) * (1. - df.r2) ** 2) ** (1. / 3.) + ((1. - df.r2) * (1. - df.r1) ** 2) ** (
                    1. / 3.)) ** -1.
        df["om_a_eq"] = (1. - df.r1 * df.x) * (1. - df.r2 * df.x) * (2. - (df.r1 + df.r2) * df.x) / (
                    (1. - df.x) * (3. - df.x * (1. + df.r1 + df.r2)) ** 2)
        df["om_b_eq"] = df.x / (3. - df.x * (1. + df.r1 + df.r2))
        df["c_eos"] = df.apply(lambda row: (1. / (1. - row.r1)) if (row.r1 == row.r2) else (
                    1. / (row.r1 - row.r2) * np.log(np.abs((1. - row.r2) / (1. - row.r1)))), axis=1)
        cpr_EOS, om_b = 0.623225240140231, 0.0777960739038885
        df["multiplier"] = cpr_EOS * om_b / (df.c_eos * df.om_b_eq)
        df["ai_eq"] = (df.om_a_eq * ((gasConstant * df.tc_k) ** 2) / (df.pc_mpa * 10. ** 6)) * df.alpha_eos
        df["bi_eq"] = df.om_b_eq * (gasConstant * df.tc_k / (df.pc_mpa * 10. ** 6))
        df["AiByBi_eq"] = np.sqrt(df.ai_eq) / df.bi_eq
        return df

    def add_cm_data(self, df):
        groups = pd.read_csv("kij_input/kij_group_data/CMGroups")
        cols = [col for col in groups.columns if col != "group"]
        cm = self.well.getCmDataAsDf()[cols]

        for index, row in groups.iterrows():
            tdf = pd.DataFrame()
            for col in cols:
                tdf[col] = cm[col] * row[col]
            df[row.group] = tdf.sum(axis=1)
        return df

    def solubility_distribution(self, df):
        groups = pd.read_csv("kij_input/kij_group_data/SolubilityDistribution")
        cols = [col for col in groups.columns if col != "group"]
        cm = self.well.getCmDataAsDf()
        cm["Five Membered ring"] = cm.nap5_ring
        cm["Six Membered ring"] = cm.nap6_ring
        cm["Conjugation_Hoy"] = (cm.ar_cc + cm.nap5_cc) / 2.
        for index, row in groups.iterrows():
            cm[row.group] = cm[cols].dot(row[cols].T)

        cm["hoy_total_sol"] = (cm["Hoy-FT"] + 277.) / cm.v25_wc
        cm["alpha_factor1"] = 10 ** (3.39066 * (cm.bp_final1 / cm.tc_k_final) - np.log10(cm.v25_wc) - 0.15848)
        cm["hoy_polar_sol"] = cm.hoy_total_sol * (cm["Hoy-Fp"] / (cm.alpha_factor1 * (cm["Hoy-FT"] + 277.))) ** 0.5
        cm["hydrogen_bond_sol"] = np.sqrt((cm["Eh=Hydorgen-bond"] / cm.v25_wc).tolist())
        cm.loc[cm.hydrogen_bond_sol > cm.hoy_polar_sol, "hydrogen_bond_sol"] = 0.8 * cm.loc[
            cm.hydrogen_bond_sol > cm.hoy_polar_sol, "hoy_polar_sol"]
        cm["hoy_dispersion_sol"] = (cm.hoy_total_sol ** 2 - cm.hoy_polar_sol ** 2 - cm.hydrogen_bond_sol ** 2) ** 0.5
        cm.loc[cm.hoy_dispersion_sol < 0, "hoy_dispersion_sol"] = cm.hoy_total_sol * 0.6
        cm.loc[cm.Hbase != 0, "acid_by_base"] = cm.loc[cm.Hbase != 0, "Hacid"] / cm.loc[cm.Hbase != 0, "Hbase"]
        cm.loc[cm.Hbase == 0, "acid_by_base"] = 0
        df["sol_total"] = cm.hoy_total_sol
        df["sol_polar"] = cm.hoy_polar_sol
        df["sol_h_bonding"] = cm.hydrogen_bond_sol
        df["sol_dispersion"] = cm.hoy_dispersion_sol
        df["sol_h_bonding_acid"] = np.sqrt((df.sol_h_bonding * cm.acid_by_base).tolist())
        df["sol_h_bonding_base"] = df.sol_h_bonding_acid / cm.acid_by_base
        df["acid_by_base"] = cm.acid_by_base
        df = self.slope_kij_c(df)
        df = self.ref_vol(df)
        df["T_Ref"] = 298.15
        return df

    def ref_vol(self, df_o):
        df = df_o.copy(deep=True)
        cm = self.well.getCmDataAsDf()
        eos_bp = self.well.getEosDataAsDf(self.args.eos)
        poas4_tr = self.well.getOmegaTuningOutputAsDf("poas_4", 0.7)
        df[["vb_fcm", "v60", "v20_wc", "v25_wc", "vc_final"]] = cm[["vb", "v60", "v20_wc", "v25_wc", "vc_final"]]
        df["vb_pr"] = eos_bp.vb
        df["omega_final"] = poas4_tr.omega_final
        df["ph_diff"] = eos_bp.phl - eos_bp.phv
        df["vb_diff_per"] = (df.vb_fcm - df.vb_pr) / df.vb_fcm * 100.
        df["v60_eos"] = df.v60 - df.vb_diff_per * df.v60 / 100.
        df["v20_eos"] = df.v20_wc - df.vb_diff_per * df.v20_wc / 100.
        df["v25_eos"] = df.v25_wc - df.vb_diff_per * df.v25_wc / 100.
        df_o["v25_eos"] = df.v25_eos
        return df_o

    def slope_kij_c(self, df_o):
        df = df_o.copy(deep=True)
        eos_bp = self.well.getEosDataAsDf(self.args.eos)
        sol_380 = self.well.getSolubilityTuningOutputAsDf(self.args.eos, 380)
        df[["hv_bp", "bp_final1", "vb", "tc_k"]] = eos_bp[["hv_bp", "bp_final1", "vb", "tc_k"]]
        df["T1"] = np.log(298.15)
        df["T2"] = np.log(380)
        df["T3"] = np.log(df.bp_final1)
        df["sol_298.15"] = np.log(df.sol_total.tolist())
        df["sol_380"] = np.log(sol_380.solubility)
        df["sol_bp"] = np.log(np.sqrt((df.hv_bp * 1000. - gasConstant * df.bp_final1) / df.vb))
        df["xi"] = df.T1 + df.T2 + df.T3
        df["xi2"] = df.T1 ** 2 + df.T2 ** 2 + df.T3 ** 2
        df["yi"] = df["sol_298.15"] + df["sol_380"] + df["sol_bp"]
        df["xiyi"] = df.T1 * df["sol_298.15"] + df.T2 * df["sol_380"] + df.T3 * df["sol_bp"]
        df["slope"] = (df.yi * df.xi - 3. * df.xiyi) / (df.xi ** 2 - 3. * df.xi2)
        df["intercept"] = (df.yi - df.xi * df.slope) / 3.
        sol_298 = self.well.getSolubilityTuningOutputAsDf(self.args.eos, 298.15)
        df_o[["slope", "intercept"]] = df[["slope", "intercept"]]
        df_o["sol_ratio"] = df["sol_total"] / sol_298.solubility
        return df_o

    def tr_data(self):
        df = self.well.getOmegaTuningOutputAsDf("poas_4", 0.7)
        df["scn"] = ["C%s" % i for i in df.index]
        df["bp"] = df.bp_final1
        for col in df.columns:
            if col not in ["omega_final", "tc_k", "pc_mpa", "zc_final", "vc_final", "bp", "alpha_l", "alpha_m", "scn",
                           "component", "mf", "seq"]:
                df.drop(columns=[col], inplace=True)
        df = self.add_cm_data(df)
        df["kij_factor"] = 10.
        for index, row in df.iterrows():
            if row.seq >= 2 and row["%s" % "alpha_m"] <= 0:
                df.loc[df.seq == row.seq, "%s" % "alpha_m"] = df.loc[df.seq == row.seq - 1, "%s" % "alpha_m"].values[
                                                                  0] ** 2 / \
                                                              df.loc[df.seq == row.seq - 2, "%s" % "alpha_m"].values[0]

        return df

    def data_121_temp_change(self, df, temp):  # self varies with temperature only
        alphas, ombs, diffs = for_pt(self.args.collection, self.args.eos,
                                     temp)  # based on eos and ref_temp get alpha and omb
        df["alpha_multiplier"] = alphas["%s" % temp]
        df["om_b"] = ombs["%s" % temp]
        df["diff_alpha_multiplier"] = diffs["%s" % temp]
        df["alpha_pr"] = ((temp / df.tc_k) ** (2 * (df.alpha_m - 1)) * (
            np.exp(df.alpha_l * (1 - (temp / df.tc_k) ** (2 * df.alpha_m)))))
        df["alpha_eos"] = df.alpha_pr * df.alpha_multiplier
        alpha_eos, om_b = self.handle_last_alpha_omb(df)
        df.loc[df.index == df.index.max(), "alpha_eos"] = alpha_eos
        df.loc[df.index == df.index.max(), "om_b"] = om_b
        df["MinPackingFraction"] = 0.1
        return df

    def user_input_data(self, max_scn):
        ival = self.well.getInputValidatedData("Individual")
        df = ival.loc[
            (ival.short_name < max_scn) | (np.isnan(ival.short_name)), ["scn", "component", "rf_mf", "bp"]].copy(
            deep=True)
        df.rename(columns={"rf_mf": "mf"}, inplace=True)
        df["tc_k"] = df.apply(
            lambda row: self.api_data.loc[self.api_data["Compound"] == row.component].CriticalTemperature.values[0],
            axis=1)
        df["pc_mpa"] = df.apply(
            lambda row: self.api_data.loc[self.api_data["Compound"] == row.component].CriticalPressure.values[0],
            axis=1)
        df["zc_final"] = df.apply(
            lambda row: self.api_data.loc[self.api_data["Compound"] == row.component].ZcCriticalCompFactor.values[0],
            axis=1)
        df["omega_final"] = df.apply(
            lambda row: self.api_data.loc[self.api_data["Compound"] == row.component].AccentricFactorOmega.values[0],
            axis=1)
        df["vc_final"] = df.apply(
            lambda row: self.api_data.loc[self.api_data["Compound"] == row.component].ExperimentalVc.values[0], axis=1)
        df["MinPackingFraction"] = df.apply(
            lambda row: self.api_data.loc[self.api_data["Compound"] == row.component].MinPackingFraction.values[0],
            axis=1)
        df["kij_factor"] = df.apply(
            lambda row: self.api_data.loc[self.api_data["Compound"] == row.component].KijFactor.values[0], axis=1)
        for i in range(1, 34):
            df["group%s" % i] = df.apply(
                lambda row: self.api_data.loc[self.api_data["Compound"] == row.component, "group%s" % i].values[0],
                axis=1)
        return df

    def ival_pressure_change(self, df, temp, pressure):  # self varies with temperaturer and pressure
        df["alpha_eos"] = df.apply(lambda row: alpha_omb_sol(row.component, "alpha", pressure, temp),
                                   axis=1)  # only self part varies with temp and pressure
        df["om_b"] = df.apply(lambda row: alpha_omb_sol(row.component, "omb", pressure, temp),
                              axis=1)  # only self part varies with temp and pressure
        if self.args.kij_method == "C":
            df["sol_dispersion"] = df.apply(lambda row: alpha_omb_sol(row.component, "sol_disp", pressure, temp),
                                            axis=1)  # only self part varies with temp and pressure
            df["sol_polar"] = df.apply(lambda row: alpha_omb_sol(row.component, "sol_polar", pressure, temp),
                                       axis=1)  # only self part varies with temp and pressure
            df["sol_h_bonding"] = df.apply(lambda row: alpha_omb_sol(row.component, "sol_h-bond", pressure, temp),
                                           axis=1)  # only self part varies with temp and pressure
            df["sol_volume"] = df.apply(lambda row: alpha_omb_sol(row.component, "volume", pressure, temp),
                                        axis=1)  # only self part varies with temp and pressure
        return df

    def prepare_input(self):
        df121 = self.tr_data()
        ival = self.user_input_data(df121.index.min())
        return df121, ival

    def combine_data(self, df121, ival):
        df = pd.concat([ival, df121], axis=0, sort=True)
        df.reset_index(inplace=True, drop=True)
        # fix om_b based on minPackingFraction
        df["om_b"] = df.apply(
            lambda row: (row.MinPackingFraction * row.vc_final * row.pc_mpa / (gasConstant * row.tc_k)) if row.om_b <= (
                        row.MinPackingFraction * row.vc_final * row.pc_mpa / (gasConstant * row.tc_k)) else row.om_b,
            axis=1)
        df = getattr(AlphaFunctions(), self.args.eos)(df)  # based on eos calculate u and w
        df = self.calculateAiByBiEquivalent(df)
        return df

    def debug_output(self, **kwargs):
        if self.args.debug:
            filename = "%s/ptmatching" % self.debugDir
            for item in kwargs["file_name_params"]:
                filename = "%s_%s" % (filename, item)
            filename = "%s.xlsx" % filename

            writer = pd.ExcelWriter(filename, engine="openpyxl")
            for i in range(len(kwargs["dfs"])):
                kwargs["data"][i].to_excel(writer, sheet_name=kwargs["dfs"][i])
            writer.save()

    def kij_methhod_a(self):
        # get previously calculated data
        df121, ival = self.prepare_input()
        for temp, pressures in self.ref_press_temp.items():
            self.logger.info("Calculating EIJ for temperature == %s" % (temp))
            # on temperature change re-calculate tr data
            df121 = self.data_121_temp_change(df121, temp)
            df = self.combine_data(df121, ival)
            eij_df = kijA().get_eij(df, temp, self.args.eos)
            # save eij output
            self.well.setEijData(eij_df, self.args.eos, temp, "NA", "A")  # varies with temperature only.
            for pressure in pressures:
                self.logger.info("executing for pressure == %s and temperature == %s" % (pressure, temp))
                # on pressure change re-calculate for user input data
                self.logger.info("Input data alpha om b")
                ival = self.ival_pressure_change(ival, temp, pressure)
                df = self.combine_data(df121, ival)
                self.logger.info("calculating kij")
                kij_df = kijA().get_kij(df, temp, self.args.eos, eij_df)
                # save kij output
                self.well.setKijData(kij_df, self.args.eos, temp, pressure, "A")
                self.debug_output(file_name_params=[temp, pressure, "method-a"], dfs=["input", "kij", "eij"],
                                  data=[df, kij_df, eij_df])
                self.logger.info("*" * 20)

    def ival_kij_b(self, df, temp, pressure):  # self varies with temperaturer and pressure
        available_temps = [180, 200, 220, 240, 260, 280, 298.15, 300, 320, 340, 360, 380, 400, 420, 440, 460, 480, 500,
                           520, 540, 560, 580, 600, 620, 640, 660, 680, 700, 720, 740, 760, 780, 800, 850, 900, 950,
                           1000]
        if temp not in available_temps:
            available_temps = available_temps + [temp]
            available_temps.sort()

        itemp = available_temps.index(temp)
        if itemp + 1 == len(available_temps):
            t1 = available_temps[itemp - 2]
            t2 = available_temps[itemp - 1]
            t3 = available_temps[itemp]
        elif itemp == 0:
            t1 = available_temps[itemp]
            t2 = available_temps[itemp + 1]
            t3 = available_temps[itemp + 2]
        else:
            t1 = available_temps[itemp - 1]
            t2 = available_temps[itemp]
            t3 = available_temps[itemp + 1]
        df["t1"] = t1
        df["t2"] = t2
        df["t3"] = t3

        df["alpha_eos_1"] = df.apply(lambda row: alpha_omb_sol(row.component, "alpha", pressure, t1),
                                     axis=1)  # only self part varies with temp and pressure
        df["alpha_eos_2"] = df.apply(lambda row: alpha_omb_sol(row.component, "alpha", pressure, t2),
                                     axis=1)  # only self part varies with temp and pressure
        df["alpha_eos_3"] = df.apply(lambda row: alpha_omb_sol(row.component, "alpha", pressure, t3),
                                     axis=1)  # only self part varies with temp and pressure

        df["k1"] = (df.t1 * df.t2 ** 2 - df.t2 * df.t1 ** 2)
        df["k2"] = (df.t1 * df.t3 ** 2 - df.t3 * df.t1 ** 2)
        df["k3"] = (df.t2 ** 2 - df.t1 ** 2)
        df["k4"] = (df.t3 ** 2 - df.t1 ** 2)

        df["d1"] = df.alpha_eos_1 * df.t2 ** 2 - df.alpha_eos_2 * df.t1 ** 2
        df["d2"] = df.alpha_eos_1 * df.t3 ** 2 - df.alpha_eos_3 * df.t1 ** 2
        df["c"] = (df.d1 * df.k2 - df.d2 * df.k1) / (df.k2 * df.k3 - df.k4 * df.k1)
        df["b"] = (df.d1 - df.c * df.k3) / df.k1
        df["a"] = (df.alpha_eos_1 - df.c - df.b * df.t1) / df.t1 ** 2
        df["diff_alpha_wrt_temp"] = 2 * df.a * df.t2 + df.b
        return df

    def kij_methhod_b(self):
        # get previously calculated data
        df121, ival = self.prepare_input()
        for temp, pressures in self.ref_press_temp.items():
            # on temperature change re-calculate tr data
            df121 = self.data_121_temp_change(df121, temp)
            df121["diff_alpha_pr_wrt_temp"] = df121.alpha_pr * (
                        (2 * df121.alpha_m - 2) / temp - (2 * df121.alpha_l * df121.alpha_m / df121.tc_k) * (
                            temp / df121.tc_k) ** (2 * df121.alpha_m - 1))
            df121["diff_alpha_wrt_temp"] = (
                        df121.alpha_multiplier * df121.diff_alpha_pr_wrt_temp + df121.alpha_pr * df121.diff_alpha_multiplier)
            for pressure in pressures:
                self.logger.info("Calculating EIJ & KIJ for pressure == %s and temperature == %s" % (pressure, temp))
                self.logger.info("Input data alpha om b")
                ival = self.ival_pressure_change(ival, temp, pressure)
                self.logger.info("Differentiation")
                ival = self.ival_kij_b(ival, temp, pressure)
                self.logger.info("Combining data and calculating cpar")
                df = self.combine_data(df121, ival)
                df["cpar"] = df.apply(
                    lambda row: 0.587 if row.epsilon - row.sigma <= 0 or 1. + row.epsilon <= 0 or (1. + row.epsilon) / (
                                1. + row.sigma) < 0 else (1. / (row.epsilon - row.sigma)) * np.log(
                        (1. + row.epsilon) / (1. + row.sigma)), axis=1)

                self.logger.info("Calculating EIJ for pressure == %s and temperature == %s" % (pressure, temp))
                eij_df = kijB().get_eij(df, temp, self.args.eos)
                self.logger.info("Calculating KIJ for pressure == %s and temperature == %s" % (pressure, temp))
                kij_df = kijB().get_kij(df, temp, self.args.eos, eij_df)
                self.logger.info("Saving output")
                self.logger.info("*" * 20)

                # save kij and eij output
                self.well.setEijData(eij_df, self.args.eos, temp, pressure, "B")
                self.well.setKijData(kij_df, self.args.eos, temp, pressure, "B")
                self.debug_output(file_name_params=[temp, pressure, "method-b"], dfs=["input", "kij", "eij"],
                                  data=[df, kij_df, eij_df])

    def kij_methhod_c(self):
        # get previously calculated data
        df121, ival = self.prepare_input()
        # add solubility distribution
        df121 = self.solubility_distribution(df121)
        for temp, pressures in self.ref_press_temp.items():
            # on temperature change re-calculate tr data
            df121 = self.data_121_temp_change(df121, temp)
            df121["diff_alpha_pr_wrt_temp"] = df121.alpha_pr * (
                        (2 * df121.alpha_m - 2) / temp - (2 * df121.alpha_l * df121.alpha_m / df121.tc_k) * (
                            temp / df121.tc_k) ** (2 * df121.alpha_m - 1))
            df121["diff_alpha_wrt_temp"] = (
                        df121.alpha_multiplier * df121.diff_alpha_pr_wrt_temp + df121.alpha_pr * df121.diff_alpha_multiplier)
            for pressure in pressures:
                self.logger.info("Calculating KIJ for pressure == %s and temperature == %s" % (pressure, temp))
                ival = self.ival_pressure_change(ival, temp, pressure)
                ival = self.ival_kij_b(ival, temp, pressure)
                df = self.combine_data(df121, ival)

                kij_df = kijC().get_kij(df, temp, pressure, self.args.eos)
                self.logger.info("Saving output")
                self.logger.info("*" * 20)

                # save kij and eij output
                self.well.setKijData(kij_df, self.args.eos, temp, pressure, "C")
                self.debug_output(file_name_params=[temp, pressure, "method-c"], dfs=["input", "kij"],
                                  data=[df, kij_df])
