import numpy as np
import pandas as pd

gasConstant = 8.314472


class kijC:
    def __init__(self):
        pass

    def run(self, df, temp, pressure, eos):
        kij = self.get_kij(df, temp, pressure, eos)
        return kij

    def get_kij(self, df, temp, pressure, eos):
        df = self.zlzv(df, temp, pressure)
        df = self.solubility(df, temp, pressure)
        kij_df = df.copy(deep=True)
        for index, row in df.iterrows():
            for_comp = row.scn if pd.isna(row.component) else row.component
            df["d12"] = 1. - (df.b / row.b) ** df.aa_par + df.aa_par * np.log(df.b / row.b)
            df["d21"] = 1. - (row.b / df.b) ** row.aa_par + row.aa_par * np.log(row.b / df.b)
            df["coeff2"] = ((df.b * 10 ** 6 / (gasConstant * temp)) * ((df.sol_dispersion - row.sol_dispersion) ** 2 + (
                        (row.qpar ** 2 * df.qpar ** 2) * (df.sol_polar - row.sol_polar) ** 2) / row.psi_par + (
                                                                                   df.sol_h_bonding_acid - row.sol_h_bonding_acid) * (
                                                                                   df.sol_h_bonding_base - row.sol_h_bonding_base) / row.zi_par) + df.d12)
            df["coeff1"] = (row.b * 10 ** 6 / (gasConstant * temp)) * ((df.sol_dispersion - row.sol_dispersion) ** 2 + (
                        (row.qpar ** 2 * df.qpar ** 2) * (df.sol_polar - row.sol_polar) ** 2) / df.psi_par + (
                                                                                   df.sol_h_bonding_acid - row.sol_h_bonding_acid) * (
                                                                                   df.sol_h_bonding_base - row.sol_h_bonding_base) / df.zi_par) + df.d21
            df["s21"] = (df.fugacity + df.coeff2 - (df.b / row.b) * (row.Z - 1.) + np.log(row.Z - row.B))
            df["s22"] = (row.A / (row.B * (row.r1 - row.r2))) * np.log(
                (row.Z + row.B * row.r2) / (row.Z + row.B * row.r1))
            df["s2"] = df.s21 / df.s22
            df["k2_infinity"] = 1 - (df.s2 + (df.b / row.b)) / (2 * np.sqrt(df.ac_alpha / row.ac_alpha))
            df["s11"] = (row.fugacity + df.coeff1 - (row.b / df.b) * (df.Z - 1) + np.log(df.Z - df.B))
            df["s12"] = (df.A / (df.B * (df.r1 - df.r2))) * np.log((df.Z + df.r2 * df.B) / (df.Z + df.r1 * df.B))
            df["s1"] = df.s11 / df.s12
            df["k1_infinity"] = 1 - (df.s1 + row.b / df.b) / (2 * np.sqrt(row.ac_alpha / df.ac_alpha))
            df[for_comp] = df.apply(lambda row1: (0.5 * row1.k2_infinity + 0.5 * row1.k1_infinity) / row.kij_factor if (
                        (row.mf + row1.mf == 0) or (row.mf == 0) or (row1.mf == 0)) else ((row.mf / (
                        row.mf + row1.mf)) * row1.k2_infinity + (row1.mf / (
                        row.mf + row1.mf)) * row1.k1_infinity) / row.kij_factor, axis=1)
            kij_df[for_comp] = df[for_comp]
            if pd.isna(row.component):
                kij_df.loc[kij_df.scn == row.scn, row.scn] = 0
            else:
                kij_df.loc[kij_df.component == row.component, row.component] = 0
            kij_df[for_comp] = kij_df.apply(lambda row: row[for_comp] if np.abs(row[for_comp]) <= 0.4 else (
                0.6 * row[for_comp] if (int(row[for_comp]) - (1 if np.sign(row[for_comp]) < 0 else 0)) == 0 else (
                            np.sign(row[for_comp]) * 0.5 * row[for_comp] / (
                                int(row[for_comp]) - (1 if np.sign(row[for_comp]) < 0 else 0)))), axis=1)
        return kij_df

    def solubility(self, df, temp, pressure):
        df["vl"] = df.ZL * gasConstant * temp / pressure
        df["vv"] = df.ZV * gasConstant * temp / pressure
        df["dm1"] = df.vl + df.r1 * df.b * 10 ** 6
        df["dm2"] = df.vl + df.r2 * df.b * 10 ** 6
        df["dm3"] = (df.vl + df.r1 * df.b * 10 ** 6) / (df.vl + df.r2 * df.b * 10 ** 6)
        df["final_v_liq"] = df.apply(
            lambda row: row.vv if (row.dm1 <= 0 or row.dm2 <= 0 or row.dm3 < 0 or row.ZL <= 0) else row.vl, axis=1)
        df["t3"] = df.apply(
            lambda row: 0 if (row.dm1 <= 0 or row.dm2 <= 0 or row.dm3 < 0 or row.vl == 0) else np.log(row.dm3), axis=1)

        df["t1"] = df.apply(lambda row: 0 if row.final_v_liq == 0 or (row.b * (row.r2 - row.r1)) == 0 else (
                    (1. / row.final_v_liq) * (
                        (row.ac_alpha - temp * row.ac * row.diff_alpha_wrt_temp) / (row.b * (row.r2 - row.r1)))),
                            axis=1)
        df["dm1"] = df.vv + df.r1 * df.b * 10 ** 6
        df["dm2"] = df.vv + df.r2 * df.b * 10 ** 6
        df["dm3"] = (df.vv + df.r1 * df.b * 10 ** 6) / (df.vv + df.r2 * df.b * 10 ** 6)
        df["t2"] = df.apply(
            lambda row: 0 if (row.dm1 <= 0 or row.dm2 <= 0 or row.dm3 < 0 or row.vv == 0) else np.log(row.dm3), axis=1)
        df["del_ui_by_vi"] = np.abs(df.t1 * (df.t2 - df.t3))
        df["limit_sol"] = np.exp(df.slope * np.log(temp) + df.intercept)
        df["sol_total"] = df.apply(
            lambda row: row.limit_sol if np.sqrt(row.del_ui_by_vi) < 0.75 * row.limit_sol or np.sqrt(
                row.del_ui_by_vi) > 1.25 * row.limit_sol else np.sqrt(row.del_ui_by_vi), axis=1)
        df["sol_total_hns"] = df.sol_total * df.sol_ratio
        index5 = df.loc[df.scn == "C5"].index.min()
        df.loc[df.index >= index5, "sol_volume"] = df.final_v_liq
        df.loc[df.index >= index5, "vref_mul"] = df.sol_total_hns / np.sqrt(((df.sol_dispersion * (
                    df.v25_eos / df.sol_volume) ** 1.25) ** 2 + (df.sol_polar * (
                    df.v25_eos / df.sol_volume) ** 0.5) ** 2 + (df.sol_h_bonding / (
            np.exp(-1.32 * 10 ** -3 * (298.15 - temp) - np.log((df.v25_eos / df.sol_volume) ** 0.5)))) ** 2).tolist())
        df.loc[df.index >= index5, "sol_dispersion"] = df.sol_dispersion * df.vref_mul * (
                    df.v25_eos / df.sol_volume) ** 1.25
        df.loc[df.index >= index5, "sol_polar"] = df.sol_polar * df.vref_mul * (df.v25_eos / df.sol_volume) ** 0.5
        df.loc[df.index >= index5, "sol_h_bonding"] = df.vref_mul * df.sol_h_bonding / (
            np.exp(-1.32 * 10 ** -3 * (298.15 - temp) - np.log((df.v25_eos / df.sol_volume) ** 0.5)))
        df.loc[df.index < index5, "acid_by_base"] = 0.95
        df["sol_h_bonding_acid"] = df.apply(lambda row: 0 if row.sol_h_bonding * row.acid_by_base < 0 else np.sqrt(
            row.sol_h_bonding * row.acid_by_base), axis=1)
        df["sol_h_bonding_base"] = df.apply(
            lambda row: 0 if row.acid_by_base == 0 else row.sol_h_bonding_acid / row.acid_by_base, axis=1)
        df.loc[df.index < index5, "qpar"] = 1.
        scns = np.array([float(scn.replace("C", "")) for scn in df.loc[df.index >= index5].scn.tolist()])
        df.loc[df.index >= index5, "qpar"] = (
                    0.000026945833142228 * scns ** 2 - 0.00501089299637658 * scns + 1.01942737237995)
        df["pol_par"] = 1. + 1.15 * df.qpar ** 4 * (1. - np.exp((-1. * 0.002337 * df.sol_polar ** 3).tolist()))
        df["zi_par"] = (0.68 * (df.pol_par - 1.) + ((3.24 - 2.4 * np.exp(
            (-1. * 0.002687 * (df.sol_h_bonding_acid * df.sol_h_bonding_base) ** 1.5).tolist())) ** (293. / temp)) ** 2)
        df["psi_par"] = df.pol_par + 0.002629 * df.sol_h_bonding_acid * df.sol_h_bonding_base
        df["aa_par"] = np.abs(0.953 - 0.002314 * (df.sol_polar ** 2 + df.sol_h_bonding_acid * df.sol_h_bonding_base))
        return df

    def zlzv(self, df, temperature, pressure):
        df["r1"] = df.sigma
        df["r2"] = df.epsilon
        df["A"] = df.ac_alpha * pressure * 10 ** 6 / (gasConstant ** 2 * temperature ** 2)
        df["B"] = (df.b * pressure * 10 ** 6 / (gasConstant * temperature))
        df["a1"] = (df.u * df.B - df.B - 1)
        df["a2"] = (df.A + df.w * df.B ** 2 - df.u * df.B - df.u * df.B ** 2)
        df["a3"] = -1. * (df.A * df.B + df.w * df.B ** 2 + df.w * df.B ** 3)
        df["Q"] = (3 * df.a2 - df.a1 ** 2) / 9
        df["L"] = (9 * df.a1 * df.a2 - 27 * df.a3 - 2 * df.a1 ** 3) / 54
        df["D"] = df.Q ** 3 + df.L ** 2
        df["S1"] = df.apply(
            lambda row: (-1. * (-1. * (row.L + np.sqrt(row.D))) ** (1. / 3.)) if (row.L + np.sqrt(row.D)) < 0 else (
                                                                                                                               row.L + np.sqrt(
                                                                                                                           row.D)) ** (
                                                                                                                               1. / 3.),
            axis=1)
        df["S2"] = df.apply(
            lambda row: (-1. * (-1. * (row.L - np.sqrt(row.D))) ** (1. / 3.)) if (row.L - np.sqrt(row.D)) < 0 else (
                                                                                                                               row.L - np.sqrt(
                                                                                                                           row.D)) ** (
                                                                                                                               1. / 3.),
            axis=1)
        df["Z1"] = df.apply(lambda row: 0 if row.Q >= 0 else 2. * np.sqrt(-row.Q) * np.cos(1. / 3. * (np.arccos(
            1. if np.abs(row.L / np.sqrt(-row.Q ** 3)) > 1 else row.L / np.sqrt(
                -row.Q ** 3))) + 2 * np.pi / 3.) - row.a1 / 3., axis=1)
        df["Z2"] = df.apply(lambda row: 0 if row.Q >= 0 else 2. * np.sqrt(-row.Q) * np.cos(1. / 3. * (np.arccos(
            1. if np.abs(row.L / np.sqrt(-row.Q ** 3)) > 1 else row.L / np.sqrt(
                -row.Q ** 3))) + 4 * np.pi / 3.) - row.a1 / 3., axis=1)
        df["Z3"] = df.apply(lambda row: 0 if row.Q >= 0 else 2. * np.sqrt(-row.Q) * np.cos(1. / 3. * (
            np.arccos(1. if np.abs(row.L / np.sqrt(-row.Q ** 3)) > 1 else row.L / np.sqrt(-row.Q ** 3)))) - row.a1 / 3.,
                            axis=1)
        df["Z1_d"] = df.S1 + df.S2 - df.a1 / 3.
        df["Z1_dd"] = df.apply(
            lambda row: ((-1. * (-1. * row.L) ** 1. / 3.) if row.L < 0 else row.L ** (1. / 3.)) * 2. - row.a1 / 3.,
            axis=1)
        df["Z2_dd"] = df.apply(
            lambda row: (((-1. * (-1. * row.L) ** 1. / 3.) if row.L < 0 else row.L ** (1. / 3.)) + row.a1 / 3.) * -1,
            axis=1)
        df["Z3_dd"] = df.Z2_dd
        df["ZL"] = df.apply(lambda row: self._min([row.Z1, row.Z2, row.Z3]) if row.D < 0 else (
            self._min([row.Z1_dd, row.Z2_dd, row.Z3_dd]) if row.D == 0 else row.Z1_d), axis=1)
        df["ZV"] = df.apply(lambda row: self._max([row.Z1, row.Z2, row.Z3]) if row.D < 0 else (
            self._max([row.Z1_dd, row.Z2_dd, row.Z3_dd]) if row.D == 0 else 0), axis=1)
        df["phl"] = df.apply(lambda row: (
                    (row.ZL - 1.) - np.log(row.ZL - row.B) - (row.A / (row.B * (row.r2 - row.r1))) * np.log(
                (row.ZL + row.r2 * row.B) / (row.ZL + row.r1 * row.B))), axis=1)
        df["phv"] = df.apply(lambda row: (
                    (row.ZV - 1.) - np.log(row.ZV - row.B) - (row.A / (row.B * (row.r2 - row.r1))) * np.log(
                (row.ZV + row.r2 * row.B) / (row.ZV + row.r1 * row.B))), axis=1)
        df["fugacity"] = df.apply(lambda row: self.fugacity(row.r1, row.r2, row.A, row.B, row.ZL, row.phl, row.phv),
                                  axis=1)
        df["Z"] = df.apply(lambda row: self.fugacity(row.r1, row.r2, row.A, row.B, row.ZL, row.ZL, row.ZV), axis=1)
        return df

    def fugacity(self, r1, r2, A, B, ZL, phl, phv):
        if (ZL - B) <= 0 or (B * (r2 - r1)) == 0 or (ZL + r1 * B) == 0 or (ZL + r2 * B) <= 0 or (ZL + r1 * B) <= 0 or (
                (ZL + r2 * B) / (ZL + r1 * B)) <= 0:
            return phv
        return phl

    def _min(self, data):
        data = [x for x in data if x > 0]
        if len(data) > 0:
            return min(data)
        return 0

    def _max(self, data):
        data = [x for x in data if x > 0]
        if len(data) > 0:
            return max(data)
        return 0
