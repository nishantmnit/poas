class utils:
    def __init__(Self):
        pass

    def get_collection(self, well):
        db = getDbCon('MBP')
        coll = db[well]
        return coll

    def get_connection(self):
        return MCOutput(self.coll)

    def get_api_data(self):
        return MCOutput().getAPIDataAsDf()

    def fTOs(self, f):
        s = ('%s' % (float(f))).replace('.', '_')
        return s

    def get_gor_data(self, oil_type, gor):
        db = getDbCon('Central')
        coll = db["PBO"]
        gen = coll.find({'Property': 'SolutionGOR'})
        fluid_codes = {}
        for data in gen:
            fluid_codes[data['OilType']] = {'GOR': data['GOR']}
            for g in data['GOR']:
                fluid_codes[data['OilType']][float(g)] = data[self.fTOs(g)]
        return np.array(fluid_codes[oil_type][gor])

    def get_user_data(self, Property):
        cols, data = list(), list()
        first = True
        for g in self.coll.find({'$and': [{"Property": Property}]}):
            row = list()
            for key in g:
                if first: cols.append(key)
                row.append(g[key])
            data.append(row)
            first = False
        df = pd.DataFrame(data=data, columns=cols, index=[i for i in range(len(data))])
        return df

    def read_user_input(self):
        gd = self.get_user_data("general-data")
        cce = self.get_user_data("cce")
        cvd = self.get_user_data("cvd")
        sep = self.get_user_data("separator")
        sep_cor = self.get_user_data("separator-corrected")
        dv_oil = self.get_user_data("dv-oil")
        bubble_points = self.get_user_data("bubble-points")
        gd.set_index("param", inplace=True)
        return gd, sep, sep_cor, dv_oil, bubble_points, cce, cvd

    def get_ind_plus_data(self):
        df, dfplus = self.well.getValidatedData()
        cm = self.well.getCmDataAsDf()[["component", "mf", "mass", "d60", "density"]].copy(deep=True)
        cm.rename(columns={"d60": "density", "density": "sg"}, inplace=True)
        cm["scn"] = ["C%s" % scn for scn in cm.index]

        max_scn = cm.index.min()

        ival = self.well.getInputValidatedData("Individual")
        df = ival.loc[
            (ival.short_name < max_scn) | (np.isnan(ival.short_name)), ["scn", "component", "mw", "rf_mf", "sg"]].copy(
            deep=True)
        df.rename(columns={"rf_mf": "mf", "mw": "mass"}, inplace=True)
        df["density"] = df.sg * 0.9991026

        df = pd.concat([df, cm], axis=0, sort=True)
        df.reset_index(inplace=True, drop=True)

        sum_mf = df.mf.sum()
        df["mf"] = df.mf / sum_mf

        temperature = list(self.ref_press_temp.keys())[0]
        pressure = self.ref_press_temp[temperature][0]

        kij = self.well.getKijData(self.args.eos, temperature, pressure, self.args.kij_method)

        df[["tc_k", "pc_mpa", "vc_final", "zc_final", "omega_final", "bp"]] = kij[
            ["tc_k", "pc_mpa", "vc_final", "zc_final", "omega_final", "bp"]]

        df["zimi"] = df.mf * df.mass
        df["zimibysgi"] = df.zimi / df.sg

        df["short_name"] = df.scn.str.extract('(^c\d.*)', expand=True, flags=re.IGNORECASE)
        df["short_name"] = df.short_name.str.extract('(\d.*)', expand=True, flags=re.IGNORECASE)
        df["short_name"] = df.short_name.astype(float)

        dfplus["mf"] = dfplus.apply(lambda row: df.loc[df.short_name >= row.name, "mf"].sum(), axis=1)
        dfplus["mass"] = dfplus.apply(lambda row: df.loc[df.short_name >= row.name, "zimi"].sum() / row.mf, axis=1)
        dfplus["sg"] = dfplus.apply(lambda row: df.loc[df.short_name >= row.name, "zimi"].sum() / df.loc[
            df.short_name >= row.name, "zimibysgi"].sum(), axis=1)

        dfplus.drop(columns=["expmass", "expdensity"], inplace=True)

        return df, dfplus

    def mat_inv(self, df):
        try:
            df_inv = pd.DataFrame(np.linalg.inv(df.values), df.columns, df.index)
        except:
            df_inv = pd.DataFrame(np.linalg.pinv(df.values), df.columns, df.index)
        return df_inv
