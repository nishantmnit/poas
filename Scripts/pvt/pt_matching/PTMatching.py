import argparse

from datetime import datetime

cwd = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(0, "%s/../../../Python" % (cwd))
from Lib.Utils import *
from kij import *
from SolutionGORBubblePoint import *
from BubblePoint import *
from Equilibrium import *


class PTMatching(object, kij, utils, BubblePoint, Equilibrium):
    def __init__(self):
        self.validEOS = ["poas_4", "pt", "generic_poas", "generic_poas_a", "als", "poas_4a", "sw_poas", "tb_poas", "pr",
                         "er_poas", "tbs_poas", "srk"]
        self.logger = getLogger('PTMatching')
        self.logger.info(" ".join(sys.argv))
        self.args = self.getoptions()
        self.coll = self.get_collection(self.args.collection)
        self.well = self.get_connection()
        self.ref_press_temp = self.user_res_temp()
        self.api_data = self.get_api_data()
        self.ensureDir()

    def getoptions(self):
        self.all_pressure = [0.10132501, 0.5, 1, 2, 4, 6, 8, 10, 15, 20, 30, 40, 50, 70, 100, 150, 200, 250, 300]
        self.all_temp = [180, 220, 260, 298.15, 300, 340, 380, 420, 460, 500, 540, 600, 640, 700, 760, 800, 900, 1000]
        parser = argparse.ArgumentParser()
        parser.add_argument('--collection', dest='collection', default='AB8', help='Well collection name MC/MC1/MC2')
        parser.add_argument('--eos', dest='eos', default="pr", help='EOS')
        parser.add_argument('--kij_method', dest='kij_method', default="A", help='kij method - (A/B/C')
        parser.add_argument('--debug', dest='debug', default=False, action='store_true', help='Run in debug mode')

        args = parser.parse_args()

        if (args.eos not in self.validEOS):
            raise ValueError("Valid EOS values are (%s)" % (", ".join(eos for eos in self.validEOS)))
        args.kij_method = args.kij_method.upper()
        return args

    def user_res_temp(self):
        press_temp = dict()
        gd, sep, sep_cor, dv_oil, bubble_points, cce, cvd = self.read_user_input()

        # at reservoir temp

        pressures = [gd['value']['pb_res_temp']]

        if not cce.empty:
            pressures = pressures + cce.pressure.tolist()

        if not cvd.empty:
            pressures = pressures + cvd.pressure.tolist()

        if not dv_oil.empty:
            pressures = pressures + dv_oil.loc[dv_oil.pressure < gd['value']['pb_res_temp']].pressure.tolist()

        press_temp[gd['value']['temp_res']] = list(set(pressures))

        # specific temp and press
        if not sep.empty:
            for index, row in sep.iterrows():
                if row.stage != "Total at Bubble Point":
                    press_temp[row.temperature] = [row.pressure]

        if not bubble_points.empty:
            for index, row in bubble_points.iterrows():
                press_temp[row.temperature] = [row.bubble_point]
        return press_temp

    def ensureDir(self):
        if self.args.debug == True:
            today = datetime.datetime.today().strftime('%Y%m%d')
            self.debugDir = "./Debug/%s/%s/%s" % (today, self.args.collection, "PTMatching")
            if not os.path.isdir(self.debugDir):
                os.makedirs(self.debugDir)
            self.logger.info("Writing debug output to - %s" % (self.debugDir))
        else:
            self.debugDir = None

    # for ganesh ka code
    def gor(self):
        gd, sep, sep_cor, dv_oil, bubble_points, cce, cvd = self.read_user_input()
        fluid_type = gd["value"]["fluid_type"]
        obj = SolutionGORBubblePoint(dbName='PBO')
        if sep_cor.shape[0] > 0:
            self.logger.info("Executing case 1")
            temp, bppCalculated = obj.solve_df(gd, sep, sep_cor, bubble_points, fluid_type, self.logger)
        else:
            self.logger.info("Executing case 2 or 3")
            temp, bppCalculated = obj.case2_3(gd, sep, sep_cor, bubble_points, fluid_type, self.logger)

        print temp, bppCalculated

    def solver1(self):
        calc_bp, check = self.checkBubblePoint()
        if not check:
            raise RuntimeError("Method not implemented")

        df, dfplus = self.equilibrium()
        print df.loc[df.index >= 9].mf.values
        print dfplus
        quit()


if __name__ == "__main__":
    obj = PTMatching()
    # if obj.args.kij_method == "A":
    #     obj.kij_methhod_a()
    # elif obj.args.kij_method == "B":
    #     obj.kij_methhod_b()
    # elif obj.args.kij_method == "C":
    #     obj.kij_methhod_c()
    # obj.gor()
    obj.solver1()
