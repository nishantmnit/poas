from SNO import *
from preComponentModeling import *
from fullComponentModeling import *
from VolumeSolver import *
from Scripts.pvt.eosbp.eosMatching import *
from Scripts.pvt.omegatuning.poas_4 import *
from Scripts.pvt.solubility.poas_4_solubility import *
from Scripts.pvt.core.storage.excel import write_excel
from Scripts.pvt.core.storage.mongo import write


class ComponentModeling(object):
    def __init__(self, parent):
        self.__dict__ = parent.__dict__.copy()

    def runComponentModeling(self):
        sno = self.runSNO()
        pcm = self.runPCM(sno)
        fcm = self.runFCM(sno, pcm)
        self.runEOSOmega()
        self.runVolumeSolver()
        self.runSolubility()

    def runSNO(self):
        sno = SNO(self)
        sno.optimize()
        write_excel(self.args.debug, self.debugDir, ["sno"], ["observed", "calculated"], [sno.ov, sno.cv])
        return sno.cv

    def runPCM(self, ov):
        pcm = preComponentModeling(self)
        pcm.optimize(ov)
        write_excel(self.args.debug, self.debugDir, ["pcm"], ["observed", "calculated"], [pcm.ov, pcm.cv])
        return pcm.cv

    def runFCM(self, sno, pcm):
        fcm = fullComponentModeling(self)
        fcm.optimize(sno, pcm)
        write_excel(self.args.debug, self.debugDir, ["fcm"], ["observed", "calculated"], [fcm.ov, fcm.cv])
        write(fcm.cv, "MBP", self.args.collection, logger=self.logger, Property="MCOutput",
              Identifier="ComponentModeling")
        return fcm.cv

    def runEOSOmega(self):
        self.runEOS("poas_4")
        self.runOmegaTuning("poas_4", 0.7)

    def runEOS(self, e):
        eos = eosMatching(self)
        eos.optimize(e)
        write_excel(self.args.debug, self.debugDir, ["eos", e], ["observed", "calculated"],
                    [eos.ov, eos.cv])
        write(eos.cv, "MBP", self.args.collection, logger=self.logger, Property="MCOutput", Identifier="EOS", Eos=e)

    def runOmegaTuning(self, e, temp):
        om = poas_4(self)
        om.optimize(temp)
        write_excel(self.args.debug, self.debugDir, ["omegaTuning_eos", e, temp],
                    ["observed", "calculated"], [om.ov, om.cv])
        write(om.cv, "MBP", self.args.collection, logger=self.logger, Property="MCOutput", Identifier="OmegaTuning",
              Eos=e, Temperature=temp)

    def runVolumeSolver(self):
        obj = VolumeSolver(self)
        df = obj.optimize()
        write_excel(self.args.debug, self.debugDir, ["VolumeSolver"], ["volume"], [df])
        write(df, "MBP", self.args.collection, logger=self.logger, Property="MCOutput", Identifier="VolumeSolver")

    def runSolubility(self):
        eos = "poas_4"
        obj = poas_4_solubility(self)
        for temp in [200, 250, 288.7056, 300, 350, 400, 475, 550]:
            obj.optimize(temp, eos)
            write_excel(self.args.debug, self.debugDir, ["Solubility_eos", eos, temp],
                        ["observed", "calculated"], [obj.ov, obj.cv])
            write(obj.cv, "MBP", self.args.collection, logger=self.logger, Property="MCOutput",
                  Identifier="SolubilityTuning", Eos=eos, Temperature=temp)
