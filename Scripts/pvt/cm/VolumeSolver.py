from Scripts.pvt.core.optimizers.ncg import *

from Scripts.pvt.core.optimizers.poas_interpolator import monotone
from Scripts.pvt.core.storage.fetch_most import validated_data, eosbp_data, cm_data
import pandas as pd

gasConstant = 8.314472
pressure = 0.101325
temperature = 288.7056
thermal_coefficient_bp = 0.8


class VolumeSolver:
    def __init__(self, parent):
        self.__dict__ = parent.__dict__.copy()

    def optimize(self):
        self._setNVar()
        ov = ObservedValues(self)
        vars = self._solver(ov.df, ov.dfplus)
        for i in range(len(vars)):
            ov.df[self._nvar[i]] = vars[i]
        cv = CalculatedValues(self, ov.df, ov.dfplus)
        return self.output(cv.df)

    def output(self, df):
        self.volume_below_bp(df)
        self.volume_bp_vc(df)
        df = self.volumes(df)
        return df

    def volumes(self, df_o):
        df = df_o[["scn", "mf", "mass", "final_vb"]].copy(deep=True)
        temps = [200, 250, temperature, 300, 350, 400, 475, 550, 298.15]
        for temp in temps:
            df["density_%s" % temp] = df_o.apply(lambda row: self.density(row, temp), axis=1)
            df["volume_%s" % temp] = df.mass / df["density_%s" % temp]
        return df

    def density(self, row, temp):
        if temp > row.bp_final1:
            return (row.density_bp * np.exp((-1 * row.final_thermal_expanstion_tc * (
                    (temp - 273.15) - (row.bp_final1 - 273.15)) * (
                                                     1 + thermal_coefficient_bp * row.final_thermal_expanstion_tc * (
                                                     (temp - 273.15) - (row.bp_final1 - 273.15))))))
        return (row.density_bp * np.exp((-1 * row.final_thermal_expanstion_bp * (
                (temp - 273.15) - (row.bp_final1 - 273.15)) * (
                                                 1 + thermal_coefficient_bp * row.final_thermal_expanstion_bp * (
                                                 (temp - 273.15) - (row.bp_final1 - 273.15))))))

    def volume_bp_vc(self, df_o):
        df = df_o.copy(deep=True)
        df["density_tc"] = df.mass / df.vc_final
        df["y1"] = np.log(df.density_tc / df.density_bp)
        df["a1"] = ((df.tc_k - 273.15) - (df.bp_final1 - 273.15))
        df["a2"] = thermal_coefficient_bp * df.a1
        df["a3"] = df.a1 * df.a2
        df["thermal_expansion_a"] = df.apply(
            lambda row: row.thermal_expansion_bp if (row.a1 ** 2 - 4. * row.a3 * row.y1) < 0 else (
                    (-row.a1 + np.sqrt(row.a1 ** 2 - 4. * row.a3 * row.y1)) / (2. * row.a3)), axis=1)
        df["thermal_expansion_b"] = df.apply(
            lambda row: row.thermal_expansion_bp if (row.a1 ** 2 - 4. * row.a3 * row.y1) < 0 else (-row.a1 - np.sqrt(
                row.a1 ** 2 - 4. * row.a3 * row.y1)) / (2. * row.a3), axis=1)
        df["thermal_expansion_1"] = df[["thermal_expansion_a", "thermal_expansion_b"]].max(axis=1)
        df["thermal_expansion_2"] = df[["thermal_expansion_a", "thermal_expansion_b"]].min(axis=1)
        df["final_thermal_expanstion"] = df.apply(
            lambda row: row.thermal_expansion_bp if row.thermal_expansion_a <= 0 and row.thermal_expansion_b <= 0 else (
                row.thermal_expansion_1 if row.thermal_expansion_a <= 0 or row.thermal_expansion_b <= 0 else row.thermal_expansion_2),
            axis=1)
        df_o["final_thermal_expanstion_tc"] = df.final_thermal_expanstion

    def volume_below_bp(self, df_o):
        df = df_o.copy(deep=True)
        df["density_60f"] = df.mass / df.volume_corrected_final
        df["y1"] = np.log(df.density_60f / df.density_bp)
        df["a1"] = ((temperature - 273.15) - (df.bp_final1 - 273.15))
        df["a2"] = thermal_coefficient_bp * df.a1
        df["a3"] = df.a1 * df.a2
        df["thermal_expansion_a"] = df.apply(
            lambda row: row.thermal_expansion_bp if (row.a1 ** 2 - 4. * row.a3 * row.y1) < 0 else (
                    (-row.a1 + np.sqrt(row.a1 ** 2 - 4. * row.a3 * row.y1)) / (2. * row.a3)), axis=1)
        df["thermal_expansion_b"] = df.apply(
            lambda row: row.thermal_expansion_bp if (row.a1 ** 2 - 4. * row.a3 * row.y1) < 0 else (-row.a1 - np.sqrt(
                row.a1 ** 2 - 4. * row.a3 * row.y1)) / (2. * row.a3), axis=1)
        df["thermal_expansion_1"] = df[["thermal_expansion_a", "thermal_expansion_b"]].max(axis=1)
        df["thermal_expansion_2"] = df[["thermal_expansion_a", "thermal_expansion_b"]].min(axis=1)
        df["final_thermal_expanstion"] = df.apply(
            lambda row: row.thermal_expansion_bp if row.thermal_expansion_2 <= 0 else row.thermal_expansion_2, axis=1)
        df.loc[
            df.final_thermal_expanstion > 3. * df.thermal_expansion_bp, "final_thermal_expanstion"] = df.thermal_expansion_bp
        df_o["final_thermal_expanstion_bp"] = df.final_thermal_expanstion

    def _setNVar(self):
        self._nvar = ["volume_slope_multiplier", "volume_correction_multiplier"]

    def _solver(self, df, dfplus):
        self.logger.info("Volume Solver started | start point = %s" % (",".join([str(x) for x in self._sp()])))
        self.ncg = ncg()
        var_calc = self.ncg.minimize(self._sp(), self._range(), self._cost_for_ncg, self.logger, max_iter=150,
                                     debug=self.args.debug, converge_f=self._converged, args=(df, dfplus))
        self.logger.info("Volume Solver completed | end point = %s" % (",".join([str(x) for x in var_calc])))
        return var_calc

    def _cost_for_ncg(self, vars, args):
        df, dfplus = args[0], args[1]
        cv, cost = self._cost(df, dfplus, vars)
        gd = Gradients(self)
        grad = gd.gradient(df, dfplus, cv)
        return cost, grad

    def _cost(self, df_o, dfplus_o, vars):
        df = df_o.copy(deep=True)
        dfplus = dfplus_o.copy(deep=True)
        for i in range(len(vars)):
            df[self._nvar[i]] = vars[i]
        cv = CalculatedValues(self, df, dfplus)
        cost = cv.dfplus.cost.sum()
        return cv, cost

    def _range(self):
        return [[0.25, 5.], [-5., 5.]]

    def _converged(self, debug, logger, iteration, grad):
        cd = np.sqrt(np.sum(np.square(grad), axis=0))

        if iteration < 3:
            return False
        elif iteration <= 10 and (cd <= 10 ** -3 or self.ncg.cost_diff <= 10 ** -5):
            if debug: logger.info("Converged by rule 1 - %s" % cd)
            return True
        elif (10 < iteration <= 25) and (cd <= 10 ** -2 or self.ncg.cost_diff <= 10 ** -3):
            if debug: logger.info("Converged by rule 2 - %s" % cd)
            return True
        elif (25 < iteration <= 50) and (cd <= 10 ** -1 or self.ncg.cost_diff <= 10 ** -2):
            if debug: logger.info("Converged by rule 3 - %s" % cd)
            return True
        elif (50 < iteration <= 75) and (cd <= 1 or self.ncg.cost_diff <= 10 ** -1):
            if debug: logger.info("Converged by rule 4 - %s" % cd)
            return True
        elif (75 < iteration <= 100) and (cd <= 5 or self.ncg.cost_diff <= 10 ** -1):
            if debug: logger.info("Converged by rule 4 - %s" % cd)
            return True
        elif (100 < iteration <= 150) and (self.ncg.cost_diff <= 5):
            if debug: logger.info("Converged by rule 4 - %s" % cd)
            return True
        return False

    def _sp(self):
        return [1., 0.1]


class Gradients(object):
    def __init__(self, parent):
        self.__dict__ = parent.__dict__.copy()

    def gradient(self, df, dfplus, cv):
        return self._grad(df, dfplus, cv)

    def _grad(self, df_o, dfplus_o, cv):
        df = df_o.copy(deep=True)
        dfplus = dfplus_o.copy(deep=True)
        df["diff_vol_corr_wrt1"] = 0
        df["diff_vol_corr_wrt2"] = df.apply(
            lambda row: 0 if row.scn > 6 else row.initial_volume_60f * (row.volume_correction / 100.), axis=1)

        df["diff_vol_slope_wrt1"] = 0
        vol_slope = list()
        for index, row in df.loc[df.scn > 6].iterrows():
            if len(vol_slope) == 0:
                vol_slope.append(row.volume_slope + df.loc[df.scn == 6, "diff_vol_slope_wrt1"].values[0])
            else:
                vol_slope.append(row.volume_slope + vol_slope[-1])
        df.loc[df.scn > 6, "diff_vol_slope_wrt1"] = vol_slope

        df["diff_vol_slope_wrt2"] = df.diff_vol_corr_wrt2
        df.loc[df.scn > 6, "diff_vol_slope_wrt2"] = df.loc[df.scn == 6, "diff_vol_slope_wrt2"].values[0]

        df["diff_vol_corr_wrt1"] = (
                df.diff_vol_slope_wrt1 * (1. + cv.df.volume_correction / 100.) + cv.df.volume_slope_for_diff * (
                0 / 100))
        df.loc[df.scn > 6, "diff_vol_corr_wrt2"] = (
                df.diff_vol_slope_wrt2 * (1. + cv.df.volume_correction / 100.) + cv.df.volume_slope_for_diff * (
                df.volume_correction / 100.))

        df["volume_corrected_final"] = cv.df.volume_corrected_final
        df["diff_vol_corr_final_wrt1"] = df.apply(lambda row: row.diff_vol_corr_wrt1 if row.scn <= 6 else (
            0 if row.volume_corrected_final >= row.final_vb else row.diff_vol_corr_wrt1), axis=1)
        df["diff_vol_corr_final_wrt2"] = df.apply(lambda row: row.diff_vol_corr_wrt2 if row.scn <= 6 else (
            0 if row.volume_corrected_final >= row.final_vb else row.diff_vol_corr_wrt2), axis=1)

        df["diff_density_60f_wrt1"] = (-1.) * df.mass * (1. / cv.df.volume_corrected ** 2) * df.diff_vol_corr_final_wrt1
        df["diff_density_60f_wrt2"] = (-1.) * df.mass * (1. / cv.df.volume_corrected ** 2) * df.diff_vol_corr_final_wrt2

        df["diff_sg_60f_wrt1"] = df.diff_density_60f_wrt1 / 0.9991026
        df["diff_sg_60f_wrt2"] = df.diff_density_60f_wrt2 / 0.9991026

        df["diff_zimibysgi_wrt1"] = df.zimi * (-1. / cv.df.sg_60f ** 2) * df.diff_sg_60f_wrt1
        df["diff_zimibysgi_wrt2"] = df.zimi * (-1. / cv.df.sg_60f ** 2) * df.diff_sg_60f_wrt2

        df["zimibysgi"] = cv.df.zimibysgi
        dfplus["diff_density_wrt1"] = dfplus.apply(lambda row: df.loc[df.scn >= row.name, "zimi"].sum() * (
                -1. / (df.loc[df.scn >= row.name, "zimibysgi"].sum()) ** 2) * df.loc[
                                                                   df.scn >= row.name, "diff_zimibysgi_wrt1"].sum(),
                                                   axis=1)
        dfplus["diff_density_wrt2"] = dfplus.apply(lambda row: df.loc[df.scn >= row.name, "zimi"].sum() * (
                -1. / (df.loc[df.scn >= row.name, "zimibysgi"].sum()) ** 2) * df.loc[
                                                                   df.scn >= row.name, "diff_zimibysgi_wrt2"].sum(),
                                                   axis=1)

        dfplus["density"] = cv.dfplus.density
        dfplus["diff_obj_wrt1"] = dfplus.scale * (
                2. * (dfplus.expdensity - dfplus.density) / dfplus.expdensity ** 2) * (
                                      -1.) * dfplus.diff_density_wrt1
        dfplus["diff_obj_wrt2"] = dfplus.scale * (
                2. * (dfplus.expdensity - dfplus.density) / dfplus.expdensity ** 2) * (
                                      -1.) * dfplus.diff_density_wrt2

        grad = np.array([dfplus.diff_obj_wrt1.sum(), dfplus.diff_obj_wrt2.sum()])
        return grad


class CalculatedValues(object):
    def __init__(self, parent, df, dfplus):
        self.__dict__ = parent.__dict__.copy()
        self._prepare(df, dfplus)

    def _prepare(self, df, dfplus):
        df["volume_slope"] = df.volume_slope * df.volume_slope_multiplier
        df["volume_correction"] = df.volume_correction * df.volume_correction_multiplier
        df["volume_corrected"] = df.initial_volume_60f * (1. + df.volume_correction / 100.)
        vol_slope = list()
        for index, row in df.loc[df.scn > 6].iterrows():
            if len(vol_slope) == 0:
                vol_slope.append(row.volume_slope + df.loc[df.scn == 6, "volume_corrected"].values[0])
            else:
                vol_slope.append(row.volume_slope + vol_slope[-1])
        df.loc[df.scn > 6, "volume_slope_for_diff"] = vol_slope
        df.loc[df.scn > 6, "volume_corrected"] = vol_slope * (1. + df.loc[df.scn > 6., "volume_correction"] / 100.)
        df.loc[df.scn > 6, "volume_corrected_final"] = df.loc[df.scn > 6].apply(
            lambda row: 0.95 * row.final_vb if row.volume_corrected >= row.final_vb else row.volume_corrected, axis=1)
        df.loc[df.scn <= 6, "volume_corrected_final"] = df.loc[df.scn <= 6, "volume_corrected"]
        df["initial_density_60f"] = df.mass / df.volume_corrected_final
        df["sg_60f"] = df.initial_density_60f / 0.9991026
        df["zimibysgi"] = df.zimi / df.sg_60f
        dfplus["density"] = dfplus.apply(
            lambda row: df.loc[df.scn >= row.name, "zimi"].sum() / df.loc[df.scn >= row.name, "zimibysgi"].sum(),
            axis=1)
        dfplus["cost"] = dfplus.scale * ((dfplus.expdensity - dfplus.density) / dfplus.expdensity) ** 2
        self.df = df
        self.dfplus = dfplus


class ObservedValues(object):
    def __init__(self, parent):
        self.__dict__ = parent.__dict__.copy()
        self._prepare()

    def _prepare(self):
        df = self.get_cm_data()
        poas_4 = self.get_poas_4_data()
        df = pd.concat([df, poas_4], axis=1)
        df["scn"] = df.index
        df.reset_index(inplace=True)
        self.final_vb(df)
        df["density_bp"] = df.mass / df.final_vb
        df["thermal_expansion_bp"] = 613.97226 / (df.density_bp * 1000.) ** 2
        df["initial_density_60f"] = (df.density_bp * np.exp((-1. * df.thermal_expansion_bp * (
                (temperature - 273.15) - (df.bp_final1 - 273.15)) * (
                                                                     1. + thermal_coefficient_bp * df.thermal_expansion_bp * (
                                                                     (temperature - 273.15) - (
                                                                     df.bp_final1 - 273.15))))))
        df["initial_volume_60f"] = df.mass / df.initial_density_60f
        df["volume_slope"] = df.initial_volume_60f - df.initial_volume_60f.shift(1)
        df.loc[df.index == df.index.min(), "volume_slope"] = \
            df.loc[df.index == df.index.min() + 1, "volume_slope"].values[0]
        df["volume_correction"] = (4.81668632961298 * np.exp(0.0140674955555049 * df.scn))
        # create 7, 12 and 20
        dfplus = self.createPlusFractions(df)
        self.scales(dfplus)
        self.df = df
        self.dfplus = dfplus

    def createPlusFractions(self, df):
        df_v, dfplus = validated_data(self.args.collection, self.logger)
        self.drop_cols(["expdensity"], dfplus)
        needed = [7, 12, 20]
        for need in needed:
            if need not in dfplus.index.tolist():
                sg = df.loc[df.scn >= need, "zimi"].sum() / df.loc[df.scn >= need, "zimibysgi"].sum()
                dfplus = pd.concat([dfplus, pd.DataFrame(data=[sg], index=[need], columns=["expdensity"])])
                dfplus.sort_index(inplace=True)
        return dfplus

    def get_cm_data(self):
        df = cm_data(self.args.collection, self.logger)
        needed = ["mf", "mass", "vb", "density"]
        self.drop_cols(needed, df)
        df.rename(columns={"vb": "vb_fcm"}, inplace=True)
        df["zimi"] = df.mass * df.mf
        df["zimibysgi"] = df.zimi / df.density
        return df

    def get_poas_4_data(self):
        df = eosbp_data(self.args.collection, self.logger, "poas_4")
        needed = ["bp_final1", "vb", "ph_diff", "tc_k", "vc_final"]
        self.drop_cols(needed, df)
        return df

    def final_vb(self, df):
        df["final_vb"] = df.vb
        _index = df.index.max() - 1
        if df.loc[df.index == _index, "ph_diff"].values[0] > 10 ** -12:
            x = df.loc[df.index < _index].tail(5).index.tolist()
            y = df.loc[df.index < _index].tail(5).vb.tolist()
            vb = monotone(x, y, [_index])[0]
            df.loc[df.index == _index, "final_vb"] = vb
        _index = df.index.max()
        if df.loc[df.index == _index, "ph_diff"].values[0] > 0.1:
            x = df.loc[df.index < _index - 1].tail(5).index.tolist()
            y = df.loc[df.index < _index - 1].tail(5).vb.tolist()
            vb = monotone(x, y, [_index])[0]
            vb_fcm = df.loc[df.index == _index, "vb_fcm"].values[0]
            vb = vb_fcm if vb < vb_fcm * 0.9 or vb > vb_fcm * 1.1 else vb
            df.loc[df.index == _index, "final_vb"] = vb

    def drop_cols(self, needed, df):
        droplist = [col for col in df.columns if col not in needed]
        df.drop(columns=droplist, inplace=True)

    def scales(self, dfplus):
        _s = {5000: [7, 8], 4500: [9, 12], 2000: [13, 16], 1000: [17, 25], 800: [26, 121]}
        for key, value in _s.items():
            dfplus.loc[(dfplus.index >= value[0]) & (dfplus.index <= value[1]), "scale"] = key
