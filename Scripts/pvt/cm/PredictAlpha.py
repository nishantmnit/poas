import numpy as np
import pandas as pd

from Scripts.pvt.core.pandas.dataframe import drop_not_in_cols
from Scripts.pvt.core.storage.fetch_most import solubility_data, eosbp_data

temps = [200, 250, 288.7056, 300]


def predict(self, temp):
    dfs = get_solubility(self.args.collection, self.logger)
    poas_4 = get_eos_data(self.args.collection, self.logger, "poas_4")
    samples, df = prepare_input(dfs, poas_4)
    df = intercept_slope(samples, df)
    df = predict_alpha(df, temp)
    return df[["intercept", "slope", "alpha_pr_req_tr", "alpha_multiplier", "alpha_l", "alpha_m", "tc_k"]]


def predict_alpha(df, temp):
    df["alpha_eos_tc"] = np.exp(df.intercept + df.tc_k * df.slope)
    df["multiplier_tc"] = 1. / df.alpha_eos_tc
    df["alpha_eos"] = df.multiplier_tc * np.exp(df.slope * temp + df.intercept)
    df["tr"] = temp / df.tc_k
    df["alpha_pr_req_tr"] = ((temp / df.tc_k) ** (2. * (df.alpha_m - 1.)) * (
        np.exp(df.alpha_l * (1. - (temp / df.tc_k) ** (2 * df.alpha_m)))))
    df["alpha_multiplier"] = df.alpha_eos / df.alpha_pr_req_tr
    return df


def intercept_slope(samples, df_o):
    df = df_o.copy(deep=True)
    for i in range(1, samples + 1):
        df["x%s" % i] = df["T_%s" % i]
        df["y%s" % i] = np.log(df["alpha_eos_%s" % i])
        if i == 1:
            df["xi"] = df["x%s" % i]
            df["yi"] = df["y%s" % i]
            df["xiyi"] = df["x%s" % i] * df["y%s" % i]
            df["xi2"] = df["x%s" % i] ** 2
        else:
            df["xi"] = df["xi"] + df["x%s" % i]
            df["yi"] = df["yi"] + df["y%s" % i]
            df["xiyi"] = df["xiyi"] + (df["x%s" % i] * df["y%s" % i])
            df["xi2"] = df["xi2"] + df["x%s" % i] ** 2

    df_o["intercept"] = (df.xi * df.xiyi - df.yi * df.xi2) / (df.xi ** 2 - samples * df.xi2)
    df_o["slope"] = (df.yi - samples * df_o.intercept) / df.xi
    return df_o


def prepare_input(dfs, poas_4):
    df = pd.DataFrame(index=poas_4.index)
    for i in range(len(temps)):
        df["T_%s" % (i + 1)] = temps[i]
        df["alpha_eos_%s" % (i + 1)] = dfs[temps[i]]["alpha_eos"]
    i += 2
    df["T_%s" % i] = poas_4["bp_final1"]
    df["alpha_eos_%s" % i] = poas_4.alpha_eos_bp
    i += 1
    df["T_%s" % i] = poas_4["tc_k"]
    df["alpha_eos_%s" % i] = 1.
    samples = i
    df[["tc_k", "alpha_l", "alpha_m"]] = poas_4[["tc_k", "alpha_l", "alpha_m"]]
    return samples, df


def get_solubility(well, logger):
    eos = "poas_4"
    dfs = dict()
    for temp in temps:
        dfs[temp] = solubility_data(well, logger, eos, temp)[["alpha_eos", "om_b_multiplier"]].copy(deep=True)
    return dfs


def get_eos_data(well, logger, eos):
    eos = eosbp_data(well, logger, eos)
    eos["alpha_eos_bp"] = eos.alpha_multiplier * eos.alpha_pr
    eos["scn"] = eos.index
    eos.reset_index(inplace=True)
    cols = ["scn", "bp_final1", "tc_k", "alpha_eos_bp", "alpha_l", "alpha_m"]
    drop_not_in_cols(eos, cols)
    return eos
