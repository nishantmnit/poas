import numpy as np
import pandas as pd
from Scripts.pvt.core.storage.fetch_most import validated_data, recommended


class SNO(object):
    def __init__(self, parent):
        self.__dict__ = parent.__dict__.copy()

    def optimize(self):
        self._nvar = ["C", "S", "O", "N"]
        ov = ObservedValues(self)
        for index, row in ov.df.iterrows():
            vars = self._solver(row)
            for i in range(len(vars)):
                ov.df.loc[ov.df.seq == row.seq, self._nvar[i]] = vars[i]
        cv = CalculatedValues(self, ov)
        self.ov = ov.df
        self.cv = cv.df

    def _solver(self, row):
        vars = self._sp(row)
        self.logger.info("SNO tuning for scn = %s | component = %s | start point = %s" % (
        row.name, row.component, ",".join([str(x) for x in vars])))
        _max = 1
        while _max <= 500:
            # cost = self._cost(row, vars)
            grad = self._grad(row, vars)
            alpha = self._alpha(row, vars, grad)
            if alpha == 0: break
            vars = self.regress(row, vars, grad, alpha)
            _max += 1
        self.logger.info("SNO tuning for scn = %s | component = %s | end point = %s | iterations = %s" % (
        row.name, row.component, ",".join([str(x) for x in vars]), _max))
        return vars

    # def _cost(self, row, vars):
    #     return (100.*np.abs((row.mass-(vars[0]*(12.011+1.008*row.hbyc)+vars[1]*32.065+vars[2]*15.9999+vars[3]*14.0067))/row.mass))+(np.abs((row.nhc25-(vars[0]*(420+100.8*row.hbyc)+vars[1]*340.2-vars[2]*180.6-vars[3]*21))/row.nhc25))+(100*np.abs((row.cfraction-(vars[0]/(vars[0]+vars[0]*row.hbyc+vars[1]+vars[2]+vars[3])))/row.cfraction))

    def _grad(self, row, vars):
        grad0 = 100. * (np.sign((row.mass - (
                    vars[0] * (12.011 + 1.008 * row.hbyc) + vars[1] * 32.065 + vars[2] * 15.9999 + vars[
                3] * 14.0067)) / row.mass) * (-1 / row.mass) * (12.011 + 1.008 * row.hbyc)) + (np.sign((row.nhc25 - (
                    vars[0] * (420 + 100.8 * row.hbyc) + vars[1] * 340.2 - vars[2] * 180.6 - vars[
                3] * 21)) / row.nhc25) * (-1 / row.nhc25) * (420 + 100.8 * row.hbyc)) + 100 * (np.sign((
                                                                                                                   row.cfraction - (
                                                                                                                       vars[
                                                                                                                           0] / (
                                                                                                                                   vars[
                                                                                                                                       0] +
                                                                                                                                   vars[
                                                                                                                                       0] * row.hbyc +
                                                                                                                                   vars[
                                                                                                                                       1] +
                                                                                                                                   vars[
                                                                                                                                       2] +
                                                                                                                                   vars[
                                                                                                                                       3]))) / row.cfraction) * (
                                                                                                           -1 / row.cfraction) * (
                                                                                                           vars[1] +
                                                                                                           vars[2] +
                                                                                                           vars[3]) / ((
                    vars[0] + vars[0] * row.hbyc + vars[1] + vars[2] + vars[3])) ** 2)
        grad1 = 100. * (np.sign((row.mass - (
                    vars[0] * (12.011 + 1.008 * row.hbyc) + vars[1] * 32.065 + vars[2] * 15.9999 + vars[
                3] * 14.0067)) / row.mass) * (-1 / row.mass) * 32.065) + (np.sign((row.nhc25 - (
                    vars[0] * (420 + 100.8 * row.hbyc) + vars[1] * 340.2 - vars[2] * 180.6 - vars[
                3] * 21)) / row.nhc25) * (-1 / row.nhc25) * 340.2) + 100 * (np.sign((row.cfraction - (
                    vars[0] / (vars[0] + vars[0] * row.hbyc + vars[1] + vars[2] + vars[3]))) / row.cfraction) * (
                                                                                        -1 / row.cfraction) * (
                                                                                        -1 * vars[0]) / ((
                    vars[0] + vars[0] * row.hbyc + vars[1] + vars[2] + vars[3])) ** 2)
        grad2 = 100. * (np.sign((row.mass - (
                    vars[0] * (12.011 + 1.008 * row.hbyc) + vars[1] * 32.065 + vars[2] * 15.9999 + vars[
                3] * 14.0067)) / row.mass) * (-1 / row.mass) * 15.9999) + (np.sign((row.nhc25 - (
                    vars[0] * (420 + 100.8 * row.hbyc) + vars[1] * 340.2 - vars[2] * 180.6 - vars[
                3] * 21)) / row.nhc25) * (1 / row.nhc25) * 180.6) + 100 * (np.sign((row.cfraction - (
                    vars[0] / (vars[0] + vars[0] * row.hbyc + vars[1] + vars[2] + vars[3]))) / row.cfraction) * (
                                                                                       -1 / row.cfraction) * (
                                                                                       -1 * vars[0]) / ((
                    vars[0] + vars[0] * row.hbyc + vars[1] + vars[2] + vars[3])) ** 2)
        grad3 = 100. * (np.sign((row.mass - (
                    vars[0] * (12.011 + 1.008 * row.hbyc) + vars[1] * 32.065 + vars[2] * 15.9999 + vars[
                3] * 14.0067)) / row.mass) * (-1 / row.mass) * 14.0067) + (np.sign((row.nhc25 - (
                    vars[0] * (420 + 100.8 * row.hbyc) + vars[1] * 340.2 - vars[2] * 180.6 - vars[
                3] * 21)) / row.nhc25) * (1 / row.nhc25) * 21) + 100 * (np.sign((row.cfraction - (
                    vars[0] / (vars[0] + vars[0] * row.hbyc + vars[1] + vars[2] + vars[3]))) / row.cfraction) * (
                                                                                    -1 / row.cfraction) * (
                                                                                    -1 * vars[0]) / ((
                    vars[0] + vars[0] * row.hbyc + vars[1] + vars[2] + vars[3])) ** 2)
        return [grad0, grad1, grad2, grad3]

    def _alpha(self, row, vars, grad):
        alphas = [1.000000000000000000000, 0.700000000000000000000, 0.329173118055010000000, 0.203103455175091000000,
                  0.120153241392875000000, 0.071482572280045300000, 0.045628333839522800000, 0.029810791974004500000,
                  0.018558379763264400000, 0.011553314643162600000, 0.010788582916176700000, 0.007462570342551960000,
                  0.005807176078545710000, 0.003615193420952190000, 0.002250598792618160000, 0.001401085456722320000,
                  0.000872230298655383000, 0.000542997352689844000, 0.000459730338027106000, 0.000378794185967992000,
                  0.000235814142792338000, 0.000228360990395213000, 0.000162472704000815000, 0.000151718384971501000,
                  0.000149546794856164000, 0.000146997959686608000, 0.000137267937866327000, 0.000109814350293061000,
                  0.000087851480234449000, 0.000070281184187559200, 0.000042168710512535500, 0.000021084355256267800,
                  0.000014759048679387400, 0.000010331334075571200, 0.000005000000000000000]
        calphas = list()
        for alpha in alphas:
            calphas.append((100. * np.abs((row.mass - ((vars[0] - alpha * grad[0]) * (12.011 + 1.008 * row.hbyc) + (
                        vars[1] - alpha * grad[1]) * 32.065 + (vars[2] - alpha * grad[2]) * 15.9999 + (
                                                                   vars[3] - alpha * grad[
                                                               3]) * 14.0067)) / row.mass)) + (np.abs((row.nhc25 - (
                        (vars[0] - alpha * grad[0]) * (420 + 100.8 * row.hbyc) + (vars[1] - alpha * grad[1]) * 340.2 - (
                            vars[2] - alpha * grad[2]) * 180.6 - (vars[3] - alpha * grad[3]) * 21)) / row.nhc25)) + (
                                       100 * np.abs((row.cfraction - ((vars[0] - alpha * grad[0]) / (
                                           (vars[0] - alpha * grad[0]) + (vars[0] - alpha * grad[0]) * row.hbyc + (
                                               vars[1] - alpha * grad[1]) + (vars[2] - alpha * grad[2]) + (
                                                       vars[3] - alpha * grad[3])))) / row.cfraction)))
        alpha = alphas[calphas.index(min(calphas))]
        mo = (100. * np.abs((row.mass - (
                    vars[0] * (12.011 + 1.008 * row.hbyc) + vars[1] * 32.065 + vars[2] * 15.9999 + vars[
                3] * 14.0067)) / row.mass))
        alpha = 0 if alpha == alphas[-1] else (alphas[0] if mo >= 5 else (
            alpha * 1000. if mo >= 10 ** -3 and alpha <= 10 ** -4 else (
                alpha * mo if mo <= 0.1 and alpha > 0.01 else alpha)))
        return alpha

    def regress(self, row, vars, grad, alpha):
        cmin = ((1. - (15.9562646275333 * np.log(row.name) - 25.6805551275745) * 0.01) * row.mass) / (
                    12.011 + 1.008 * row.hbyc)
        vars = vars - np.array(grad) * alpha
        residual = self.residual(row, vars)
        nvars = self.newVars(row, vars, residual)
        _c = (row.mass / (12.011 + 1.008 * row.hbyc) + 10 ** -5) if nvars[1] < 10 ** -4 and nvars[2] < 10 ** -4 and \
                                                                    nvars[3] < 10 ** -4 else nvars[0]
        _c = cmin if cmin >= _c else _c
        nvars[0] = _c
        return nvars

    def newVars(self, row, vars, residual):
        _c = vars[0] + sum(residual)
        _s = 0 if vars[1] < 0 else vars[1]
        _o = 0 if vars[2] < 0 else vars[2]
        _n = 0 if vars[3] < 0 else vars[3]
        return [_c, _s, _o, _n]

    def residual(self, row, vars):
        _cr = 0
        _sr = (-1. * (vars[1] * 32.065) / (12.011 + 1.008 * row.hbyc)) if vars[1] < 0 else 0
        _or = (-1. * (vars[2] * 15.9999) / (12.011 + 1.008 * row.hbyc)) if vars[2] < 0 else 0
        _nr = (-1. * (vars[3] * 14.0067) / (12.011 + 1.008 * row.hbyc)) if vars[3] < 0 else 0
        return [_cr, _sr, _or, _nr]

    def _sp(self, row):
        m_c = np.mean([row.min_c, row.max_c])
        c_sp = ((row.mass * (1. - (6.86692485698784 * np.log(row.name) - 11.0518843587855) * 1.2 / 100.)) / (
                    12.011 + 1.008 * row.hbyc))
        c_sp = m_c if c_sp < m_c else c_sp
        sp = [c_sp, row.min_s * 2., row.min_o * 2., row.min_n * 2.]
        return sp


class CalculatedValues(object):
    def __init__(self, parent, ov):
        self.__dict__ = parent.__dict__.copy()
        self._prepare(ov)

    def _prepare(self, ov):
        df = ov.df.copy(deep=True)
        self.fixVars(df, ov.df)
        df["H"] = (df.mass - df.C * 12.011 - df.S * 32.065 - df.O * 15.9999 - df.N * 14.0067) / 1.008
        df["cfraction"] = df.C / (df.S + df.O + df.N + df.H)
        df["hbyc"] = df.H / df.C
        df["bp"] = df.apply(lambda row: row.bp if row.name <= 6 else (
                    47.6112866933562 * row.mass ** 0.509665090106662 * row.cfraction ** 0.26807533679463), axis=1)
        df["nhc25"] = df.C * (420 + 0.24 * 420 * df.hbyc) + 340.2 * df.S - 180.6 * df.O - 21 * df.N
        df["lbmv"] = df.lbmv - (25.6 * df.S + 7.4 * df.O + 11.25 * df.N)
        df["mass_hc"] = 12.011 * df.C + 1.008 * df.H
        df["cfraction_hc"] = df.C / (df.C + df.H)
        df["bp_hc"] = df.apply(lambda row: row.bp if row.name <= 6 else (
                    47.6112866933562 * row.mass_hc ** 0.509665090106662 * row.cfraction_hc ** 0.26807533679463), axis=1)
        self.d20_hc(df)
        df["d25_hc"] = ((df.d20_hc * 1000. * np.exp((-1. * (613.97226 / (df.d20_hc * 1000.) ** 2) * (25. - 20.) * (
                    1. + 0.8 * (613.97226 / (df.d20_hc * 1000.) ** 2) * (25. - 20.))))) / 1000.)
        df["R_20_hc"] = df.apply(lambda row: self._refractivity(row), axis=1)
        df["ri_hc"] = np.sqrt((1. + 2. * df.R_20_hc) / (1. - df.R_20_hc))
        df["mbynd20_hc"] = df.mass_hc / df.ri_hc
        df["rll_hc"] = df.mass_hc / df.d20_hc * df.R_20_hc
        df["mnd20_hc"] = df.mass_hc * df.ri_hc
        df["st_hc"] = 34.39 * (((df.d20_hc + 0.1674) ** 14) / df.d20_hc) ** (1. / 8.) - 7.509
        df["para_hc"] = (df.st_hc ** 0.25) * (df.mass_hc / df.d20_hc)
        df["d20_por"] = df.apply(lambda row: self._d20_por(row), axis=1)
        df["d20_ali"] = df.apply(lambda row: self._d20_ali(row), axis=1)
        df["d20_nap"] = df.apply(lambda row: self._d20_nap(row), axis=1)
        df["v25"] = df.mass_hc / df.d25_hc
        df["mbynd20_dep"] = df.apply(
            lambda row: 0 if (row.mbynd20_hc - (9.5013636627887 * row.C + 5.8626784355479)) > 0 else (
                        row.mbynd20_hc - (9.5013636627887 * row.C + 5.8626784355479)), axis=1)
        self.df = df

    def _d20_nap(self, row):
        if row.C <= 15:
            return (
                        -0.00012079625145045 * row.C ** 4 + 0.00519493513767344 * row.C ** 3 - 0.0808917600746053 * row.C ** 2 + 0.554624548015966 * row.C - 0.579097973264385)
        else:
            return (0.173440291796096 * np.log(row.C) + 0.487535073065162)

    def _d20_ali(self, row):
        if row.C <= 30:
            return (
                        2.8868630945E-10 * row.C ** 6 + 2.0729620662553E-07 * row.C ** 5 - 0.0000225661467711281 * row.C ** 4 + 0.000901638009900008 * row.C ** 3 - 0.0173333070960121 * row.C ** 2 + 0.166058512570425 * row.C + 0.115680916261106)
        else:
            return (0.0604102877761363 * np.log(row.C) + 0.606186312956607)

    def _d20_por(self, row):
        if row.C <= 6:
            return 0.867
        elif row.C <= 15:
            return (-0.0003 * row.C ** 3 + 0.0149 * row.C ** 2 - 0.1661 * row.C + 1.3977 + 0.004179)
        else:
            return (0.300637954533737 * np.log(row.C) + 0.426327227650635)

    def _refractivity(self, row):
        if row.d20_hc <= 0.65:
            return (-0.771988205690377 * row.d20_hc ** 2 + 1.25683786884901 * row.d20_hc - 0.263582391020546)
        elif row.d20_hc <= 0.685:
            return (0.323066708963071 * row.d20_hc ** 0.825367199519558)
        elif row.d20_hc <= 0.694:
            return (9.33747573594155 * row.d20_hc ** 2 - 12.6643155256813 * row.d20_hc + 4.52953399612808)
        elif row.d20_hc <= 0.7:
            return (45.2446673429513 * row.d20_hc ** 2 - 62.3150182598514 * row.d20_hc + 21.6929685696088)
        elif row.d20_hc <= 0.74:
            return (0.310905890442151 * row.d20_hc ** 0.697222127007676)
        elif row.d20_hc <= 0.9:
            return (0.131843282949603 * row.d20_hc ** 2 + 0.100833492150919 * row.d20_hc + 0.100895494309563 + 0.004)
        elif row.d20_hc <= 1.17:
            return (0.280866083905693 * row.d20_hc ** 2 - 0.209842167791342 * row.d20_hc + 0.254487058045577 + 0.009)
        elif row.d20_hc <= 2.8:
            return (-0.126906413488938 * row.d20_hc ** 2 + 0.651119502049113 * row.d20_hc - 0.185838323984245)
        else:
            return (0.247678193996086 * np.log(row.d20_hc) + 0.375251277321885 + 0.012580930886738)

    def fixVars(self, df, obs):
        df["S"] = df.apply(lambda row: row.min_s if row.S < row.min_s else row.S, axis=1)
        df["O"] = df.apply(lambda row: row.min_o if row.O < row.min_o else row.O, axis=1)
        df["N"] = df.apply(lambda row: row.min_n if row.N < row.min_n else row.N, axis=1)
        df["C"] = obs.C - ((df.S - obs.S) * 32.065 - (df.O - obs.O) * 15.9999 - (df.N - obs.N) * 14.0067) / 12.011

    def d20_hc(self, df_o):
        df = df_o.copy(deep=True)
        df["wf_s"] = df.apply(lambda row: 0 if row.S == 0 else (row.S * 32.065) / row.mass, axis=1)
        df["wf_o"] = df.apply(lambda row: 0 if row.O == 0 else (row.O * 15.9999) / row.mass, axis=1)
        df["wf_n"] = df.apply(lambda row: 0 if row.N == 0 else (row.N * 14.0067) / row.mass, axis=1)
        df["wf_hc"] = 1. - (df.wf_s + df.wf_n + df.wf_o)
        df["min_d20_hc"] = df.apply(
            lambda row: (-8.82519803114407E-06 * row.C ** 2 + 0.00331822386370742 * row.C + 0.977013330741513) * ((
                                                                                                                              2.8868630945E-10 * row.C ** 6 + 2.0729620662553E-07 * row.C ** 5 - 0.0000225661467711281 * row.C ** 4 + 0.000901638009900008 * row.C ** 3 - 0.0173333070960121 * row.C ** 2 + 0.166058512570425 * row.C + 0.115680916261106) if row.C <= 30 else (
                        0.0604102877761363 * np.log(row.C) + 0.606186312956607)) if row.C > 10 \
                else (
                        2.8868630945E-10 * row.C ** 6 + 2.0729620662553E-07 * row.C ** 5 - 0.0000225661467711281 * row.C ** 4 + 0.000901638009900008 * row.C ** 3 - 0.0173333070960121 * row.C ** 2 + 0.166058512570425 * row.C + 0.115680916261106) \
            , axis=1)
        df["d20_hc"] = df.apply(lambda row: row.d20 if row.C <= 6 else ((row.min_d20_hc * 1.06) if (row.wf_hc / (
                    1. / row.d20 - row.wf_s / 2.5 - row.wf_o / 1.62 - row.wf_n / 1.542)) < row.min_d20_hc else (
                    row.wf_hc / (1. / row.d20 - row.wf_s / 2.5 - row.wf_o / 1.62 - row.wf_n / 1.542))), axis=1)
        df["d20_wc"] = df.apply(lambda row: row.d20 if row.C <= 6 else (
            (1. / (row.wf_hc / row.min_d20_hc + row.wf_s / 2.5 + row.wf_o / 1.62 + row.wf_n / 1.542)) if row.d20 < (
                        1. / (
                            row.wf_hc / row.min_d20_hc + row.wf_s / 2.5 + row.wf_o / 1.62 + row.wf_n / 1.542)) else row.d20),
                                axis=1)
        df_o[["d20_hc", "d20_wc"]] = df[["d20_hc", "d20_wc"]]


class ObservedValues(object):
    def __init__(self, parent):
        self.__dict__ = parent.__dict__.copy()
        self._prepare()

    def _prepare(self):
        df = self.getMassDensityBpRi()
        df["d60"] = df.density * 0.999016
        df["d20"] = (df.d60 * 1000. * np.exp((-1. * 613.97226 / (df.d60 * 1000.) ** 2 * (20. - 15.) * (
                    1. + 0.8 * 613.97226 / (df.d60 * 1000.) ** 2 * (20. - 15.))))) / 1000.
        df["d25"] = (df.d20 * 1000. * np.exp((-1. * 613.97226 / (df.d20 * 1000.) ** 2 * (25. - 20.) * (
                    1. + 0.8 * 613.97226 / (df.d20 * 1000.) ** 2 * (25. - 20.))))) / 1000.
        df["v25"] = df.mass / df.d25
        df["v60"] = df.mass / df.d60
        df["rll20"] = df.R * df.mass / df.d20
        df["ri20"] = np.sqrt((1. + 2. * df.R) / (1. - df.R))
        df["mnd20"] = df.mass * df.ri20
        df["st"] = 34.39 * (((df.d20 + 0.1674) ** 14) / df.d20) ** (1. / 8.) - 7.509
        df["para"] = (df.st ** 0.25) * (df.mass / df.d20)
        df["api"] = df.apply(
            lambda row: ((row.density / 1.43) ** (-1. / 0.15)) if row.density > 1. else (141.5 / row.density - 131.5),
            axis=1)
        df["vc_parachor"] = df.para / df.apply(
            lambda row: 0.72 if (-0.0257075535439389 * np.log(row.density * 0.999016) + 0.747511293729167) < 0.72 else (
                        -0.0257075535439389 * np.log(row.density * 0.999016) + 0.747511293729167), axis=1)
        df["lbmv"] = 0.3445 * df.vc_parachor ** 1.0135
        self.nitrogen(df)
        self.oxygen(df)
        self.sulfur(df)
        self.carbon(df)
        self.hbyc(df)
        self.nhc25(df)
        for col in self._nvar:
            df[col] = np.nan
        df["seq"] = range(df.shape[0])
        self.df = df

    def nitrogen(self, df):
        df["min_n"] = ((df.apply(
            lambda row: 0 if row.bp < (218. + 273.15) else (2.03651477044276 * np.exp(-0.153706703092679 * row.api)),
            axis=1) * df.mass / 100.) / 14.0067) * 0.08
        df["max_n"] = ((df.apply(
            lambda row: 0 if row.bp < (218. + 273.15) else (2.45276763820172 * np.exp(-0.0813879816568074 * row.api)),
            axis=1) * df.mass / 100.) / 14.0067) * 3.

    def oxygen(self, df):
        df["min_o"] = df.min_n * 1.05
        df["max_o"] = df.max_n * 3.

    def sulfur(self, df):
        df["min_s"] = ((df.apply(
            lambda row: 0 if row.bp < (218. + 273.15) else (10.8957731840327 * np.exp(-0.225159084234834 * row.api)),
            axis=1) * df.mass / 100.) / 32.065) * 0.1
        df["max_s"] = ((df.apply(
            lambda row: 0 if row.bp < (218. + 273.15) else (13.2848517480824 * np.exp(-0.0799729918929779 * row.api)),
            axis=1) * df.mass / 100.) / 32.065) * 6

    def carbon(self, df):
        df["min_c"] = (df.index - df.index * (df.apply(
            lambda row: 30. if row.name >= 120 else (3.83202829994525 * np.exp(0.0175863848593195 * row.name)),
            axis=1) / 100.)) * 0.97
        df["max_c"] = (df.index + df.index * (df.apply(
            lambda row: 40. if row.name >= 120 else (3.83202829994525 * np.exp(0.0175863848593195 * row.name)),
            axis=1) / 100. + 0.1)) * 0.95

    def hbyc(self, df):
        df["temp"] = (df.bp / (47.6112866933562 * df.mass ** 0.509665090106662)) ** (1. / 0.26807533679463)
        df["cfraction"] = df.apply(lambda row: 0.005 if row.temp < 0.005 else (0.99 if row.temp > 0.99 else row.temp),
                                   axis=1)
        df["hbyc_ar2"] = 11.9147 / (3.4707 * (
            np.exp(1.485 * 10 ** -2 * df.bp + 16.94 * df.density - 1.2492 * 10 ** -2 * df.bp * df.density)) * df.bp ** (
                                        -2.725) * df.density ** (-6.798))
        df["temp"] = (-0.0238805970149254 * df.index + 2.08955223880597 + 0.029850746268657)
        df["max_hbyc"] = (2. * df.index + df.apply(lambda row: 1. if row.temp < 1. else row.temp, axis=1)) / df.index
        df["hbyc"] = df.apply(lambda row: row.hbyc_ar2 if row.name <= 9 else \
            ((
                         -74.8366274408181 * row.cfraction ** 3 + 26.2199898293658 * row.cfraction ** 2 - 0.037645231242048 * row.cfraction + 1.9486415829277) if row.cfraction < 0.22 else \
                 ((0.331284194561323 * row.cfraction ** -1.62832651872397) if row.cfraction <= 0.45 else \
                      ((0.202399471147768 * row.cfraction ** -2.26386197332771) if row.cfraction <= 6. else (
                                  -1.29373735411075 * np.log(row.cfraction) - 0.0120988776134345)))) \
                              , axis=1)
        df["hbyc"] = df.apply(lambda row: row.max_hbyc if row.hbyc > row.max_hbyc else row.hbyc, axis=1)
        df.drop(columns=["temp", "max_hbyc", "hbyc_ar2"], inplace=True)

    def nhc25(self, df_o):
        df = df_o.copy(deep=True)
        df["temp"] = ((-0.216971696050033 * (
                    13.7119980393449 * (1. / df.ri20) ** 0.761356864307216) ** 3 + 6.64961268109592 * (
                                   13.7119980393449 * (1. / df.ri20) ** 0.761356864307216) ** 2 - 67.7531281795701 * (
                                   13.7119980393449 * (1. / df.ri20) ** 0.761356864307216) + 229.487004138441) + (
                                  13.7119980393449 * (1. / df.ri20) ** 0.761356864307216)) * 4.186 * df.mass
        df.loc[df.mass > 300, "temp"] = 0.
        df.drop(df.loc[df.temp == 0].index, inplace=True)
        df["x"] = np.log(df.mass)
        df["y"] = np.log(df.temp)
        df["xiyi"] = df.x * df.y
        df["xi2"] = df.x ** 2
        xi = df.x.sum()
        yi = df.y.sum()
        xiyi = df.xiyi.sum()
        xi2 = df.xi2.sum()
        samples = df.shape[0]
        b = (xi * xiyi - yi * xi2) / (xi ** 2 - samples * xi2)
        a = (yi - samples * b) / xi
        df_o.loc[df_o.mass <= 300, "nhc25"] = df.temp
        df_o.loc[df_o.mass > 300, "nhc25"] = (np.exp(a * np.log(df_o.loc[df_o.mass > 300].mass) + b))

    def getMassDensityBpRi(self):
        df, dfplus_o = validated_data(self.args.collection, self.logger)
        df.drop(df.loc[df.index > 5].index, inplace=True)
        df.drop(columns=["expbp", "expri", "scn"], inplace=True)
        df.rename(columns={'expmass': 'mass', "expdensity": "density"}, inplace=True)
        dfmass, dfplusmass = recommended(self.args.collection, self.logger, "mass")
        dfdensity, dfplusdensity = recommended(self.args.collection, self.logger, "density")
        dfbp = recommended(self.args.collection, self.logger, "bp")
        dfri = recommended(self.args.collection, self.logger, "ri")
        dfmass_bkp = dfmass.copy(deep=True)
        dfmass.drop(dfmass.loc[dfmass.index <= 5].index, inplace=True)
        dfdensity.drop(dfdensity.loc[dfdensity.index <= 5].index, inplace=True)
        df = pd.concat([df, dfmass], sort=False)
        df.loc[df.index > 5, "density"] = dfdensity["density"]
        df["mass"] = dfmass_bkp["mass"]
        df = self.makePlus(df, dfbp)
        df["bp"] = dfbp["bp"]
        df["R"] = dfri["ri"]  # self is refractivity at 20 deg c
        df.drop(df.loc[df.index < 5].index, inplace=True)
        return df

    def makePlus(self, df_o, dfbp):
        df = df_o.copy(deep=True)
        bp_m = dfbp.index.max()
        m_m = df.index.max()
        if bp_m < m_m:
            df.drop(df.loc[df.index > bp_m].index, inplace=True)
            df.loc[df.index == bp_m, "mf"] = df_o.loc[df_o.index >= bp_m, "mf"].sum()
            df_o["zimi"] = df_o.mass * df_o.mf
            df.loc[df.index == bp_m, "mass"] = df_o.loc[df_o.index >= bp_m, "zimi"].sum() / df_o.loc[
                df_o.index >= bp_m, "mf"].sum()
            df_o["zimibysgi"] = df_o.zimi / df_o.density
            df.loc[df.index == bp_m, "density"] = df_o.loc[df_o.index >= bp_m, "zimi"].sum() / df_o.loc[
                df_o.index >= bp_m, "zimibysgi"].sum()
        return df
