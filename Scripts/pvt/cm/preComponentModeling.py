import numpy as np
import pandas as pd


class preComponentModeling(object):
    def __init__(self, parent):
        self.__dict__ = parent.__dict__.copy()

    def optimize(self, sno):
        self._nvar = ["Ali_wf", "Nap_wf", "Ar_wf", "Nap5_wf"]
        ov = ObservedValues(self, sno)
        for index, row in ov.df.iterrows():
            vars = self._solver(row)
            for i in range(len(vars)):
                ov.df.loc[ov.df.seq == row.seq, self._nvar[i]] = vars[i]
        cv = CalculatedValues(self, ov, sno)
        self.ov = ov.df
        self.cv = cv.df

    def _solver(self, row):
        vars = self._sp(row)
        self.logger.info("PCM tuning for scn = %s | component = %s | start point = %s" % (
        row.name, row.component, ",".join([str(x) for x in vars])))
        _max = 1
        while _max <= 200:
            cost = self._cost(row, vars)
            grad = self._grad(row, vars, cost)
            alpha, mcost = self._alpha(row, vars, grad)
            nvars = self.regress(row, vars, grad, alpha)
            if self._converged(row, vars, mcost): break
            vars = nvars
            _max += 1
        self.logger.info("PCM tuning for scn = %s | component = %s | end point = %s | iterations = %s" % (
        row.name, row.component, ",".join([str(x) for x in vars]), _max - 1))
        return vars

    def _cost(self, row, vars):
        c = self._c(row, vars)
        d20_po, d20_nap, d20_ali, d20 = self._d20(row, vars, c)
        h = self._h(row, vars)
        hbyc = 0 if h == 0 or c == 0 else h / c
        para = self._para(row, vars)
        v25 = self._v25(row, vars)
        rll = self._rll(row, vars)
        mnd20 = self._mnd20(row, vars)
        lbmv = self._lbmv(row, vars)
        mbynd20_dep = self._mbynd20_dep(row, vars)
        wf = vars[0] + vars[1] + vars[2]
        cost = pd.Series(data=[d20, c, h, hbyc, para, v25, rll, mnd20, lbmv, mbynd20_dep, wf, d20_po, d20_nap, d20_ali],
                         index=["d20", "c", "h", "hbyc", "para", "v25", "rll", "mnd20", "lbmv", "mbynd20_dep", "wf",
                                "d20_por", "d20_nap", "d20_ali"])
        return cost

    def _objective(self, row, vars):
        cost = self._cost(row, vars)
        d20 = 300. * ((row.d20_hc - cost.d20) / row.d20_hc) ** 2
        hbyc = 10. * ((row.hbyc - cost.hbyc) / row.hbyc) ** 2
        par = 50. * ((row.para_hc - cost.para) / row.para_hc) ** 2
        v25 = 10. * ((row.v25_hc - cost.v25) / row.v25_hc) ** 2
        rll = 100. * ((row.rll_hc - cost.rll) / row.rll_hc) ** 2
        mnd20 = 100. * ((row.mnd20_hc - cost.mnd20) / row.mnd20_hc) ** 2
        c = 100. * ((row.C - cost.c) / row.C) ** 2
        lbmv = 500. * ((row.lbmv - cost.lbmv) / row.lbmv) ** 2
        mbynd20_dep = 500. * (
            0. if row.mbynd20_dep >= 0 else ((row.mbynd20_dep - cost.mbynd20_dep) / row.mbynd20_dep) ** 2)
        wf = 500. * (1. - cost.wf) ** 2
        return d20 + hbyc + par + v25 + rll + mnd20 + c + lbmv + mbynd20_dep + wf

    def _mbynd20_dep(self, row, vars):
        mbynd20_dep_ar = (-0.138691077327068 * (vars[2] * row.mass_hc)) if (vars[2] * row.mass_hc) <= 78.114 else ( \
            (-2.5767E-16 * (vars[2] * row.mass_hc) ** 6 + 1.25570849E-12 * (
                        vars[2] * row.mass_hc) ** 5 - 2.33339465412E-09 * (
                         vars[2] * row.mass_hc) ** 4 + 2.05922029150513E-06 * (
                         vars[2] * row.mass_hc) ** 3 - 0.000903990644090139 * (
                         vars[2] * row.mass_hc) ** 2 - 0.15864057854991 * (
                         vars[2] * row.mass_hc) + 10.4586452565478) if (vars[2] * row.mass_hc) <= 1350 else (
                        -0.0000112642961817011 * (vars[2] * row.mass_hc) ** 2 - 0.362846148407561 * (
                            vars[2] * row.mass_hc) + 45.8178034639216) \
            )
        mbynd20_dep_nap = ((-0.0499691144951728 * (row.mass_hc * vars[1] * vars[3])) if (row.mass_hc * vars[1] * vars[
            3]) <= 70.135 else ( \
            (6.64124123196186E-06 * (row.mass_hc * vars[1] * vars[3]) ** 3 - 0.00163014843927023 * (
                        row.mass_hc * vars[1] * vars[3]) ** 2 + 0.0316936237605425 * (
                         row.mass_hc * vars[1] * vars[3]) - 1.15107923E-12) if (row.mass_hc * vars[1] * vars[
                3]) <= 150 else (-0.0139845478076861 * (
                0 if (row.mass_hc * vars[1] * vars[3]) < 0 else (row.mass_hc * vars[1] * vars[3])) ** 1.30734582329863)
        )) + \
                          (-0.0000916000485151024 * (row.mass_hc * vars[1] - row.mass_hc * vars[1] * vars[
                              3]) ** 2 - 0.0381575593603518 * (
                                       row.mass_hc * vars[1] - row.mass_hc * vars[1] * vars[3])) if (row.mass_hc * vars[
            1] - row.mass_hc * vars[1] * vars[3]) <= 220 else (-0.0118553726879464 * (
            0 if (row.mass_hc * vars[1] - row.mass_hc * vars[1] * vars[3]) < 0 else (
                        row.mass_hc * vars[1] - row.mass_hc * vars[1] * vars[3])) ** 1.31929385202031)
        return mbynd20_dep_ar + mbynd20_dep_nap

    def _lbmv(self, row, vars):
        lbmv_ar = (1.22897303940395 * (vars[2] * row.mass_hc)) if (vars[2] * row.mass_hc) <= 78.114 else ( \
            (-0.0000000000000003456 * (vars[2] * row.mass_hc) ** 6 + 1.88579496E-12 * (
                        vars[2] * row.mass_hc) ** 5 - 4.09056994439E-09 * (
                         vars[2] * row.mass_hc) ** 4 + 4.48623820651704E-06 * (
                         vars[2] * row.mass_hc) ** 3 - 0.00262651390002428 * (
                         vars[2] * row.mass_hc) ** 2 + 1.50855371168788 * (
                         vars[2] * row.mass_hc) - 8.75690977467315) if (vars[2] * row.mass_hc) <= 1350 else (
                        1.61128548674228 * (
                    0 if (vars[2] * row.mass_hc) <= 0 else (vars[2] * row.mass_hc)) ** 0.899697806202926) \
            )
        lbmv_nap = ((0.0000137764595451451 * (row.mass_hc * vars[1] * vars[3]) ** 3 - 0.00455449752561954 * (
                    row.mass_hc * vars[1] * vars[3]) ** 2 + 1.67035692759237 * (
                                 row.mass_hc * vars[1] * vars[3]) - 2.626165951E-11) if (row.mass_hc * vars[1] * vars[
            3]) <= 150 else ( \
            (- 8.43E-18 * (row.mass_hc * vars[1] * vars[3]) ** 3 + 7.6452E-16 * (
                        row.mass_hc * vars[1] * vars[3]) ** 2 + 1.19056533134165 * (
                         row.mass_hc * vars[1] * vars[3]) + 15.9997004846206) if (row.mass_hc * vars[1] * vars[
                3]) <= 950 else (1.34009804098614 * (0 if (row.mass_hc * vars[1] * vars[3]) <= 0 else (
                        row.mass_hc * vars[1] * vars[3])) ** 0.984803528800257) \
            )) + \
                   ((-2.699290111937E-08 * (
                               row.mass_hc * vars[1] - row.mass_hc * vars[1] * vars[3]) ** 4 + 0.0000178477062199311 * (
                                 row.mass_hc * vars[1] - row.mass_hc * vars[1] * vars[3]) ** 3 - 0.00422788943328101 * (
                                 row.mass_hc * vars[1] - row.mass_hc * vars[1] * vars[3]) ** 2 + 1.64993385270191 * (
                                 row.mass_hc * vars[1] - row.mass_hc * vars[1] * vars[3]) - 2.74712874671E-09) if (
                                                                                                                              row.mass_hc *
                                                                                                                              vars[
                                                                                                                                  1] - row.mass_hc *
                                                                                                                              vars[
                                                                                                                                  1] *
                                                                                                                              vars[
                                                                                                                                  3]) <= 245 else ( \
                       (-8.0182407936E-10 * (row.mass_hc * vars[1] - row.mass_hc * vars[1] * vars[
                           3]) ** 3 - 1.93901517697986E-06 * (
                                    row.mass_hc * vars[1] - row.mass_hc * vars[1] * vars[3]) ** 2 + 1.23418932687637 * (
                                    row.mass_hc * vars[1] - row.mass_hc * vars[1] * vars[3]) + 11.2968615678507) if (
                                                                                                                                row.mass_hc *
                                                                                                                                vars[
                                                                                                                                    1] - row.mass_hc *
                                                                                                                                vars[
                                                                                                                                    1] *
                                                                                                                                vars[
                                                                                                                                    3]) <= 1350 else (
                                   1.22753826813575 * (
                                       row.mass_hc * vars[1] - row.mass_hc * vars[1] * vars[3]) + 14.8879242773457) \
                       ))
        lbmv_ali = ((row.mass_hc * vars[0]) * 1.58266200898268 + 2.10467669494546 * row.factor_f)
        return lbmv_ar + lbmv_nap + lbmv_ali

    def _mnd20(self, row, vars):
        mnd20_ar = (1.50112 * (vars[2] * row.mass_hc)) if (vars[2] * row.mass_hc) <= 78.114 else ( \
            (-5.84206450751114E-06 * (vars[2] * row.mass_hc) ** 3 + 0.00751213531409437 * (
                        vars[2] * row.mass_hc) ** 2 + 0.144073628503315 * (
                         vars[2] * row.mass_hc) + 68.4532599149825) if (vars[2] * row.mass_hc) <= 500 else ( \
                (-8.41693144913E-09 * (vars[2] * row.mass_hc) ** 3 + 0.000161108611351861 * (
                            vars[2] * row.mass_hc) ** 2 + 2.12248219255403 * (
                             vars[2] * row.mass_hc) - 63.9862717815407) if (vars[2] * row.mass_hc) <= 1350 else (
                            1.18286661434459 * (
                        0 if (vars[2] * row.mass_hc) <= 0 else (vars[2] * row.mass_hc)) ** 1.09094887407156) \
                )
        )
        mnd20_nap = (1.42058467041269 * (row.mass_hc * vars[1])) if (row.mass_hc * vars[1]) <= 70.135 else ( \
            (1.447284293541 * (row.mass_hc * vars[1]) - 0.37610425029041) if (row.mass_hc * vars[1]) <= 150 else ( \
                (-6.624138640713E-08 * (row.mass_hc * vars[1]) ** 3 + 0.000275943845907455 * (
                            row.mass_hc * vars[1]) ** 2 + 1.60710605833917 * (
                             row.mass_hc * vars[1]) - 21.4347121998645) if (row.mass_hc * vars[1]) <= 1350 else (
                            1.02283899923738 * (
                        0 if (row.mass_hc * vars[1]) <= 0 else (row.mass_hc * vars[1])) ** 1.0815776154784) \
                )
        )
        mnd20_ali = ((row.mass_hc * vars[0]) * 1.47073501105012 - 4.04250089113852 * row.factor_f)
        return mnd20_ar + mnd20_nap + mnd20_ali

    def _rll(self, row, vars):
        rll_ar = (0.335239449951955 * (vars[2] * row.mass_hc)) if (vars[2] * row.mass_hc) <= 78.114 else ( \
            (-1.87116097323482E-06 * (vars[2] * row.mass_hc) ** 3 + 0.0018792988594211 * (
                        vars[2] * row.mass_hc) ** 2 - 0.0939803177037452 * (
                         vars[2] * row.mass_hc) + 24.2899661854656) if (vars[2] * row.mass_hc) <= 500 else ( \
                (2.452083914396E-08 * (vars[2] * row.mass_hc) ** 3 - 0.000079209630494905 * (
                            vars[2] * row.mass_hc) ** 2 + 0.381092016734914 * (
                             vars[2] * row.mass_hc) - 3.73279981787174) if (vars[2] * row.mass_hc) <= 1350 else (
                            0.528496537381154 * (
                        0 if (vars[2] * row.mass_hc) <= 0 else (vars[2] * row.mass_hc)) ** 0.928884019445315) \
                )
        )
        rll_nap = (0.339821860964231 * (row.mass_hc * vars[1])) if (row.mass_hc * vars[1]) <= 70.135 else ( \
            (0.33771553289526 * (row.mass_hc * vars[1]) + 0.0306062107298265) if (row.mass_hc * vars[1]) <= 150 else ( \
                (-2.293E-17 * (row.mass_hc * vars[1]) ** 6 + 1.0414983E-13 * (
                            row.mass_hc * vars[1]) ** 5 - 1.5843345177E-10 * (
                             row.mass_hc * vars[1]) ** 4 + 6.998936592203E-08 * (
                             row.mass_hc * vars[1]) ** 3 + 0.0000329446336797695 * (
                             row.mass_hc * vars[1]) ** 2 + 0.316604983947332 * (
                             row.mass_hc * vars[1]) + 1.74679158038743) if (row.mass_hc * vars[1]) <= 1350 else (
                            0.366228631794776 * (
                        0 if (row.mass_hc * vars[1]) <= 0 else (row.mass_hc * vars[1])) ** 0.991034715216667) \
                )
        )
        rll_ali = ((row.mass_hc * vars[0]) * 0.329222214300991 + 0.768144007984601 * row.factor_f)
        return rll_ar + rll_nap + rll_ali

    def _v25(self, row, vars):
        v_ar = (1.14219998676821 * (vars[2] * row.mass_hc)) if (vars[2] * row.mass_hc) <= 78.114 else ( \
            (-3.0818872804607E-07 * (vars[2] * row.mass_hc) ** 3 + 0.000157473969023783 * (
                        vars[2] * row.mass_hc) ** 2 + 0.562071271755067 * (
                         vars[2] * row.mass_hc) + 45.1777143670648) if (vars[2] * row.mass_hc) <= 500 else ( \
                (-1.336E-17 * (vars[2] * row.mass_hc) ** 6 + 8.597456E-14 * (
                            vars[2] * row.mass_hc) ** 5 - 2.3321333134E-10 * (
                             vars[2] * row.mass_hc) ** 4 + 3.5394846103436E-07 * (
                             vars[2] * row.mass_hc) ** 3 - 0.000354659417799298 * (
                             vars[2] * row.mass_hc) ** 2 + 0.702633249568294 * (
                             vars[2] * row.mass_hc) + 30.8475817662467) if (vars[2] * row.mass_hc) <= 1350 else (
                            1.78553362642803 * (
                        0 if (vars[2] * row.mass_hc) <= 0 else (vars[2] * row.mass_hc)) ** 0.835009655244618) \
                )
        )
        v_nap = (1.34866139998192 * (row.mass_hc * vars[1])) if (row.mass_hc * vars[1]) <= 70.135 else ( \
            (1.28366284131307 * (row.mass_hc * vars[1]) + 0.651255737588963) if (row.mass_hc * vars[1]) <= 150 else ( \
                (-3.280281826E-11 * (row.mass_hc * vars[1]) ** 4 + 1.5036964504228E-07 * (
                            row.mass_hc * vars[1]) ** 3 - 0.000287245765992295 * (
                             row.mass_hc * vars[1]) ** 2 + 0.946749304356398 * (
                             row.mass_hc * vars[1]) + 31.5857375708661) if (row.mass_hc * vars[1]) <= 1350 else (
                            2.08318821509476 * (
                        0 if (row.mass_hc * vars[1]) <= 0 else (row.mass_hc * vars[1])) ** 0.862908532042399)
            )
        )
        v_ali = (-2.09149718E-12 * (row.mass_hc * vars[0]) ** 6 + 2.56273813802E-09 * (
                    row.mass_hc * vars[0]) ** 5 - 1.23907173033011E-06 * (
                             row.mass_hc * vars[0]) ** 4 + 0.000296187570285156 * (
                             row.mass_hc * vars[0]) ** 3 - 0.0355571041254734 * (
                             row.mass_hc * vars[0]) ** 2 + 3.04701929242583 * (
                             row.mass_hc * vars[0]) + 0.00439327908679843) if (row.mass_hc * vars[0]) <= 325 else ( \
            (7.41289E-15 * (row.mass_hc * vars[0]) ** 5 - 4.954497963E-11 * (
                        row.mass_hc * vars[0]) ** 4 + 1.4086741743278E-07 * (
                         row.mass_hc * vars[0]) ** 3 - 0.00024576304582591 * (
                         row.mass_hc * vars[0]) ** 2 + 1.25190727201385 * (
                         row.mass_hc * vars[0]) + 20.7143141299502) if (row.mass_hc * vars[0]) <= 1350 else (
                        1.87454241096822 * (
                    0 if (row.mass_hc * vars[0]) <= 0 else (row.mass_hc * vars[0])) ** 0.929332334824849) \
            )
        return v_ar + v_nap + v_ali

    def _para(self, row, vars):
        para_ar = (2.62187448054632 * (vars[2] * row.mass_hc)) if (vars[2] * row.mass_hc) <= 78.15 else ( \
            (-1.822524384834E-08 * (vars[2] * row.mass_hc) ** 4 + 0.0000199651937544676 * (
                        vars[2] * row.mass_hc) ** 3 - 0.00688962262335124 * (
                         vars[2] * row.mass_hc) ** 2 + 2.91787913445933 * (
                         vars[2] * row.mass_hc) + 1.99335676321425) if (vars[2] * row.mass_hc) <= 400. else ( \
                (2.2187371E-13 * (vars[2] * row.mass_hc) ** 5 - 1.09452475889E-09 * (
                            vars[2] * row.mass_hc) ** 4 + 2.07146705611516E-06 * (
                             vars[2] * row.mass_hc) ** 3 - 0.00192220203281091 * (
                             vars[2] * row.mass_hc) ** 2 + 2.49255064334514 * (
                             vars[2] * row.mass_hc) + 3.58255134747975) if (vars[2] * row.mass_hc) <= 1350. else (
                            3.50525295532602 * (
                        0 if (vars[2] * row.mass_hc) <= 0 else (vars[2] * row.mass_hc)) ** 0.901185761435326) \
                )
        )
        para_nap = (2.89506991671068 * (row.mass_hc * vars[1])) if (row.mass_hc * vars[1]) <= 70.135 else ( \
            (6.069839E-14 * (row.mass_hc * vars[1]) ** 5 - 0.0000000003134148489 * (
                        row.mass_hc * vars[1]) ** 4 + 6.4959169160542E-07 * (
                         row.mass_hc * vars[1]) ** 3 - 0.000750319943029562 * (
                         row.mass_hc * vars[1]) ** 2 + 2.49938707512647 * (
                         row.mass_hc * vars[1]) + 35.4336466618015) if (row.mass_hc * vars[1]) <= 1350 else (
                        3.82825010509589 * (
                    0 if (row.mass_hc * vars[1]) <= 0 else (row.mass_hc * vars[1])) ** 0.918472389443328) \
            )
        para_ali = ((row.mass_hc * vars[0]) * 2.79461039423968 + row.factor_f * 12.1830327226064)
        return para_ar + para_nap + para_ali

    def _h(self, row, vars):
        h = (row.mass_hc * vars[2] * 0.992063492063492 - 11.9156746031746 * ((-0.0000001354040217456 * (
                    vars[2] * row.mass_hc) ** 3 + 0.0000520658252493034 * (vars[
                                                                               2] * row.mass_hc) ** 2 + 0.0735699529423499 * (
                                                                                          vars[
                                                                                              2] * row.mass_hc) + 1.68398628E-12) if (
                                                                                                                                                 vars[
                                                                                                                                                     2] * row.mass_hc) <= 178 else (
                    0.081366952742896 * (vars[2] * row.mass_hc) - 0.495292073123728)))
        h1 = (row.mass_hc * vars[1] * 0.992063492063492 - 11.9156746031746 * (
            (row.mass_hc * vars[1] * 0.0712910814857061) if (row.mass_hc * vars[1]) <= 85 else ( \
                (row.mass_hc * vars[1] * 0.0712910814857061) if ((row.mass_hc * vars[1] * 0.0712910814857061) >= (
                            0.0681542447698304 * (
                        0 if (row.mass_hc * vars[1]) <= 0 else (row.mass_hc * vars[1])) ** 1.01250029139992)) else (
                            0.0681542447698304 * (
                        0 if (row.mass_hc * vars[1]) <= 0 else (row.mass_hc * vars[1])) ** 1.01250029139992)
            )))
        h2 = (row.mass_hc * vars[0] * 0.142582162971412 + 0.856277179724817 * row.factor_f)
        return h + h1 + h2

    def _d20(self, row, vars, c):
        if c <= 6:
            d20_por = 0.867
        elif c <= 15:
            d20_por = (-0.0003 * c ** 3 + 0.0149 * c ** 2 - 0.1661 * c + 1.3977 + 0.004179)
        else:
            d20_por = (0.300637954533737 * np.log(10 ** -3 if c < 10 ** -3 else np.abs(c)) + 0.426327227650635)
        if c <= 15:
            d20_nap = (
                        -0.00012079625145045 * c ** 4 + 0.00519493513767344 * c ** 3 - 0.0808917600746053 * c ** 2 + 0.554624548015966 * c - 0.579097973264385)
        else:
            d20_nap = (0.173440291796096 * np.log(10 ** -3 if c < 10 ** -3 else np.abs(c)) + 0.487535073065162)
        if c <= 30:
            d20_ali = (
                        2.8868630945E-10 * c ** 6 + 2.0729620662553E-07 * c ** 5 - 0.0000225661467711281 * c ** 4 + 0.000901638009900008 * c ** 3 - 0.0173333070960121 * c ** 2 + 0.166058512570425 * c + 0.115680916261106)
        else:
            d20_ali = (0.0604102877761363 * np.log(10 ** -3 if c < 10 ** -3 else np.abs(c)) + 0.606186312956607)
        d20 = 0 if (vars[0] / d20_ali + vars[1] / d20_nap + vars[
            2] / d20_por) == 0 or d20_ali == 0 or d20_nap == 0 or d20_por == 0 else (
                    1. / (vars[0] / d20_ali + vars[1] / d20_nap + vars[2] / d20_por))
        return d20_por, d20_nap, d20_ali, d20

    def _c(self, row, vars):
        if vars[2] * row.mass_hc <= 178:
            c = (-0.0000001354040217456 * (vars[2] * row.mass_hc) ** 3 + 0.0000520658252493034 * (
                        vars[2] * row.mass_hc) ** 2 + 0.0735699529423499 * (vars[2] * row.mass_hc) + 1.68398628E-12)
        else:
            c = (0.081366952742896 * (vars[2] * row.mass_hc) - 0.495292073123728)
        if row.mass_hc * vars[1] <= 85:
            c += (row.mass_hc * vars[1] * 0.0712910814857061)
        elif ((row.mass_hc * vars[1] * 0.0712910814857061) >= (0.0681542447698304 * (
        0 if (row.mass_hc * vars[1]) <= 0 else (row.mass_hc * vars[1])) ** 1.01250029139992)):
            c += (row.mass_hc * vars[1] * 0.0712910814857061)
        else:
            c += (0.0681542447698304 * (
                0 if (row.mass_hc * vars[1]) <= 0 else (row.mass_hc * vars[1])) ** 1.01250029139992)
        c += ((row.mass_hc * vars[0] - 1.008 * row.factor_f) * 0.0712910814857061)
        return c

    def _grad(self, row, vars, cost):
        grad0 = self._grad0(row, vars, cost)
        grad1 = self._grad1(row, vars, cost)
        grad2 = self._grad2(row, vars, cost)
        grad3 = self._grad3(row, vars, cost)
        return [grad0, grad1, grad2, grad3]

    def _grad0(self, row, vars, cost):
        c1 = (row.mass_hc * 0.0712910814857061)
        h1 = (row.mass_hc * 0.142582162971412)
        d20_por = 0 if cost.c <= 6 else (
            (-0.0003 * 3 * cost.c ** 2 + 0.0149 * 2 * cost.c - 0.1661) if cost.c <= 15 else (
                        0.300637954533737 * 1 / cost.c))
        d20_nap = (
                    -0.00012079625145045 * 4 * cost.c ** 3 + 0.00519493513767344 * 3 * cost.c ** 2 - 0.0808917600746053 * 2 * cost.c + 0.554624548015966) if cost.c <= 15 else (
                    0.173440291796096 * 1 / cost.c)
        d20_ali = (
                    2.8868630945E-10 * 6 * cost.c ** 5 + 2.0729620662553E-07 * 5 * cost.c ** 4 - 0.0000225661467711281 * 4 * cost.c ** 3 + 0.000901638009900008 * 3 * cost.c ** 2 - 0.0173333070960121 * 2 * cost.c + 0.166058512570425) if cost.c <= 30 else (
                    0.0604102877761363 * 1 / cost.c)
        d20 = 300. * (2. * (row.d20_hc - cost.d20) / row.d20_hc ** 2) * (-1.) * ((-1.) * (
                    (cost.d20_ali - vars[0] * d20_ali * c1) / cost.d20_ali ** 2 - (
                        vars[1] * d20_nap * c1 / cost.d20_nap ** 2) - (vars[2] * d20_por * c1 / cost.d20_por ** 2)) / (
                                                                                             vars[0] / cost.d20_ali +
                                                                                             vars[1] / cost.d20_nap +
                                                                                             vars[
                                                                                                 2] / cost.d20_por) ** 2)
        c = 100. * (2. * (row.C - cost.c) / row.C ** 2) * (-1.) * (c1)
        hbyc = 10. * (2. * (row.hbyc - cost.hbyc) / row.hbyc ** 2) * (-1.) * ((cost.c * h1 - cost.h * c1) / cost.c ** 2)
        para = 50. * (2. * (row.para_hc - cost.para) / row.para_hc ** 2) * (-1.) * (row.mass_hc * 2.79461039423968)
        v25 = 10. * (2. * (row.v25_hc - cost.v25) / row.v25_hc ** 2) * (-1.) * ((-2.09149718E-12 * 6 * (
                    row.mass_hc * vars[0]) ** 5 + 2.56273813802E-09 * 5 * (row.mass_hc * vars[
            0]) ** 4 - 1.23907173033011E-06 * 4 * (row.mass_hc * vars[0]) ** 3 + 0.000296187570285156 * 3 * (
                                                                                             row.mass_hc * vars[
                                                                                         0]) ** 2 - 0.0355571041254734 * 2 * (
                                                                                             row.mass_hc * vars[
                                                                                         0]) + 3.04701929242583) if (
                                                                                                                                row.mass_hc *
                                                                                                                                vars[
                                                                                                                                    0]) <= 325 else ( \
            (7.41289E-15 * 5 * (row.mass_hc * vars[0]) ** 4 - 4.954497963E-11 * 4 * (
                        row.mass_hc * vars[0]) ** 3 + 1.4086741743278E-07 * 3 * (
                         row.mass_hc * vars[0]) ** 2 - 0.00024576304582591 * 2 * (
                         row.mass_hc * vars[0]) + 1.25190727201385) if (row.mass_hc * vars[0]) <= 1350 else (
                        1.87454241096822 * 0.929332334824849 * (row.mass_hc * vars[0]) ** (0.929332334824849 - 1))
        )) * row.mass_hc
        rll = 100. * (2. * (row.rll_hc - cost.rll) / row.rll_hc ** 2) * (-1.) * (row.mass_hc * 0.329222214300991)
        mnd20 = 100. * (2. * (row.mnd20_hc - cost.mnd20) / row.mnd20_hc ** 2) * (-1.) * (row.mass_hc * 1.47073501105012)
        lbmv = 500. * (2. * (row.lbmv - cost.lbmv) / row.lbmv ** 2) * (-1.) * (row.mass_hc * 1.58266200898268)
        mbynd20_dep = 500. * (
            0 if row.mbynd20_dep >= 0 else (2. * (row.mbynd20_dep - cost.mbynd20_dep) / row.mbynd20_dep ** 2)) * (
                          -1.) * (0)
        wf = 2. * (1. - cost.wf) * (-1.) * 1.
        return d20 + c + hbyc + para + v25 + rll + mnd20 + lbmv + mbynd20_dep + wf

    def _grad1(self, row, vars, cost):
        c1 = ((0.0712910814857061) if (row.mass_hc * vars[1]) <= 85 else ( \
            (0.0712910814857061) if ((row.mass_hc * vars[1] * 0.0712910814857061) >= (0.0681542447698304 * (
                0 if (row.mass_hc * vars[1]) <= 0 else (row.mass_hc * vars[1])) ** 1.01250029139992)) else (
                        0.0681542447698304 * 1.01250029139992 * (row.mass_hc * vars[1]) ** (1.01250029139992 - 1.))
        )) * row.mass_hc
        h1 = (0.992063492063492 - 11.9156746031746 * ((0.0712910814857061) if (row.mass_hc * vars[1]) <= 85 else ( \
            (0.0712910814857061) if ((row.mass_hc * vars[1] * 0.0712910814857061) >= (0.0681542447698304 * (
                0 if (row.mass_hc * vars[1]) <= 0 else (row.mass_hc * vars[1])) ** 1.01250029139992)) else (
                        0.0681542447698304 * 1.01250029139992 * (row.mass_hc * vars[1]) ** (1.01250029139992 - 1.))
        ))) * row.mass_hc
        d20_por = 0 if cost.c <= 6 else (
            (-0.0003 * 3 * cost.c ** 2 + 0.0149 * 2 * cost.c - 0.1661) if cost.c <= 15 else (
                        0.300637954533737 * 1 / cost.c))
        d20_nap = (
                    -0.00012079625145045 * 4 * cost.c ** 3 + 0.00519493513767344 * 3 * cost.c ** 2 - 0.0808917600746053 * 2 * cost.c + 0.554624548015966) if cost.c <= 15 else (
                    0.173440291796096 * 1 / cost.c)
        d20_ali = (
                    2.8868630945E-10 * 6 * cost.c ** 5 + 2.0729620662553E-07 * 5 * cost.c ** 4 - 0.0000225661467711281 * 4 * cost.c ** 3 + 0.000901638009900008 * 3 * cost.c ** 2 - 0.0173333070960121 * 2 * cost.c + 0.166058512570425) if cost.c <= 30 else (
                    0.0604102877761363 * 1 / cost.c)
        d20 = 300. * (2. * (row.d20_hc - cost.d20) / row.d20_hc ** 2) * (-1.) * ((-1.) * (
                    (-1. * vars[0] * d20_ali * c1) / cost.d20_ali ** 2 + (
                        (cost.d20_nap - vars[1] * d20_nap * c1) / cost.d20_nap ** 2) - (
                                vars[2] * d20_por * c1 / cost.d20_por ** 2)) / (vars[0] / cost.d20_ali + vars[
            1] / cost.d20_nap + vars[2] / cost.d20_por) ** 2)
        c = 100. * (2. * (row.C - cost.c) / row.C ** 2) * (-1.) * (c1)
        hbyc = 10. * (2. * (row.hbyc - cost.hbyc) / row.hbyc ** 2) * (-1.) * ((cost.c * h1 - cost.h * c1) / cost.c ** 2)
        para = 50. * (2. * (row.para_hc - cost.para) / row.para_hc ** 2) * (-1.) * (row.mass_hc) * (
            (2.89506991671068) if (row.mass_hc * vars[1]) <= 70.135 else ( \
                (6.069839E-14 * 5 * (row.mass_hc * vars[1]) ** 4 - 0.0000000003134148489 * 4 * (
                            row.mass_hc * vars[1]) ** 3 + 6.4959169160542E-07 * 3 * (
                             row.mass_hc * vars[1]) ** 2 - 0.000750319943029562 * 2 * (
                             row.mass_hc * vars[1]) + 2.49938707512647) if (row.mass_hc * vars[1]) <= 1350 else (
                            3.82825010509589 * 0.918472389443328 * (row.mass_hc * vars[1]) ** (0.918472389443328 - 1.))
            ))
        v25 = 10. * (2. * (row.v25_hc - cost.v25) / row.v25_hc ** 2) * (-1.) * (
            (1.34866139998192) if (row.mass_hc * vars[1]) <= 70.135 else ( \
                (1.28366284131307) if (row.mass_hc * vars[1]) <= 150 else ( \
                    (-3.280281826E-11 * 4 * (row.mass_hc * vars[1]) ** 3 + 1.5036964504228E-07 * 3 * (
                                row.mass_hc * vars[1]) ** 2 - 0.000287245765992295 * 2 * (
                                 row.mass_hc * vars[1]) + 0.946749304356398) if (row.mass_hc * vars[1]) <= 1350 else (
                                2.08318821509476 * 0.862908532042399 * (row.mass_hc * vars[1]) ** (
                                    0.862908532042399 - 1))
                )
            )) * row.mass_hc
        rll = 100. * (2. * (row.rll_hc - cost.rll) / row.rll_hc ** 2) * (-1.) * (row.mass_hc) * (
            (0.339821860964231) if (row.mass_hc * vars[1]) <= 70.135 else ( \
                (0.33771553289526) if (row.mass_hc * vars[1]) <= 150 else ( \
                    (-2.293E-17 * 6 * (row.mass_hc * vars[1]) ** 5 + 1.0414983E-13 * 5 * (
                                row.mass_hc * vars[1]) ** 4 - 1.5843345177E-10 * 4 * (
                                 row.mass_hc * vars[1]) ** 3 + 6.998936592203E-08 * 3 * (
                                 row.mass_hc * vars[1]) ** 2 + 0.0000329446336797695 * 2 * (
                                 row.mass_hc * vars[1]) + 0.316604983947332) if (row.mass_hc * vars[1]) <= 1350 else (
                                0.366228631794776 * 0.991034715216667 * (row.mass_hc * vars[1]) ** (
                                    0.991034715216667 - 1))
                )
            ))
        mnd20 = 100. * (2. * (row.mnd20_hc - cost.mnd20) / row.mnd20_hc ** 2) * (-1.) * (row.mass_hc) * (
            (1.42058467041269) if (row.mass_hc * vars[1]) <= 70.135 else ( \
                (1.447284293541) if (row.mass_hc * vars[1]) <= 150 else ( \
                    (-6.624138640713E-08 * 3 * (row.mass_hc * vars[1]) ** 2 + 0.000275943845907455 * 2 * (
                                row.mass_hc * vars[1]) + 1.60710605833917) if (row.mass_hc * vars[1]) <= 1350 else (
                                1.02283899923738 * 1.0815776154784 * (row.mass_hc * vars[1]) ** (1.0815776154784 - 1))
                )
            ))
        lbmv = 500. * (2. * (row.lbmv - cost.lbmv) / row.lbmv ** 2) * (-1.) * ((((0.0000137764595451451 * 3 * (
                    row.mass_hc * vars[1] * vars[3]) ** 2 - 0.00455449752561954 * 2 * (row.mass_hc * vars[1] * vars[
            3]) + 1.67035692759237) if (row.mass_hc * vars[1] * vars[3]) <= 150 else ( \
            (- 8.43E-18 * 3 * (row.mass_hc * vars[1] * vars[3]) ** 2 + 7.6452E-16 * 2 * (
                        row.mass_hc * vars[1] * vars[3]) + 1.19056533134165) if (row.mass_hc * vars[1] * vars[
                3]) <= 950 else (1.34009804098614 * 0.984803528800257 * (row.mass_hc * vars[1] * vars[3]) ** (
                        0.984803528800257 - 1))
        )) * (row.mass_hc * vars[3]) + ((-2.699290111937E-08 * 4 * (
                    row.mass_hc * vars[1] - row.mass_hc * vars[1] * vars[3]) ** 3 + 0.0000178477062199311 * 3 * (
                                                     row.mass_hc * vars[1] - row.mass_hc * vars[1] * vars[
                                                 3]) ** 2 - 0.00422788943328101 * 2 * (
                                                     row.mass_hc * vars[1] - row.mass_hc * vars[1] * vars[
                                                 3]) + 1.64993385270191) if (row.mass_hc * vars[1] - row.mass_hc * vars[
            1] * vars[3]) <= 245 else ( \
            (-8.0182407936E-10 * 3 * (
                        row.mass_hc * vars[1] - row.mass_hc * vars[1] * vars[3]) ** 2 - 1.93901517697986E-06 * 2 * (
                         row.mass_hc * vars[1] - row.mass_hc * vars[1] * vars[3]) + 1.23418932687637) if (row.mass_hc *
                                                                                                          vars[
                                                                                                              1] - row.mass_hc *
                                                                                                          vars[1] *
                                                                                                          vars[
                                                                                                              3]) <= 1350 else (
                1.22753826813575)
        )) * (row.mass_hc - row.mass_hc * vars[3])
                                                                                ))
        mbynd20_dep = 500. * (
            0 if row.mbynd20_dep >= 0 else (2. * (row.mbynd20_dep - cost.mbynd20_dep) / row.mbynd20_dep ** 2)) * (
                          -1.) * (((-0.0499691144951728) if (row.mass_hc * vars[1] * vars[3]) <= 70.135 else ( \
            (6.64124123196186E-06 * 3 * (row.mass_hc * vars[1] * vars[3]) ** 2 - 0.00163014843927023 * 2 * (
                        row.mass_hc * vars[1] * vars[3]) + 0.0316936237605425) if (row.mass_hc * vars[1] * vars[
                3]) <= 150 else (-0.0139845478076861 * 1.30734582329863 * (row.mass_hc * vars[1] * vars[3]) ** (
                        1.30734582329863 - 1))
        )) * (row.mass_hc * vars[3]) + ((-0.0000916000485151024 * 2 * (
                    row.mass_hc * vars[1] - row.mass_hc * vars[1] * vars[3]) - 0.0381575593603518) if (row.mass_hc *
                                                                                                       vars[
                                                                                                           1] - row.mass_hc *
                                                                                                       vars[1] * vars[
                                                                                                           3]) <= 220 else (
                    -0.0118553726879464 * 1.31929385202031 * (
                0 if (row.mass_hc * vars[1] - row.mass_hc * vars[1] * vars[3]) <= 0 else (
                            row.mass_hc * vars[1] - row.mass_hc * vars[1] * vars[3])) ** (1.31929385202031 - 1))) * (
                                              row.mass_hc - row.mass_hc * vars[3]))
        wf = 2. * (1. - cost.wf) * (-1.) * 1.
        return d20 + c + hbyc + para + v25 + rll + mnd20 + lbmv + mbynd20_dep + wf

    def _grad2(self, row, vars, cost):
        c1 = ((-0.0000001354040217456 * 3 * (vars[2] * row.mass_hc) ** 2 + 0.0000520658252493034 * 2 * (
                    vars[2] * row.mass_hc) + 0.0735699529423499) if (vars[2] * row.mass_hc) <= 178 else (
            0.081366952742896)) * row.mass_hc
        h1 = row.mass_hc * 0.992063492063492 - 11.9156746031746 * ((-0.0000001354040217456 * 3 * row.mass_hc * (
                    vars[2] * row.mass_hc) ** 2 + 0.0000520658252493034 * 2 * row.mass_hc * (vars[
                                                                                                 2] * row.mass_hc) + 0.0735699529423499 * row.mass_hc) if (
                                                                                                                                                                      vars[
                                                                                                                                                                          2] * row.mass_hc) <= 178 else (
                    0.081366952742896 * row.mass_hc))
        d20_por = 0 if cost.c <= 6 else (
            (-0.0003 * 3 * cost.c ** 2 + 0.0149 * 2 * cost.c - 0.1661) if cost.c <= 15 else (
                        0.300637954533737 * 1 / cost.c))
        d20_nap = (
                    -0.00012079625145045 * 4 * cost.c ** 3 + 0.00519493513767344 * 3 * cost.c ** 2 - 0.0808917600746053 * 2 * cost.c + 0.554624548015966) if cost.c <= 15 else (
                    0.173440291796096 * 1 / cost.c)
        d20_ali = (
                    2.8868630945E-10 * 6 * cost.c ** 5 + 2.0729620662553E-07 * 5 * cost.c ** 4 - 0.0000225661467711281 * 4 * cost.c ** 3 + 0.000901638009900008 * 3 * cost.c ** 2 - 0.0173333070960121 * 2 * cost.c + 0.166058512570425) if cost.c <= 30 else (
                    0.0604102877761363 * 1 / cost.c)
        d20 = 300. * (2. * (row.d20_hc - cost.d20) / row.d20_hc ** 2) * (-1.) * (-1.) * (((-1. * vars[
            0] * d20_ali * c1) / cost.d20_ali ** 2 + ((-1. * vars[1] * d20_nap * c1) / cost.d20_nap ** 2) + ((
                                                                                                                         cost.d20_por -
                                                                                                                         vars[
                                                                                                                             2] * d20_por * c1) / cost.d20_por ** 2)) / (
                                                                                                     vars[
                                                                                                         0] / cost.d20_ali +
                                                                                                     vars[
                                                                                                         1] / cost.d20_nap +
                                                                                                     vars[
                                                                                                         2] / cost.d20_por) ** 2)
        c = 100. * (2. * (row.C - cost.c) / row.C ** 2) * (-1.) * (c1)
        hbyc = 10. * (2. * (row.hbyc - cost.hbyc) / row.hbyc ** 2) * (-1.) * ((cost.c * h1 - cost.h * c1) / cost.c ** 2)
        para = 50. * (2. * (row.para_hc - cost.para) / row.para_hc ** 2) * (-1.) * (
            (2.62187448054632) if (vars[2] * row.mass_hc) <= 78.15 else ( \
                (-1.822524384834E-08 * 4 * (vars[2] * row.mass_hc) ** 3 + 0.0000199651937544676 * 3 * (
                            vars[2] * row.mass_hc) ** 2 - 0.00688962262335124 * 2 * (
                             vars[2] * row.mass_hc) + 2.91787913445933) if (vars[2] * row.mass_hc) <= 400 else ( \
                    (2.2187371E-13 * 5 * (vars[2] * row.mass_hc) ** 4 - 1.09452475889E-09 * 4 * (
                                vars[2] * row.mass_hc) ** 3 + 2.07146705611516E-06 * 3 * (
                                 vars[2] * row.mass_hc) ** 2 - 0.00192220203281091 * 2 * (
                                 vars[2] * row.mass_hc) + 2.49255064334514) if (vars[2] * row.mass_hc) <= 1350 else (
                                3.50525295532602 * 0.901185761435326 * (vars[2] * row.mass_hc) ** (
                                    0.901185761435326 - 1))
                )
            )) * row.mass_hc
        v25 = 10. * (2. * (row.v25_hc - cost.v25) / row.v25_hc ** 2) * (-1.) * (
            (1.14219998676821) if (vars[2] * row.mass_hc) <= 78.114 else ( \
                (-3.0818872804607E-07 * 3 * (vars[2] * row.mass_hc) ** 2 + 0.000157473969023783 * 2 * (
                            vars[2] * row.mass_hc) + 0.562071271755067) if (vars[2] * row.mass_hc) <= 500 else ( \
                    (-1.336E-17 * 6 * (vars[2] * row.mass_hc) ** 5 + 8.597456E-14 * 5 * (
                                vars[2] * row.mass_hc) ** 4 - 2.3321333134E-10 * 4 * (
                                 vars[2] * row.mass_hc) ** 3 + 3.5394846103436E-07 * 3 * (
                                 vars[2] * row.mass_hc) ** 2 - 0.000354659417799298 * 2 * (
                                 vars[2] * row.mass_hc) + 0.702633249568294) if (vars[2] * row.mass_hc) <= 1350 else (
                                1.78553362642803 * 0.835009655244618 * (vars[2] * row.mass_hc) ** (
                                    0.835009655244618 - 1))
                )
            )) * row.mass_hc
        rll = 100. * (2. * (row.rll_hc - cost.rll) / row.rll_hc ** 2) * (-1.) * (
            (0.335239449951955) if (vars[2] * row.mass_hc) <= 78.114 else ( \
                (-1.87116097323482E-06 * 3 * (vars[2] * row.mass_hc) ** 2 + 0.0018792988594211 * 2 * (
                            vars[2] * row.mass_hc) - 0.0939803177037452) if (vars[2] * row.mass_hc) <= 500 else ( \
                    (2.452083914396E-08 * 3 * (vars[2] * row.mass_hc) ** 2 - 0.000079209630494905 * 2 * (
                                vars[2] * row.mass_hc) + 0.381092016734914) if (vars[2] * row.mass_hc) <= 1350 else (
                                0.528496537381154 * 0.928884019445315 * (vars[2] * row.mass_hc) ** (
                                    0.928884019445315 - 1))
                )
            )) * row.mass_hc
        mnd20 = 100. * (2. * (row.mnd20_hc - cost.mnd20) / row.mnd20_hc ** 2) * (-1.) * (
            (1.50112) if (vars[2] * row.mass_hc) <= 78.114 else ( \
                (-5.84206450751114E-06 * 3 * (vars[2] * row.mass_hc) ** 2 + 0.00751213531409437 * 2 * (
                            vars[2] * row.mass_hc) + 0.144073628503315) if (vars[2] * row.mass_hc) <= 500 else ( \
                    (-8.41693144913E-09 * 3 * (vars[2] * row.mass_hc) ** 2 + 0.000161108611351861 * 2 * (
                                vars[2] * row.mass_hc) + 2.12248219255403) if (vars[2] * row.mass_hc) <= 1350 else (
                                1.18286661434459 * 1.09094887407156 * (vars[2] * row.mass_hc) ** (1.09094887407156 - 1))
                )
            )) * row.mass_hc
        lbmv = 500. * (2. * (row.lbmv - cost.lbmv) / row.lbmv ** 2) * (-1.) * (
            (1.22897303940395) if (vars[2] * row.mass_hc) <= 78.114 else ( \
                (-0.0000000000000003456 * 6 * (vars[2] * row.mass_hc) ** 5 + 1.88579496E-12 * 5 * (
                            vars[2] * row.mass_hc) ** 4 - 4.09056994439E-09 * 4 * (
                             vars[2] * row.mass_hc) ** 3 + 4.48623820651704E-06 * 3 * (
                             vars[2] * row.mass_hc) ** 2 - 0.00262651390002428 * 2 * (
                             vars[2] * row.mass_hc) + 1.50855371168788) if (vars[2] * row.mass_hc) <= 1350 else (
                            1.61128548674228 * 0.899697806202926 * (vars[2] * row.mass_hc) ** (0.899697806202926 - 1))
            )) * row.mass_hc
        mbynd20_dep = 500. * (
            0 if row.mbynd20_dep >= 0 else (2. * (row.mbynd20_dep - cost.mbynd20_dep) / row.mbynd20_dep ** 2)) * (
                          -1.) * ((-0.138691077327068) if (vars[2] * row.mass_hc) <= 78.114 else ( \
            (-2.5767E-16 * 6 * (vars[2] * row.mass_hc) ** 5 + 1.25570849E-12 * 5 * (
                        vars[2] * row.mass_hc) ** 4 - 2.33339465412E-09 * 4 * (
                         vars[2] * row.mass_hc) ** 3 + 2.05922029150513E-06 * 3 * (
                         vars[2] * row.mass_hc) ** 2 - 0.000903990644090139 * 2 * (
                         vars[2] * row.mass_hc) - 0.15864057854991) if (vars[2] * row.mass_hc) <= 1350 else (
                        -0.0000112642961817011 * 2 * (vars[2] * row.mass_hc) - 0.362846148407561)
        )) * row.mass_hc
        wf = 2. * (1. - cost.wf) * (-1.) * 1.
        return d20 + c + hbyc + para + v25 + rll + mnd20 + lbmv + mbynd20_dep + wf

    def _grad3(self, row, vars, cost):
        c1 = 0
        h1 = 0
        d20 = 0
        c = 100. * (2. * (row.C - cost.c) / row.C ** 2) * (-1.) * (c1)
        hbyc = 10. * (2. * (row.hbyc - cost.hbyc) / row.hbyc ** 2) * (-1.) * ((cost.c * h1 - cost.h * c1) / cost.c ** 2)
        para = 50. * (2. * (row.para_hc - cost.para) / row.para_hc ** 2) * (-1.) * (0)
        v25 = 10. * (2. * (row.v25_hc - cost.v25) / row.v25_hc ** 2) * (-1.) * (0)
        rll = 100. * (2. * (row.rll_hc - cost.rll) / row.rll_hc ** 2) * (-1.) * (0)
        mnd20 = 100. * (2. * (row.mnd20_hc - cost.mnd20) / row.mnd20_hc ** 2) * (-1.) * (0)
        lbmv = 500. * (2. * (row.lbmv - cost.lbmv) / row.lbmv ** 2) * (-1.) * (((0.0000137764595451451 * 3 * (
                    row.mass_hc * vars[1] * vars[3]) ** 2 - 0.00455449752561954 * 2 * (row.mass_hc * vars[1] * vars[
            3]) + 1.67035692759237) if (row.mass_hc * vars[1] * vars[3]) <= 150 else ( \
            (- 8.43E-18 * 3 * (row.mass_hc * vars[1] * vars[3]) ** 2 + 7.6452E-16 * 2 * (
                        row.mass_hc * vars[1] * vars[3]) + 1.19056533134165) if (row.mass_hc * vars[1] * vars[
                3]) <= 950 else (1.34009804098614 * 0.984803528800257 * (row.mass_hc * vars[1] * vars[3]) ** (
                        0.984803528800257 - 1))
        )) * (row.mass_hc * vars[1]) + ((-2.699290111937E-08 * 4 * (
                    row.mass_hc * vars[1] - row.mass_hc * vars[1] * vars[3]) ** 3 + 0.0000178477062199311 * 3 * (
                                                     row.mass_hc * vars[1] - row.mass_hc * vars[1] * vars[
                                                 3]) ** 2 - 0.00422788943328101 * 2 * (
                                                     row.mass_hc * vars[1] - row.mass_hc * vars[1] * vars[
                                                 3]) + 1.64993385270191) if (row.mass_hc * vars[1] - row.mass_hc * vars[
            1] * vars[3]) <= 245 else ( \
            (-8.0182407936E-10 * 3 * (
                        row.mass_hc * vars[1] - row.mass_hc * vars[1] * vars[3]) ** 2 - 1.93901517697986E-06 * 2 * (
                         row.mass_hc * vars[1] - row.mass_hc * vars[1] * vars[3]) + 1.23418932687637) if (row.mass_hc *
                                                                                                          vars[
                                                                                                              1] - row.mass_hc *
                                                                                                          vars[1] *
                                                                                                          vars[
                                                                                                              3]) <= 1350 else (
                1.22753826813575)
        )) * (-row.mass_hc * vars[1]))
        mbynd20_dep = 500. * (
            0 if row.mbynd20_dep >= 0 else (2. * (row.mbynd20_dep - cost.mbynd20_dep) / row.mbynd20_dep ** 2)) * (
                          -1.) * (((-0.0499691144951728) if (row.mass_hc * vars[1] * vars[3]) <= 70.135 else ( \
            (6.64124123196186E-06 * 3 * (row.mass_hc * vars[1] * vars[3]) ** 2 - 0.00163014843927023 * 2 * (
                        row.mass_hc * vars[1] * vars[3]) + 0.0316936237605425) if (row.mass_hc * vars[1] * vars[
                3]) <= 150 else (-0.0139845478076861 * 1.30734582329863 * (row.mass_hc * vars[1] * vars[3]) ** (
                        1.30734582329863 - 1))
        )) * (row.mass_hc * vars[1]) + ((-0.0000916000485151024 * 2 * (
                    row.mass_hc * vars[1] - row.mass_hc * vars[1] * vars[3]) - 0.0381575593603518) if (row.mass_hc *
                                                                                                       vars[
                                                                                                           1] - row.mass_hc *
                                                                                                       vars[1] * vars[
                                                                                                           3]) <= 220 else (
                    -0.0118553726879464 * 1.31929385202031 * (
                0 if (row.mass_hc * vars[1] - row.mass_hc * vars[1] * vars[3]) <= 0 else (
                            row.mass_hc * vars[1] - row.mass_hc * vars[1] * vars[3])) ** (1.31929385202031 - 1))) * (
                                              -row.mass_hc * vars[1]))
        wf = 2. * (1. - cost.wf) * (-1.) * 1.
        return d20 + c + hbyc + para + v25 + rll + mnd20 + lbmv + mbynd20_dep + wf

    def _alpha(self, row, vars, grad):
        alphas, calphas, nvars = list(), list(), list()
        for i in range(1, 46):
            alpha = (0.00230808038753905 * np.exp(-0.189804192865194 * i)) * 0.9
            alphas.append(alpha)
            new_vars = np.array(vars) - alpha * np.array(grad)
            new_vars = [0 if x < 0 else x for x in new_vars]
            nvars.append(new_vars)
            calphas.append(self._objective(row, new_vars))
        alpha = alphas[calphas.index(min(calphas))]
        mcost = min(calphas)
        mvars = nvars[calphas.index(min(calphas))]
        return alpha, mcost

    def regress(self, row, vars, grad, alpha):
        nvars = np.array(vars) - alpha * np.array(grad)
        mvar0 = 0.7 if row.name <= 5.5 else 0.5 * (359.41 * row.name ** -1.133) / 100.
        mvar1 = 0. if row.name <= 5.5 else 0.005
        mvar2 = 0. if row.name <= 5.5 else (0.01 if row.name <= 118.5 else 0.005)
        mvar3 = 0. if row.name <= 6.5 else (0.005 if row.name <= 15.5 else 0.01)
        nvars[0] = 1. if nvars[0] > 1. else (mvar0 if nvars[0] < mvar0 else nvars[0])
        nvars[1] = 0.95 if nvars[1] > 0.95 else (mvar1 if nvars[1] < mvar1 else nvars[1])
        nvars[2] = 1. if nvars[2] > 1. else (mvar2 if nvars[2] < mvar2 else nvars[2])
        nvars[3] = 1. if nvars[3] > 1. else (mvar3 if nvars[3] < mvar3 else nvars[3])
        return nvars

    def _converged(self, row, vars, mcost):
        cd = np.abs(self._objective(row, vars) - mcost)
        if cd < 10 ** -3: return True
        return False

    def _sp(self, row):
        Ali_wf = ((86.0114249492252 * np.exp(-1. * 0.0177673248982099 * row.name)) if row.name > 8. else (
            100. if (305.499331374587 * row.name ** -0.670255496913115) > 100. else (
                        305.499331374587 * row.name ** -0.670255496913115))) / 100.
        Nap_wf = ((2.82897935494434 * row.name ** 0.448514890024682) if row.name > 15. else (
            0. if (-0.617455953455017 * row.name ** 2 + 13.3922281099951 * row.name - 51.5940531155635) * 1.1 < 0 else (
                                                                                                                                   -0.617455953455017 * row.name ** 2 + 13.3922281099951 * row.name - 51.5940531155635) * 1.1)) / 100.
        Ar_wf = 1. - (Ali_wf + Nap_wf)
        Nap5_wf = (
                              8.13565318377E-09 * row.name ** 6 - 3.14560346557703E-06 * row.name ** 5 + 0.000471967158649088 * row.name ** 4 - 0.0346670621679459 * row.name ** 3 + 1.2919295926371 * row.name ** 2 - 22.7746200504744 * row.name + 186.66230336393 - 1.1) / 100
        sp = [Ali_wf, Nap_wf, Ar_wf, Nap5_wf]
        return sp


class CalculatedValues(object):
    def __init__(self, parent, ov, sno):
        self.__dict__ = parent.__dict__.copy()
        self._prepare(ov, sno)

    def _prepare(self, ov, sno):
        df = ov.df.copy(deep=True)
        self.fixVars(df, ov.df)
        self.mass(df)
        self.hc_params(df)
        df[["S", "O", "N", "mf"]] = sno[["S", "N", "O", "mf"]]
        self.wc(df, sno)
        self.aromatic(df)
        self.napthenic(df)
        self.aliphatic(df)
        self.df = df

    def fixVars(self, df, obs):
        obs.loc[obs.C <= 5.5, "Ar_wf"] = 0
        df["Ali_wf"] = obs.Ali_wf / (obs.Ali_wf + obs.Nap_wf + obs.Ar_wf)
        df["Nap_wf"] = obs.Nap_wf / (obs.Ali_wf + obs.Nap_wf + obs.Ar_wf)
        df["Ar_wf"] = obs.Ar_wf / (obs.Ali_wf + obs.Nap_wf + obs.Ar_wf)

    def mass(self, df):
        df["ali_mass"] = df.mass_hc * df.Ali_wf
        df["nap_mass"] = df.mass_hc * df.Nap_wf
        df["ar_mass"] = df.mass_hc * df.Ar_wf
        df["nap5_mass"] = df.nap_mass * df.Nap5_wf
        df["nap6_mass"] = df.apply(
            lambda row: 0 if (row.nap_mass - row.nap5_mass) < 0 else (row.nap_mass - row.nap5_mass), axis=1)
        df["mass_hc"] = df.ali_mass + df.nap_mass + df.ar_mass

    def hc_params(self, df):
        pc = preComponentModeling(self)
        for index, row in df.iterrows():
            vars = [row.Ali_wf, row.Nap_wf, row.Ar_wf, row.Nap5_wf]
            cost = pc._cost(row, vars)
            for col in ["d20", "c", "h", "hbyc", "para", "v25", "rll", "mnd20", "lbmv", "mbynd20_dep", "wf", "d20_por",
                        "d20_nap", "d20_ali"]:
                df.loc[df.seq == row.seq, col] = cost[col]
        df.drop(columns=["d20_hc", "C", "H"], inplace=True)
        df.rename(columns={"d20": "d20_hc", "d20_por": "d20_par", "c": "C", "h": "H"}, inplace=True)
        df["d25_hc"] = ((df.d20_hc * 1000. * np.exp((-1. * (613.97226 / (df.d20_hc * 1000.) ** 2) * (25. - 20.) * (
                    1. + 0.8 * (613.97226 / (df.d20_hc * 1000.) ** 2) * (25. - 20.))))) / 1000.)

    def wc(self, df, sno):
        df["c_fr_wc"] = df.C / (df.C + df.H + df.S + df.O + df.N)
        df["mass_wc"] = sno.mass
        df["bp_wc"] = (47.6112866933562 * df.mass_wc ** 0.509665090106662 * df.c_fr_wc ** 0.26807533679463)
        df["par_wc"] = df.para + df.S * 50. + df.O * 20. + df.N * 17.5
        df["rll_wc"] = df.rll + df.S * 7.91 + df.O * 1.643 + df.N * 2.84
        df["mnd20_wc"] = df.mnd20 + df.S * 52.86 + df.O * 22.74 + df.N * 30.23
        df["ri_wc"] = df.mnd20_wc / df.mass_wc
        df["rgd_wc"] = df.rll_wc * (df.ri_wc ** 2 + 2.) / (df.ri_wc + 1.)
        df["lbmv_wc"] = df.lbmv + 25.6 * df.S + 7.4 * df.O + 11.25 * df.N
        df["d25_wc"] = df.mass_wc / (df.mass_hc / df.d25_hc + 14. * df.S + 3.75 * df.O + 1.5 * df.N)
        df["d20_wc"] = ((df.d25_wc * 1000. * np.exp((-1. * (613.97226 / (df.d25_wc * 1000.) ** 2) * (20. - 25.) * (
                    1. + 0.8 * (613.97226 / (df.d25_wc * 1000.) ** 2) * (20. - 25.))))) / 1000.)
        df["v25_wc"] = df.mass_wc / df.d25_wc
        df["v20_wc"] = df.mass_wc / df.d20_wc

    def aromatic(self, df):
        df["arc_ring"] = df.apply(lambda row: (
                    -0.0000001354040217456 * (row.Ar_wf * row.mass_hc) ** 3 + 0.0000520658252493034 * (
                        row.Ar_wf * row.mass_hc) ** 2 + 0.0735699529423499 * (
                                row.Ar_wf * row.mass_hc) + 1.68398628E-12) if (row.Ar_wf * row.mass_hc) <= 178 else (
                    0.081366952742896 * (row.Ar_wf * row.mass_hc) - 0.495292073123728), axis=1)
        df["arh_ring"] = df.apply(lambda row: (row.mass_hc * row.Ar_wf * 0.992063492063492 - 11.9156746031746 * ((
                                                                                                                             -0.0000001354040217456 * (
                                                                                                                                 row.Ar_wf * row.mass_hc) ** 3 + 0.0000520658252493034 * (
                                                                                                                                         row.Ar_wf * row.mass_hc) ** 2 + 0.0735699529423499 * (
                                                                                                                                         row.Ar_wf * row.mass_hc) + 1.68398628E-12) if (
                                                                                                                                                                                                   row.Ar_wf * row.mass_hc) <= 178 else (
                    0.081366952742896 * (row.Ar_wf * row.mass_hc) - 0.495292073123728))), axis=1)
        df["para_ar"] = df.apply(lambda row: self._para_ar(row, [row.Ali_wf, row.Nap_wf, row.Ar_wf, row.Nap5_wf]),
                                 axis=1)
        df["v25_ar"] = df.apply(lambda row: self._v25_ar(row, [row.Ali_wf, row.Nap_wf, row.Ar_wf, row.Nap5_wf]), axis=1)
        df["rll_ar"] = df.apply(lambda row: self._rll_ar(row, [row.Ali_wf, row.Nap_wf, row.Ar_wf, row.Nap5_wf]), axis=1)
        df["mnd20_ar"] = df.apply(lambda row: self._mnd20_ar(row, [row.Ali_wf, row.Nap_wf, row.Ar_wf, row.Nap5_wf]),
                                  axis=1)
        df["no_ar_ring"] = df.apply(
            lambda row: (0.0128018024937911 * (row.Ar_wf * row.mass_hc)) if (row.Ar_wf * row.mass_hc) <= 78.114 else (
                (0.0202183047870432 * (row.Ar_wf * row.mass_hc) - 0.587335140981496) if (
                                                                                                    row.Ar_wf * row.mass_hc) <= 226.3 else (
                            0.0338676948709907 * (row.Ar_wf * row.mass_hc) - 3.66041883727174)), axis=1)
        df["lbmv_ar"] = df.apply(lambda row: self._lbmv_ar(row, [row.Ali_wf, row.Nap_wf, row.Ar_wf, row.Nap5_wf]),
                                 axis=1)
        df["mbynd20_dep_ar"] = df.apply(
            lambda row: self._mbynd20_dep_ar(row, [row.Ali_wf, row.Nap_wf, row.Ar_wf, row.Nap5_wf]), axis=1)
        df["no_of_db"] = df.apply(
            lambda row: (0.0384054074813734 * (row.Ar_wf * row.mass_hc)) if (row.Ar_wf * row.mass_hc) <= 78.114 else (
                        0.0408799637586271 * (row.Ar_wf * row.mass_hc) - 0.354541730913743), axis=1)
        df["ar_cc"] = df.apply(lambda row: 0 if (row.arc_ring * (-row.arh_ring / row.arc_ring + 1)) < 10 ** -4 else (
                    row.arc_ring * (-row.arh_ring / row.arc_ring + 1.)), axis=1)
        df["ar_ach_alk"] = df.arc_ring - df.ar_cc
        df["ar_cc_ring"] = df.apply(lambda row: 0 if row.ar_cc == 0 else (row.ar_cc + 2.) / 2., axis=1)
        df["ar_ring1"] = df.apply(
            lambda row: (0.0128018024937911 * (row.Ar_wf * row.mass_hc)) if (row.Ar_wf * row.mass_hc) <= 78.114 else (
                (0.0202183047870432 * (row.Ar_wf * row.mass_hc) - 0.587335140981496) if (
                                                                                                    row.Ar_wf * row.mass_hc) <= 226.3 else (
                            0.0338676948709907 * (row.Ar_wf * row.mass_hc) - 3.66041883727174)), axis=1)
        df["ar_ring"] = df.apply(lambda row: max(row.ar_cc_ring, row.ar_ring1), axis=1)
        df["ar_alk_ring"] = df.apply(
            lambda row: 0 if (row.ar_ring - row.ar_cc_ring) < 0 else (row.ar_ring - row.ar_cc_ring), axis=1)

    def napthenic(self, df):
        df["nap_c"] = df.apply(
            lambda row: (row.mass_hc * row.Nap_wf * 0.0712910814857061) if (row.mass_hc * row.Nap_wf) <= 85 else (
                (row.mass_hc * row.Nap_wf * 0.0712910814857061) if ((row.mass_hc * row.Nap_wf * 0.0712910814857061) >= (
                            0.0681542447698304 * (0 if (row.mass_hc * row.Nap_wf) <= 0 else (
                                row.mass_hc * row.Nap_wf)) ** 1.01250029139992)) else (0.0681542447698304 * (
                    0 if (row.mass_hc * row.Nap_wf) <= 0 else (row.mass_hc * row.Nap_wf)) ** 1.01250029139992)), axis=1)
        df["nap_h"] = df.apply(lambda row: (row.mass_hc * row.Nap_wf * 0.992063492063492 - 11.9156746031746 * (
            (row.mass_hc * row.Nap_wf * 0.0712910814857061) if (row.mass_hc * row.Nap_wf) <= 85 else (
                (row.mass_hc * row.Nap_wf * 0.0712910814857061) if ((row.mass_hc * row.Nap_wf * 0.0712910814857061) >= (
                            0.0681542447698304 * (0 if (row.mass_hc * row.Nap_wf) <= 0 else (
                                row.mass_hc * row.Nap_wf)) ** 1.01250029139992)) else (0.0681542447698304 * (
                    0 if (row.mass_hc * row.Nap_wf) <= 0 else (row.mass_hc * row.Nap_wf)) ** 1.01250029139992)))),
                               axis=1)
        df["para_nap"] = df.apply(lambda row: self._para_nap(row, [row.Ali_wf, row.Nap_wf, row.Ar_wf, row.Nap5_wf]),
                                  axis=1)
        df["v25_nap"] = df.apply(lambda row: self._v25_nap(row, [row.Ali_wf, row.Nap_wf, row.Ar_wf, row.Nap5_wf]),
                                 axis=1)
        df["rll_nap"] = df.apply(lambda row: self._rll_nap(row, [row.Ali_wf, row.Nap_wf, row.Ar_wf, row.Nap5_wf]),
                                 axis=1)
        df["mnd20_nap"] = df.apply(lambda row: self._mnd20_nap(row, [row.Ali_wf, row.Nap_wf, row.Ar_wf, row.Nap5_wf]),
                                   axis=1)
        df["lbmv_nap"] = df.apply(lambda row: self._lbmv_nap(row, [row.Ali_wf, row.Nap_wf, row.Ar_wf, row.Nap5_wf]),
                                  axis=1)
        df["mbynd20_dep_nap"] = df.apply(
            lambda row: self._mbynd20_dep_nap(row, [row.Ali_wf, row.Nap_wf, row.Ar_wf, row.Nap5_wf]), axis=1)
        df["nap5_ring"] = df.apply(lambda row: (row.mass_hc * row.Nap5_wf * row.Nap_wf * 0.0142857142857143) if (
                                                                                                                            row.mass_hc * row.Nap5_wf * row.Nap_wf) <= 70.135 else (
                    0.0249594409085236 * row.mass_hc * row.Nap5_wf * row.Nap_wf - 0.750530388119291), axis=1)
        df["nap6_ring"] = df.apply(
            lambda row: ((row.mass_hc * row.Nap_wf - row.mass_hc * row.Nap5_wf * row.Nap_wf) * 0.0118818469142844) if (
                                                                                                                                  row.mass_hc * row.Nap_wf - row.mass_hc * row.Nap5_wf * row.Nap_wf) <= 84.162 else (
                        0.0183539141721682 * (
                            row.mass_hc * row.Nap_wf - row.mass_hc * row.Nap5_wf * row.Nap_wf) - 0.417520813955541),
            axis=1)
        df["nap6_c"] = df.apply(lambda row: self._nap6_c(row), axis=1)
        df["nap5_c"] = df.apply(lambda row: 0 if (row.nap_c - row.nap6_c) < 0 else (row.nap_c - row.nap6_c), axis=1)
        df["nap5_cc"] = df.apply(lambda row: self._nap5_cc(row), axis=1)
        df["nap6_cc"] = df.apply(lambda row: self._nap6_cc(row), axis=1)
        df["nap_none_cc"] = df.apply(
            lambda row: 0 if (row.nap_c - row.nap5_cc - row.nap6_cc) < 0 else (row.nap_c - row.nap5_cc - row.nap6_cc),
            axis=1)

    def aliphatic(self, df):
        df["ali_c"] = ((df.mass_hc * df.Ali_wf - 1.008 * df.factor_f) * 0.0712910814857061)
        df["ali_h"] = (df.mass_hc * df.Ali_wf * 0.142582162971412 + 0.856277179724817 * df.factor_f)
        df["para_ali"] = df.apply(lambda row: self._para_ali(row, [row.Ali_wf, row.Nap_wf, row.Ar_wf, row.Nap5_wf]),
                                  axis=1)
        df["v25_ali"] = df.apply(lambda row: self._v25_ali(row, [row.Ali_wf, row.Nap_wf, row.Ar_wf, row.Nap5_wf]),
                                 axis=1)
        df["rll_ali"] = df.apply(lambda row: self._rll_ali(row, [row.Ali_wf, row.Nap_wf, row.Ar_wf, row.Nap5_wf]),
                                 axis=1)
        df["mnd20_ali"] = df.apply(lambda row: self._mnd20_ali(row, [row.Ali_wf, row.Nap_wf, row.Ar_wf, row.Nap5_wf]),
                                   axis=1)
        df["lbmv_ali"] = df.apply(lambda row: self._lbmv_ali(row, [row.Ali_wf, row.Nap_wf, row.Ar_wf, row.Nap5_wf]),
                                  axis=1)

    def _nap5_cc(self, row):
        if (2 * row.nap5_ring - 2) >= (0 if row.C <= 8 else (
        (0.25 * row.nap5_c) if row.nap5_c <= 8 else (0.666666666666667 * row.nap5_c - 3.33333333333333))):
            return (2 * row.nap5_ring - 2)
        else:
            return (0 if row.C <= 8 else (
                (0.25 * row.nap5_c) if row.nap5_c <= 8 else (0.666666666666667 * row.nap5_c - 3.33333333333333)))

    def _nap6_cc(self, row):
        if (2 * row.nap6_ring - 2) >= (0 if row.C <= 10 else (
        (0.2 * row.nap6_c) if row.nap6_c <= 10 else (0.496539162112933 * row.nap6_c - 2.7344262295082))):
            return (2 * row.nap6_ring - 2)
        else:
            return (0 if row.C <= 10 else (
                (0.2 * row.nap6_c) if row.nap6_c <= 10 else (0.496539162112933 * row.nap6_c - 2.7344262295082)))

    def _nap6_c(self, row):
        if (row.mass_hc * row.Nap_wf - row.mass_hc * row.Nap_wf * row.Nap5_wf) <= 85:
            return ((row.mass_hc * row.Nap_wf - row.mass_hc * row.Nap_wf * row.Nap5_wf) * 0.0712910814857061)
        elif (((row.mass_hc * row.Nap_wf - row.mass_hc * row.Nap_wf * row.Nap5_wf) * 0.0712910814857061) >= (
                0.0681542447698304 * (
        0 if (row.mass_hc * row.Nap_wf - row.mass_hc * row.Nap_wf * row.Nap5_wf) <= 0 else (
                row.mass_hc * row.Nap_wf - row.mass_hc * row.Nap_wf * row.Nap5_wf)) ** 1.01250029139992)):
            return ((row.mass_hc * row.Nap_wf - row.mass_hc * row.Nap_wf * row.Nap5_wf) * 0.0712910814857061)
        else:
            return (0.0681542447698304 * (
                0 if (row.mass_hc * row.Nap_wf - row.mass_hc * row.Nap_wf * row.Nap5_wf) <= 0 else (
                            row.mass_hc * row.Nap_wf - row.mass_hc * row.Nap_wf * row.Nap5_wf)) ** 1.01250029139992)

    def _mbynd20_dep_ar(self, row, vars):
        mbynd20_dep_ar = (-0.138691077327068 * (vars[2] * row.mass_hc)) if (vars[2] * row.mass_hc) <= 78.114 else ( \
            (-2.5767E-16 * (vars[2] * row.mass_hc) ** 6 + 1.25570849E-12 * (
                        vars[2] * row.mass_hc) ** 5 - 2.33339465412E-09 * (
                         vars[2] * row.mass_hc) ** 4 + 2.05922029150513E-06 * (
                         vars[2] * row.mass_hc) ** 3 - 0.000903990644090139 * (
                         vars[2] * row.mass_hc) ** 2 - 0.15864057854991 * (
                         vars[2] * row.mass_hc) + 10.4586452565478) if (vars[2] * row.mass_hc) <= 1350 else (
                        -0.0000112642961817011 * (vars[2] * row.mass_hc) ** 2 - 0.362846148407561 * (
                            vars[2] * row.mass_hc) + 45.8178034639216) \
            )
        return mbynd20_dep_ar

    def _mbynd20_dep_nap(self, row, vars):
        mbynd20_dep_nap = ((-0.0499691144951728 * (row.mass_hc * vars[1] * vars[3])) if (row.mass_hc * vars[1] * vars[
            3]) <= 70.135 else ( \
            (6.64124123196186E-06 * (row.mass_hc * vars[1] * vars[3]) ** 3 - 0.00163014843927023 * (
                        row.mass_hc * vars[1] * vars[3]) ** 2 + 0.0316936237605425 * (
                         row.mass_hc * vars[1] * vars[3]) - 1.15107923E-12) if (row.mass_hc * vars[1] * vars[
                3]) <= 150 else (-0.0139845478076861 * (
                0 if (row.mass_hc * vars[1] * vars[3]) < 0 else (row.mass_hc * vars[1] * vars[3])) ** 1.30734582329863)
        )) + \
                          (-0.0000916000485151024 * (row.mass_hc * vars[1] - row.mass_hc * vars[1] * vars[
                              3]) ** 2 - 0.0381575593603518 * (
                                       row.mass_hc * vars[1] - row.mass_hc * vars[1] * vars[3])) if (row.mass_hc * vars[
            1] - row.mass_hc * vars[1] * vars[3]) <= 220 else (-0.0118553726879464 * (
            0 if (row.mass_hc * vars[1] - row.mass_hc * vars[1] * vars[3]) < 0 else (
                        row.mass_hc * vars[1] - row.mass_hc * vars[1] * vars[3])) ** 1.31929385202031)
        return mbynd20_dep_nap

    def _lbmv_ar(self, row, vars):
        lbmv_ar = (1.22897303940395 * (vars[2] * row.mass_hc)) if (vars[2] * row.mass_hc) <= 78.114 else ( \
            (-0.0000000000000003456 * (vars[2] * row.mass_hc) ** 6 + 1.88579496E-12 * (
                        vars[2] * row.mass_hc) ** 5 - 4.09056994439E-09 * (
                         vars[2] * row.mass_hc) ** 4 + 4.48623820651704E-06 * (
                         vars[2] * row.mass_hc) ** 3 - 0.00262651390002428 * (
                         vars[2] * row.mass_hc) ** 2 + 1.50855371168788 * (
                         vars[2] * row.mass_hc) - 8.75690977467315) if (vars[2] * row.mass_hc) <= 1350 else (
                        1.61128548674228 * (
                    0 if (vars[2] * row.mass_hc) <= 0 else (vars[2] * row.mass_hc)) ** 0.899697806202926) \
            )
        return lbmv_ar

    def _lbmv_nap(self, row, vars):
        lbmv_nap = ((0.0000137764595451451 * (row.mass_hc * vars[1] * vars[3]) ** 3 - 0.00455449752561954 * (
                    row.mass_hc * vars[1] * vars[3]) ** 2 + 1.67035692759237 * (
                                 row.mass_hc * vars[1] * vars[3]) - 2.626165951E-11) if (row.mass_hc * vars[1] * vars[
            3]) <= 150 else ( \
            (- 8.43E-18 * (row.mass_hc * vars[1] * vars[3]) ** 3 + 7.6452E-16 * (
                        row.mass_hc * vars[1] * vars[3]) ** 2 + 1.19056533134165 * (
                         row.mass_hc * vars[1] * vars[3]) + 15.9997004846206) if (row.mass_hc * vars[1] * vars[
                3]) <= 950 else (1.34009804098614 * (0 if (row.mass_hc * vars[1] * vars[3]) <= 0 else (
                        row.mass_hc * vars[1] * vars[3])) ** 0.984803528800257) \
            )) + \
                   ((-2.699290111937E-08 * (
                               row.mass_hc * vars[1] - row.mass_hc * vars[1] * vars[3]) ** 4 + 0.0000178477062199311 * (
                                 row.mass_hc * vars[1] - row.mass_hc * vars[1] * vars[3]) ** 3 - 0.00422788943328101 * (
                                 row.mass_hc * vars[1] - row.mass_hc * vars[1] * vars[3]) ** 2 + 1.64993385270191 * (
                                 row.mass_hc * vars[1] - row.mass_hc * vars[1] * vars[3]) - 2.74712874671E-09) if (
                                                                                                                              row.mass_hc *
                                                                                                                              vars[
                                                                                                                                  1] - row.mass_hc *
                                                                                                                              vars[
                                                                                                                                  1] *
                                                                                                                              vars[
                                                                                                                                  3]) <= 245 else ( \
                       (-8.0182407936E-10 * (row.mass_hc * vars[1] - row.mass_hc * vars[1] * vars[
                           3]) ** 3 - 1.93901517697986E-06 * (
                                    row.mass_hc * vars[1] - row.mass_hc * vars[1] * vars[3]) ** 2 + 1.23418932687637 * (
                                    row.mass_hc * vars[1] - row.mass_hc * vars[1] * vars[3]) + 11.2968615678507) if (
                                                                                                                                row.mass_hc *
                                                                                                                                vars[
                                                                                                                                    1] - row.mass_hc *
                                                                                                                                vars[
                                                                                                                                    1] *
                                                                                                                                vars[
                                                                                                                                    3]) <= 1350 else (
                                   1.22753826813575 * (
                                       row.mass_hc * vars[1] - row.mass_hc * vars[1] * vars[3]) + 14.8879242773457) \
                       ))
        return lbmv_nap

    def _lbmv_ali(self, row, vars):
        lbmv_ali = ((row.mass_hc * vars[0]) * 1.58266200898268 + 2.10467669494546 * row.factor_f)
        return lbmv_ali

    def _mnd20_ar(self, row, vars):
        mnd20_ar = (1.50112 * (vars[2] * row.mass_hc)) if (vars[2] * row.mass_hc) <= 78.114 else ( \
            (-5.84206450751114E-06 * (vars[2] * row.mass_hc) ** 3 + 0.00751213531409437 * (
                        vars[2] * row.mass_hc) ** 2 + 0.144073628503315 * (
                         vars[2] * row.mass_hc) + 68.4532599149825) if (vars[2] * row.mass_hc) <= 500 else ( \
                (-8.41693144913E-09 * (vars[2] * row.mass_hc) ** 3 + 0.000161108611351861 * (
                            vars[2] * row.mass_hc) ** 2 + 2.12248219255403 * (
                             vars[2] * row.mass_hc) - 63.9862717815407) if (vars[2] * row.mass_hc) <= 1350 else (
                            1.18286661434459 * (
                        0 if (vars[2] * row.mass_hc) <= 0 else (vars[2] * row.mass_hc)) ** 1.09094887407156) \
                )
        )
        return mnd20_ar

    def _mnd20_nap(self, row, vars):
        mnd20_nap = (1.42058467041269 * (row.mass_hc * vars[1])) if (row.mass_hc * vars[1]) <= 70.135 else ( \
            (1.447284293541 * (row.mass_hc * vars[1]) - 0.37610425029041) if (row.mass_hc * vars[1]) <= 150 else ( \
                (-6.624138640713E-08 * (row.mass_hc * vars[1]) ** 3 + 0.000275943845907455 * (
                            row.mass_hc * vars[1]) ** 2 + 1.60710605833917 * (
                             row.mass_hc * vars[1]) - 21.4347121998645) if (row.mass_hc * vars[1]) <= 1350 else (
                            1.02283899923738 * (
                        0 if (row.mass_hc * vars[1]) <= 0 else (row.mass_hc * vars[1])) ** 1.0815776154784) \
                )
        )
        return mnd20_nap

    def _mnd20_ali(self, row, vars):
        mnd20_ali = ((row.mass_hc * vars[0]) * 1.47073501105012 - 4.04250089113852 * row.factor_f)
        return mnd20_ali

    def _rll_ar(self, row, vars):
        rll_ar = (0.335239449951955 * (vars[2] * row.mass_hc)) if (vars[2] * row.mass_hc) <= 78.114 else ( \
            (-1.87116097323482E-06 * (vars[2] * row.mass_hc) ** 3 + 0.0018792988594211 * (
                        vars[2] * row.mass_hc) ** 2 - 0.0939803177037452 * (
                         vars[2] * row.mass_hc) + 24.2899661854656) if (vars[2] * row.mass_hc) <= 500 else ( \
                (2.452083914396E-08 * (vars[2] * row.mass_hc) ** 3 - 0.000079209630494905 * (
                            vars[2] * row.mass_hc) ** 2 + 0.381092016734914 * (
                             vars[2] * row.mass_hc) - 3.73279981787174) if (vars[2] * row.mass_hc) <= 1350 else (
                            0.528496537381154 * (
                        0 if (vars[2] * row.mass_hc) <= 0 else (vars[2] * row.mass_hc)) ** 0.928884019445315) \
                )
        )
        return rll_ar

    def _rll_nap(self, row, vars):
        rll_nap = (0.339821860964231 * (row.mass_hc * vars[1])) if (row.mass_hc * vars[1]) <= 70.135 else ( \
            (0.33771553289526 * (row.mass_hc * vars[1]) + 0.0306062107298265) if (row.mass_hc * vars[1]) <= 150 else ( \
                (-2.293E-17 * (row.mass_hc * vars[1]) ** 6 + 1.0414983E-13 * (
                            row.mass_hc * vars[1]) ** 5 - 1.5843345177E-10 * (
                             row.mass_hc * vars[1]) ** 4 + 6.998936592203E-08 * (
                             row.mass_hc * vars[1]) ** 3 + 0.0000329446336797695 * (
                             row.mass_hc * vars[1]) ** 2 + 0.316604983947332 * (
                             row.mass_hc * vars[1]) + 1.74679158038743) if (row.mass_hc * vars[1]) <= 1350 else (
                            0.366228631794776 * (
                        0 if (row.mass_hc * vars[1]) <= 0 else (row.mass_hc * vars[1])) ** 0.991034715216667) \
                )
        )
        return rll_nap

    def _rll_ali(self, row, vars):
        rll_ali = ((row.mass_hc * vars[0]) * 0.329222214300991 + 0.768144007984601 * row.factor_f)
        return rll_ali

    def _v25_ar(self, row, vars):
        v_ar = (1.14219998676821 * (vars[2] * row.mass_hc)) if (vars[2] * row.mass_hc) <= 78.114 else ( \
            (-3.0818872804607E-07 * (vars[2] * row.mass_hc) ** 3 + 0.000157473969023783 * (
                        vars[2] * row.mass_hc) ** 2 + 0.562071271755067 * (
                         vars[2] * row.mass_hc) + 45.1777143670648) if (vars[2] * row.mass_hc) <= 500 else ( \
                (-1.336E-17 * (vars[2] * row.mass_hc) ** 6 + 8.597456E-14 * (
                            vars[2] * row.mass_hc) ** 5 - 2.3321333134E-10 * (
                             vars[2] * row.mass_hc) ** 4 + 3.5394846103436E-07 * (
                             vars[2] * row.mass_hc) ** 3 - 0.000354659417799298 * (
                             vars[2] * row.mass_hc) ** 2 + 0.702633249568294 * (
                             vars[2] * row.mass_hc) + 30.8475817662467) if (vars[2] * row.mass_hc) <= 1350 else (
                            1.78553362642803 * (
                        0 if (vars[2] * row.mass_hc) <= 0 else (vars[2] * row.mass_hc)) ** 0.835009655244618) \
                )
        )
        return v_ar

    def _v25_nap(self, row, vars):
        v_nap = (1.34866139998192 * (row.mass_hc * vars[1])) if (row.mass_hc * vars[1]) <= 70.135 else ( \
            (1.28366284131307 * (row.mass_hc * vars[1]) + 0.651255737588963) if (row.mass_hc * vars[1]) <= 150 else ( \
                (-3.280281826E-11 * (row.mass_hc * vars[1]) ** 4 + 1.5036964504228E-07 * (
                            row.mass_hc * vars[1]) ** 3 - 0.000287245765992295 * (
                             row.mass_hc * vars[1]) ** 2 + 0.946749304356398 * (
                             row.mass_hc * vars[1]) + 31.5857375708661) if (row.mass_hc * vars[1]) <= 1350 else (
                            2.08318821509476 * (
                        0 if (row.mass_hc * vars[1]) <= 0 else (row.mass_hc * vars[1])) ** 0.862908532042399)
            )
        )
        return v_nap

    def _v25_ali(self, row, vars):
        v_ali = (-2.09149718E-12 * (row.mass_hc * vars[0]) ** 6 + 2.56273813802E-09 * (
                    row.mass_hc * vars[0]) ** 5 - 1.23907173033011E-06 * (
                             row.mass_hc * vars[0]) ** 4 + 0.000296187570285156 * (
                             row.mass_hc * vars[0]) ** 3 - 0.0355571041254734 * (
                             row.mass_hc * vars[0]) ** 2 + 3.04701929242583 * (
                             row.mass_hc * vars[0]) + 0.00439327908679843) if (row.mass_hc * vars[0]) <= 325 else ( \
            (7.41289E-15 * (row.mass_hc * vars[0]) ** 5 - 4.954497963E-11 * (
                        row.mass_hc * vars[0]) ** 4 + 1.4086741743278E-07 * (
                         row.mass_hc * vars[0]) ** 3 - 0.00024576304582591 * (
                         row.mass_hc * vars[0]) ** 2 + 1.25190727201385 * (
                         row.mass_hc * vars[0]) + 20.7143141299502) if (row.mass_hc * vars[0]) <= 1350 else (
                        1.87454241096822 * (
                    0 if (row.mass_hc * vars[0]) <= 0 else (row.mass_hc * vars[0])) ** 0.929332334824849) \
            )
        return v_ali

    def _para_ar(self, row, vars):
        para_ar = (2.62187448054632 * (vars[2] * row.mass_hc)) if (vars[2] * row.mass_hc) <= 78.15 else ( \
            (-1.822524384834E-08 * (vars[2] * row.mass_hc) ** 4 + 0.0000199651937544676 * (
                        vars[2] * row.mass_hc) ** 3 - 0.00688962262335124 * (
                         vars[2] * row.mass_hc) ** 2 + 2.91787913445933 * (
                         vars[2] * row.mass_hc) + 1.99335676321425) if (vars[2] * row.mass_hc) <= 400. else ( \
                (2.2187371E-13 * (vars[2] * row.mass_hc) ** 5 - 1.09452475889E-09 * (
                            vars[2] * row.mass_hc) ** 4 + 2.07146705611516E-06 * (
                             vars[2] * row.mass_hc) ** 3 - 0.00192220203281091 * (
                             vars[2] * row.mass_hc) ** 2 + 2.49255064334514 * (
                             vars[2] * row.mass_hc) + 3.58255134747975) if (vars[2] * row.mass_hc) <= 1350. else (
                            3.50525295532602 * (
                        0 if (vars[2] * row.mass_hc) <= 0 else (vars[2] * row.mass_hc)) ** 0.901185761435326) \
                )
        )
        return para_ar

    def _para_nap(self, row, vars):
        para_nap = (2.89506991671068 * (row.mass_hc * vars[1])) if (row.mass_hc * vars[1]) <= 70.135 else ( \
            (6.069839E-14 * (row.mass_hc * vars[1]) ** 5 - 0.0000000003134148489 * (
                        row.mass_hc * vars[1]) ** 4 + 6.4959169160542E-07 * (
                         row.mass_hc * vars[1]) ** 3 - 0.000750319943029562 * (
                         row.mass_hc * vars[1]) ** 2 + 2.49938707512647 * (
                         row.mass_hc * vars[1]) + 35.4336466618015) if (row.mass_hc * vars[1]) <= 1350 else (
                        3.82825010509589 * (
                    0 if (row.mass_hc * vars[1]) <= 0 else (row.mass_hc * vars[1])) ** 0.918472389443328) \
            )
        return para_nap

    def _para_ali(self, row, vars):
        para_ali = ((row.mass_hc * vars[0]) * 2.79461039423968 + row.factor_f * 12.1830327226064)
        return para_ali


class ObservedValues(object):
    def __init__(self, parent, df):
        self.__dict__ = parent.__dict__.copy()
        self._prepare(df)

    def _prepare(self, df_o):
        df = df_o.copy(deep=True)
        df["v25_hc"] = df.v25 - df.S * 14. - df.O * 3.75 - df.N * 1.5
        df["d25_hc"] = df.mass_hc / df.v25_hc
        df["d20_hc"] = df.apply(lambda row: self._d20(row), axis=1)
        df["rll_hc"] = df.apply(lambda row: self._rll(row), axis=1)
        df["mnd20_hc"] = df.apply(lambda row: self._mnd20(row), axis=1)
        df["ri_hc"] = df.mnd20_hc / df.mass_hc
        df["R_20_hc"] = (df.ri_hc ** 2 - 1.) / (df.ri_hc ** 2 + 2.)
        df["mbynd20_hc"] = df.mass_hc / df.ri_hc
        df["mbynd20_dep"] = df.apply(
            lambda row: 0 if (row.mbynd20_hc - (9.5013636627887 * row.C + 5.8626784355479)) > 0 else (
                        row.mbynd20_hc - (9.5013636627887 * row.C + 5.8626784355479)), axis=1)
        df["st_hc"] = 34.39 * (((df.d20_hc + 0.1674) ** 14) / df.d20_hc) ** (1. / 8.) - 7.509
        df["para_hc"] = (df.st_hc ** 0.25) * (df.mass_hc / df.d20_hc)
        df["factor_f"] = df.apply(
            lambda row: 1. if (-0.0238805970149254 * row.name + 2.08955223880597 + 0.029850746268657) < 1 else (
                        -0.0238805970149254 * row.name + 2.08955223880597 + 0.029850746268657), axis=1)
        df["hbyc"] = df.H / df.C
        self._cleardf(df)
        self.df = df

    def _cleardf(self, df):
        needed = ["component", "seq", "C", "H", "mass_hc", "d20_hc", "mbynd20_hc", "rll_hc", "mnd20_hc", "para_hc",
                  "d20_por", "d20_ali", "d20_nap", "v25_hc", "cfraction_hc", "lbmv", "factor_f", "hbyc", "mbynd20_dep"]
        remove = list()
        for col in df.columns:
            if col not in needed:
                remove.append(col)
        df.drop(columns=remove, inplace=True)

    def _d20(self, row):
        temp = ((row.d25_hc * 1000. * np.exp((-1. * (613.97226 / (row.d25_hc * 1000.) ** 2) * (20. - 25.) * (
                    1. + 0.8 * (613.97226 / (row.d25_hc * 1000.) ** 2) * (20. - 25.))))) / 1000.)
        if row.d20_hc <= temp * 0.98:
            return temp
        elif row.d20_hc >= temp * 1.02:
            return temp
        else:
            return row.d20_hc

    def _rll(self, row):
        temp = row.rll20 - row.S * 7.91 - row.O * 1.643 - row.N * 2.84
        if row.rll_hc <= temp * 0.995:
            return temp
        elif row.rll_hc >= temp * 1.005:
            return temp
        else:
            return row.rll_hc

    def _mnd20(self, row):
        temp = row.mnd20 - row.S * 52.86 - row.O * 22.74 - row.N * 30.23
        if row.mnd20_hc <= temp * 0.995:
            return temp
        elif row.mnd20_hc >= temp * 1.005:
            return temp
        else:
            return row.mnd20_hc
