import os
import socket
from itertools import product
from multiprocessing import Pool

import Scripts.pvt.settings as settings
from Scripts.pvt.settings import CSV_DATA
from Scripts.pvt.core.send_email import send_email


def run_process_vle(args):
    eos, compound = args[0], args[1]
    print("starting eos = %s | compound = %s" % (eos, compound))
    vle_main = os.path.join(settings.PVT_DIR, "vle", "vle.py")
    cmd = "python %s --eos '%s' --compound '%s'" % (vle_main, eos, compound)
    if eos == "poas_4":
        cmd += " --volume"
    os.system(cmd)
    print("completed eos = %s | compound = %s" % (eos, compound))


def run_test(all_compounds, all_eos):
    send_email(subject="POAS VLE TEST ALERT", body="VLE Test has started on %s" % socket.gethostname())
    cartesian = [i for i in product(*[all_eos, all_compounds])]
    max_processes = 4
    num = len(cartesian) if len(cartesian) < max_processes else max_processes

    pool = Pool(processes=num)
    pool.map(run_process_vle, cartesian)
    send_email(subject="POAS VLE TEST ALERT", body="VLE Test has completed on %s" % socket.gethostname())


def get_filenames():
    input_dir = os.path.join(CSV_DATA, "vle")
    if os.path.exists(input_dir):
        return [dI.split(".")[0] for dI in os.listdir(input_dir) if not os.path.isdir(os.path.join(input_dir, dI))]
    return []


if __name__ == "__main__":
    all_test_compounds = get_filenames()
    print "All Compounds = %s" % all_test_compounds
    all_test_eos = ["poas_4", "pr", "srk", "generic_poas_a", "als", "poas_4a", "pt", "sw_poas"]
    run_test(all_test_compounds, all_test_eos)
    # usage
    # nohup python test.py > vle_test.out &
