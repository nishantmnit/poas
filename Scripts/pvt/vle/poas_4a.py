import numpy as np
import pandas as pd

from Scripts.pvt.core.constants import gasConstant


def calc(row):
    row["beta_est"] = 0.79132 * row.zc_final - 0.02207
    row["beta"] = row.beta_est * row.multiplier
    row["om_b"] = (row.zc_final - row.beta)
    row["om_c"] = (1. - row.beta) ** 2 * (row.beta - 0.25)
    row["om_d"] = row.zc_final - (1. - row.beta) / 2.
    row["om_a"] = (1. - row.beta) ** 3
    row["b"] = row.om_b * gasConstant * row.tc_k / (row.pc_mpa * 10 ** 6)
    row["c"] = row.om_c * (gasConstant * row.tc_k / (row.pc_mpa * 10 ** 6)) ** 2
    row["d"] = row.om_d * gasConstant * row.tc_k / (row.pc_mpa * 10 ** 6)
    row["ac"] = (row.om_a * gasConstant ** 2 * row.tc_k ** 2 / (row.pc_mpa * 10 ** 6))
    row["ac_alpha"] = (row.ac * row.alpha_eos)
    if isinstance(row, pd.DataFrame):
        row["u"] = row.apply(lambda row1: 0 if row1.b == 0 else ((-2. * row1.d) / row1.b), axis=1)
    else:
        row["u"] = 0 if row.b == 0 else ((-2. * row.d) / row.b)
    if isinstance(row, pd.DataFrame):
        row["w"] = row.apply(lambda row1: 0 if row1.b == 0 else ((row1.d ** 2 + row1.c) / row1.b ** 2), axis=1)
    else:
        row["w"] = 0 if row.b == 0 else ((row.d ** 2 + row.c) / row.b ** 2)
    return row