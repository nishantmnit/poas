import numpy as np
import pandas as pd

from Scripts.pvt.core.constants import gasConstant


def calc(row):
    row["pf_min"] = 0.1
    row["om_b"] = 0.0777960739038885 * row.multiplier
    row["b"] = row.om_b * gasConstant * row.tc_k / (row.pc_mpa * 10 ** 6)
    row["pf"] = row.b * 10 ** 6 / row.vc
    if isinstance(row, pd.DataFrame):
        row["u"] = row.apply(lambda row1: 0 if row1.pf == 0 else (1. + 1. / row1.pf * (1. / row1.zc_final - 3.)),
                             axis=1)
    else:
        row["u"] = 0 if row.pf == 0 else (1. + 1. / row.pf * (1. / row.zc_final - 3.))
    if isinstance(row, pd.DataFrame):
        row["w"] = row.apply(lambda row1: 0 if ((row1.u - 1) * row1.pf ** 3 + 3 * row1.pf ** 2) == 0 else (
                (1 - 3 * row1.pf - row1.u ** 2 * row1.pf ** 3 - 3 * row1.u * row1.pf ** 2) / (
                (row1.u - 1) * row1.pf ** 3 + 3 * row1.pf ** 2)), axis=1)
    else:
        row["w"] = 0 if ((row.u - 1) * row.pf ** 3 + 3 * row.pf ** 2) == 0 else (
                (1 - 3 * row.pf - row.u ** 2 * row.pf ** 3 - 3 * row.u * row.pf ** 2) / (
                (row.u - 1) * row.pf ** 3 + 3 * row.pf ** 2))
    row["om_a"] = (3. * row.zc_final ** 2 - row.w * row.om_b ** 2 + row.u * (row.om_b + row.om_b ** 2))
    row["ac"] = (row.om_a * gasConstant ** 2 * row.tc_k ** 2 / (row.pc_mpa * 10 ** 6))
    row["ac_alpha"] = (row.ac * row.alpha_eos)
    return row

