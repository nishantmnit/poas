from importlib import import_module

import numpy as np


def get_multiplier(row, eos):
    multipliers = list()
    var_range = [row.multiplier_minimum, 1.5]
    if eos in ["als", "sw_poas"]:
        var_range[1] = 17.
    steps = 50.
    interval = (var_range[1] - var_range[0]) / steps
    var = var_range[0]
    while round(var, 2) <= var_range[1]:
        multipliers.append(var)
        var = var + interval
    return multipliers


def get_multiplier_objective(row, eos):
    row["sigma"] = 0 if (row.u ** 2 - 4 * row.w) <= 0 else (row.u - np.sqrt(row.u ** 2 - 4 * row.w)) / 2.
    if eos != "poas_4a":
        row["objective"] = 10000. if (row.volume + row.sigma * row.b * 10 ** 6) <= 0 or (
                    row.u ** 2 - 4 * row.w) < 0 else np.log(
            row.volume + row.sigma * row.b * 10 ** 6)
    else:
        row["objective"] = 10 ** -10 if (row.volume + row.sigma * row.b * 10 ** 6) <= 0 or (
                    row.u ** 2 - 4 * row.w) < 0 else np.log(
            row.volume + row.sigma * row.b * 10 ** 6)
    return row


def special_min_max_logic(objectives, multipliers):
    max_multiplier = multipliers[np.where(objectives == max(objectives[objectives > 0]))[0][-1]]
    min_objective = min(objectives[objectives < 0]) if np.any(objectives < 0) else min(objectives[objectives > 10**-10])
    min_multiplier = multipliers[np.where(objectives == min_objective)[0][-1]]
    return max_multiplier, min_multiplier


def find_max(row, eos):
    eos_module = import_module("Scripts.pvt.vle.%s" % eos)
    objectives = list()
    multipliers = get_multiplier(row, eos)
    for multiplier in multipliers:
        row["multiplier"] = multiplier
        row = eos_module.calc(row)
        row = get_multiplier_objective(row, eos)
        objectives.append(row.objective)
    objectives = np.array(objectives)
    if eos == "poas_4a":
        return special_min_max_logic(objectives, multipliers)
    min_objective = max(objectives[objectives < 0]) if np.any(objectives < 0) else min(objectives[objectives >= 0])
    if max(objectives) < 10000.:
        min_objective_index = len(objectives) - 1
    else:
        min_objective_index = np.where(objectives == min_objective)[0][-1]
    return multipliers[min_objective_index], row.multiplier_minimum


if __name__ == "__main__":
    import os
    from Scripts.pvt.settings import CSV_DATA
    from Scripts.pvt.vle.generic import pre_processing
    import pandas as pd
    from min_multiplier import find_min

    test_eos = "poas_4a"
    compound = "ISOBUTANE"
    observed_data_file = os.path.join(CSV_DATA, "vle", "%s.csv" % compound)
    ov = pd.read_csv(observed_data_file)
    ov = pre_processing(ov, test_eos)
    test_row = ov.loc[ov.seq == 1].squeeze()
    ov["multiplier_minimum"] = find_min(test_row, test_eos)
    for index, test_row in ov.iterrows():
        test_max_multiplier, test_min_multiplier = find_max(test_row, test_eos)
        ov.loc[ov.index == index, "multiplier_maximum"] = test_max_multiplier
        ov.loc[ov.index == index, "multiplier_minimum"] = test_min_multiplier


