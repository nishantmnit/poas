import numpy as np
import pandas as pd

from Scripts.pvt.core.constants import gasConstant


def calc(row):
    row["om_b"] = ((0.9129 * row.zc_final ** 2 - 0.0551 * row.zc_final + 0.0368) * row.multiplier)
    row["del_b"] = ((row.zc_final - row.om_b) ** 3 + 0.25 * (1 - 3 * row.zc_final + row.om_b) ** 2 +
                    3 * row.zc_final * row.om_b - row.om_b * (1 + 2 * row.om_b))
    if isinstance(row, pd.DataFrame):
        row["del_b"] = row["del_b"].apply(lambda x: 0 if x < 0 else x)
    else:
        row["del_b"] = 0 if row.del_b < 0 else row.del_b
    row["om_c"] = (row.om_b + 1 - 3 * row.zc_final + (4 * row.del_b) ** 0.5) / 2
    row["om_d"] = (row.om_b + 1 - 3 * row.zc_final - (4 * row.del_b) ** 0.5) / 2
    row["om_a"] = (3 * row.zc_final ** 2 + row.om_b * row.om_c + row.om_b * row.om_d
                   + row.om_c * row.om_d + row.om_c + row.om_d)
    row["b"] = row.om_b * gasConstant * row.tc_k / (row.pc_mpa * 10 ** 6)
    row["c"] = row.om_c * gasConstant * row.tc_k / (row.pc_mpa * 10 ** 6)
    row["d"] = row.om_d * gasConstant * row.tc_k / (row.pc_mpa * 10 ** 6)
    row["ac"] = (row.om_a * gasConstant ** 2 * row.tc_k ** 2 / (row.pc_mpa * 10 ** 6))
    row["ac_alpha"] = (row.ac * row.alpha_eos)
    if isinstance(row, pd.DataFrame):
        row["u"] = row.apply(lambda row1: 0 if row1.b == 0 else ((row1.c + row1.d) / row1.b), axis=1)
        row["w"] = row.apply(lambda row1: 0 if row1.b == 0 else (-1 * (row1.c * row1.d) / row1.b ** 2), axis=1)
    else:
        row["u"] = 0 if row.b == 0 else ((row.c + row.d) / row.b)
        row["w"] = 0 if row.b == 0 else (-1 * (row.c * row.d) / row.b ** 2)
    return row
