from importlib import import_module

import numpy as np
import pandas as pd


def get_multiplier(eos):
    multipliers = list()
    var_range = [0.25, 1.1]
    if eos in ["als", "sw_poas"]:
        var_range[1] = 17.
    elif eos == "pt":
        var_range[1] = 1.5
    steps = 50.
    interval = (var_range[1] - var_range[0]) / steps
    var = var_range[0]
    while round(var, 2) <= var_range[1]:
        multipliers.append(var)
        var = var + interval
    return multipliers


def get_multiplier_objective(row):
    row["objective"] = row.u ** 2 - 4. * row.w
    return row


def find_min(row, eos):
    eos_module = import_module("Scripts.pvt.vle.%s" % eos)
    objectives = list()
    multipliers = get_multiplier(eos)
    for multiplier in multipliers:
        row["multiplier"] = multiplier
        row = eos_module.calc(row)
        row = get_multiplier_objective(row)
        objectives.append(row.objective)
        if len(objectives) == 1 and row.objective > 0:
            return multiplier
    return tune_multiplier(objectives, multipliers)


def find_minima(objectives):
    min_negative_objective = max(objectives[objectives < 0]) if np.any(objectives < 0) else 500
    min_negative_objective_index = np.where(objectives == min_negative_objective)[0][0] if np.any(
        objectives < 0) else 500
    min_objective_index = 0 if min_negative_objective_index == 500 else min_negative_objective_index
    min_objective = objectives[min_objective_index]
    return min_objective, min_objective_index


def tune_multiplier(objectives, multipliers):
    df = pd.DataFrame(columns=["objective", "multiplier"], data=list(zip(objectives, multipliers)))
    df["difference"] = df.objective - df.objective.shift(-1)
    df = df.loc[df.difference != 0].reset_index(drop=True)
    objectives = df.objective.values
    multipliers = df.multiplier.tolist()
    min_objective, min_objective_index = find_minima(objectives)
    indexes = [min_objective_index + i for i in range(0, 4)]

    if np.any(np.array(indexes) < 0):
        indexes = list(np.array(indexes) + np.abs(min(indexes)))
    elif min_objective_index == len(multipliers) - 1:
        indexes = list(np.array(indexes) - ((len(multipliers) - 1) + max(indexes)))

    costs = [list(objectives)[i] for i in indexes]
    alphas = [multipliers[i] for i in indexes]

    k1 = (costs[1] ** 3 - costs[0] ** 3) * (costs[2] - costs[0]) - (costs[2] ** 3 - costs[0] ** 3) * (
            costs[1] - costs[0])
    k2 = (costs[1] ** 2 - costs[0] ** 2) * (costs[2] - costs[0]) - (costs[2] ** 2 - costs[0] ** 2) * (
            costs[1] - costs[0])
    k3 = (costs[1] ** 3 - costs[0] ** 3) * (costs[3] - costs[0]) - (costs[3] ** 3 - costs[0] ** 3) * (
            costs[1] - costs[0])
    k4 = (costs[1] ** 2 - costs[0] ** 2) * (costs[3] - costs[0]) - (costs[3] ** 2 - costs[0] ** 2) * (
            costs[1] - costs[0])
    p1 = (alphas[1] - alphas[0]) * (costs[2] - costs[0]) - (alphas[2] - alphas[0]) * (costs[1] - costs[0])
    p2 = (alphas[1] - alphas[0]) * (costs[3] - costs[0]) - (alphas[3] - alphas[0]) * (costs[1] - costs[0])

    a = 0 if (k1 * k4 - k2 * k3) == 0 else (p1 * k4 - p2 * k2) / (k1 * k4 - k2 * k3)
    if np.abs(a) > 10 ** 50:
        a = 10 ** 50
    b = 0 if k2 == 0 else (p1 - a * k1) / k2
    if np.abs(b) > 10 ** 50:
        b = 10 ** 50

    c = 0 if (costs[1] - costs[0]) == 0 else ((alphas[1] - alphas[0]) - a * (costs[1] ** 3 - costs[0] ** 3) - b * (
            costs[1] ** 2 - costs[0] ** 2)) / (costs[1] - costs[0])
    if np.abs(c) > 10 ** 50:
        c = 10 ** 50

    d = alphas[0] - (a * costs[0] ** 3 + b * costs[0] ** 2 + c * costs[0])
    if np.abs(d) > 10 ** 50:
        d = 10 ** 50

    multiplier_min = a * 0.07 ** 3 + b * 0.07 ** 2 + c * 0.07 + d

    multiplier_min = max(multipliers) if multiplier_min >= max(multipliers) else (
        min(multipliers) if multiplier_min <= min(multipliers) else multiplier_min)

    # multiplier_min = min_objective if min_objective_index == 0 else multiplier_min
    return multiplier_min


if __name__ == "__main__":
    import os
    from Scripts.pvt.settings import CSV_DATA
    from Scripts.pvt.vle.generic import pre_processing

    test_eos = "poas_4a"
    compound = "ISOBUTANE"
    observed_data_file = os.path.join(CSV_DATA, "vle", "%s.csv" % compound)
    ov = pd.read_csv(observed_data_file)
    ov = pre_processing(ov, test_eos)
    test_row = ov.loc[ov.seq == 1].squeeze()
    print find_min(test_row, test_eos)
