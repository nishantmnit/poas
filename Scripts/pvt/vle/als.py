import numpy as np
import pandas as pd

from Scripts.pvt.core.constants import gasConstant


def calc(row):
    row["omega_final"] = row.omega * row.multiplier
    row["om_b"] = 0.08974 - 0.03452 * row.omega_final + 0.0033 * row.omega_final ** 2
    row = min_logic(row, "om_b")
    row["om_c"] = 0.03686 + 0.00405 * row.omega_final - 0.01073 * row.omega_final ** 2 + 0.00157 * row.omega_final ** 3
    row = min_logic(row, "om_c")
    row["om_d"] = 0.154 + 0.14122 * row.omega_final - 0.00272 * row.omega_final ** 2 - 0.00484 * row.omega_final ** 3
    row = min_logic(row, "om_d")
    row["om_a"] = 0.44869 + 0.04024 * row.omega_final + 0.01111 * row.omega_final ** 2 - 0.00576 * row.omega_final ** 3
    row = min_logic(row, "om_a")
    row["b"] = row.om_b * gasConstant * row.tc_k / (row.pc_mpa * 10 ** 6)
    row["c"] = row.om_c * gasConstant * row.tc_k / (row.pc_mpa * 10 ** 6)
    row["d"] = row.om_d * gasConstant * row.tc_k / (row.pc_mpa * 10 ** 6)
    row["ac"] = (row.om_a * gasConstant ** 2 * row.tc_k ** 2 / (row.pc_mpa * 10 ** 6))
    row["ac_alpha"] = (row.ac * row.alpha_eos)
    if isinstance(row, pd.DataFrame):
        row["u"] = row.apply(lambda row1: 0. if row1.b == 0 else ((row1.d - row1.c) / row1.b), axis=1)
    else:
        row["u"] = 0. if row.b == 0 else ((row.d - row.c) / row.b)
    if isinstance(row, pd.DataFrame):
        row["w"] = row.apply(lambda row1: 0. if row1.b == 0 else (-1. * row1.d * row1.c / row1.b ** 2), axis=1)
    else:
        row["w"] = 0. if row.b == 0 else (-1. * row.d * row.c / row.b ** 2)
    return row


def min_logic(row, col):
    if isinstance(row, pd.DataFrame):
        row[col] = row.apply(lambda row1: 0.001 if row1[col] < 0.001 else row1[col], axis=1)
    else:
        row[col] = 0.001 if row[col] < 0.001 else row[col]
    return row