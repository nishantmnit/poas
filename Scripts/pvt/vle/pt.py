import numpy as np
import pandas as pd

from Scripts.pvt.core.constants import gasConstant


def calc(row):
    row["ziac"] = row.zc_final * row.multiplier
    row["om_c"] = 1. - 3. * row.ziac
    row["a1_d"] = 2. - 3. * row.ziac
    row["a2_d"] = 3. * row.ziac ** 2
    row["a3_d"] = -1. * row.ziac ** 3
    if isinstance(row, pd.DataFrame):
        row["om_b"] = row.apply(lambda row1: _om_b(row1), axis=1)
    else:
        row["om_b"] = _om_b(row)
    row["om_a"] = 3 * row.ziac ** 2 + 3 * (1 - 2 * row.ziac) * row.om_b + row.om_b ** 2 + 1 - 3 * row.ziac
    row["b"] = row.om_b * gasConstant * row.tc_k / (row.pc_mpa * 10 ** 6)
    row["c"] = row.om_c * gasConstant * row.tc_k / (row.pc_mpa * 10 ** 6)
    row["ac"] = (row.om_a * gasConstant ** 2 * row.tc_k ** 2 / (row.pc_mpa * 10 ** 6))
    row["ac_alpha"] = (row.ac * row.alpha_eos)
    if isinstance(row, pd.DataFrame):
        row["u"] = row.apply(lambda row1: 0 if row1.b == 0 else ((row1.c + row1.b) / row1.b), axis=1)
    else:
        row["u"] = 0 if row.b == 0 else ((row.c + row.b) / row.b)
    if isinstance(row, pd.DataFrame):
        row["w"] = row.apply(lambda row1: 0 if row1.b == 0 else (-1 * (row1.c / row1.b)), axis=1)
    else:
        row["w"] = 0 if row.b == 0 else (-1 * (row.c / row.b))
    return row


def _om_b(row):
    row["Q_d"] = (3. * row.a2_d - row.a1_d ** 2) / 9.
    row["L_d"] = (9. * row.a1_d * row.a2_d - 27. * row.a3_d - 2. * row.a1_d ** 3) / 54.
    row["D_d"] = row.Q_d ** 3 + row.L_d ** 2
    row["S1_d"] = (-1. * (-1. * (row.L_d + np.sqrt(row.D_d))) ** (1. / 3.)) if (row.L_d + np.sqrt(
        row.D_d)) < 0 else (row.L_d + np.sqrt(row.D_d)) ** (1. / 3.)
    row["S2_d"] = (-1. * (-1. * (row.L_d - np.sqrt(row.D_d))) ** (1. / 3.)) if (row.L_d - np.sqrt(
        row.D_d)) < 0 else (row.L_d - np.sqrt(row.D_d)) ** (1. / 3.)
    row["theta"] = np.degrees(
        np.arccos(1. if np.abs(row.L_d / np.sqrt(-row.Q_d ** 3)) > 1 else row.L_d / np.sqrt(-row.Q_d ** 3)))
    row["Z1_ov"] = 0 if row.Q_d >= 0 else 2. * np.sqrt(-row.Q_d) * np.cos(
        np.radians(row.theta * 1. / 3. + 120)) - row.a1_d / 3.
    row["Z2_ov"] = 0 if row.Q_d >= 0 else 2. * np.sqrt(-row.Q_d) * np.cos(
        np.radians(row.theta * 1. / 3. + 240)) - row.a1_d / 3.
    row["Z3_ov"] = 0 if row.Q_d >= 0 else 2. * np.sqrt(-row.Q_d) * np.cos(
        np.radians(row.theta * 1. / 3.)) - row.a1_d / 3.
    row["Z1_ov_d"] = row.S1_d + row.S2_d - row.a1_d / 3.
    row["Z1_ov_dd"] = ((-1. * (-1. * row.L_d) ** 1. / 3.) if row.L_d < 0 else row.L_d ** (
            1. / 3.)) * 2. - row.a1_d / 3.
    row["Z2_ov_dd"] = (((-1. * (-1. * row.L_d) ** 1. / 3.) if row.L_d < 0 else row.L_d ** (
            1. / 3.)) + row.a1_d / 3.) * -1
    row["Z3_ov_dd"] = row.Z2_ov_dd
    row["om_b1"] = _min([row.Z1_ov, row.Z2_ov, row.Z3_ov])
    row["om_b2"] = _min([row.Z1_ov_dd, row.Z2_ov_dd, row.Z3_ov_dd])
    row["om_b"] = row.om_b1 if row.D_d < 0 else (row.Z1_ov_d if row.D_d > 0 else row.om_b2)
    return row["om_b"]


def _min(data):
    data = [x for x in data if x > 0]
    if len(data) > 0:
        return min(data)
    return 0


def _max(data):
    data = [x for x in data if x > 0]
    if len(data) > 0:
        return max(data)
    return 0
