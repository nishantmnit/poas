import numpy as np
import pandas as pd

from Lib.Utils import getLogger
from Scripts.pvt.core.constants import gasConstant
from Scripts.pvt.core.least_square import intercept_slope
from Scripts.pvt.core.optimizers.poas_interpolator import monotone

logger = getLogger(qualname='vle volume standardize')

standard_temperatures = [180, 200, 220, 240, 260, 280, 298.15, 300, 320, 340, 360, 380, 400, 420, 440, 460, 480, 500,
                         520, 540, 560, 580, 600, 620, 640, 660, 680, 700, 720, 740, 760, 780, 800, 850, 900, 950, 1000,
                         1050, 1100, 1150, 1200, 1250, 1300, 1350, 1400, 1450, 1500, 1550, 1600, 1650, 1700, 1750, 1800,
                         1850, 1900, 1950, 2000, 2050, 2100, 2150, 2200, 2250, 2300, 2350, 2400, 2450, 2500]
standard_pressures = [300, 250, 200, 150, 125, 100, 90, 80, 70, 60, 50, 45, 40, 35, 30, 25, 20, 15, 10, 8, 6, 4, 3, 2,
                      1, 0.5, 0.10132501]


def standardize_volume(df):
    logger.info("Standardizing vle volume data for temperatures and pressures.")
    df = df[["temperature", "pressure", "v_liq", "v_vapour"]]
    df["volume"] = df.apply(lambda row: row.v_vapour if row.v_liq == 0 else row.v_liq, axis=1)
    df.sort_values("temperature", inplace=True)
    df.reset_index(inplace=True)
    df.rename(columns={"temperature": "xr", "volume": "y"}, inplace=True)
    new_df = standardize_temperatures(df)
    new_df = standardize_pressures(new_df)
    return new_df


def standardize_temperatures(df):
    dict_of_df = dict()
    for pressure in df.pressure.unique():
        logger.info("Executing for pressure = %s" % pressure)
        # part 1 - top 3 rows
        df_subset = df.loc[df.pressure == pressure].head(3)
        df_subset.reset_index(inplace=True)
        intercept, slope = intercept_slope(np.log(df_subset.xr.values), np.log(df_subset.y.values))
        min_temp = df_subset.xr.min()
        new_df = pd.DataFrame(columns=["xr"], data=standard_temperatures)
        new_df["y_calc"] = np.exp(intercept + np.log(new_df.xr) * slope)

        # part 2 - bottom 3 rows
        df_subset = df.loc[df.pressure == pressure].tail(3)
        df_subset.reset_index(inplace=True)
        intercept, slope = intercept_slope(np.log(df_subset.xr.values), np.log(df_subset.y.values))
        max_temp = df_subset.xr.max()
        new_df1 = pd.DataFrame(columns=["xr"], data=standard_temperatures)
        new_df1["y_calc"] = np.exp(intercept + np.log(new_df1.xr) * slope)

        # combine part 1 and 2
        new_df.loc[new_df.xr > df_subset.xr.max(), "y_calc"] = new_df1.loc[new_df1.xr > df_subset.xr.max(), "y_calc"]

        # interpolate if pressure is within the available range
        x = df.loc[df.pressure == pressure].xr.tolist()
        y = df.loc[df.pressure == pressure].y.tolist()
        find_x = new_df.loc[(new_df.xr <= max_temp) & (new_df.xr >= min_temp), "xr"].tolist()
        new_df.loc[(new_df.xr <= max_temp) & (new_df.xr >= min_temp), "y_calc"] = monotone(x, y, find_x)

        # as usual data manipulations after fitting
        new_df["optimal_y"] = (1.1 * gasConstant * new_df.xr / pressure)
        new_df["y_calc"] = new_df.apply(lambda
                                            row: row.optimal_y if pressure <= 10 and row.xr >= 100 and row.y_calc >= row.optimal_y else row.y_calc,
                                        axis=1)

        new_df["pressure"] = pressure
        dict_of_df[pressure] = new_df
    new_df = pd.concat(dict_of_df.values())
    new_df.sort_values(["pressure", "xr"], inplace=True)
    new_df.reset_index(inplace=True, drop=True)
    return new_df


def standardize_pressures(df):
    new_df = pd.DataFrame(index=standard_pressures, columns=standard_temperatures)
    x = list(df.pressure.unique())
    for i in range(len(standard_temperatures)):
        temperature = standard_temperatures[i]
        y = df.loc[df.xr == temperature, "y_calc"].tolist()
        find_xr = new_df.index.tolist()
        new_df[temperature] = monotone(x, y, find_xr)
    return new_df
