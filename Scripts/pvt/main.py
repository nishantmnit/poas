import argparse
import os
import sys

import settings
from Lib.DBLib import getDbCon
from Lib.Utils import getLogger
from Scripts.pvt.cm.ComponentModeling import *
from mdbr.BetaDistribution import *
from mdbr.GammaDistribution import *
from mdbr.GeneralDistribution import *
from mdbr.Validate import *
from omegatuning.omegaTuning import *
from solubility.Solubility import *


class main(Validate, object):
    def __init__(self):
        super(main, self).__init__()
        self.debugDir = None
        self.validEOS = ["pt", "generic_poas_a", "als", "poas_4a", "sw_poas", "pr", "srk"]
        self.args = self.getoptions()
        self.logger = getLogger(qualname='pvt_main_%s' % self.args.collection, master_module=True)
        self.logger.info(" ".join(sys.argv))
        self.db, self.coll = self.getDBConnection()

    def getDBConnection(self):
        db = getDbCon('MBP')
        if self.args.collection not in db.list_collection_names():
            raise Exception("%s well not defined in mongo. Please enter input data first." % self.args.collection)
        coll = db[self.args.collection]
        return db, coll

    def ensureDir(self, what):
        if what == "mass" or what == "density" or what == "bp" or what == "ri":
            where = "mdbr"
        else:
            where = what
        if self.args.debug:
            self.debugDir = os.path.join(settings.DEBUG_DIR, self.args.collection, self.args.bias, where)
            if not os.path.isdir(self.debugDir):
                os.makedirs(self.debugDir)
            self.logger.info("Writing debug output to - %s" % self.debugDir)
        else:
            pass

    def getoptions(self):
        parser = argparse.ArgumentParser()
        parser.add_argument('--collection', dest='collection', default='MC', help='Well collection name MC/MC1/MC2')
        parser.add_argument('--validate', dest='validate', default=False, action='store_true',
                            help='Provide this flag to just validate data.')
        parser.add_argument('--bias', dest='bias', default='general',
                            help='Bias - general/gamma/beta. Used in MDBR analysis')
        parser.add_argument('--mass', dest='mass', default=False, action='store_true',
                            help='Provide this flag for mass calculations.')
        parser.add_argument('--density', dest='density', default=False, action='store_true',
                            help='Provide this flag for density calculations.')
        parser.add_argument('--bp', dest='bp', default=False, action='store_true',
                            help='Provide this flag for bp calculations.')
        parser.add_argument('--ri', dest='ri', default=False, action='store_true',
                            help='Provide this flag for Refractivity calculations.')
        parser.add_argument('--cm', dest='cm', default=False, action='store_true',
                            help='Provide this flag for Component Modeling.')
        parser.add_argument('--eosbp', dest='eosbp', default=False, action='store_true',
                            help='Provide this flag for EOS at BP.')
        parser.add_argument('--tr', dest='tr', default=False, action='store_true',
                            help='Provide this flag for EOS at TR.')
        parser.add_argument('--sol', dest='sol', default=False, action='store_true',
                            help='Provide this flag for EOS Solubility.')
        parser.add_argument('--eos', dest='eos', default='all',
                            help='EOS to run after CM. POAS-4 is run as part of CM.')
        parser.add_argument('--temp', dest='utemp', default=380, help='Reservoir or reference temperature')
        parser.add_argument('--gas', dest='gas', default=False, action='store_true',
                            help='Pass this flag if the well is Gas Condensate')
        parser.add_argument('--debug', dest='debug', default=False, action='store_true', help='Run in debug mode')

        args = parser.parse_args()

        if not (args.mass or args.density or args.bp or args.ri or args.cm or args.eosbp or args.tr or args.sol or args.validate):
            print "Please provide at-least one flag to run the program"
            quit()
        if args.bias not in ["general", "gamma", "beta"]:
            print "Valid bias values are general/gamma and beta. %s" % args.bias
            quit()
        if (args.eosbp or args.tr or args.sol) and (
                args.eos not in self.validEOS + ["poas_4"] and args.eos != "all"):
            print "Valid EOS values are all, %s" % (", ".join(eos for eos in self.validEOS))
            quit()

        args.utemp = float(args.utemp)
        return args

    def validateInput(self):
        self.ensureDir("validation")
        df_ind, df_plus = self.validate()
        df_ind, df_plus = self.ExtendMF(df_ind, df_plus)
        self.save(df_ind, df_plus)

    def runMassAnalysis(self):
        self.ensureDir("mass")

        gd = GeneralDistribution(self, "mass")
        gd.logger.info("General mass distribution analysis started.")
        gd.optimize()
        gd.logger.info("General mass distribution analysis Completed.")

        gd1 = GammaDistribution(self, "mass")
        gd1.logger.info("Gamma mass distribution analysis started.")
        gd1.optimize()
        gd1.logger.info("Gamma mass distribution analysis Completed.")

        gd2 = BetaDistribution(self, "mass")
        gd2.logger.info("Beta mass distribution analysis started.")
        gd2.optimize()
        gd2.logger.info("Beta mass distribution analysis Completed.")

        gd2.logger.info("Mass - All distributions analysis Completed.")

    def runDensityAnalysis(self):
        self.ensureDir("density")
        gd = GeneralDistribution(self, "density")
        gd.logger.info("General density distribution analysis started.")
        gd.optimize()
        gd.logger.info("General density distribution analysis Completed.")

        gd1 = GammaDistribution(self, "density")
        gd1.logger.info("Gamma density distribution analysis started.")
        gd1.optimize()
        gd1.logger.info("Gamma density distribution analysis Completed.")

        gd2 = BetaDistribution(self, "density")
        gd2.logger.info("Beta density distribution analysis started.")
        gd2.optimize()
        gd2.logger.info("Beta density distribution analysis Completed.")

        gd2.logger.info("Density - All distributions analysis Completed.")

    def runBoilingPointAnalysis(self):
        self.ensureDir("bp")
        gd = GeneralDistribution(self, "bp")
        gd.logger.info("General Boiling Point distribution analysis started.")
        gd.optimize()
        gd.logger.info("General Boiling Point distribution analysis Completed.")

        gd1 = GammaDistribution(self, "bp")
        gd1.logger.info("Gamma Boiling Point distribution analysis started.")
        gd1.optimize()
        gd1.logger.info("Gamma Boiling Point distribution analysis Completed.")

        gd2 = BetaDistribution(self, "bp")
        gd2.logger.info("Beta Boiling Point distribution analysis started.")
        gd2.optimize()
        gd2.logger.info("Beta Boiling Point distribution analysis Completed.")

        gd2.logger.info("Boiling Point - All distributions analysis Completed!")

    def runRefractivityIndexAnalysis(self):
        self.ensureDir("ri")
        gd = GeneralDistribution(self, "ri")
        gd.logger.info("General Refractivity distribution analysis started.")
        gd.optimize()
        gd.logger.info("General Refractivity distribution analysis Completed.")

        gd1 = GammaDistribution(self, "ri")
        gd1.logger.info("Gamma Refractivity distribution analysis started.")
        gd1.optimize()
        gd1.logger.info("Gamma Refractivity distribution analysis Completed.")

        gd2 = BetaDistribution(self, "ri")
        gd2.logger.info("Beta Refractivity distribution analysis started.")
        gd2.optimize()
        gd2.logger.info("Beta Refractivity distribution analysis Completed.")

        gd2.logger.info("Refractivity All - distributions analysis Completed.")

    def runComponentModeling(self):
        self.ensureDir("cm")
        cm = ComponentModeling(self)
        cm.logger.info("Component Modeling started.")
        cm.runComponentModeling()
        cm.logger.info("Component Modeling completed.")

    def runEOSBp(self):
        self.ensureDir("eos")
        obj = eosMatching(self)
        obj.logger.info("EOS Bp Analysis started")
        obj.runEOSBp()
        obj.logger.info("EOS Bp Analysis completed")

    def runTr(self):
        self.ensureDir("tr")
        obj = omegaTuning(self)
        obj.logger.info("OmegaTuning Analysis started")
        obj.runOmegaTuningAnalysis(0.7)
        obj.logger.info("OmegaTuning Analysis completed")

    def runSolubility(self):
        self.ensureDir("sol")
        sol = Solubility(self)
        sol.logger.info("Solubility Analysis started")
        sol.runSolubilityAnalysis()
        sol.logger.info("Solubility Analysis completed")


if __name__ == "__main__":
    obj = main()
    try:
        if obj.args.validate:
            obj.validateInput()
        if obj.args.mass:
            obj.runMassAnalysis()
        if obj.args.density:
            obj.runDensityAnalysis()
        if obj.args.bp:
            obj.runBoilingPointAnalysis()
        if obj.args.ri:
            obj.runRefractivityIndexAnalysis()
        if obj.args.cm:
            obj.runComponentModeling()
        if obj.args.eosbp:
            obj.runEOSBp()
        if obj.args.tr:
            obj.runTr()
        if obj.args.sol:
            obj.runSolubility()
    except:
        obj.logger.exception('EXCEPTION IN MAIN!')
        raise
