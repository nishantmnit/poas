import numpy as np
import pandas as pd

from Scripts.pvt.core.constants import solubility_temperatures
from Scripts.pvt.core.storage.fetch_most import solubility_data
from solubility_param import solubility
from volume import volume_298


def prepare_input(well, logger, eos):
    volume = volume_298(well, logger, eos)
    sol = solubility(well, logger, volume.volume.tolist())

    for i in range(len(solubility_temperatures)):
        df = solubility_data(well, logger, eos, solubility_temperatures[i])[
            ["scn", "component", "v_liq", "temperature"]]
        df["solubility_%s" % solubility_temperatures[i]] = np.sqrt(((sol.sol_dispersion / (
                (volume["volume"] / df.v_liq) ** (-1.25))) ** 2 + (sol.sol_polar / (
                (volume["volume"] / df.v_liq) ** (-0.5))) ** 2 + (sol.sol_h_bonding / (np.exp(
            -1. * 1.32 * 10. ** -3. * (298.15 - solubility_temperatures[i]) - np.log(
                (volume["volume"] / df.v_liq) ** 0.5)))) ** 2).values)
        sol[["scn", "component", "t_%s" % i, "solubility_%s" % i]] = df[
            ["scn", "component", "temperature", "solubility_%s" % solubility_temperatures[i]]]
    return sol


def predict_alpha(well, logger, eos, temperature):
    sol = prepare_input(well, logger, eos)
    sol = smooth_alpha(well, logger, eos, sol)
    sol = intercept_slope(8, sol, "alpha_eos")
    sol["alpha_eos"] = np.exp(sol.slope * temperature + sol.intercept)
    sol["differentiation"] = sol.alpha_eos * (((2. * sol.alpha_m - 2.) / (temperature / sol.tc_k) + sol.alpha_l * (
            -2. * sol.alpha_m) * (temperature / sol.tc_k) ** (2. * sol.alpha_m - 1.)) / sol.tc_k)
    return sol[["scn", "component", "alpha_eos", "differentiation"]]


def intercept_slope(samples, df_o, y_col):
    df = df_o.copy(deep=True)
    for i in range(samples):
        df["x%s" % i] = df["t_%s" % i]
        df["y%s" % i] = np.log(df["%s_%s" % (y_col, i)])
        if i == 0:
            df["xi"] = df["x%s" % i]
            df["yi"] = df["y%s" % i]
            df["xiyi"] = df["x%s" % i] * df["y%s" % i]
            df["xi2"] = df["x%s" % i] ** 2
        else:
            df["xi"] = df["xi"] + df["x%s" % i]
            df["yi"] = df["yi"] + df["y%s" % i]
            df["xiyi"] = df["xiyi"] + (df["x%s" % i] * df["y%s" % i])
            df["xi2"] = df["xi2"] + df["x%s" % i] ** 2

    df_o["intercept"] = (df.xi * df.xiyi - df.yi * df.xi2) / (df.xi ** 2 - samples * df.xi2)
    df_o["slope"] = (df.yi - samples * df_o.intercept) / df.xi
    return df_o


def smooth_alpha(well, logger, eos, sol):
    df = pd.DataFrame()
    for i in range(len(solubility_temperatures)):
        df = solubility_data(well, logger, eos, solubility_temperatures[i])[
            ["scn", "v_liq", "solubility", "alpha_eos", "alpha_pr", "alpha_l", "alpha_m", "r1", "r2", "b", "ac", "t2",
             "t3", "tc_k", "alpha_multiplier"]]
        df["error"] = 100. * np.abs((sol["solubility_%s" % i] - df.solubility) / sol["solubility_%s" % i])
        df["t1"] = sol["solubility_%s" % i] ** 2 / (np.abs(df.t2 - df.t3))
        df["factor_k"] = (df.v_liq * df.t1 * df.b * (df.r2 - df.r1)) / df.ac
        df["factor_s"] = df.alpha_pr - df.alpha_pr * solubility_temperatures[i] * (((2. * df.alpha_m - 2.) / (
                solubility_temperatures[i] / df.tc_k) + df.alpha_l * (-2. * df.alpha_m) * (solubility_temperatures[
                                                                                               i] / df.tc_k) ** (
                                                                                            2. * df.alpha_m - 1.)) / df.tc_k)
        df["expected_alpha_multiplier"] = df.factor_k / df.factor_s
        df["smooth_alpha_multiplier"] = df.apply(lambda row: row.alpha_multiplier if row.solubility <= 5 else (
            row.expected_alpha_multiplier if row.error >= 5 else row.alpha_multiplier), axis=1)
        sol["alpha_eos_%s" % i] = df.smooth_alpha_multiplier * df.alpha_pr

    sol[["tc_k", "alpha_l", "alpha_m"]] = df[["tc_k", "alpha_l", "alpha_m"]]
    return sol


if __name__ == "__main__":
    import os
    from Lib.Utils import getLogger
    import Scripts.pvt.settings as settings

    well = "AB8"
    logger = getLogger(qualname='hc alpha')
    eos = "pt"
    print predict_alpha(well, logger, eos=eos, temperature=600)
