import numpy as np
import pandas as pd
from scipy.interpolate import pchip_interpolate

from Scripts.pvt.core.constants import solubility_temperatures
from Scripts.pvt.core.storage.fetch_most import solubility_data


def prepare_input(well, logger, eos):
    dfs = dict()
    if eos == "sw_poas":
        y_col = "omega_final"
    elif eos == "pt":
        y_col = "ziac"
    else:
        y_col = "om_b"
    for i in range(len(solubility_temperatures)):
        df = solubility_data(well, logger, eos, solubility_temperatures[i])[["scn", "temperature", y_col, "bp_final1"]]
        df[["t_%s" % i, "om_b_%s" % i]] = df[["temperature", y_col]]
        dfs[i] = df[["t_%s" % i, "om_b_%s" % i]]
    df = pd.concat(dfs.values(), axis=1)
    df.reset_index(inplace=True, drop=True)
    df[["scn", "component", "bp_final1", "zc_final"]] = solubility_data(well, logger, eos, solubility_temperatures[0])[
        ["scn", "component", "bp_final1", "zc_final"]]
    df["max_temp_phase_split"] = df.bp_final1 * (0.0996239690235248 * np.log(df.scn) + 1.0438832127323)

    om_b_cols = ["om_b_%s" % i for i in range(len(solubility_temperatures))]
    df["max_om_b"] = df[om_b_cols].max(axis=1)
    df["temperature_max_om_b"] = df.apply(lambda row: find_temperature_max_om_b(row), axis=1)
    return df


def find_temperature_max_om_b(row):
    om_b_cols = ["om_b_%s" % i for i in range(len(solubility_temperatures))]
    temp_cols = ["t_%s" % i for i in range(len(solubility_temperatures))]
    temp_col_max_om_b = temp_cols[row[om_b_cols].tolist().index(row.max_om_b)]
    return row[temp_col_max_om_b]


def predict_om_b_200(well, logger, eos, temperature):
    df = prepare_input(well, logger, eos)
    df = intercept_slope(3, df, "om_b")
    df["om_b"] = (np.exp(df.slope * temperature + df.intercept))
    return df


def predict_om_b_550(well, logger, eos, temperature):
    df = prepare_input(well, logger, eos)
    om_b_cols = ["om_b_%s" % i for i in range(len(solubility_temperatures))]
    t_cols = ["t_%s" % i for i in range(len(solubility_temperatures))]
    # pi = poas_interpolator()
    # df["om_b_poas"] = df.apply(lambda row: pi.monotone(row[t_cols].tolist(), row[om_b_cols].tolist(), [temperature])[0],
    #                            axis=1)
    df["om_b"] = df.apply(
        lambda row: pchip_interpolate(row[t_cols].tolist(), row[om_b_cols].tolist(), temperature), axis=1)
    return df


def predict_om_b_others(row, temperature, multiplier=0.985):
    tx1 = max(550, row.max_temp_phase_split)
    if row.temperature_max_om_b <= 350.:
        row["t_0"] = 400.
    elif row.temperature_max_om_b <= 400.:
        row["t_0"] = 475.
    elif row.temperature_max_om_b <= 475.:
        row["t_0"] = 550.
    else:
        row["t_0"] = 400.

    if row.t_0 == 400.:
        row["t_1"] = 475.
    elif row.t_0 <= 475.:
        row["t_1"] = 550.
    elif row.t_0 <= 550.:
        row["t_1"] = 600.

    if row.t_1 == 475.:
        row["t_2"] = 550.
    elif row.t_1 == 550.:
        row["t_2"] = 600.
    else:
        row["t_2"] = 650.

    if row.t_0 == 400:
        row["om_b_0"] = row.om_b_5
    elif row.t_0 == 475:
        row["om_b_0"] = row.om_b_6
    else:
        row["om_b_0"] = row.om_b_7

    if row.t_1 == 475:
        row["om_b_1"] = row.om_b_6
    elif row.t_1 == 550:
        row["om_b_1"] = row.om_b_7
    else:
        row["om_b_1"] = row.om_b_7 * multiplier

    if row.t_2 == 550:
        row["om_b_2"] = row.om_b_7
    else:
        row["om_b_2"] = row.om_b_1 * multiplier

    row = intercept_slope(3, row, "om_b")

    if temperature <= tx1:
        row["om_b"] = np.exp(row.slope * temperature + row.intercept)
        return row
    else:
        row["t_0"], row["t_1"], row["t_2"] = tx1, tx1 + 50., tx1 + 100.
        row["om_b_0"] = row.om_b_7 if tx1 == 550. else np.exp(row.slope * tx1 + row.intercept)
        row["om_b_1"] = row.om_b_0 * multiplier
        row["om_b_2"] = row.om_b_1 * multiplier
        row = intercept_slope(3, row, "om_b")
        row["om_b"] = np.exp(row.slope * temperature + row.intercept)
        return row


def predict_om_b(well, logger, eos, temperature):
    if eos in ["pr", "srk", "poas_4a", "als", "sw_poas", "pt"]:
        if temperature < 200:
            df = predict_om_b_200(well, logger, eos, temperature)
        elif temperature <= 550:
            df = predict_om_b_550(well, logger, eos, temperature)
        else:
            df = prepare_input(well, logger, eos)
            multiplier = 0.997 if eos == "als" else 0.985
            for index, row in df.iterrows():
                row = predict_om_b_others(row, temperature, multiplier)
                for col in row.index:
                    df.loc[df.index == index, col] = row[col]
        if eos == "poas_4a":
            df["beta_estimate"] = (0.79132 * df.zc_final - 0.02207) * 0.8
            df.loc[(df.zc_final - df.om_b) > 0, "beta_estimate"] = df.zc_final - df.om_b
            return df[["scn", "component", "intercept", "slope", "om_b", "beta_estimate"]]
        elif eos == "als":
            df[["omega_final", "omega_multiplier"]] = solubility_data(well, logger, eos, solubility_temperatures[0])[
                ["omega_final", "omega_multiplier"]]
            df["omega"] = df.omega_final / df.omega_multiplier
            df["term1"] = ((-0.03452) ** 2 - 4 * 0.0033 * (0.08974 - df.om_b))
            df["term2"] = ((0.03452 - np.sqrt((-0.03452) ** 2 - 4 * 0.0033 * (0.08974 - df.om_b))) / (2 * 0.0033))
            df["term3"] = ((0.03452 + np.sqrt((-0.03452) ** 2 - 4 * 0.0033 * (0.08974 - df.om_b))) / (2 * 0.0033))
            df["omega"] = df.apply(lambda row: row.omega if row.term1 < 0 else min(row.term2, row.term3), axis=1)
            return df[["scn", "component", "intercept", "slope", "om_b", "omega"]]
        elif eos == "sw_poas":
            df["omega"] = df.om_b
            return df[["scn", "component", "intercept", "slope", "omega"]]
        elif eos == "pt":
            df["ziac"] = df.om_b
            return df[["scn", "component", "intercept", "slope", "ziac"]]
        else:
            return df[["scn", "component", "intercept", "slope", "om_b"]]
    elif eos in ["generic_poas_a", "poas_4"]:
        df = prepare_input(well, logger, eos)
        df = intercept_slope(8, df, "om_b")
        df["om_b"] = (np.exp(df.slope * temperature + df.intercept))
        return df[["scn", "component", "intercept", "slope", "om_b"]]


def intercept_slope(samples, df_o, y_col):
    df = df_o.copy(deep=True)
    for i in range(samples):
        df["x%s" % i] = df["t_%s" % i]
        df["y%s" % i] = np.log(df["%s_%s" % (y_col, i)])
        if i == 0:
            df["xi"] = df["x%s" % i]
            df["yi"] = df["y%s" % i]
            df["xiyi"] = df["x%s" % i] * df["y%s" % i]
            df["xi2"] = df["x%s" % i] ** 2
        else:
            df["xi"] = df["xi"] + df["x%s" % i]
            df["yi"] = df["yi"] + df["y%s" % i]
            df["xiyi"] = df["xiyi"] + (df["x%s" % i] * df["y%s" % i])
            df["xi2"] = df["xi2"] + df["x%s" % i] ** 2

    df_o["intercept"] = (df.xi * df.xiyi - df.yi * df.xi2) / (df.xi ** 2 - samples * df.xi2)
    df_o["slope"] = (df.yi - samples * df_o.intercept) / df.xi
    return df_o


if __name__ == "__main__":
    import os
    from Lib.Utils import getLogger
    import Scripts.pvt.settings as settings

    well = "AB8"
    logger = getLogger(qualname='hc omb')
    eos = "pt"
    print predict_om_b(well, logger, eos=eos, temperature=2025)
