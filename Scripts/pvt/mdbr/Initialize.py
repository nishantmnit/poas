import math

from Scripts.pvt.core.storage.excel import write_excel
from Scripts.pvt.core.storage.fetch_most import validated_data, recommended
from Scripts.pvt.core.storage.mongo import fetch
from numpy import exp

from Validate import *
from Whitson import *


class Initialize(Validate, Whitson, object):
    def __init__(self, parent, what, solver, Distribution=None):
        self.__dict__ = parent.__dict__.copy()
        if Distribution in ("Gamma", "Beta"):
            if what == "mass":
                try:
                    self.df, self.dfplus = self.initializeMassGamma(solver, Distribution)
                except Exception as e:
                    self.logger.info('Error:{"Type": "Initialize", "Message": "Failed to initialize gamma mass '
                                     'solver. Please raise with POAS support team."} ')

                    raise
        else:
            if what == "mass":
                self.df, self.dfplus = self.initializeMass(solver)
            elif what == "density":
                self.df, self.dfplus = self.initializeDensity()
            elif what == "bp":
                self.df = self.initializeBp()
            elif what == "ri":
                self.df = self.initializeRi()

    def initializeMassGamma(self, solver, Distribution):
        df_o, dfplus_o = validated_data(self.args.collection, self.logger)
        df_o = self.average(df_o, df_o.index.min())
        try:
            df = fetch("MBP", self.args.collection, logger=self.logger, Property="MCOutput", Identifier="mass",
                       Distribution="General", Solver=5, PlusOrIndividual="Individual")
            dfplus = fetch("MBP", self.args.collection, logger=self.logger, Property="MCOutput", Identifier="mass",
                           Distribution="General", Solver=5, PlusOrIndividual="Plus")
            # df, dfplus = o.getMassDataAsDf("General", 5)
        except:
            df = fetch("MBP", self.args.collection, logger=self.logger, Property="MCOutput", Identifier="mass",
                       Distribution="General", Solver=6, PlusOrIndividual="Individual")
            dfplus = fetch("MBP", self.args.collection, logger=self.logger, Property="MCOutput", Identifier="mass",
                           Distribution="General", Solver=6, PlusOrIndividual="Plus")
            # df, dfplus = o.getMassDataAsDf("General", 6)
        df[["expmass", "expdensity"]] = df_o[["expmass", "expdensity"]]
        dfplus[["expmass", "expdensity"]] = dfplus_o[["expmass", "expdensity"]]
        added = False
        if df.index.max() not in dfplus.index:
            dfplus = pd.concat([dfplus, df.loc[df.index == df.index.max()]], axis=0, sort=True)
            added = True
        dfplus["expmass"] = dfplus["mass"]
        df["expmass"] = df["mass"]
        fs = df.loc[df.mf > 10 ** -5].index.min()
        df.drop(df.loc[df.index < fs].index, inplace=True)
        whitson = Whitson(self)
        desired = [7, 12, 20, 25, 30, 36] + dfplus.index.tolist() + [max(7, df.index.min())]
        desired = list(set(desired))
        desired = [scn for scn in desired if scn >= max(7, df.index.min())]
        desired.sort()
        dfplus = whitson.definePlusFractions(df, dfplus, desired)
        df[["expmass", "expdensity"]] = df_o[["expmass", "expdensity"]]
        if added:
            dfplus.drop([df.index.max()], inplace=True)

        df["scn"] = df.apply(lambda row: "C%s" % (row.name), axis=1)
        df.drop(columns=["expdensity"], inplace=True)
        dfplus.drop(columns=["expdensity"], inplace=True)
        df.rename(columns={'mass': 'mass5'}, inplace=True)
        dfplus.rename(columns={'mass': 'mass5'}, inplace=True)

        if solver > 2:
            df_2 = fetch("MBP", self.args.collection, logger=self.logger, Property="MCOutput", Identifier="mass",
                         Distribution="Gamma", Solver=2, PlusOrIndividual="Individual")
            dfplus_2 = fetch("MBP", self.args.collection, logger=self.logger, Property="MCOutput", Identifier="mass",
                             Distribution="Gamma", Solver=2, PlusOrIndividual="Plus")
            # df_2, dfplus_2 = o.getMassDataAsDf("Gamma", 2)
            df["mass2"] = df_2.mass

        write_excel(self.args.debug, self.debugDir, ["MassSolver", solver, Distribution],
                    ["Individual", "Plus"], [df, dfplus])
        # if self.args.debug == True:
        #     with pd.ExcelWriter("%s/MassSolver_%s_Input_%s.xlsx" %(self.debugDir, solver, Distribution)) as writer:
        #         df.to_excel(writer, sheet_name="Individual")
        #         dfplus.to_excel(writer, sheet_name="Plus")

        return df, dfplus

    def initializeMass(self, solver):
        df, dfplus = validated_data(self.args.collection, self.logger)
        df = self.average(df, df.index.min())
        whitson = Whitson(self)
        desired = range(4, dfplus.index.max() + 1) if solver == 1 else None
        dfplus = whitson.definePlusFractions(df, dfplus, desired)
        df.drop(df.loc[df.index >= dfplus.index.max()].index, inplace=True)
        df.drop(columns=["expbp", "expdensity", "expri"], inplace=True)
        if solver in [1, 5, 6]:
            df.drop([4], inplace=True)
            dfplus.drop([4], inplace=True)
        else:
            dfplus.drop([5], inplace=True)

            mf = fetch("MBP", self.args.collection, logger=self.logger, Property="MCOutput", Identifier="mass",
                       Distribution="General", Solver=5, PlusOrIndividual="Individual")
            mfplus = fetch("MBP", self.args.collection, logger=self.logger, Property="MCOutput", Identifier="mass",
                           Distribution="General", Solver=5, PlusOrIndividual="Plus")

            try:
                mf = fetch("MBP", self.args.collection, logger=self.logger, Property="MCOutput", Identifier="mass",
                           Distribution="General", Solver=5.0, PlusOrIndividual="Individual")
                mfplus = fetch("MBP", self.args.collection, logger=self.logger, Property="MCOutput", Identifier="mass",
                               Distribution="General", Solver=5.0, PlusOrIndividual="Plus")
            except:
                mf = fetch("MBP", self.args.collection, logger=self.logger, Property="MCOutput", Identifier="mass",
                           Distribution="General", Solver=6, PlusOrIndividual="Individual")
                mfplus = fetch("MBP", self.args.collection, logger=self.logger, Property="MCOutput", Identifier="mass",
                               Distribution="General", Solver=6, PlusOrIndividual="Plus")
            df = pd.concat([df, mf.loc[mf.index > df.index.max()]], axis=0, sort=True)
            df["scn"] = df.apply(lambda row: "C%s" % (row.name), axis=1)
            df.drop(columns=["mass"], inplace=True)
            dfplus["mf"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "mf"].sum(), axis=1)
            df['ziinside'] = df.mf.values / dfplus.loc[dfplus.index == 4, "mf"].values[0]
            df['cummziinside'] = df.ziinside.cumsum()
            dfplus['ziinside'] = 1 - dfplus.mf.values / dfplus.loc[dfplus.index == 4, "mf"].values[0]
            dfplus.loc[dfplus.ziinside <= 0, "ziinside"] = 10 ** -30
            dfplus['cummziinside'] = dfplus['ziinside']

        if solver in [3, 4]:
            df.rename(columns={'mf': 'expzi', 'ziinside': 'expziinside', 'cummziinside': 'expcummziinside'},
                      inplace=True)
            dfplus.rename(columns={'mf': 'expzi', 'ziinside': 'expziinside', 'cummziinside': 'expcummziinside'},
                          inplace=True)

        write_excel(self.args.debug, self.debugDir, ["MassSolver", solver, "Input"],
                    ["Individual", "Plus"], [df, dfplus])
        # if self.args.debug == True:
        #     with pd.ExcelWriter("%s/MassSolver_%s_Input.xlsx" %(self.debugDir, solver)) as writer:
        #         df.to_excel(writer, sheet_name="Individual")
        #         dfplus.to_excel(writer, sheet_name="Plus")
        return df, dfplus

    def initializeDensity(self):
        try:
            df_o, dfplus_o = validated_data(self.args.collection, self.logger)
            df_o = self.average(df_o, df_o.index.min())
            df, dfplus = recommended(self.args.collection, self.logger, "mass")
            df["expdensity"] = df_o.expdensity
            dfplus["expdensity"] = dfplus_o.expdensity
            df["zimi"] = df.mass * df.mf
            df["xw"] = df.zimi / df.zimi.sum()
            dfplus["mf"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "mf"].sum(), axis=1)
            dfplus["mass"] = dfplus.apply(
                lambda row: df.loc[df.index >= row.name, "zimi"].sum() / df.loc[df.index >= row.name, "mf"].sum(),
                axis=1)
            dfplus.drop(dfplus.loc[dfplus.expdensity.isnull()].index, inplace=True)
            df.drop(df.loc[df.index < 6].index, inplace=True)
        except Exception as e:
            self.logger.info(
                'Error:{"Type": "Initialize", "Message": "Failed to initialize density solver. Please run mass solver first."}')
            raise
        write_excel(self.args.debug, self.debugDir, ["DensitySolverInput"], ["Individual", "Plus"],
                    [df, dfplus])
        return df, dfplus

    def createLastFraction(self, df_o):
        df_v, dfplus_v = validated_data(self.args.collection, self.logger)
        ls = df_v.index.max()
        # create plus fraction where mf < 10**-8
        df_o["zimibysgi"] = df_o.zimi / df_o.density
        mp = df_o.loc[(df_o.mf < 10 ** -8) & (df_o.index > ls)].index.min() - 1
        if np.isnan(mp):
            return df_o
        else:
            mp = int(mp)
        mf, mass, zimi = df_o.loc[df_o.index >= mp, "mf"].sum(), df_o.loc[df_o.index >= mp, "zimi"].sum() / df_o.loc[
            df_o.index >= mp, "mf"].sum(), df_o.loc[df_o.index >= mp, "zimi"].sum()
        density = zimi / df_o.loc[df_o.index >= mp, "zimibysgi"].sum()
        temp_df = pd.DataFrame(data=[[mf, mass, zimi, "C%s" % (mp), density]],
                               columns=["mf", "mass", "zimi", "scn", "density"], index=[mp])
        df_o.drop(df_o.loc[df_o.index >= mp].index, inplace=True)
        df_o = pd.concat([df_o, temp_df], sort=True, axis=0)
        return df_o

    def initializeBp(self):
        try:
            df, dfplus = validated_data(self.args.collection, self.logger)
            dfmass, dfplusmass = recommended(self.args.collection, self.logger, "mass")
            dfdensity, dfplusdensity = recommended(self.args.collection, self.logger, "density")
            df = pd.concat([df, dfmass.loc[dfmass.index > df.index.max()]], axis=0, sort=True)
            df["mass"] = dfmass["mass"]
            df["density"] = dfdensity["density"]
            df.loc[df.index <= 5, "mass"] = df.loc[df.index <= 5, "expmass"]
            df.loc[df.index <= 5, "density"] = df.loc[df.index <= 5, "expdensity"]
            df["scn"] = df.apply(lambda row: "C%s" % (row.name), axis=1)
            df["zimi"] = df.mass * df.mf
            df = self.createLastFraction(df)
            df["xwi"] = df.zimi / df.zimi.sum()
            df["xwibysgi"] = df.xwi / df.density
            df["xvi"] = df.xwibysgi / df.xwibysgi.sum()
            df["cummxvi"] = df.xvi.cumsum()
            df["expbp"] = df.apply(
                lambda row: np.NaN if row.mass > 300 and math.isnan(float(row.expbp)) else self.calExpBPInDegK(row.mass,
                                                                                                               row.density,
                                                                                                               row.expbp),
                axis=1)
        except Exception as e:
            self.logger.info(
                'Error:{"Type": "Initialize", "Message": "Failed to initialize BP solver. Please run mass and density solvers first."}')
            raise
        write_excel(self.args.debug, self.debugDir, ["BPSolverInput"], ["Individual"], [df])
        return df

    def initializeRi(self):
        try:
            df, dfplus = validated_data(self.args.collection, self.logger)
            dfmass, dfplusmass = recommended(self.args.collection, self.logger, "mass")
            dfdensity, dfplusdensity = recommended(self.args.collection, self.logger, "density")
            df = pd.concat([df, dfmass.loc[dfmass.index > df.index.max()]], axis=0, sort=True)
            df["mass"] = dfmass["mass"]
            df["density"] = dfdensity["density"]
            df.loc[df.index <= 5, "mass"] = df.loc[df.index <= 5, "expmass"]
            df.loc[df.index <= 5, "density"] = df.loc[df.index <= 5, "expdensity"]
            df["scn"] = df.apply(lambda row: "C%s" % (row.name), axis=1)
            df["zimi"] = df.mass * df.mf
            df = self.createLastFraction(df)
            df["xwi"] = df.zimi / df.zimi.sum()
            df["xwibysgi"] = df.xwi / df.density
            df["xvi"] = df.xwibysgi / df.xwibysgi.sum()
            df["cummxvi"] = df.xvi.cumsum()
            df["expri"] = (df.expri ** 2 - 1.) / (df.expri ** 2 + 2.)
            df["expri"] = df.apply(
                lambda row: np.NaN if row.mass > 300 and math.isnan(float(row.expri)) else self.calExpRefractivity(
                    row.mass, row.density, row.expri), axis=1)
            df.drop(columns=["expbp"], inplace=True)
            df.rename(columns={'expri': 'expbp'}, inplace=True)
        except Exception as e:
            self.logger.info(
                'Error:{"Type": "Initialize", "Message": "Failed to initialize RI solver. Please run mass and density solvers first."}')
            raise
        write_excel(self.args.debug, self.debugDir, ["RISolverInput"], ["Individual"], [df])
        return df

    def calExpBPInDegK(self, mw, sg, bp=float('NaN')):
        if not math.isnan(float(bp)) and not bp == 0:
            return float(bp)
        else:
            return ((5.0 / 9.0) * (1928.3 - 1.695 * 10.0 ** 5.0 * mw ** (-0.03522) * sg ** 3.266 * (
                exp(-4.922 * 10.0 ** -3.0 * mw - 4.7685 * sg + 3.462 * 10.0 ** -3.0 * mw * sg)))) if mw < 300 else (
                    9.3369 * (
                exp(1.6514 * 10.0 ** -4.0 * mw + 1.4103 * sg - 7.5152 * 10.0 ** -4.0 * mw * sg)) * mw ** 0.5369 * sg ** -0.7276)

    def calExpRefractivity(self, mw, sg, ri=float('NaN')):
        if not math.isnan(float(ri)) and not ri == 0:
            return float(ri)
        else:
            return 0.12399 * exp(
                3.4622 * 10 ** -4 * mw + 0.90389 * sg - 6.0955 * 10 ** -4 * mw * sg) * mw ** 0.02264 * sg ** 0.22423
