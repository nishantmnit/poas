import numpy as np
from Scripts.pvt.core.storage.excel import write_excel
from Scripts.pvt.core.storage.fetch_most import set_recommended
from Scripts.pvt.core.storage.mongo import write, delete_all


class POASRi(object):
    def __init__(self, logger, debug, debugDir, well, what):
        self.logger = logger
        self.debug = debug
        self.debugDir = debugDir
        self.well = well
        self.what = what

    def run(self, df):
        self.logger.info("Executing POAS Ri Finder.")
        df.rename(columns={'expbp': 'expri'}, inplace=True)
        df["d60"] = df.density * 0.999016
        df["d20"] = ((df.d60 * 1000 * np.exp((-1 * (613.97226 / (df.d60 * 1000) ** 2) * (20 - 15) * (
                    1 + 0.8 * (613.97226 / (df.d60 * 1000) ** 2) * (20 - 15))))) / 1000)
        df["poas_ri"] = df.apply(lambda row: self.calcRi(row.d20), axis=1)
        self.save(df)
        return df

    def calcRi(self, d20):
        if d20 <= 0.65:
            return (-0.771988205690377 * d20 ** 2 + 1.25683786884901 * d20 - 0.263582391020546)
        elif d20 <= 0.685:
            return (0.323066708963071 * d20 ** 0.825367199519558)
        elif d20 <= 0.694:
            return (9.33747573594155 * d20 ** 2 - 12.6643155256813 * d20 + 4.52953399612808)
        elif d20 <= 0.7:
            return (45.2446673429513 * d20 ** 2 - 62.3150182598514 * d20 + 21.6929685696088)
        elif d20 <= 0.74:
            return (0.310905890442151 * d20 ** 0.697222127007676)
        elif d20 <= 0.9:
            return (0.131843282949603 * d20 ** 2 + 0.100833492150919 * d20 + 0.100895494309563 + 0.004)
        elif d20 <= 1.17:
            return (0.280866083905693 * d20 ** 2 - 0.209842167791342 * d20 + 0.254487058045577 + 0.009)
        elif d20 <= 2.8:
            return (-0.126906413488938 * d20 ** 2 + 0.651119502049113 * d20 - 0.185838323984245)
        else:
            return (0.247678193996086 * np.log(d20) + 0.375251277321885 + 0.012580930886738)

    def save(self, df):
        df["ri"] = df.poas_ri
        delete_all("MBP", self.well, self.logger, Identifier=self.what, Distribution="POAS")
        write_excel(self.debug, self.debugDir, ["POAS_ri-solver1"], ["Individual"], [df])
        write(df, "MBP", self.well, logger=self.logger, Property="MCOutput", Identifier=self.what,
              PlusOrIndividual="Individual", Solver=1, Recommended=0, Distribution="POAS")
        set_recommended(self.well, self.logger, self.what)
