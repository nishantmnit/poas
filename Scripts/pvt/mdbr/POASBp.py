import numpy as np
from Scripts.pvt.core.storage.excel import write_excel
from Scripts.pvt.core.storage.fetch_most import set_recommended
from Scripts.pvt.core.storage.mongo import write, delete_all


class POASBp(object):
    def __init__(self, logger, debug, debugDir, well, what):
        self.logger = logger
        self.debug = debug
        self.debugDir = debugDir
        self.well = well
        self.what = what

    def run(self, df):
        self.logger.info("Executing POAS Bp Finder.")
        df["xi"] = df.apply(lambda row: 0 if row.mass == 0 or row.density == 0 else np.log(row.mass / row.density),
                            axis=1)
        df["yi"] = df.apply(lambda row: 0 if row.expbp == 0 or row.density == 0 else np.log(row.expbp / row.density),
                            axis=1)

        # divide into two brackets, Bracket 1 (SCN6-10) and Bracket 2 (SCN 10-21). In case the Input data terminates before SCN 21, the 21 will be replaced by Last Terminating SCN-1
        ls = df.loc[~df.expbp.isnull()].index.max()
        intercepts, slopes = list(), list()
        for b in [6, 10], [10, min(21, ls)]:
            bracket = range(b[0], b[1] + 1)
            intercept, slope = self.intercept(df, bracket)
            intercepts.append(intercept)
            slopes.append(slope)
        df["poas_bp"] = df.apply(lambda row: row.expbp if row.name <= 6 else (
            (np.exp(slopes[0] * row.xi + intercepts[0]) * row.density) if row.name <= 10 else (
                        np.exp(slopes[1] * row.xi + intercepts[1]) * row.density)), axis=1)
        self.save(df)
        return df

    def intercept(self, df, bracket):
        xi = df.loc[df.index.isin(bracket), "xi"].sum()
        yi = df.loc[df.index.isin(bracket), "yi"].sum()
        xiyi = (df.loc[df.index.isin(bracket), "xi"] * df.loc[df.index.isin(bracket), "yi"]).sum()
        xi2 = (df.loc[df.index.isin(bracket), "xi"] ** 2).sum()
        yi2 = (df.loc[df.index.isin(bracket), "yi"] ** 2).sum()
        intercept = (xi * xiyi - yi * xi2) / (xi ** 2 - len(bracket) * xi2)
        slope = (yi - len(bracket) * intercept) / xi
        return intercept, slope

    def save(self, df):
        df["bp"] = df.poas_bp
        delete_all("MBP", self.well, self.logger, Identifier=self.what, Distribution="POAS")
        write_excel(self.debug, self.debugDir, ["POAS_bp-solver1"], ["Individual"], [df])
        write(df, "MBP", self.well, logger=self.logger, Property="MCOutput", Identifier=self.what,
              PlusOrIndividual="Individual", Solver=1, Recommended=0, Distribution="POAS")
        set_recommended(self.well, self.logger, self.what)
