import re

from Scripts.pvt.core.storage.excel import write_excel
from Scripts.pvt.core.storage.mongo import write

from Whitson import *


class Validate(object):
    def __init__(self):
        pass

    def individuals(self):
        sections = ["FlashedLiquid", "FlashedGas", "RecombinedFluid"]
        subsections = ["MolePercentage", "WeightPercentage"]
        data = list()
        for c in self.coll.find({'$and': [{'Property': 'MCInput'}]}):
            subdata = list()
            subdata.append(c["Component"])
            for section in sections:
                for subsection in subsections:
                    subdata.append(c[section][subsection])
            data.append(
                [c["SCN"]] + subdata + [c["BoilingPoint"], c["Density"], c["RefractiveIndex"], c["MolecularWeight"]])
        columns = ["scn", "component", "fl_mf", "fl_wf", "fg_mf", "fg_wf", "rf_mf", "rf_wf", "bp", "sg", "ri", "mw"]
        df = pd.DataFrame(data, columns=columns)
        df.fillna(value=pd.np.nan, inplace=True)
        df["short_name"] = df.scn.str.extract('(^c\d.*)', expand=True, flags=re.IGNORECASE)
        df["short_name"] = df.short_name.str.extract('(\d.*)', expand=True, flags=re.IGNORECASE)
        df["short_name"] = df.short_name.astype(float)
        df["short_name"] = df["short_name"].replace(np.nan, 0)
        df = df.sort_values(by=["short_name"])
        df.reset_index(inplace=True, drop=True)
        for column in ["fl_mf", "fg_mf", "rf_mf"]: df[column] = df[column] / 100.
        return df

    def plus(self):
        sections = ["FlashedLiquid", "FlashedGas", "RecombinedFluid"]
        subsections = ["MolePercentage", "WeightPercentage", "MolecularWeight", "Density"]
        data = list()
        for c in self.coll.find({'$and': [{'Property': 'MCCalc'}]}):
            subdata = list()
            for section in sections:
                for subsection in subsections:
                    subdata.append(c[section][subsection])
            data.append([c["Identifier"]] + subdata)
        columns = ["scn", "fl_mf", "fl_wf", "fl_mw", "fl_sg", "fg_mf", "fg_wf", "fg_mw", "fg_sg", "rf_mf", "rf_wf",
                   "rf_mw", "rf_sg", ]
        df = pd.DataFrame(data, columns=columns)
        df.fillna(value=pd.np.nan, inplace=True)
        df["short_name"] = df.scn.str.extract('(^c\d.*)', expand=True, flags=re.IGNORECASE)
        df["short_name"] = df.short_name.str.extract('(\d.*)', expand=True, flags=re.IGNORECASE)
        df["short_name"] = df.short_name.astype(float)
        df = df.sort_values(by=["short_name"])
        df.reset_index(inplace=True, drop=True)
        for column in ["fl_mf", "fg_mf", "rf_mf"]: df[column] = df[column] / 100.
        return df

    def whole(self):
        sections = ["FlashedLiquid", "FlashedGas", "RecombinedFluid"]
        subsections = ["MolecularWeight", "Density"]
        index, data = list(), list()
        for c in self.coll.find({'$and': [{'Property': 'WSAverage'}]}):
            subdata = list()
            for section in sections:
                for subsection in subsections:
                    subdata.append(c[section][subsection])
            data.append(subdata + [c["Nl"], c["Nv"]])
        columns = ["fl_mw", "fl_sg", "fg_mw", "fg_sg", "rf_mw", "rf_sg", "nl", "nv"]
        df = pd.DataFrame(data, columns=columns)
        df.fillna(value=pd.np.nan, inplace=True)
        return df

    def validate(self):
        df_ind, df_plus, df_wc = self.individuals(), self.plus(), self.whole()
        write_excel(self.args.debug, self.debugDir, ["InputDataFromUser"],
                    ["Individual", "Plus", "Whole Crude"], [df_ind, df_plus, df_wc])
        self.consistency(df_plus, df_ind)
        df_ind, df_plus, df_wc = self.validateWC(df_ind, df_plus, df_wc)
        if not df_ind.fl_wf.isnull().values.any():
            obj = WFtoMF(self.logger)
            df_ind, df_plus = obj.run(df_ind, df_plus, df_wc)
        df_ind, df_plus = self.validateMF(df_ind, df_plus)
        df_ind, df_plus = self.validateMasses(df_ind, df_plus, df_wc)
        df_ind, df_plus = self.rf_mf(df_ind, df_plus, df_wc)
        df_ind, df_plus = self.validateSG(df_ind, df_plus, df_wc)
        write_excel(self.args.debug, self.debugDir, ["ValidatedData"], ["Individual", "Plus"],
                    [df_ind, df_plus])
        self.save_for_PT(df_ind, df_plus)
        return df_ind, df_plus

    def ExtendMF(self, df_ind_o, df_plus_o):
        df_ind_o.loc[df_ind_o.rf_mf == 0, "rf_mf"] = 10 ** -20
        df_ind, df_plus = self.prepareData(df_ind_o, df_plus_o)
        # if df_plus.index.max()>=20:
        #     df_ind["user_mf"] = df_ind.mf
        #     df_plus["user_mf"] = df_plus.mf
        #     obj = ExtendMF()
        #     df_ind, df_plus = obj.extend(df_ind, df_plus, self.logger, self.debugDir)
        #     if self.debugDir is not None:
        #         with pd.ExcelWriter("%s/MFExtended.xlsx" %(self.debugDir)) as writer:
        #             df_ind.to_excel(writer, sheet_name="Individual")
        #             df_plus.to_excel(writer, sheet_name="Plus")
        return df_ind, df_plus

    def prepareData(self, df_ind_o, df_plus_o):
        df_ind, df_plus = pd.DataFrame(), pd.DataFrame()
        df_ind[["scn", "component", "mf", "expmass", "expdensity", "expbp", "expri"]] = df_ind_o.loc[
            df_ind_o.short_name >= 4, ["scn", "component", "rf_mf", "mw", "sg", "bp", "ri"]]
        df_plus[["scn", "mf", "expmass", "expdensity"]] = df_plus_o[["scn", "rf_mf", "rf_mw", "rf_sg"]]
        df_ind.index = df_ind_o.loc[df_ind_o.short_name >= 4, "short_name"].astype(int)
        df_plus.index = df_plus_o["short_name"].astype(int)
        df_ind = self.average(df_ind)
        return df_ind, df_plus

    def average(self, df_ind, start_scn=6):
        df = df_ind.loc[df_ind.index < start_scn]
        short_name, mf, mass, sg, bp, ri, user_mf = list(), list(), list(), list(), list(), list(), list()
        for index, row in df_ind.loc[df_ind.index >= start_scn].iterrows():
            if index not in short_name:
                short_name.append(index)
                mf.append(self.averageMF(df_ind, index))
                if "user_mf" in df_ind.columns:
                    user_mf.append(self.averageUserMF(df_ind, index))
                mass.append(self.averageMW(df_ind, index))
                sg.append(self.averageSG(df_ind, index))
                bp.append(self.averageBP(df_ind, index))
                ri.append(self.averageRI(df_ind, index))
        scn = ["C%s" % (x) for x in short_name]
        if "user_mf" in df_ind.columns:
            temp_df = pd.DataFrame(data=list(zip(mf, mass, sg, bp, ri, scn, user_mf)),
                                   columns=["mf", "expmass", "expdensity", "expbp", "expri", "scn", "user_mf"],
                                   index=short_name)
        else:
            temp_df = pd.DataFrame(data=list(zip(mf, mass, sg, bp, ri, scn)),
                                   columns=["mf", "expmass", "expdensity", "expbp", "expri", "scn"], index=short_name)
        df = pd.concat([df, temp_df], axis=0, sort=True)
        return df

    def averageMF(self, df, short_name):
        return df.loc[df.index == short_name, "mf"].sum()

    def averageUserMF(self, df, short_name):
        return df.loc[df.index == short_name, "user_mf"].sum()

    def averageMW(self, df_o, short_name):
        df = df_o.copy(deep=True)
        if df.loc[df.index == short_name, "expmass"].isnull().values.any(): return np.nan
        if df.loc[df.index == short_name].shape[0] == 1: return df.loc[df.index == short_name, "expmass"].values[0]
        df["zimi"] = df.mf * df.expmass
        return df.loc[df.index == short_name, "zimi"].sum() / df.loc[df.index == short_name, "mf"].sum()

    def averageSG(self, df_o, short_name):
        df = df_o.copy(deep=True)
        if df.loc[df.index == short_name, "expdensity"].isnull().values.any(): return np.nan
        if df.loc[df.index == short_name].shape[0] == 1: return df.loc[df.index == short_name, "expdensity"].values[0]
        df["zimi"] = df.mf * df.expmass
        df["zimibysg"] = df.zimi / df.expdensity
        return df.loc[df.index == short_name, "zimi"].sum() / df.loc[df.index == short_name, "zimibysg"].sum()

    def averageBP(self, df_o, short_name):
        df = df_o.copy(deep=True)
        if df.loc[df.index == short_name, "expbp"].isnull().values.any(): return np.nan
        if df.loc[df.index == short_name].shape[0] == 1: return df.loc[df.index == short_name, "expbp"].values[0]
        df["zimi"] = df.mf * df.expmass
        df.loc[df.index == short_name, "xwi"] = df.loc[df.index == short_name, "zimi"] / df.loc[
            df.index == short_name, "zimi"].sum()
        df["xwibysg"] = df.xwi / df.expdensity
        df.loc[df.index == short_name, "xvi"] = df.loc[df.index == short_name, "xwibysg"] / df.loc[
            df.index == short_name, "xwibysg"].sum()
        df["xvibytb"] = df.xvi * df.expbp
        return df.loc[df.index == short_name, "xvibytb"].sum()

    def averageRI(self, df_o, short_name):
        df = df_o.copy(deep=True)
        if df.loc[df.index == short_name, "expri"].isnull().values.any(): return np.nan
        if df.loc[df.index == short_name].shape[0] == 1: return df.loc[df.index == short_name, "expri"].values[0]
        df["zimi"] = df.mf * df.expmass
        df.loc[df.index == short_name, "xwi"] = df.loc[df.index == short_name, "zimi"] / df.loc[
            df.index == short_name, "zimi"].sum()
        df["xwibysg"] = df.xwi / df.expdensity
        df.loc[df.index == short_name, "xvi"] = df.loc[df.index == short_name, "xwibysg"] / df.loc[
            df.index == short_name, "xwibysg"].sum()
        df["xvibyri"] = df.xvi * df.expri
        return df.loc[df.index == short_name, "xvibyri"].sum()

    def consistency(self, df_plus, df_ind):
        ind_max = df_ind.short_name.max()
        plus_max = df_plus.short_name.max()
        if ind_max + 1 != plus_max:
            print 'Error:{"Type": "Validation", "Message": "Consistency check failed: Maximum individual short name should be one(1) less than Maximum plus fraction."}'
            quit()

    def validateWC(self, df_ind, df_plus, df_wc):
        obj = FixWC(self.logger)
        df_wc = obj.fix(df_ind, df_plus, df_wc)
        return df_wc

    def validateMF(self, df_ind, df_plus):
        obj = FixMF(self.logger)
        df_ind, df_plus = obj.fix(df_ind, df_plus)
        return df_ind, df_plus

    def validateMasses(self, df_ind, df_plus, df_wc):
        obj = FixMasses(self.logger)
        df_ind, df_plus = obj.fix(df_ind, df_plus, df_wc)
        return df_ind, df_plus

    def validateSG(self, df_ind, df_plus, df_wc):
        obj = FixSG(self.logger)
        df_ind, df_plus = obj.fix(df_ind, df_plus, df_wc)
        return df_ind, df_plus

    def rf_mf(self, df_ind, df_plus, df_wc):
        if df_ind.rf_mf.isnull().values.any() or df_plus.rf_mf.isnull().values.any():
            self.logger.info("Monophasic fluid mf not provided.")
            nl, nv = self.nlavailable(df_ind, df_plus, df_wc)
            if df_ind.fl_mf.isnull().values.any() or df_ind.fg_mf.isnull().values.any() or df_plus.fl_mf.isnull().values.any() or df_plus.fg_mf.isnull().values.any():
                print 'Error:{"Type": "Validation", "Message": "Insufficient data to run calculations. Please provide monophasic fluid MFs or other data to derive the values."}'
                quit()
            df_ind["rf_mf"] = df_ind.fl_mf * nl + df_ind.fg_mf * nv
            df_plus["rf_mf"] = df_plus.fl_mf * nl + df_plus.fg_mf * nv

            if not df_plus.fl_mw.isnull().values.any() and not df_plus.fl_mf.isnull().values.any() and not df_plus.fg_mw.isnull().values.any() and not df_plus.fg_mf.isnull().values.any() and not df_plus.rf_mf.isnull().values.any() and df_plus.rf_mw.isnull().values.any():
                df_plus["rf_mw"] = (
                                           df_plus.fl_mw * df_plus.fl_mf * nl + df_plus.fg_mw * df_plus.fg_mf * nv) / df_plus.rf_mf
            elif df_plus.rf_mw.isnull().values.any():
                print 'Error:{"Type": "Validation", "Message": "Insufficient data to run calculations. Please provide monophasic fluid MWs or other data to derive the values."}'
                quit()
            df_plus = self.plusMW(df_ind, df_plus, "rf_mf", "rf_mw")
        return df_ind, df_plus

    def plusMW(self, df_ind, df_plus, mf_column, mw_column):
        df_plus[mw_column] = df_plus.apply(lambda row: ((df_ind.loc[df_ind.short_name >= row.short_name, mf_column] *
                                                         df_ind.loc[df_ind.short_name >= row.short_name, "mw"]).sum() \
                                                        + self.getLastPlus(df_plus, mw_column) * self.getLastPlus(
                    df_plus, mf_column)) \
                                                       / (df_ind.loc[
                                                              df_ind.short_name >= row.short_name, mf_column].sum() + self.getLastPlus(
            df_plus, mf_column)) \
                                           , axis=1)
        df_plus[mw_column].fillna(0, inplace=True)
        return df_plus

    def getLastPlus(self, df_plus, column):
        return df_plus.loc[df_plus.short_name == df_plus.short_name.max(), column].values[0]

    def nlavailable(self, df_ind, df_plus, df_wc):
        if (df_wc.nl.isnull().values.any() and df_wc.nv.isnull().values.any()):
            obj = findNlNv(self.logger)
            nl, nv = obj.optimize(df_ind, df_plus, df_wc)
        else:
            nl, nv = df_wc.nl.values[0], df_wc.nv.values[0]
        return nl, nv

    def save(self, df, dfplus):
        write(df, "MBP", self.args.collection, logger=self.logger, Property="MCOutput", Identifier="ValidatedData",
              PlusOrIndividual="Individual")
        write(dfplus, "MBP", self.args.collection, logger=self.logger, Property="MCOutput", Identifier="ValidatedData",
              PlusOrIndividual="Plus")

    def save_for_PT(self, df, dfplus):
        write(df, "MBP", self.args.collection, logger=self.logger, Property="MCOutput", Identifier="InputValidatedData",
              PlusOrIndividual="Individual")
        write(dfplus, "MBP", self.args.collection, logger=self.logger, Property="MCOutput",
              Identifier="InputValidatedData", PlusOrIndividual="Plus")


class FixWC(object):
    def __init__(self, logger):
        self.logger = logger
        self.columns = ["fl", "fg", "rf"]

    def fix(self, df_ind, df_plus, df_wc):
        for col in self.columns:
            if self.missingMass(df_wc, col) and self.canBeCalculated(df_ind, df_plus, col):
                self.logger.info("Trying to find wc %s_mw" % (col))
                df_ind, df_plus, df_wc = self.calculateMW(df_ind, df_plus, df_wc, col)
            if self.missingSG(df_wc, col) and self.canBeCalculatedSG(df_ind, df_plus, col):
                self.logger.info("Trying to find wc %s_sg" % (col))
                df_wc = self.calculateSG(df_ind, df_plus, df_wc, col)
        return df_ind, df_plus, df_wc

    def missingMass(self, df_wc, col):
        if df_wc["%s_mw" % (col)].isnull().values.any():
            return True
        return False

    def missingSG(self, df_wc, col):
        if df_wc["%s_sg" % (col)].isnull().values.any():
            return True
        return False

    def canBeCalculated(self, df_ind, df_plus, col):
        if (not df_ind["%s_mf" % (col)].isnull().values.any() or not df_ind[
            "%s_wf" % (col)].isnull().values.any()) and not df_ind.mw.isnull().values.any() and (
                not df_plus["%s_mf" % (col)].isnull().values.any() or not df_plus[
            "%s_wf" % (col)].isnull().values.any()) and not df_plus["%s_mw" % (col)].isnull().values.any():
            return True
        return False

    def canBeCalculatedSG(self, df_ind, df_plus, col):
        if not df_ind["%s_mf" % (col)].isnull().values.any() and not df_ind.sg.isnull().values.any() and not df_plus[
            "%s_mf" % (col)].isnull().values.any() and not df_plus["%s_sg" % (col)].isnull().values.any():
            return True
        return False

    def calculateSG(self, df_ind, df_plus, df_wc, col):
        zimi = (df_ind["%s_mf" % (col)] * df_ind.mw).sum() + (
                df_plus.loc[df_plus.short_name == df_plus.short_name.max(), "%s_mf" % (col)] * df_plus.loc[
            df_plus.short_name == df_plus.short_name.max(), "%s_mw" % (col)]).sum()
        zimibysgi = (df_ind["%s_mf" % (col)] * df_ind.mw / df_ind.sg).sum() + (
                df_plus.loc[df_plus.short_name == df_plus.short_name.max(), "%s_mf" % (col)] * df_plus.loc[
            df_plus.short_name == df_plus.short_name.max(), "%s_mw" % (col)] / df_plus.loc[
                    df_plus.short_name == df_plus.short_name.max(), "%s_sg" % (col)]).sum()
        df_wc["%s_sg" % (col)] = zimi / zimibysgi
        return df_wc

    def calculateMW(self, df_ind, df_plus, df_wc, col):
        if not df_ind["%s_mf" % (col)].isnull().values.any() and not df_plus["%s_mf" % (col)].isnull().values.any():
            self.logger.info("Executing Method 1 to find out WC MW")
            zimi = (df_ind["%s_mf" % (col)] * df_ind.mw).sum() + (
                    df_plus.loc[df_plus.short_name == df_plus.short_name.max(), "%s_mf" % (col)] * df_plus.loc[
                df_plus.short_name == df_plus.short_name.max(), "%s_mw" % (col)]).sum()
            zi = df_ind["%s_mf" % (col)].sum() + df_plus.loc[
                df_plus.short_name == df_plus.short_name.max(), "%s_mf" % (col)].sum()
            df_wc["%s_mw" % (col)] = zimi / zi
        else:
            self.logger.info("Executing Method 2 to find out WC MW")
            res = optimize.shgo(self.cost_function, bounds=[[1., 10000.]], args=(df_ind, df_plus, df_wc, col), iters=5)
            if res.success == False:
                print 'Error:{"Type": "Validation", "Message": "Failed to derive whole crude molecular weights. Please check input data."}'
                quit()
            df_wc["%s_mw" % (col)] = res.x[0]
            df_ind, df_plus = self.calcZi(res.x, df_ind, df_plus, df_wc, col)
            df_ind["%s_mf" % (col)] = df_ind.zi
            df_plus["%s_mf" % (col)] = df_plus.calc_zi
            df_ind.drop(columns=["zimi", "zi"], inplace=True)
            df_plus.drop(columns=["zimi", "exp_zi", "calc_zi", "error"], inplace=True)
        return df_ind, df_plus, df_wc

    def cost_function(self, vars, df_ind_o, df_plus_o, df_wc, col):
        df_ind, df_plus = self.calcZi(vars, df_ind_o, df_plus_o, df_wc, col)
        return df_plus.error.sum()

    def calcZi(self, vars, df_ind_o, df_plus_o, df_wc, col):
        df_ind, df_plus = df_ind_o.copy(deep=True), df_plus_o.copy(deep=True)
        multiplier = 100. / (df_ind["%s_wf" % (col)].sum() + df_plus.loc[
            df_plus.short_name == df_plus.short_name.max(), "%s_wf" % (col)].sum())
        df_ind["%s_wf" % (col)] = df_ind["%s_wf" % (col)] * multiplier
        df_ind["zimi"] = df_ind["%s_wf" % (col)] * vars[0] / 100.
        df_ind["zi"] = df_ind.zimi / df_ind.mw
        df_plus["zimi"] = df_plus["%s_wf" % (col)] * vars[0] / 100.
        df_plus["exp_zi"] = df_plus.apply(lambda row: self.ExpZi(df_ind, row), axis=1)
        df_plus["calc_zi"] = df_plus.zimi / df_plus["%s_mw" % (col)]
        df_plus["calc_zi"].fillna(0, inplace=True)
        df_plus["error"] = np.abs(df_plus.exp_zi - df_plus.calc_zi)
        return df_ind, df_plus

    def ExpZi(self, df, row):
        index = df.loc[df.short_name <= row.short_name - 1].index.max()
        return 1. - df.loc[df.index < index, "zi"].sum()


class WFtoMF(object):
    def __init__(self, logger):
        self.logger = logger

    def run(self, df_ind, df_plus, df_wc):
        if (
                not df_ind.rf_wf.isnull().values.any() and not df_wc.rf_mw.isnull().values.any() and not df_ind.mw.isnull().values.any()) and (
                not df_plus.rf_wf.isnull().values.any() and not df_plus.rf_mw.isnull().values.any()):
            self.logger.info("Executing WF to MF Method 1 to determine mf.")
            df_ind["rf_mf"] = (df_ind.rf_wf * df_wc.rf_mw.values[0] / df_ind.mw) / 100.
            df_plus["rf_mf"] = (df_plus.rf_wf * df_wc.rf_mw.values[0] / df_plus.rf_mw) / 100.
        elif (not df_wc.nl.isnull().values.any() and not df_wc.nv.isnull().values.any()):
            self.logger.info("Executing WF to MF Method 2 to determine mf.")
            df_ind, df_plus = self.nlnv(df_ind, df_plus, df_wc, df_wc.nl.values[0], df_wc.nv.values[0])
        elif not df_wc.fl_mw.isnull().values.any() and not df_wc.fg_mw.isnull().values.any() and not df_wc.rf_mw.isnull().values.any():
            self.logger.info("Executing WF to MF Method 3 to determine mf.")
            nl = ((df_wc.rf_mw - df_wc.fg_mw) / (df_wc.fl_mw - df_wc.fg_mw)).values[0]
            nv = 1. - nl
            df_ind, df_plus = self.nlnv(df_ind, df_plus, df_wc, nl, nv)
        else:
            self.logger.info("Executing WF to MF Method 4 to determine mf.")
            res = optimize.shgo(self.cost_function, bounds=[[0.001, 1.]], args=(df_ind, df_plus, df_wc), iters=5)
            if res.success == False:
                print 'Error:{"Type": "Validation", "Message": "Failed to find optimal NL and NV values. Please provide nl and nv values or Monophasic fluid mf."}'
                quit()
            nl = res.x[0]
            nv = 1. - nl
            df_ind, df_plus = self.nlnv(df_ind, df_plus, df_wc, nl, nv)
        return df_ind, df_plus

    def nlnv(self, df_ind, df_plus, df_wc, nl, nv):
        if not df_ind.fl_wf.isnull().values.any() and not df_ind.fg_wf.isnull().values.any() and not df_plus.fl_wf.isnull().values.any() and not df_plus.fg_wf.isnull().values.any() and not df_wc.fg_mw.isnull().values.any() and not df_wc.fl_mw.isnull().values.any():
            df_ind["fl_mf"] = (df_ind.fl_wf * df_wc.fl_mw.values[0] / df_ind.mw) / 100.
            df_ind["fg_mf"] = (df_ind.fg_wf * df_wc.fg_mw.values[0] / df_ind.mw) / 100.
            df_plus["fl_mf"] = (df_plus.fl_wf * df_wc.fl_mw.values[0] / df_plus.fl_mw) / 100.
            df_plus["fg_mf"] = (df_plus.fg_wf * df_wc.fg_mw.values[0] / df_plus.fg_mw) / 100.
            df_plus.fl_mf.fillna(0, inplace=True)
            df_plus.fg_mf.fillna(0, inplace=True)
            df_ind["rf_mf"] = df_ind.fl_mf * nl + df_ind.fg_mf * nv
            df_plus["rf_mf"] = df_plus.fl_mf * nl + df_plus.fg_mf * nv
        else:
            print 'Error:{"Type": "Validation", "Message": "Insufficient data to calculate Monophasic MF."}'
            quit()
        return df_ind, df_plus

    def cost_function(self, vars, df_ind_o, df_plus_o, df_wc):
        df_ind, df_plus = df_ind_o.copy(deep=True), df_plus_o.copy(deep=True)
        nl = vars[0]
        nv = 1. - nl
        df_ind, df_plus = self.nlnv(df_ind, df_plus, df_wc, nl, nv)
        if not df_ind.rf_wf.isnull().values.any():
            df_ind["zimi"] = df_ind.rf_mf * df_ind.mw
            df_plus["zimi"] = df_plus.rf_mf * df_plus.rf_mw
            total = df_ind["zimi"].sum() + df_plus.loc[df_plus.short_name == df_plus.short_name.max(), "zimi"].sum()
            df_ind["error"] = np.abs(df_ind.rf_wf / 100. - df_ind.rf_mf / total)
            df_plus["error"] = np.abs(df_plus.rf_wf / 100. - df_plus.rf_mf / total)
            return df_ind.error.sum() + df_plus.error.sum()
        else:
            rf_mw = df_wc.fl_mw * nl + df_wc.fg_mw * nv
            df_plus["rf_mf"] = (rf_mw * df_plus.rf_wf / 100.) / df_plus.rf_mw
            df_plus["error"] = df_plus.apply(lambda row: np.abs(row.rf_mf - (1. - df_ind.loc[
                df_ind.index <= df_ind.loc[df_ind.short_name == row.short_name - 1].index.max(), "rf_mf"].sum())),
                                             axis=1)
            return df_plus.error.sum()


class findNlNv(object):
    def __init__(self, logger):
        self.logger = logger

    def optimize(self, df_ind, df_plus, df_wc):
        self.logger.info("NL and NV not provided. Trying to find NL and NV.")
        if not df_wc.fl_mw.isnull().values.any() and not df_wc.fg_mw.isnull().values.any() and not df_wc.rf_mw.isnull().values.any():
            self.logger.info("Calculating NL and NV from Whole crude Properties.")
            nl = ((df_wc.rf_mw - df_wc.fg_mw) / (df_wc.fl_mw - df_wc.fg_mw)).values[0]
            nv = 1. - nl
        elif df_ind.fl_mf.isnull().values.any() or df_ind.fg_mf.isnull().values.any() or df_plus.fl_mf.isnull().values.any() or df_plus.fg_mf.isnull().values.any() or df_plus.rf_mw.isnull().values.any() or df_wc.rf_mw.isnull().values.any():
            print 'Error:{"Type": "Validation", "Message": "Please provide Monophasic liquid mf or flashed liquid, gas mf and whole crude Monophasic molecular weight."}'
            quit()
        else:
            res = optimize.shgo(self.cost_function, bounds=[[0.001, 1.]], args=(df_ind, df_plus, df_wc), iters=5)
            if res.success == False:
                print 'Error:{"Type": "Validation", "Message": "Failed to find optimal NL and NV values. Please provide nl and nv values or Monophasic fluid mf."}'
                quit()
            nl = res.x[0]
            nv = 1. - nl
        self.logger.info("Found NL and NV.")
        return nl, nv

    def cost_function(self, vars, df_ind_o, df_plus_o, df_wc):
        df_ind, df_plus = df_ind_o.copy(deep=True), df_plus_o.copy(deep=True)
        nl = vars[0]
        nv = 1. - nl
        df_ind["wc_mf"] = df_ind.fl_mf * nl + df_ind.fg_mf * nv
        df_plus["wc_mf"] = df_plus.fl_mf * nl + df_plus.fg_mf * nv
        df_ind["zimi"] = df_ind.wc_mf * df_ind.mw
        df_plus["zimi"] = df_plus.wc_mf * df_plus.rf_mw
        mw = df_ind.zimi.sum() + df_plus.loc[df_plus.short_name == df_plus.short_name.max(), "zimi"].sum()
        mw = mw / (df_ind.wc_mf.sum() + df_plus.loc[df_plus.short_name == df_plus.short_name.max(), "wc_mf"].sum())
        return np.abs(df_wc.rf_mw.values[0] - mw)


class FixMF(object):
    def __init__(self, logger):
        self.mf_columns = ["rf_mf", "fl_mf", "fg_mf"]
        self.logger = logger

    def fix(self, df_ind, df_plus):
        for i in range(len(self.mf_columns)):
            mf_column = self.mf_columns[i]
            if not df_ind[mf_column].isnull().values.any() and not df_plus[mf_column].isnull().values.any():
                if not self.isone(df_ind, df_plus, mf_column):
                    self.logger.info("individual Mole Fractions sum is not 1 (%s). Trying to fix." % (mf_column))
                    res = optimize.shgo(self.cost_function, bounds=[[0.8, 1.2]], args=(df_ind, df_plus, mf_column),
                                        iters=5)
                    if res.success == False:
                        print 'Error:{"Type": "Validation", "Message": "individual and plus Mole Fractions sum should be 1. Please check and fix input data (%s) before proceeding further."}' % (
                            mf_column)
                        quit()
                    df_ind, df_plus = self.mf(res.x, df_ind, df_plus, mf_column)
                    if not self.isone(df_ind, df_plus, mf_column):
                        print 'Error:{"Type": "Validation", "Message": "individual and plus Mole Fractions sum should be 1. Please check and fix input data (%s) before proceeding further."}' % (
                            mf_column)
                        quit()
                    self.logger.info("individual Mole Fractions sum fixed (%s)." % (mf_column))
                else:
                    df_ind, df_plus = self.mf([1.], df_ind, df_plus, mf_column)
        return df_ind, df_plus

    def cost_function(self, vars, df_ind_o, df_plus_o, mf_column):
        df_ind, df_plus = df_ind_o.copy(deep=True), df_plus_o.copy(deep=True)
        df_ind, df_plus = self.mf(vars, df_ind, df_plus, mf_column)
        total = self.totalMF(df_ind, df_plus, mf_column)
        return np.abs(1. - total)

    def mf(self, vars, df_ind, df_plus, mf_column):
        df_ind[mf_column] = df_ind[mf_column] * vars[0]
        df_plus[mf_column] = df_plus[mf_column] * vars[0]
        df_plus = self.plusMF(df_ind, df_plus, mf_column)
        return df_ind, df_plus

    def totalMF(self, df_ind, df_plus, mf_column):
        return df_ind[mf_column].sum() + self.getLastPlus(df_plus, mf_column)

    def isone(self, df_ind, df_plus, mf_column):
        total = self.totalMF(df_ind, df_plus, mf_column)
        if total >= 0.9999 and total <= 1.0001: return True
        return False

    def plusMF(self, df_ind, df_plus, mf_column):
        df_plus[mf_column] = df_plus.apply(
            lambda row: df_ind.loc[df_ind.short_name >= row.short_name, mf_column].sum() + self.getLastPlus(df_plus,
                                                                                                            mf_column),
            axis=1)
        return df_plus

    def getLastPlus(self, df_plus, column):
        return df_plus.loc[df_plus.short_name == df_plus.short_name.max(), column].values[0]


class FixMasses(object):
    def __init__(self, logger):
        self.mf_columns = ["rf_mf", "fl_mf", "fg_mf"]
        self.mw_columns = ["rf_mw", "fl_mw", "fg_mw"]
        self.logger = logger

    def fix(self, df_ind, df_plus, df_wc):
        for i in range(len(self.mf_columns)):
            mf_column = self.mf_columns[i]
            mw_column = self.mw_columns[i]
            if not df_ind[mf_column].isnull().values.any() and not df_plus[mf_column].isnull().values.any() and not \
                    df_plus[mw_column].isnull().values.any() and not df_wc[mw_column].isnull().values.any():
                if not self.isone(df_ind, df_plus, df_wc, mf_column, mw_column):
                    self.logger.info(
                        "Individual, plus fractions and whole crude molecular weights do not tally (%s, %s). Trying to fix." % (
                            mf_column, mw_column))
                    res = optimize.shgo(self.cost_function, bounds=[[0.97, 1.5]],
                                        args=(df_ind, df_plus, df_wc, mf_column, mw_column), iters=5)
                    if res.success == False:
                        lb, ub = self.wc_mw([0.97], df_ind, df_plus, df_wc, mf_column, mw_column), self.wc_mw([1.2],
                                                                                                              df_ind,
                                                                                                              df_plus,
                                                                                                              df_wc,
                                                                                                              mf_column,
                                                                                                              mw_column)
                        self.logger.info("Fixing the masses failed.")
                        print 'Error:{"Type": "Validation", "Message": "Individual, plus fractions and whole crude molecular weights do not tally. Please check and fix input data (%s, %s) before proceeding further. Whole Crude mass should be between %s and %s."}' % (
                            mf_column, mw_column, lb, ub)
                        quit()
                    df_ind, df_plus = self.mw(res.x, df_ind, df_plus, mf_column, mw_column)
                    if not self.isone(df_ind, df_plus, df_wc, mf_column, mw_column):
                        self.logger.info("Solver converged, still the masses are not tallying.")
                        lb, ub = self.wc_mw([0.97], df_ind, df_plus, df_wc, mf_column, mw_column), self.wc_mw([1.2],
                                                                                                              df_ind,
                                                                                                              df_plus,
                                                                                                              df_wc,
                                                                                                              mf_column,
                                                                                                              mw_column)
                        print 'Error:{"Type": "Validation", "Message": "Individual, plus fractions and whole crude molecular weights do not tally. Please check and fix input data (%s, %s) before proceeding further. Whole Crude mass should be between %s and %s."}' % (
                            mf_column, mw_column, lb, ub)
                        quit()
                    self.logger.info("Individual, plus fractions and whole crude molecular weights fixed (%s, %s)." % (
                        mf_column, mw_column))
                else:
                    df_ind, df_plus = self.mw([1.], df_ind, df_plus, mf_column, mw_column)
                break
        return df_ind, df_plus

    def cost_function(self, vars, df_ind_o, df_plus_o, df_wc, mf_column, mw_column):
        mw = self.wc_mw(vars, df_ind_o, df_plus_o, df_wc, mf_column, mw_column)
        return np.abs(np.abs(100. * (df_wc[mw_column] - mw) / df_wc[mw_column])).values[0]

    def wc_mw(self, vars, df_ind_o, df_plus_o, df_wc, mf_column, mw_column):
        df_ind, df_plus = df_ind_o.copy(deep=True), df_plus_o.copy(deep=True)
        df_ind, df_plus = self.mw(vars, df_ind, df_plus, mf_column, mw_column)
        mw = self.totalMW(df_ind, df_plus, mf_column, mw_column)
        return mw

    def mw(self, vars, df_ind, df_plus, mf_column, mw_column):
        df_ind.loc[df_ind.short_name >= 6, "mw"] = df_ind.loc[df_ind.short_name >= 6, "mw"] * vars[0]
        df_plus[mw_column] = df_plus[mw_column] * vars[0]
        df_plus = self.plusMW(df_ind, df_plus, mf_column, mw_column)
        return df_ind, df_plus

    def totalMW(self, df_ind, df_plus, mf_column, mw_column):
        return (df_ind[mf_column] * df_ind.mw).sum() + self.getLastPlus(df_plus, mw_column) * self.getLastPlus(df_plus,
                                                                                                               mf_column) \
               / (df_ind[mf_column].sum() + self.getLastPlus(df_plus, mf_column))

    def isone(self, df_ind, df_plus, df_wc, mf_column, mw_column):
        mw = self.totalMW(df_ind, df_plus, mf_column, mw_column)
        self.logger.info("Given MW = %s, Calculated MW = %s. Error = %s" % (
            df_wc[mw_column].values[0], mw, np.abs(100. * (df_wc[mw_column] - mw) / df_wc[mw_column]).values[0]))
        if np.abs(100. * (df_wc[mw_column] - mw) / df_wc[mw_column]).values[0] <= 5.: return True
        return False

    def plusMW(self, df_ind, df_plus, mf_column, mw_column):
        df_plus[mw_column] = df_plus.apply(lambda row: ((df_ind.loc[df_ind.short_name >= row.short_name, mf_column] *
                                                         df_ind.loc[df_ind.short_name >= row.short_name, "mw"]).sum() \
                                                        + self.getLastPlus(df_plus, mw_column) * self.getLastPlus(
                    df_plus, mf_column)) \
                                                       / (df_ind.loc[
                                                              df_ind.short_name >= row.short_name, mf_column].sum() + self.getLastPlus(
            df_plus, mf_column)) \
                                           , axis=1)
        df_plus[mw_column].fillna(0, inplace=True)
        return df_plus

    def getLastPlus(self, df_plus, column):
        return df_plus.loc[df_plus.short_name == df_plus.short_name.max(), column].values[0]


class FixSG(object):
    def __init__(self, logger):
        self.mf_columns = ["rf_mf", "fl_mf", "fg_mf"]
        self.sg_columns = ["rf_sg", "fl_sg", "fg_sg"]
        self.mw_columns = ["rf_mw", "fl_mw", "fg_mw"]
        self.logger = logger

    def fix(self, df_ind, df_plus, df_wc):
        for i in range(len(self.mf_columns)):
            mf_column = self.mf_columns[i]
            sg_column = self.sg_columns[i]
            mw_column = self.mw_columns[i]
            if not df_ind[mf_column].isnull().values.any() and not df_plus[mf_column].isnull().values.any() and not \
                    df_plus[sg_column].isnull().values.any() and not df_plus[mw_column].isnull().values.any() and not \
                    df_wc[
                        sg_column].isnull().values.any():
                if not self.isone(df_ind, df_plus, df_wc, mf_column, sg_column, mw_column):
                    self.logger.info(
                        "Individual, plus fractions and whole crude specific gravity do not tally (%s, %s, %s). Trying to fix." % (
                            mf_column, sg_column, mw_column))
                    res = optimize.shgo(self.cost_function, bounds=[[0.97, 1.1]],
                                        args=(df_ind, df_plus, df_wc, mf_column, sg_column, mw_column), iters=5)
                    if res.success == False:
                        lb, ub = self.wc_sg([0.97], df_ind, df_plus, df_wc, mf_column, sg_column,
                                            mw_column), self.wc_sg([1.1], df_ind, df_plus, df_wc, mf_column, sg_column,
                                                                   mw_column)
                        print 'Error:{"Type": "Validation", "Message": "Individual, plus fractions and whole crude specific gravity do not tally. Please check and fix input data (%s, %s, %s). whole scrude SG should be between %s and %s."}' % (
                            mf_column, sg_column, mw_column, lb, ub)
                        quit()
                    df_ind, df_plus = self.sg(res.x, df_ind, df_plus, mf_column, sg_column, mw_column)
                    if not self.isone(df_ind, df_plus, df_wc, mf_column, sg_column, mw_column):
                        lb, ub = self.wc_sg([0.97], df_ind, df_plus, df_wc, mf_column, sg_column,
                                            mw_column), self.wc_sg([1.1], df_ind, df_plus, df_wc, mf_column, sg_column,
                                                                   mw_column)
                        print 'Error:{"Type": "Validation", "Message": "Individual, plus fractions and whole crude specific gravity do not tally. Please check and fix input data (%s, %s, %s). whole scrude SG should be between %s and %s."}' % (
                            mf_column, sg_column, mw_column, lb, ub)
                        quit()
                    self.logger.info(
                        "Individual, plus fractions and whole crude specific gravity fixed (%s, %s, %s)." % (
                            mf_column, sg_column, mw_column))
                else:
                    df_ind, df_plus = self.sg([1.], df_ind, df_plus, mf_column, sg_column, mw_column)
                break
        return df_ind, df_plus

    def cost_function(self, vars, df_ind_o, df_plus_o, df_wc, mf_column, sg_column, mw_column):
        sg = self.wc_sg(vars, df_ind_o, df_plus_o, df_wc, mf_column, sg_column, mw_column)
        return np.abs(np.abs(100. * (df_wc[sg_column] - sg) / df_wc[sg_column])).values[0]

    def wc_sg(self, vars, df_ind_o, df_plus_o, df_wc, mf_column, sg_column, mw_column):
        df_ind, df_plus = df_ind_o.copy(deep=True), df_plus_o.copy(deep=True)
        df_ind, df_plus = self.sg(vars, df_ind, df_plus, mf_column, sg_column, mw_column)
        sg = self.totalSG(df_ind, df_plus, mf_column, sg_column, mw_column)
        return sg

    def sg(self, vars, df_ind, df_plus, mf_column, sg_column, mw_column):
        df_ind.loc[df_ind.short_name >= 6, "sg"] = df_ind.loc[df_ind.short_name >= 6, "sg"] * vars[0]
        df_plus[sg_column] = df_plus[sg_column] * vars[0]
        df_plus = self.plusSG(df_ind, df_plus, mf_column, sg_column, mw_column)
        return df_ind, df_plus

    def totalSG(self, df_ind, df_plus, mf_column, sg_column, mw_column):
        df_ind["zimi"] = df_ind[mf_column] * df_ind.mw
        df_plus["zimi"] = df_plus[mf_column] * df_plus[mw_column]
        return ((df_ind.zimi).sum() + self.getLastPlus(df_plus, "zimi")) \
               / ((df_ind.zimi / df_ind.sg).sum() + self.getLastPlus(df_plus, "zimi") / self.getLastPlus(df_plus,
                                                                                                         sg_column))

    def isone(self, df_ind, df_plus, df_wc, mf_column, sg_column, mw_column):
        sg = self.totalSG(df_ind, df_plus, mf_column, sg_column, mw_column)
        self.logger.info("Given SG = %s, Calculated SG = %s. Error = %s" % (
            df_wc[sg_column].values[0], sg, np.abs(100. * (df_wc[sg_column] - sg) / df_wc[sg_column]).values[0]))
        if np.abs(100. * (df_wc[sg_column] - sg) / df_wc[sg_column]).values[0] <= 5.: return True
        return False

    def plusSG(self, df_ind, df_plus, mf_column, sg_column, mw_column):
        df_ind["zimi"] = df_ind[mf_column] * df_ind.mw
        df_plus["zimi"] = df_plus[mf_column] * df_plus[mw_column]
        df_plus[sg_column] = df_plus.apply(lambda row: (df_ind.loc[
                                                            df_ind.short_name >= row.short_name, "zimi"].sum() + self.getLastPlus(
            df_plus, "zimi")) \
                                                       / ((df_ind.loc[df_ind.short_name >= row.short_name, "zimi"] /
                                                           df_ind.loc[df_ind.short_name >= row.short_name, "sg"]).sum() \
                                                          + self.getLastPlus(df_plus, "zimi") / self.getLastPlus(
                    df_plus, sg_column)) \
                                           , axis=1)
        df_plus[sg_column].fillna(0, inplace=True)
        return df_plus

    def getLastPlus(self, df_plus, column):
        return df_plus.loc[df_plus.short_name == df_plus.short_name.max(), column].values[0]
