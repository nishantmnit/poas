import numpy as np


class DistributionAnalysis(object):
    # needed in new code
    def sortKeys(self, dict, after=None, before=None):
        if after == None and before == None:
            return [scn for scn in sorted(dict, key=lambda x: float(x))]
        elif after != None and before == None:
            result = list()
            for scn in sorted(dict, key=lambda x: float(x)):
                if float(scn) >= float(after):
                    result.append(scn)
            return result
        elif after != None and before != None:
            result = list()
            for scn in sorted(dict, key=lambda x: float(x)):
                if float(scn) >= float(after) and float(scn) < before:
                    result.append(scn)
            return result

    def getUniqueScn(self, scn, uniq):
        i = 1
        input_scn = scn
        while scn in uniq:
            scn = "%s.%s" % (input_scn, i)
            i += 1
        return scn

    def polyFitRSquare(self, x, y, degree):
        results = {}
        coeffs = np.polyfit(x, y, degree)
        results['polynomial'] = coeffs.tolist()
        # r-squared
        p = np.poly1d(coeffs)
        # fit values, and mean
        yhat = p(x)  # or [p(z) for z in x]
        ybar = np.sum(y) / len(y)  # or sum(y)/len(y)
        ssreg = np.sum((yhat - ybar) ** 2)  # or sum([ (yihat - ybar)**2 for yihat in yhat])
        sstot = np.sum((y - ybar) ** 2)  # or sum([ (yi - ybar)**2 for yi in y])
        results['determination'] = ssreg / sstot
        return results, p

    def findLastZi(self, df, col, maxzi):
        df["cummtemp"] = df[col].cumsum()
        maxscn = (df.loc[df.cummtemp >= maxzi].index.values)
        if len(maxscn) > 0:
            maxscn = maxscn[0]
            df.drop(df.loc[df.index > maxscn].index, inplace=True)
        else:
            maxscn = df.index.max()
        return maxscn, df

    def find_intersect_vec(self, x_down, y_down, x_up, y_up):
        p = np.column_stack((x_down, y_down))
        q = np.column_stack((x_up, y_up))
        p0, p1, q0, q1 = p[:-1], p[1:], q[:-1], q[1:]
        rhs = q0 - p0[:, np.newaxis, :]
        mat = np.empty((len(p0), len(q0), 2, 2))
        mat[..., 0] = (p1 - p0)[:, np.newaxis]
        mat[..., 1] = q0 - q1
        mat_inv = -mat.copy()
        mat_inv[..., 0, 0] = mat[..., 1, 1]
        mat_inv[..., 1, 1] = mat[..., 0, 0]
        det = mat[..., 0, 0] * mat[..., 1, 1] - mat[..., 0, 1] * mat[..., 1, 0]
        mat_inv /= det[..., np.newaxis, np.newaxis]
        import numpy.core.umath_tests as ut
        params = ut.matrix_multiply(mat_inv, rhs[..., np.newaxis])
        intersection = np.all((params >= 0) & (params <= 1), axis=(-1, -2))
        p0_s = params[intersection, 0, :] * mat[intersection, :, 0]
        return p0_s + p0[np.where(intersection)[0]]
