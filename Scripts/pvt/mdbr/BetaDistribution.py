from Scripts.pvt.core.optimizers.GenericSolver import *
from Scripts.pvt.core.storage.excel import write_excel
from Scripts.pvt.core.storage.fetch_most import validated_data, set_recommended, poas_recommended_density
from Scripts.pvt.core.storage.mongo import write, delete_all
from scipy.stats import beta as betaInbuilt

from DistributionAnalysis import *
from Initialize import *
from Validate import *


class BetaDistribution(DistributionAnalysis, object):
    def __init__(self, parent, what):
        self.__dict__ = parent.__dict__.copy()
        self.what = what

    def optimize(self):
        delete_all("MBP", self.args.collection, self.logger, Identifier=self.what, Distribution="Beta")
        objects = self.getOptimizeClass()
        for obj in objects:
            res, df, dfplus = obj.optimize()
            if type(res) != str:
                obj.save(res, df, dfplus)
                set_recommended(self.args.collection, self.logger, self.what)

    def getOptimizeClass(self):
        if self.what == "mass":
            return [MassSolver2(self), MassSolver3(self), MassSolver4(self)]
        elif self.what == "density":
            return [DensitySolver1(self)]
        elif self.what == "bp":
            return [BpSolver1(self)]
        elif self.what == "ri":
            return [RiSolver1(self)]


class Common(DistributionAnalysis, Validate, object):
    def __init__(self, parent):
        self.__dict__ = parent.__dict__.copy()

    def calcMass(self, df, alpha, eta, fmass):
        beta = self.calcBeta(alpha, eta, fmass)
        b_max = 15000.
        df["y"] = (df.mb - eta) / (b_max - eta)
        df.loc[df.y > 1., "y"] = 0.9999999999
        df["p0"] = df.apply(lambda row: betaInbuilt.cdf(row.y, alpha, beta), axis=1)
        df.loc[df.p0 > 1., "p0"] = 0.9999999999
        df["p1"] = df.apply(lambda row: betaInbuilt.cdf(row.y, alpha + 1, beta), axis=1)
        df.loc[df.p1 > 1., "p1"] = 0.9999999999
        df['p0diff'] = df['p0'].sub(df['p0'].shift())
        df['p0diff'] = df['p0diff'].fillna(df['p0'].iloc[0])
        df['p1diff'] = df['p1'].sub(df['p1'].shift())
        df['p1diff'] = df['p1diff'].fillna(df['p1'].iloc[0])
        df.loc[df.p0diff == 0, "p0diff"] = 1.
        df["mass"] = eta + (alpha / (alpha + beta)) * (b_max - eta) * df.p1diff.values / df.p0diff.values
        df["mass_Beta"] = df.mass
        for scn, row in df.iterrows():
            if (scn - 1.) in df.index:
                prow = df.loc[df.index == row.name - 1].squeeze()
                if row.mass < prow.mass:
                    df.loc[df.index == scn, "mass"] = row.mass5
        for scn, row in df.iterrows():
            if (scn + 1.) in df.index:
                nrow = df.loc[df.index == row.name + 1].squeeze()
                if row.mass > nrow.mass:
                    df.loc[df.index == scn, "mass"] = row.mass5
        return df

    def calcDensity(self, df, alpha, eta, beta):
        b_max = 3.
        df["y"] = (df.db - eta) / (b_max - eta)
        df.loc[df.y > 1., "y"] = 0.9999999999
        df["p0"] = df.apply(lambda row: betaInbuilt.cdf(row.y, alpha, beta), axis=1)
        df.loc[df.p0 > 1., "p0"] = 0.9999999999
        df["p1"] = df.apply(lambda row: betaInbuilt.cdf(row.y, alpha + 1, beta), axis=1)
        df.loc[df.p1 > 1., "p1"] = 0.9999999999
        df['p0diff'] = df['p0'].sub(df['p0'].shift())
        df['p0diff'] = df['p0diff'].fillna(df['p0'].iloc[0])
        df['p1diff'] = df['p1'].sub(df['p1'].shift())
        df['p1diff'] = df['p1diff'].fillna(df['p1'].iloc[0])
        df.loc[df.p0diff == 0, "p0diff"] = 1.
        df["density"] = eta + (alpha / (alpha + beta)) * (b_max - eta) * (df.p1diff.values) / (df.p0diff.values)
        df["density_Beta"] = df.density
        for scn, row in df.iterrows():
            if (scn - 1.) in df.index:
                prow = df.loc[df.index == row.name - 1].squeeze()
                if row.density < prow.poas_density:
                    df.loc[df.index == scn, "density"] = row.poas_density
        for scn, row in df.iterrows():
            if (scn + 1.) in df.index:
                nrow = df.loc[df.index == row.name + 1].squeeze()
                if row.density > nrow.poas_density:
                    df.loc[df.index == scn, "density"] = row.poas_density
        return df

    def calcBeta(self, alpha, eta, fmass):
        b_max = 15000.
        return alpha * (b_max - fmass) / (fmass - eta)

    def getuserInputData(self):
        return self.individuals(), self.plus(), self.whole()

    def getValidatedData(self):
        df, dfplus = validated_data(self.args.collection, self.logger)
        df = self.average(df, df.index.min())
        return df, dfplus

    def revertFractions(self, df_o, dfplus_o):
        df_ind, df_plus = self.getValidatedData()
        df_ind["mass"] = df_ind.expmass
        df_o = pd.concat([df_ind.loc[df_ind.index < df_o.index.min()], df_o], axis=0, sort=True)
        df_o["expmass"] = df_ind.expmass
        df_o["zimi"] = df_o.mf * df_o.mass
        dfplus_o.drop(dfplus_o.loc[(dfplus_o.index != 4) & (dfplus_o.index != 5)].index, inplace=True)
        dfplus = pd.DataFrame()
        dfplus[["expmass", "scn"]] = df_plus[["expmass", "scn"]]
        dfplus = pd.concat([dfplus, dfplus_o], sort=True)
        dfplus.sort_index(inplace=True)
        dfplus["mf"] = dfplus.apply(lambda row: df_o.loc[df_o.index >= row.name, "mf"].sum(), axis=1)
        dfplus["mass"] = dfplus.apply(
            lambda row: df_o.loc[df_o.index >= row.name, "zimi"].sum() / df_o.loc[df_o.index >= row.name, "mf"].sum(),
            axis=1)
        return df_o, dfplus


class MassSolver2(Initialize, object):
    def __init__(self, parent):
        self.__dict__ = parent.__dict__.copy()
        self.common = Common(self)

    def optimize(self):
        self.logger.info("Executing Beta Mass Solver 2")
        obj = Initialize(self, "mass", 2, "Beta")
        df, dfplus = obj.df, obj.dfplus
        sp, bnds = self.startPoint(df, dfplus)
        res, attempt = "Failed", 1
        while type(res) == str:
            res, df, dfplus = self.runSolver(df, dfplus, sp, bnds, attempt)
            attempt += 1
        self.logger.info("Completed Beta Mass Solver 2")
        self.logger.info(
            "Beta Mass solver final variables - %s, objective - %s" % (",".join([str(x) for x in res.x]), res.fun))
        return res, df, dfplus

    def runSolver(self, df, dfplus, sp, bnds, attempt):
        target = None if attempt == 1 else 1
        res = optimize.minimize(self.cost_function, sp, bounds=bnds, method="SLSQP", args=(df, dfplus, target),
                                options={'maxiter': 500})
        if not res.success:
            if attempt > 2:
                res.x = [1., 1., 1.]
                return res, df, dfplus
            else:
                print res
                self.logger.info("Beta Mass Solver 2 failed to find a feasible solution with Target = %s" % (target))
                return "Failed", df, dfplus
        return res, df, dfplus

    def startPoint(self, df_o, dfplus):
        df = df_o.copy(deep=True)
        df_o["mb"] = (df_o.mass5 + df_o.mass5.shift(-1)) / 2.
        df_o.loc[df_o.index == df_o.index.max(), "mb"] = 10000.
        df["ziinside"] = df.mf / df.mf.sum()
        df["ziinsidemi"] = df.ziinside * df.mass5
        df["variance"] = df.ziinside * (df.mass5 - df.ziinsidemi.sum()) ** 2
        variance = df.variance.sum()
        m_mass = df.ziinsidemi.sum()
        df["skewness"] = df.ziinside * ((df.ziinsidemi.sum() - df.mass5) / np.sqrt(variance)) ** 3
        skewness = df.skewness.sum()

        eta = max(df.mass5.head(1).values[0] * 0.98, 65.)
        b_max = 15000.
        d1 = (b_max - eta) / (m_mass - eta)
        alpha = np.abs(
            -1. / d1 ** 2 * (2. * d1 + (2. - d1) * 2. * (b_max - eta) / (np.abs(skewness) * np.sqrt(variance))))
        beta = alpha * (b_max - m_mass) / (m_mass - eta)

        alpha_bound = [max(alpha * 0.65, 0.35) / alpha,
                       max((1.96085987239067 * np.log(dfplus.expmass.head(1).values[0]) - 7.28596631070479), 2) / alpha]
        # mass BD multiplier, alpha multiplier, eta multiplier
        self.alpha, self.eta = alpha, eta
        return [1., max(alpha_bound[0] * 2.3, 2.3), 1.], [[0.98, 1.2], alpha_bound, [0.5, (
                np.mean(df.mass5.head(2).values) * 0.98 * (1. - 0.01 / 100.)) / eta]]

    def cost_function(self, vars, df_o, dfplus, target=None):
        df = df_o.copy(deep=True)
        df, dfplus = self.calculateMass(vars, df, dfplus)
        dfplus["error"] = ((dfplus.expmass - dfplus.mass) / dfplus.expmass) ** 2 / (0.001 if target is None else 1)
        error = dfplus.error.sum() + df.error.sum() + df.error.head(1).values[0] / (0.01 if target is None else 1)
        return error

    def calculateMass(self, vars, df, dfplus):
        mb_multi, alpha_multi, eta_multi = vars
        alpha, eta = self.alpha * alpha_multi, self.eta * eta_multi
        df["mb"] = df.mb * mb_multi
        df["zimi"] = df.mass5 * df.mf
        df = self.common.calcMass(df, alpha, eta, df.zimi.sum() / df.mf.sum())
        df["error"] = ((df.mass5 - df.mass) / df.mass5) ** 2
        df.loc[df.mass5 == 0, "error"] = 0
        df.loc[~df.expmass.isnull(), "mass"] = df.expmass
        df["zimi"] = df.mf * df.mass
        dfplus["mass"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "zimi"].sum() / row.mf, axis=1)
        return df, dfplus

    def save(self, res, df, dfplus):
        df, dfplus = self.calculateMass(res.x, df, dfplus)
        df, dfplus = self.common.revertFractions(df, dfplus)
        write_excel(self.args.debug, self.debugDir, ["mass-solver2_Beta"], ["Individual", "Plus"],
                    [df, dfplus])
        divisor = df.zimi.sum()
        df["wf"] = (df["mass"] * df["mf"]) / divisor
        X = {"Regression": 0, "Iterations": res.nit, "Values": list(res.x), "Message": res.message}
        write(df, "MBP", self.args.collection, logger=self.logger, Property="MCOutput", Identifier=self.what,
              PlusOrIndividual="Individual", Solver=2, Recommended=0, Distribution="Beta")
        write(dfplus, "MBP", self.args.collection, logger=self.logger, Property="MCOutput", Identifier=self.what,
              PlusOrIndividual="Plus", Solver=2, Recommended=0, Distribution="Beta")


class wismooth(object):
    def __init__(self, parent):
        self.__dict__ = parent.__dict__.copy()

    def smooth(self, df, dfplus, solver=3):
        self.logger.info("Executing Beta Mass Solver %s stage 1" % (solver))
        df["zimi"] = df.mass5 * df.mf
        df["wf"] = df.zimi / df.zimi.sum()
        df["slope"] = (df.mf - df.mf.shift(1))
        fs = max(8, df.index.min() + 1)
        avg_slope = np.mean(df.loc[df.index.isin([fs, fs + 1]), "slope"].tolist())
        swf = df.loc[df.index == fs - 1, "wf"].values[0] + avg_slope
        if not (df.loc[df.index == fs, "wf"].values[0] * 0.9 <= swf <= df.loc[df.index == fs, "wf"].values[
            0] * 1.1):
            df.loc[df.index == fs, "wf"] = swf
        if solver == 3:
            res = optimize.minimize(self.cost_function, [1.], bounds=[[0.8, 1.2]], method="SLSQP", args=(df, dfplus),
                                    options={'maxiter': 500})
            if not res.success:
                print res
                self.logger.info("Beta Mass Solver 3 stage 1 failed to find a feasible solution.")
                return "Failed"
            self.logger.info("Completed Beta Mass Solver 3 stage 1")
            self.logger.info("Beta Mass solver stage 1 final variables - %s, objective - %s" % (
                ",".join([str(x) for x in res.x]), res.fun))
            df = self.calcWf(res.x, df)
        else:
            df = self.calcWf([1.], df, solver)
        df["wf"] = df.new_wf
        df["wf"] = df.wf * (1. / df.wf.sum())
        df.drop(columns=["new_wf"], inplace=True)
        return df

    def cost_function(self, var, df_o, dfplus):
        df = df_o.copy(deep=True)
        df = self.calcWf(var, df)
        df["error"] = np.abs((df.wf - df.new_wf) / df.wf)
        df.loc[df.error > 10., "error"] = 0
        return df.error.sum() * 0.01

    def calcWf(self, var, df, solver=3):
        fs = max(8, df.index.min() + 1)
        fs = max(fs + 1, 10)
        df_s = df["wf"]
        for i in range(0, df.shape[0] - 5):
            ti = i + df.index.min()  # true index
            if ti < fs - 2: continue
            if solver == 3:
                df.loc[df.index == ti + 2, 'new_wf'] = (1. / 8. * df_s.iloc[i] + 1. / 4. * df_s.iloc[i + 1] + 1. / 4. *
                                                        df_s.iloc[i + 2] + 1. / 4. * df_s.iloc[i + 3] + 1. / 8. *
                                                        df_s.iloc[i + 4]) * var[0]
            else:
                df.loc[df.index == ti + 2, 'new_wf'] = np.median(
                    [df_s.iloc[i], df_s.iloc[i + 1], df_s.iloc[i + 2], df_s.iloc[i + 3], df_s.iloc[i + 4]])
        df.loc[df.new_wf.isnull(), "new_wf"] = df.wf
        return df


class MassSolver3(Initialize, object):
    def __init__(self, parent):
        self.__dict__ = parent.__dict__.copy()
        self.common = Common(self)

    def optimize(self):
        self.logger.info("Executing Beta Mass Solver 3")
        obj = Initialize(self, "mass", 3, "Beta")
        df, dfplus = obj.df, obj.dfplus
        obj = wismooth(self)
        df = obj.smooth(df, dfplus)
        if type(df) == str:
            print 'Warning:{"Type": "Mass Solver", "Message": "Beta Mass Solver 3 failed to find a feasible solution. ' \
                  'Executing Solver 4."} '
            return "Failed", df, dfplus
        res, df, dfplus = self.runSolver(df, dfplus)
        return res, df, dfplus

    def runSolver(self, df, dfplus, solver=3):
        df = self.calcNewMF(df)
        res = optimize.minimize(self.cost_function, [0.], bounds=[[-3., 3.]], method="SLSQP", args=(df, dfplus),
                                options={'maxiter': 500})
        if not res.success:
            print res
            self.logger.info("Beta Mass Solver %s failed to find a feasible solution." % (solver))
            res.x = [1.]
        self.logger.info("Completed Beta Mass Solver %s" % (solver))
        self.logger.info("Beta Mass solver %s final variables - %s, objective - %s" % (
            solver, ",".join([str(x) for x in res.x]), res.fun))
        return res, df, dfplus

    def calcNewMF(self, df):
        df["zimi"] = df.mass5 * df.mf
        df["new_mf"] = df.wf * df.zimi.sum() / df.mass5
        df["new_mf"] = df.new_mf * (df.mf.sum() / df.new_mf.sum())
        df["mf"] = df.new_mf
        df.drop(columns=["new_mf"], inplace=True)
        return df

    def cost_function(self, var, df_o, dfplus):
        df = df_o.copy(deep=True)
        df, dfplus = self.calculateMass(var, df, dfplus)
        dfplus["error"] = ((dfplus.expmass - dfplus.mass) / dfplus.expmass) ** 2 / 0.001
        error = dfplus.error.sum()
        return error

    def calculateMass(self, var, df, dfplus):
        df["correction"] = (4.81668632961298 * np.exp(0.0140674955555049 * df.index)) * var[0]
        df.loc[df.index <= 5, "mass"] = df.expmass
        df.loc[df.index > 5, "mass"] = df.mass2 + df.mass2 * df.correction / 100.
        df["zimi"] = df.mf * df.mass
        dfplus["mf"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "mf"].sum(), axis=1)
        dfplus["mass"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "zimi"].sum() / row.mf, axis=1)
        return df, dfplus

    def save(self, res, df, dfplus):
        df, dfplus = self.calculateMass(res.x, df, dfplus)
        df, dfplus = self.common.revertFractions(df, dfplus)
        write_excel(self.args.debug, self.debugDir, ["mass-solver3_Beta"], ["Individual", "Plus"],
                    [df, dfplus])
        divisor = df.zimi.sum()
        df["wf"] = (df["mass"] * df["mf"]) / divisor
        X = {"Regression": 0, "Iterations": res.nit, "Values": list(res.x), "Message": res.message}
        write(df, "MBP", self.args.collection, logger=self.logger, Property="MCOutput", Identifier=self.what,
              PlusOrIndividual="Individual", Solver=3, Recommended=0, Distribution="Beta")
        write(dfplus, "MBP", self.args.collection, logger=self.logger, Property="MCOutput", Identifier=self.what,
              PlusOrIndividual="Plus", Solver=3, Recommended=0, Distribution="Beta")


class MassSolver4(Initialize, object):
    def __init__(self, parent):
        self.__dict__ = parent.__dict__.copy()
        self.common = Common(self)

    def optimize(self):
        self.logger.info("Executing Beta Mass Solver 4")
        obj = Initialize(self, "mass", 4, "Beta")
        df, dfplus = obj.df, obj.dfplus
        obj = wismooth(self)
        df = obj.smooth(df, dfplus, 4)
        obj = MassSolver3(self)
        res, df, dfplus = obj.runSolver(df, dfplus, 4)
        return res, df, dfplus

    def save(self, res, df, dfplus):
        obj = MassSolver3(self)
        df, dfplus = obj.calculateMass(res.x, df, dfplus)
        df, dfplus = self.common.revertFractions(df, dfplus)
        write_excel(self.args.debug, self.debugDir, ["mass-solver4_Beta"], ["Individual", "Plus"],
                    [df, dfplus])
        divisor = df.zimi.sum()
        df["wf"] = (df["mass"] * df["mf"]) / divisor
        X = {"Regression": 0, "Iterations": res.nit, "Values": list(res.x), "Message": res.message}

        write(df, "MBP", self.args.collection, logger=self.logger, Property="MCOutput", Identifier=self.what,
              PlusOrIndividual="Individual", Solver=4, Recommended=0, Distribution="Beta")
        write(dfplus, "MBP", self.args.collection, logger=self.logger, Property="MCOutput", Identifier=self.what,
              PlusOrIndividual="Plus", Solver=4, Recommended=0, Distribution="Beta")


class DensitySolver1(Initialize, object):
    def __init__(self, parent):
        self.__dict__ = parent.__dict__.copy()
        self.common = Common(self)

    def getPoasDensity(self, df, dfplus):
        df_p, dfplus_p = poas_recommended_density(self.args.collection, self.logger)
        df["poas_density"] = df_p.density
        dfplus["poas_density"] = dfplus_p.density
        dfplus = self.definePlusFractions(df, dfplus)
        return df, dfplus

    def definePlusFractions(self, df, dfplus):
        desired = [7, 12, 20, 25, 30, 36] + dfplus.index.tolist() + [max(7, df.index.min())]
        desired = list(set(desired))
        desired = [scn for scn in desired if scn >= max(7, df.index.min())]
        desired.sort()
        df["zimi"] = df.mass * df.mf
        df["zimibysgi"] = df.zimi / df.poas_density
        dfplus_n = pd.DataFrame(index=desired, columns=["mf", "mass", "expdensity", "poas_density"])
        dfplus_n[["mf", "mass", "expdensity", "poas_density"]] = dfplus[["mf", "mass", "expdensity", "poas_density"]]
        dfplus_n["mf"] = dfplus_n.apply(lambda row: df.loc[df.index >= row.name, "mf"].sum(), axis=1)
        dfplus_n["mass"] = dfplus_n.apply(lambda row: df.loc[df.index >= row.name, "zimi"].sum() / row.mf, axis=1)
        dfplus_n["expdensity"] = dfplus_n.apply(lambda row: df.loc[df.index >= row.name, "zimi"].sum() / df.loc[
            df.index >= row.name, "zimibysgi"].sum() if np.isnan(row.expdensity) else row.expdensity, axis=1)
        dfplus_n["poas_density"] = dfplus_n.apply(lambda row: df.loc[df.index >= row.name, "zimi"].sum() / df.loc[
            df.index >= row.name, "zimibysgi"].sum() if np.isnan(row.poas_density) else row.poas_density, axis=1)
        return dfplus_n

    def optimize(self):
        self.logger.info("Executing Beta Density Solver 1")
        obj = Initialize(self, "density", 1)
        df, dfplus = obj.df, obj.dfplus
        df, dfplus = self.getPoasDensity(df, dfplus)
        sp, bnds = self.startPoint(df, dfplus)
        res = optimize.minimize(self.cost_function, sp, bounds=bnds, method="SLSQP", args=(df, dfplus),
                                options={'maxiter': 500})
        if not res.success:
            print res
            self.logger.info("Beta Density Solver failed to find a feasible solution.")
            res.x = [1., 1., 1., 1.]
        self.logger.info("Completed Beta Density Solver 1")
        self.logger.info(
            "Beta Density solver final variables - %s, objective - %s" % (",".join([str(x) for x in res.x]), res.fun))
        return res, df, dfplus

    def startPoint(self, df_o, dfplus):
        df = df_o.copy(deep=True)
        df["zimi"] = df.mass * df.mf
        df["xwi"] = df.zimi / df.zimi.sum()
        df["xwsgi"] = df.poas_density * df.xwi
        df["af"] = df.xwi * (df.xwsgi.sum() - df.poas_density) ** 2
        df["skewness"] = df.xwi * ((df.xwsgi.sum() - df.poas_density) / np.sqrt(df.af.sum())) ** 3
        variance = df.af.sum()
        s_mean = df.xwsgi.sum()
        skewness = df.skewness.sum()
        eta = min(0.6, df.loc[df.index == 6, "poas_density"].values[0] * 0.98)

        b_max = 3.
        d1 = (b_max - eta) / (s_mean - eta)
        alpha = np.abs(
            -1. / d1 ** 2 * (2. * d1 + (2. - d1) * 2. * (b_max - eta) / (np.abs(skewness) * np.sqrt(variance))))
        beta = alpha * (b_max - s_mean) / (s_mean - eta)

        # mass boundary, alpha, beta, eta
        self.eta, self.alpha, self.beta = eta, alpha, beta
        df_o["db"] = (df_o.poas_density + df_o.poas_density.shift(-1)) / 2.
        bounds = [[0.98, 1.2], [0.2, 3.], [0.2, 3], [0.4, (df_o.db.min() * 0.98 * (1. - 0.01 / 100.)) / eta]]
        return [1., 1., 1., 1.], bounds

    def save(self, res, df, dfplus):
        df, dfplus = self.calculateDensities(res.x, df, dfplus)
        write_excel(self.args.debug, self.debugDir, ["density-solver1_Beta"], ["Individual", "Plus"],
                    [df, dfplus])
        write(df, "MBP", self.args.collection, logger=self.logger, Property="MCOutput", Identifier=self.what,
              PlusOrIndividual="Individual", Solver=1, Recommended=0, Distribution="Beta")
        write(dfplus, "MBP", self.args.collection, logger=self.logger, Property="MCOutput", Identifier=self.what,
              PlusOrIndividual="Plus", Solver=1, Recommended=0, Distribution="Beta")

    def cost_function(self, vars, df_o, dfplus_o):
        df, dfplus = df_o.copy(deep=True), dfplus_o.copy(deep=True)
        df, dfplus = self.calculateDensities(vars, df, dfplus)
        dfplus["error"] = np.abs((dfplus.expdensity - dfplus.density) / dfplus.expdensity) / 0.01
        error = dfplus.error.sum() + df.loc[df.index > 6].error.sum() + df.error.head(1).values[0] / 0.01
        lid = df.loc[~df.expdensity.isnull()].index.max()
        if not np.isnan(lid):
            df["exp_error"] = np.abs((df.expdensity - df.density_Beta) / df.expdensity)
            error += df.exp_error.sum() / 0.05
            error += df.loc[df.index >= lid - 2, "exp_error"].sum() / 0.01
        return error

    def targetCost(self, val, target):
        return val / target

    def calculateDensities(self, vars, df, dfplus):
        db_multi, alpha_multi, beta_multi, eta_multi = vars
        alpha, beta, eta = self.alpha * alpha_multi, self.beta * beta_multi, self.eta * eta_multi
        # density calculations
        df["db"] = df.db * db_multi
        df.loc[df.index == df.index.max(), "db"] = max(2.8,
                                                       df.loc[df.index == df.index.max(), "poas_density"].values[0])
        df = self.common.calcDensity(df, alpha, eta, beta)

        df["error"] = np.abs((df.poas_density - df.density) / df.poas_density)
        df.loc[~df.expdensity.isnull(), "density"] = df.expdensity

        # smoothing
        df["diff"] = df.expdensity - df.density
        lid = df.loc[~df.expdensity.isnull()].index.max()
        if not np.isnan(lid) and lid >= 9:
            avg_diff = np.mean(df.loc[(df.index >= lid - 1) & (df.index <= lid), "diff"].tolist())
            df.loc[df.index > lid, "density"] = df.loc[df.index > lid].apply(
                lambda row: 0 if row.density == 0 else row.density + avg_diff, axis=1)

        df["zimi"] = df.mass * df.mf
        df["zimibysgi"] = df.apply(lambda row: 0 if row.density == 0 else row.zimi / row.density, axis=1)
        dfplus["density"] = dfplus.apply(
            lambda row: df.loc[df.index >= row.name, "zimi"].sum() / df.loc[df.index >= row.name, "zimibysgi"].sum(),
            axis=1)
        return df, dfplus


class BpSolver1(Initialize, object):
    def __init__(self, parent):
        self.__dict__ = parent.__dict__.copy()
        self.common = Common(self)

    def optimize(self):
        obj = Initialize(self, "bp", 1)
        df = obj.df
        df = self.getPoasBp(df)
        sp, bnds = self.startPoint(df)
        res = optimize.minimize(self.cost_function, sp, bounds=bnds, method="SLSQP", args=(df),
                                options={'maxiter': 500})
        if not res.success:
            print res
            self.logger.info("Beta BP Solver failed to find a feasible solution.")
            res.x = [1., 1., 1., 1.]
        self.logger.info("Completed Beta BP Solver 1")
        self.logger.info(
            "Beta BP solver final variables - %s, objective - %s" % (",".join([str(x) for x in res.x]), res.fun))
        return res, df, None

    def getPoasBp(self, df):
        df_p = fetch("MBP", self.args.collection, logger=self.logger, Property="MCOutput", Identifier="bp",
                     PlusOrIndividual="Individual", Distribution="POAS", Solver=1)
        df["poas_bp"] = df_p.bp
        return df

    def startPoint(self, df_o):
        df = df_o.copy(deep=True)
        df["xvibp"] = df.xvi * df.poas_bp
        smean = df.xvibp.sum()
        df["variance"] = df.xvi * (smean - df.poas_bp) ** 2
        variance = df.variance.sum()
        df["skewness"] = df.xvi * ((smean - df.poas_bp) / np.sqrt(variance)) ** 3
        skewness = df.skewness.sum()

        eta = min(255., df.poas_bp.head(1).values[0] * 0.98)
        b_max = max(3000., 1.25 * df.poas_bp.tail(1).values[0])
        d1 = (b_max - eta) / (smean - eta)
        alpha = np.abs(
            -1. / d1 ** 2 * (2. * d1 + (2. - d1) * 2. * (b_max - eta) / (np.abs(skewness) * np.sqrt(variance))))
        beta = alpha * (b_max - smean) / (smean - eta)
        # mass boundary, alpha, beta, eta
        self.eta, self.alpha, self.beta = eta, alpha, beta
        bounds = [[0.98, 1.2], [0.2, 3.], [0.2, 3],
                  [0.5, (np.mean(df.poas_bp.head(2).values) * 0.98 * (1. - 0.01 / 100.)) / eta]]
        df_o["bb"] = (df_o.poas_bp + df_o.poas_bp.shift(-1)) / 2.
        df.loc[df.index == df.index.max(), "bb"] = max(2800.,
                                                       df.loc[df.index == df.index.max(), "poas_bp"].values[0] * 1.1)
        return [1., 1., 1., 1.], bounds

    def save(self, res, df, dfplus):
        df = self.calculateBp(res.x, df)
        write_excel(self.args.debug, self.debugDir, ["bp-solver1_Beta"], ["Individual"], [df])
        write(df, "MBP", self.args.collection, logger=self.logger, Property="MCOutput", Identifier=self.what,
              PlusOrIndividual="Individual", Solver=1, Recommended=0, Distribution="Beta")

    def cost_function(self, vars, df_o):
        df = df_o.copy(deep=True)
        df = self.calculateBp(vars, df)
        error = df.exp_error.sum() / 0.01 + df.poas_error.sum() / 0.1
        return error

    def calculateBp(self, vars, df):
        bb_multi, alpha_multi, beta_multi, eta_multi = vars
        alpha, beta, eta = self.alpha * alpha_multi, self.beta * beta_multi, self.eta * eta_multi
        # bp calculations
        df["bb"] = df.bb * bb_multi

        b_max = max(3000., 1.25 * df.poas_bp.tail(1).values[0])
        df["y"] = (df.bb - eta) / (b_max - eta)
        df.loc[df.y > 1., "y"] = 0.9999999999
        df["p0"] = df.apply(lambda row: betaInbuilt.cdf(row.y, alpha, beta), axis=1)
        df.loc[df.p0 > 1., "p0"] = 0.9999999999
        df["p1"] = df.apply(lambda row: betaInbuilt.cdf(row.y, alpha + 1, beta), axis=1)
        df.loc[df.p1 > 1., "p1"] = 0.9999999999
        df['p0diff'] = df['p0'].sub(df['p0'].shift())
        df['p0diff'] = df['p0diff'].fillna(df['p0'].iloc[0])
        df['p1diff'] = df['p1'].sub(df['p1'].shift())
        df['p1diff'] = df['p1diff'].fillna(df['p1'].iloc[0])
        df.loc[df.p0diff == 0, "p0diff"] = 1.
        df["bp"] = eta + (alpha / (alpha + beta)) * (b_max - eta) * (df.p1diff.values) / (df.p0diff.values)
        df["bp_Beta"] = df.bp

        df["seq"] = range(0, df.shape[0])

        for scn, row in df.iterrows():
            if (row.seq - 1.) in df.seq.tolist():
                prow = df.loc[df.seq == row.seq - 1].squeeze()
                if row.bp < prow.bp:
                    df.loc[df.seq == row.seq, "bp"] = row.poas_bp
        for scn, row in df.iterrows():
            if (row.seq + 1.) in df.seq.tolist():
                nrow = df.loc[df.seq == row.seq + 1].squeeze()
                if row.bp > nrow.bp:
                    df.loc[df.seq == row.seq, "bp"] = row.poas_bp

        df["exp_error"] = ((df.expbp - df.bp) / df.expbp) ** 2
        df["poas_error"] = ((df.poas_bp - df.bp) / df.poas_bp) ** 2
        df.scn = df.index
        df.index = df.seq
        df.loc[df.index <= 5, "bp"] = df.loc[df.index <= 5, "expbp"]
        df.index = df.scn
        return df


class RiSolver1(Initialize, object):
    def __init__(self, parent):
        self.__dict__ = parent.__dict__.copy()
        self.common = Common(self)

    def optimize(self):
        obj = Initialize(self, "ri", 1)
        df = obj.df
        df = self.getPoasRi(df)
        sp, bnds = self.startPoint(df)
        res = optimize.minimize(self.cost_function, sp, bounds=bnds, method="SLSQP", args=(df),
                                options={'maxiter': 500})
        if not res.success:
            print res
            self.logger.info("Beta RI Solver failed to find a feasible solution.")
            res.x = [1., 1., 1., 1.]
        self.logger.info("Completed Beta RI Solver 1")
        self.logger.info(
            "Beta RI solver final variables - %s, objective - %s" % (",".join([str(x) for x in res.x]), res.fun))
        return res, df, None

    def getPoasRi(self, df):
        df_p = fetch("MBP", self.args.collection, logger=self.logger, Property="MCOutput", Identifier="ri",
                     PlusOrIndividual="Individual", Distribution="POAS", Solver=1)
        df["poas_ri"] = df_p.ri
        df.rename(columns={'expbp': 'expri'}, inplace=True)
        return df

    def startPoint(self, df_o):
        df = df_o.copy(deep=True)
        df["xviri"] = df.xvi * df.poas_ri
        smean = df.xviri.sum()
        df["variance"] = df.xvi * (smean - df.poas_ri) ** 2
        variance = df.variance.sum()
        df["skewness"] = df.xvi * ((smean - df.poas_ri) / np.sqrt(variance)) ** 3
        skewness = df.skewness.sum()

        eta = min(0.196, df.poas_ri.head(1).values[0] * 0.98)
        b_max = max(0.65, 1.25 * df.poas_ri.tail(1).values[0])
        d1 = (b_max - eta) / (smean - eta)
        alpha = np.abs(
            -1. / d1 ** 2 * (2. * d1 + (2. - d1) * 2. * (b_max - eta) / (np.abs(skewness) * np.sqrt(variance))))
        beta = alpha * (b_max - smean) / (smean - eta)

        # mass boundary, alpha, beta, eta
        self.eta, self.alpha, self.beta = eta, alpha, beta
        df_o["rb"] = (df_o.poas_ri + df_o.poas_ri.shift(-1)) / 2.
        df.loc[df.index == df.index.max(), "rb"] = max(0.65,
                                                       df.loc[df.index == df.index.max(), "poas_ri"].values[0] * 1.1)
        bounds = [[0.98, 1.2], [0.2, 3.], [0.2, 3], [0.5, (df_o.rb.min() * 0.98 * (1. - 0.01 / 100.)) / eta]]
        return [1., 1., 1., 1.], bounds

    def save(self, res, df, dfplus):
        df = self.calculateri(res.x, df)
        write_excel(self.args.debug, self.debugDir, ["ri-solver1_Beta"], ["Individual"], [df])
        write(df, "MBP", self.args.collection, logger=self.logger, Property="MCOutput", Identifier=self.what,
              PlusOrIndividual="Individual", Solver=1, Recommended=0, Distribution="Beta")

    def cost_function(self, vars, df_o):
        df = df_o.copy(deep=True)
        df = self.calculateri(vars, df)
        c4_c = (df.xvi * df.ri).sum()
        mc4 = df.zimi.sum() / df.mf.sum()
        dc4 = df.zimi.sum() / (df.zimi / df.density).sum()
        c4_e = (0.12399 * np.exp(
            3.4622 * 10 ** -4 * mc4 + 0.90389 * dc4 - 6.0955 * 10 ** -4 * mc4 * dc4) * mc4 ** 0.02264 * dc4 ** 0.22423) if mc4 <= 300 else (
                0.01102 * np.exp(
            -8.61126 * 10 ** -4 * mc4 + 3.228607 * dc4 + 9.07171 * 10 ** -4 * mc4 * dc4) * mc4 ** 0.02426 * dc4 ** -2.25051)
        error = df.exp_error.sum() / 0.1 + df.poas_error.sum() / 0.1 + (np.abs(c4_e - c4_c) / c4_e)
        return error

    def calculateri(self, vars, df):
        rb_multi, alpha_multi, beta_multi, eta_multi = vars
        alpha, beta, eta = self.alpha * alpha_multi, self.beta * beta_multi, self.eta * eta_multi
        # ri calculations
        df["rb"] = df.rb * rb_multi

        b_max = max(0.65, 1.25 * df.poas_ri.tail(1).values[0])
        df["y"] = (df.rb - eta) / (b_max - eta)
        df.loc[df.y > 1., "y"] = 0.9999999999
        df["p0"] = df.apply(lambda row: betaInbuilt.cdf(row.y, alpha, beta), axis=1)
        df.loc[df.p0 > 1., "p0"] = 0.9999999999
        df["p1"] = df.apply(lambda row: betaInbuilt.cdf(row.y, alpha + 1, beta), axis=1)
        df.loc[df.p1 > 1., "p1"] = 0.9999999999
        df['p0diff'] = df['p0'].sub(df['p0'].shift())
        df['p0diff'] = df['p0diff'].fillna(df['p0'].iloc[0])
        df['p1diff'] = df['p1'].sub(df['p1'].shift())
        df['p1diff'] = df['p1diff'].fillna(df['p1'].iloc[0])
        df.loc[df.p0diff == 0, "p0diff"] = 1.
        df["ri"] = eta + (alpha / (alpha + beta)) * (b_max - eta) * (df.p1diff.values) / (df.p0diff.values)
        df["ri_Beta"] = df.ri

        df["seq"] = range(0, df.shape[0])

        for scn, row in df.iterrows():
            if (row.seq - 1.) in df.seq.tolist():
                prow = df.loc[df.seq == row.seq - 1].squeeze()
                if row.ri < prow.ri:
                    df.loc[df.seq == row.seq, "ri"] = row.poas_ri
        for scn, row in df.iterrows():
            if (row.seq + 1.) in df.seq.tolist():
                nrow = df.loc[df.seq == row.seq + 1].squeeze()
                if row.ri > nrow.ri:
                    df.loc[df.seq == row.seq, "ri"] = row.poas_ri

        df["exp_error"] = ((df.expri - df.ri) / df.expri) ** 2
        df["poas_error"] = ((df.poas_ri - df.ri) / df.poas_ri) ** 2

        df.scn = df.index
        df.index = df.seq
        df.loc[df.index <= 5, "ri"] = df.loc[df.index <= 5, "expri"]
        df.index = df.scn
        return df
