import numpy as np
from scipy import optimize


class msmooth(object):
    def __init__(self, solver, gas):
        self.gas = gas
        self.solver = solver

    def smooth(self, df, dfplus, start_point):
        if dfplus.index.max() >= 15:
            df["slope"] = (np.log(df.mass) - np.log(df.mass.shift(1))) / (np.log(df.index) - np.log(df.index - 1))
            avg_slope = np.mean(
                df.loc[(df.index >= dfplus.index.max() - 4) & (df.index <= dfplus.index.max() + 4), "slope"].tolist())
            slope_m = ((np.log(df.loc[df.index == dfplus.index.max() + 4, "mass"].values[0]) - np.log(
                df.loc[df.index == dfplus.index.max() - 5, "mass"].values[0])) / (
                                   np.log(dfplus.index.max() + 4) - np.log(dfplus.index.max() - 5))) / avg_slope
            df = self.mass(slope_m, df, dfplus, start_point)
            df["mass"] = df.cmass
            dfplus["mass"] = dfplus.cmass
        obj = fixPlot(self.solver)
        df, dfplus = obj.fix(df, dfplus)
        return df, dfplus

    def mass(self, slope, df_o, dfplus, start_point):
        a = slope
        df = df_o.copy(deep=True)
        start, end = dfplus.index.max() - 5, dfplus.index.max() + 5
        df.loc[(df.index >= start) & (df.index <= end), "ln_scn"] = np.log(
            df.loc[(df.index >= start) & (df.index <= end)].index)
        df.loc[(df.index >= start) & (df.index <= end), "ln_mass"] = np.log(
            df.loc[(df.index >= start) & (df.index <= end), "mass"])
        df.loc[(df.index >= start + 1) & (df.index <= end), "slope"] = df.loc[
            (df.index >= start + 1) & (df.index <= end)].apply(lambda row: \
                                                                   (row.ln_mass -
                                                                    df.loc[df.index == row.name - 1, "ln_mass"].values[
                                                                        0]) / (row.ln_scn - df.loc[
                                                                       df.index == row.name - 1, "ln_scn"].values[0]),
                                                               axis=1)
        avg_slope = df.loc[(df.index >= start + 1) & (df.index <= end), "slope"].mean() * a
        new_mass = list()
        for index, row in df.loc[(df.index >= start + 1) & (df.index <= end)].iterrows():
            if len(new_mass) == 0:
                first_ln_mass = df.loc[df.index == start, "ln_mass"].values[0]
                ln_scn = df.loc[df.index == index, "ln_scn"].values[0] - df.loc[df.index == start, "ln_scn"].values[0]
                new_mass.append(first_ln_mass + avg_slope * (ln_scn))
            else:
                ln_scn = df.loc[df.index == index, "ln_scn"].values[0] - df.loc[df.index == index - 1, "ln_scn"].values[
                    0]
                new_mass.append(new_mass[-1] + avg_slope * (ln_scn))
        df.loc[(df.index >= start + 1) & (df.index <= end), "ln_mass_c"] = new_mass
        df["cmass"] = df.apply(
            lambda row: row.mass if row.name <= dfplus.index.max() - 1 or row.name >= end else np.exp(row.ln_mass_c),
            axis=1)
        df["zimi"] = df.cmass * df.mf
        ls = dfplus.loc[dfplus.index >= start_point].index.min()
        multiplier = dfplus.loc[dfplus.index == ls, "mf"].values[0] * dfplus.loc[dfplus.index == ls, "mass"].values[0] / \
                     df.loc[df.index >= ls, "zimi"].sum()
        df.loc[df.index > start_point, "cmass"] = df.loc[df.index > start_point, "cmass"] * multiplier
        df["zimi"] = df.mf * df.cmass
        dfplus["cmass"] = dfplus.apply(
            lambda row: df.loc[df.index >= row.name, "zimi"].sum() / df.loc[df.index >= row.name, "mf"].sum(), axis=1)
        return df


class fixPlot(object):
    def __init__(self, solver):
        self.solver = solver

    def fix(self, df, dfplus):
        self.end_scn = max(41, dfplus.index.max()) - 1 if self.solver == 1 else df.index.max() - 16
        self.start_scn = self.end_scn - 15
        res = optimize.minimize(self.cost_function, [9., 1., 1.], bounds=[[5., 60.], [0.69, 2.], [0.1, 0.2]],
                                method="SLSQP", args=(df, dfplus), options={'maxiter': 500})
        if res.success == False:
            print res
            print 'Warning:{"Type": "Mass Solver", "Message": "Stage 3, Mass Solver failed to find a feasible solution. Please check input data. If issue persists, please raise with POAS support team."}'
            return df, dfplus
        # print "Poonch fix -->", res
        df, dfplus = self.multiply_mass(res.x, df, dfplus)
        df["mass"] = df.cmass
        dfplus["mass"] = dfplus.cmass
        return df, dfplus

    def cost_function(self, vars, df_o, dfplus_o):
        df, dfplus = df_o.copy(deep=True), dfplus_o.copy(deep=True)
        df, dfplus = self.multiply_mass(vars, df, dfplus)
        df["error"] = np.abs(df.mass - df.cmass) / df.mass
        dfplus["error"] = np.abs(dfplus.mass - dfplus.cmass) / dfplus.mass
        error = self.targetCost(df.loc[(df.index >= self.start_scn) & (df.index <= self.end_scn), "error"].sum(), 0.01)
        error += self.targetCost(dfplus.error.sum(), 0.001)
        return error

    def targetCost(self, val, target):
        return val / target

    def multiply_mass(self, vars, df, dfplus):
        A, B, C = vars
        df["cmass"] = df.mass
        df.loc[df.index >= self.start_scn, "cmass"] = A * df.loc[df.index >= self.start_scn].index ** B
        df.loc[df.index == df.index.max(), "cmass"] = df.loc[df.index == df.index.max() - 1, "cmass"].values[0] + \
                                                      df.loc[df.index == df.index.max() - 1, "cmass"].values[0] * C
        # if df.loc[df.index == 6, "cmass"].values[0] >  (df.loc[df.index == 7, "cmass"].values[0]/81.7)*78.:
        #     df.loc[df.index == 6, "cmass"] = (df.loc[df.index == 7, "cmass"].values[0]/81.7)*78.
        df["zimi"] = df.cmass * df.mf
        dfplus["cmass"] = dfplus.apply(
            lambda row: df.loc[df.index >= row.name, "zimi"].sum() / df.loc[df.index >= row.name, "mf"].sum(), axis=1)
        return df, dfplus
