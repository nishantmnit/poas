import numpy as np
from scipy import optimize


class wismooth(object):
    def __init__(self):
        pass

    def smooth(self, df, dfplus, logger):
        df = self.addWi(df, dfplus)
        df = self.prepareForSolver(df)
        logger.info("Executing WI Smoothing Solver")
        res = optimize.minimize(self.cost_function, [1.], bounds=[[0.5, 2.]], method="SLSQP", args=(df, dfplus),
                                options={'maxiter': 500})
        if res.success == False:
            print res
            print 'Error:{"Type": "Mass Solver", "Message": "Zi Smoothing, Mass Solver failed to find a feasible solution. Please check input data. If issue persists, please raise with POAS support team."}'
            quit()
        logger.info(
            "WI smooth solver final variables - %s, objective - %s" % (",".join([str(x) for x in res.x]), res.fun))
        df, dfplus = self.calculateFinalMass(res, df, dfplus)
        return df, dfplus

    def cost_function(self, vars, df_o, dfplus):
        df, dfplus = self.calculateMass(vars, df_o, dfplus)
        dfplus["zi_error"] = np.abs((dfplus.mf - dfplus.zi) / dfplus.mf)
        dfplus["mass_error"] = np.abs((dfplus.mass - dfplus.expmass) / dfplus.expmass)
        error = self.targetCost(dfplus.loc[dfplus.index == dfplus.index.min(), "zi_error"].sum(), 0.001)
        error += self.targetCost(dfplus.loc[dfplus.index == dfplus.index.max(), "zi_error"].sum(), 0.001)
        error += self.targetCost(dfplus.loc[dfplus.index == dfplus.index.max(), "mass_error"].sum(), 0.01)
        return error

    def targetCost(self, val, target):
        return val / target

    def calculateMass(self, vars, df_o, dfplus):
        df = df_o.copy(deep=True)
        df.loc[df.index >= dfplus.index.max(), "zi"] = df.loc[df.index >= dfplus.index.max(), "zi"] * vars[0]
        df["zimi"] = df.zi * df.mass
        dfplus["zi"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "zi"].sum(), axis=1)
        dfplus["mass"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "zimi"].sum() / row.zi, axis=1)
        return df, dfplus

    def calculateFinalMass(self, res, df_o, dfplus):
        vars = res.x
        df, dfplus = self.calculateMass(vars, df_o, dfplus)
        df, dfplus = self.fixZiMass(df, dfplus)
        df["mf"] = df.zi
        dfplus["mf"] = dfplus.zi
        df.drop(columns=["zi"], inplace=True)
        dfplus.drop(columns=["zi"], inplace=True)
        return df, dfplus

    def fixZiMass(self, df, dfplus):
        if dfplus.loc[dfplus.index == dfplus.index.max(), "mass"].values[0] != \
                dfplus.loc[dfplus.index == dfplus.index.max(), "expmass"].values[0]:
            multiplier = dfplus.loc[dfplus.index == dfplus.index.max(), "expmass"].values[0] / \
                         dfplus.loc[dfplus.index == dfplus.index.max(), "mass"].values[0]
            df.loc[df.index >= dfplus.index.max(), "mass"] = df.loc[df.index >= dfplus.index.max(), "mass"] * multiplier
        if dfplus.loc[dfplus.index == dfplus.index.max(), "mf"].values[0] != \
                dfplus.loc[dfplus.index == dfplus.index.max(), "zi"].values[0]:
            multiplier = dfplus.loc[dfplus.index == dfplus.index.max(), "mf"].values[0] / \
                         dfplus.loc[dfplus.index == dfplus.index.max(), "zi"].values[0]
            df.loc[df.index >= dfplus.index.max(), "zi"] = df.loc[df.index >= dfplus.index.max(), "zi"] * multiplier
        df["zimi"] = df.zi * df.mass
        dfplus["zi"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "zi"].sum(), axis=1)
        dfplus["mass"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "zimi"].sum() / row.zi, axis=1)
        return df, dfplus

    def prepareForSolver(self, df):
        zimisum = df.zimi.sum()
        df["zimi"] = df.new_wiinside * zimisum
        df["zi"] = df.zimi / df.mass
        return df

    def addWi(self, df, dfplus):
        df["zimi"] = df.mf * df.mass
        df["wiinside"] = df.zimi / df.zimi.sum()
        new_wi = list()
        bracket = None
        for scn in range(5, 122):
            if scn < dfplus.index.max() or scn <= 6 or scn >= 15:
                new_wi.append(df.loc[df.index == scn, "wiinside"].values[0])
            else:
                bracket = self.findBracket(scn, bracket)
                slope = self.getSlope(df, scn, bracket)
                new_wi.append(new_wi[-1] + slope)
        df["new_wiinside"] = new_wi
        return df

    def findBracket(self, scn, bracket=None):
        if bracket is not None and scn <= bracket[1]: return bracket
        brackets = [[6, 8], [8, 10], [10, 12], [12, 14]]
        for bracket in brackets:
            if scn >= bracket[0] and scn <= bracket[1]:
                return bracket

    def getSlope(self, df, scn, bracket):
        max_wi = df.wiinside.max()
        max_wi_scn = df.loc[df.wiinside == max_wi].index.min()
        if max_wi_scn == (bracket[0] + bracket[1]) / 2.:
            if scn == bracket[1]:
                return (df.loc[df.index == bracket[1], "wiinside"].values[0] - max_wi) / (bracket[1] - max_wi_scn)
            else:
                return (max_wi - df.loc[df.index == bracket[0], "wiinside"].values[0]) / (max_wi_scn - bracket[0])
        else:
            return (df.loc[df.index == bracket[1], "wiinside"].values[0] -
                    df.loc[df.index == bracket[0], "wiinside"].values[0]) / (bracket[1] - bracket[0])
