import numpy as np
from Scripts.pvt.core.optimizers.ncg import *
from Scripts.pvt.core.storage.excel import write_excel
from Scripts.pvt.core.storage.fetch_most import set_recommended
from Scripts.pvt.core.storage.mongo import write, delete_all
from scipy import optimize


class POASDensity(object):
    def __init__(self, logger, debug, debugDir, well, what):
        self.logger = logger
        self.debug = debug
        self.debugDir = debugDir
        self.well = well
        self.what = what
        delete_all("MBP", self.well, self.logger, Identifier=self.what, Distribution="POAS")
        delete_all("MBP", self.well, self.logger, Identifier=self.what, Distribution="POASc")

    def run(self, df, dfplus):
        self.logger.info("Executing POAS Density Finder.")
        self.lid = df.loc[~df.expdensity.isnull()].index.max()
        self.lid = 0 if np.isnan(self.lid) else self.lid
        if dfplus.index.max() <= 16:
            res = optimize.minimize(self.cost_function, [0., 1.], bounds=[[-3., 6.], [.0005, 6.]], method="SLSQP",
                                    args=(df, dfplus), constraints=self.constraints(df, dfplus),
                                    options={'maxiter': 500})
        else:
            res = optimize.minimize(self.cost_function, [0., 1.], bounds=[[-3., 6.], [.0005, 6.]], method="SLSQP",
                                    args=(df, dfplus), options={'maxiter': 500})
        if res.success == False:
            print res
            print 'Error:{"Type": "Density Solver", "Message": "POAS Density Solver failed to find a feasible solution. Please check input data. If issue persists, please raise with POAS support team."}'
            quit()

        # save main solver results
        df, dfplus = self.save(res, df, dfplus)
        self.logger.info("POAS Density solver final variables - %s" % (",".join([str(x) for x in res.x])))
        self.logger.info("Completed POAS Density Solver.")

        if res.x[1] <= 0.5:
            self.logger.info(
                "POAS Density solver results not reliable. Executing Contingency Solver. Var2 = %s" % (res.x[1]))
            df, dfplus = self.calculateDensity(res.x, df, dfplus)
            obj = POASDensityContingency(self.logger, self.debug, self.debugDir, self.well, self.what)
            ret = obj.run(df, dfplus)

    def save(self, res, df, dfplus):
        df, dfplus = self.calculateDensity(res.x, df, dfplus)
        write_excel(self.debug, self.debugDir, ["POAS_density-solver1"], ["Individual", "Plus"],
                    [df, dfplus])
        write(df, "MBP", self.well, logger=self.logger, Property="MCOutput", Identifier=self.what,
              PlusOrIndividual="Individual", Solver=1, Recommended=0, Distribution="POAS")
        write(dfplus, "MBP", self.well, logger=self.logger, Property="MCOutput", Identifier=self.what,
              PlusOrIndividual="Plus", Solver=1, Recommended=0, Distribution="POAS")
        set_recommended(self.well, self.logger, self.what)
        return df, dfplus

    def cost_function(self, vars, df_o, dfplus):
        df, dfplus = self.calculateDensity(vars, df_o, dfplus)
        df["error"] = np.abs(df.expdensity - df.POASDensity) / df.expdensity
        dfplus["error"] = ((dfplus.expdensity - dfplus.density) / dfplus.expdensity) ** 2
        error = self.targetCost(df.loc[df.index >= 7, "error"].sum(), 0.01)
        for index, row in dfplus.iterrows():
            error += self.targetCost(row.error, 0.001)
        if not np.isnan(self.lid) and self.lid != 0:
            error += self.targetCost(df.loc[df.index > self.lid - 3, "error"].sum(), 0.01)
        return error

    def targetCost(self, val, target):
        return val / target

    def calculateDensity(self, vars, df_o, dfplus):
        df = df_o.copy(deep=True)
        df = self.POASDensity(df, vars[0], vars[1])
        df, dfplus = self.finalDensity(df, dfplus)
        return df, dfplus

    def finalDensity(self, df, dfplus):
        df["density"] = df.apply(lambda row: row.POASDensity if np.isnan(row.expdensity) else row.expdensity, axis=1)
        points = [max(19, self.lid), max(19, self.lid) + 11, max(19, self.lid) + 16]
        points = [float(x) for x in points]
        densities = df.loc[df.index.isin(points), "density"].values.tolist()
        params = [1. / (points[2] - points[0]) * (
                (densities[2] - densities[1]) / (points[2] - points[1]) - (densities[1] - densities[0]) / (
                points[1] - points[0]))]
        params = params + [
            (densities[1] - densities[0]) / (points[1] - points[0]) - params[0] * (points[1] + points[0])]
        params = params + [(densities[0] - params[1] * points[0] - params[0] * points[0] ** 2)]
        df["density"] = df.apply(
            lambda row: params[0] * row.name ** 2 + params[1] * row.name + params[2] if row.name > points[
                0] and row.name < points[2] else row.density, axis=1)
        df["zimibysgi"] = df.zimi / df.density
        dfplus["density"] = dfplus.apply(
            lambda row: df.loc[df.index >= row.name, "zimi"].sum() / df.loc[df.index >= row.name, "zimibysgi"].sum(),
            axis=1)
        return df, dfplus

    def aliphaticDensity(self, scn):
        # from scn 6 to 121 
        all_densities = [0.6497773095056900, 0.6765171765303820, 0.6970152608650540, 0.7132531517677730,
                         0.7264494970774210, 0.7373960330624230, 0.7466300599078980, 0.7545293850206410, \
                         0.7613676163109280, 0.7673478940701880, 0.7726242864728250, 0.7773158179807860,
                         0.7815159342163050, 0.7852990490227780, 0.7887251735941340, 0.7918432537654240,
                         0.7946936180955970, \
                         0.7973098018897410, 0.7997199255265320, 0.8019477494038700, 0.8040134908503150,
                         0.8059344635078030, 0.8077255827063110, 0.8093997685539040, 0.8109682701518690,
                         0.8124409284067970, \
                         0.8138263906178320, 0.8151322868765220, 0.8163653759946470, 0.8175316669413190,
                         0.8186365204635100, 0.8196847345702580, 0.8206806167987170, 0.8216280455913880,
                         0.8225305226555910, \
                         0.8233912178170610, 0.8242130075961990, 0.8249985085106260, 0.8257501059280980,
                         0.8264699791497080, 0.8271601232868710, 0.8278223684012470, 0.8284583962997460,
                         0.8290697553137550, \
                         0.8296578733399000, 0.8302240693768070, 0.8307695637568260, 0.8312954872421090,
                         0.8318028891297480, 0.8322927444899020, 0.8327659606434620, 0.8332233829710510,
                         0.8336658001326770, \
                         0.8340939487667980, 0.8345085177284950, 0.8349101519187600, 0.8352994557502730,
                         0.8356769962894100, 0.8360433061092730, 0.8363988858843640, 0.8367442067538340,
                         0.8370797124771030, \
                         0.8374058214028500, 0.8377229282700120, 0.8380314058573000, 0.8383316064959330,
                         0.8386238634586640, 0.8389084922367630, 0.8391857917153940, 0.8394560452567010,
                         0.8397195216989740, \
                         0.8399764762793980, 0.8402271514871360, 0.8404717778528010, 0.8407105746798270,
                         0.8409437507226370, 0.8411715048161190, 0.8413940264604140, 0.8416114963647060,
                         0.8418240869533260, \
                         0.8420319628371830, 0.8422352812532810, 0.8424341924748080, 0.8426288401940800,
                         0.8428193618804240, 0.8430058891148750, 0.8431885479034590, 0.8433674589706100,
                         0.8435427380342070, \
                         0.8437144960635450, 0.8438828395214760, 0.8440478705918410, 0.8442096873932230,
                         0.8443683841799760, 0.8445240515314150, 0.8446767765299570, 0.8448266429289770,
                         0.8449737313110580, \
                         0.8451181192372660, 0.8452598813880610, 0.8453990896963560, 0.8455358134732600,
                         0.8456701195269530, 0.8458020722751300, 0.8459317338514250, 0.8460591642061800,
                         0.8461844212019150, \
                         0.8463075607038090, 0.8464286366655130, 0.8465477012105560, 0.8466648047096160,
                         0.8467799958539030, 0.8468933217248660, 0.8470048278604530, 0.8471145583181170,
                         0.8472225557347470]
        return all_densities[scn - 6]

    def baseDensity(self, scn):
        if scn <= max(25, self.lid + 5):
            if scn <= 33:
                return (
                        -7.27411968529E-09 * scn ** 6 + 9.8804076628049E-07 * scn ** 5 - 0.0000544578952353303 * scn ** 4 + 0.00156130415898253 * scn ** 3 - 0.0247730420179834 * scn ** 2 + 0.21511514278639 * scn - 0.0080429446478434)
            else:
                return (0.686069942338086 * scn ** 0.0814141102460385)
        else:
            return 0

    def perChange(self, scn):
        return 5.29314130144534E-06 * scn ** 3 - 0.00111243172535057 * scn ** 2 + 0.0772646126408345 * scn + 2.64506454446415

    def POASDensity(self, df, c_multiplier, s_multiplier):
        density = list()
        last_scn = min(122, df.index.max() + 1)
        for scn in range(6, last_scn):
            if scn <= max(20, self.lid):
                density.append(self.density1(scn, c_multiplier))
            elif scn <= max(self.lid + 4, 24):
                avg_density = self.avgDensity(c_multiplier)
                density.append(np.exp(avg_density * (np.log(scn) - np.log(scn - 1)) + np.log(density[-1])))
            else:
                slope = (np.log(density[max(self.lid, 20) + 4 - 6]) - np.log(
                    density[max(self.lid, 20) + 4 - 1 - 6])) * s_multiplier
                density.append(np.exp(slope + np.log(density[-1])))
            if density[-1] < self.aliphaticDensity(scn):
                density[-1] = self.aliphaticDensity(scn)
        df.loc[df.index >= 6, "POASDensity"] = density
        mr = df.loc[df.index == df.index.max(), "mass"].values[0] / \
             df.loc[df.index == df.index.max() - 1, "mass"].values[0]
        dr = 1.02 if mr <= 1.02 else (1.05 if mr > 1.05 else mr)
        df.loc[df.index == df.index.max(), "POASDensity"] = \
            df.loc[df.index == df.index.max() - 1, "POASDensity"].values[0] * dr
        return df

    def density1(self, scn, c_multiplier):
        return self.baseDensity(scn) + self.perChange(scn) * c_multiplier / 100.

    def avgDensity(self, c_multiplier):
        d, l, s = list(), list(), list()
        for scn in range(max(self.lid - 1, 19), max(25, self.lid + 5)):
            d.append(self.density1(scn, c_multiplier))
            l.append(np.log(d[-1]))
            if len(d) > 1:
                s.append((l[-1] - l[-2]) / (np.log(scn) - np.log(scn - 1)))
        return np.mean(s)

    def constraints(self, df, dfplus):
        constraints = list()
        constraints.append({'type': 'ineq', 'fun': self.min_density_constraint, 'args': [df, dfplus]})
        constraints.append({'type': 'ineq', 'fun': self.max_density_constraint, 'args': [df, dfplus]})
        return constraints

    def min_density_constraint(self, vars, df_o, dfplus):
        df = df_o.copy(deep=True)
        df = self.POASDensity(df, vars[0], vars[1])
        df, dfplus = self.finalDensity(df, dfplus)
        min_density = min((0.1172 * np.log(dfplus.index) + 0.3754).tolist())
        return df.loc[df.index == df.index.max(), "density"].values[0] - min_density

    def max_density_constraint(self, vars, df_o, dfplus):
        df = df_o.copy(deep=True)
        df = self.POASDensity(df, vars[0], vars[1])
        df, dfplus = self.finalDensity(df, dfplus)
        max_density = max((dfplus.apply(lambda row: 1.3 * ((
                                                                   -9.259953526186E-08 * row.name ** 4 - 5.35267342716139E-06 * row.name ** 3 + 0.000735231107221068 * row.name ** 2 - 0.024352285572931 * row.name + 1.55207500562452) \
                                                               if row.name <= 40 else (
                -0.111023831942787 * np.log(row.name) + 1.63490295684644)) * row.expdensity, axis=1)).tolist())
        return max_density - df.loc[df.index == df.index.max(), "density"].values[0]


class POASDensityContingency(object):
    def __init__(self, logger, debug, debugDir, well, what):
        self.logger = logger
        self.debug = debug
        self.debugDir = debugDir
        self.well = well
        self.what = what

    def run(self, df, dfplus):
        self.logger.info("Executing POAS Density Contingency Finder.")
        dfplus["target"] = 0.001
        df["target"] = 0.001
        if dfplus.shape[0] > 2:
            dfplus.loc[(dfplus.index == dfplus.index.min()) | (dfplus.index == dfplus.index.max()), "target"] = 0.0001
        self.ncg = ncg()
        vars = self.ncg.minimize(self._sp(), self._range(), self._cost_for_ncg, self.logger, max_iter=200,
                                 debug=self.debug, converge_f=self._converged, args=(df, dfplus))
        self.logger.info("POAS Density Contingency solver final variables - %s" % (",".join([str(x) for x in vars])))
        df, dfplus = self.save(vars, df, dfplus)
        return 0

    def _sp(self):
        return [1.1, 1., 3., 0.089]

    def _range(self):
        return [[1., 2.], [1., 4.], [1., 4.], [0.084, 1.]]

    def _cost_for_ncg(self, vars, args):
        df_o, dfplus = args[0], args[1]
        df = df_o.copy(deep=True)
        cost, df, dfplus = self._cost(vars, df, dfplus)
        grad = self._gradient(vars, df, dfplus)
        return cost, grad

    def _cost(self, vars, df_o, dfplus):
        df, dfplus = self.calculateDensity(vars, df_o, dfplus)
        self.scales = [1., 5.]
        return (df.error / df.target).sum() * self.scales[1] + (dfplus.error / dfplus.target).sum() * self.scales[
            0], df, dfplus

    def _gradient(self, vars, df, dfplus):
        df["density_wrt1"] = df.apply(lambda row: 0 if row.name <= self.lid else 1, axis=1)
        df["density_wrt2"] = df.apply(
            lambda row: 0 if row.name <= self.lid else (-1. * np.exp(vars[1] - vars[2] * row.name ** vars[3])), axis=1)
        df["density_wrt3"] = df.apply(lambda row: 0 if row.name <= self.lid else (
                row.name ** vars[3] * np.exp(vars[1] - vars[2] * row.name ** vars[3])), axis=1)
        df["density_wrt4"] = df.apply(lambda row: 0 if row.name <= self.lid else (
                vars[2] * np.log(row.name) * row.name ** vars[3] * np.exp(vars[1] - vars[2] * row.name ** vars[3])),
                                      axis=1)

        df["zimibysgi_wrt1"] = -1. * df.zimi / df.density_before_smoothing ** 2 * df.density_wrt1
        df["zimibysgi_wrt2"] = -1. * df.zimi / df.density_before_smoothing ** 2 * df.density_wrt2
        df["zimibysgi_wrt3"] = -1. * df.zimi / df.density_before_smoothing ** 2 * df.density_wrt3
        df["zimibysgi_wrt4"] = -1. * df.zimi / df.density_before_smoothing ** 2 * df.density_wrt4

        df["obj_wrt1"] = df.apply(
            lambda row: 0 if row.POASDensity == 0 or row.name > self.lid or row.name <= self.lid - 3 else (
                    (2. * (row.POASDensity - row.density_calc) / row.POASDensity ** 2) * -1.), axis=1) * \
                         self.scales[1] / df.target
        df["obj_wrt2"] = df.apply(
            lambda row: 0 if row.POASDensity == 0 or row.name > self.lid or row.name <= self.lid - 3 else (
                    (2. * (row.POASDensity - row.density_calc) / row.POASDensity ** 2) * -1. * (
                    -1 * np.exp(vars[1] - vars[2] * row.name ** vars[3]))), axis=1) * self.scales[1] / df.target
        df["obj_wrt3"] = df.apply(
            lambda row: 0 if row.POASDensity == 0 or row.name > self.lid or row.name <= self.lid - 3 else (
                    (2. * (row.POASDensity - row.density_calc) / row.POASDensity ** 2) * -1. * (
                    row.name ** vars[3] * np.exp(vars[1] - vars[2] * row.name ** vars[3]))), axis=1) * \
                         self.scales[1] / df.target
        df["obj_wrt4"] = df.apply(
            lambda row: 0 if row.POASDensity == 0 or row.name > self.lid or row.name <= self.lid - 3 else (
                    (2. * (row.POASDensity - row.density_calc) / row.POASDensity ** 2) * -1. * (
                    vars[2] * np.log(row.name) * row.name ** vars[3] * np.exp(
                vars[1] - vars[2] * row.name ** vars[3]))), axis=1) * self.scales[1] / df.target

        dfplus["density_wrt1"] = dfplus.apply(lambda row: (-1. * df.loc[df.index >= row.name, "zimi"].sum() / df.loc[
            df.index >= row.name, "zimibysgi"].sum() ** 2) * df.loc[df.index >= row.name, "zimibysgi_wrt1"].sum(),
                                              axis=1)
        dfplus["density_wrt2"] = dfplus.apply(lambda row: (-1. * df.loc[df.index >= row.name, "zimi"].sum() / df.loc[
            df.index >= row.name, "zimibysgi"].sum() ** 2) * df.loc[df.index >= row.name, "zimibysgi_wrt2"].sum(),
                                              axis=1)
        dfplus["density_wrt3"] = dfplus.apply(lambda row: (-1. * df.loc[df.index >= row.name, "zimi"].sum() / df.loc[
            df.index >= row.name, "zimibysgi"].sum() ** 2) * df.loc[df.index >= row.name, "zimibysgi_wrt3"].sum(),
                                              axis=1)
        dfplus["density_wrt4"] = dfplus.apply(lambda row: (-1. * df.loc[df.index >= row.name, "zimi"].sum() / df.loc[
            df.index >= row.name, "zimibysgi"].sum() ** 2) * df.loc[df.index >= row.name, "zimibysgi_wrt4"].sum(),
                                              axis=1)

        dfplus["obj_wrt1"] = (self.scales[0] * (2. * (dfplus.expdensity - dfplus.density) / dfplus.expdensity ** 2) * (
            -1.) * dfplus.density_wrt1) / dfplus.target
        dfplus["obj_wrt2"] = (self.scales[0] * (2. * (dfplus.expdensity - dfplus.density) / dfplus.expdensity ** 2) * (
            -1.) * dfplus.density_wrt2) / dfplus.target
        dfplus["obj_wrt3"] = (self.scales[0] * (2. * (dfplus.expdensity - dfplus.density) / dfplus.expdensity ** 2) * (
            -1.) * dfplus.density_wrt3) / dfplus.target
        dfplus["obj_wrt4"] = (self.scales[0] * (2. * (dfplus.expdensity - dfplus.density) / dfplus.expdensity ** 2) * (
            -1.) * dfplus.density_wrt4) / dfplus.target

        gradients = np.array([df.obj_wrt1.sum() + dfplus.obj_wrt1.sum(), df.obj_wrt2.sum() + dfplus.obj_wrt2.sum(),
                              df.obj_wrt3.sum() + dfplus.obj_wrt3.sum(), df.obj_wrt4.sum() + dfplus.obj_wrt4.sum()])
        return gradients

    def _converged(self, debug, logger, iteration, grad):
        cd = self.ncg.cost_diff
        if iteration < 3:
            return False
        if cd <= 10 ** -4 and iteration <= 50:
            if debug: logger.info("Converged by rule 1 - %s" % (cd))
            return True
        elif cd <= 10 ** -3 and iteration > 50 and iteration <= 75:
            if debug: logger.info("Converged by rule 1 - %s" % (cd))
            return True
        elif cd <= 10 ** -2 and iteration > 75 and iteration <= 100:
            if debug: logger.info("Converged by rule 1 - %s" % (cd))
            return True
        elif cd <= 10 ** -1 and iteration > 100:
            if debug: logger.info("Converged by rule 1 - %s" % (cd))
            return True
        if iteration > 5 and np.all(grad - self.ncg.pgrad == 0):
            if debug: logger.info("Converged by rule 1 - %s" % (cd))
            return True
        return False

    def save(self, vars, df, dfplus):
        df, dfplus = self.calculateDensity(vars, df, dfplus)
        dfplus["diff"] = (np.abs(dfplus.expdensity - dfplus.density) / dfplus.expdensity) * 100.
        if dfplus.loc[dfplus.index == dfplus.index.max(), "diff"].values[0] > 0.5:
            write_excel(self.debug, self.debugDir, ["POASc_density-solver1_before_smoothing"],
                        ["Individual", "Plus"], [df, dfplus])
            self.logger.info("Smoothing POASc density smoothing before saving.")
            df, dfplus = self.smooth(df, dfplus)
        write_excel(self.debug, self.debugDir, ["POASc_density-solver1"], ["Individual", "Plus"],
                    [df, dfplus])
        write(df, "MBP", self.well, logger=self.logger, Property="MCOutput", Identifier=self.what,
              PlusOrIndividual="Individual", Solver=1, Recommended=0, Distribution="POASc")
        write(dfplus, "MBP", self.well, logger=self.logger, Property="MCOutput", Identifier=self.what,
              PlusOrIndividual="Plus", Solver=1, Recommended=0, Distribution="POASc")
        set_recommended(self.well, self.logger, self.what)
        return df, dfplus

    def smooth(self, df_o, dfplus):
        df = df_o.copy(deep=True)
        ls = df.index.max()
        if ls >= 80:
            fs = 40
        elif ls >= 50:
            fs = 30
        else:
            fs = 20
        ls = fs + 1
        slope = np.log(df.loc[df.index == ls, "density"].values[0]) - np.log(
            df.loc[df.index == fs, "density"].values[0])
        alphas, calphas, dalphas = list(), list(), list()
        d50 = np.log(df.loc[df.index == ls, "density"].values[0])
        for s_multiplier in [0.0001, 0.0005, 0.0025, 0.0125, 0.0625, 0.39375, 0.725, 1.05625, 1.3875, 1.71875, 2.05,
                             2.38125, 2.7125, 3.04375, 3.375, 3.70625, 4.0375, 4.36875, 4.7, 5.03125, 5.3625, 5.69375,
                             6.025, 6.35625, 6.6875, 7.01875, 7.35, 7.68125, 8.0125, 8.34375, 8.675, 9.00625, 9.3375,
                             9.66875, 10.]:
            df["density"] = df.apply(
                lambda row: row.density if row.name <= ls else np.exp(d50 + (row.name - ls) * (slope * s_multiplier)),
                axis=1)
            df["zimibysgi"] = df.zimi / df.density
            dfplus["density"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "zimi"].sum() / df.loc[
                df.index >= row.name, "zimibysgi"].sum(), axis=1)
            dfplus["error"] = (np.abs(dfplus.expdensity - dfplus.density) / dfplus.expdensity)
            alphas.append(s_multiplier)
            calphas.append(dfplus.loc[dfplus.index == dfplus.index.max(), "error"].values[0])
            dalphas.append(dfplus.loc[dfplus.index == dfplus.index.max(), "density"].values[0])

        m_index = calphas.index(min(calphas))
        m_alpha = alphas[m_index]
        m_cost = calphas[m_index]

        index1 = max(0, m_index - 1)
        index2 = max(0, m_index - 2)
        index3 = min(len(alphas) - 1, m_index + 1)

        density1, density2, density3, density4 = dalphas[m_index], dalphas[index2], dalphas[index1], dalphas[index3]
        cost1, cost2, cost3, cost4 = alphas[m_index], alphas[index2], alphas[index1], alphas[index3]
        k1 = (density2 ** 3 - density1 ** 3) * (density3 - density1) - (density3 ** 3 - density1 ** 3) * (
                density2 - density1)
        k2 = (density2 ** 2 - density1 ** 2) * (density3 - density1) - (density3 ** 2 - density1 ** 2) * (
                density2 - density1)
        k3 = (density2 ** 3 - density1 ** 3) * (density4 - density1) - (density4 ** 3 - density1 ** 3) * (
                density2 - density1)
        k4 = (density2 ** 2 - density1 ** 2) * (density4 - density1) - (density4 ** 2 - density1 ** 2) * (
                density2 - density1)
        p1 = (cost2 - cost1) * (density3 - density1) - (cost3 - cost1) * (density2 - density1)
        p2 = (cost2 - cost1) * (density4 - density1) - (cost4 - cost1) * (density2 - density1)

        a = 0 if (k1 * k4 - k2 * k3) == 0 else (p1 * k4 - p2 * k2) / (k1 * k4 - k2 * k3)
        b = 0 if k2 == 0 else (p1 - a * k1) / k2
        c = 0 if (density2 - density1) == 0 else ((cost2 - cost1) - a * (density2 ** 3 - density1 ** 3) - b * (
                density2 ** 2 - density1 ** 2)) / (density2 - density1)
        d = cost1 - (a * density1 ** 3 + b * density1 ** 2 + c * density1)

        if a > 10 ** 50:
            a = 10 ** 50

        if b > 10 ** 50:
            b = 10 ** 50

        if c > 10 ** 50:
            c = 10 ** 50

        if d > 10 ** 50:
            d = 10 ** 50

        last_density = dfplus.loc[dfplus.index == dfplus.index.max(), "expdensity"].values[0]

        s_multiplier = a * last_density ** 3 + b * last_density ** 2 + c * last_density + d

        df["density"] = df.apply(
            lambda row: row.density if row.name <= ls else np.exp(d50 + (row.name - ls) * (slope * s_multiplier)),
            axis=1)
        df["zimibysgi"] = df.zimi / df.density
        dfplus["density"] = dfplus.apply(
            lambda row: df.loc[df.index >= row.name, "zimi"].sum() / df.loc[df.index >= row.name, "zimibysgi"].sum(),
            axis=1)

        return df, dfplus

    def calculateDensity(self, vars, df_o, dfplus):
        theta, a, b, c = vars
        df = df_o.copy(deep=True)
        df["density"] = theta - np.exp(a - b * df.index ** c)
        df["density_calc"] = df.density
        lid = df.loc[~df.expdensity.isnull()].index.max()
        lid = 19 if lid > 15 else 15
        df["error"] = df.apply(lambda row: 0 if row.POASDensity == 0 or row.name > lid or row.name <= lid - 3 else ((
                                                                                                                            row.POASDensity - row.density) / row.POASDensity) ** 2,
                               axis=1)
        df["diff"] = df.apply(lambda row: 0 if row.name > lid or row.name <= lid - 2 else row.POASDensity - row.density,
                              axis=1)
        df.loc[df.index <= lid, "density"] = df.POASDensity
        df.loc[~df.expdensity.isnull(), "density"] = df.expdensity
        df["density_before_smoothing"] = df.density
        avg_diff = np.mean(df.loc[(df.index >= lid - 1) & (df.index <= lid), "diff"].tolist())
        df.loc[df.index > lid, "density"] = df.loc[df.index > lid].apply(
            lambda row: 0 if row.density == 0 else row.density + avg_diff, axis=1)
        df["zimibysgi"] = df.zimi / df.density
        dfplus["density"] = dfplus.apply(
            lambda row: df.loc[df.index >= row.name, "zimi"].sum() / df.loc[df.index >= row.name, "zimibysgi"].sum(),
            axis=1)
        dfplus["error"] = ((dfplus.expdensity - dfplus.density) / dfplus.expdensity) ** 2
        self.lid = lid
        return df, dfplus
