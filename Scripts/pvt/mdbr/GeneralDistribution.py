from collections import OrderedDict
from math import ceil

from Scripts.pvt.core.optimizers.GenericSolver import *
from Scripts.pvt.core.storage.excel import write_excel
from Scripts.pvt.core.storage.fetch_most import validated_data, set_recommended, poas_recommended_density
from Scripts.pvt.core.storage.mongo import write, delete_all
from scipy.interpolate import PchipInterpolator
from scipy.special import gammainc, gamma, digamma
from scipy.stats import beta as betaInbuilt

from DistributionAnalysis import *
from Initialize import *
from POASDensity import *
from Validate import *
from msmooth import *
from wismooth import *


class GeneralDistribution(DistributionAnalysis, object):
    def __init__(self, parent, what):
        self.__dict__ = parent.__dict__.copy()
        self.what = what

    def optimize(self):
        delete_all("MBP", self.args.collection, self.logger, Identifier=self.what, Distribution="General")
        try:
            df, dfplus = validated_data(self.args.collection, self.logger)
        except Exception as e:
            print 'Error:{"Type": "Validation", "Message": "Data validation failed. Please check and fix input data first."}'
            quit()
        objects = self.getOptimizeClass(dfplus)
        for obj in objects:
            res, df, dfplus = obj.optimize()
            if type(res) != str:
                obj.save(res, df, dfplus)
                set_recommended(self.args.collection, self.logger, self.what)

    def getOptimizeClass(self, dfplus):
        if self.what == "mass":
            return self.massOptimizationClass(dfplus)
        elif self.what == "density":
            return [DensitySolver1(self)]
        elif self.what == "bp":
            return [BpSolver1(self)]
        elif self.what == "ri":
            return [RiSolver1(self)]

    def massOptimizationClass(self, dfplus):
        if dfplus.index.max() <= 9:
            return [self.selectBias(self.args.bias)] + [MassSolver7(self), MassSolver8(self)]
        elif dfplus.index.max() <= 19:
            return [MassSolver5(self), MassSolver7(self), MassSolver8(self)]
        else:
            return [MassSolver5(self), MassSolver2(self), MassSolver3(self), MassSolver4(self), MassSolver7(self),
                    MassSolver8(self)]

    def selectBias(self, bias):
        if bias == "general":
            return MassSolver1_general(self)
        elif bias == "gamma":
            return MassSolver1_gamma(self)
        else:
            return MassSolver1_beta(self)


class Common(DistributionAnalysis, Validate, object):
    def __init__(self, parent):
        self.__dict__ = parent.__dict__.copy()

    def calcMass(self, df_o, a, b, eta, plus=None):
        df = df_o.copy(deep=True)
        df.loc[df.cummziinside > 0.999999999999, "cummziinside"] = 0.999999999999
        df["qi"] = df.apply(
            lambda row: log(1 / (1 - (row.cummziinside if row.cummziinside < 0.99999999 else 0.99999999))), axis=1)
        df["incg"] = df.apply(lambda row: gammainc(1 + 1 / b, row.qi) * gamma(1 + 1 / b), axis=1)
        if plus is not None:
            p = (10000 - eta) / eta
            q = 0.99999999
            qi = log(1 / (1 - q))
            incg = gammainc(1 + 1 / b, qi) * gamma(1 + 1 / b)
            df["pav"] = (1 / (1 - df.cummziinside.values)) * (a / b) ** (1 / b) * (incg - df.incg.values)
        else:
            df['cummziinsidediff'] = df['cummziinside'].sub(df['cummziinside'].shift())
            df.cummziinsidediff = df.cummziinsidediff.fillna(df['cummziinside'].iloc[0])
            df['incgdiff'] = df['incg'].sub(df['incg'].shift())
            df.incgdiff = df.incgdiff.fillna(df['incg'].iloc[0])
            df["pav"] = (1 / df.cummziinsidediff.values) * (a / b) ** (1 / b) * df.incgdiff.values
        df["mass"] = eta * (1 + df.pav.values)
        return df

    def calcBp(self, X, Y):
        a, b, eta = X
        df = Y.copy(deep=True)
        df["qi"] = df.apply(lambda row: log(1 / (1 - (row.cummxvi if row.cummxvi < 0.99999999 else 0.99999999))),
                            axis=1)
        df["incg"] = df.apply(lambda row: gammainc(1 + 1 / b, row.qi) * gamma(1 + 1 / b), axis=1)
        df['incgdiff'] = df['incg'].sub(df['incg'].shift())
        df.incgdiff = df.incgdiff.fillna(df['incg'].iloc[0])
        df["pav"] = (1 / df.xvi.values) * (a / b) ** (1 / b) * df.incgdiff.values
        df["bp"] = eta * (1 + df.pav.values)
        df["error"] = ((df.expbp - df.bp) / df.expbp) ** 2
        return df

    def SumTerm(self, b, qi):
        index = 1 + 1 / b + 1
        st = 0
        while True:
            st_ind = (digamma(index)) * ((qi ** (index - 1)) / (gamma(index)))
            if math.isnan(st_ind): st_ind = 0
            if abs(st_ind) < 10 ** -12: break
            st = st + st_ind
            index += 1
        st = exp(-qi) * gamma(1 + 1 / b) * st
        return st

    def Differentiate(self, b, qi, incg):
        st = self.SumTerm(b, qi)
        if qi == 0: qi = 10 ** -30
        return (-1 / b ** 2) * (incg * (log(qi) + digamma(1 + 1 / b)) - st)

    def SumTermSpecial(self, b, qi, G9):
        index = 1 + 1 / b + 1
        st = 0
        i = 0
        while True:
            st_ind = (digamma(index)) * qi ** i / (gamma(index)) - (i * qi ** (i - 1) * G9 / gamma(index))
            if math.isnan(st_ind): st_ind = 0
            if abs(st_ind) < 10 ** -12: break
            st = st + st_ind
            index += 1
            i += 1
        st = exp(-qi) * qi ** (1 + 1 / b) * gamma(1 + 1 / b) * st
        return st

    def DifferentiateSpecial(self, b, qi, incg, G9):
        st = self.SumTermSpecial(b, qi, G9)
        st = incg * (-1 * G9 + log(qi) + ((1 + 1 / b) / qi) * G9 + digamma(1 + 1 / b)) - st
        return (-1 / b ** 2) * st

    def getuserInputData(self):
        return self.individuals(), self.plus(), self.whole()

    def getValidatedData(self):
        df, dfplus = validated_data(self.args.collection, self.logger)
        df = self.average(df, df.index.min())
        return df, dfplus

    def revertFractions(self, df_o, dfplus_o):
        df_ind, df_plus = self.getValidatedData()
        df_o["expmass"] = df_ind.expmass
        df_o["zimi"] = df_o.mf * df_o.mass
        dfplus_o.drop(dfplus_o.loc[(dfplus_o.index != 4) & (dfplus_o.index != 5)].index, inplace=True)
        dfplus = pd.DataFrame()
        dfplus[["expmass", "scn"]] = df_plus[["expmass", "scn"]]
        dfplus = pd.concat([dfplus, dfplus_o], sort=True)
        dfplus.sort_index(inplace=True)
        dfplus["mf"] = dfplus.apply(lambda row: df_o.loc[df_o.index >= row.name, "mf"].sum(), axis=1)
        dfplus["mass"] = dfplus.apply(
            lambda row: df_o.loc[df_o.index >= row.name, "zimi"].sum() / df_o.loc[df_o.index >= row.name, "mf"].sum(),
            axis=1)
        return df_o, dfplus


class wismooth_new(object):
    def __init__(self, parent):
        self.__dict__ = parent.__dict__.copy()

    def smooth(self, df, dfplus, solver=7):
        self.logger.info("Executing General Mass Solver %s stage 1" % (solver))
        df["zimi"] = df.mass5 * df.mf
        df["wf"] = df.zimi / df.zimi.sum()
        df["slope"] = (df.mf - df.mf.shift(1))
        fs = max(8, df.index.min() + 1)
        avg_slope = np.mean(df.loc[df.index.isin([fs, fs + 1]), "slope"].tolist())
        swf = df.loc[df.index == fs - 1, "wf"].values[0] + avg_slope
        if not (df.loc[df.index == fs, "wf"].values[0] * 0.9 <= swf and swf <= df.loc[df.index == fs, "wf"].values[
            0] * 1.1):
            df.loc[df.index == fs, "wf"] = swf
        if solver == 7:
            res = optimize.minimize(self.cost_function, [1.], bounds=[[0.8, 1.2]], method="SLSQP", args=(df, dfplus),
                                    options={'maxiter': 500})
            if res.success == False:
                print res
                self.logger.info("General Mass Solver 7 stage 1 failed to find a feasible solution.")
                return "Failed"
            self.logger.info("Completed General Mass Solver 7 stage 1")
            self.logger.info("General Mass solver stage 1 final variables - %s, objective - %s" % (
                ",".join([str(x) for x in res.x]), res.fun))
            df = self.calcWf(res.x, df)
        else:
            df = self.calcWf([1.], df, solver)
        df["wf"] = df.new_wf
        df["wf"] = df.wf * (1. / df.wf.sum())
        df.drop(columns=["new_wf"], inplace=True)
        return df

    def cost_function(self, var, df_o, dfplus):
        df = df_o.copy(deep=True)
        df = self.calcWf(var, df)
        df["error"] = np.abs((df.wf - df.new_wf) / df.wf)
        df.loc[df.error > 10., "error"] = 0
        return df.error.sum() * 0.01

    def calcWf(self, var, df, solver=3):
        fs = max(8, df.index.min() + 1)
        fs = max(fs + 1, 10)
        df_s = df["wf"]
        for i in range(0, df.shape[0] - 5):
            ti = i + df.index.min()  # true index
            if ti < fs - 2: continue
            if solver == 3:
                df.loc[df.index == ti + 2, 'new_wf'] = (1. / 8. * df_s.iloc[i] + 1. / 4. * df_s.iloc[i + 1] + 1. / 4. *
                                                        df_s.iloc[i + 2] + 1. / 4. * df_s.iloc[i + 3] + 1. / 8. *
                                                        df_s.iloc[i + 4]) * var[0]
            else:
                df.loc[df.index == ti + 2, 'new_wf'] = np.median(
                    [df_s.iloc[i], df_s.iloc[i + 1], df_s.iloc[i + 2], df_s.iloc[i + 3], df_s.iloc[i + 4]])
        df.loc[df.new_wf.isnull(), "new_wf"] = df.wf
        return df


class MassSolver7(Initialize, object):
    def __init__(self, parent):
        self.__dict__ = parent.__dict__.copy()
        self.common = Common(self)

    def optimize(self):
        self.logger.info("Executing General Mass Solver 7")
        obj = Initialize(self, "mass", 2, "Gamma")
        df, dfplus = obj.df, obj.dfplus
        obj = wismooth_new(self)
        df = obj.smooth(df, dfplus)
        if type(df) == str:
            print 'Warning:{"Type": "Mass Solver", "Message": "General Mass Solver 7 failed to find a feasible solution. Executing Solver 8."}'
            return "Failed", df, dfplus
        res, df, dfplus = self.runSolver(df, dfplus)
        return res, df, dfplus

    def runSolver(self, df, dfplus, solver=7):
        df = self.calcNewMF(df)
        res = optimize.minimize(self.cost_function, [0.], bounds=[[-3., 3.]], method="SLSQP", args=(df, dfplus),
                                options={'maxiter': 500})
        if res.success == False:
            print res
            self.logger.info("General Mass Solver %s failed to find a feasible solution." % (solver))
            res.x = [1.]
        self.logger.info("Completed General Mass Solver %s" % (solver))
        self.logger.info("General Mass solver %s final variables - %s, objective - %s" % (
            solver, ",".join([str(x) for x in res.x]), res.fun))
        return res, df, dfplus

    def calcNewMF(self, df):
        df["zimi"] = df.mass5 * df.mf
        df["new_mf"] = df.wf * df.zimi.sum() / df.mass5
        df["new_mf"] = df.new_mf * (df.mf.sum() / df.new_mf.sum())
        df["mf"] = df.new_mf
        df.drop(columns=["new_mf"], inplace=True)
        return df

    def cost_function(self, var, df_o, dfplus):
        df = df_o.copy(deep=True)
        df, dfplus = self.calculateMass(var, df, dfplus)
        dfplus["error"] = ((dfplus.expmass - dfplus.mass) / dfplus.expmass) ** 2 / 0.001
        error = dfplus.error.sum()
        return error

    def calculateMass(self, var, df, dfplus):
        df["correction"] = (4.81668632961298 * np.exp(0.0140674955555049 * df.index)) * var[0]
        df.loc[df.index <= 5, "mass"] = df.expmass
        df.loc[df.index > 5, "mass"] = df.mass5 + df.mass5 * df.correction / 100.
        df["zimi"] = df.mf * df.mass
        dfplus["mf"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "mf"].sum(), axis=1)
        dfplus["mass"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "zimi"].sum() / row.mf, axis=1)
        return df, dfplus

    def save(self, res, df, dfplus):
        df, dfplus = self.calculateMass(res.x, df, dfplus)
        df, dfplus = self.common.revertFractions(df, dfplus)
        write_excel(self.args.debug, self.debugDir, ["mass-solver7_General"], ["Individual", "Plus"],
                    [df, dfplus])
        divisor = df.zimi.sum()
        df["wf"] = (df["mass"] * df["mf"]) / divisor
        write(df, "MBP", self.args.collection, logger=self.logger, Property="MCOutput", Identifier=self.what,
              PlusOrIndividual="Individual", Solver=7, Recommended=0, Distribution="General")
        write(dfplus, "MBP", self.args.collection, logger=self.logger, Property="MCOutput", Identifier=self.what,
              PlusOrIndividual="Plus", Solver=7, Recommended=0, Distribution="General")


class MassSolver8(Initialize, object):
    def __init__(self, parent):
        self.__dict__ = parent.__dict__.copy()
        self.common = Common(self)

    def optimize(self):
        self.logger.info("Executing General Mass Solver 8")
        obj = Initialize(self, "mass", 2, "Gamma")
        df, dfplus = obj.df, obj.dfplus
        obj = wismooth_new(self)
        df = obj.smooth(df, dfplus, 8)
        obj = MassSolver7(self)
        res, df, dfplus = obj.runSolver(df, dfplus, 8)
        return res, df, dfplus

    def save(self, res, df, dfplus):
        obj = MassSolver7(self)
        df, dfplus = obj.calculateMass(res.x, df, dfplus)
        df, dfplus = self.common.revertFractions(df, dfplus)
        write_excel(self.args.debug, self.debugDir, ["mass-solver8_General"], ["Individual", "Plus"],
                    [df, dfplus])
        divisor = df.zimi.sum()
        df["wf"] = (df["mass"] * df["mf"]) / divisor
        write(df, "MBP", self.args.collection, logger=self.logger, Property="MCOutput", Identifier=self.what,
              PlusOrIndividual="Individual", Solver=8, Recommended=0, Distribution="General")
        write(dfplus, "MBP", self.args.collection, logger=self.logger, Property="MCOutput", Identifier=self.what,
              PlusOrIndividual="Plus", Solver=8, Recommended=0, Distribution="General")


class MassSolver6(Initialize, object):
    def __init__(self, parent):
        self.__dict__ = parent.__dict__.copy()
        self.common = Common(self)

    def optimize(self):
        self.logger.info("Executing General Mass Solver 6.")
        obj = Initialize(self, "mass", 5)
        df, dfplus = obj.df, obj.dfplus
        sp = MassSolver5StartPoint()
        # The Starting Point of Solver 6, is required to be assigned from SOLVE5 as Starting Point4-5_Desilogic
        start_point = sp.findVarsStartingPoint(df, dfplus)
        start_point = [start_point[2], start_point[1]]
        bound = [[0.95, 1.1], [-1.5, 2.5]]
        res = optimize.minimize(self.cost_function, start_point, bounds=bound, method="SLSQP", args=(df, dfplus),
                                options={'maxiter': 500})
        df_res, dfplus_res = self.calculateFinalMass(res, df, dfplus)
        # In case any of the Zi <=10^-20, then print the results at starting point
        if res.success == False or df_res.loc[(df_res.index >= dfplus_res.index.max()) & (df_res.mf < 10 ** -20)].shape[
            0] >= 1:
            self.logger.info("%s" % (res))
            self.logger.info("General Mass Solver 6 failed to find a feasible solution.")
            self.logger.info(
                "Replacing solver variables with starting point and returning the output on start point as final output")
            res.x = start_point
            res.success = True
        self.logger.info("Completed POAS Special Mass Solver 6.")
        self.logger.info(
            "General Mass solver final variables - %s, objective - %s" % (",".join([str(x) for x in res.x]), res.fun))
        return res, df, dfplus

    def save(self, res, df, dfplus):
        df, dfplus = self.calculateFinalMass(res, df, dfplus)
        df, dfplus = self.common.revertFractions(df, dfplus)
        divisor = (dfplus.loc[dfplus.index == 5, "expmass"].values[0] * dfplus.loc[dfplus.index == 5, "mf"].values[0])
        df["wf"] = (df["mass"] * df["mf"]) / divisor
        write_excel(self.args.debug, self.debugDir, ["mass-solver6"], ["Individual", "Plus"], [df, dfplus])
        X = {"Regression": 0, "Iterations": res.nit, "Values": list(res.x), "Message": res.message}
        write(df, "MBP", self.args.collection, logger=self.logger, Property="MCOutput", Identifier=self.what,
              PlusOrIndividual="Individual", Solver=5, Recommended=0, Distribution="General")
        write(dfplus, "MBP", self.args.collection, logger=self.logger, Property="MCOutput", Identifier=self.what,
              PlusOrIndividual="Plus", Solver=5, Recommended=0, Distribution="General")

    def calculateFinalMass(self, res, df, dfplus):
        vars = res.x
        df, dfplus = self.calculateMass(vars, df, dfplus)
        df["mass"] = df.apply(lambda row: row.expmass if row.name < dfplus.index.max() else row.mass, axis=1)
        df["mf"] = df.apply(lambda row: row.mf if row.name < dfplus.index.max() else row.zi, axis=1)
        df["zimi"] = df.mf * df.mass
        dfplus["mass"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "zimi"].sum() / row.zi, axis=1)
        df.drop(columns=["zi"], inplace=True)
        dfplus.drop(columns=["zi"], inplace=True)
        return df, dfplus

    def cost_function(self, vars, df_o, dfplus):
        df, dfplus = self.calculateErrors(vars, df_o, dfplus)
        last_mf_ratio = (
            0 if df.loc[df.index == 121, "zi"].values[0] == 0 or df.loc[df.index == 120, "zi"].values[0] == 0 else
            df.loc[df.index == 121, "zi"].values[0] / df.loc[df.index == 120, "zi"].values[0])
        last_mass_ratio = (
            0 if df.loc[df.index == 121, "mass"].values[0] == 0 or df.loc[df.index == 120, "mass"].values[0] == 0 else
            df.loc[df.index == 121, "mass"].values[0] / df.loc[df.index == 120, "mass"].values[0])
        error = self.targetCost(dfplus.loc[dfplus.index == dfplus.index.max(), "zi_error"].sum(), 0.001)
        error += self.targetCost(dfplus.loc[dfplus.index == dfplus.index.max(), "mass_error"].sum(), 0.01)
        error += self.targetCost(df.loc[df.index <= dfplus.index.max(), "poas_error"].sum(), 0.01)
        error += self.targetCost(df.loc[df.index == dfplus.index.max() - 1, "zi_error"].sum(), 0.001)
        error += self.targetCost(df.loc[df.index == dfplus.index.max() - 1, "mass_error"].sum(), 0.01)
        error += self.targetCost(np.abs(1.3 - last_mf_ratio) / 1.3, 0.1)
        error += self.targetCost(np.abs(1.3 - last_mass_ratio) / 1.3, 0.1)
        return error

    def targetCost(self, val, target):
        return val / target

    def calculateErrors(self, vars, df_o, dfplus):
        df, dfplus = self.calculateMass(vars, df_o, dfplus)
        df["zi_error"] = np.abs((df.mf - df.zi) / df.mf)
        df["mass_error"] = np.abs((df.poas_mass - df.expmass) / df.expmass)
        dfplus["zi_error"] = np.abs((dfplus.mf - dfplus.zi) / dfplus.mf)
        dfplus["mass_error"] = np.abs((dfplus.mass - dfplus.expmass) / dfplus.expmass)
        df["poas_error"] = np.abs((df.mass_plus - df.poas_mass_plus) / df.mass_plus)
        return df, dfplus

    def calculateMass(self, vars, df_o, dfplus):
        df_o = self.addPOASMass(vars, df_o)
        df = df_o.copy(deep=True)
        df["mass"] = df.apply(lambda row: row.expmass if row.name < dfplus.index.max() else row.poas_mass, axis=1)
        df["zi_plus"] = df.apply(lambda row: dfplus.loc[dfplus.index == 5, "mf"].values[0] - df.loc[
            df.index < row.name, "mf"].sum() if row.name <= dfplus.index.max() else np.NaN, axis=1)
        df = self.calculateMassPlus(df, dfplus)
        df = self.calculateZi(df, dfplus)
        df["final_zi"] = df.apply(lambda row: row.mf if row.name < dfplus.index.max() else row.zi, axis=1)
        df["zimi"] = df.final_zi * df.mass
        df_o[["zi", "final_zi", "mass", "zimi", "poas_mass", "mass_plus", "poas_mass_plus"]] = df[
            ["zi", "final_zi", "mass", "zimi", "poas_mass", "mass_plus", "poas_mass_plus"]]
        dfplus["zi"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "final_zi"].sum(), axis=1)
        dfplus["mass"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "zimi"].sum() / row.zi, axis=1)
        return df_o, dfplus

    def calculateZi(self, df, dfplus):
        zi_plus, zi = list(), list()
        for index, row in df.iterrows():
            if index < dfplus.index.max() - 1:
                zi.append(row.mf)
                zi_plus.append(row.zi_plus)
            elif index == dfplus.index.max() - 1:
                zi_plus.append(row.zi_plus)
                zi_plus[-1] = 0 if zi_plus[-1] <= 10 ** -20 else zi_plus[-1]
                try:
                    mass_plus = df.loc[df.index == row.name + 1, "mass_plus"].values[0]
                except:
                    mass_plus = 0
                zi.append(zi_plus[-1] * (mass_plus - row.mass_plus) / (mass_plus - row.mass))
            else:
                zi_plus.append(zi_plus[-1] - zi[-1])
                try:
                    mass_plus = df.loc[df.index == row.name + 1, "mass_plus"].values[0]
                except:
                    mass_plus = 0
                zi.append(zi_plus[-1] * (mass_plus - row.mass_plus) / (mass_plus - row.mass))
                zi[-1] = 10 ** -20 if zi[-1] <= 10 ** -20 else zi[-1]
        df["zi_plus"] = zi_plus
        df["zi"] = zi
        return df

    def calculateMassPlus(self, df, dfplus):
        mass_plus, poas_mass_plus = list(), list()
        for index, row in df.iterrows():
            if 5 == index:
                mass = dfplus.loc[dfplus.index == index, "expmass"].values[0]
                poas_mass = mass
            elif index >= dfplus.index.max():
                mass = mass_plus[-1] + row.poas_mass_plus_slope
                poas_mass = poas_mass_plus[-1] + row.poas_mass_plus_slope
            else:
                prow = df.loc[df.index == row.name - 1].squeeze()
                mass = (mass_plus[-1] * prow.zi_plus - prow.expmass * prow.mf) / row.zi_plus
                poas_mass = poas_mass_plus[-1] + row.poas_mass_plus_slope if index > 7 else mass
            mass_plus.append(mass)
            poas_mass_plus.append(poas_mass)
        df["mass_plus"] = mass_plus
        df["poas_mass_plus"] = poas_mass_plus
        df.loc[df.index == df.index.max(), "mass"] = df.loc[df.index == df.index.max(), "mass_plus"]
        return df

    def addPOASMass(self, vars, df_o):
        df = df_o.copy(deep=True)
        df = pd.DataFrame()
        df["scn"] = ["C%s" % (x) for x in range(5, 122)]
        df.index = [x for x in range(5, 122)]
        df_o = pd.concat([df_o, df.loc[df.index > df_o.index.max()]], axis=0, sort=True)
        df["poas_mass_slope"] = df.apply(lambda row: (
                -0.0000000081143800838 * row.name ** 5 + 2.54535602253662E-06 * row.name ** 4 - 0.000278130020201878 * row.name ** 3 + 0.0132162236519363 * row.name ** 2 - 0.258386996099669 * row.name + 15.4092766381938) if row.name <= 100 else (
                0.509448043249933 * row.name ** 0.768554188962656), axis=1)
        df["poas_mass_plus_slope"] = df.poas_mass_slope
        df["poas_mass_slope"] = df.poas_mass_slope * vars[0]
        df["poas_mass"] = df.apply(
            lambda row: 82.81 + df.loc[(df.index <= row.name) & (df.index >= 7), "poas_mass_slope"].sum(), axis=1)
        df["mass_correction"] = (4.81668632961298 * np.exp(0.0140674955555049 * df.index)) * vars[1]
        df["poas_mass"] = df.poas_mass + df.poas_mass * df.mass_correction / 100.
        df_o[["poas_mass", "poas_mass_plus_slope"]] = df[["poas_mass", "poas_mass_plus_slope"]]
        df_o.loc[df_o.index == 5, "poas_mass"] = 72.15
        df_o.loc[df_o.index == 6, "poas_mass"] = 85.21
        return df_o


class MassSolver5StartPoint(object):
    def __init__(self):
        pass

    # Plus Fraction Slope Multiplier-till SCN 16  - var1
    # First Slope Multiplier  - var4
    # Second Slope Multiplier - var5
    # Mass correction Multiplier  - var2
    # Mass Slope Multiplier  - var3

    def findVarsStartingPoint(self, df_o, dfplus):
        df = df_o.copy(deep=True)
        df = pd.concat([df, dfplus.loc[dfplus.index == dfplus.index.max()]], axis=0, sort=True)
        df["zimi"] = df.expmass * df.mf
        df["mass_plus"] = df.apply(
            lambda row: df.loc[df.index >= row.name, "zimi"].sum() / df.loc[df.index >= row.name, "mf"].sum(), axis=1)
        var1 = self.firstVar(df, dfplus)
        var2, var3 = self.var2_3(df, dfplus)
        var4 = self.fourthVar(df, dfplus, var1)
        var5 = self.fifthVar(df, dfplus, var1, var4)
        return [var1, var2, var3, var4, var5]

    def firstVar(self, df, dfplus):
        last_plus = min(16, dfplus.index.max())
        # mass of last plus fraction -1
        mass_plus_olm1 = df.loc[df.index == last_plus - 1, "mass_plus"].values[0]
        last_slope = self.slope(last_plus)
        var = max(4, (df.loc[df.index == last_plus, "mass_plus"].values[0] - mass_plus_olm1)) / last_slope
        if dfplus.index.max() > 16:
            var = self.finalFirstVar(df, dfplus, var)
        return var

    def finalFirstVar(self, df, dfplus, tempVar1):
        last_plus = dfplus.index.max()
        vars, slopes = list(), list()
        multiplier = 80.1
        while multiplier >= 0.1:
            multiplier -= 0.1
            var1 = tempVar1 * multiplier
            var4 = self.fourthVar(df, dfplus, var1)
            slope_at = max(35, last_plus + 15)
            slope16, slope = self.slopeNumber(var1)
            slope = np.exp(np.log(slope16) + var4 * slope * (np.log(slope_at) - np.log(16)))
            vars.append(var1)
            slopes.append(slope)
        required_slope = self.requiredSlope(df)
        return PchipInterpolator(slopes, vars, extrapolate=True)(required_slope).item(0)

    def requiredSlope(self, df):
        df["slope"] = (np.log(df.mass_plus) - np.log(df.mass_plus.shift(1))) / (np.log(df.index) - np.log(df.index - 1))
        avg_slope = np.mean(df.slope.tail(4 if df.index.max() >= 11 else 3))
        bounds = OrderedDict(zip([160, 170, 200, 300, 1000], [0.8, 0.815, 0.95, 1.12, 1.15]))
        c7mass = df.loc[df.index == 7, "mass_plus"].values[0]
        for (key, value) in bounds.iteritems():
            if c7mass <= key:
                if avg_slope < value:
                    avg_slope = value
                break
        lpm = df.loc[df.index == df.index.max(), "mass_plus"].values[0]
        ls = df.index.max()
        slope_at = max(35, ls + 15)
        required_slope = (np.exp(np.log(lpm) + avg_slope * (np.log(slope_at) - np.log(ls))) - np.exp(
            np.log(lpm) + avg_slope * (np.log(slope_at - 1) - np.log(ls))))
        if required_slope <= 12:
            required_slope = 12
        elif required_slope >= 65:
            required_slope = 65
        return required_slope

    def slopeNumber(self, var1):
        slope15 = self.slope(15) * var1
        slope16 = self.slope(16) * var1
        slope = (np.log(slope16) - np.log(slope15)) / (np.log(16) - np.log(15))
        return slope16, slope

    def fourthVar(self, df, dfplus, var1):
        slope16, slope = self.slopeNumber(var1)
        last_plus = dfplus.index.max()
        required_slope = self.requiredSlope(df) if dfplus.index.max() <= 16 else max(4, df.loc[
            df.index == last_plus, "mass_plus"].values[0] - df.loc[df.index == last_plus - 1, "mass_plus"].values[0])
        last_plus = 35 if dfplus.index.max() <= 16 else dfplus.index.max()
        var = (np.log(required_slope) - np.log(slope16)) / ((np.log(last_plus) - np.log(16)) * slope)
        return var

    def fifthVar(self, df, dfplus, var1, var4):
        slope16, slope = self.slopeNumber(var1)
        last_plus = dfplus.index.max()
        slope_at = max(35, last_plus + 15)
        slope1 = np.exp(np.log(slope16) + slope * var4 * (np.log(slope_at + 4) - np.log(16)))
        slope2 = np.exp(np.log(slope16) + slope * var4 * (np.log(slope_at + 5) - np.log(16)))
        slope = (np.log(slope2) - np.log(slope1)) / (np.log(slope_at + 5) - np.log(slope_at + 4))
        var = (np.log(slope2 / 2. if slope2 / 2. >= 11.5 else 11.5) - np.log(slope2)) / (
                (np.log(121) - np.log(slope_at + 5)) * slope)
        return var

    def var2_3(self, df_o, dfplus):
        df = df_o.copy(deep=True)
        df.drop(df.loc[(df.index < 7) | (df.index >= dfplus.index.max())].index, inplace=True)
        vars3 = self.vars3()
        vars, errors = list(), list()
        for var3 in vars3:
            df["poas_mass_slope"] = var3 * df.apply(lambda row: (
                    -0.0000000081143800838 * row.name ** 5 + 2.54535602253662E-06 * row.name ** 4 - 0.000278130020201878 * row.name ** 3 + 0.0132162236519363 * row.name ** 2 - 0.258386996099669 * row.name + 15.4092766381938) if row.name <= 100 else (
                    0.509448043249933 * row.name ** 0.768554188962656), axis=1)
            df["poas_mass"] = df.apply(
                lambda row: 82.81 + df.loc[(df.index <= row.name) & (df.index >= 7), "poas_mass_slope"].sum(), axis=1)
            mass_correction_last_scn = (4.81668632961298 * np.exp(0.0140674955555049 * df.index.max()))
            var2 = (df.loc[df.index == df.index.max(), "expmass"].values[0] * 100. -
                    df.loc[df.index == df.index.max(), "poas_mass"].values[0] * 100.) / (
                           df.loc[df.index == df.index.max(), "poas_mass"].values[0] * mass_correction_last_scn)
            var2 = min(max(-1.2, var2), 2)
            df["poas_mass"] = df.poas_mass + df.poas_mass * var2 * (
                    4.81668632961298 * np.exp(0.0140674955555049 * df.index)) / 100.
            df["error"] = np.abs(df.expmass - df.poas_mass) / df.expmass
            vars.append([var2, var3])
            errors.append(df.error.sum())
        return vars[errors.index(min(errors))]

    def vars3(self):
        x = 1.1
        vars = list()
        while x >= 0.95:
            vars.append(x)
            x -= 0.005
        return vars

    def slope(self, scn):
        return (
                -0.0000554601725806236 * scn ** 5 + 0.00754033655953279 * scn ** 4 - 0.230779871372405 * scn ** 3 + 2.48690642287389 * scn ** 2 - 6.95180703002185 * scn + 1.50595054461093)


class MassSolver5(Initialize, object):
    def __init__(self, parent):
        self.__dict__ = parent.__dict__.copy()
        self.common = Common(self)

    def optimize(self, df=None, dfplus=None):
        self.logger.info("Executing General Mass Solver 5")
        if df is None and dfplus is None:
            obj = Initialize(self, "mass", 5)
            df, dfplus = obj.df, obj.dfplus
        self.logger.info("Finding start point for General Mass Solver 5")
        sp = MassSolver5StartPoint()
        start_point = sp.findVarsStartingPoint(df, dfplus)
        self.logger.info("General Mass solver final variables - %s" % (",".join([str(x) for x in start_point])))
        return start_point, df, dfplus

    def save(self, res, df, dfplus):
        df, dfplus = self.calculateFinalMass(res, df, dfplus)
        # In Case the Last Plus Fraction Mole Fraction is <Previous One, make the last plus fraction = previous one.
        if df.loc[df.index == df.index.max(), "mf"].values[0] < df.loc[df.index == df.index.max() - 1, "mf"].values[0]:
            df.loc[df.index == df.index.max(), "mf"].values[0] = df.loc[df.index == df.index.max() - 1, "mf"].values[0]

        # get all mfs
        df["slope"] = df.mf - df.mf.shift(1)
        slopes = df.loc[df.index >= 7, "slope"].tolist()
        trend, max_count = 0, 0
        for slope in slopes:
            if slope >= 10 ** -5:
                trend += 1
            else:
                if trend >= 5: max_count += 1
                trend = 0
        if max_count > 1:
            self.logger.info("Solver 5 result contain more than 1 maxima for zi. Executing solver 6.")
            write_excel(self.args.debug, self.debugDir, ["mass-solver5_rejected_output"],
                        ["Individual", "Plus"], [df, dfplus])
            obj = MassSolver6(self)
            res, df, dfplus = obj.optimize()
            obj.save(res, df, dfplus)
            set_recommended(self.args.collection, self.logger, self.what)
        else:
            df, dfplus = self.common.revertFractions(df, dfplus)
            divisor = (dfplus.loc[dfplus.index == 5, "expmass"].values[0] * dfplus.loc[dfplus.index == 5, "mf"].values[
                0])
            df["wf"] = (df["mass"] * df["mf"]) / divisor
            write_excel(self.args.debug, self.debugDir, ["mass-solver5"], ["Individual", "Plus"],
                        [df, dfplus])
            write(df, "MBP", self.args.collection, logger=self.logger, Property="MCOutput", Identifier=self.what,
                  PlusOrIndividual="Individual", Solver=5, Recommended=0, Distribution="General")
            write(dfplus, "MBP", self.args.collection, logger=self.logger, Property="MCOutput", Identifier=self.what,
                  PlusOrIndividual="Plus", Solver=5, Recommended=0, Distribution="General")

    def calculateFinalMass(self, res, df, dfplus):
        vars = res
        df, dfplus = self.calculateMass(vars, df, dfplus)
        df["mass"] = df.apply(lambda row: row.expmass if row.name < dfplus.index.max() else row.mass, axis=1)
        df["mf"] = df.apply(lambda row: row.mf if row.name < dfplus.index.max() else row.zi, axis=1)
        df["zimi"] = df.mf * df.mass
        dfplus["mass"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "zimi"].sum() / row.zi, axis=1)
        df.drop(columns=["zi"], inplace=True)
        dfplus.drop(columns=["zi"], inplace=True)
        return df, dfplus

    def calculateMass(self, vars, df_o, dfplus):
        df = self.calculateMassPlus(vars, df_o, dfplus)
        df = self.addPOASMass(vars, df, dfplus)
        df["mass"] = df.apply(lambda row: row.expmass if row.name < dfplus.index.max() else row.poas_mass, axis=1)
        df = self.calculateZi(df, dfplus)
        df["final_zi"] = df.apply(lambda row: row.mf if row.name < dfplus.index.max() else row.zi, axis=1)
        df["zimi"] = df.final_zi * df.mass
        dfplus["zi"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "final_zi"].sum(), axis=1)
        dfplus["mass"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "zimi"].sum() / row.zi, axis=1)
        return df, dfplus

    def calculateZi(self, df, dfplus):
        zi_plus, zi = list(), list()
        for index, row in df.iterrows():
            if index < dfplus.index.max() - 1:
                zi.append(row.mf)
                zi_plus.append(row.zi_plus)
            elif index == dfplus.index.max() - 1:
                zi_plus.append(row.zi_plus)
                try:
                    mass_plus = df.loc[df.index == row.name + 1, "mass_plus"].values[0]
                except:
                    mass_plus = 0
                zi.append(zi_plus[-1] * (mass_plus - row.mass_plus) / (mass_plus - row.mass))
            else:
                zi_plus.append(zi_plus[-1] - zi[-1])
                zi_plus[-1] = 0 if zi_plus[-1] <= 10 ** -20 else zi_plus[-1]
                try:
                    mass_plus = df.loc[df.index == row.name + 1, "mass_plus"].values[0]
                except:
                    mass_plus = 0
                zi.append(zi_plus[-1] * (mass_plus - row.mass_plus) / (mass_plus - row.mass))
                zi[-1] = 10 ** -20 if zi[-1] <= 10 ** -20 else zi[-1]
        df["zi_plus"] = zi_plus
        df["zi"] = zi
        return df

    def calculateMassPlus(self, vars, df_o, dfplus):
        df = pd.DataFrame()
        df["scn"] = ["C%s" % (x) for x in range(5, 122)]
        df.index = [x for x in range(5, 122)]
        df = pd.concat([df_o, df.loc[df.index > df_o.index.max()]], axis=0, sort=True)
        slopes = list()
        last_plus = dfplus.index.max()
        slope_at = max(35, last_plus + 15)
        change_at = slope_at + 5
        for index, row in df.iterrows():
            if index <= 16:
                slopes.append(vars[0] * (
                        -0.0000554601725806236 * index ** 5 + 0.00754033655953279 * index ** 4 - 0.230779871372405 * index ** 3 + 2.48690642287389 * index ** 2 - 6.95180703002185 * index + 1.50595054461093))
                if index == 16: slope = ((np.log(slopes[-1]) - np.log(slopes[-2])) / (
                        np.log(index) - np.log(index - 1))) * vars[3]
            elif index <= change_at:
                slopes.append(np.exp(np.log(slopes[-1]) + slope * (np.log(index) - np.log(index - 1))))
                if index == change_at: slope = ((np.log(slopes[-1]) - np.log(slopes[-2])) / (
                        np.log(index) - np.log(index - 1))) * vars[4]
            else:
                slopes.append(np.exp(np.log(slopes[-1]) + slope * (np.log(index) - np.log(index - 1))))
        df["poas_mass_plus_slope"] = slopes
        df["zi_plus"] = df.apply(lambda row: df.loc[df.index >= row.name, "mf"].sum() + dfplus.loc[
            (dfplus.index >= row.name) & (dfplus.index == dfplus.index.max()), "mf"].sum(), axis=1)
        mass_plus = list()
        for index, row in df.iterrows():
            if 5 == index:
                mass = dfplus.loc[dfplus.index == index, "expmass"].values[0]
            elif index >= dfplus.index.max():
                mass = mass_plus[-1] + row.poas_mass_plus_slope
            else:
                prow = df.loc[df.index == row.name - 1].squeeze()
                mass = (mass_plus[-1] * prow.zi_plus - prow.expmass * prow.mf) / row.zi_plus
            mass_plus.append(mass)
        df["mass_plus"] = mass_plus
        return df

    def addPOASMass(self, vars, df, dfplus):
        df["poas_mass_slope"] = vars[2] * df.apply(lambda row: (
                -0.0000000081143800838 * row.name ** 5 + 2.54535602253662E-06 * row.name ** 4 - 0.000278130020201878 * row.name ** 3 + 0.0132162236519363 * row.name ** 2 - 0.258386996099669 * row.name + 15.4092766381938) if row.name <= 100 else (
                0.509448043249933 * row.name ** 0.768554188962656), axis=1)
        df["poas_mass"] = df.apply(
            lambda row: 82.81 + df.loc[(df.index <= row.name) & (df.index >= 7), "poas_mass_slope"].sum(), axis=1)
        df["mass_correction"] = (4.81668632961298 * np.exp(0.0140674955555049 * df.index)) * vars[1]
        df["poas_mass"] = df.poas_mass + df.poas_mass * df.mass_correction / 100.
        df.loc[df.index == 5, "poas_mass"] = df.loc[df.index == 5, "expmass"]
        df.loc[df.index == 6, "poas_mass"] = df.loc[df.index == 6, "expmass"]
        mmd, ls = self.massMultiplierDistribution(df, dfplus)
        df.loc[df.index >= 20, "poas_mass"] = df.poas_mass + df.poas_mass * (
                4.81668632961298 * np.exp(0.0140674955555049 * df.index)) * mmd * (df.index - ls) / 100.
        df.loc[df.index == df.index.max(), "poas_mass"] = df.loc[df.index == df.index.max(), "mass_plus"].values[0]
        return df

    def massMultiplierDistribution(self, df, dfplus):
        m121 = df.loc[df.index == df.index.max(), "mass_plus"].values[0]
        multi = 1.02 if (
                                2.889447236181E-08 * m121 ** 2 + 0.0000409296482412009 * m121 + 0.879321608040208) < 1.02 else (
                2.889447236181E-08 * m121 ** 2 + 0.0000409296482412009 * m121 + 0.879321608040208)
        m120 = df.loc[df.index == df.index.max() - 1, "poas_mass"].values[0]
        m120r = m120 if m121 / multi <= m120 and m120 <= m121 / 1.02 else m121 / multi
        pmcr = (m120r - m120) / m120 * 100.
        mc120 = (4.81668632961298 * np.exp(0.0140674955555049 * 120.))
        multiplier = pmcr / mc120
        ls = max(19, dfplus.index.max() - 1)
        mmd = multiplier / (120. - ls)
        return mmd, ls


class MassSolver4(DistributionAnalysis, Initialize, object):
    def __init__(self, parent):
        self.__dict__ = parent.__dict__.copy()
        self.common = Common(self)

    def optimize(self):
        obj = Initialize(self, "mass", 4)
        df, dfplus = obj.df.copy(deep=True), obj.dfplus.copy(deep=True)
        start_points, bounds = self.bounds(df, dfplus)
        res = self.solver4_1(start_points, bounds, df, dfplus)
        if res["success"] == False:
            print res
            self.logger.info("General Mass Solver 4 failed to find a feasible solution.")
            return "Failed", df, dfplus
        res, df = self.solver4_2(res, obj.df, obj.dfplus)
        if res["success"] == False:
            print 'Error:{"Type": "Mass Solver", "Message": "Mass Solver failed to find a feasible solution. Please check input data. If issue persists, please raise with POAS support team."}'
            quit()
        obj.df["mass_bkp"] = df["expmass"]
        return res, obj.df, obj.dfplus

    def save(self, res, df, dfplus):
        a, b, eta = res["Values"]
        df.rename(columns={'expzi': 'mf', 'expziinside': 'ziinside', 'expcummziinside': 'cummziinside'}, inplace=True)
        dfplus.rename(columns={'expzi': 'mf', 'expziinside': 'ziinside', 'expcummziinside': 'cummziinside'},
                      inplace=True)
        df = self.common.calcMass(df, a, b, eta)
        max_replacement = dfplus.index.max() - 2
        df["mass"] = df.apply(lambda row: row.mass_bkp if row.name < max_replacement else row.mass, axis=1)
        divisor = (dfplus.loc[dfplus.index == 4, "expmass"].values[0] * dfplus.loc[dfplus.index == 4, "mf"].values[0])
        self.logger.info("Trying to smoothen output.")
        df, dfplus = self.common.revertFractions(df, dfplus)
        write_excel(self.args.debug, self.debugDir, ["mass-solver4_before_smoothing"],
                    ["Individual", "Plus"], [df, dfplus])
        sm = msmooth(4, self.args.gas)
        df, dfplus = sm.smooth(df, dfplus, df.index.min())
        df["wf"] = (df["mass"] * df["mf"]) / divisor
        write_excel(self.args.debug, self.debugDir, ["mass-solver4"], ["Individual", "Plus"], [df, dfplus])
        if df.index.max() >= dfplus.index.max():
            write(df, "MBP", self.args.collection, logger=self.logger, Property="MCOutput", Identifier=self.what,
                  PlusOrIndividual="Individual", Solver=4, Recommended=0, Distribution="General")
            write(dfplus, "MBP", self.args.collection, logger=self.logger, Property="MCOutput", Identifier=self.what,
                  PlusOrIndividual="Plus", Solver=4, Recommended=0, Distribution="General")
        else:
            self.logger.info("Max Plus fraction is less than user provided plus fraction. Discarding output.")

    def solver4_1(self, start_points, bounds, df, dfplus):
        self.logger.info("Executing General Mass Solver 4-1")
        res = GenericSolver(start_points, bounds, self.cost_function, debug=self.args.debug,
                            args=(df.copy(deep=True), dfplus.copy(deep=True)))
        self.logger.info("Result -- %s" % (res))
        self.logger.info("Completed General Mass Solver 4-1")
        return res

    def solver4_2(self, res, df, dfplus):
        b, eta, slope = res["Values"]
        df, dfplus = self.mass(res["Values"], [df, dfplus], False)
        a = ((((self.c4mass - eta) / eta) / gamma(1. + 1. / b)) ** b) * b
        df["zimi"] = df.mass * df.expzi
        dfplus["mass"] = dfplus.apply(lambda row: ((df.loc[df.index >= row.name, "zimi"].sum()) / row.expzi), axis=1)
        df[["mf", "ziinside", "cummziinside", "expmass"]] = df[["expzi", "expziinside", "expcummziinside", "mass"]]
        dfplus[["mf", "ziinside", "cummziinside"]] = dfplus[["expzi", "expziinside", "expcummziinside"]]
        df.drop(df.loc[df.index >= dfplus.index.max()].index, inplace=True)
        obj = MassSolver2(self)
        start_points, bounds = obj.bounds(df)
        res = obj.solver2(start_points, bounds, df, dfplus, printname="4-2")
        return res, df

    def bounds(self, df, dfplus):
        self.c4mass = dfplus.loc[dfplus.index == 4, "expmass"].values[0]
        self.Bounds = dict()
        eta, slope = 51.11, 13.5
        c7mass = dfplus.loc[dfplus.index == 7, "expmass"].values[0]
        B = 0.98 if (1.01449364938449 * np.log(c7mass) - 4.02010653506624) * 1.35 < 0.98 else (
                1.01449364938449 * np.log(c7mass) - 4.02010653506624)
        self.Bounds["B"] = [B * 0.82, B * 1.25]
        self.Bounds["Eta"] = [10., 70.]
        self.Bounds["Slope"] = [9., 15.5]
        initialBFn = (B - self.Bounds["B"][0]) / (self.Bounds["B"][1] - self.Bounds["B"][0])
        initialEtaFn = (eta - self.Bounds["Eta"][0]) / (self.Bounds["Eta"][1] - self.Bounds["Eta"][0])
        initialSlopeFn = (slope - self.Bounds["Slope"][0]) / (self.Bounds["Slope"][1] - self.Bounds["Slope"][0])
        return ([initialBFn, initialEtaFn, initialSlopeFn],
                [[self.Bounds["B"][0], self.Bounds["B"][1]], [self.Bounds["Eta"][0], self.Bounds["Eta"][1]],
                 [self.Bounds["Slope"][0], self.Bounds["Slope"][1]]])

    def cost_function(self, vars, args):
        df, dfplus = self.mass(vars, args)
        return self.gradients(vars, df, dfplus)

    def intercept(self, df, dfplus, vars):
        b, eta, slope = vars
        a = ((((self.c4mass - eta) / eta) / gamma(1. + 1. / b)) ** b) * b
        c6pluszi = 1. - (
                (dfplus.loc[dfplus.index == 7, "expzi"].values)[0] + (df.loc[df.index == 6, "expzi"].values)[0]) / \
                   dfplus.loc[dfplus.index == 4, "expzi"].values[0]
        c6pluspstar = ((log(1. / (1. - c6pluszi))) * (a / b)) ** (1. / b)
        c6plusmb = c6pluspstar * eta + eta
        c6plusqi = (log(1. / (1. - c6pluszi)))
        intercept = (2. * c6plusmb - slope * (5. - 5. + 6. - 5.)) / 2.
        return intercept

    def gradients(self, vars, df, dfplus):
        b, eta, slope = vars
        a = ((((self.c4mass - eta) / eta) / gamma(1 + 1 / b)) ** b) * b
        intercept = self.intercept(df, dfplus, vars)
        c6pluszi = 1 - (
                (dfplus.loc[dfplus.index == 7, "expzi"].values)[0] + (df.loc[df.index == 6, "expzi"].values)[0]) / \
                   dfplus.loc[dfplus.index == 4, "expzi"].values[0]
        c6plusqi = (log(1 / (1 - c6pluszi)))
        df["diffawrtb"] = (a / b) * (1 + log(a / b) + digamma(1 + 1 / b))
        df["diffawrteta"] = a * b * (1 / ((self.c4mass - eta) / eta)) * (-self.c4mass / eta ** 2)
        df["diffpwrtb"] = ((c6plusqi * (a / b)) ** (1 / b)) * (
                -1 / b * np.log(((c6plusqi * (a / b)) ** (1 / b))) + 1 / (a * b) * df.diffawrtb - 1 / b ** 2)
        df["diffpwrteta"] = (-1) * (slope * (df.scn + df.scn + 1 - 11)) / (2 * eta ** 2) + (
                (c6plusqi * (a / b)) ** (1 / b)) * (1 / (a * b) * df.diffawrteta)
        df["diffpwrtslope"] = (df.scn + df.scn + 1 - 11) / (2 * eta)
        df["diffqiwrtb"] = ((b / a) * df.pmb ** b) * (
                1 / b - 1 / a * df.diffawrtb + np.log(df.pmb) + (b / df.pmb) * df.diffpwrtb)
        df["diffqiwrteta"] = ((b / a) * df.pmb ** b) * ((b / df.pmb) * df.diffpwrteta - 1 / a * df.diffawrteta)
        df["diffqiwrtslope"] = ((b / a) * df.pmb ** b) * ((b / df.pmb) * df.diffpwrtslope)
        df["difflwrtb"] = np.exp(-df.qimb) * (-1) * df.diffqiwrtb
        df["difflwrteta"] = np.exp(-df.qimb) * (-1) * df.diffqiwrteta
        df["difflwrtslope"] = np.exp(-df.qimb) * (-1) * df.diffqiwrtslope
        df["changeinziwrtb"] = df['difflwrtb'].sub(df['difflwrtb'].shift())
        df["changeinziwrtb"] = -1 * df["changeinziwrtb"].fillna(df['difflwrtb'].iloc[0])
        df["changeinziwrteta"] = df['difflwrteta'].sub(df['difflwrteta'].shift())
        df["changeinziwrteta"] = -1 * df["changeinziwrteta"].fillna(df['difflwrteta'].iloc[0])
        df["changeinziwrtslope"] = df['difflwrtslope'].sub(df['difflwrtslope'].shift())
        df["changeinziwrtslope"] = -1 * df["changeinziwrtslope"].fillna(df['difflwrtslope'].iloc[0])
        df["changeinincgwrtqi"] = np.exp(-1 * df.qi) * df.qi ** (1 / b)
        df["changeinqi"] = df.diffqiwrtb * (-1 / ((1 + 1 / b) - 1) ** 2)
        df["changeinincgwrtb"] = df.apply(
            lambda row: self.common.DifferentiateSpecial(b, row.qi, row.incg, row.changeinqi), axis=1)
        df["changeinincgwrteta"] = df.changeinincgwrtqi * df.diffqiwrteta
        df["changeinincgwrtslope"] = df.changeinincgwrtqi * df.diffqiwrtslope
        df['tempdiff'] = df['changeinincgwrtb'].sub(df['changeinincgwrtb'].shift())
        df["tempdiff"] = df["tempdiff"].fillna(df['changeinincgwrtb'].iloc[0])
        df["changeinmasswrtb"] = ((1 / df.ziinside * (a / b) ** (1 / b)) * (df.tempdiff) + (df.incgdiff) * (
                1 / df.ziinside * (a / b) ** (1 / b)) * (-1 / df.ziinside * df.changeinziwrtb + 1 / (
                a * b) * df.diffawrtb - 1 / b ** 2 * (1 + log(a / b)))) * eta
        df['tempdiff'] = df['changeinincgwrteta'].sub(df['changeinincgwrteta'].shift())
        df["tempdiff"] = df["tempdiff"].fillna(df['changeinincgwrteta'].iloc[0])
        df["changeinmasswrteta"] = ((1 / df.ziinside * (a / b) ** (1 / b)) * (df.tempdiff) + (df.incgdiff) * (
                1 / df.ziinside * (a / b) ** (1 / b)) * (-1 / df.ziinside * df.changeinziwrteta + 1 / (
                a * b) * df.diffawrteta)) * eta + df.pav + 1
        df['tempdiff'] = df['changeinincgwrtslope'].sub(df['changeinincgwrtslope'].shift())
        df["tempdiff"] = df["tempdiff"].fillna(df['changeinincgwrtslope'].iloc[0])
        df["changeinmasswrtslope"] = ((a / b) ** (1 / b)) * (
                1 / df.ziinside * (df.tempdiff) - 1 / df.ziinside ** 2 * df.changeinziwrtslope * (
            df.incgdiff)) * eta
        df["changeinzimiwrtb"] = df.expzi * df.changeinmasswrtb
        df["changeinzimiwrteta"] = df.expzi * df.changeinmasswrteta
        df["changeinzimiwrtslope"] = df.expzi * df.changeinmasswrtslope
        dfplus.loc[dfplus.index < dfplus.index.max(), "changeinmasswrtb"] = dfplus.apply(
            lambda row: ((df.loc[df.index >= row.name, "changeinzimiwrtb"].sum()) / row.expzi), axis=1)
        dfplus.loc[dfplus.index < dfplus.index.max(), "changeinmasswrteta"] = dfplus.apply(
            lambda row: ((df.loc[df.index >= row.name, "changeinzimiwrteta"].sum()) / row.expzi), axis=1)
        dfplus.loc[dfplus.index < dfplus.index.max(), "changeinmasswrtslope"] = dfplus.apply(
            lambda row: ((df.loc[df.index >= row.name, "changeinzimiwrtslope"].sum()) / row.expzi), axis=1)
        dfplus = dfplus.drop(dfplus.tail(1).index)
        errors = dfplus["masserror"].tolist() + df["masserror"].tolist() + df["qierror"].tolist()
        derivativesb = (dfplus["changeinmasswrtb"] * (
                2 * (dfplus.mass - dfplus.expmass) / dfplus.expmass ** 2)).tolist() + (
                               df["changeinmasswrtb"] * (2 * (df.mass - df.expmass) / df.expmass ** 2)).tolist() + (
                               df["diffqiwrtb"] * (2 * (df.qimb - df.qizi) / df.qizi ** 2)).tolist()
        derivativeseta = (dfplus["changeinmasswrteta"] * (
                2 * (dfplus.mass - dfplus.expmass) / dfplus.expmass ** 2)).tolist() + (df["changeinmasswrteta"] * (
                2 * (df.mass - df.expmass) / df.expmass ** 2)).tolist() + (
                                 df["diffqiwrteta"] * (2 * (df.qimb - df.qizi) / df.qizi ** 2)).tolist()
        derivativesslope = (dfplus["changeinmasswrtslope"] * (
                2 * (dfplus.mass - dfplus.expmass) / dfplus.expmass ** 2)).tolist() + (
                                   df["changeinmasswrtslope"] * (
                                   2 * (df.mass - df.expmass) / df.expmass ** 2)).tolist() + (
                                   df["diffqiwrtslope"] * (2 * (df.qimb - df.qizi) / df.qizi ** 2)).tolist()
        derivatives = np.matmul(errors, np.transpose([derivativesb, derivativeseta, derivativesslope])).tolist()
        return sum(errors), derivatives, 0

    def mass(self, vars, args, truncate=True):
        b, eta, slope = vars
        a = ((((self.c4mass - eta) / eta) / gamma(1 + 1 / b)) ** b) * b
        df = args[0].copy(deep=True)
        dfplus = args[1].copy(deep=True)

        intercept = self.intercept(df, dfplus, vars)

        index = [x for x in range(df.index.max() + 1, 123)]
        df = pd.concat([df, pd.DataFrame(index, index=index, columns=["scn"])], sort=False)
        df["scn"] = df.index
        df["pzi"] = ((np.log(1 / (1 - df.expcummziinside))) * a / b) ** (1 / b)
        df["qizi"] = (np.log(1 / (1 - df.expcummziinside)))
        df["massnalkane"] = df.apply(lambda row: slope * (row.scn - 5) + intercept, axis=1)
        df["mb"] = df.massnalkane.add(df.massnalkane.shift(-1)) / 2
        df = df.drop(df.tail(1).index)
        df["pmb"] = (df.mb - eta) / eta
        df["qimb"] = (b / a) * df.pmb ** b

        df["ziinside"] = np.exp(-1 * df.qimb.shift(1)) - np.exp(-1 * df.qimb)
        df.loc[df.index == df.index.min(), "ziinside"] = 1 - np.exp(-1 * df.qimb.iloc[0])
        df["zicalc"] = df.ziinside * dfplus.loc[dfplus.index == 4, "expzi"].values[0]
        maxscn, df = self.findLastZi(df, "zicalc", dfplus.loc[dfplus.index == 4, "expzi"].values[0])
        df.loc[df.index == maxscn, "zicalc"] = dfplus.loc[dfplus.index == 4, "expzi"].values[0] - df.loc[
            df.index < maxscn, "zicalc"].sum()
        df["cummziinside"] = df.ziinside.cumsum()

        df = self.common.calcMass(df, a, b, eta)
        df["zimi"] = df.expzi * df.mass

        if truncate: df = df.loc[df.index < dfplus.index.max()]
        dfplus["zimi"] = dfplus.expmass * dfplus.expzi
        maxzimi = ((dfplus.loc[dfplus.index == dfplus.index.max(), "expmass"] * dfplus.loc[
            dfplus.index == dfplus.index.max(), "expzi"]).values)[0]
        dfplus.loc[dfplus.index < dfplus.index.max(), "mass"] = dfplus.apply(
            lambda row: ((df.loc[df.index >= row.name, "zimi"].sum() + maxzimi) / row.expzi), axis=1)

        # calculate errors
        df["masserror"] = df.apply(
            lambda row: np.NaN if row.expmass == 0 else ((row.expmass - row.mass) / row.expmass) ** 2, axis=1)
        df["qierror"] = ((df.qizi - df.qimb) / df.qizi) ** 2
        dfplus["masserror"] = ((dfplus.expmass - dfplus.mass) / dfplus.expmass) ** 2
        return df, dfplus


class MassSolver3(DistributionAnalysis, Initialize, object):
    def __init__(self, parent):
        self.__dict__ = parent.__dict__.copy()
        self.common = Common(self)

    def optimize(self):
        obj = Initialize(self, "mass", 3)
        df, dfplus = obj.df.copy(deep=True), obj.dfplus.copy(deep=True)
        start_points, bounds = self.bounds(df, dfplus)
        res = self.solver3_1(start_points, bounds, df, dfplus)
        if res["success"] == False:
            print res
            self.logger.info("General Mass Solver 3 failed to find a feasible solution.")
            return "Failed", df, dfplus
        res, df = self.solver3_2(res, obj.df, obj.dfplus)
        if res["success"] == False:
            print 'Error:{"Type": "Mass Solver", "Message": "Mass Solver failed to find a feasible solution. Please check input data. If issue persists, please raise with POAS support team."}'
            quit()
        return res, obj.df, obj.dfplus

    def save(self, res, df, dfplus):
        a, b, eta = res["Values"]
        df.rename(columns={'expzi': 'mf', 'expziinside': 'ziinside', 'expcummziinside': 'cummziinside'}, inplace=True)
        dfplus.rename(columns={'expzi': 'mf', 'expziinside': 'ziinside', 'expcummziinside': 'cummziinside'},
                      inplace=True)
        df = self.common.calcMass(df, a, b, eta)
        max_replacement = dfplus.index.max() - 2
        df["mass"] = df.apply(lambda row: row.expmass if row.name < max_replacement else row.mass, axis=1)
        divisor = (dfplus.loc[dfplus.index == 4, "expmass"].values[0] * dfplus.loc[dfplus.index == 4, "mf"].values[0])
        self.logger.info("Trying to smoothen output.")
        df, dfplus = self.common.revertFractions(df, dfplus)
        write_excel(self.args.debug, self.debugDir, ["mass-solver3_before_smoothing"],
                    ["Individual", "Plus"], [df, dfplus])
        sm = msmooth(3, self.args.gas)
        df, dfplus = sm.smooth(df, dfplus, df.loc[~df.expmass.isnull()].index.max())
        df["wf"] = (df["mass"] * df["mf"]) / divisor
        write_excel(self.args.debug, self.debugDir, ["mass-solver3"], ["Individual", "Plus"], [df, dfplus])
        if df.index.max() >= dfplus.index.max():
            write(df, "MBP", self.args.collection, logger=self.logger, Property="MCOutput", Identifier=self.what,
                  PlusOrIndividual="Individual", Solver=3, Recommended=0, Distribution="General")
            write(dfplus, "MBP", self.args.collection, logger=self.logger, Property="MCOutput", Identifier=self.what,
                  PlusOrIndividual="Plus", Solver=3, Recommended=0, Distribution="General")
        else:
            self.logger.info("Max Plus fraction is less than user provided plus fraction. Discarding output.")

    def solver3_1(self, start_points, bounds, df, dfplus):
        self.logger.info("Executing General Mass Solver 3-1")
        res = GenericSolver(start_points, bounds, self.cost_function, debug=self.args.debug,
                            args=(df.copy(deep=True), dfplus.copy(deep=True)))
        self.logger.info("Result -- %s" % (res))
        self.logger.info("Completed General Mass Solver 3-1")
        return res

    def solver3_2(self, res, df, dfplus):
        b, eta, slope = res["Values"]
        df, dfplus = self.mass(res["Values"], [df, dfplus], False)
        df["zimi"] = df.mass * df.expzi
        dfplus["mass"] = dfplus.apply(lambda row: ((df.loc[df.index >= row.name, "zimi"].sum()) / row.expzi), axis=1)
        a = ((((self.c4mass - eta) / eta) / gamma(1 + 1 / b)) ** b) * b
        df[["mf", "ziinside", "cummziinside", "expmass"]] = df[["expzi", "expziinside", "expcummziinside", "mass"]]
        dfplus[["mf", "ziinside", "cummziinside"]] = dfplus[["expzi", "expziinside", "expcummziinside"]]
        df.drop(df.loc[df.index >= dfplus.index.max()].index, inplace=True)
        obj = MassSolver2(self)
        start_points, bounds = obj.bounds(df)
        res = obj.solver2(start_points, bounds, df, dfplus, printname="3-2")
        return res, df

    def bounds(self, df, dfplus):
        self.c4mass = dfplus.loc[dfplus.index == 4, "expmass"].values[0]
        self.Bounds = dict()
        eta, slope = 51.11, 13.5
        c7mass = dfplus.loc[dfplus.index == 7, "expmass"].values[0]
        B = 0.98 if (1.01449364938449 * np.log(c7mass) - 4.02010653506624) * 1.35 < 0.98 else (
                1.01449364938449 * np.log(c7mass) - 4.02010653506624)
        self.Bounds["B"] = [B * 0.82, B * 1.25]
        self.Bounds["Eta"] = [10., 70.]
        self.Bounds["Slope"] = [9., 15.5]
        initialBFn = (B - self.Bounds["B"][0]) / (self.Bounds["B"][1] - self.Bounds["B"][0])
        initialEtaFn = (eta - self.Bounds["Eta"][0]) / (self.Bounds["Eta"][1] - self.Bounds["Eta"][0])
        initialSlopeFn = (slope - self.Bounds["Slope"][0]) / (self.Bounds["Slope"][1] - self.Bounds["Slope"][0])
        return ([initialBFn, initialEtaFn, initialSlopeFn],
                [[self.Bounds["B"][0], self.Bounds["B"][1]], [self.Bounds["Eta"][0], self.Bounds["Eta"][1]],
                 [self.Bounds["Slope"][0], self.Bounds["Slope"][1]]])

    def cost_function(self, vars, args):
        df, dfplus = self.mass(vars, args)
        return self.gradients(vars, df, dfplus)

    def intercept(self, df, dfplus, vars):
        b, eta, slope = vars
        a = ((((self.c4mass - eta) / eta) / gamma(1 + 1 / b)) ** b) * b
        c6pluszi = 1 - (dfplus.loc[dfplus.index == 7, "expzi"].values[0] + df.loc[df.index == 6, "expzi"].values[0]) / \
                   dfplus.loc[dfplus.index == 4, "expzi"].values[0]
        c6pluspstar = ((log(1 / (1 - c6pluszi))) * (a / b)) ** (1 / b)
        c6plusmb = c6pluspstar * eta + eta
        c6plusqi = (log(1 / (1 - c6pluszi)))
        intercept = (2 * c6plusmb - slope * (5 - 5 + 6 - 5)) / 2
        return intercept

    def gradients(self, vars, df, dfplus):
        b, eta, slope = vars
        a = ((((self.c4mass - eta) / eta) / gamma(1 + 1 / b)) ** b) * b
        intercept = self.intercept(df, dfplus, vars)
        c6pluszi = 1 - (
                (dfplus.loc[dfplus.index == 7, "expzi"].values)[0] + (df.loc[df.index == 6, "expzi"].values)[0]) / \
                   dfplus.loc[dfplus.index == 4, "expzi"].values[0]
        c6plusqi = (log(1 / (1 - c6pluszi)))
        df["diffawrtb"] = (a / b) * (1 + log(a / b) + digamma(1 + 1 / b))
        df["diffawrteta"] = a * b * (1 / ((self.c4mass - eta) / eta)) * (-self.c4mass / eta ** 2)
        df["diffpwrtb"] = ((c6plusqi * (a / b)) ** (1 / b)) * (
                -1 / b * np.log(((c6plusqi * (a / b)) ** (1 / b))) + 1 / (a * b) * df.diffawrtb - 1 / b ** 2)
        df["diffpwrteta"] = (-1) * (slope * (df.scn + df.scn + 1 - 11)) / (2 * eta ** 2) + (
                (c6plusqi * (a / b)) ** (1 / b)) * (1 / (a * b) * df.diffawrteta)
        df["diffpwrtslope"] = (df.scn + df.scn + 1 - 11) / (2 * eta)
        df["diffqiwrtb"] = ((b / a) * df.pmb ** b) * (
                1 / b - 1 / a * df.diffawrtb + np.log(df.pmb) + (b / df.pmb) * df.diffpwrtb)
        df["diffqiwrteta"] = ((b / a) * df.pmb ** b) * ((b / df.pmb) * df.diffpwrteta - 1 / a * df.diffawrteta)
        df["diffqiwrtslope"] = ((b / a) * df.pmb ** b) * ((b / df.pmb) * df.diffpwrtslope)
        df["difflwrtb"] = np.exp(-df.qimb) * (-1) * df.diffqiwrtb
        df["difflwrteta"] = np.exp(-df.qimb) * (-1) * df.diffqiwrteta
        df["difflwrtslope"] = np.exp(-df.qimb) * (-1) * df.diffqiwrtslope
        df["changeinziwrtb"] = df['difflwrtb'].sub(df['difflwrtb'].shift())
        df["changeinziwrtb"] = -1 * df["changeinziwrtb"].fillna(df['difflwrtb'].iloc[0])
        df["changeinziwrteta"] = df['difflwrteta'].sub(df['difflwrteta'].shift())
        df["changeinziwrteta"] = -1 * df["changeinziwrteta"].fillna(df['difflwrteta'].iloc[0])
        df["changeinziwrtslope"] = df['difflwrtslope'].sub(df['difflwrtslope'].shift())
        df["changeinziwrtslope"] = -1 * df["changeinziwrtslope"].fillna(df['difflwrtslope'].iloc[0])
        df["changeinincgwrtqi"] = np.exp(-1 * df.qi) * df.qi ** (1 / b)
        df["changeinqi"] = df.diffqiwrtb * (-1 / ((1 + 1 / b) - 1) ** 2)
        df["changeinincgwrtb"] = df.apply(
            lambda row: self.common.DifferentiateSpecial(b, row.qi, row.incg, row.changeinqi), axis=1)
        df["changeinincgwrteta"] = df.changeinincgwrtqi * df.diffqiwrteta
        df["changeinincgwrtslope"] = df.changeinincgwrtqi * df.diffqiwrtslope
        df['tempdiff'] = df['changeinincgwrtb'].sub(df['changeinincgwrtb'].shift())
        df["tempdiff"] = df["tempdiff"].fillna(df['changeinincgwrtb'].iloc[0])
        df["changeinmasswrtb"] = ((1 / df.ziinside * (a / b) ** (1 / b)) * (df.tempdiff) + (df.incgdiff) * (
                1 / df.ziinside * (a / b) ** (1 / b)) * (-1 / df.ziinside * df.changeinziwrtb + 1 / (
                a * b) * df.diffawrtb - 1 / b ** 2 * (1 + log(a / b)))) * eta
        df['tempdiff'] = df['changeinincgwrteta'].sub(df['changeinincgwrteta'].shift())
        df["tempdiff"] = df["tempdiff"].fillna(df['changeinincgwrteta'].iloc[0])
        df["changeinmasswrteta"] = ((1 / df.ziinside * (a / b) ** (1 / b)) * (df.tempdiff) + (df.incgdiff) * (
                1 / df.ziinside * (a / b) ** (1 / b)) * (-1 / df.ziinside * df.changeinziwrteta + 1 / (
                a * b) * df.diffawrteta)) * eta + df.pav + 1
        df['tempdiff'] = df['changeinincgwrtslope'].sub(df['changeinincgwrtslope'].shift())
        df["tempdiff"] = df["tempdiff"].fillna(df['changeinincgwrtslope'].iloc[0])
        df["changeinmasswrtslope"] = ((a / b) ** (1 / b)) * (
                1 / df.ziinside * (df.tempdiff) - 1 / df.ziinside ** 2 * df.changeinziwrtslope * (
            df.incgdiff)) * eta

        df["changeinzimiwrtb"] = df.expzi * df.changeinmasswrtb
        df["changeinzimiwrteta"] = df.expzi * df.changeinmasswrteta
        df["changeinzimiwrtslope"] = df.expzi * df.changeinmasswrtslope
        last_expmass_scn = df.loc[~df.expmass.isnull()].index.max()
        df.loc[df.index <= last_expmass_scn, "changeinzimiwrtb"] = 0
        df.loc[df.index <= last_expmass_scn, "changeinzimiwrteta"] = 0
        df.loc[df.index <= last_expmass_scn, "changeinzimiwrtslope"] = 0

        dfplus.loc[dfplus.index < dfplus.index.max(), "changeinmasswrtb"] = dfplus.apply(
            lambda row: ((df.loc[df.index >= row.name, "changeinzimiwrtb"].sum()) / row.expzi), axis=1)
        dfplus.loc[dfplus.index < dfplus.index.max(), "changeinmasswrteta"] = dfplus.apply(
            lambda row: ((df.loc[df.index >= row.name, "changeinzimiwrteta"].sum()) / row.expzi), axis=1)
        dfplus.loc[dfplus.index < dfplus.index.max(), "changeinmasswrtslope"] = dfplus.apply(
            lambda row: ((df.loc[df.index >= row.name, "changeinzimiwrtslope"].sum()) / row.expzi), axis=1)
        dfplus = dfplus.drop(dfplus.tail(1).index)
        errors = dfplus["masserror"].tolist() + df["masserror"].tolist() + df["qierror"].tolist()
        derivativesb = (dfplus["changeinmasswrtb"] * (
                2 * (dfplus.mass - dfplus.expmass) / dfplus.expmass ** 2)).tolist() + (
                               df["changeinmasswrtb"] * (2 * (df.mass - df.expmass) / df.expmass ** 2)).tolist() + (
                               df["diffqiwrtb"] * (2 * (df.qimb - df.qizi) / df.qizi ** 2)).tolist()
        derivativeseta = (dfplus["changeinmasswrteta"] * (
                2 * (dfplus.mass - dfplus.expmass) / dfplus.expmass ** 2)).tolist() + (df["changeinmasswrteta"] * (
                2 * (df.mass - df.expmass) / df.expmass ** 2)).tolist() + (
                                 df["diffqiwrteta"] * (2 * (df.qimb - df.qizi) / df.qizi ** 2)).tolist()
        derivativesslope = (dfplus["changeinmasswrtslope"] * (
                2 * (dfplus.mass - dfplus.expmass) / dfplus.expmass ** 2)).tolist() + (
                                   df["changeinmasswrtslope"] * (
                                   2 * (df.mass - df.expmass) / df.expmass ** 2)).tolist() + (
                                   df["diffqiwrtslope"] * (2 * (df.qimb - df.qizi) / df.qizi ** 2)).tolist()
        derivatives = np.matmul(errors, np.transpose([derivativesb, derivativeseta, derivativesslope])).tolist()
        return sum(errors), derivatives, 0

    def mass(self, vars, args, truncate=True):
        b, eta, slope = vars
        a = ((((self.c4mass - eta) / eta) / gamma(1 + 1 / b)) ** b) * b
        df = args[0].copy(deep=True)
        dfplus = args[1].copy(deep=True)
        intercept = self.intercept(df, dfplus, vars)

        index = [x for x in range(df.index.max() + 1, df.index.max() + 2)]
        df = pd.concat([df, pd.DataFrame(index, index=index, columns=["scn"])], sort=False)

        df["scn"] = df.index
        df["pzi"] = ((np.log(1 / (1 - df.expcummziinside))) * a / b) ** (1 / b)
        df["qizi"] = (np.log(1 / (1 - df.expcummziinside)))
        df["massnalkane"] = df.apply(lambda row: slope * (row.scn - 5) + intercept, axis=1)
        df["mb"] = df.massnalkane.add(df.massnalkane.shift(-1)) / 2
        df = df.drop(df.tail(1).index)
        df["pmb"] = (df.mb - eta) / eta
        df["qimb"] = (b / a) * df.pmb ** b

        df["ziinside"] = np.exp(-1 * df.qimb.shift(1)) - np.exp(-1 * df.qimb)
        df.loc[df.index == df.index.min(), "ziinside"] = 1 - np.exp(-1 * df.qimb.iloc[0])
        df["zicalc"] = df.ziinside * dfplus.loc[dfplus.index == 4, "expzi"].values[0]
        maxscn, df = self.findLastZi(df, "zicalc", dfplus.loc[dfplus.index == 4, "expzi"].values[0])
        df.loc[df.index == maxscn, "zicalc"] = dfplus.loc[dfplus.index == 4, "expzi"].values[0] - df.loc[
            df.index < maxscn, "zicalc"].sum()
        df["cummziinside"] = df.ziinside.cumsum()

        df = self.common.calcMass(df, a, b, eta)

        last_expmass_scn = df.loc[~df.expmass.isnull()].index.max()
        df.loc[df.index <= last_expmass_scn, "zimi"] = df.expzi * df.expmass
        df.loc[df.index > last_expmass_scn, "zimi"] = df.expzi * df.mass

        if truncate: df = df.loc[df.index < dfplus.index.max()]
        dfplus["zimi"] = dfplus.expmass * dfplus.expzi
        maxzimi = ((dfplus.loc[dfplus.index == dfplus.index.max(), "expmass"] * dfplus.loc[
            dfplus.index == dfplus.index.max(), "expzi"]).values)[0]
        dfplus.loc[dfplus.index < dfplus.index.max(), "mass"] = dfplus.apply(
            lambda row: ((df.loc[df.index >= row.name, "zimi"].sum() + maxzimi) / row.expzi), axis=1)

        # calculate errors
        df["masserror"] = df.apply(
            lambda row: np.NaN if row.expmass == 0 else ((row.expmass - row.mass) / row.expmass) ** 2, axis=1)
        df["qierror"] = ((df.qizi - df.qimb) / df.qizi) ** 2
        dfplus["masserror"] = ((dfplus.expmass - dfplus.mass) / dfplus.expmass) ** 2
        return df, dfplus


class MassSolver2(Initialize, object):
    def __init__(self, parent):
        self.__dict__ = parent.__dict__.copy()
        self.common = Common(self)

    def optimize(self):
        obj = Initialize(self, "mass", 2)
        df, dfplus = obj.df.copy(deep=True), obj.dfplus.copy(deep=True)
        df.drop(df.loc[np.isnan(df.expmass)].index, inplace=True)
        start_points, bounds = self.bounds(df)
        res = self.solver2(start_points, bounds, df, dfplus)
        return res, obj.df, obj.dfplus

    def save(self, res, df, dfplus):
        a, b, eta = res["Values"]
        df = self.common.calcMass(df, a, b, eta)
        df["mass"] = df.apply(lambda row: row.expmass if row.name < dfplus.index.max() - 2 else row.mass, axis=1)
        df["zimi"] = df.mf * df.mass
        dfplus["mass"] = dfplus.apply(
            lambda row: df.loc[df.index >= row.name, "zimi"].sum() / df.loc[df.index >= row.name, "mf"].sum(), axis=1)
        self.logger.info("Trying to smoothen output.")
        df, dfplus = self.common.revertFractions(df, dfplus)
        write_excel(self.args.debug, self.debugDir, ["mass-solver2_before_smoothing"],
                    ["Individual", "Plus"], [df, dfplus])
        sm = msmooth(2, self.args.gas)
        df, dfplus = sm.smooth(df, dfplus, df.loc[~df.expmass.isnull()].index.max())
        divisor = (dfplus.loc[dfplus.index == 4, "expmass"].values[0] * dfplus.loc[dfplus.index == 4, "mf"].values[0])
        df["wf"] = (df["mass"] * df["mf"]) / divisor
        write_excel(self.args.debug, self.debugDir, ["mass-solver2"], ["Individual", "Plus"], [df, dfplus])
        if df.index.max() >= dfplus.index.max():
            write(df, "MBP", self.args.collection, logger=self.logger, Property="MCOutput", Identifier=self.what,
                  PlusOrIndividual="Individual", Solver=2, Recommended=0, Distribution="General")
            write(dfplus, "MBP", self.args.collection, logger=self.logger, Property="MCOutput", Identifier=self.what,
                  PlusOrIndividual="Plus", Solver=2, Recommended=0, Distribution="General")
        else:
            self.logger.info("Max Plus fraction is less than user provided plus fraction. Discarding output.")

    def solver2(self, start_points, bounds, df, dfplus, printname=None):
        self.logger.info("Executing General Mass Solver %s" % ("2" if printname is None else printname))
        res = GenericSolver(start_points, bounds, self.cost_function, debug=self.args.debug,
                            args=(df.copy(deep=True), dfplus.copy(deep=True)))
        if res["success"] == False:
            print res
            self.logger.info("General Mass Solver %s failed to find a feasible solution." % (
                "2" if printname is None else printname))
            return "Failed", df, dfplus
        self.logger.info("Result -- %s" % (res))
        self.logger.info("Completed General Mass Solver %s" % ("2" if printname is None else printname))
        return res

    def bounds(self, df):
        self.Bounds = dict()
        self.Bounds["Eta"] = [10., 70.]
        df['xzci'] = df.cummziinside / 2 + df.cummziinside.shift() / 2
        df.xzci = df.xzci.fillna(df['cummziinside'].iloc[0] / 2)
        df["xi"] = df.apply(lambda row: log(log(1 / (1 - row.xzci))), axis=1)
        df["xi2"] = df.xi ** 2
        A, B, eta = self.maximizeR2(df.copy(deep=True))
        initialEtaFn = (eta - self.Bounds["Eta"][0]) / (self.Bounds["Eta"][1] - self.Bounds["Eta"][0])
        initialAFn = (A - self.Bounds["A"][0]) / (self.Bounds["A"][1] - self.Bounds["A"][0])
        initialBFn = (B - self.Bounds["B"][0]) / (self.Bounds["B"][1] - self.Bounds["B"][0])
        return ([initialAFn, initialBFn, initialEtaFn],
                [[self.Bounds["A"][0], self.Bounds["A"][1]], [self.Bounds["B"][0], self.Bounds["B"][1]],
                 [self.Bounds["Eta"][0], self.Bounds["Eta"][1]]])

    def maximizeR2(self, df_o):
        df = df_o.copy(deep=True)
        count = df.shape[0]
        eta = self.Bounds["Eta"][0]
        result = dict()
        result["r2"] = list()
        result["vars"] = list()
        while eta < df.expmass.min():
            df["yi"] = df.apply(lambda row: log((row.expmass - eta) / eta), axis=1)
            df["xiyi"] = df.xi.values * df.yi.values
            df["yi2"] = df.yi.values ** 2
            r2 = (count * df.xiyi.sum() - df.xi.sum() * df.yi.sum()) ** 2 / (
                    (count * df.xi2.sum() - df.xi.sum() ** 2) * (count * df.yi2.sum() - df.yi.sum() ** 2))
            c2 = (df.xi.sum() * df.yi.sum() - count * df.xiyi.sum()) / (df.xi.sum() ** 2 - count * df.xi2.sum())
            c1 = (df.yi.sum() - c2 * df.xi.sum()) / count
            b = 1 / c2
            a = b * exp(c1 * b)
            result["r2"].append(r2)
            result["vars"].append([a, b, eta])
            df.drop(["yi", "xiyi", "yi2"], axis=1, inplace=True)
            eta += 0.1
        maxima = result["r2"].index(max(result["r2"]))
        self.Bounds["A"] = [0.2, ceil(result["vars"][maxima][0] * 3)]
        self.Bounds["B"] = [0.2, ceil(result["vars"][maxima][1] * 3)]
        return result["vars"][maxima]

    def cost_function(self, vars, args):
        error1, differentiations1, r = self.costIndividual(vars, args)
        error2, differentiations2, r2 = self.costPlus(vars, args)
        differentiations = list()
        for i in range(0, len(differentiations1)):
            differentiations.append(differentiations1[i] + differentiations2[i])
        return error1 + error2, differentiations, r

    def costIndividual(self, vars, args):
        a, b, eta = vars
        df = args[0].copy(deep=True)
        df = self.common.calcMass(df, a, b, eta)
        df["error"] = ((df.expmass - df.mass) / df.expmass) ** 2
        df["diffincg"] = df.apply(lambda row: self.common.Differentiate(b, row.qi, row.incg), axis=1)
        df["diffincgdiff"] = df['diffincg'].sub(df['diffincg'].shift())
        df.diffincgdiff = df.diffincgdiff.fillna(df['diffincg'].iloc[0])
        df = df.loc[df.index >= df.index.max() - 1]
        df["diffmasswrta"] = df.pav.values * eta / (a * b)
        df["diffmasswrteta"] = 1 + df.pav.values
        df["diffmasswrtb"] = (1 / df.cummziinsidediff.values) * (a / b) ** (1 / b) * (
                df.incgdiff.values * (-(log(a / b) + 1) / b ** 2) + df.diffincgdiff.values) * eta
        df["differrorwrta"] = df.diffmasswrta.values * 2 * (
                (df.mass.values - df.expmass.values) / df.expmass.values ** 2)
        df["differrorwrtb"] = df.diffmasswrtb.values * 2 * (
                (df.mass.values - df.expmass.values) / df.expmass.values ** 2)
        df["differrorwrteta"] = df.diffmasswrteta.values * 2 * (
                (df.mass.values - df.expmass.values) / df.expmass.values ** 2)
        df["derivativea"] = df.differrorwrta.values * df.error.values
        df["derivativeb"] = df.differrorwrtb.values * df.error.values
        df["derivativeeta"] = df.differrorwrteta.values * df.error.values
        avg = df.expmass.mean()
        df["r21"] = (df.mass.values - df.expmass.values) ** 2
        df["r22"] = (df.mass.values - avg) ** 2
        r2 = 1 - df.r21.sum() / df.r22.sum()
        df.drop(["r21", "r22"], axis=1, inplace=True)
        return df["error"].sum(), [df["derivativea"].sum(), df["derivativeb"].sum(), df["derivativeeta"].sum()], r2

    def costPlus(self, vars, args):
        a, b, eta = vars
        dfplus = args[1].copy(deep=True)
        dfplus = self.common.calcMass(dfplus, a, b, eta, 1)
        dfplus = dfplus.loc[dfplus.index == dfplus.index.max()]
        dfplus["error"] = ((dfplus.expmass - dfplus.mass) / dfplus.expmass) ** 2
        p = (10000 - eta) / eta
        q = 0.99999999
        qi = log(1 / (1 - q))
        incg = gammainc(1 + 1 / b, qi) * gamma(1 + 1 / b)
        lastdifferentiation = self.common.Differentiate(b, qi, incg)
        dfplus["diffincg"] = dfplus.apply(lambda row: self.common.Differentiate(b, row.qi, row.incg), axis=1)
        dfplus["diffmasswrtb"] = (1 / (1 - dfplus.cummziinside.values)) * (a / b) ** (1 / b) * (
                (incg - dfplus.incg.values) * (-(log(a / b) + 1) / b ** 2) + (
                lastdifferentiation - dfplus.diffincg.values)) * eta
        dfplus["diffmasswrta"] = dfplus.pav.values * eta / (a * b)
        dfplus["diffmasswrteta"] = 1 + dfplus.pav.values
        dfplus["differrorwrta"] = dfplus.diffmasswrta.values * 2 * (
                (dfplus.mass.values - dfplus.expmass.values) / dfplus.expmass.values ** 2)
        dfplus["differrorwrtb"] = dfplus.diffmasswrtb.values * 2 * (
                (dfplus.mass.values - dfplus.expmass.values) / dfplus.expmass.values ** 2)
        dfplus["differrorwrteta"] = dfplus.diffmasswrteta.values * 2 * (
                (dfplus.mass.values - dfplus.expmass.values) / dfplus.expmass.values ** 2)
        dfplus["derivativea"] = dfplus.differrorwrta.values * dfplus.error.values
        dfplus["derivativeb"] = dfplus.differrorwrtb.values * dfplus.error.values
        dfplus["derivativeeta"] = dfplus.differrorwrteta.values * dfplus.error.values
        avg = dfplus.expmass.mean()
        dfplus["r21"] = (dfplus.mass.values - dfplus.expmass.values) ** 2
        dfplus["r22"] = (dfplus.mass.values - avg) ** 2
        r2 = 1 - dfplus.r21.sum() / dfplus.r22.sum()
        dfplus.drop(["r21", "r22"], axis=1, inplace=True)
        return dfplus["error"].sum(), [dfplus["derivativea"].sum(), dfplus["derivativeb"].sum(),
                                       dfplus["derivativeeta"].sum()], r2


class MassSolver1_general(Initialize, object):
    def __init__(self, parent):
        self.__dict__ = parent.__dict__.copy()
        self.common = Common(self)

    def optimize(self):
        self.logger.info("Executing General Mass Solver 1")
        obj = Initialize(self, "mass", 1)
        df, dfplus = obj.df, obj.dfplus
        b = 1.05001515362526000000 * np.log(dfplus.loc[dfplus.index == 5, "expmass"].values[0]) - 4.24581065084051000000
        res = optimize.minimize(self.cost_function, [0., 1, b, 70.],
                                bounds=[[-1.5, 4.], [0.95, 1.1], [0.5, 5.], [68., 72.1]], method="SLSQP",
                                args=(df, dfplus), options={'maxiter': 500})
        if res.success == False:
            print res
            self.logger.info("General Mass Solver 1 failed to find a feasible solution.")
            return "Failed", df, dfplus
        self.logger.info("Completed General Special Mass Solver 1")
        self.logger.info(
            "General Mass solver final variables - %s, objective - %s" % (",".join([str(x) for x in res.x]), res.fun))
        return res, df, dfplus

    def save(self, res, df, dfplus):
        df, dfplus = self.calculateMass(res.x, df, dfplus)
        df[["mf", "mf_bkp"]] = df[["zi", "mf"]]
        self.logger.info("Trying to smoothen output.")
        write_excel(self.args.debug, self.debugDir, ["mass-solver1_before_smoothing"],
                    ["Individual", "Plus"], [df, dfplus])
        wi = wismooth()
        df, dfplus = wi.smooth(df, dfplus, self.logger)
        df, dfplus = self.calculateFinalMass(df, dfplus)
        write_excel(self.args.debug, self.debugDir, ["mass-solver1"], ["Individual", "Plus"], [df, dfplus])

        # create 16+
        dfplus["zimi"] = dfplus.expmass * dfplus.mf
        df["zimi"] = df.mass * df.mf
        mf = dfplus.loc[dfplus.index == dfplus.index.max(), "mf"].values[0] - df.loc[
            (df.index >= dfplus.index.max()) & (df.index < 16), "mf"].sum()
        mass = (dfplus.loc[dfplus.index == dfplus.index.max(), "zimi"].values[0] - df.loc[
            (df.index >= dfplus.index.max()) & (df.index < 16), "zimi"].sum()) / mf
        dfplus_temp = pd.DataFrame(data=[[mf, mass, mass, "C16"]], columns=["mf", "expmass", "mass", "scn"], index=[16])
        df.drop(df.loc[df.index >= 16].index, inplace=True)
        df["expmass"] = df.mass
        dfplus = pd.concat([dfplus, dfplus_temp], axis=0, sort=True)
        obj = MassSolver5(self)
        res, df, dfplus = obj.optimize(df, dfplus)
        if type(res) != str:
            obj.save(res, df, dfplus)
            set_recommended(self.args.collection, self.logger, self.what)

    def calculateFinalMass(self, df, dfplus):
        df["mass"] = df.apply(lambda row: row.expmass if row.name < dfplus.index.max() else row.mass, axis=1)
        df["mf"] = df.apply(lambda row: row.mf_bkp if row.name < dfplus.index.max() else row.mf, axis=1)
        df["zimi"] = df.mf * df.mass
        dfplus["mass"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "zimi"].sum() / row.mf, axis=1)
        return df, dfplus

    def cost_function(self, vars, df_o, dfplus):
        df, dfplus = self.calculateErrors(vars, df_o, dfplus)
        error = 0
        for index, row in dfplus.iterrows():
            error += self.targetCost(dfplus.loc[dfplus.index == index, "zi_error"].values[0],
                                     0.001) if index >= 6 else 0
            error += self.targetCost(dfplus.loc[dfplus.index == index, "mass_error"].values[0],
                                     0.001) if index >= 6 else 0
        return error

    def targetCost(self, val, target):
        if math.isnan(val): val = 0
        return val / target

    def calculateErrors(self, vars, df_o, dfplus):
        df, dfplus = self.calculateMass(vars, df_o, dfplus)
        dfplus["zi_error"] = ((dfplus.mf - dfplus.zi) / dfplus.mf) ** 2
        dfplus["mass_error"] = ((dfplus.mass - dfplus.expmass) / dfplus.expmass) ** 2
        return df, dfplus

    def calculateMass(self, vars, df_o, dfplus):
        df_o = self.addPOASMass(vars, df_o)
        df = df_o.copy(deep=True)
        A = self.calculateA(vars, dfplus)
        df["pstar"] = (df.poas_mass - vars[3]) / vars[3]
        df["qi"] = (vars[2] / A) * df.pstar ** vars[2]
        df["zicumm"] = df.apply(lambda row: 10 ** -20 if np.exp(-1. * row.qi) <= 10 ** -20 else np.exp(-1. * row.qi),
                                axis=1)
        df["ziinside"] = df.zicumm.shift(1) - df.zicumm
        df.loc[df.index == df.index.min(), 'ziinside'] = 1. - df['zicumm'].iloc[0]
        df["incg"] = df.apply(lambda row: gammainc(1. + 1. / vars[2], row.qi) * gamma(1. + 1. / vars[2]), axis=1)
        df['incgdiff'] = df['incg'].sub(df['incg'].shift())
        df.incgdiff = df.incgdiff.fillna(df['incg'].iloc[0])
        df["pav"] = (1 / df.ziinside) * (A / vars[2]) ** (1. / vars[2]) * df.incgdiff
        df["mass"] = df.apply(lambda row: 0 if row.pav == 0 else vars[3] * (1. + row.pav), axis=1)
        df["zi"] = df.ziinside * dfplus.loc[dfplus.index == 5, "mf"].values[0]
        df["zimi"] = df.zi * df.mass
        df_o[["zi", "mass", "zimi"]] = df[["zi", "mass", "zimi"]]
        dfplus["zi"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "zi"].sum(), axis=1)
        dfplus["mass"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "zimi"].sum() / row.zi, axis=1)
        return df_o, dfplus

    def calculateA(self, vars, dfplus):
        pstar_avg = (dfplus.loc[dfplus.index == 5, "expmass"].values[0] - vars[3]) / vars[3]
        A = vars[2] * (pstar_avg / (gamma(1. + 1. / vars[2]))) ** vars[2]
        return A

    def addPOASMass(self, vars, df_o):
        df = df_o.copy(deep=True)
        df = pd.DataFrame()
        df["scn"] = ["C%s" % (x) for x in range(5, 122)]
        df.index = [x for x in range(5, 122)]
        df_o = pd.concat([df_o, df.loc[df.index > df_o.index.max()]], axis=0, sort=True)
        df["poas_mass_slope"] = vars[1] * df.apply(lambda row: (
                -0.0000000081143800838 * row.name ** 5 + 2.54535602253662E-06 * row.name ** 4 - 0.000278130020201878 * row.name ** 3 + 0.0132162236519363 * row.name ** 2 - 0.258386996099669 * row.name + 15.4092766381938) if row.name <= 100 else (
                0.509448043249933 * row.name ** 0.768554188962656), axis=1)
        df["poas_mass"] = df.apply(
            lambda row: 82.81 + df.loc[(df.index <= row.name) & (df.index >= 7), "poas_mass_slope"].sum(), axis=1)
        df["mass_correction"] = (4.81668632961298 * np.exp(0.0140674955555049 * df.index)) * vars[0]
        df["poas_mass"] = df.poas_mass + df.poas_mass * df.mass_correction / 100.
        df_o["poas_mass"] = df["poas_mass"]
        df_o.loc[df_o.index == 5, "poas_mass"] = df_o.loc[df_o.index == 5, "expmass"]
        df_o.loc[df_o.index == 6, "poas_mass"] = df_o.loc[df_o.index == 6, "expmass"]
        df_o.loc[df_o.index == 121, "poas_mass"] = 15000
        return df_o


class MassSolver1_gamma(Initialize, object):
    def __init__(self, parent):
        self.__dict__ = parent.__dict__.copy()
        self.common = Common(self)

    def optimize(self):
        self.logger.info("Executing Gamma Mass Solver 1")
        obj = Initialize(self, "mass", 1)
        df, dfplus = obj.df, obj.dfplus
        mc5 = dfplus.loc[dfplus.index == 5, "expmass"].values[0]
        a = max(0.9, (1.22905198035228 * np.log(mc5) - 5.1827424270118))
        sp = [0., 1, a, 70.]
        res = optimize.minimize(self.cost_function, sp, bounds=[[-1.2, 2.], [0.95, 1.1], [0.5, 2.25 * a], [35., 72.1]],
                                method="SLSQP", args=(df, dfplus), options={'maxiter': 500})
        if res.success == False:
            print res
            self.logger.info(
                "Gamma Mass Solver 1 failed to find a feasible solution. Considering start point as final result.")
            res.x = sp
        self.logger.info("Completed Gamma Special Mass Solver 1")
        self.logger.info(
            "Gamma Mass solver final variables - %s, objective - %s" % (",".join([str(x) for x in res.x]), res.fun))
        return res, df, dfplus

    def save(self, res, df, dfplus):
        df, dfplus = self.calculateMass(res.x, df, dfplus)
        df[["mf", "mf_bkp"]] = df[["zi", "mf"]]
        self.logger.info("Trying to smoothen output.")
        write_excel(self.args.debug, self.debugDir, ["mass-solver1_before_smoothing"],
                    ["Individual", "Plus"], [df, dfplus])
        wi = wismooth()
        df, dfplus = wi.smooth(df, dfplus, self.logger)
        df, dfplus = self.calculateFinalMass(df, dfplus)

        write_excel(self.args.debug, self.debugDir, ["mass-solver1"], ["Individual", "Plus"], [df, dfplus])

        # create 16+
        dfplus["zimi"] = dfplus.expmass * dfplus.mf
        df["zimi"] = df.mass * df.mf
        mf = dfplus.loc[dfplus.index == dfplus.index.max(), "mf"].values[0] - df.loc[
            (df.index >= dfplus.index.max()) & (df.index < 16), "mf"].sum()
        mass = (dfplus.loc[dfplus.index == dfplus.index.max(), "zimi"].values[0] - df.loc[
            (df.index >= dfplus.index.max()) & (df.index < 16), "zimi"].sum()) / mf
        dfplus_temp = pd.DataFrame(data=[[mf, mass, mass, "C16"]], columns=["mf", "expmass", "mass", "scn"], index=[16])
        df.drop(df.loc[df.index >= 16].index, inplace=True)
        df["expmass"] = df.mass
        dfplus = pd.concat([dfplus, dfplus_temp], axis=0, sort=True)
        obj = MassSolver5(self)
        res, df, dfplus = obj.optimize(df, dfplus)
        if type(res) != str:
            obj.save(res, df, dfplus)
            set_recommended(self.args.collection, self.logger, self.what)

    def calculateFinalMass(self, df, dfplus):
        df["mass"] = df.apply(lambda row: row.expmass if row.name < dfplus.index.max() else row.mass, axis=1)
        df["mf"] = df.apply(lambda row: row.mf_bkp if row.name < dfplus.index.max() else row.mf, axis=1)
        df["zimi"] = df.mf * df.mass
        dfplus["mass"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "zimi"].sum() / row.mf, axis=1)
        return df, dfplus

    def cost_function(self, vars, df_o, dfplus):
        df, dfplus = self.calculateErrors(vars, df_o, dfplus)
        error = 0
        for index, row in dfplus.iterrows():
            error += self.targetCost(dfplus.loc[dfplus.index == index, "zi_error"].values[0],
                                     0.001) if index >= 6 else 0
            error += self.targetCost(dfplus.loc[dfplus.index == index, "mass_error"].values[0],
                                     0.001) if index >= 6 else 0
        return error

    def targetCost(self, val, target):
        if math.isnan(val): val = 0
        return val / target

    def calculateErrors(self, vars, df_o, dfplus):
        df, dfplus = self.calculateMass(vars, df_o, dfplus)
        dfplus["zi_error"] = ((dfplus.mf - dfplus.zi) / dfplus.mf) ** 2
        dfplus["mass_error"] = ((dfplus.mass - dfplus.expmass) / dfplus.expmass) ** 2
        return df, dfplus

    def calculateMass(self, vars, df_o, dfplus):
        df_o = self.addPOASMass(vars, df_o)
        df = df_o.copy(deep=True)

        df["mb"] = (df.poas_mass + df.poas_mass.shift(-1)) / 2.
        df.loc[df.index == df.index.max(), "mb"] = 10000.

        alpha, eta = vars[2], vars[3]
        beta = (dfplus.loc[dfplus.index == 5, "expmass"].values[0] - eta) / alpha

        df["y"] = (df.mb - eta) / beta
        df["p0"] = df.apply(lambda row: gammainc(alpha, row.y), axis=1)
        df["p1"] = df.apply(lambda row: gammainc(alpha + 1, row.y), axis=1)
        df['p0diff'] = df['p0'].sub(df['p0'].shift())
        df["p0diff"] = df["p0diff"].fillna(df['p0'].iloc[0])
        df['p1diff'] = df['p1'].sub(df['p1'].shift())
        df['p1diff'] = df['p1diff'].fillna(df['p1'].iloc[0])
        df.loc[df.p0diff == 0, "p0diff"] = 1.
        df["mass"] = eta + alpha * beta * (df.p1diff / df.p0diff)

        df["ziinside"] = df.p0 - df.p0.shift(1)
        df.loc[df.index == df.index.min(), 'ziinside'] = df['p0'].iloc[0]
        df["zi"] = df.ziinside * dfplus.loc[dfplus.index == 5, "mf"].values[0]
        df["zimi"] = df.zi * df.mass
        df_o[["zi", "mass", "zimi"]] = df[["zi", "mass", "zimi"]]
        dfplus["zi"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "zi"].sum(), axis=1)
        dfplus["mass"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "zimi"].sum() / row.zi, axis=1)
        return df_o, dfplus

    def addPOASMass(self, vars, df_o):
        df = df_o.copy(deep=True)
        df = pd.DataFrame()
        df["scn"] = ["C%s" % (x) for x in range(5, 122)]
        df.index = [x for x in range(5, 122)]
        df_o = pd.concat([df_o, df.loc[df.index > df_o.index.max()]], axis=0, sort=True)
        df["poas_mass_slope"] = vars[1] * df.apply(lambda row: (
                -0.0000000081143800838 * row.name ** 5 + 2.54535602253662E-06 * row.name ** 4 - 0.000278130020201878 * row.name ** 3 + 0.0132162236519363 * row.name ** 2 - 0.258386996099669 * row.name + 15.4092766381938) if row.name <= 100 else (
                0.509448043249933 * row.name ** 0.768554188962656), axis=1)
        df["poas_mass"] = df.apply(
            lambda row: 82.81 + df.loc[(df.index <= row.name) & (df.index >= 7), "poas_mass_slope"].sum(), axis=1)
        df["mass_correction"] = (4.81668632961298 * np.exp(0.0140674955555049 * df.index)) * vars[0]
        df["poas_mass"] = df.poas_mass + df.poas_mass * df.mass_correction / 100.
        df_o["poas_mass"] = df["poas_mass"]
        df_o.loc[df_o.index == 5, "poas_mass"] = df_o.loc[df_o.index == 5, "expmass"]
        df_o.loc[df_o.index == 6, "poas_mass"] = df_o.loc[df_o.index == 6, "expmass"]
        return df_o


class MassSolver1_beta(Initialize, object):
    def __init__(self, parent):
        self.__dict__ = parent.__dict__.copy()
        self.common = Common(self)

    def optimize(self):
        self.logger.info("Executing Beta Mass Solver 1")
        obj = Initialize(self, "mass", 1)
        df, dfplus = obj.df, obj.dfplus
        mc5 = dfplus.loc[dfplus.index == 5, "expmass"].values[0]
        a = max(0.9, (1.22905198035228 * np.log(mc5) - 5.1827424270118))
        sp = [0., 1, a, 70.]
        res = optimize.minimize(self.cost_function, sp, bounds=[[-1.2, 2.], [0.95, 1.1], [0.5, 2.25 * a], [35., 72.1]],
                                method="SLSQP", args=(df, dfplus), options={'maxiter': 500})
        if res.success == False:
            print res
            self.logger.info(
                "Beta Mass Solver 1 failed to find a feasible solution. Considering start point as final result.")
            res.x = sp
        self.logger.info("Completed Beta Special Mass Solver 1")
        self.logger.info(
            "Beta Mass solver final variables - %s, objective - %s" % (",".join([str(x) for x in res.x]), res.fun))
        return res, df, dfplus

    def save(self, res, df, dfplus):
        df, dfplus = self.calculateMass(res.x, df, dfplus)
        df[["mf", "mf_bkp"]] = df[["zi", "mf"]]
        self.logger.info("Trying to smoothen output.")
        write_excel(self.args.debug, self.debugDir, ["mass-solver1_before_smoothing"],
                    ["Individual", "Plus"], [df, dfplus])
        wi = wismooth()
        df, dfplus = wi.smooth(df, dfplus, self.logger)
        df, dfplus = self.calculateFinalMass(df, dfplus)
        write_excel(self.args.debug, self.debugDir, ["mass-solver1"], ["Individual", "Plus"], [df, dfplus])

        # create 16+
        dfplus["zimi"] = dfplus.expmass * dfplus.mf
        df["zimi"] = df.mass * df.mf
        mf = dfplus.loc[dfplus.index == dfplus.index.max(), "mf"].values[0] - df.loc[
            (df.index >= dfplus.index.max()) & (df.index < 16), "mf"].sum()
        mass = (dfplus.loc[dfplus.index == dfplus.index.max(), "zimi"].values[0] - df.loc[
            (df.index >= dfplus.index.max()) & (df.index < 16), "zimi"].sum()) / mf
        dfplus_temp = pd.DataFrame(data=[[mf, mass, mass, "C16"]], columns=["mf", "expmass", "mass", "scn"], index=[16])
        df.drop(df.loc[df.index >= 16].index, inplace=True)
        df["expmass"] = df.mass
        dfplus = pd.concat([dfplus, dfplus_temp], axis=0, sort=True)
        obj = MassSolver5(self)
        res, df, dfplus = obj.optimize(df, dfplus)
        if type(res) != str:
            obj.save(res, df, dfplus)
            set_recommended(self.args.collection, self.logger, self.what)

    def calculateFinalMass(self, df, dfplus):
        df["mass"] = df.apply(lambda row: row.expmass if row.name < dfplus.index.max() else row.mass, axis=1)
        df["mf"] = df.apply(lambda row: row.mf_bkp if row.name < dfplus.index.max() else row.mf, axis=1)
        df["zimi"] = df.mf * df.mass
        dfplus["mass"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "zimi"].sum() / row.mf, axis=1)
        return df, dfplus

    def cost_function(self, vars, df_o, dfplus):
        df, dfplus = self.calculateErrors(vars, df_o, dfplus)
        error = 0
        for index, row in dfplus.iterrows():
            error += self.targetCost(dfplus.loc[dfplus.index == index, "zi_error"].values[0],
                                     0.001) if index >= 6 else 0
            error += self.targetCost(dfplus.loc[dfplus.index == index, "mass_error"].values[0],
                                     0.001) if index >= 6 else 0
        return error

    def targetCost(self, val, target):
        if math.isnan(val): val = 0
        return val / target

    def calculateErrors(self, vars, df_o, dfplus):
        df, dfplus = self.calculateMass(vars, df_o, dfplus)
        dfplus["zi_error"] = ((dfplus.mf - dfplus.zi) / dfplus.mf) ** 2
        dfplus["mass_error"] = ((dfplus.mass - dfplus.expmass) / dfplus.expmass) ** 2
        return df, dfplus

    def calculateMass(self, vars, df_o, dfplus):
        df_o = self.addPOASMass(vars, df_o)
        df = df_o.copy(deep=True)

        df["mb"] = (df.poas_mass + df.poas_mass.shift(-1)) / 2.
        df.loc[df.index == df.index.max(), "mb"] = 10000.

        alpha, eta = vars[2], vars[3]
        b_max = 15000.
        beta = alpha * (b_max - dfplus.loc[dfplus.index == 5, "expmass"].values[0]) / (
                dfplus.loc[dfplus.index == 5, "expmass"].values[0] - eta)

        df["y"] = (df.mb - eta) / (b_max - eta)
        df.loc[df.y > 1., "y"] = 0.9999999999
        df["p0"] = df.apply(lambda row: betaInbuilt.cdf(row.y, alpha, beta), axis=1)
        df.loc[df.p0 > 1., "p0"] = 0.9999999999
        df["p1"] = df.apply(lambda row: betaInbuilt.cdf(row.y, alpha + 1, beta), axis=1)
        df.loc[df.p1 > 1., "p1"] = 0.9999999999
        df['p0diff'] = df['p0'].sub(df['p0'].shift())
        df['p0diff'] = df['p0diff'].fillna(df['p0'].iloc[0])
        df['p1diff'] = df['p1'].sub(df['p1'].shift())
        df['p1diff'] = df['p1diff'].fillna(df['p1'].iloc[0])
        df.loc[df.p0diff == 0, "p0diff"] = 1.
        df["mass"] = eta + (alpha / (alpha + beta)) * (b_max - eta) * (df.p1diff.values) / (df.p0diff.values)

        df["ziinside"] = df.p0 - df.p0.shift(1)
        df.loc[df.index == df.index.min(), 'ziinside'] = df['p0'].iloc[0]
        df["zi"] = df.ziinside * dfplus.loc[dfplus.index == 5, "mf"].values[0]
        df["zimi"] = df.zi * df.mass
        df_o[["zi", "mass", "zimi"]] = df[["zi", "mass", "zimi"]]
        dfplus["zi"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "zi"].sum(), axis=1)
        dfplus["mass"] = dfplus.apply(lambda row: df.loc[df.index >= row.name, "zimi"].sum() / row.zi, axis=1)
        return df_o, dfplus

    def addPOASMass(self, vars, df_o):
        df = df_o.copy(deep=True)
        df = pd.DataFrame()
        df["scn"] = ["C%s" % (x) for x in range(5, 122)]
        df.index = [x for x in range(5, 122)]
        df_o = pd.concat([df_o, df.loc[df.index > df_o.index.max()]], axis=0, sort=True)
        df["poas_mass_slope"] = vars[1] * df.apply(lambda row: (
                -0.0000000081143800838 * row.name ** 5 + 2.54535602253662E-06 * row.name ** 4 - 0.000278130020201878 * row.name ** 3 + 0.0132162236519363 * row.name ** 2 - 0.258386996099669 * row.name + 15.4092766381938) if row.name <= 100 else (
                0.509448043249933 * row.name ** 0.768554188962656), axis=1)
        df["poas_mass"] = df.apply(
            lambda row: 82.81 + df.loc[(df.index <= row.name) & (df.index >= 7), "poas_mass_slope"].sum(), axis=1)
        df["mass_correction"] = (4.81668632961298 * np.exp(0.0140674955555049 * df.index)) * vars[0]
        df["poas_mass"] = df.poas_mass + df.poas_mass * df.mass_correction / 100.
        df_o["poas_mass"] = df["poas_mass"]
        df_o.loc[df_o.index == 5, "poas_mass"] = df_o.loc[df_o.index == 5, "expmass"]
        df_o.loc[df_o.index == 6, "poas_mass"] = df_o.loc[df_o.index == 6, "expmass"]
        return df_o


class DensitySolver1(Initialize, object):
    def __init__(self, parent):
        self.__dict__ = parent.__dict__.copy()

    def getPoasDensity(self, df, dfplus):
        poas = POASDensity(self.logger, self.args.debug, self.debugDir, self.args.collection, self.what)
        poas.run(df, dfplus)
        df_p, dfplus_p = poas_recommended_density(self.args.collection, self.logger)
        df["poas_density"] = df_p.density
        dfplus["poas_density"] = dfplus_p.density
        return df, dfplus

    def optimize(self):
        obj = Initialize(self, "density", 1)
        df, dfplus = obj.df, obj.dfplus
        df, dfplus = self.getPoasDensity(df, dfplus)
        self.logger.info("Executing General Density Solver 1")
        d6 = df.loc[df.index == 6, "expdensity"].values[0]
        res = optimize.minimize(self.cost_function, [2.5, 0.55],
                                bounds=[[.5, 10.], [.1, .6634 if np.isnan(d6) else d6 * (1. - 0.1 / 100)]],
                                method="SLSQP", args=(df, dfplus), options={'maxiter': 500})
        if res.success == False:
            print res
            print 'Error:{"Type": "Density Solver", "Message": "Density Solver failed to find a feasible solution. Please check input data. If issue persists, please raise with POAS support team."}'
            return "Failed", df, dfplus
        self.logger.info("Completed General Density Solver 1")
        self.logger.info("General Density solver final variables - %s" % (",".join([str(x) for x in res.x])))
        return res, df, dfplus

    def save(self, res, df, dfplus):
        df, dfplus = self.calculateDensities(res.x, df, dfplus)
        write_excel(self.args.debug, self.debugDir, ["density-solver1"], ["Individual", "Plus"],
                    [df, dfplus])
        write(df, "MBP", self.args.collection, logger=self.logger, Property="MCOutput", Identifier=self.what,
              PlusOrIndividual="Individual", Solver=1, Recommended=0, Distribution="General")
        write(dfplus, "MBP", self.args.collection, logger=self.logger, Property="MCOutput", Identifier=self.what,
              PlusOrIndividual="Plus", Solver=1, Recommended=0, Distribution="General")

    def cost_function(self, vars, df_o, dfplus_o):
        df, dfplus = self.calculateDensities(vars, df_o, dfplus_o)
        error = 0
        for index, row in dfplus.iterrows():
            error += self.targetCost(row.error, 0.001)
        error += self.targetCost(df.error.sum(), 1.)
        lid = df.loc[~df.expdensity.isnull()].index.max()
        if not np.isnan(lid):
            error += self.targetCost(df.exp_error.sum(), 0.05)
            error += self.targetCost(df.loc[df.index >= lid - 2, "exp_error"].sum(), 0.001)
        return error

    def targetCost(self, val, target):
        return val / target

    def calculateDensities(self, vars, df_o, dfplus_o):
        b, sg0 = vars
        df, dfplus = df_o.copy(deep=True), dfplus_o.copy(deep=True)
        a = self.calculateA(vars, df, dfplus)
        df["xwcumm"] = df.xw.cumsum()
        df["xwcumm_1"] = df.apply(lambda row: 10 ** -20 if 1. - row.xwcumm == 0 else 1. - row.xwcumm, axis=1)
        df["qi"] = -1. * np.log(1. - df.xwcumm)
        df["xwi_inside"] = df.xwcumm_1.shift(1) - df.xwcumm_1
        df.loc[df.index == df.index.min(), "xwi_inside"] = 1. - df.loc[df.index == df.index.min(), "xwcumm_1"].values[0]
        df["incg"] = df.apply(lambda row: gammainc(1 + 1 / b, row.qi) * gamma(1 + 1 / b), axis=1)
        df['incgdiff'] = df['incg'].sub(df['incg'].shift())
        df.incgdiff = df.incgdiff.fillna(df['incg'].iloc[0])
        df["pav"] = (1 / df.xwi_inside.values) * (a / b) ** (1 / b) * df.incgdiff.values
        df["pav"] = df.apply(lambda row: 0 if row.xwi_inside == 0 else row.pav, axis=1)
        df["density"] = df.apply(lambda row: 0 if row.pav == 0 else vars[1] * (1. + row.pav), axis=1)
        df.loc[df.density <= vars[1], "density"] = df.poas_density
        df["exp_error"] = np.abs(df.density - df.expdensity) / df.expdensity
        df["diff"] = df.expdensity - df.density
        df["error"] = df.apply(
            lambda row: 0 if row.name <= 6 else np.abs(row.poas_density - row.density) / row.poas_density, axis=1)
        df["density"] = df.apply(lambda row: row.expdensity if not np.isnan(row.expdensity) else row.density, axis=1)

        # poonch fix
        df["slope"] = (np.log(df.density) - np.log(df.density.shift(1))) / (np.log(df.index) - np.log(df.index - 1))
        df["slope"] = df.apply(lambda row: 10 ** -20 if row.slope == 0 or np.isnan(row.slope) else row.slope, axis=1)
        avg_slope = np.mean(df["slope"].tail(12).head(2))
        fs = df.tail(12).head(1).index.values[0]
        fd = df["density"].tail(12).head(1).values[0]
        df.loc[df.index > fs, "density"] = np.exp(
            np.log(fd) + avg_slope * (np.log(df.loc[df.index > fs].index) - np.log(fs)))
        df.loc[df.index > fs, "density"] = df.loc[df.index > fs].apply(
            lambda row: 0 if np.isnan(row.density) else row.density, axis=1)
        mr = df.loc[df.index == df.index.max(), "mass"].values[0] / \
             df.loc[df.index == df.index.max() - 1, "mass"].values[0]
        dr = 1.03 if mr <= 1.03 else (1.1 if mr > 1.1 else mr)
        df.loc[df.index == df.index.max(), "density"] = df.loc[df.index == df.index.max() - 1, "density"].values[0] * dr
        lid = df.loc[~df.expdensity.isnull()].index.max()
        if not np.isnan(lid) and lid >= 9:
            avg_diff = np.mean(df.loc[(df.index >= lid - 1) & (df.index <= lid), "diff"].tolist())
            df.loc[df.index > lid, "density"] = df.loc[df.index > lid].apply(
                lambda row: 0 if row.density == 0 else row.density + avg_diff, axis=1)
        df["zimi"] = df.mass * df.mf
        df["zimibysgi"] = df.apply(lambda row: 0 if row.density == 0 else row.zimi / row.density, axis=1)
        dfplus["density"] = dfplus.apply(
            lambda row: df.loc[df.index >= row.name, "zimi"].sum() / df.loc[df.index >= row.name, "zimibysgi"].sum(),
            axis=1)
        dfplus["error"] = np.abs(dfplus.poas_density - dfplus.density) / dfplus.poas_density
        return df, dfplus

    def calculateA(self, vars, df, dfplus):
        avgDensity = (df.poas_density * df.xw).sum()
        pstar_avg = (avgDensity - vars[1]) / vars[1]
        A = vars[0] * (pstar_avg / (gamma(1. + 1. / vars[0]))) ** vars[0]
        return A

    def calculateC6Density(self, df_o, dfplus):
        df = df_o.copy(deep=True)
        df["zimi"] = df.mass * df.mf
        dfplus["zimi"] = dfplus.mass * dfplus.mf
        df["expdensity"] = df.apply(lambda row: row.poas_density if np.isnan(
            row.expdensity) and row.name < dfplus.index.max() else row.expdensity, axis=1)
        df["zimibysgi"] = df.mass * df.mf / df.expdensity
        dfplus["zimibysgi"] = dfplus.mass * dfplus.mf / dfplus.poas_density
        return (df.loc[(df.index >= 6) & (df.index < dfplus.index.max()), "zimi"].sum() +
                dfplus.loc[dfplus.index == dfplus.index.max(), "zimi"].values[0]) \
               / (df.loc[(df.index >= 6) & (df.index < dfplus.index.max()), "zimibysgi"].sum() +
                  dfplus.loc[dfplus.index == dfplus.index.max(), "zimibysgi"].values[0])


class BpSolver1(Initialize, object):
    def __init__(self, parent):
        self.__dict__ = parent.__dict__.copy()
        self.common = Common(self)

    def optimize(self):
        obj = Initialize(self, "bp", 1)
        df = obj.df
        start_points, bounds = self.bounds()
        res = self.solver1(start_points, bounds, df)
        if res["success"] == False:
            print 'Error:{"Type": "BP Solver", "Message": "BP Solver failed to find a feasible solution. Please check input data. If issue persists, please raise with POAS support team."}'
            quit()
        return res, df, None

    def save(self, res, df, dfplus):
        a, b, eta = res["Values"]
        df = self.common.calcBp(res["Values"], df)
        df.loc[df.index <= 5, "bp"] = df.loc[df.index <= 5, "expbp"]
        write_excel(self.args.debug, self.debugDir, ["bp-solver1"], ["Individual"], [df])
        write(df, "MBP", self.args.collection, logger=self.logger, Property="MCOutput", Identifier=self.what,
              PlusOrIndividual="Individual", Solver=1, Recommended=0, Distribution="General")

    def solver1(self, start_points, bounds, df, printname=None):
        self.logger.info("Executing General %s Solver 1" % ("BP" if printname is None else printname))
        res = GenericSolver(start_points, bounds, self.cost_function, debug=self.args.debug, args=(df.copy(deep=True)))
        self.logger.info("Result -- %s" % (res))
        self.logger.info("Completed General %s Solver 1" % ("BP" if printname is None else printname))
        return res

    def bounds(self):
        self.Bounds = dict()
        A, B, eta = 15.0, 15.0, 200.
        self.Bounds["A"] = [0.5, 30.]
        self.Bounds["B"] = [0.5, 30.]
        self.Bounds["Eta"] = [100., 300.]
        initialEtaFn = (eta - self.Bounds["Eta"][0]) / (self.Bounds["Eta"][1] - self.Bounds["Eta"][0])
        initialAFn = (A - self.Bounds["A"][0]) / (self.Bounds["A"][1] - self.Bounds["A"][0])
        initialBFn = (B - self.Bounds["B"][0]) / (self.Bounds["B"][1] - self.Bounds["B"][0])
        return ([initialAFn, initialBFn, initialEtaFn],
                [[self.Bounds["A"][0], self.Bounds["A"][1]], [self.Bounds["B"][0], self.Bounds["B"][1]],
                 [self.Bounds["Eta"][0], self.Bounds["Eta"][1]]])

    def cost_function(self, vars, args):
        a, b, eta = vars
        df = self.common.calcBp(vars, args)
        df.drop(df.loc[df.mass > 300].index, inplace=True)
        df["diffincg"] = df.apply(lambda row: self.common.Differentiate(b, row.qi, row.incg), axis=1)
        df["diffincgdiff"] = df['diffincg'].sub(df['diffincg'].shift())
        df.diffincgdiff = df.diffincgdiff.fillna(df['diffincg'].iloc[0])
        df["diffbpwrta"] = df.pav.values * eta / (a * b)
        df["diffbpwrteta"] = 1 + df.pav.values
        df["diffbpwrtb"] = (1 / df.xvi.values) * (a / b) ** (1 / b) * (
                df.incgdiff.values * (-(log(a / b) + 1) / b ** 2) + df.diffincgdiff.values) * eta
        df.loc[df.xvi == 0, "diffbpwrtb"] = 0.
        df["differrorwrta"] = df.diffbpwrta.values * 2 * ((df.bp.values - df.expbp.values) / df.expbp.values ** 2)
        df["differrorwrtb"] = df.diffbpwrtb.values * 2 * ((df.bp.values - df.expbp.values) / df.expbp.values ** 2)
        df["differrorwrteta"] = df.diffbpwrteta.values * 2 * ((df.bp.values - df.expbp.values) / df.expbp.values ** 2)
        df["derivativea"] = df.differrorwrta.values * df.error.values
        df["derivativeb"] = df.differrorwrtb.values * df.error.values
        df["derivativeeta"] = df.differrorwrteta.values * df.error.values
        avg = df.expbp.mean()
        df["r21"] = (df.bp.values - df.expbp.values) ** 2
        df["r22"] = (df.bp.values - avg) ** 2
        r2 = 1 - df.r21.sum() / df.r22.sum()
        df.drop(["r21", "r22"], axis=1, inplace=True)
        return df["error"].sum(), [df["derivativea"].sum(), df["derivativeb"].sum(), df["derivativeeta"].sum()], r2


class RiSolver1(Initialize, object):
    def __init__(self, parent):
        self.__dict__ = parent.__dict__.copy()
        self.common = Common(self)

    def optimize(self):
        obj = Initialize(self, "ri", 1)
        df = obj.df
        start_points, bounds = self.bounds()
        obj = BpSolver1(self)
        res = obj.solver1(start_points, bounds, df, printname="RI")
        if res["success"] == False:
            print 'Error:{"Type": "RI Solver", "Message": "RI Solver failed to find a feasible solution. Please check input data. If issue persists, please raise with POAS support team."}'
            quit()
        return res, df, None

    def save(self, res, df, dfplus):
        a, b, eta = res["Values"]
        df = self.common.calcBp(res["Values"], df)
        df.loc[df.index <= 5, "bp"] = df.loc[df.index <= 5, "expbp"]
        df.rename(columns={'expbp': 'expri', 'bp': 'ri'}, inplace=True)
        write_excel(self.args.debug, self.debugDir, ["ri-solver1"], ["Individual"], [df])
        write(df, "MBP", self.args.collection, logger=self.logger, Property="MCOutput", Identifier=self.what,
              PlusOrIndividual="Individual", Solver=1, Recommended=0, Distribution="General")

    def bounds(self):
        self.Bounds = dict()
        A, B, eta = 0.9, 2.5, 0.18
        self.Bounds["A"] = [0.2, 5.]
        self.Bounds["B"] = [0.2, 5.]
        self.Bounds["Eta"] = [0., 0.5]
        initialEtaFn = (eta - self.Bounds["Eta"][0]) / (self.Bounds["Eta"][1] - self.Bounds["Eta"][0])
        initialAFn = (A - self.Bounds["A"][0]) / (self.Bounds["A"][1] - self.Bounds["A"][0])
        initialBFn = (B - self.Bounds["B"][0]) / (self.Bounds["B"][1] - self.Bounds["B"][0])
        return ([initialAFn, initialBFn, initialEtaFn],
                [[self.Bounds["A"][0], self.Bounds["A"][1]], [self.Bounds["B"][0], self.Bounds["B"][1]],
                 [self.Bounds["Eta"][0], self.Bounds["Eta"][1]]])
