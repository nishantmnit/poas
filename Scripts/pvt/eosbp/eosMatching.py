import pandas as pd

from Scripts.pvt.core.optimizers.ncg import *
from Scripts.pvt.core.storage.excel import write_excel
from Scripts.pvt.core.storage.mongo import write
from Scripts.pvt.core.storage.fetch_most import cm_data, eosbp_data
from Scripts.pvt.core.optimizers.poas_interpolator import monotone


gasConstant = 8.314472
pressure = 0.101325


class eosMatching(object):
    def __init__(self, parent):
        self.__dict__ = parent.__dict__.copy()  

    def runEOSBp(self):
        if self.args.eos == "all":
            self.logger.info("Executing all EOS %s" %(", ".join(eos for eos in self.validEOS)))
            for eos in self.validEOS:
                self.runEOS(eos)
        else:
            self.logger.info("Executing EOS = %s" %(self.args.eos))
            self.runEOS(self.args.eos)  

    def runEOS(self, e):
        self.optimize(e)
        write_excel(self.args.debug, self.debugDir, ["eos", e], ["observed", "calculated"], [self.ov, self.cv])
        write(self.cv, "MBP", self.args.collection, logger=self.logger, Property="MCOutput", Identifier="EOS", Eos=e)

    def optimize(self, eos):
        self._setNVar(eos)
        ov = ObservedValues(self, eos)
        for index, row in ov.df.iterrows():
            self.logger.info("well = %s | EOS = %s | EOSBp tuning for scn = %s | component = %s" %(self.args.collection, eos, row.name, row.component))
            self.ncg = ncg()
            vars = self.ncg.minimize(self._sp(row, eos), self._range(row, eos), self._cost_for_ncg, self.logger, max_iter=200, debug=self.args.debug, converge_f=self._converged, args=(row, eos))
            for i in range(len(vars)):
                ov.df.loc[ov.df.seq == row.seq, self._nvar[i]] = vars[i]
        cv = CalculatedValues(self, ov.df, eos, normalize=True)
        self.ov = ov.df
        self.cv = cv.df

    def _setNVar(self, eos):
        if eos in ["poas_4", "generic_poas_a", "pr", "srk"]:
            self._nvar = ["alpha_multiplier", "om_b_multiplier"]
        elif eos in ["pt"]:
            self._nvar = ["alpha_multiplier", "zc_multiplier"]
        elif eos in ["sw_poas", "als"]:
            self._nvar = ["alpha_multiplier", "omega_multiplier"]
        elif eos in ["poas_4a"]:
            self._nvar = ["alpha_multiplier", "beta_multiplier"]

    def _cost_for_ncg(self, vars, args):
        row, eos = args[0], args[1]
        cv, cost = self._cost(row, vars, eos)
        gd = Gradients(self)
        grad = gd.gradient(row, cv, eos)
        return cost, grad

    def _cost(self, row, vars, eos):
        for i in range(len(vars)): row[self._nvar[i]] = vars[i]
        calc = CalculatedValues(self, row, eos)
        cv = calc.row
        objs = ["hv_bp", "vb"]
        self._scales(eos)
        costs = pd.Series()
        for i in range(len(objs)):
            costs[objs[i]] = 0 if row[objs[i]] == 0 else self.scales[i]*((row[objs[i]] - cv[objs[i]])/row[objs[i]])**2
        return cv, costs.sum() + self.scales[2] * max(0, cv.ph_diff - 10**-7)**2

    def _scales(self, eos):
        if eos == "poas_4":
            self.scales = [0.01, 1., 500.]
        elif eos == "pt":
            self.scales = [0.14, 1., 150.]
        elif eos in ["sw_poas"]:
            self.scales = [0.14, 25., 200.]
        elif eos in ["als"]:
            self.scales = [1., 10., 200.]
        elif eos in ["generic_poas_a"]:
            self.scales = [0.14, 1., 300.]
        elif eos in ["poas_4a"]:
            self.scales = [1., 1.5, 200.]
        elif eos in ["srk"]:
            self.scales = [1., 1., 275.]
        elif eos in ["pr"]:
            self.scales = [1., 15., 250.]

    def _range(self, row, eos):
        if eos in ["sw_poas"]:
            bound = [[1.000001/row.alpha_pr, 1.25], [0.01, 10.]]
        elif eos in ["als"]:
            bound = [[1.000001/row.alpha_pr, 1.25], [0.2, 5.]]
        elif eos in ["generic_poas_a"]:
            bound = [[1.000001/row.alpha_pr, 1.25], [0.01, 20.]]
        elif eos in ["poas_4a"]:
            bound = [[1.000001/row.alpha_pr, 1.25], [0.5, 1.7]]
        elif eos in ["srk"]:
            bound = [[1.000001/row.alpha_pr, 2.5], [0.1, 2.]]
        elif eos in ["pr"]:
            bound = [[1.000001/row.alpha_pr, 2.5], [0.1, 1.5]]
        elif eos in ["poas_4", "pt"]:
            bound = [[1.000001/row.alpha_pr, 1.25], [0.01, 4.]]
        else:
            bound = [[0.2, 3.], [0.01, 4.]]
        return bound

    def _converged(self, debug, logger, iteration, grad):
        row, eos = self.ncg.curr_args[0], self.ncg.curr_args[1]
        cv, cost = self._cost(row, self.ncg.curr_vars, eos)
        cd = cv.ph_diff
        if iteration < 3: 
            return False
        if cd <= 10**-4 and iteration <= 50: 
            self.logger.info("Converged by rule 1 - %s" %(cd))
            return True
        elif cd <= 10**-3 and iteration > 50 and iteration <= 75: 
            self.logger.info("Converged by rule 2 - %s" %(cd))
            return True
        elif cd <= 10**-2 and iteration > 75 and iteration <= 100:
            self.logger.info("Converged by rule 3 - %s" %(cd))
            return True
        elif cd <= 10**-1 and iteration > 100: 
            self.logger.info("Converged by rule 4 - %s" %(cd))
            return True
        if iteration > 5 and np.all(grad - self.ncg.pgrad == 0): 
            self.logger.info("Converged by rule 5 - %s" %(cd))
            return True
        return False

    def _sp(self, row, eos):
        vars = list()
        if eos in ["poas_4"]:
            vars = [(1.000001/row.alpha_pr*1.1) if (-0.0996900019304187*np.log(row.name) + 1.18306731835084)<1.000001/row.alpha_pr*1.1 else (-0.0996900019304187*np.log(row.name) + 1.18306731835084), 0.0305405900409994*np.log(row.name) + 1.14767398454079]            
        elif eos in ["generic_poas_a"]:
            vars = [(1.000001/row.alpha_pr*1.1) if (-0.0996900019304187*np.log(row.name) + 1.18306731835084)<1.000001/row.alpha_pr*1.1 else (-0.0996900019304187*np.log(row.name) + 1.18306731835084), 0.0305405900409994*np.log(row.name) + 1.14767398454079]
        elif eos == "pt":
            vars = [(1.000001/row.alpha_pr*1.1) if (-0.0996900019304187*np.log(row.name) + 1.18306731835084)*1.1<1.000001/row.alpha_pr*1.1 else (-0.0996900019304187*np.log(row.name) + 1.18306731835084)*1.1, 0.0483600098446223*np.log(row.name) + 0.975636286416085]
        elif eos in ["sw_poas"]:
            vars = [(1.000001/row.alpha_pr*1.05) if (-0.0996900019304187*np.log(row.name) + 1.18306731835084)*1.1<1.000001/row.alpha_pr*1.05 else (-0.0996900019304187*np.log(row.name) + 1.18306731835084)*1.1, 1.06134115451027*np.exp(0.000488020527107394*row.name)]            
        elif eos in ["als"]:
            vars = [(1.000001/row.alpha_pr*1.1) if (-0.0996900019304187*np.log(row.name) + 1.18306731835084)<1.000001/row.alpha_pr*1.1 else (-0.0996900019304187*np.log(row.name) + 1.18306731835084), 1.14437586198642*np.exp(0.000613668469982046*row.name)*1.25]
        elif eos in ["poas_4a"]:
            vars = [(1.000001/row.alpha_pr*1.1) if (-0.0996900019304187*np.log(row.name) + 1.18306731835084)<1.000001/row.alpha_pr*1.1 else (-0.0996900019304187*np.log(row.name) + 1.18306731835084), 1.02676600315626*np.exp(0.000572446998568102*row.name)*0.96]
        elif eos in ["pr"]:
            vars = [(1.000001/row.alpha_pr*1.1) if (0.053332778363657*np.log(row.name) + 1.0903237635827) < 1.000001/row.alpha_pr*1.1 else (0.053332778363657*np.log(row.name) + 1.0903237635827), (0.994224780507426*np.exp(-0.00304016543633199*row.name))]
        elif eos in ["srk"]:
            vars = [(1.000001/row.alpha_pr*1.1) if (0.053332778363657*np.log(row.name) + 1.0903237635827)*1.05 < 1.000001/row.alpha_pr*1.1 else (0.053332778363657*np.log(row.name) + 1.0903237635827)*1.05, (0.994224780507426*np.exp(-0.00304016543633199*row.name))]
        vars = [round(x, 8) for x in vars]
        return vars


class Gradients(object):
    def __init__(self, parent):
        self.__dict__ = parent.__dict__.copy()

    def gradient(self, ov, cv, eos):
        return getattr(self, eos)(ov, cv)

    def srk(self, ov, cv):
        diff_alpha_wrt1 = ov.alpha_pr
        diff_alpha_wrt2 = 0
        
        diff_diff_alpha_wrt1 = ov.alpha_pr*(((2*ov.alpha_m-2)/(ov.bp_final1/ov.tc_k)+ov.alpha_l*(-2*ov.alpha_m)*(ov.bp_final1/ov.tc_k)**(2*ov.alpha_m-1))/ov.tc_k)
        diff_diff_alpha_wrt2 = 0

        diff_om_b_wrt1 = 0
        diff_om_b_wrt2 = 0.0866403499649577

        diff_om_a_wrt1 = 0
        diff_om_a_wrt2 = 0

        diff_b_wrt1 = diff_om_b_wrt1*gasConstant*ov.tc_k/(ov.pc_mpa*10**6)
        diff_b_wrt2 = diff_om_b_wrt2*gasConstant*ov.tc_k/(ov.pc_mpa*10**6)

        diff_pf_wrt1 = 0
        diff_pf_wrt2 = diff_b_wrt2 * 10**6/ov.vc_final

        diff_ac_wrt1 = 0
        diff_ac_wrt2 = diff_om_a_wrt2*gasConstant**2*ov.tc_k**2/(ov.pc_mpa*10**6)

        diff_ac_alpha_wrt1 = (cv.ac*diff_alpha_wrt1+diff_ac_wrt1*cv.alpha_eos)
        diff_ac_alpha_wrt2 = (cv.ac*diff_alpha_wrt2+diff_ac_wrt2*cv.alpha_eos)

        diff_u_wrt1 = 0 if cv.pf == 0 else ((-1. * diff_pf_wrt1)/cv.pf**2 * (1./ov.zc_final - 3.))
        diff_u_wrt2 = 0 if cv.pf == 0 else ((-1. * diff_pf_wrt2)/cv.pf**2 * (1./ov.zc_final - 3.))

        diff_w_wrt1 = 0 if ((cv.u-1)*cv.pf**3+3*cv.pf**2) == 0 else (((-3*diff_pf_wrt1-(2*diff_u_wrt1*cv.u*cv.pf**3+cv.u**2*3*diff_pf_wrt1*cv.pf**2)-3*(diff_u_wrt1*cv.pf**2+cv.u*2*diff_pf_wrt1*cv.pf))*((cv.u-1)*cv.pf**3+3*cv.pf**2)-(1-3*cv.pf-cv.u**2*cv.pf**3-3*cv.u*cv.pf**2)*((diff_u_wrt1*cv.pf**3+(cv.u-1)*3*diff_pf_wrt1*cv.pf**2)+3*2*diff_pf_wrt1*cv.pf))/((cv.u-1)*cv.pf**3+3*cv.pf**2)**2)
        diff_w_wrt2 = 0 if ((cv.u-1)*cv.pf**3+3*cv.pf**2) == 0 else (((-3*diff_pf_wrt2-(2*diff_u_wrt2*cv.u*cv.pf**3+cv.u**2*3*diff_pf_wrt2*cv.pf**2)-3*(diff_u_wrt2*cv.pf**2+cv.u*2*diff_pf_wrt2*cv.pf))*((cv.u-1)*cv.pf**3+3*cv.pf**2)-(1-3*cv.pf-cv.u**2*cv.pf**3-3*cv.u*cv.pf**2)*((diff_u_wrt2*cv.pf**3+(cv.u-1)*3*diff_pf_wrt2*cv.pf**2)+3*2*diff_pf_wrt2*cv.pf))/((cv.u-1)*cv.pf**3+3*cv.pf**2)**2)

        diff_r2_wrt1 = 0
        diff_r2_wrt2 = 0 if (cv.u**2-4*cv.w) <= 0 else (diff_u_wrt2+0.5*(2*cv.u*diff_u_wrt2-4*diff_w_wrt2)*(cv.u**2-4*cv.w)**-0.5)/2

        diff_r1_wrt1 = 0
        diff_r1_wrt2 = 0 if (cv.u**2-4*cv.w) <= 0 else (diff_u_wrt2-0.5*(2*cv.u*diff_u_wrt2-4*diff_w_wrt2)*(cv.u**2-4*cv.w)**-0.5)/2              

        diff_A_wrt1 = diff_ac_alpha_wrt1*pressure*10**6/(gasConstant**2*ov.bp_final1**2)
        diff_A_wrt2 = diff_ac_alpha_wrt2*pressure*10**6/(gasConstant**2*ov.bp_final1**2)

        diff_B_wrt1 = 0
        diff_B_wrt2 = diff_b_wrt2*pressure*10**6/(gasConstant*ov.bp_final1)   

        diff_a1_wrt1 = 0
        diff_a1_wrt2 = ((cv.u*diff_B_wrt2+diff_u_wrt2*cv.B)-diff_B_wrt2) 

        diff_a2_wrt1 = diff_A_wrt1+(cv.w*2*cv.B*diff_B_wrt1+diff_w_wrt1*cv.B**2)-(cv.u*diff_B_wrt1+diff_u_wrt1*cv.B)-(cv.u*2*cv.B*diff_B_wrt1+diff_u_wrt1*cv.B**2)
        diff_a2_wrt2 = diff_A_wrt2+(cv.w*2*cv.B*diff_B_wrt2+diff_w_wrt2*cv.B**2)-(cv.u*diff_B_wrt2+diff_u_wrt2*cv.B)-(cv.u*2*cv.B*diff_B_wrt2+diff_u_wrt2*cv.B**2)

        diff_a3_wrt1 = -1*((cv.A*diff_B_wrt1+diff_A_wrt1*cv.B)+(cv.w*2*cv.B*diff_B_wrt1+diff_w_wrt1*cv.B**2)+(cv.w*3*cv.B**2*diff_B_wrt1+diff_w_wrt1*cv.B**3))
        diff_a3_wrt2 = -1*((cv.A*diff_B_wrt2+diff_A_wrt2*cv.B)+(cv.w*2*cv.B*diff_B_wrt2+diff_w_wrt2*cv.B**2)+(cv.w*3*cv.B**2*diff_B_wrt2+diff_w_wrt2*cv.B**3))

        diff_Q_wrt1 = (3*diff_a2_wrt1-2*cv.a1*diff_a1_wrt1)/9.
        diff_Q_wrt2 = (3*diff_a2_wrt2-2*cv.a1*diff_a1_wrt2)/9.

        diff_L_wrt1 = (9*(cv.a1*diff_a2_wrt1+diff_a1_wrt1*cv.a2)-27*diff_a3_wrt1-2*3*diff_a1_wrt1*cv.a1**2)/54.
        diff_L_wrt2 = (9*(cv.a1*diff_a2_wrt2+diff_a1_wrt2*cv.a2)-27*diff_a3_wrt2-2*3*diff_a1_wrt2*cv.a1**2)/54.

        diff_D_wrt1 = 3*diff_Q_wrt1*cv.Q**2+2*diff_L_wrt1*cv.L
        diff_D_wrt2 = 3*diff_Q_wrt2*cv.Q**2+2*diff_L_wrt2*cv.L

        diff_S1_wrt1 = (-1*(1./3.)*(-1*(diff_L_wrt1+0.5*diff_D_wrt1/np.sqrt(cv.D)))*(-1*(cv.L+np.sqrt(cv.D)))**(1./3.-1)) if (cv.L+np.sqrt(cv.D))<0 else ((1./3.)*(diff_L_wrt1+0.5*diff_D_wrt1/np.sqrt(cv.D))*(cv.L+np.sqrt(cv.D))**(1./3.-1))
        diff_S1_wrt2 = (-1*(1./3.)*(-1*(diff_L_wrt2+0.5*diff_D_wrt2/np.sqrt(cv.D)))*(-1*(cv.L+np.sqrt(cv.D)))**(1./3.-1)) if (cv.L+np.sqrt(cv.D))<0 else ((1./3.)*(diff_L_wrt2+0.5*diff_D_wrt2/np.sqrt(cv.D))*(cv.L+np.sqrt(cv.D))**(1./3.-1))

        diff_S2_wrt1 = (-1*(1./3.)*(-1*(diff_L_wrt1-0.5*diff_D_wrt1/np.sqrt(cv.D)))*(-1*(cv.L-np.sqrt(cv.D)))**(1./3.-1)) if (cv.L-np.sqrt(cv.D))<0 else ((1./3.)*(diff_L_wrt1-0.5*diff_D_wrt1/np.sqrt(cv.D))*(cv.L-np.sqrt(cv.D))**(1./3.-1))
        diff_S2_wrt2 = (-1*(1./3.)*(-1*(diff_L_wrt2-0.5*diff_D_wrt2/np.sqrt(cv.D)))*(-1*(cv.L-np.sqrt(cv.D)))**(1./3.-1)) if (cv.L-np.sqrt(cv.D))<0 else ((1./3.)*(diff_L_wrt2-0.5*diff_D_wrt2/np.sqrt(cv.D))*(cv.L-np.sqrt(cv.D))**(1./3.-1))

        diff_Z1_d_wrt1 = diff_S1_wrt1+diff_S2_wrt1-diff_a1_wrt1/3
        diff_Z1_d_wrt2 = diff_S1_wrt2+diff_S2_wrt2-diff_a1_wrt2/3

        diff_Z1_dd_wrt1 = ((-1*(1./3.)*(-1*diff_L_wrt1)*(-1*cv.L)**(1./3.-1)) if cv.L<0 else (1./3.*diff_L_wrt1*cv.L**(1./3.-1))) *2-diff_a1_wrt1/3
        diff_Z1_dd_wrt2 = ((-1*(1./3.)*(-1*diff_L_wrt2)*(-1*cv.L)**(1./3.-1)) if cv.L<0 else (1./3.*diff_L_wrt2*cv.L**(1./3.-1))) *2-diff_a1_wrt2/3

        diff_Z2_dd_wrt1 = (((-1*(1./3.)*(-1*diff_L_wrt1)*(-1*cv.L)**(1./3.-1)) if cv.L<0 else (1./3.*diff_L_wrt1*cv.L**(1./3.-1))) +diff_a1_wrt1/3) * -1.
        diff_Z2_dd_wrt2 = (((-1*(1./3.)*(-1*diff_L_wrt2)*(-1*cv.L)**(1./3.-1)) if cv.L<0 else (1./3.*diff_L_wrt2*cv.L**(1./3.-1))) +diff_a1_wrt2/3) * -1.
        
        diff_Z3_dd_wrt1 = diff_Z2_dd_wrt1
        diff_Z3_dd_wrt2 = diff_Z2_dd_wrt2

        diff_Z1_wrt1 = 0 if cv.D>=0 or cv.Q>=0 else (2*(-0.5*diff_Q_wrt1/np.sqrt(-1*cv.Q)*np.cos(1./3.*(np.arccos( 1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3) ))+2*np.pi/3)+np.sqrt(-1*cv.Q)*(-1)*(1./3.*-1/np.sqrt(1-(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3))**2))*((np.sqrt(-cv.Q**3)*diff_L_wrt1-(0.5*-1*3*diff_Q_wrt1*cv.Q**2)/np.sqrt(-cv.Q**3)*cv.L)/(-cv.Q**3))*np.sin(1./3.*(np.arccos(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3)))+2*np.pi/3))-diff_a1_wrt1/3)
        diff_Z1_wrt2 = 0 if cv.D>=0 or cv.Q>=0 else (2*(-0.5*diff_Q_wrt2/np.sqrt(-1*cv.Q)*np.cos(1./3.*(np.arccos( 1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3) ))+2*np.pi/3)+np.sqrt(-1*cv.Q)*(-1)*(1./3.*-1/np.sqrt(1-(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3))**2))*((np.sqrt(-cv.Q**3)*diff_L_wrt2-(0.5*-1*3*diff_Q_wrt2*cv.Q**2)/np.sqrt(-cv.Q**3)*cv.L)/(-cv.Q**3))*np.sin(1./3.*(np.arccos(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3)))+2*np.pi/3))-diff_a1_wrt2/3)

        diff_Z2_wrt1 = 0 if cv.D>=0 or cv.Q>=0 else (2*(-0.5*diff_Q_wrt1/np.sqrt(-1*cv.Q)*np.cos(1./3.*(np.arccos( 1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3) ))+4*np.pi/3)+np.sqrt(-1*cv.Q)*(-1)*(1./3.*-1/np.sqrt(1-(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3))**2))*((np.sqrt(-cv.Q**3)*diff_L_wrt1-(0.5*-1*3*diff_Q_wrt1*cv.Q**2)/np.sqrt(-cv.Q**3)*cv.L)/(-cv.Q**3))*np.sin(1./3.*(np.arccos(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3)))+4*np.pi/3))-diff_a1_wrt1/3)
        diff_Z2_wrt2 = 0 if cv.D>=0 or cv.Q>=0 else (2*(-0.5*diff_Q_wrt2/np.sqrt(-1*cv.Q)*np.cos(1./3.*(np.arccos( 1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3) ))+4*np.pi/3)+np.sqrt(-1*cv.Q)*(-1)*(1./3.*-1/np.sqrt(1-(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3))**2))*((np.sqrt(-cv.Q**3)*diff_L_wrt2-(0.5*-1*3*diff_Q_wrt2*cv.Q**2)/np.sqrt(-cv.Q**3)*cv.L)/(-cv.Q**3))*np.sin(1./3.*(np.arccos(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3)))+4*np.pi/3))-diff_a1_wrt2/3)

        diff_Z3_wrt1 = 0 if cv.D>=0 or cv.Q>=0 else (2*(-0.5*diff_Q_wrt1/np.sqrt(-1*cv.Q)*np.cos(1./3.*(np.arccos( 1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3) )))+np.sqrt(-1*cv.Q)*(-1)*(1./3.*-1/np.sqrt(1-(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3))**2))*((np.sqrt(-cv.Q**3)*diff_L_wrt1-(0.5*-1*3*diff_Q_wrt1*cv.Q**2)/np.sqrt(-cv.Q**3)*cv.L)/(-cv.Q**3))*np.sin(1./3.*(np.arccos(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3)))))-diff_a1_wrt1/3)
        diff_Z3_wrt2 = 0 if cv.D>=0 or cv.Q>=0 else (2*(-0.5*diff_Q_wrt2/np.sqrt(-1*cv.Q)*np.cos(1./3.*(np.arccos( 1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3) )))+np.sqrt(-1*cv.Q)*(-1)*(1./3.*-1/np.sqrt(1-(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3))**2))*((np.sqrt(-cv.Q**3)*diff_L_wrt2-(0.5*-1*3*diff_Q_wrt2*cv.Q**2)/np.sqrt(-cv.Q**3)*cv.L)/(-cv.Q**3))*np.sin(1./3.*(np.arccos(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3)))))-diff_a1_wrt2/3)

        diff_ZL_wrt1 = (diff_Z1_wrt1 if cv.ZL==cv.Z1 else (diff_Z2_wrt1 if cv.ZL==cv.Z2 else  diff_Z3_wrt1)) if cv.D<0 else ((diff_Z1_dd_wrt1 if cv.ZL==cv.Z1_dd else (diff_Z2_dd_wrt1 if cv.ZL==cv.Z2_dd else diff_Z3_dd_wrt1)) if cv.D==0 else diff_Z1_d_wrt1)
        diff_ZL_wrt2 = (diff_Z1_wrt2 if cv.ZL==cv.Z1 else (diff_Z2_wrt2 if cv.ZL==cv.Z2 else  diff_Z3_wrt2)) if cv.D<0 else ((diff_Z1_dd_wrt2 if cv.ZL==cv.Z1_dd else (diff_Z2_dd_wrt2 if cv.ZL==cv.Z2_dd else diff_Z3_dd_wrt2)) if cv.D==0 else diff_Z1_d_wrt2)

        diff_ZV_wrt1 = (diff_Z1_wrt1 if cv.ZV==cv.Z1 else (diff_Z2_wrt1 if cv.ZV==cv.Z2 else  diff_Z3_wrt1)) if cv.D<0 else ((diff_Z1_dd_wrt1 if cv.ZV==cv.Z1_dd else (diff_Z2_dd_wrt1 if cv.ZV==cv.Z2_dd else diff_Z3_dd_wrt1)) if cv.D==0 else 0)
        diff_ZV_wrt2 = (diff_Z1_wrt2 if cv.ZV==cv.Z1 else (diff_Z2_wrt2 if cv.ZV==cv.Z2 else  diff_Z3_wrt2)) if cv.D<0 else ((diff_Z1_dd_wrt2 if cv.ZV==cv.Z1_dd else (diff_Z2_dd_wrt2 if cv.ZV==cv.Z2_dd else diff_Z3_dd_wrt2)) if cv.D==0 else 0)

        diff_phl_wrt1 = 0 if (cv.ZL-cv.B)<=0 or cv.B*(cv.r2 - cv.r1) == 0 or (cv.ZL + cv.r1 * cv.B) == 0 or (cv.ZL + cv.r2 * cv.B) == 0 or (cv.ZL + cv.r2 * cv.B) * (cv.ZL + cv.r1 * cv.B) < 0  else ((diff_ZL_wrt1)-(diff_ZL_wrt1-diff_B_wrt1)/(cv.ZL-cv.B)-((((cv.B*(cv.r2-cv.r1))*diff_A_wrt1-(diff_B_wrt1*(cv.r2-cv.r1)+cv.B*(diff_r2_wrt1-diff_r1_wrt1))*cv.A)/(cv.B*(cv.r2-cv.r1))**2)*np.log((cv.ZL+cv.r2*cv.B)/(cv.ZL+cv.r1*cv.B))+(cv.A/(cv.B*(cv.r2-cv.r1)))*1/((cv.ZL+cv.r2*cv.B)/(cv.ZL+cv.r1*cv.B))*(((cv.ZL+cv.r1*cv.B)*(diff_ZL_wrt1+diff_r2_wrt1*cv.B+cv.r2*diff_B_wrt1)-(cv.ZL+cv.r2*cv.B)*(diff_ZL_wrt1+diff_r1_wrt1*cv.B+cv.r1*diff_B_wrt1))/(cv.ZL+cv.r1*cv.B)**2)))
        diff_phl_wrt2 = 0 if (cv.ZL-cv.B)<=0 or cv.B*(cv.r2 - cv.r1) == 0 or (cv.ZL + cv.r1 * cv.B) == 0 or (cv.ZL + cv.r2 * cv.B) == 0 or (cv.ZL + cv.r2 * cv.B) * (cv.ZL + cv.r1 * cv.B) < 0 else ((diff_ZL_wrt2)-(diff_ZL_wrt2-diff_B_wrt2)/(cv.ZL-cv.B)-((((cv.B*(cv.r2-cv.r1))*diff_A_wrt2-(diff_B_wrt2*(cv.r2-cv.r1)+cv.B*(diff_r2_wrt2-diff_r1_wrt2))*cv.A)/(cv.B*(cv.r2-cv.r1))**2)*np.log((cv.ZL+cv.r2*cv.B)/(cv.ZL+cv.r1*cv.B))+(cv.A/(cv.B*(cv.r2-cv.r1)))*1/((cv.ZL+cv.r2*cv.B)/(cv.ZL+cv.r1*cv.B))*(((cv.ZL+cv.r1*cv.B)*(diff_ZL_wrt2+diff_r2_wrt2*cv.B+cv.r2*diff_B_wrt2)-(cv.ZL+cv.r2*cv.B)*(diff_ZL_wrt2+diff_r1_wrt2*cv.B+cv.r1*diff_B_wrt2))/(cv.ZL+cv.r1*cv.B)**2)))

        diff_phv_wrt1 = 0 if (cv.ZV-cv.B)<=0 or cv.B*(cv.r2 - cv.r1) == 0 or (cv.ZV + cv.r1 * cv.B) == 0 or (cv.ZV + cv.r2 * cv.B) == 0 or (cv.ZV + cv.r2 * cv.B) * (cv.ZV + cv.r1 * cv.B) < 0 else ((diff_ZV_wrt1)-(diff_ZV_wrt1-diff_B_wrt1)/(cv.ZV-cv.B)-((((cv.B*(cv.r2-cv.r1))*diff_A_wrt1-(diff_B_wrt1*(cv.r2-cv.r1)+cv.B*(diff_r2_wrt1-diff_r1_wrt1))*cv.A)/(cv.B*(cv.r2-cv.r1))**2)*np.log((cv.ZV+cv.r2*cv.B)/(cv.ZV+cv.r1*cv.B))+(cv.A/(cv.B*(cv.r2-cv.r1)))*1/((cv.ZV+cv.r2*cv.B)/(cv.ZV+cv.r1*cv.B))*(((cv.ZV+cv.r1*cv.B)*(diff_ZV_wrt1+diff_r2_wrt1*cv.B+cv.r2*diff_B_wrt1)-(cv.ZV+cv.r2*cv.B)*(diff_ZV_wrt1+diff_r1_wrt1*cv.B+cv.r1*diff_B_wrt1))/(cv.ZV+cv.r1*cv.B)**2)))
        diff_phv_wrt2 = 0 if (cv.ZV-cv.B)<=0 or cv.B*(cv.r2 - cv.r1) == 0 or (cv.ZV + cv.r1 * cv.B) == 0 or (cv.ZV + cv.r2 * cv.B) == 0 or (cv.ZV + cv.r2 * cv.B) * (cv.ZV + cv.r1 * cv.B) < 0 else ((diff_ZV_wrt2)-(diff_ZV_wrt2-diff_B_wrt2)/(cv.ZV-cv.B)-((((cv.B*(cv.r2-cv.r1))*diff_A_wrt2-(diff_B_wrt2*(cv.r2-cv.r1)+cv.B*(diff_r2_wrt2-diff_r1_wrt2))*cv.A)/(cv.B*(cv.r2-cv.r1))**2)*np.log((cv.ZV+cv.r2*cv.B)/(cv.ZV+cv.r1*cv.B))+(cv.A/(cv.B*(cv.r2-cv.r1)))*1/((cv.ZV+cv.r2*cv.B)/(cv.ZV+cv.r1*cv.B))*(((cv.ZV+cv.r1*cv.B)*(diff_ZV_wrt2+diff_r2_wrt2*cv.B+cv.r2*diff_B_wrt2)-(cv.ZV+cv.r2*cv.B)*(diff_ZV_wrt2+diff_r1_wrt2*cv.B+cv.r1*diff_B_wrt2))/(cv.ZV+cv.r1*cv.B)**2)))

        diff_ph_diff_wrt1 = np.sign(cv.phl-cv.phv)*(diff_phl_wrt1-diff_phv_wrt1)
        diff_ph_diff_wrt2 = np.sign(cv.phl-cv.phv)*(diff_phl_wrt2-diff_phv_wrt2)

        diff_t1_wrt1 = gasConstant*ov.bp_final1*(diff_ZV_wrt1-diff_ZL_wrt1)
        diff_t1_wrt2 = gasConstant*ov.bp_final1*(diff_ZV_wrt2-diff_ZL_wrt2)

        diff_t2_wrt1 = 0 if cv.b == 0 or (cv.u**2-4*cv.w) <= 0 else ((cv.b*np.sqrt(cv.u**2-4*cv.w))*(diff_ac_wrt1*(ov.bp_final1*cv.diff_alpha_wrt_temp-cv.alpha_eos)+cv.ac*(ov.bp_final1*diff_diff_alpha_wrt1-diff_alpha_wrt1))-(diff_b_wrt1*np.sqrt(cv.u**2-4*cv.w)+cv.b*0.5*(2*cv.u*diff_u_wrt1-4*diff_w_wrt1)/np.sqrt(cv.u**2-4*cv.w))*(cv.ac*(ov.bp_final1*cv.diff_alpha_wrt_temp-cv.alpha_eos)))/(cv.b*np.sqrt(cv.u**2-4*cv.w))**2
        diff_t2_wrt2 = 0 if cv.b == 0 or (cv.u**2-4*cv.w) <= 0 else ((cv.b*np.sqrt(cv.u**2-4*cv.w))*(diff_ac_wrt2*(ov.bp_final1*cv.diff_alpha_wrt_temp-cv.alpha_eos)+cv.ac*(ov.bp_final1*diff_diff_alpha_wrt2-diff_alpha_wrt2))-(diff_b_wrt2*np.sqrt(cv.u**2-4*cv.w)+cv.b*0.5*(2*cv.u*diff_u_wrt2-4*diff_w_wrt2)/np.sqrt(cv.u**2-4*cv.w))*(cv.ac*(ov.bp_final1*cv.diff_alpha_wrt_temp-cv.alpha_eos)))/(cv.b*np.sqrt(cv.u**2-4*cv.w))**2

        diff_t3_wrt1 = 0 if (cv.ZV+cv.B*cv.r2) == 0 or (cv.ZV+cv.B*cv.r1) == 0 or (cv.ZV+cv.B*cv.r2)*(cv.ZV+cv.B*cv.r1) < 0 else (((cv.ZV+cv.B*cv.r1)/(cv.ZV+cv.B*cv.r2))*((cv.ZV+cv.B*cv.r1)*(diff_ZV_wrt1+diff_B_wrt1*cv.r2+cv.B*diff_r2_wrt1)-(diff_ZV_wrt1+diff_B_wrt1*cv.r1+cv.B*diff_r1_wrt1)*(cv.ZV+cv.B*cv.r2))/(cv.ZV+cv.B*cv.r1)**2)
        diff_t3_wrt2 = 0 if (cv.ZV+cv.B*cv.r2) == 0 or (cv.ZV+cv.B*cv.r1) == 0 or (cv.ZV+cv.B*cv.r2)*(cv.ZV+cv.B*cv.r1) < 0 else (((cv.ZV+cv.B*cv.r1)/(cv.ZV+cv.B*cv.r2))*((cv.ZV+cv.B*cv.r1)*(diff_ZV_wrt2+diff_B_wrt2*cv.r2+cv.B*diff_r2_wrt2)-(diff_ZV_wrt2+diff_B_wrt2*cv.r1+cv.B*diff_r1_wrt2)*(cv.ZV+cv.B*cv.r2))/(cv.ZV+cv.B*cv.r1)**2)

        diff_t4_wrt1 = 0 if (cv.ZL+cv.B*cv.r2) == 0 or (cv.ZL+cv.B*cv.r1) == 0 or (cv.ZL+cv.B*cv.r2)*(cv.ZL+cv.B*cv.r1) < 0 else (((cv.ZL+cv.B*cv.r1)/(cv.ZL+cv.B*cv.r2))*((cv.ZL+cv.B*cv.r1)*(diff_ZL_wrt1+diff_B_wrt1*cv.r2+cv.B*diff_r2_wrt1)-(diff_ZL_wrt1+diff_B_wrt1*cv.r1+cv.B*diff_r1_wrt1)*(cv.ZL+cv.B*cv.r2))/(cv.ZL+cv.B*cv.r1)**2)
        diff_t4_wrt2 = 0 if (cv.ZL+cv.B*cv.r2) == 0 or (cv.ZL+cv.B*cv.r1) == 0 or (cv.ZL+cv.B*cv.r2)*(cv.ZL+cv.B*cv.r1) < 0 else (((cv.ZL+cv.B*cv.r1)/(cv.ZL+cv.B*cv.r2))*((cv.ZL+cv.B*cv.r1)*(diff_ZL_wrt2+diff_B_wrt2*cv.r2+cv.B*diff_r2_wrt2)-(diff_ZL_wrt2+diff_B_wrt2*cv.r1+cv.B*diff_r1_wrt2)*(cv.ZL+cv.B*cv.r2))/(cv.ZL+cv.B*cv.r1)**2)

        diff_hv_bp_wrt1 = (diff_t1_wrt1+diff_t2_wrt1*(cv.t3-cv.t4)+cv.t2*(diff_t3_wrt1-diff_t4_wrt1))/1000.
        diff_hv_bp_wrt2 = (diff_t1_wrt2+diff_t2_wrt2*(cv.t3-cv.t4)+cv.t2*(diff_t3_wrt2-diff_t4_wrt2))/1000.

        diff_vb_wrt1 = diff_ZL_wrt1*gasConstant*ov.bp_final1/pressure
        diff_vb_wrt2 = diff_ZL_wrt2*gasConstant*ov.bp_final1/pressure

        diff_wrt1 = self.scales[0]*(2*(ov.hv_bp-cv.hv_bp)/ov.hv_bp**2)*(-1.)*diff_hv_bp_wrt1+self.scales[1]*(2*(ov.vb-cv.vb)/ov.vb**2)*(-1)*diff_vb_wrt1+self.scales[2]*2*(max(0,(cv.ph_diff-10**-7)))*(0 if(cv.ph_diff-10**-7)<=0 else diff_ph_diff_wrt1)
        diff_wrt2 = self.scales[0]*(2*(ov.hv_bp-cv.hv_bp)/ov.hv_bp**2)*(-1.)*diff_hv_bp_wrt2+self.scales[1]*(2*(ov.vb-cv.vb)/ov.vb**2)*(-1)*diff_vb_wrt2+self.scales[2]*2*(max(0,(cv.ph_diff-10**-7)))*(0 if(cv.ph_diff-10**-7)<=0 else diff_ph_diff_wrt2)

        diff_wrt1 = (np.sign(diff_wrt1)*10**7) if np.abs(diff_wrt1)>10**7 else diff_wrt1
        diff_wrt2 = (np.sign(diff_wrt2)*10**7) if np.abs(diff_wrt2)>10**7 else diff_wrt2    
        return np.array([diff_wrt1, diff_wrt2])

    def pr(self, ov, cv):
        diff_alpha_wrt1 = ov.alpha_pr
        diff_alpha_wrt2 = 0
        
        diff_diff_alpha_wrt1 = ov.alpha_pr*(((2*ov.alpha_m-2)/(ov.bp_final1/ov.tc_k)+ov.alpha_l*(-2*ov.alpha_m)*(ov.bp_final1/ov.tc_k)**(2*ov.alpha_m-1))/ov.tc_k)
        diff_diff_alpha_wrt2 = 0

        diff_om_b_wrt1 = 0
        diff_om_b_wrt2 = 0.0777960739038885

        diff_om_a_wrt1 = 0
        diff_om_a_wrt2 = 0

        diff_b_wrt1 = diff_om_b_wrt1*gasConstant*ov.tc_k/(ov.pc_mpa*10**6)
        diff_b_wrt2 = diff_om_b_wrt2*gasConstant*ov.tc_k/(ov.pc_mpa*10**6)

        diff_pf_wrt1 = 0
        diff_pf_wrt2 = diff_b_wrt2 * 10**6/ov.vc_final

        diff_ac_wrt1 = 0
        diff_ac_wrt2 = diff_om_a_wrt2*gasConstant**2*ov.tc_k**2/(ov.pc_mpa*10**6)

        diff_ac_alpha_wrt1 = (cv.ac*diff_alpha_wrt1+diff_ac_wrt1*cv.alpha_eos)
        diff_ac_alpha_wrt2 = (cv.ac*diff_alpha_wrt2+diff_ac_wrt2*cv.alpha_eos)

        diff_u_wrt1 = 0 if cv.pf == 0 else ((-1. * diff_pf_wrt1)/cv.pf**2 * (1./ov.zc_final - 3.))
        diff_u_wrt2 = 0 if cv.pf == 0 else ((-1. * diff_pf_wrt2)/cv.pf**2 * (1./ov.zc_final - 3.))

        diff_w_wrt1 = 0 if ((cv.u-1)*cv.pf**3+3*cv.pf**2) == 0 else (((-3*diff_pf_wrt1-(2*diff_u_wrt1*cv.u*cv.pf**3+cv.u**2*3*diff_pf_wrt1*cv.pf**2)-3*(diff_u_wrt1*cv.pf**2+cv.u*2*diff_pf_wrt1*cv.pf))*((cv.u-1)*cv.pf**3+3*cv.pf**2)-(1-3*cv.pf-cv.u**2*cv.pf**3-3*cv.u*cv.pf**2)*((diff_u_wrt1*cv.pf**3+(cv.u-1)*3*diff_pf_wrt1*cv.pf**2)+3*2*diff_pf_wrt1*cv.pf))/((cv.u-1)*cv.pf**3+3*cv.pf**2)**2)
        diff_w_wrt2 = 0 if ((cv.u-1)*cv.pf**3+3*cv.pf**2) == 0 else (((-3*diff_pf_wrt2-(2*diff_u_wrt2*cv.u*cv.pf**3+cv.u**2*3*diff_pf_wrt2*cv.pf**2)-3*(diff_u_wrt2*cv.pf**2+cv.u*2*diff_pf_wrt2*cv.pf))*((cv.u-1)*cv.pf**3+3*cv.pf**2)-(1-3*cv.pf-cv.u**2*cv.pf**3-3*cv.u*cv.pf**2)*((diff_u_wrt2*cv.pf**3+(cv.u-1)*3*diff_pf_wrt2*cv.pf**2)+3*2*diff_pf_wrt2*cv.pf))/((cv.u-1)*cv.pf**3+3*cv.pf**2)**2)

        diff_r2_wrt1 = 0
        diff_r2_wrt2 = 0 if (cv.u**2-4*cv.w) <= 0 else (diff_u_wrt2+0.5*(2*cv.u*diff_u_wrt2-4*diff_w_wrt2)*(cv.u**2-4*cv.w)**-0.5)/2

        diff_r1_wrt1 = 0
        diff_r1_wrt2 = 0 if (cv.u**2-4*cv.w) <= 0 else (diff_u_wrt2-0.5*(2*cv.u*diff_u_wrt2-4*diff_w_wrt2)*(cv.u**2-4*cv.w)**-0.5)/2              

        diff_A_wrt1 = diff_ac_alpha_wrt1*pressure*10**6/(gasConstant**2*ov.bp_final1**2)
        diff_A_wrt2 = diff_ac_alpha_wrt2*pressure*10**6/(gasConstant**2*ov.bp_final1**2)

        diff_B_wrt1 = 0
        diff_B_wrt2 = diff_b_wrt2*pressure*10**6/(gasConstant*ov.bp_final1)   

        diff_a1_wrt1 = 0
        diff_a1_wrt2 = ((cv.u*diff_B_wrt2+diff_u_wrt2*cv.B)-diff_B_wrt2) 

        diff_a2_wrt1 = diff_A_wrt1+(cv.w*2*cv.B*diff_B_wrt1+diff_w_wrt1*cv.B**2)-(cv.u*diff_B_wrt1+diff_u_wrt1*cv.B)-(cv.u*2*cv.B*diff_B_wrt1+diff_u_wrt1*cv.B**2)
        diff_a2_wrt2 = diff_A_wrt2+(cv.w*2*cv.B*diff_B_wrt2+diff_w_wrt2*cv.B**2)-(cv.u*diff_B_wrt2+diff_u_wrt2*cv.B)-(cv.u*2*cv.B*diff_B_wrt2+diff_u_wrt2*cv.B**2)

        diff_a3_wrt1 = -1*((cv.A*diff_B_wrt1+diff_A_wrt1*cv.B)+(cv.w*2*cv.B*diff_B_wrt1+diff_w_wrt1*cv.B**2)+(cv.w*3*cv.B**2*diff_B_wrt1+diff_w_wrt1*cv.B**3))
        diff_a3_wrt2 = -1*((cv.A*diff_B_wrt2+diff_A_wrt2*cv.B)+(cv.w*2*cv.B*diff_B_wrt2+diff_w_wrt2*cv.B**2)+(cv.w*3*cv.B**2*diff_B_wrt2+diff_w_wrt2*cv.B**3))

        diff_Q_wrt1 = (3*diff_a2_wrt1-2*cv.a1*diff_a1_wrt1)/9.
        diff_Q_wrt2 = (3*diff_a2_wrt2-2*cv.a1*diff_a1_wrt2)/9.

        diff_L_wrt1 = (9*(cv.a1*diff_a2_wrt1+diff_a1_wrt1*cv.a2)-27*diff_a3_wrt1-2*3*diff_a1_wrt1*cv.a1**2)/54.
        diff_L_wrt2 = (9*(cv.a1*diff_a2_wrt2+diff_a1_wrt2*cv.a2)-27*diff_a3_wrt2-2*3*diff_a1_wrt2*cv.a1**2)/54.

        diff_D_wrt1 = 3*diff_Q_wrt1*cv.Q**2+2*diff_L_wrt1*cv.L
        diff_D_wrt2 = 3*diff_Q_wrt2*cv.Q**2+2*diff_L_wrt2*cv.L

        diff_S1_wrt1 = (-1*(1./3.)*(-1*(diff_L_wrt1+0.5*diff_D_wrt1/np.sqrt(cv.D)))*(-1*(cv.L+np.sqrt(cv.D)))**(1./3.-1)) if (cv.L+np.sqrt(cv.D))<0 else ((1./3.)*(diff_L_wrt1+0.5*diff_D_wrt1/np.sqrt(cv.D))*(cv.L+np.sqrt(cv.D))**(1./3.-1))
        diff_S1_wrt2 = (-1*(1./3.)*(-1*(diff_L_wrt2+0.5*diff_D_wrt2/np.sqrt(cv.D)))*(-1*(cv.L+np.sqrt(cv.D)))**(1./3.-1)) if (cv.L+np.sqrt(cv.D))<0 else ((1./3.)*(diff_L_wrt2+0.5*diff_D_wrt2/np.sqrt(cv.D))*(cv.L+np.sqrt(cv.D))**(1./3.-1))

        diff_S2_wrt1 = (-1*(1./3.)*(-1*(diff_L_wrt1-0.5*diff_D_wrt1/np.sqrt(cv.D)))*(-1*(cv.L-np.sqrt(cv.D)))**(1./3.-1)) if (cv.L-np.sqrt(cv.D))<0 else ((1./3.)*(diff_L_wrt1-0.5*diff_D_wrt1/np.sqrt(cv.D))*(cv.L-np.sqrt(cv.D))**(1./3.-1))
        diff_S2_wrt2 = (-1*(1./3.)*(-1*(diff_L_wrt2-0.5*diff_D_wrt2/np.sqrt(cv.D)))*(-1*(cv.L-np.sqrt(cv.D)))**(1./3.-1)) if (cv.L-np.sqrt(cv.D))<0 else ((1./3.)*(diff_L_wrt2-0.5*diff_D_wrt2/np.sqrt(cv.D))*(cv.L-np.sqrt(cv.D))**(1./3.-1))

        diff_Z1_d_wrt1 = diff_S1_wrt1+diff_S2_wrt1-diff_a1_wrt1/3
        diff_Z1_d_wrt2 = diff_S1_wrt2+diff_S2_wrt2-diff_a1_wrt2/3

        diff_Z1_dd_wrt1 = ((-1*(1./3.)*(-1*diff_L_wrt1)*(-1*cv.L)**(1./3.-1)) if cv.L<0 else (1./3.*diff_L_wrt1*cv.L**(1./3.-1))) *2-diff_a1_wrt1/3
        diff_Z1_dd_wrt2 = ((-1*(1./3.)*(-1*diff_L_wrt2)*(-1*cv.L)**(1./3.-1)) if cv.L<0 else (1./3.*diff_L_wrt2*cv.L**(1./3.-1))) *2-diff_a1_wrt2/3

        diff_Z2_dd_wrt1 = (((-1*(1./3.)*(-1*diff_L_wrt1)*(-1*cv.L)**(1./3.-1)) if cv.L<0 else (1./3.*diff_L_wrt1*cv.L**(1./3.-1))) +diff_a1_wrt1/3) * -1.
        diff_Z2_dd_wrt2 = (((-1*(1./3.)*(-1*diff_L_wrt2)*(-1*cv.L)**(1./3.-1)) if cv.L<0 else (1./3.*diff_L_wrt2*cv.L**(1./3.-1))) +diff_a1_wrt2/3) * -1.
        
        diff_Z3_dd_wrt1 = diff_Z2_dd_wrt1
        diff_Z3_dd_wrt2 = diff_Z2_dd_wrt2

        diff_Z1_wrt1 = 0 if cv.D>=0 or cv.Q>=0 else (2*(-0.5*diff_Q_wrt1/np.sqrt(-1*cv.Q)*np.cos(1./3.*(np.arccos( 1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3) ))+2*np.pi/3)+np.sqrt(-1*cv.Q)*(-1)*(1./3.*-1/np.sqrt(1-(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3))**2))*((np.sqrt(-cv.Q**3)*diff_L_wrt1-(0.5*-1*3*diff_Q_wrt1*cv.Q**2)/np.sqrt(-cv.Q**3)*cv.L)/(-cv.Q**3))*np.sin(1./3.*(np.arccos(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3)))+2*np.pi/3))-diff_a1_wrt1/3)
        diff_Z1_wrt2 = 0 if cv.D>=0 or cv.Q>=0 else (2*(-0.5*diff_Q_wrt2/np.sqrt(-1*cv.Q)*np.cos(1./3.*(np.arccos( 1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3) ))+2*np.pi/3)+np.sqrt(-1*cv.Q)*(-1)*(1./3.*-1/np.sqrt(1-(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3))**2))*((np.sqrt(-cv.Q**3)*diff_L_wrt2-(0.5*-1*3*diff_Q_wrt2*cv.Q**2)/np.sqrt(-cv.Q**3)*cv.L)/(-cv.Q**3))*np.sin(1./3.*(np.arccos(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3)))+2*np.pi/3))-diff_a1_wrt2/3)

        diff_Z2_wrt1 = 0 if cv.D>=0 or cv.Q>=0 else (2*(-0.5*diff_Q_wrt1/np.sqrt(-1*cv.Q)*np.cos(1./3.*(np.arccos( 1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3) ))+4*np.pi/3)+np.sqrt(-1*cv.Q)*(-1)*(1./3.*-1/np.sqrt(1-(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3))**2))*((np.sqrt(-cv.Q**3)*diff_L_wrt1-(0.5*-1*3*diff_Q_wrt1*cv.Q**2)/np.sqrt(-cv.Q**3)*cv.L)/(-cv.Q**3))*np.sin(1./3.*(np.arccos(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3)))+4*np.pi/3))-diff_a1_wrt1/3)
        diff_Z2_wrt2 = 0 if cv.D>=0 or cv.Q>=0 else (2*(-0.5*diff_Q_wrt2/np.sqrt(-1*cv.Q)*np.cos(1./3.*(np.arccos( 1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3) ))+4*np.pi/3)+np.sqrt(-1*cv.Q)*(-1)*(1./3.*-1/np.sqrt(1-(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3))**2))*((np.sqrt(-cv.Q**3)*diff_L_wrt2-(0.5*-1*3*diff_Q_wrt2*cv.Q**2)/np.sqrt(-cv.Q**3)*cv.L)/(-cv.Q**3))*np.sin(1./3.*(np.arccos(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3)))+4*np.pi/3))-diff_a1_wrt2/3)

        diff_Z3_wrt1 = 0 if cv.D>=0 or cv.Q>=0 else (2*(-0.5*diff_Q_wrt1/np.sqrt(-1*cv.Q)*np.cos(1./3.*(np.arccos( 1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3) )))+np.sqrt(-1*cv.Q)*(-1)*(1./3.*-1/np.sqrt(1-(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3))**2))*((np.sqrt(-cv.Q**3)*diff_L_wrt1-(0.5*-1*3*diff_Q_wrt1*cv.Q**2)/np.sqrt(-cv.Q**3)*cv.L)/(-cv.Q**3))*np.sin(1./3.*(np.arccos(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3)))))-diff_a1_wrt1/3)
        diff_Z3_wrt2 = 0 if cv.D>=0 or cv.Q>=0 else (2*(-0.5*diff_Q_wrt2/np.sqrt(-1*cv.Q)*np.cos(1./3.*(np.arccos( 1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3) )))+np.sqrt(-1*cv.Q)*(-1)*(1./3.*-1/np.sqrt(1-(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3))**2))*((np.sqrt(-cv.Q**3)*diff_L_wrt2-(0.5*-1*3*diff_Q_wrt2*cv.Q**2)/np.sqrt(-cv.Q**3)*cv.L)/(-cv.Q**3))*np.sin(1./3.*(np.arccos(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3)))))-diff_a1_wrt2/3)

        diff_ZL_wrt1 = (diff_Z1_wrt1 if cv.ZL==cv.Z1 else (diff_Z2_wrt1 if cv.ZL==cv.Z2 else  diff_Z3_wrt1)) if cv.D<0 else ((diff_Z1_dd_wrt1 if cv.ZL==cv.Z1_dd else (diff_Z2_dd_wrt1 if cv.ZL==cv.Z2_dd else diff_Z3_dd_wrt1)) if cv.D==0 else diff_Z1_d_wrt1)
        diff_ZL_wrt2 = (diff_Z1_wrt2 if cv.ZL==cv.Z1 else (diff_Z2_wrt2 if cv.ZL==cv.Z2 else  diff_Z3_wrt2)) if cv.D<0 else ((diff_Z1_dd_wrt2 if cv.ZL==cv.Z1_dd else (diff_Z2_dd_wrt2 if cv.ZL==cv.Z2_dd else diff_Z3_dd_wrt2)) if cv.D==0 else diff_Z1_d_wrt2)

        diff_ZV_wrt1 = (diff_Z1_wrt1 if cv.ZV==cv.Z1 else (diff_Z2_wrt1 if cv.ZV==cv.Z2 else  diff_Z3_wrt1)) if cv.D<0 else ((diff_Z1_dd_wrt1 if cv.ZV==cv.Z1_dd else (diff_Z2_dd_wrt1 if cv.ZV==cv.Z2_dd else diff_Z3_dd_wrt1)) if cv.D==0 else 0)
        diff_ZV_wrt2 = (diff_Z1_wrt2 if cv.ZV==cv.Z1 else (diff_Z2_wrt2 if cv.ZV==cv.Z2 else  diff_Z3_wrt2)) if cv.D<0 else ((diff_Z1_dd_wrt2 if cv.ZV==cv.Z1_dd else (diff_Z2_dd_wrt2 if cv.ZV==cv.Z2_dd else diff_Z3_dd_wrt2)) if cv.D==0 else 0)

        diff_phl_wrt1 = 0 if (cv.ZL-cv.B)<=0 or cv.B*(cv.r2 - cv.r1) == 0 or (cv.ZL + cv.r1 * cv.B) == 0 or (cv.ZL + cv.r2 * cv.B) == 0 or (cv.ZL + cv.r2 * cv.B) * (cv.ZL + cv.r1 * cv.B) < 0  else ((diff_ZL_wrt1)-(diff_ZL_wrt1-diff_B_wrt1)/(cv.ZL-cv.B)-((((cv.B*(cv.r2-cv.r1))*diff_A_wrt1-(diff_B_wrt1*(cv.r2-cv.r1)+cv.B*(diff_r2_wrt1-diff_r1_wrt1))*cv.A)/(cv.B*(cv.r2-cv.r1))**2)*np.log((cv.ZL+cv.r2*cv.B)/(cv.ZL+cv.r1*cv.B))+(cv.A/(cv.B*(cv.r2-cv.r1)))*1/((cv.ZL+cv.r2*cv.B)/(cv.ZL+cv.r1*cv.B))*(((cv.ZL+cv.r1*cv.B)*(diff_ZL_wrt1+diff_r2_wrt1*cv.B+cv.r2*diff_B_wrt1)-(cv.ZL+cv.r2*cv.B)*(diff_ZL_wrt1+diff_r1_wrt1*cv.B+cv.r1*diff_B_wrt1))/(cv.ZL+cv.r1*cv.B)**2)))
        diff_phl_wrt2 = 0 if (cv.ZL-cv.B)<=0 or cv.B*(cv.r2 - cv.r1) == 0 or (cv.ZL + cv.r1 * cv.B) == 0 or (cv.ZL + cv.r2 * cv.B) == 0 or (cv.ZL + cv.r2 * cv.B) * (cv.ZL + cv.r1 * cv.B) < 0 else ((diff_ZL_wrt2)-(diff_ZL_wrt2-diff_B_wrt2)/(cv.ZL-cv.B)-((((cv.B*(cv.r2-cv.r1))*diff_A_wrt2-(diff_B_wrt2*(cv.r2-cv.r1)+cv.B*(diff_r2_wrt2-diff_r1_wrt2))*cv.A)/(cv.B*(cv.r2-cv.r1))**2)*np.log((cv.ZL+cv.r2*cv.B)/(cv.ZL+cv.r1*cv.B))+(cv.A/(cv.B*(cv.r2-cv.r1)))*1/((cv.ZL+cv.r2*cv.B)/(cv.ZL+cv.r1*cv.B))*(((cv.ZL+cv.r1*cv.B)*(diff_ZL_wrt2+diff_r2_wrt2*cv.B+cv.r2*diff_B_wrt2)-(cv.ZL+cv.r2*cv.B)*(diff_ZL_wrt2+diff_r1_wrt2*cv.B+cv.r1*diff_B_wrt2))/(cv.ZL+cv.r1*cv.B)**2)))

        diff_phv_wrt1 = 0 if (cv.ZV-cv.B)<=0 or cv.B*(cv.r2 - cv.r1) == 0 or (cv.ZV + cv.r1 * cv.B) == 0 or (cv.ZV + cv.r2 * cv.B) == 0 or (cv.ZV + cv.r2 * cv.B) * (cv.ZV + cv.r1 * cv.B) < 0 else ((diff_ZV_wrt1)-(diff_ZV_wrt1-diff_B_wrt1)/(cv.ZV-cv.B)-((((cv.B*(cv.r2-cv.r1))*diff_A_wrt1-(diff_B_wrt1*(cv.r2-cv.r1)+cv.B*(diff_r2_wrt1-diff_r1_wrt1))*cv.A)/(cv.B*(cv.r2-cv.r1))**2)*np.log((cv.ZV+cv.r2*cv.B)/(cv.ZV+cv.r1*cv.B))+(cv.A/(cv.B*(cv.r2-cv.r1)))*1/((cv.ZV+cv.r2*cv.B)/(cv.ZV+cv.r1*cv.B))*(((cv.ZV+cv.r1*cv.B)*(diff_ZV_wrt1+diff_r2_wrt1*cv.B+cv.r2*diff_B_wrt1)-(cv.ZV+cv.r2*cv.B)*(diff_ZV_wrt1+diff_r1_wrt1*cv.B+cv.r1*diff_B_wrt1))/(cv.ZV+cv.r1*cv.B)**2)))
        diff_phv_wrt2 = 0 if (cv.ZV-cv.B)<=0 or cv.B*(cv.r2 - cv.r1) == 0 or (cv.ZV + cv.r1 * cv.B) == 0 or (cv.ZV + cv.r2 * cv.B) == 0 or (cv.ZV + cv.r2 * cv.B) * (cv.ZV + cv.r1 * cv.B) < 0 else ((diff_ZV_wrt2)-(diff_ZV_wrt2-diff_B_wrt2)/(cv.ZV-cv.B)-((((cv.B*(cv.r2-cv.r1))*diff_A_wrt2-(diff_B_wrt2*(cv.r2-cv.r1)+cv.B*(diff_r2_wrt2-diff_r1_wrt2))*cv.A)/(cv.B*(cv.r2-cv.r1))**2)*np.log((cv.ZV+cv.r2*cv.B)/(cv.ZV+cv.r1*cv.B))+(cv.A/(cv.B*(cv.r2-cv.r1)))*1/((cv.ZV+cv.r2*cv.B)/(cv.ZV+cv.r1*cv.B))*(((cv.ZV+cv.r1*cv.B)*(diff_ZV_wrt2+diff_r2_wrt2*cv.B+cv.r2*diff_B_wrt2)-(cv.ZV+cv.r2*cv.B)*(diff_ZV_wrt2+diff_r1_wrt2*cv.B+cv.r1*diff_B_wrt2))/(cv.ZV+cv.r1*cv.B)**2)))

        diff_ph_diff_wrt1 = np.sign(cv.phl-cv.phv)*(diff_phl_wrt1-diff_phv_wrt1)
        diff_ph_diff_wrt2 = np.sign(cv.phl-cv.phv)*(diff_phl_wrt2-diff_phv_wrt2)

        diff_t1_wrt1 = gasConstant*ov.bp_final1*(diff_ZV_wrt1-diff_ZL_wrt1)
        diff_t1_wrt2 = gasConstant*ov.bp_final1*(diff_ZV_wrt2-diff_ZL_wrt2)

        diff_t2_wrt1 = 0 if cv.b == 0 or (cv.u**2-4*cv.w) <= 0 else ((cv.b*np.sqrt(cv.u**2-4*cv.w))*(diff_ac_wrt1*(ov.bp_final1*cv.diff_alpha_wrt_temp-cv.alpha_eos)+cv.ac*(ov.bp_final1*diff_diff_alpha_wrt1-diff_alpha_wrt1))-(diff_b_wrt1*np.sqrt(cv.u**2-4*cv.w)+cv.b*0.5*(2*cv.u*diff_u_wrt1-4*diff_w_wrt1)/np.sqrt(cv.u**2-4*cv.w))*(cv.ac*(ov.bp_final1*cv.diff_alpha_wrt_temp-cv.alpha_eos)))/(cv.b*np.sqrt(cv.u**2-4*cv.w))**2
        diff_t2_wrt2 = 0 if cv.b == 0 or (cv.u**2-4*cv.w) <= 0 else ((cv.b*np.sqrt(cv.u**2-4*cv.w))*(diff_ac_wrt2*(ov.bp_final1*cv.diff_alpha_wrt_temp-cv.alpha_eos)+cv.ac*(ov.bp_final1*diff_diff_alpha_wrt2-diff_alpha_wrt2))-(diff_b_wrt2*np.sqrt(cv.u**2-4*cv.w)+cv.b*0.5*(2*cv.u*diff_u_wrt2-4*diff_w_wrt2)/np.sqrt(cv.u**2-4*cv.w))*(cv.ac*(ov.bp_final1*cv.diff_alpha_wrt_temp-cv.alpha_eos)))/(cv.b*np.sqrt(cv.u**2-4*cv.w))**2

        diff_t3_wrt1 = 0 if (cv.ZV+cv.B*cv.r2) == 0 or (cv.ZV+cv.B*cv.r1) == 0 or (cv.ZV+cv.B*cv.r2)*(cv.ZV+cv.B*cv.r1) < 0 else (((cv.ZV+cv.B*cv.r1)/(cv.ZV+cv.B*cv.r2))*((cv.ZV+cv.B*cv.r1)*(diff_ZV_wrt1+diff_B_wrt1*cv.r2+cv.B*diff_r2_wrt1)-(diff_ZV_wrt1+diff_B_wrt1*cv.r1+cv.B*diff_r1_wrt1)*(cv.ZV+cv.B*cv.r2))/(cv.ZV+cv.B*cv.r1)**2)
        diff_t3_wrt2 = 0 if (cv.ZV+cv.B*cv.r2) == 0 or (cv.ZV+cv.B*cv.r1) == 0 or (cv.ZV+cv.B*cv.r2)*(cv.ZV+cv.B*cv.r1) < 0 else (((cv.ZV+cv.B*cv.r1)/(cv.ZV+cv.B*cv.r2))*((cv.ZV+cv.B*cv.r1)*(diff_ZV_wrt2+diff_B_wrt2*cv.r2+cv.B*diff_r2_wrt2)-(diff_ZV_wrt2+diff_B_wrt2*cv.r1+cv.B*diff_r1_wrt2)*(cv.ZV+cv.B*cv.r2))/(cv.ZV+cv.B*cv.r1)**2)

        diff_t4_wrt1 = 0 if (cv.ZL+cv.B*cv.r2) == 0 or (cv.ZL+cv.B*cv.r1) == 0 or (cv.ZL+cv.B*cv.r2)*(cv.ZL+cv.B*cv.r1) < 0 else (((cv.ZL+cv.B*cv.r1)/(cv.ZL+cv.B*cv.r2))*((cv.ZL+cv.B*cv.r1)*(diff_ZL_wrt1+diff_B_wrt1*cv.r2+cv.B*diff_r2_wrt1)-(diff_ZL_wrt1+diff_B_wrt1*cv.r1+cv.B*diff_r1_wrt1)*(cv.ZL+cv.B*cv.r2))/(cv.ZL+cv.B*cv.r1)**2)
        diff_t4_wrt2 = 0 if (cv.ZL+cv.B*cv.r2) == 0 or (cv.ZL+cv.B*cv.r1) == 0 or (cv.ZL+cv.B*cv.r2)*(cv.ZL+cv.B*cv.r1) < 0 else (((cv.ZL+cv.B*cv.r1)/(cv.ZL+cv.B*cv.r2))*((cv.ZL+cv.B*cv.r1)*(diff_ZL_wrt2+diff_B_wrt2*cv.r2+cv.B*diff_r2_wrt2)-(diff_ZL_wrt2+diff_B_wrt2*cv.r1+cv.B*diff_r1_wrt2)*(cv.ZL+cv.B*cv.r2))/(cv.ZL+cv.B*cv.r1)**2)

        diff_hv_bp_wrt1 = (diff_t1_wrt1+diff_t2_wrt1*(cv.t3-cv.t4)+cv.t2*(diff_t3_wrt1-diff_t4_wrt1))/1000.
        diff_hv_bp_wrt2 = (diff_t1_wrt2+diff_t2_wrt2*(cv.t3-cv.t4)+cv.t2*(diff_t3_wrt2-diff_t4_wrt2))/1000.

        diff_vb_wrt1 = diff_ZL_wrt1*gasConstant*ov.bp_final1/pressure
        diff_vb_wrt2 = diff_ZL_wrt2*gasConstant*ov.bp_final1/pressure

        diff_wrt1 = self.scales[0]*(2*(ov.hv_bp-cv.hv_bp)/ov.hv_bp**2)*(-1.)*diff_hv_bp_wrt1+self.scales[1]*(2*(ov.vb-cv.vb)/ov.vb**2)*(-1)*diff_vb_wrt1+self.scales[2]*2*(max(0,(cv.ph_diff-10**-7)))*(0 if(cv.ph_diff-10**-7)<=0 else diff_ph_diff_wrt1)
        diff_wrt2 = self.scales[0]*(2*(ov.hv_bp-cv.hv_bp)/ov.hv_bp**2)*(-1.)*diff_hv_bp_wrt2+self.scales[1]*(2*(ov.vb-cv.vb)/ov.vb**2)*(-1)*diff_vb_wrt2+self.scales[2]*2*(max(0,(cv.ph_diff-10**-7)))*(0 if(cv.ph_diff-10**-7)<=0 else diff_ph_diff_wrt2)

        diff_wrt1 = (np.sign(diff_wrt1)*10**7) if np.abs(diff_wrt1)>10**7 else diff_wrt1
        diff_wrt2 = (np.sign(diff_wrt2)*10**7) if np.abs(diff_wrt2)>10**7 else diff_wrt2    
        return np.array([diff_wrt1, diff_wrt2])

    def poas_4a(self, ov, cv):
        diff_alpha_wrt1 = ov.alpha_pr
        diff_alpha_wrt2 = 0
        
        diff_diff_alpha_wrt1 = ov.alpha_pr*(((2*ov.alpha_m-2)/(ov.bp_final1/ov.tc_k)+ov.alpha_l*(-2*ov.alpha_m)*(ov.bp_final1/ov.tc_k)**(2*ov.alpha_m-1))/ov.tc_k)
        diff_diff_alpha_wrt2 = 0

        diff_beta_wrt1 = 0
        diff_beta_wrt2 = ov.beta_est

        diff_om_b_wrt1 = 0
        diff_om_b_wrt2 = 0 if (ov.zc_final - cv.beta) < ov.om_b_min else -1.*diff_beta_wrt2

        diff_om_c_wrt1 = 0
        diff_om_c_wrt2 = (-2. * (1. - cv.beta) * diff_beta_wrt2 * (cv.beta - 0.25) + (1. - cv.beta)**2 * diff_beta_wrt2)

        diff_om_d_wrt1 = 0
        diff_om_d_wrt2 = 0.5 * diff_beta_wrt2

        diff_om_a_wrt1 = -3. * diff_beta_wrt1 * (1. - cv.beta)**2
        diff_om_a_wrt2 = -3. * diff_beta_wrt2 * (1. - cv.beta)**2

        diff_b_wrt1 = diff_om_b_wrt1*gasConstant*ov.tc_k/(ov.pc_mpa*10**6)
        diff_b_wrt2 = diff_om_b_wrt2*gasConstant*ov.tc_k/(ov.pc_mpa*10**6)

        diff_c_wrt1 = diff_om_c_wrt1*(gasConstant*ov.tc_k/(ov.pc_mpa*10**6))**2
        diff_c_wrt2 = diff_om_c_wrt2*(gasConstant*ov.tc_k/(ov.pc_mpa*10**6))**2

        diff_d_wrt1 = diff_om_d_wrt1*gasConstant*ov.tc_k/(ov.pc_mpa*10**6)
        diff_d_wrt2 = diff_om_d_wrt2*gasConstant*ov.tc_k/(ov.pc_mpa*10**6)

        diff_ac_wrt1 = 0
        diff_ac_wrt2 = diff_om_a_wrt2*gasConstant**2*ov.tc_k**2/(ov.pc_mpa*10**6)

        diff_ac_alpha_wrt1 = (cv.ac*diff_alpha_wrt1+diff_ac_wrt1*cv.alpha_eos)
        diff_ac_alpha_wrt2 = (cv.ac*diff_alpha_wrt2+diff_ac_wrt2*cv.alpha_eos)

        diff_u_wrt1 = 0 if cv.b == 0 else ((-2. * (diff_d_wrt1 * cv.b - cv.d * diff_b_wrt1))/cv.b**2)
        diff_u_wrt2 = 0 if cv.b == 0 else ((-2. * (diff_d_wrt2 * cv.b - cv.d * diff_b_wrt2))/cv.b**2)

        diff_w_wrt1 = 0 if cv.b == 0 else (((2. * diff_d_wrt1 * cv.d + diff_c_wrt1) * cv.b**2 -(cv.d**2 + cv.c) * 2. * diff_b_wrt1 * cv.b)/cv.b**4)
        diff_w_wrt2 = 0 if cv.b == 0 else (((2. * diff_d_wrt2 * cv.d + diff_c_wrt2) * cv.b**2 -(cv.d**2 + cv.c) * 2. * diff_b_wrt2 * cv.b)/cv.b**4)

        diff_r2_wrt1 = 0
        diff_r2_wrt2 = 0 if (cv.u**2-4*cv.w) <= 0 else (diff_u_wrt2+0.5*(2*cv.u*diff_u_wrt2-4*diff_w_wrt2)*(cv.u**2-4*cv.w)**-0.5)/2

        diff_r1_wrt1 = 0
        diff_r1_wrt2 = 0 if (cv.u**2-4*cv.w) <= 0 else (diff_u_wrt2-0.5*(2*cv.u*diff_u_wrt2-4*diff_w_wrt2)*(cv.u**2-4*cv.w)**-0.5)/2              

        diff_A_wrt1 = diff_ac_alpha_wrt1*pressure*10**6/(gasConstant**2*ov.bp_final1**2)
        diff_A_wrt2 = diff_ac_alpha_wrt2*pressure*10**6/(gasConstant**2*ov.bp_final1**2)

        diff_B_wrt1 = 0
        diff_B_wrt2 = diff_b_wrt2*pressure*10**6/(gasConstant*ov.bp_final1)   

        diff_a1_wrt1 = 0
        diff_a1_wrt2 = ((cv.u*diff_B_wrt2+diff_u_wrt2*cv.B)-diff_B_wrt2) 

        diff_a2_wrt1 = diff_A_wrt1+(cv.w*2*cv.B*diff_B_wrt1+diff_w_wrt1*cv.B**2)-(cv.u*diff_B_wrt1+diff_u_wrt1*cv.B)-(cv.u*2*cv.B*diff_B_wrt1+diff_u_wrt1*cv.B**2)
        diff_a2_wrt2 = diff_A_wrt2+(cv.w*2*cv.B*diff_B_wrt2+diff_w_wrt2*cv.B**2)-(cv.u*diff_B_wrt2+diff_u_wrt2*cv.B)-(cv.u*2*cv.B*diff_B_wrt2+diff_u_wrt2*cv.B**2)

        diff_a3_wrt1 = -1*((cv.A*diff_B_wrt1+diff_A_wrt1*cv.B)+(cv.w*2*cv.B*diff_B_wrt1+diff_w_wrt1*cv.B**2)+(cv.w*3*cv.B**2*diff_B_wrt1+diff_w_wrt1*cv.B**3))
        diff_a3_wrt2 = -1*((cv.A*diff_B_wrt2+diff_A_wrt2*cv.B)+(cv.w*2*cv.B*diff_B_wrt2+diff_w_wrt2*cv.B**2)+(cv.w*3*cv.B**2*diff_B_wrt2+diff_w_wrt2*cv.B**3))

        diff_Q_wrt1 = (3*diff_a2_wrt1-2*cv.a1*diff_a1_wrt1)/9.
        diff_Q_wrt2 = (3*diff_a2_wrt2-2*cv.a1*diff_a1_wrt2)/9.

        diff_L_wrt1 = (9*(cv.a1*diff_a2_wrt1+diff_a1_wrt1*cv.a2)-27*diff_a3_wrt1-2*3*diff_a1_wrt1*cv.a1**2)/54.
        diff_L_wrt2 = (9*(cv.a1*diff_a2_wrt2+diff_a1_wrt2*cv.a2)-27*diff_a3_wrt2-2*3*diff_a1_wrt2*cv.a1**2)/54.

        diff_D_wrt1 = 3*diff_Q_wrt1*cv.Q**2+2*diff_L_wrt1*cv.L
        diff_D_wrt2 = 3*diff_Q_wrt2*cv.Q**2+2*diff_L_wrt2*cv.L

        diff_S1_wrt1 = (-1*(1./3.)*(-1*(diff_L_wrt1+0.5*diff_D_wrt1/np.sqrt(cv.D)))*(-1*(cv.L+np.sqrt(cv.D)))**(1./3.-1)) if (cv.L+np.sqrt(cv.D))<0 else ((1./3.)*(diff_L_wrt1+0.5*diff_D_wrt1/np.sqrt(cv.D))*(cv.L+np.sqrt(cv.D))**(1./3.-1))
        diff_S1_wrt2 = (-1*(1./3.)*(-1*(diff_L_wrt2+0.5*diff_D_wrt2/np.sqrt(cv.D)))*(-1*(cv.L+np.sqrt(cv.D)))**(1./3.-1)) if (cv.L+np.sqrt(cv.D))<0 else ((1./3.)*(diff_L_wrt2+0.5*diff_D_wrt2/np.sqrt(cv.D))*(cv.L+np.sqrt(cv.D))**(1./3.-1))

        diff_S2_wrt1 = (-1*(1./3.)*(-1*(diff_L_wrt1-0.5*diff_D_wrt1/np.sqrt(cv.D)))*(-1*(cv.L-np.sqrt(cv.D)))**(1./3.-1)) if (cv.L-np.sqrt(cv.D))<0 else ((1./3.)*(diff_L_wrt1-0.5*diff_D_wrt1/np.sqrt(cv.D))*(cv.L-np.sqrt(cv.D))**(1./3.-1))
        diff_S2_wrt2 = (-1*(1./3.)*(-1*(diff_L_wrt2-0.5*diff_D_wrt2/np.sqrt(cv.D)))*(-1*(cv.L-np.sqrt(cv.D)))**(1./3.-1)) if (cv.L-np.sqrt(cv.D))<0 else ((1./3.)*(diff_L_wrt2-0.5*diff_D_wrt2/np.sqrt(cv.D))*(cv.L-np.sqrt(cv.D))**(1./3.-1))

        diff_Z1_d_wrt1 = diff_S1_wrt1+diff_S2_wrt1-diff_a1_wrt1/3
        diff_Z1_d_wrt2 = diff_S1_wrt2+diff_S2_wrt2-diff_a1_wrt2/3

        diff_Z1_dd_wrt1 = ((-1*(1./3.)*(-1*diff_L_wrt1)*(-1*cv.L)**(1./3.-1)) if cv.L<0 else (1./3.*diff_L_wrt1*cv.L**(1./3.-1))) *2-diff_a1_wrt1/3
        diff_Z1_dd_wrt2 = ((-1*(1./3.)*(-1*diff_L_wrt2)*(-1*cv.L)**(1./3.-1)) if cv.L<0 else (1./3.*diff_L_wrt2*cv.L**(1./3.-1))) *2-diff_a1_wrt2/3

        diff_Z2_dd_wrt1 = (((-1*(1./3.)*(-1*diff_L_wrt1)*(-1*cv.L)**(1./3.-1)) if cv.L<0 else (1./3.*diff_L_wrt1*cv.L**(1./3.-1))) +diff_a1_wrt1/3) * -1.
        diff_Z2_dd_wrt2 = (((-1*(1./3.)*(-1*diff_L_wrt2)*(-1*cv.L)**(1./3.-1)) if cv.L<0 else (1./3.*diff_L_wrt2*cv.L**(1./3.-1))) +diff_a1_wrt2/3) * -1.
        
        diff_Z3_dd_wrt1 = diff_Z2_dd_wrt1
        diff_Z3_dd_wrt2 = diff_Z2_dd_wrt2

        diff_Z1_wrt1 = 0 if cv.D>=0 or cv.Q>=0 else (2*(-0.5*diff_Q_wrt1/np.sqrt(-1*cv.Q)*np.cos(1./3.*(np.arccos( 1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3) ))+2*np.pi/3)+np.sqrt(-1*cv.Q)*(-1)*(1./3.*-1/np.sqrt(1-(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3))**2))*((np.sqrt(-cv.Q**3)*diff_L_wrt1-(0.5*-1*3*diff_Q_wrt1*cv.Q**2)/np.sqrt(-cv.Q**3)*cv.L)/(-cv.Q**3))*np.sin(1./3.*(np.arccos(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3)))+2*np.pi/3))-diff_a1_wrt1/3)
        diff_Z1_wrt2 = 0 if cv.D>=0 or cv.Q>=0 else (2*(-0.5*diff_Q_wrt2/np.sqrt(-1*cv.Q)*np.cos(1./3.*(np.arccos( 1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3) ))+2*np.pi/3)+np.sqrt(-1*cv.Q)*(-1)*(1./3.*-1/np.sqrt(1-(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3))**2))*((np.sqrt(-cv.Q**3)*diff_L_wrt2-(0.5*-1*3*diff_Q_wrt2*cv.Q**2)/np.sqrt(-cv.Q**3)*cv.L)/(-cv.Q**3))*np.sin(1./3.*(np.arccos(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3)))+2*np.pi/3))-diff_a1_wrt2/3)

        diff_Z2_wrt1 = 0 if cv.D>=0 or cv.Q>=0 else (2*(-0.5*diff_Q_wrt1/np.sqrt(-1*cv.Q)*np.cos(1./3.*(np.arccos( 1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3) ))+4*np.pi/3)+np.sqrt(-1*cv.Q)*(-1)*(1./3.*-1/np.sqrt(1-(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3))**2))*((np.sqrt(-cv.Q**3)*diff_L_wrt1-(0.5*-1*3*diff_Q_wrt1*cv.Q**2)/np.sqrt(-cv.Q**3)*cv.L)/(-cv.Q**3))*np.sin(1./3.*(np.arccos(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3)))+4*np.pi/3))-diff_a1_wrt1/3)
        diff_Z2_wrt2 = 0 if cv.D>=0 or cv.Q>=0 else (2*(-0.5*diff_Q_wrt2/np.sqrt(-1*cv.Q)*np.cos(1./3.*(np.arccos( 1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3) ))+4*np.pi/3)+np.sqrt(-1*cv.Q)*(-1)*(1./3.*-1/np.sqrt(1-(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3))**2))*((np.sqrt(-cv.Q**3)*diff_L_wrt2-(0.5*-1*3*diff_Q_wrt2*cv.Q**2)/np.sqrt(-cv.Q**3)*cv.L)/(-cv.Q**3))*np.sin(1./3.*(np.arccos(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3)))+4*np.pi/3))-diff_a1_wrt2/3)

        diff_Z3_wrt1 = 0 if cv.D>=0 or cv.Q>=0 else (2*(-0.5*diff_Q_wrt1/np.sqrt(-1*cv.Q)*np.cos(1./3.*(np.arccos( 1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3) )))+np.sqrt(-1*cv.Q)*(-1)*(1./3.*-1/np.sqrt(1-(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3))**2))*((np.sqrt(-cv.Q**3)*diff_L_wrt1-(0.5*-1*3*diff_Q_wrt1*cv.Q**2)/np.sqrt(-cv.Q**3)*cv.L)/(-cv.Q**3))*np.sin(1./3.*(np.arccos(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3)))))-diff_a1_wrt1/3)
        diff_Z3_wrt2 = 0 if cv.D>=0 or cv.Q>=0 else (2*(-0.5*diff_Q_wrt2/np.sqrt(-1*cv.Q)*np.cos(1./3.*(np.arccos( 1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3) )))+np.sqrt(-1*cv.Q)*(-1)*(1./3.*-1/np.sqrt(1-(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3))**2))*((np.sqrt(-cv.Q**3)*diff_L_wrt2-(0.5*-1*3*diff_Q_wrt2*cv.Q**2)/np.sqrt(-cv.Q**3)*cv.L)/(-cv.Q**3))*np.sin(1./3.*(np.arccos(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3)))))-diff_a1_wrt2/3)

        diff_ZL_wrt1 = (diff_Z1_wrt1 if cv.ZL==cv.Z1 else (diff_Z2_wrt1 if cv.ZL==cv.Z2 else  diff_Z3_wrt1)) if cv.D<0 else ((diff_Z1_dd_wrt1 if cv.ZL==cv.Z1_dd else (diff_Z2_dd_wrt1 if cv.ZL==cv.Z2_dd else diff_Z3_dd_wrt1)) if cv.D==0 else diff_Z1_d_wrt1)
        diff_ZL_wrt2 = (diff_Z1_wrt2 if cv.ZL==cv.Z1 else (diff_Z2_wrt2 if cv.ZL==cv.Z2 else  diff_Z3_wrt2)) if cv.D<0 else ((diff_Z1_dd_wrt2 if cv.ZL==cv.Z1_dd else (diff_Z2_dd_wrt2 if cv.ZL==cv.Z2_dd else diff_Z3_dd_wrt2)) if cv.D==0 else diff_Z1_d_wrt2)

        diff_ZV_wrt1 = (diff_Z1_wrt1 if cv.ZV==cv.Z1 else (diff_Z2_wrt1 if cv.ZV==cv.Z2 else  diff_Z3_wrt1)) if cv.D<0 else ((diff_Z1_dd_wrt1 if cv.ZV==cv.Z1_dd else (diff_Z2_dd_wrt1 if cv.ZV==cv.Z2_dd else diff_Z3_dd_wrt1)) if cv.D==0 else 0)
        diff_ZV_wrt2 = (diff_Z1_wrt2 if cv.ZV==cv.Z1 else (diff_Z2_wrt2 if cv.ZV==cv.Z2 else  diff_Z3_wrt2)) if cv.D<0 else ((diff_Z1_dd_wrt2 if cv.ZV==cv.Z1_dd else (diff_Z2_dd_wrt2 if cv.ZV==cv.Z2_dd else diff_Z3_dd_wrt2)) if cv.D==0 else 0)

        diff_phl_wrt1 = 0 if (cv.ZL-cv.B)<=0 or cv.B*(cv.r2 - cv.r1) == 0 or (cv.ZL + cv.r1 * cv.B) == 0 or (cv.ZL + cv.r2 * cv.B) == 0 or (cv.ZL + cv.r2 * cv.B) * (cv.ZL + cv.r1 * cv.B) < 0  else ((diff_ZL_wrt1)-(diff_ZL_wrt1-diff_B_wrt1)/(cv.ZL-cv.B)-((((cv.B*(cv.r2-cv.r1))*diff_A_wrt1-(diff_B_wrt1*(cv.r2-cv.r1)+cv.B*(diff_r2_wrt1-diff_r1_wrt1))*cv.A)/(cv.B*(cv.r2-cv.r1))**2)*np.log((cv.ZL+cv.r2*cv.B)/(cv.ZL+cv.r1*cv.B))+(cv.A/(cv.B*(cv.r2-cv.r1)))*1/((cv.ZL+cv.r2*cv.B)/(cv.ZL+cv.r1*cv.B))*(((cv.ZL+cv.r1*cv.B)*(diff_ZL_wrt1+diff_r2_wrt1*cv.B+cv.r2*diff_B_wrt1)-(cv.ZL+cv.r2*cv.B)*(diff_ZL_wrt1+diff_r1_wrt1*cv.B+cv.r1*diff_B_wrt1))/(cv.ZL+cv.r1*cv.B)**2)))
        diff_phl_wrt2 = 0 if (cv.ZL-cv.B)<=0 or cv.B*(cv.r2 - cv.r1) == 0 or (cv.ZL + cv.r1 * cv.B) == 0 or (cv.ZL + cv.r2 * cv.B) == 0 or (cv.ZL + cv.r2 * cv.B) * (cv.ZL + cv.r1 * cv.B) < 0 else ((diff_ZL_wrt2)-(diff_ZL_wrt2-diff_B_wrt2)/(cv.ZL-cv.B)-((((cv.B*(cv.r2-cv.r1))*diff_A_wrt2-(diff_B_wrt2*(cv.r2-cv.r1)+cv.B*(diff_r2_wrt2-diff_r1_wrt2))*cv.A)/(cv.B*(cv.r2-cv.r1))**2)*np.log((cv.ZL+cv.r2*cv.B)/(cv.ZL+cv.r1*cv.B))+(cv.A/(cv.B*(cv.r2-cv.r1)))*1/((cv.ZL+cv.r2*cv.B)/(cv.ZL+cv.r1*cv.B))*(((cv.ZL+cv.r1*cv.B)*(diff_ZL_wrt2+diff_r2_wrt2*cv.B+cv.r2*diff_B_wrt2)-(cv.ZL+cv.r2*cv.B)*(diff_ZL_wrt2+diff_r1_wrt2*cv.B+cv.r1*diff_B_wrt2))/(cv.ZL+cv.r1*cv.B)**2)))

        diff_phv_wrt1 = 0 if (cv.ZV-cv.B)<=0 or cv.B*(cv.r2 - cv.r1) == 0 or (cv.ZV + cv.r1 * cv.B) == 0 or (cv.ZV + cv.r2 * cv.B) == 0 or (cv.ZV + cv.r2 * cv.B) * (cv.ZV + cv.r1 * cv.B) < 0 else ((diff_ZV_wrt1)-(diff_ZV_wrt1-diff_B_wrt1)/(cv.ZV-cv.B)-((((cv.B*(cv.r2-cv.r1))*diff_A_wrt1-(diff_B_wrt1*(cv.r2-cv.r1)+cv.B*(diff_r2_wrt1-diff_r1_wrt1))*cv.A)/(cv.B*(cv.r2-cv.r1))**2)*np.log((cv.ZV+cv.r2*cv.B)/(cv.ZV+cv.r1*cv.B))+(cv.A/(cv.B*(cv.r2-cv.r1)))*1/((cv.ZV+cv.r2*cv.B)/(cv.ZV+cv.r1*cv.B))*(((cv.ZV+cv.r1*cv.B)*(diff_ZV_wrt1+diff_r2_wrt1*cv.B+cv.r2*diff_B_wrt1)-(cv.ZV+cv.r2*cv.B)*(diff_ZV_wrt1+diff_r1_wrt1*cv.B+cv.r1*diff_B_wrt1))/(cv.ZV+cv.r1*cv.B)**2)))
        diff_phv_wrt2 = 0 if (cv.ZV-cv.B)<=0 or cv.B*(cv.r2 - cv.r1) == 0 or (cv.ZV + cv.r1 * cv.B) == 0 or (cv.ZV + cv.r2 * cv.B) == 0 or (cv.ZV + cv.r2 * cv.B) * (cv.ZV + cv.r1 * cv.B) < 0 else ((diff_ZV_wrt2)-(diff_ZV_wrt2-diff_B_wrt2)/(cv.ZV-cv.B)-((((cv.B*(cv.r2-cv.r1))*diff_A_wrt2-(diff_B_wrt2*(cv.r2-cv.r1)+cv.B*(diff_r2_wrt2-diff_r1_wrt2))*cv.A)/(cv.B*(cv.r2-cv.r1))**2)*np.log((cv.ZV+cv.r2*cv.B)/(cv.ZV+cv.r1*cv.B))+(cv.A/(cv.B*(cv.r2-cv.r1)))*1/((cv.ZV+cv.r2*cv.B)/(cv.ZV+cv.r1*cv.B))*(((cv.ZV+cv.r1*cv.B)*(diff_ZV_wrt2+diff_r2_wrt2*cv.B+cv.r2*diff_B_wrt2)-(cv.ZV+cv.r2*cv.B)*(diff_ZV_wrt2+diff_r1_wrt2*cv.B+cv.r1*diff_B_wrt2))/(cv.ZV+cv.r1*cv.B)**2)))

        diff_ph_diff_wrt1 = np.sign(cv.phl-cv.phv)*(diff_phl_wrt1-diff_phv_wrt1)
        diff_ph_diff_wrt2 = np.sign(cv.phl-cv.phv)*(diff_phl_wrt2-diff_phv_wrt2)

        diff_t1_wrt1 = gasConstant*ov.bp_final1*(diff_ZV_wrt1-diff_ZL_wrt1)
        diff_t1_wrt2 = gasConstant*ov.bp_final1*(diff_ZV_wrt2-diff_ZL_wrt2)

        diff_t2_wrt1 = 0 if cv.b == 0 or (cv.u**2-4*cv.w) <= 0 else ((cv.b*np.sqrt(cv.u**2-4*cv.w))*(diff_ac_wrt1*(ov.bp_final1*cv.diff_alpha_wrt_temp-cv.alpha_eos)+cv.ac*(ov.bp_final1*diff_diff_alpha_wrt1-diff_alpha_wrt1))-(diff_b_wrt1*np.sqrt(cv.u**2-4*cv.w)+cv.b*0.5*(2*cv.u*diff_u_wrt1-4*diff_w_wrt1)/np.sqrt(cv.u**2-4*cv.w))*(cv.ac*(ov.bp_final1*cv.diff_alpha_wrt_temp-cv.alpha_eos)))/(cv.b*np.sqrt(cv.u**2-4*cv.w))**2
        diff_t2_wrt2 = 0 if cv.b == 0 or (cv.u**2-4*cv.w) <= 0 else ((cv.b*np.sqrt(cv.u**2-4*cv.w))*(diff_ac_wrt2*(ov.bp_final1*cv.diff_alpha_wrt_temp-cv.alpha_eos)+cv.ac*(ov.bp_final1*diff_diff_alpha_wrt2-diff_alpha_wrt2))-(diff_b_wrt2*np.sqrt(cv.u**2-4*cv.w)+cv.b*0.5*(2*cv.u*diff_u_wrt2-4*diff_w_wrt2)/np.sqrt(cv.u**2-4*cv.w))*(cv.ac*(ov.bp_final1*cv.diff_alpha_wrt_temp-cv.alpha_eos)))/(cv.b*np.sqrt(cv.u**2-4*cv.w))**2

        diff_t3_wrt1 = 0 if (cv.ZV+cv.B*cv.r2) == 0 or (cv.ZV+cv.B*cv.r1) == 0 or (cv.ZV+cv.B*cv.r2)*(cv.ZV+cv.B*cv.r1) < 0 else (((cv.ZV+cv.B*cv.r1)/(cv.ZV+cv.B*cv.r2))*((cv.ZV+cv.B*cv.r1)*(diff_ZV_wrt1+diff_B_wrt1*cv.r2+cv.B*diff_r2_wrt1)-(diff_ZV_wrt1+diff_B_wrt1*cv.r1+cv.B*diff_r1_wrt1)*(cv.ZV+cv.B*cv.r2))/(cv.ZV+cv.B*cv.r1)**2)
        diff_t3_wrt2 = 0 if (cv.ZV+cv.B*cv.r2) == 0 or (cv.ZV+cv.B*cv.r1) == 0 or (cv.ZV+cv.B*cv.r2)*(cv.ZV+cv.B*cv.r1) < 0 else (((cv.ZV+cv.B*cv.r1)/(cv.ZV+cv.B*cv.r2))*((cv.ZV+cv.B*cv.r1)*(diff_ZV_wrt2+diff_B_wrt2*cv.r2+cv.B*diff_r2_wrt2)-(diff_ZV_wrt2+diff_B_wrt2*cv.r1+cv.B*diff_r1_wrt2)*(cv.ZV+cv.B*cv.r2))/(cv.ZV+cv.B*cv.r1)**2)

        diff_t4_wrt1 = 0 if (cv.ZL+cv.B*cv.r2) == 0 or (cv.ZL+cv.B*cv.r1) == 0 or (cv.ZL+cv.B*cv.r2)*(cv.ZL+cv.B*cv.r1) < 0 else (((cv.ZL+cv.B*cv.r1)/(cv.ZL+cv.B*cv.r2))*((cv.ZL+cv.B*cv.r1)*(diff_ZL_wrt1+diff_B_wrt1*cv.r2+cv.B*diff_r2_wrt1)-(diff_ZL_wrt1+diff_B_wrt1*cv.r1+cv.B*diff_r1_wrt1)*(cv.ZL+cv.B*cv.r2))/(cv.ZL+cv.B*cv.r1)**2)
        diff_t4_wrt2 = 0 if (cv.ZL+cv.B*cv.r2) == 0 or (cv.ZL+cv.B*cv.r1) == 0 or (cv.ZL+cv.B*cv.r2)*(cv.ZL+cv.B*cv.r1) < 0 else (((cv.ZL+cv.B*cv.r1)/(cv.ZL+cv.B*cv.r2))*((cv.ZL+cv.B*cv.r1)*(diff_ZL_wrt2+diff_B_wrt2*cv.r2+cv.B*diff_r2_wrt2)-(diff_ZL_wrt2+diff_B_wrt2*cv.r1+cv.B*diff_r1_wrt2)*(cv.ZL+cv.B*cv.r2))/(cv.ZL+cv.B*cv.r1)**2)

        diff_hv_bp_wrt1 = (diff_t1_wrt1+diff_t2_wrt1*(cv.t3-cv.t4)+cv.t2*(diff_t3_wrt1-diff_t4_wrt1))/1000.
        diff_hv_bp_wrt2 = (diff_t1_wrt2+diff_t2_wrt2*(cv.t3-cv.t4)+cv.t2*(diff_t3_wrt2-diff_t4_wrt2))/1000.

        diff_vb_wrt1 = diff_ZL_wrt1*gasConstant*ov.bp_final1/pressure
        diff_vb_wrt2 = diff_ZL_wrt2*gasConstant*ov.bp_final1/pressure

        diff_wrt1 = self.scales[0]*(2*(ov.hv_bp-cv.hv_bp)/ov.hv_bp**2)*(-1.)*diff_hv_bp_wrt1+self.scales[1]*(2*(ov.vb-cv.vb)/ov.vb**2)*(-1)*diff_vb_wrt1+self.scales[2]*2*(max(0,(cv.ph_diff-10**-7)))*(0 if(cv.ph_diff-10**-7)<=0 else diff_ph_diff_wrt1)
        diff_wrt2 = self.scales[0]*(2*(ov.hv_bp-cv.hv_bp)/ov.hv_bp**2)*(-1.)*diff_hv_bp_wrt2+self.scales[1]*(2*(ov.vb-cv.vb)/ov.vb**2)*(-1)*diff_vb_wrt2+self.scales[2]*2*(max(0,(cv.ph_diff-10**-7)))*(0 if(cv.ph_diff-10**-7)<=0 else diff_ph_diff_wrt2)

        diff_wrt1 = (np.sign(diff_wrt1)*10**7) if np.abs(diff_wrt1)>10**7 else diff_wrt1
        diff_wrt2 = (np.sign(diff_wrt2)*10**7) if np.abs(diff_wrt2)>10**7 else diff_wrt2    
        return np.array([diff_wrt1, diff_wrt2])

    def generic_poas_a(self, ov, cv):
        diff_alpha_wrt1 = ov.alpha_pr
        diff_alpha_wrt2 = 0
        
        diff_diff_alpha_wrt1 = ov.alpha_pr*(((2*ov.alpha_m-2)/(ov.bp_final1/ov.tc_k)+ov.alpha_l*(-2*ov.alpha_m)*(ov.bp_final1/ov.tc_k)**(2*ov.alpha_m-1))/ov.tc_k)
        diff_diff_alpha_wrt2 = 0

        diff_om_b_wrt1 = 0
        diff_om_b_wrt2 = (0.9129*ov.zc_final**2-0.0551*ov.zc_final+0.0368) if ((0.9129*ov.zc_final**2-0.0551*ov.zc_final+0.0368)*ov.om_b_multiplier)<ov.om_b else 0

        diff_b_wrt1 = 0
        diff_b_wrt2 = diff_om_b_wrt2*gasConstant*ov.tc_k/(ov.pc_mpa*10**6)

        diff_pf_wrt1 = diff_b_wrt1/ov.vc_final*10**6
        diff_pf_wrt2 = diff_b_wrt2/ov.vc_final*10**6

        diff_u_wrt1 = 0 if cv.pf == 0 else ((-1. * diff_pf_wrt1)/cv.pf**2 * (1./ov.zc_final-3.))
        diff_u_wrt2 = 0 if cv.pf == 0 else ((-1. * diff_pf_wrt2)/cv.pf**2 * (1./ov.zc_final-3.))

        diff_w_wrt1 = 0 if ((cv.u-1)*cv.pf**3+3*cv.pf**2)==0 else (((-3*diff_pf_wrt1-(2*diff_u_wrt1*cv.u*cv.pf**3+cv.u**2*3*diff_pf_wrt1*cv.pf**2)-3*(diff_u_wrt1*cv.pf**2+cv.u*2*diff_pf_wrt1*cv.pf))*((cv.u-1)*cv.pf**3+3*cv.pf**2)-(1-3*cv.pf-cv.u**2*cv.pf**3-3*cv.u*cv.pf**2)*((diff_u_wrt1*cv.pf**3+(cv.u-1)*3*diff_pf_wrt1*cv.pf**2)+3*2*diff_pf_wrt1*cv.pf))/((cv.u-1)*cv.pf**3+3*cv.pf**2)**2)
        diff_w_wrt2 = 0 if ((cv.u-1)*cv.pf**3+3*cv.pf**2)==0 else (((-3*diff_pf_wrt2-(2*diff_u_wrt2*cv.u*cv.pf**3+cv.u**2*3*diff_pf_wrt2*cv.pf**2)-3*(diff_u_wrt2*cv.pf**2+cv.u*2*diff_pf_wrt2*cv.pf))*((cv.u-1)*cv.pf**3+3*cv.pf**2)-(1-3*cv.pf-cv.u**2*cv.pf**3-3*cv.u*cv.pf**2)*((diff_u_wrt2*cv.pf**3+(cv.u-1)*3*diff_pf_wrt2*cv.pf**2)+3*2*diff_pf_wrt2*cv.pf))/((cv.u-1)*cv.pf**3+3*cv.pf**2)**2)

        diff_om_a_wrt1 = -1.*(diff_w_wrt1*cv.om_b**2+cv.w*2*diff_om_b_wrt1*cv.om_b)+(diff_u_wrt1*(cv.om_b+cv.om_b**2)+cv.u*(diff_om_b_wrt1+2*diff_om_b_wrt1*cv.om_b))
        diff_om_a_wrt2 = -1.*(diff_w_wrt2*cv.om_b**2+cv.w*2*diff_om_b_wrt2*cv.om_b)+(diff_u_wrt2*(cv.om_b+cv.om_b**2)+cv.u*(diff_om_b_wrt2+2*diff_om_b_wrt2*cv.om_b))

        diff_ac_wrt1 = 0
        diff_ac_wrt2 = diff_om_a_wrt2*gasConstant**2*ov.tc_k**2/(ov.pc_mpa*10**6)

        diff_ac_alpha_wrt1 = (cv.ac*diff_alpha_wrt1+diff_ac_wrt1*cv.alpha_eos)
        diff_ac_alpha_wrt2 = (cv.ac*diff_alpha_wrt2+diff_ac_wrt2*cv.alpha_eos)

        diff_r2_wrt1 = 0
        diff_r2_wrt2 = 0 if (cv.u**2-4*cv.w) <= 0 else (diff_u_wrt2+0.5*(2*cv.u*diff_u_wrt2-4*diff_w_wrt2)*(cv.u**2-4*cv.w)**-0.5)/2

        diff_r1_wrt1 = 0
        diff_r1_wrt2 = 0 if (cv.u**2-4*cv.w) <= 0 else (diff_u_wrt2-0.5*(2*cv.u*diff_u_wrt2-4*diff_w_wrt2)*(cv.u**2-4*cv.w)**-0.5)/2              

        diff_A_wrt1 = diff_ac_alpha_wrt1*pressure*10**6/(gasConstant**2*ov.bp_final1**2)
        diff_A_wrt2 = diff_ac_alpha_wrt2*pressure*10**6/(gasConstant**2*ov.bp_final1**2)

        diff_B_wrt1 = 0
        diff_B_wrt2 = diff_b_wrt2*pressure*10**6/(gasConstant*ov.bp_final1)   

        diff_a1_wrt1 = 0
        diff_a1_wrt2 = ((cv.u*diff_B_wrt2+diff_u_wrt2*cv.B)-diff_B_wrt2) 

        diff_a2_wrt1 = diff_A_wrt1+(cv.w*2*cv.B*diff_B_wrt1+diff_w_wrt1*cv.B**2)-(cv.u*diff_B_wrt1+diff_u_wrt1*cv.B)-(cv.u*2*cv.B*diff_B_wrt1+diff_u_wrt1*cv.B**2)
        diff_a2_wrt2 = diff_A_wrt2+(cv.w*2*cv.B*diff_B_wrt2+diff_w_wrt2*cv.B**2)-(cv.u*diff_B_wrt2+diff_u_wrt2*cv.B)-(cv.u*2*cv.B*diff_B_wrt2+diff_u_wrt2*cv.B**2)

        diff_a3_wrt1 = -1*((cv.A*diff_B_wrt1+diff_A_wrt1*cv.B)+(cv.w*2*cv.B*diff_B_wrt1+diff_w_wrt1*cv.B**2)+(cv.w*3*cv.B**2*diff_B_wrt1+diff_w_wrt1*cv.B**3))
        diff_a3_wrt2 = -1*((cv.A*diff_B_wrt2+diff_A_wrt2*cv.B)+(cv.w*2*cv.B*diff_B_wrt2+diff_w_wrt2*cv.B**2)+(cv.w*3*cv.B**2*diff_B_wrt2+diff_w_wrt2*cv.B**3))

        diff_Q_wrt1 = (3*diff_a2_wrt1-2*cv.a1*diff_a1_wrt1)/9.
        diff_Q_wrt2 = (3*diff_a2_wrt2-2*cv.a1*diff_a1_wrt2)/9.

        diff_L_wrt1 = (9*(cv.a1*diff_a2_wrt1+diff_a1_wrt1*cv.a2)-27*diff_a3_wrt1-2*3*diff_a1_wrt1*cv.a1**2)/54.
        diff_L_wrt2 = (9*(cv.a1*diff_a2_wrt2+diff_a1_wrt2*cv.a2)-27*diff_a3_wrt2-2*3*diff_a1_wrt2*cv.a1**2)/54.

        diff_D_wrt1 = 3*diff_Q_wrt1*cv.Q**2+2*diff_L_wrt1*cv.L
        diff_D_wrt2 = 3*diff_Q_wrt2*cv.Q**2+2*diff_L_wrt2*cv.L

        diff_S1_wrt1 = (-1*(1./3.)*(-1*(diff_L_wrt1+0.5*diff_D_wrt1/np.sqrt(cv.D)))*(-1*(cv.L+np.sqrt(cv.D)))**(1./3.-1)) if (cv.L+np.sqrt(cv.D))<0 else ((1./3.)*(diff_L_wrt1+0.5*diff_D_wrt1/np.sqrt(cv.D))*(cv.L+np.sqrt(cv.D))**(1./3.-1))
        diff_S1_wrt2 = (-1*(1./3.)*(-1*(diff_L_wrt2+0.5*diff_D_wrt2/np.sqrt(cv.D)))*(-1*(cv.L+np.sqrt(cv.D)))**(1./3.-1)) if (cv.L+np.sqrt(cv.D))<0 else ((1./3.)*(diff_L_wrt2+0.5*diff_D_wrt2/np.sqrt(cv.D))*(cv.L+np.sqrt(cv.D))**(1./3.-1))

        diff_S2_wrt1 = (-1*(1./3.)*(-1*(diff_L_wrt1-0.5*diff_D_wrt1/np.sqrt(cv.D)))*(-1*(cv.L-np.sqrt(cv.D)))**(1./3.-1)) if (cv.L-np.sqrt(cv.D))<0 else ((1./3.)*(diff_L_wrt1-0.5*diff_D_wrt1/np.sqrt(cv.D))*(cv.L-np.sqrt(cv.D))**(1./3.-1))
        diff_S2_wrt2 = (-1*(1./3.)*(-1*(diff_L_wrt2-0.5*diff_D_wrt2/np.sqrt(cv.D)))*(-1*(cv.L-np.sqrt(cv.D)))**(1./3.-1)) if (cv.L-np.sqrt(cv.D))<0 else ((1./3.)*(diff_L_wrt2-0.5*diff_D_wrt2/np.sqrt(cv.D))*(cv.L-np.sqrt(cv.D))**(1./3.-1))

        diff_Z1_d_wrt1 = diff_S1_wrt1+diff_S2_wrt1-diff_a1_wrt1/3
        diff_Z1_d_wrt2 = diff_S1_wrt2+diff_S2_wrt2-diff_a1_wrt2/3

        diff_Z1_dd_wrt1 = ((-1*(1./3.)*(-1*diff_L_wrt1)*(-1*cv.L)**(1./3.-1)) if cv.L<0 else (1./3.*diff_L_wrt1*cv.L**(1./3.-1))) *2-diff_a1_wrt1/3
        diff_Z1_dd_wrt2 = ((-1*(1./3.)*(-1*diff_L_wrt2)*(-1*cv.L)**(1./3.-1)) if cv.L<0 else (1./3.*diff_L_wrt2*cv.L**(1./3.-1))) *2-diff_a1_wrt2/3

        diff_Z2_dd_wrt1 = (((-1*(1./3.)*(-1*diff_L_wrt1)*(-1*cv.L)**(1./3.-1)) if cv.L<0 else (1./3.*diff_L_wrt1*cv.L**(1./3.-1))) +diff_a1_wrt1/3) * -1.
        diff_Z2_dd_wrt2 = (((-1*(1./3.)*(-1*diff_L_wrt2)*(-1*cv.L)**(1./3.-1)) if cv.L<0 else (1./3.*diff_L_wrt2*cv.L**(1./3.-1))) +diff_a1_wrt2/3) * -1.
        
        diff_Z3_dd_wrt1 = diff_Z2_dd_wrt1
        diff_Z3_dd_wrt2 = diff_Z2_dd_wrt2

        diff_Z1_wrt1 = 0 if cv.D>=0 or cv.Q>=0 else (2*(-0.5*diff_Q_wrt1/np.sqrt(-1*cv.Q)*np.cos(1./3.*(np.arccos( 1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3) ))+2*np.pi/3)+np.sqrt(-1*cv.Q)*(-1)*(1./3.*-1/np.sqrt(1-(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3))**2))*((np.sqrt(-cv.Q**3)*diff_L_wrt1-(0.5*-1*3*diff_Q_wrt1*cv.Q**2)/np.sqrt(-cv.Q**3)*cv.L)/(-cv.Q**3))*np.sin(1./3.*(np.arccos(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3)))+2*np.pi/3))-diff_a1_wrt1/3)
        diff_Z1_wrt2 = 0 if cv.D>=0 or cv.Q>=0 else (2*(-0.5*diff_Q_wrt2/np.sqrt(-1*cv.Q)*np.cos(1./3.*(np.arccos( 1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3) ))+2*np.pi/3)+np.sqrt(-1*cv.Q)*(-1)*(1./3.*-1/np.sqrt(1-(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3))**2))*((np.sqrt(-cv.Q**3)*diff_L_wrt2-(0.5*-1*3*diff_Q_wrt2*cv.Q**2)/np.sqrt(-cv.Q**3)*cv.L)/(-cv.Q**3))*np.sin(1./3.*(np.arccos(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3)))+2*np.pi/3))-diff_a1_wrt2/3)

        diff_Z2_wrt1 = 0 if cv.D>=0 or cv.Q>=0 else (2*(-0.5*diff_Q_wrt1/np.sqrt(-1*cv.Q)*np.cos(1./3.*(np.arccos( 1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3) ))+4*np.pi/3)+np.sqrt(-1*cv.Q)*(-1)*(1./3.*-1/np.sqrt(1-(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3))**2))*((np.sqrt(-cv.Q**3)*diff_L_wrt1-(0.5*-1*3*diff_Q_wrt1*cv.Q**2)/np.sqrt(-cv.Q**3)*cv.L)/(-cv.Q**3))*np.sin(1./3.*(np.arccos(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3)))+4*np.pi/3))-diff_a1_wrt1/3)
        diff_Z2_wrt2 = 0 if cv.D>=0 or cv.Q>=0 else (2*(-0.5*diff_Q_wrt2/np.sqrt(-1*cv.Q)*np.cos(1./3.*(np.arccos( 1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3) ))+4*np.pi/3)+np.sqrt(-1*cv.Q)*(-1)*(1./3.*-1/np.sqrt(1-(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3))**2))*((np.sqrt(-cv.Q**3)*diff_L_wrt2-(0.5*-1*3*diff_Q_wrt2*cv.Q**2)/np.sqrt(-cv.Q**3)*cv.L)/(-cv.Q**3))*np.sin(1./3.*(np.arccos(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3)))+4*np.pi/3))-diff_a1_wrt2/3)

        diff_Z3_wrt1 = 0 if cv.D>=0 or cv.Q>=0 else (2*(-0.5*diff_Q_wrt1/np.sqrt(-1*cv.Q)*np.cos(1./3.*(np.arccos( 1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3) )))+np.sqrt(-1*cv.Q)*(-1)*(1./3.*-1/np.sqrt(1-(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3))**2))*((np.sqrt(-cv.Q**3)*diff_L_wrt1-(0.5*-1*3*diff_Q_wrt1*cv.Q**2)/np.sqrt(-cv.Q**3)*cv.L)/(-cv.Q**3))*np.sin(1./3.*(np.arccos(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3)))))-diff_a1_wrt1/3)
        diff_Z3_wrt2 = 0 if cv.D>=0 or cv.Q>=0 else (2*(-0.5*diff_Q_wrt2/np.sqrt(-1*cv.Q)*np.cos(1./3.*(np.arccos( 1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3) )))+np.sqrt(-1*cv.Q)*(-1)*(1./3.*-1/np.sqrt(1-(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3))**2))*((np.sqrt(-cv.Q**3)*diff_L_wrt2-(0.5*-1*3*diff_Q_wrt2*cv.Q**2)/np.sqrt(-cv.Q**3)*cv.L)/(-cv.Q**3))*np.sin(1./3.*(np.arccos(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3)))))-diff_a1_wrt2/3)

        diff_ZL_wrt1 = (diff_Z1_wrt1 if cv.ZL==cv.Z1 else (diff_Z2_wrt1 if cv.ZL==cv.Z2 else  diff_Z3_wrt1)) if cv.D<0 else ((diff_Z1_dd_wrt1 if cv.ZL==cv.Z1_dd else (diff_Z2_dd_wrt1 if cv.ZL==cv.Z2_dd else diff_Z3_dd_wrt1)) if cv.D==0 else diff_Z1_d_wrt1)
        diff_ZL_wrt2 = (diff_Z1_wrt2 if cv.ZL==cv.Z1 else (diff_Z2_wrt2 if cv.ZL==cv.Z2 else  diff_Z3_wrt2)) if cv.D<0 else ((diff_Z1_dd_wrt2 if cv.ZL==cv.Z1_dd else (diff_Z2_dd_wrt2 if cv.ZL==cv.Z2_dd else diff_Z3_dd_wrt2)) if cv.D==0 else diff_Z1_d_wrt2)

        diff_ZV_wrt1 = (diff_Z1_wrt1 if cv.ZV==cv.Z1 else (diff_Z2_wrt1 if cv.ZV==cv.Z2 else  diff_Z3_wrt1)) if cv.D<0 else ((diff_Z1_dd_wrt1 if cv.ZV==cv.Z1_dd else (diff_Z2_dd_wrt1 if cv.ZV==cv.Z2_dd else diff_Z3_dd_wrt1)) if cv.D==0 else 0)
        diff_ZV_wrt2 = (diff_Z1_wrt2 if cv.ZV==cv.Z1 else (diff_Z2_wrt2 if cv.ZV==cv.Z2 else  diff_Z3_wrt2)) if cv.D<0 else ((diff_Z1_dd_wrt2 if cv.ZV==cv.Z1_dd else (diff_Z2_dd_wrt2 if cv.ZV==cv.Z2_dd else diff_Z3_dd_wrt2)) if cv.D==0 else 0)

        diff_phl_wrt1 = 0 if (cv.ZL-cv.B)<=0 or cv.B*(cv.r2 - cv.r1) == 0 or (cv.ZL + cv.r1 * cv.B) == 0 or (cv.ZL + cv.r2 * cv.B) == 0 or (cv.ZL + cv.r2 * cv.B) * (cv.ZL + cv.r1 * cv.B) < 0  else ((diff_ZL_wrt1)-(diff_ZL_wrt1-diff_B_wrt1)/(cv.ZL-cv.B)-((((cv.B*(cv.r2-cv.r1))*diff_A_wrt1-(diff_B_wrt1*(cv.r2-cv.r1)+cv.B*(diff_r2_wrt1-diff_r1_wrt1))*cv.A)/(cv.B*(cv.r2-cv.r1))**2)*np.log((cv.ZL+cv.r2*cv.B)/(cv.ZL+cv.r1*cv.B))+(cv.A/(cv.B*(cv.r2-cv.r1)))*1/((cv.ZL+cv.r2*cv.B)/(cv.ZL+cv.r1*cv.B))*(((cv.ZL+cv.r1*cv.B)*(diff_ZL_wrt1+diff_r2_wrt1*cv.B+cv.r2*diff_B_wrt1)-(cv.ZL+cv.r2*cv.B)*(diff_ZL_wrt1+diff_r1_wrt1*cv.B+cv.r1*diff_B_wrt1))/(cv.ZL+cv.r1*cv.B)**2)))
        diff_phl_wrt2 = 0 if (cv.ZL-cv.B)<=0 or cv.B*(cv.r2 - cv.r1) == 0 or (cv.ZL + cv.r1 * cv.B) == 0 or (cv.ZL + cv.r2 * cv.B) == 0 or (cv.ZL + cv.r2 * cv.B) * (cv.ZL + cv.r1 * cv.B) < 0 else ((diff_ZL_wrt2)-(diff_ZL_wrt2-diff_B_wrt2)/(cv.ZL-cv.B)-((((cv.B*(cv.r2-cv.r1))*diff_A_wrt2-(diff_B_wrt2*(cv.r2-cv.r1)+cv.B*(diff_r2_wrt2-diff_r1_wrt2))*cv.A)/(cv.B*(cv.r2-cv.r1))**2)*np.log((cv.ZL+cv.r2*cv.B)/(cv.ZL+cv.r1*cv.B))+(cv.A/(cv.B*(cv.r2-cv.r1)))*1/((cv.ZL+cv.r2*cv.B)/(cv.ZL+cv.r1*cv.B))*(((cv.ZL+cv.r1*cv.B)*(diff_ZL_wrt2+diff_r2_wrt2*cv.B+cv.r2*diff_B_wrt2)-(cv.ZL+cv.r2*cv.B)*(diff_ZL_wrt2+diff_r1_wrt2*cv.B+cv.r1*diff_B_wrt2))/(cv.ZL+cv.r1*cv.B)**2)))

        diff_phv_wrt1 = 0 if (cv.ZV-cv.B)<=0 or cv.B*(cv.r2 - cv.r1) == 0 or (cv.ZV + cv.r1 * cv.B) == 0 or (cv.ZV + cv.r2 * cv.B) == 0 or (cv.ZV + cv.r2 * cv.B) * (cv.ZV + cv.r1 * cv.B) < 0 else ((diff_ZV_wrt1)-(diff_ZV_wrt1-diff_B_wrt1)/(cv.ZV-cv.B)-((((cv.B*(cv.r2-cv.r1))*diff_A_wrt1-(diff_B_wrt1*(cv.r2-cv.r1)+cv.B*(diff_r2_wrt1-diff_r1_wrt1))*cv.A)/(cv.B*(cv.r2-cv.r1))**2)*np.log((cv.ZV+cv.r2*cv.B)/(cv.ZV+cv.r1*cv.B))+(cv.A/(cv.B*(cv.r2-cv.r1)))*1/((cv.ZV+cv.r2*cv.B)/(cv.ZV+cv.r1*cv.B))*(((cv.ZV+cv.r1*cv.B)*(diff_ZV_wrt1+diff_r2_wrt1*cv.B+cv.r2*diff_B_wrt1)-(cv.ZV+cv.r2*cv.B)*(diff_ZV_wrt1+diff_r1_wrt1*cv.B+cv.r1*diff_B_wrt1))/(cv.ZV+cv.r1*cv.B)**2)))
        diff_phv_wrt2 = 0 if (cv.ZV-cv.B)<=0 or cv.B*(cv.r2 - cv.r1) == 0 or (cv.ZV + cv.r1 * cv.B) == 0 or (cv.ZV + cv.r2 * cv.B) == 0 or (cv.ZV + cv.r2 * cv.B) * (cv.ZV + cv.r1 * cv.B) < 0 else ((diff_ZV_wrt2)-(diff_ZV_wrt2-diff_B_wrt2)/(cv.ZV-cv.B)-((((cv.B*(cv.r2-cv.r1))*diff_A_wrt2-(diff_B_wrt2*(cv.r2-cv.r1)+cv.B*(diff_r2_wrt2-diff_r1_wrt2))*cv.A)/(cv.B*(cv.r2-cv.r1))**2)*np.log((cv.ZV+cv.r2*cv.B)/(cv.ZV+cv.r1*cv.B))+(cv.A/(cv.B*(cv.r2-cv.r1)))*1/((cv.ZV+cv.r2*cv.B)/(cv.ZV+cv.r1*cv.B))*(((cv.ZV+cv.r1*cv.B)*(diff_ZV_wrt2+diff_r2_wrt2*cv.B+cv.r2*diff_B_wrt2)-(cv.ZV+cv.r2*cv.B)*(diff_ZV_wrt2+diff_r1_wrt2*cv.B+cv.r1*diff_B_wrt2))/(cv.ZV+cv.r1*cv.B)**2)))

        diff_ph_diff_wrt1 = np.sign(cv.phl-cv.phv)*(diff_phl_wrt1-diff_phv_wrt1)
        diff_ph_diff_wrt2 = np.sign(cv.phl-cv.phv)*(diff_phl_wrt2-diff_phv_wrt2)

        diff_t1_wrt1 = gasConstant*ov.bp_final1*(diff_ZV_wrt1-diff_ZL_wrt1)
        diff_t1_wrt2 = gasConstant*ov.bp_final1*(diff_ZV_wrt2-diff_ZL_wrt2)

        diff_t2_wrt1 = 0 if cv.b == 0 or (cv.u**2-4*cv.w) <= 0 else ((cv.b*np.sqrt(cv.u**2-4*cv.w))*(diff_ac_wrt1*(ov.bp_final1*cv.diff_alpha_wrt_temp-cv.alpha_eos)+cv.ac*(ov.bp_final1*diff_diff_alpha_wrt1-diff_alpha_wrt1))-(diff_b_wrt1*np.sqrt(cv.u**2-4*cv.w)+cv.b*0.5*(2*cv.u*diff_u_wrt1-4*diff_w_wrt1)/np.sqrt(cv.u**2-4*cv.w))*(cv.ac*(ov.bp_final1*cv.diff_alpha_wrt_temp-cv.alpha_eos)))/(cv.b*np.sqrt(cv.u**2-4*cv.w))**2
        diff_t2_wrt2 = 0 if cv.b == 0 or (cv.u**2-4*cv.w) <= 0 else ((cv.b*np.sqrt(cv.u**2-4*cv.w))*(diff_ac_wrt2*(ov.bp_final1*cv.diff_alpha_wrt_temp-cv.alpha_eos)+cv.ac*(ov.bp_final1*diff_diff_alpha_wrt2-diff_alpha_wrt2))-(diff_b_wrt2*np.sqrt(cv.u**2-4*cv.w)+cv.b*0.5*(2*cv.u*diff_u_wrt2-4*diff_w_wrt2)/np.sqrt(cv.u**2-4*cv.w))*(cv.ac*(ov.bp_final1*cv.diff_alpha_wrt_temp-cv.alpha_eos)))/(cv.b*np.sqrt(cv.u**2-4*cv.w))**2

        diff_t3_wrt1 = 0 if (cv.ZV+cv.B*cv.r2) == 0 or (cv.ZV+cv.B*cv.r1) == 0 or (cv.ZV+cv.B*cv.r2)*(cv.ZV+cv.B*cv.r1) < 0 else (((cv.ZV+cv.B*cv.r1)/(cv.ZV+cv.B*cv.r2))*((cv.ZV+cv.B*cv.r1)*(diff_ZV_wrt1+diff_B_wrt1*cv.r2+cv.B*diff_r2_wrt1)-(diff_ZV_wrt1+diff_B_wrt1*cv.r1+cv.B*diff_r1_wrt1)*(cv.ZV+cv.B*cv.r2))/(cv.ZV+cv.B*cv.r1)**2)
        diff_t3_wrt2 = 0 if (cv.ZV+cv.B*cv.r2) == 0 or (cv.ZV+cv.B*cv.r1) == 0 or (cv.ZV+cv.B*cv.r2)*(cv.ZV+cv.B*cv.r1) < 0 else (((cv.ZV+cv.B*cv.r1)/(cv.ZV+cv.B*cv.r2))*((cv.ZV+cv.B*cv.r1)*(diff_ZV_wrt2+diff_B_wrt2*cv.r2+cv.B*diff_r2_wrt2)-(diff_ZV_wrt2+diff_B_wrt2*cv.r1+cv.B*diff_r1_wrt2)*(cv.ZV+cv.B*cv.r2))/(cv.ZV+cv.B*cv.r1)**2)

        diff_t4_wrt1 = 0 if (cv.ZL+cv.B*cv.r2) == 0 or (cv.ZL+cv.B*cv.r1) == 0 or (cv.ZL+cv.B*cv.r2)*(cv.ZL+cv.B*cv.r1) < 0 else (((cv.ZL+cv.B*cv.r1)/(cv.ZL+cv.B*cv.r2))*((cv.ZL+cv.B*cv.r1)*(diff_ZL_wrt1+diff_B_wrt1*cv.r2+cv.B*diff_r2_wrt1)-(diff_ZL_wrt1+diff_B_wrt1*cv.r1+cv.B*diff_r1_wrt1)*(cv.ZL+cv.B*cv.r2))/(cv.ZL+cv.B*cv.r1)**2)
        diff_t4_wrt2 = 0 if (cv.ZL+cv.B*cv.r2) == 0 or (cv.ZL+cv.B*cv.r1) == 0 or (cv.ZL+cv.B*cv.r2)*(cv.ZL+cv.B*cv.r1) < 0 else (((cv.ZL+cv.B*cv.r1)/(cv.ZL+cv.B*cv.r2))*((cv.ZL+cv.B*cv.r1)*(diff_ZL_wrt2+diff_B_wrt2*cv.r2+cv.B*diff_r2_wrt2)-(diff_ZL_wrt2+diff_B_wrt2*cv.r1+cv.B*diff_r1_wrt2)*(cv.ZL+cv.B*cv.r2))/(cv.ZL+cv.B*cv.r1)**2)

        diff_hv_bp_wrt1 = (diff_t1_wrt1+diff_t2_wrt1*(cv.t3-cv.t4)+cv.t2*(diff_t3_wrt1-diff_t4_wrt1))/1000.
        diff_hv_bp_wrt2 = (diff_t1_wrt2+diff_t2_wrt2*(cv.t3-cv.t4)+cv.t2*(diff_t3_wrt2-diff_t4_wrt2))/1000.

        diff_vb_wrt1 = diff_ZL_wrt1*gasConstant*ov.bp_final1/pressure
        diff_vb_wrt2 = diff_ZL_wrt2*gasConstant*ov.bp_final1/pressure

        diff_wrt1 = self.scales[0]*(2*(ov.hv_bp-cv.hv_bp)/ov.hv_bp**2)*(-1.)*diff_hv_bp_wrt1+self.scales[1]*(2*(ov.vb-cv.vb)/ov.vb**2)*(-1)*diff_vb_wrt1+self.scales[2]*2*(max(0,(cv.ph_diff-10**-7)))*(0 if(cv.ph_diff-10**-7)<=0 else diff_ph_diff_wrt1)
        diff_wrt2 = self.scales[0]*(2*(ov.hv_bp-cv.hv_bp)/ov.hv_bp**2)*(-1.)*diff_hv_bp_wrt2+self.scales[1]*(2*(ov.vb-cv.vb)/ov.vb**2)*(-1)*diff_vb_wrt2+self.scales[2]*2*(max(0,(cv.ph_diff-10**-7)))*(0 if(cv.ph_diff-10**-7)<=0 else diff_ph_diff_wrt2)

        diff_wrt1 = (np.sign(diff_wrt1)*10**7) if np.abs(diff_wrt1)>10**7 else diff_wrt1
        diff_wrt2 = (np.sign(diff_wrt2)*10**7) if np.abs(diff_wrt2)>10**7 else diff_wrt2    
        return np.array([diff_wrt1, diff_wrt2])

    def als(self, ov, cv):
        diff_alpha_wrt1 = ov.alpha_pr
        diff_alpha_wrt2 = 0
        
        diff_diff_alpha_wrt1 = ov.alpha_pr*(((2*ov.alpha_m-2)/(ov.bp_final1/ov.tc_k)+ov.alpha_l*(-2*ov.alpha_m)*(ov.bp_final1/ov.tc_k)**(2*ov.alpha_m-1))/ov.tc_k)
        diff_diff_alpha_wrt2 = 0

        diff_omega_wrt1 = 0.
        diff_omega_wrt2 = ov.omega_final

        diff_om_b_wrt1 = 0
        diff_om_b_wrt2 = 0 if (0.08974-0.03452*cv.omega_final+0.0033*cv.omega_final**2)<0.001 < 0.001 else (-0.03452*diff_omega_wrt2+0.0033*2*diff_omega_wrt2*cv.omega_final)

        diff_om_c_wrt1 = 0
        diff_om_c_wrt2 = 0 if (0.03686+0.00405*cv.omega_final-0.01073*cv.omega_final**2+0.00157*cv.omega_final**3) < 0.001 else (0.00405*diff_omega_wrt2-0.01073*2*diff_omega_wrt2*cv.omega_final+0.00157*3*diff_omega_wrt2*cv.omega_final**2)

        diff_om_d_wrt1 = 0
        diff_om_d_wrt2 = 0 if (0.154+0.14122*cv.omega_final-0.00272*cv.omega_final**2-0.00484*cv.omega_final**3) < 0.001 else (0.14122*diff_omega_wrt2-0.00272*2*diff_omega_wrt2*cv.omega_final-0.00484*3*diff_omega_wrt2*cv.omega_final**2)

        diff_om_a_wrt1 = 0
        diff_om_a_wrt2 = 0 if (0.44869+0.04024*cv.omega_final+0.01111*cv.omega_final**2-0.00576*cv.omega_final**3) < 0.001 else (0.04024*diff_omega_wrt2+0.01111*2*diff_omega_wrt2*cv.omega_final-0.00576*3*diff_omega_wrt2*cv.omega_final**2)

        diff_b_wrt1 = diff_om_b_wrt1*gasConstant*ov.tc_k/(ov.pc_mpa*10**6)
        diff_b_wrt2 = diff_om_b_wrt2*gasConstant*ov.tc_k/(ov.pc_mpa*10**6)

        diff_c_wrt1 = diff_om_c_wrt1*gasConstant*ov.tc_k/(ov.pc_mpa*10**6)
        diff_c_wrt2 = diff_om_c_wrt2*gasConstant*ov.tc_k/(ov.pc_mpa*10**6)

        diff_d_wrt1 = diff_om_d_wrt1*gasConstant*ov.tc_k/(ov.pc_mpa*10**6)
        diff_d_wrt2 = diff_om_d_wrt2*gasConstant*ov.tc_k/(ov.pc_mpa*10**6)

        diff_ac_wrt1 = diff_om_a_wrt1*gasConstant**2*ov.tc_k**2/(ov.pc_mpa*10**6)
        diff_ac_wrt2 = diff_om_a_wrt2*gasConstant**2*ov.tc_k**2/(ov.pc_mpa*10**6)

        diff_ac_alpha_wrt1 = (cv.ac*diff_alpha_wrt1+diff_ac_wrt1*cv.alpha_eos)
        diff_ac_alpha_wrt2 = (cv.ac*diff_alpha_wrt2+diff_ac_wrt2*cv.alpha_eos)

        diff_u_wrt1 = 0. if cv.b == 0 else (((diff_d_wrt1 - diff_c_wrt1)*cv.b - (cv.d - cv.c) * diff_b_wrt1)/cv.b**2)
        diff_u_wrt2 = 0. if cv.b == 0 else (((diff_d_wrt2 - diff_c_wrt2)*cv.b - (cv.d - cv.c) * diff_b_wrt2)/cv.b**2)

        diff_w_wrt1 = 0. if cv.b == 0 else (-1. * ((diff_d_wrt1 * cv.c + cv.d * diff_c_wrt1) * cv.b**2 - cv.d * cv.c * 2. * diff_b_wrt1 * cv.b)/cv.b**4)
        diff_w_wrt2 = 0. if cv.b == 0 else (-1. * ((diff_d_wrt2 * cv.c + cv.d * diff_c_wrt2) * cv.b**2 - cv.d * cv.c * 2. * diff_b_wrt2 * cv.b)/cv.b**4)

        diff_r2_wrt1 = 0
        diff_r2_wrt2 = 0 if (cv.u**2-4*cv.w) <= 0 else (diff_u_wrt2+0.5*(2*cv.u*diff_u_wrt2-4*diff_w_wrt2)*(cv.u**2-4*cv.w)**-0.5)/2

        diff_r1_wrt1 = 0
        diff_r1_wrt2 = 0 if (cv.u**2-4*cv.w) <= 0 else (diff_u_wrt2-0.5*(2*cv.u*diff_u_wrt2-4*diff_w_wrt2)*(cv.u**2-4*cv.w)**-0.5)/2              

        diff_A_wrt1 = diff_ac_alpha_wrt1*pressure*10**6/(gasConstant**2*ov.bp_final1**2)
        diff_A_wrt2 = diff_ac_alpha_wrt2*pressure*10**6/(gasConstant**2*ov.bp_final1**2)

        diff_B_wrt1 = 0
        diff_B_wrt2 = diff_b_wrt2*pressure*10**6/(gasConstant*ov.bp_final1)   

        diff_a1_wrt1 = 0
        diff_a1_wrt2 = ((cv.u*diff_B_wrt2+diff_u_wrt2*cv.B)-diff_B_wrt2) 

        diff_a2_wrt1 = diff_A_wrt1+(cv.w*2*cv.B*diff_B_wrt1+diff_w_wrt1*cv.B**2)-(cv.u*diff_B_wrt1+diff_u_wrt1*cv.B)-(cv.u*2*cv.B*diff_B_wrt1+diff_u_wrt1*cv.B**2)
        diff_a2_wrt2 = diff_A_wrt2+(cv.w*2*cv.B*diff_B_wrt2+diff_w_wrt2*cv.B**2)-(cv.u*diff_B_wrt2+diff_u_wrt2*cv.B)-(cv.u*2*cv.B*diff_B_wrt2+diff_u_wrt2*cv.B**2)

        diff_a3_wrt1 = -1*((cv.A*diff_B_wrt1+diff_A_wrt1*cv.B)+(cv.w*2*cv.B*diff_B_wrt1+diff_w_wrt1*cv.B**2)+(cv.w*3*cv.B**2*diff_B_wrt1+diff_w_wrt1*cv.B**3))
        diff_a3_wrt2 = -1*((cv.A*diff_B_wrt2+diff_A_wrt2*cv.B)+(cv.w*2*cv.B*diff_B_wrt2+diff_w_wrt2*cv.B**2)+(cv.w*3*cv.B**2*diff_B_wrt2+diff_w_wrt2*cv.B**3))

        diff_Q_wrt1 = (3*diff_a2_wrt1-2*cv.a1*diff_a1_wrt1)/9.
        diff_Q_wrt2 = (3*diff_a2_wrt2-2*cv.a1*diff_a1_wrt2)/9.

        diff_L_wrt1 = (9*(cv.a1*diff_a2_wrt1+diff_a1_wrt1*cv.a2)-27*diff_a3_wrt1-2*3*diff_a1_wrt1*cv.a1**2)/54.
        diff_L_wrt2 = (9*(cv.a1*diff_a2_wrt2+diff_a1_wrt2*cv.a2)-27*diff_a3_wrt2-2*3*diff_a1_wrt2*cv.a1**2)/54.

        diff_D_wrt1 = 3*diff_Q_wrt1*cv.Q**2+2*diff_L_wrt1*cv.L
        diff_D_wrt2 = 3*diff_Q_wrt2*cv.Q**2+2*diff_L_wrt2*cv.L

        diff_S1_wrt1 = 0 if cv.L+np.sqrt(cv.D) == 0 else ((-1*(1./3.)*(-1*(diff_L_wrt1+0.5*diff_D_wrt1/np.sqrt(cv.D)))*(-1*(cv.L+np.sqrt(cv.D)))**(1./3.-1)) if (cv.L+np.sqrt(cv.D))<0 else ((1./3.)*(diff_L_wrt1+0.5*diff_D_wrt1/np.sqrt(cv.D))*(cv.L+np.sqrt(cv.D))**(1./3.-1)))
        diff_S1_wrt2 = 0 if cv.L+np.sqrt(cv.D) == 0 else ((-1*(1./3.)*(-1*(diff_L_wrt2+0.5*diff_D_wrt2/np.sqrt(cv.D)))*(-1*(cv.L+np.sqrt(cv.D)))**(1./3.-1)) if (cv.L+np.sqrt(cv.D))<0 else ((1./3.)*(diff_L_wrt2+0.5*diff_D_wrt2/np.sqrt(cv.D))*(cv.L+np.sqrt(cv.D))**(1./3.-1)))

        diff_S2_wrt1 = 0 if cv.L-np.sqrt(cv.D) == 0 else ((-1*(1./3.)*(-1*(diff_L_wrt1-0.5*diff_D_wrt1/np.sqrt(cv.D)))*(-1*(cv.L-np.sqrt(cv.D)))**(1./3.-1)) if (cv.L-np.sqrt(cv.D))<0 else ((1./3.)*(diff_L_wrt1-0.5*diff_D_wrt1/np.sqrt(cv.D))*(cv.L-np.sqrt(cv.D))**(1./3.-1)))
        diff_S2_wrt2 = 0 if cv.L-np.sqrt(cv.D) == 0 else ((-1*(1./3.)*(-1*(diff_L_wrt2-0.5*diff_D_wrt2/np.sqrt(cv.D)))*(-1*(cv.L-np.sqrt(cv.D)))**(1./3.-1)) if (cv.L-np.sqrt(cv.D))<0 else ((1./3.)*(diff_L_wrt2-0.5*diff_D_wrt2/np.sqrt(cv.D))*(cv.L-np.sqrt(cv.D))**(1./3.-1)))

        diff_Z1_d_wrt1 = diff_S1_wrt1+diff_S2_wrt1-diff_a1_wrt1/3
        diff_Z1_d_wrt2 = diff_S1_wrt2+diff_S2_wrt2-diff_a1_wrt2/3

        diff_Z1_dd_wrt1 = ((-1*(1./3.)*(-1*diff_L_wrt1)*(-1*cv.L)**(1./3.-1)) if cv.L<0 else (1./3.*diff_L_wrt1*cv.L**(1./3.-1))) *2-diff_a1_wrt1/3
        diff_Z1_dd_wrt2 = ((-1*(1./3.)*(-1*diff_L_wrt2)*(-1*cv.L)**(1./3.-1)) if cv.L<0 else (1./3.*diff_L_wrt2*cv.L**(1./3.-1))) *2-diff_a1_wrt2/3

        diff_Z2_dd_wrt1 = (((-1*(1./3.)*(-1*diff_L_wrt1)*(-1*cv.L)**(1./3.-1)) if cv.L<0 else (1./3.*diff_L_wrt1*cv.L**(1./3.-1))) +diff_a1_wrt1/3) * -1.
        diff_Z2_dd_wrt2 = (((-1*(1./3.)*(-1*diff_L_wrt2)*(-1*cv.L)**(1./3.-1)) if cv.L<0 else (1./3.*diff_L_wrt2*cv.L**(1./3.-1))) +diff_a1_wrt2/3) * -1.
        
        diff_Z3_dd_wrt1 = diff_Z2_dd_wrt1
        diff_Z3_dd_wrt2 = diff_Z2_dd_wrt2

        diff_Z1_wrt1 = 0 if cv.D>=0 or cv.Q>=0 else (2*(-0.5*diff_Q_wrt1/np.sqrt(-1*cv.Q)*np.cos(1./3.*(np.arccos( 1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3) ))+2*np.pi/3)+np.sqrt(-1*cv.Q)*(-1)*(1./3.*-1/np.sqrt(1-(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3))**2))*((np.sqrt(-cv.Q**3)*diff_L_wrt1-(0.5*-1*3*diff_Q_wrt1*cv.Q**2)/np.sqrt(-cv.Q**3)*cv.L)/(-cv.Q**3))*np.sin(1./3.*(np.arccos(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3)))+2*np.pi/3))-diff_a1_wrt1/3)
        diff_Z1_wrt2 = 0 if cv.D>=0 or cv.Q>=0 else (2*(-0.5*diff_Q_wrt2/np.sqrt(-1*cv.Q)*np.cos(1./3.*(np.arccos( 1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3) ))+2*np.pi/3)+np.sqrt(-1*cv.Q)*(-1)*(1./3.*-1/np.sqrt(1-(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3))**2))*((np.sqrt(-cv.Q**3)*diff_L_wrt2-(0.5*-1*3*diff_Q_wrt2*cv.Q**2)/np.sqrt(-cv.Q**3)*cv.L)/(-cv.Q**3))*np.sin(1./3.*(np.arccos(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3)))+2*np.pi/3))-diff_a1_wrt2/3)

        diff_Z2_wrt1 = 0 if cv.D>=0 or cv.Q>=0 else (2*(-0.5*diff_Q_wrt1/np.sqrt(-1*cv.Q)*np.cos(1./3.*(np.arccos( 1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3) ))+4*np.pi/3)+np.sqrt(-1*cv.Q)*(-1)*(1./3.*-1/np.sqrt(1-(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3))**2))*((np.sqrt(-cv.Q**3)*diff_L_wrt1-(0.5*-1*3*diff_Q_wrt1*cv.Q**2)/np.sqrt(-cv.Q**3)*cv.L)/(-cv.Q**3))*np.sin(1./3.*(np.arccos(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3)))+4*np.pi/3))-diff_a1_wrt1/3)
        diff_Z2_wrt2 = 0 if cv.D>=0 or cv.Q>=0 else (2*(-0.5*diff_Q_wrt2/np.sqrt(-1*cv.Q)*np.cos(1./3.*(np.arccos( 1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3) ))+4*np.pi/3)+np.sqrt(-1*cv.Q)*(-1)*(1./3.*-1/np.sqrt(1-(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3))**2))*((np.sqrt(-cv.Q**3)*diff_L_wrt2-(0.5*-1*3*diff_Q_wrt2*cv.Q**2)/np.sqrt(-cv.Q**3)*cv.L)/(-cv.Q**3))*np.sin(1./3.*(np.arccos(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3)))+4*np.pi/3))-diff_a1_wrt2/3)

        diff_Z3_wrt1 = 0 if cv.D>=0 or cv.Q>=0 else (2*(-0.5*diff_Q_wrt1/np.sqrt(-1*cv.Q)*np.cos(1./3.*(np.arccos( 1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3) )))+np.sqrt(-1*cv.Q)*(-1)*(1./3.*-1/np.sqrt(1-(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3))**2))*((np.sqrt(-cv.Q**3)*diff_L_wrt1-(0.5*-1*3*diff_Q_wrt1*cv.Q**2)/np.sqrt(-cv.Q**3)*cv.L)/(-cv.Q**3))*np.sin(1./3.*(np.arccos(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3)))))-diff_a1_wrt1/3)
        diff_Z3_wrt2 = 0 if cv.D>=0 or cv.Q>=0 else (2*(-0.5*diff_Q_wrt2/np.sqrt(-1*cv.Q)*np.cos(1./3.*(np.arccos( 1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3) )))+np.sqrt(-1*cv.Q)*(-1)*(1./3.*-1/np.sqrt(1-(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3))**2))*((np.sqrt(-cv.Q**3)*diff_L_wrt2-(0.5*-1*3*diff_Q_wrt2*cv.Q**2)/np.sqrt(-cv.Q**3)*cv.L)/(-cv.Q**3))*np.sin(1./3.*(np.arccos(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3)))))-diff_a1_wrt2/3)

        diff_ZL_wrt1 = (diff_Z1_wrt1 if cv.ZL==cv.Z1 else (diff_Z2_wrt1 if cv.ZL==cv.Z2 else  diff_Z3_wrt1)) if cv.D<0 else ((diff_Z1_dd_wrt1 if cv.ZL==cv.Z1_dd else (diff_Z2_dd_wrt1 if cv.ZL==cv.Z2_dd else diff_Z3_dd_wrt1)) if cv.D==0 else diff_Z1_d_wrt1)
        diff_ZL_wrt2 = (diff_Z1_wrt2 if cv.ZL==cv.Z1 else (diff_Z2_wrt2 if cv.ZL==cv.Z2 else  diff_Z3_wrt2)) if cv.D<0 else ((diff_Z1_dd_wrt2 if cv.ZL==cv.Z1_dd else (diff_Z2_dd_wrt2 if cv.ZL==cv.Z2_dd else diff_Z3_dd_wrt2)) if cv.D==0 else diff_Z1_d_wrt2)

        diff_ZV_wrt1 = (diff_Z1_wrt1 if cv.ZV==cv.Z1 else (diff_Z2_wrt1 if cv.ZV==cv.Z2 else  diff_Z3_wrt1)) if cv.D<0 else ((diff_Z1_dd_wrt1 if cv.ZV==cv.Z1_dd else (diff_Z2_dd_wrt1 if cv.ZV==cv.Z2_dd else diff_Z3_dd_wrt1)) if cv.D==0 else 0)
        diff_ZV_wrt2 = (diff_Z1_wrt2 if cv.ZV==cv.Z1 else (diff_Z2_wrt2 if cv.ZV==cv.Z2 else  diff_Z3_wrt2)) if cv.D<0 else ((diff_Z1_dd_wrt2 if cv.ZV==cv.Z1_dd else (diff_Z2_dd_wrt2 if cv.ZV==cv.Z2_dd else diff_Z3_dd_wrt2)) if cv.D==0 else 0)

        diff_phl_wrt1 = 0 if (cv.ZL-cv.B)<=0 or cv.B*(cv.r2 - cv.r1) == 0 or (cv.ZL + cv.r1 * cv.B) == 0 or (cv.ZL + cv.r2 * cv.B) == 0 or (cv.ZL + cv.r2 * cv.B) * (cv.ZL + cv.r1 * cv.B) < 0  else ((diff_ZL_wrt1)-(diff_ZL_wrt1-diff_B_wrt1)/(cv.ZL-cv.B)-((((cv.B*(cv.r2-cv.r1))*diff_A_wrt1-(diff_B_wrt1*(cv.r2-cv.r1)+cv.B*(diff_r2_wrt1-diff_r1_wrt1))*cv.A)/(cv.B*(cv.r2-cv.r1))**2)*np.log((cv.ZL+cv.r2*cv.B)/(cv.ZL+cv.r1*cv.B))+(cv.A/(cv.B*(cv.r2-cv.r1)))*1/((cv.ZL+cv.r2*cv.B)/(cv.ZL+cv.r1*cv.B))*(((cv.ZL+cv.r1*cv.B)*(diff_ZL_wrt1+diff_r2_wrt1*cv.B+cv.r2*diff_B_wrt1)-(cv.ZL+cv.r2*cv.B)*(diff_ZL_wrt1+diff_r1_wrt1*cv.B+cv.r1*diff_B_wrt1))/(cv.ZL+cv.r1*cv.B)**2)))
        diff_phl_wrt2 = 0 if (cv.ZL-cv.B)<=0 or cv.B*(cv.r2 - cv.r1) == 0 or (cv.ZL + cv.r1 * cv.B) == 0 or (cv.ZL + cv.r2 * cv.B) == 0 or (cv.ZL + cv.r2 * cv.B) * (cv.ZL + cv.r1 * cv.B) < 0 else ((diff_ZL_wrt2)-(diff_ZL_wrt2-diff_B_wrt2)/(cv.ZL-cv.B)-((((cv.B*(cv.r2-cv.r1))*diff_A_wrt2-(diff_B_wrt2*(cv.r2-cv.r1)+cv.B*(diff_r2_wrt2-diff_r1_wrt2))*cv.A)/(cv.B*(cv.r2-cv.r1))**2)*np.log((cv.ZL+cv.r2*cv.B)/(cv.ZL+cv.r1*cv.B))+(cv.A/(cv.B*(cv.r2-cv.r1)))*1/((cv.ZL+cv.r2*cv.B)/(cv.ZL+cv.r1*cv.B))*(((cv.ZL+cv.r1*cv.B)*(diff_ZL_wrt2+diff_r2_wrt2*cv.B+cv.r2*diff_B_wrt2)-(cv.ZL+cv.r2*cv.B)*(diff_ZL_wrt2+diff_r1_wrt2*cv.B+cv.r1*diff_B_wrt2))/(cv.ZL+cv.r1*cv.B)**2)))

        diff_phv_wrt1 = 0 if (cv.ZV-cv.B)<=0 or cv.B*(cv.r2 - cv.r1) == 0 or (cv.ZV + cv.r1 * cv.B) == 0 or (cv.ZV + cv.r2 * cv.B) == 0 or (cv.ZV + cv.r2 * cv.B) * (cv.ZV + cv.r1 * cv.B) < 0 else ((diff_ZV_wrt1)-(diff_ZV_wrt1-diff_B_wrt1)/(cv.ZV-cv.B)-((((cv.B*(cv.r2-cv.r1))*diff_A_wrt1-(diff_B_wrt1*(cv.r2-cv.r1)+cv.B*(diff_r2_wrt1-diff_r1_wrt1))*cv.A)/(cv.B*(cv.r2-cv.r1))**2)*np.log((cv.ZV+cv.r2*cv.B)/(cv.ZV+cv.r1*cv.B))+(cv.A/(cv.B*(cv.r2-cv.r1)))*1/((cv.ZV+cv.r2*cv.B)/(cv.ZV+cv.r1*cv.B))*(((cv.ZV+cv.r1*cv.B)*(diff_ZV_wrt1+diff_r2_wrt1*cv.B+cv.r2*diff_B_wrt1)-(cv.ZV+cv.r2*cv.B)*(diff_ZV_wrt1+diff_r1_wrt1*cv.B+cv.r1*diff_B_wrt1))/(cv.ZV+cv.r1*cv.B)**2)))
        diff_phv_wrt2 = 0 if (cv.ZV-cv.B)<=0 or cv.B*(cv.r2 - cv.r1) == 0 or (cv.ZV + cv.r1 * cv.B) == 0 or (cv.ZV + cv.r2 * cv.B) == 0 or (cv.ZV + cv.r2 * cv.B) * (cv.ZV + cv.r1 * cv.B) < 0 else ((diff_ZV_wrt2)-(diff_ZV_wrt2-diff_B_wrt2)/(cv.ZV-cv.B)-((((cv.B*(cv.r2-cv.r1))*diff_A_wrt2-(diff_B_wrt2*(cv.r2-cv.r1)+cv.B*(diff_r2_wrt2-diff_r1_wrt2))*cv.A)/(cv.B*(cv.r2-cv.r1))**2)*np.log((cv.ZV+cv.r2*cv.B)/(cv.ZV+cv.r1*cv.B))+(cv.A/(cv.B*(cv.r2-cv.r1)))*1/((cv.ZV+cv.r2*cv.B)/(cv.ZV+cv.r1*cv.B))*(((cv.ZV+cv.r1*cv.B)*(diff_ZV_wrt2+diff_r2_wrt2*cv.B+cv.r2*diff_B_wrt2)-(cv.ZV+cv.r2*cv.B)*(diff_ZV_wrt2+diff_r1_wrt2*cv.B+cv.r1*diff_B_wrt2))/(cv.ZV+cv.r1*cv.B)**2)))

        diff_ph_diff_wrt1 = np.sign(cv.phl-cv.phv)*(diff_phl_wrt1-diff_phv_wrt1)
        diff_ph_diff_wrt2 = np.sign(cv.phl-cv.phv)*(diff_phl_wrt2-diff_phv_wrt2)

        diff_t1_wrt1 = gasConstant*ov.bp_final1*(diff_ZV_wrt1-diff_ZL_wrt1)
        diff_t1_wrt2 = gasConstant*ov.bp_final1*(diff_ZV_wrt2-diff_ZL_wrt2)

        diff_t2_wrt1 = 0 if cv.b == 0 or (cv.u**2-4*cv.w) <= 0 else ((cv.b*np.sqrt(cv.u**2-4*cv.w))*(diff_ac_wrt1*(ov.bp_final1*cv.diff_alpha_wrt_temp-cv.alpha_eos)+cv.ac*(ov.bp_final1*diff_diff_alpha_wrt1-diff_alpha_wrt1))-(diff_b_wrt1*np.sqrt(cv.u**2-4*cv.w)+cv.b*0.5*(2*cv.u*diff_u_wrt1-4*diff_w_wrt1)/np.sqrt(cv.u**2-4*cv.w))*(cv.ac*(ov.bp_final1*cv.diff_alpha_wrt_temp-cv.alpha_eos)))/(cv.b*np.sqrt(cv.u**2-4*cv.w))**2
        diff_t2_wrt2 = 0 if cv.b == 0 or (cv.u**2-4*cv.w) <= 0 else ((cv.b*np.sqrt(cv.u**2-4*cv.w))*(diff_ac_wrt2*(ov.bp_final1*cv.diff_alpha_wrt_temp-cv.alpha_eos)+cv.ac*(ov.bp_final1*diff_diff_alpha_wrt2-diff_alpha_wrt2))-(diff_b_wrt2*np.sqrt(cv.u**2-4*cv.w)+cv.b*0.5*(2*cv.u*diff_u_wrt2-4*diff_w_wrt2)/np.sqrt(cv.u**2-4*cv.w))*(cv.ac*(ov.bp_final1*cv.diff_alpha_wrt_temp-cv.alpha_eos)))/(cv.b*np.sqrt(cv.u**2-4*cv.w))**2

        diff_t3_wrt1 = 0 if (cv.ZV+cv.B*cv.r2) == 0 or (cv.ZV+cv.B*cv.r1) == 0 or (cv.ZV+cv.B*cv.r2)*(cv.ZV+cv.B*cv.r1) < 0 else (((cv.ZV+cv.B*cv.r1)/(cv.ZV+cv.B*cv.r2))*((cv.ZV+cv.B*cv.r1)*(diff_ZV_wrt1+diff_B_wrt1*cv.r2+cv.B*diff_r2_wrt1)-(diff_ZV_wrt1+diff_B_wrt1*cv.r1+cv.B*diff_r1_wrt1)*(cv.ZV+cv.B*cv.r2))/(cv.ZV+cv.B*cv.r1)**2)
        diff_t3_wrt2 = 0 if (cv.ZV+cv.B*cv.r2) == 0 or (cv.ZV+cv.B*cv.r1) == 0 or (cv.ZV+cv.B*cv.r2)*(cv.ZV+cv.B*cv.r1) < 0 else (((cv.ZV+cv.B*cv.r1)/(cv.ZV+cv.B*cv.r2))*((cv.ZV+cv.B*cv.r1)*(diff_ZV_wrt2+diff_B_wrt2*cv.r2+cv.B*diff_r2_wrt2)-(diff_ZV_wrt2+diff_B_wrt2*cv.r1+cv.B*diff_r1_wrt2)*(cv.ZV+cv.B*cv.r2))/(cv.ZV+cv.B*cv.r1)**2)

        diff_t4_wrt1 = 0 if (cv.ZL+cv.B*cv.r2) == 0 or (cv.ZL+cv.B*cv.r1) == 0 or (cv.ZL+cv.B*cv.r2)*(cv.ZL+cv.B*cv.r1) < 0 else (((cv.ZL+cv.B*cv.r1)/(cv.ZL+cv.B*cv.r2))*((cv.ZL+cv.B*cv.r1)*(diff_ZL_wrt1+diff_B_wrt1*cv.r2+cv.B*diff_r2_wrt1)-(diff_ZL_wrt1+diff_B_wrt1*cv.r1+cv.B*diff_r1_wrt1)*(cv.ZL+cv.B*cv.r2))/(cv.ZL+cv.B*cv.r1)**2)
        diff_t4_wrt2 = 0 if (cv.ZL+cv.B*cv.r2) == 0 or (cv.ZL+cv.B*cv.r1) == 0 or (cv.ZL+cv.B*cv.r2)*(cv.ZL+cv.B*cv.r1) < 0 else (((cv.ZL+cv.B*cv.r1)/(cv.ZL+cv.B*cv.r2))*((cv.ZL+cv.B*cv.r1)*(diff_ZL_wrt2+diff_B_wrt2*cv.r2+cv.B*diff_r2_wrt2)-(diff_ZL_wrt2+diff_B_wrt2*cv.r1+cv.B*diff_r1_wrt2)*(cv.ZL+cv.B*cv.r2))/(cv.ZL+cv.B*cv.r1)**2)

        diff_hv_bp_wrt1 = (diff_t1_wrt1+diff_t2_wrt1*(cv.t3-cv.t4)+cv.t2*(diff_t3_wrt1-diff_t4_wrt1))/1000.
        diff_hv_bp_wrt2 = (diff_t1_wrt2+diff_t2_wrt2*(cv.t3-cv.t4)+cv.t2*(diff_t3_wrt2-diff_t4_wrt2))/1000.

        diff_vb_wrt1 = diff_ZL_wrt1*gasConstant*ov.bp_final1/pressure
        diff_vb_wrt2 = diff_ZL_wrt2*gasConstant*ov.bp_final1/pressure

        diff_wrt1 = self.scales[0]*(2*(ov.hv_bp-cv.hv_bp)/ov.hv_bp**2)*(-1.)*diff_hv_bp_wrt1+self.scales[1]*(2*(ov.vb-cv.vb)/ov.vb**2)*(-1)*diff_vb_wrt1+self.scales[2]*2*(max(0,(cv.ph_diff-10**-7)))*(0 if(cv.ph_diff-10**-7)<=0 else diff_ph_diff_wrt1)
        diff_wrt2 = self.scales[0]*(2*(ov.hv_bp-cv.hv_bp)/ov.hv_bp**2)*(-1.)*diff_hv_bp_wrt2+self.scales[1]*(2*(ov.vb-cv.vb)/ov.vb**2)*(-1)*diff_vb_wrt2+self.scales[2]*2*(max(0,(cv.ph_diff-10**-7)))*(0 if(cv.ph_diff-10**-7)<=0 else diff_ph_diff_wrt2)

        diff_wrt1 = (np.sign(diff_wrt1)*10**7) if np.abs(diff_wrt1)>10**7 else diff_wrt1
        diff_wrt2 = (np.sign(diff_wrt2)*10**7) if np.abs(diff_wrt2)>10**7 else diff_wrt2  
        return np.array([diff_wrt1, diff_wrt2])

    def sw_poas(self, ov, cv):
        diff_alpha_wrt1 = ov.alpha_pr
        diff_alpha_wrt2 = 0
        
        diff_diff_alpha_wrt1 = ov.alpha_pr*(((2*ov.alpha_m-2)/(ov.bp_final1/ov.tc_k)+ov.alpha_l*(-2*ov.alpha_m)*(ov.bp_final1/ov.tc_k)**(2*ov.alpha_m-1))/ov.tc_k)
        diff_diff_alpha_wrt2 = 0

        diff_omega_wrt1 = 0.
        diff_omega_wrt2 = ov.omega_final

        diff_a1_d_wrt1 = 0
        diff_a1_d_wrt2 = 3. * (-1. * 6. * diff_omega_wrt2)/(1. + 6. * cv.omega_final)**2

        diff_a2_d_wrt1 = 0
        diff_a2_d_wrt2 = 3. * (-1. * 6. * diff_omega_wrt2)/(1. + 6. * cv.omega_final)**2

        diff_a3_d_wrt1 = 0
        diff_a3_d_wrt2 = -1. * (-1. * 6. * diff_omega_wrt2)/(1. + 6. * cv.omega_final)**2

        diff_Q_d_wrt1 = 0
        diff_Q_d_wrt2 = (3. * diff_a2_d_wrt2 - 2. * cv.a1_d * diff_a1_d_wrt2)/9.

        diff_L_d_wrt1 = 0
        diff_L_d_wrt2 = (9. * (diff_a1_d_wrt2 * cv.a2_d + cv.a1_d * diff_a2_d_wrt2) - 27. * diff_a3_d_wrt2 -2. * 3. * diff_a1_d_wrt2 * cv.a1_d**2)/54. 

        diff_D_d_wrt1 = 0
        diff_D_d_wrt2 = 3. * diff_Q_d_wrt2 * cv.Q_d**2 + 2. * diff_L_d_wrt2 * cv.L_d

        diff_S1_d_wrt1 = 0 if cv.L_d+np.sqrt(cv.D_d) == 0 else ((-1*(1./3.)*(-1*(diff_L_d_wrt1+0.5*diff_D_d_wrt1/np.sqrt(cv.D_d)))*(-1*(cv.L_d+np.sqrt(cv.D_d)))**(1./3.-1)) if (cv.L_d+np.sqrt(cv.D_d))<0 else ((1./3.)*(diff_L_d_wrt1+0.5*diff_D_d_wrt1/np.sqrt(cv.D_d))*(cv.L_d+np.sqrt(cv.D_d))**(1./3.-1)))
        diff_S1_d_wrt2 = 0 if cv.L_d+np.sqrt(cv.D_d) == 0 else ((-1*(1./3.)*(-1*(diff_L_d_wrt2+0.5*diff_D_d_wrt2/np.sqrt(cv.D_d)))*(-1*(cv.L_d+np.sqrt(cv.D_d)))**(1./3.-1)) if (cv.L_d+np.sqrt(cv.D_d))<0 else ((1./3.)*(diff_L_d_wrt2+0.5*diff_D_d_wrt2/np.sqrt(cv.D_d))*(cv.L_d+np.sqrt(cv.D_d))**(1./3.-1)))

        diff_S2_d_wrt1 = 0 if cv.L_d-np.sqrt(cv.D_d) == 0 else ((-1*(1./3.)*(-1*(diff_L_d_wrt1-0.5*diff_D_d_wrt1/np.sqrt(cv.D_d)))*(-1*(cv.L_d-np.sqrt(cv.D_d)))**(1./3.-1)) if (cv.L_d-np.sqrt(cv.D_d))<0 else ((1./3.)*(diff_L_d_wrt1-0.5*diff_D_d_wrt1/np.sqrt(cv.D_d))*(cv.L_d-np.sqrt(cv.D_d))**(1./3.-1)))
        diff_S2_d_wrt2 = 0 if cv.L_d-np.sqrt(cv.D_d) == 0 else ((-1*(1./3.)*(-1*(diff_L_d_wrt2-0.5*diff_D_d_wrt2/np.sqrt(cv.D_d)))*(-1*(cv.L_d-np.sqrt(cv.D_d)))**(1./3.-1)) if (cv.L_d-np.sqrt(cv.D_d))<0 else ((1./3.)*(diff_L_d_wrt2-0.5*diff_D_d_wrt2/np.sqrt(cv.D_d))*(cv.L_d-np.sqrt(cv.D_d))**(1./3.-1)))

        diff_Z1_ov_wrt1 = 0 if cv.D_d>=0 else (2*(-0.5*diff_Q_d_wrt1/np.sqrt(-1*cv.Q_d)*np.cos(1./3.*(np.arccos(cv.L_d/np.sqrt(-cv.Q_d**3)))+2*np.pi/3)+np.sqrt(-1*cv.Q_d)*(-1)*(1./3.*-1/np.sqrt(1-(cv.L_d/np.sqrt(-cv.Q_d**3))**2))*((np.sqrt(-cv.Q_d**3)*diff_L_d_wrt1-(0.5*-1*3*diff_Q_d_wrt1*cv.Q_d**2)/np.sqrt(-cv.Q_d**3)*cv.L_d)/(-cv.Q_d**3))*np.sin(1./3.*(np.arccos(cv.L_d/np.sqrt(-cv.Q_d**3)))+2*np.pi/3))-diff_a1_d_wrt1/3)
        diff_Z1_ov_wrt2 = 0 if cv.D_d>=0 else (2*(-0.5*diff_Q_d_wrt2/np.sqrt(-1*cv.Q_d)*np.cos(1./3.*(np.arccos(cv.L_d/np.sqrt(-cv.Q_d**3)))+2*np.pi/3)+np.sqrt(-1*cv.Q_d)*(-1)*(1./3.*-1/np.sqrt(1-(cv.L_d/np.sqrt(-cv.Q_d**3))**2))*((np.sqrt(-cv.Q_d**3)*diff_L_d_wrt2-(0.5*-1*3*diff_Q_d_wrt2*cv.Q_d**2)/np.sqrt(-cv.Q_d**3)*cv.L_d)/(-cv.Q_d**3))*np.sin(1./3.*(np.arccos(cv.L_d/np.sqrt(-cv.Q_d**3)))+2*np.pi/3))-diff_a1_d_wrt2/3)

        diff_Z2_ov_wrt1 = 0 if cv.D_d>=0 else (2*(-0.5*diff_Q_d_wrt1/np.sqrt(-1*cv.Q_d)*np.cos(1./3.*(np.arccos(cv.L_d/np.sqrt(-cv.Q_d**3)))+4*np.pi/3)+np.sqrt(-1*cv.Q_d)*(-1)*(1./3.*-1/np.sqrt(1-(cv.L_d/np.sqrt(-cv.Q_d**3))**2))*((np.sqrt(-cv.Q_d**3)*diff_L_d_wrt1-(0.5*-1*3*diff_Q_d_wrt1*cv.Q_d**2)/np.sqrt(-cv.Q_d**3)*cv.L_d)/(-cv.Q_d**3))*np.sin(1./3.*(np.arccos(cv.L_d/np.sqrt(-cv.Q_d**3)))+4*np.pi/3))-diff_a1_d_wrt1/3)
        diff_Z2_ov_wrt2 = 0 if cv.D_d>=0 else (2*(-0.5*diff_Q_d_wrt2/np.sqrt(-1*cv.Q_d)*np.cos(1./3.*(np.arccos(cv.L_d/np.sqrt(-cv.Q_d**3)))+4*np.pi/3)+np.sqrt(-1*cv.Q_d)*(-1)*(1./3.*-1/np.sqrt(1-(cv.L_d/np.sqrt(-cv.Q_d**3))**2))*((np.sqrt(-cv.Q_d**3)*diff_L_d_wrt2-(0.5*-1*3*diff_Q_d_wrt2*cv.Q_d**2)/np.sqrt(-cv.Q_d**3)*cv.L_d)/(-cv.Q_d**3))*np.sin(1./3.*(np.arccos(cv.L_d/np.sqrt(-cv.Q_d**3)))+4*np.pi/3))-diff_a1_d_wrt2/3)

        diff_Z3_ov_wrt1 = 0 if cv.D_d>=0 else (2*(-0.5*diff_Q_d_wrt1/np.sqrt(-1*cv.Q_d)*np.cos(1./3.*(np.arccos(cv.L_d/np.sqrt(-cv.Q_d**3))))+np.sqrt(-1*cv.Q_d)*(-1)*(1./3.*-1/np.sqrt(1-(cv.L_d/np.sqrt(-cv.Q_d**3))**2))*((np.sqrt(-cv.Q_d**3)*diff_L_d_wrt1-(0.5*-1*3*diff_Q_d_wrt1*cv.Q_d**2)/np.sqrt(-cv.Q_d**3)*cv.L_d)/(-cv.Q_d**3))*np.sin(1./3.*(np.arccos(cv.L_d/np.sqrt(-cv.Q_d**3)))))-diff_a1_d_wrt1/3)
        diff_Z3_ov_wrt2 = 0 if cv.D_d>=0 else (2*(-0.5*diff_Q_d_wrt2/np.sqrt(-1*cv.Q_d)*np.cos(1./3.*(np.arccos(cv.L_d/np.sqrt(-cv.Q_d**3))))+np.sqrt(-1*cv.Q_d)*(-1)*(1./3.*-1/np.sqrt(1-(cv.L_d/np.sqrt(-cv.Q_d**3))**2))*((np.sqrt(-cv.Q_d**3)*diff_L_d_wrt2-(0.5*-1*3*diff_Q_d_wrt2*cv.Q_d**2)/np.sqrt(-cv.Q_d**3)*cv.L_d)/(-cv.Q_d**3))*np.sin(1./3.*(np.arccos(cv.L_d/np.sqrt(-cv.Q_d**3)))))-diff_a1_d_wrt2/3)

        diff_Z1_ov_d_wrt1 = diff_S1_d_wrt1+diff_S2_d_wrt1-diff_a1_d_wrt1/3.
        diff_Z1_ov_d_wrt2 = diff_S1_d_wrt2+diff_S2_d_wrt2-diff_a1_d_wrt2/3.

        diff_Z1_ov_dd_wrt1 = ((-1*(1./3.)*(-1*diff_L_d_wrt1)*(-1*cv.L_d)**(1./3.-1)) if cv.L_d<0 else (1./3.*diff_L_d_wrt1*cv.L_d**(1./3.-1))) *2-diff_a1_d_wrt1/3
        diff_Z1_ov_dd_wrt2 = ((-1*(1./3.)*(-1*diff_L_d_wrt2)*(-1*cv.L_d)**(1./3.-1)) if cv.L_d<0 else (1./3.*diff_L_d_wrt2*cv.L_d**(1./3.-1))) *2-diff_a1_d_wrt2/3

        diff_Z2_ov_dd_wrt1 = (((-1*(1./3.)*(-1*diff_L_d_wrt1)*(-1*cv.L_d)**(1./3.-1)) if cv.L_d<0 else (1./3.*diff_L_d_wrt1*cv.L_d**(1./3.-1))) +diff_a1_d_wrt1/3) * -1.
        diff_Z2_ov_dd_wrt2 = (((-1*(1./3.)*(-1*diff_L_d_wrt2)*(-1*cv.L_d)**(1./3.-1)) if cv.L_d<0 else (1./3.*diff_L_d_wrt2*cv.L_d**(1./3.-1))) +diff_a1_d_wrt2/3) * -1.
        
        diff_Z3_ov_dd_wrt1 = diff_Z2_ov_dd_wrt1
        diff_Z3_ov_dd_wrt2 = diff_Z2_ov_dd_wrt2

        diff_final_q_wrt1 = (diff_Z1_ov_wrt1 if cv.final_q == cv.Z1_ov else (diff_Z2_ov_wrt1 if cv.final_q == cv.Z2_ov else diff_Z3_ov_wrt1)) if cv.D_d < 0 else ((diff_Z1_ov_dd_wrt1 if cv.final_q == cv.Z1_ov_dd else (diff_Z2_ov_dd_wrt1 if cv.final_q == cv.Z2_ov_dd else  diff_Z3_ov_dd_wrt1)) if cv.D_d == 0 else diff_Z1_ov_d_wrt1)
        diff_final_q_wrt2 = (diff_Z1_ov_wrt2 if cv.final_q == cv.Z1_ov else (diff_Z2_ov_wrt2 if cv.final_q == cv.Z2_ov else diff_Z3_ov_wrt2)) if cv.D_d < 0 else ((diff_Z1_ov_dd_wrt2 if cv.final_q == cv.Z1_ov_dd else (diff_Z2_ov_dd_wrt2 if cv.final_q == cv.Z2_ov_dd else  diff_Z3_ov_dd_wrt2)) if cv.D_d == 0 else diff_Z1_ov_d_wrt2)

        diff_eta_wrt1 = 0.
        diff_eta_wrt2 = -3. * (diff_final_q_wrt2 * cv.omega_final + cv.final_q * diff_omega_wrt2)/(3. * (1. + cv.omega_final * cv.final_q))**2

        diff_om_b_wrt1 = 0.
        diff_om_b_wrt2 = (diff_final_q_wrt2 * cv.eta + cv.final_q * diff_eta_wrt2)

        diff_om_a_wrt1 = 0.
        diff_om_a_wrt2 = -3. * (diff_eta_wrt2 * (1. - cv.final_q) - cv.eta * diff_final_q_wrt2)*(1. - cv.eta * (1. - cv.final_q))**2

        diff_b_wrt1 = diff_om_b_wrt1*gasConstant*ov.tc_k/(ov.pc_mpa*10**6)
        diff_b_wrt2 = diff_om_b_wrt2*gasConstant*ov.tc_k/(ov.pc_mpa*10**6)

        diff_ac_wrt1 = diff_om_a_wrt1*gasConstant**2*ov.tc_k**2/(ov.pc_mpa*10**6)
        diff_ac_wrt2 = diff_om_a_wrt2*gasConstant**2*ov.tc_k**2/(ov.pc_mpa*10**6)

        diff_ac_alpha_wrt1 = (cv.ac*diff_alpha_wrt1+diff_ac_wrt1*cv.alpha_eos)
        diff_ac_alpha_wrt2 = (cv.ac*diff_alpha_wrt2+diff_ac_wrt2*cv.alpha_eos)

        diff_u_wrt1 = 0.
        diff_u_wrt2 = (3. * diff_omega_wrt2)

        diff_w_wrt1 = 0.
        diff_w_wrt2 = 0 if cv.omega_final < 0 else -3. * diff_omega_wrt2

        diff_r2_wrt1 = 0
        diff_r2_wrt2 = 0 if (cv.u**2-4*cv.w) <= 0 else (diff_u_wrt2+0.5*(2*cv.u*diff_u_wrt2-4*diff_w_wrt2)*(cv.u**2-4*cv.w)**-0.5)/2

        diff_r1_wrt1 = 0
        diff_r1_wrt2 = 0 if (cv.u**2-4*cv.w) <= 0 else (diff_u_wrt2-0.5*(2*cv.u*diff_u_wrt2-4*diff_w_wrt2)*(cv.u**2-4*cv.w)**-0.5)/2              

        diff_A_wrt1 = diff_ac_alpha_wrt1*pressure*10**6/(gasConstant**2*ov.bp_final1**2)
        diff_A_wrt2 = diff_ac_alpha_wrt2*pressure*10**6/(gasConstant**2*ov.bp_final1**2)

        diff_B_wrt1 = 0
        diff_B_wrt2 = diff_b_wrt2*pressure*10**6/(gasConstant*ov.bp_final1)   

        diff_a1_wrt1 = 0
        diff_a1_wrt2 = ((cv.u*diff_B_wrt2+diff_u_wrt2*cv.B)-diff_B_wrt2) 

        diff_a2_wrt1 = diff_A_wrt1+(cv.w*2*cv.B*diff_B_wrt1+diff_w_wrt1*cv.B**2)-(cv.u*diff_B_wrt1+diff_u_wrt1*cv.B)-(cv.u*2*cv.B*diff_B_wrt1+diff_u_wrt1*cv.B**2)
        diff_a2_wrt2 = diff_A_wrt2+(cv.w*2*cv.B*diff_B_wrt2+diff_w_wrt2*cv.B**2)-(cv.u*diff_B_wrt2+diff_u_wrt2*cv.B)-(cv.u*2*cv.B*diff_B_wrt2+diff_u_wrt2*cv.B**2)

        diff_a3_wrt1 = -1*((cv.A*diff_B_wrt1+diff_A_wrt1*cv.B)+(cv.w*2*cv.B*diff_B_wrt1+diff_w_wrt1*cv.B**2)+(cv.w*3*cv.B**2*diff_B_wrt1+diff_w_wrt1*cv.B**3))
        diff_a3_wrt2 = -1*((cv.A*diff_B_wrt2+diff_A_wrt2*cv.B)+(cv.w*2*cv.B*diff_B_wrt2+diff_w_wrt2*cv.B**2)+(cv.w*3*cv.B**2*diff_B_wrt2+diff_w_wrt2*cv.B**3))

        diff_Q_wrt1 = (3*diff_a2_wrt1-2*cv.a1*diff_a1_wrt1)/9.
        diff_Q_wrt2 = (3*diff_a2_wrt2-2*cv.a1*diff_a1_wrt2)/9.

        diff_L_wrt1 = (9*(cv.a1*diff_a2_wrt1+diff_a1_wrt1*cv.a2)-27*diff_a3_wrt1-2*3*diff_a1_wrt1*cv.a1**2)/54.
        diff_L_wrt2 = (9*(cv.a1*diff_a2_wrt2+diff_a1_wrt2*cv.a2)-27*diff_a3_wrt2-2*3*diff_a1_wrt2*cv.a1**2)/54.

        diff_D_wrt1 = 3*diff_Q_wrt1*cv.Q**2+2*diff_L_wrt1*cv.L
        diff_D_wrt2 = 3*diff_Q_wrt2*cv.Q**2+2*diff_L_wrt2*cv.L

        diff_S1_wrt1 = 0 if cv.L+np.sqrt(cv.D) == 0 else ((-1*(1./3.)*(-1*(diff_L_wrt1+0.5*diff_D_wrt1/np.sqrt(cv.D)))*(-1*(cv.L+np.sqrt(cv.D)))**(1./3.-1)) if (cv.L+np.sqrt(cv.D))<0 else ((1./3.)*(diff_L_wrt1+0.5*diff_D_wrt1/np.sqrt(cv.D))*(cv.L+np.sqrt(cv.D))**(1./3.-1)))
        diff_S1_wrt2 = 0 if cv.L+np.sqrt(cv.D) == 0 else ((-1*(1./3.)*(-1*(diff_L_wrt2+0.5*diff_D_wrt2/np.sqrt(cv.D)))*(-1*(cv.L+np.sqrt(cv.D)))**(1./3.-1)) if (cv.L+np.sqrt(cv.D))<0 else ((1./3.)*(diff_L_wrt2+0.5*diff_D_wrt2/np.sqrt(cv.D))*(cv.L+np.sqrt(cv.D))**(1./3.-1)))

        diff_S2_wrt1 = 0 if cv.L-np.sqrt(cv.D) == 0 else ((-1*(1./3.)*(-1*(diff_L_wrt1-0.5*diff_D_wrt1/np.sqrt(cv.D)))*(-1*(cv.L-np.sqrt(cv.D)))**(1./3.-1)) if (cv.L-np.sqrt(cv.D))<0 else ((1./3.)*(diff_L_wrt1-0.5*diff_D_wrt1/np.sqrt(cv.D))*(cv.L-np.sqrt(cv.D))**(1./3.-1)))
        diff_S2_wrt2 = 0 if cv.L-np.sqrt(cv.D) == 0 else ((-1*(1./3.)*(-1*(diff_L_wrt2-0.5*diff_D_wrt2/np.sqrt(cv.D)))*(-1*(cv.L-np.sqrt(cv.D)))**(1./3.-1)) if (cv.L-np.sqrt(cv.D))<0 else ((1./3.)*(diff_L_wrt2-0.5*diff_D_wrt2/np.sqrt(cv.D))*(cv.L-np.sqrt(cv.D))**(1./3.-1)))

        diff_Z1_d_wrt1 = diff_S1_wrt1+diff_S2_wrt1-diff_a1_wrt1/3
        diff_Z1_d_wrt2 = diff_S1_wrt2+diff_S2_wrt2-diff_a1_wrt2/3

        diff_Z1_dd_wrt1 = ((-1*(1./3.)*(-1*diff_L_wrt1)*(-1*cv.L)**(1./3.-1)) if cv.L<0 else (1./3.*diff_L_wrt1*cv.L**(1./3.-1))) *2-diff_a1_wrt1/3
        diff_Z1_dd_wrt2 = ((-1*(1./3.)*(-1*diff_L_wrt2)*(-1*cv.L)**(1./3.-1)) if cv.L<0 else (1./3.*diff_L_wrt2*cv.L**(1./3.-1))) *2-diff_a1_wrt2/3

        diff_Z2_dd_wrt1 = (((-1*(1./3.)*(-1*diff_L_wrt1)*(-1*cv.L)**(1./3.-1)) if cv.L<0 else (1./3.*diff_L_wrt1*cv.L**(1./3.-1))) +diff_a1_wrt1/3) * -1.
        diff_Z2_dd_wrt2 = (((-1*(1./3.)*(-1*diff_L_wrt2)*(-1*cv.L)**(1./3.-1)) if cv.L<0 else (1./3.*diff_L_wrt2*cv.L**(1./3.-1))) +diff_a1_wrt2/3) * -1.
        
        diff_Z3_dd_wrt1 = diff_Z2_dd_wrt1
        diff_Z3_dd_wrt2 = diff_Z2_dd_wrt2

        diff_Z1_wrt1 = 0 if cv.D>=0 or cv.Q>=0 else (2*(-0.5*diff_Q_wrt1/np.sqrt(-1*cv.Q)*np.cos(1./3.*(np.arccos( 1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3) ))+2*np.pi/3)+np.sqrt(-1*cv.Q)*(-1)*(1./3.*-1/np.sqrt(1-(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3))**2))*((np.sqrt(-cv.Q**3)*diff_L_wrt1-(0.5*-1*3*diff_Q_wrt1*cv.Q**2)/np.sqrt(-cv.Q**3)*cv.L)/(-cv.Q**3))*np.sin(1./3.*(np.arccos(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3)))+2*np.pi/3))-diff_a1_wrt1/3)
        diff_Z1_wrt2 = 0 if cv.D>=0 or cv.Q>=0 else (2*(-0.5*diff_Q_wrt2/np.sqrt(-1*cv.Q)*np.cos(1./3.*(np.arccos( 1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3) ))+2*np.pi/3)+np.sqrt(-1*cv.Q)*(-1)*(1./3.*-1/np.sqrt(1-(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3))**2))*((np.sqrt(-cv.Q**3)*diff_L_wrt2-(0.5*-1*3*diff_Q_wrt2*cv.Q**2)/np.sqrt(-cv.Q**3)*cv.L)/(-cv.Q**3))*np.sin(1./3.*(np.arccos(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3)))+2*np.pi/3))-diff_a1_wrt2/3)

        diff_Z2_wrt1 = 0 if cv.D>=0 or cv.Q>=0 else (2*(-0.5*diff_Q_wrt1/np.sqrt(-1*cv.Q)*np.cos(1./3.*(np.arccos( 1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3) ))+4*np.pi/3)+np.sqrt(-1*cv.Q)*(-1)*(1./3.*-1/np.sqrt(1-(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3))**2))*((np.sqrt(-cv.Q**3)*diff_L_wrt1-(0.5*-1*3*diff_Q_wrt1*cv.Q**2)/np.sqrt(-cv.Q**3)*cv.L)/(-cv.Q**3))*np.sin(1./3.*(np.arccos(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3)))+4*np.pi/3))-diff_a1_wrt1/3)
        diff_Z2_wrt2 = 0 if cv.D>=0 or cv.Q>=0 else (2*(-0.5*diff_Q_wrt2/np.sqrt(-1*cv.Q)*np.cos(1./3.*(np.arccos( 1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3) ))+4*np.pi/3)+np.sqrt(-1*cv.Q)*(-1)*(1./3.*-1/np.sqrt(1-(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3))**2))*((np.sqrt(-cv.Q**3)*diff_L_wrt2-(0.5*-1*3*diff_Q_wrt2*cv.Q**2)/np.sqrt(-cv.Q**3)*cv.L)/(-cv.Q**3))*np.sin(1./3.*(np.arccos(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3)))+4*np.pi/3))-diff_a1_wrt2/3)

        diff_Z3_wrt1 = 0 if cv.D>=0 or cv.Q>=0 else (2*(-0.5*diff_Q_wrt1/np.sqrt(-1*cv.Q)*np.cos(1./3.*(np.arccos( 1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3) )))+np.sqrt(-1*cv.Q)*(-1)*(1./3.*-1/np.sqrt(1-(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3))**2))*((np.sqrt(-cv.Q**3)*diff_L_wrt1-(0.5*-1*3*diff_Q_wrt1*cv.Q**2)/np.sqrt(-cv.Q**3)*cv.L)/(-cv.Q**3))*np.sin(1./3.*(np.arccos(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3)))))-diff_a1_wrt1/3)
        diff_Z3_wrt2 = 0 if cv.D>=0 or cv.Q>=0 else (2*(-0.5*diff_Q_wrt2/np.sqrt(-1*cv.Q)*np.cos(1./3.*(np.arccos( 1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3) )))+np.sqrt(-1*cv.Q)*(-1)*(1./3.*-1/np.sqrt(1-(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3))**2))*((np.sqrt(-cv.Q**3)*diff_L_wrt2-(0.5*-1*3*diff_Q_wrt2*cv.Q**2)/np.sqrt(-cv.Q**3)*cv.L)/(-cv.Q**3))*np.sin(1./3.*(np.arccos(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3)))))-diff_a1_wrt2/3)

        diff_ZL_wrt1 = (diff_Z1_wrt1 if cv.ZL==cv.Z1 else (diff_Z2_wrt1 if cv.ZL==cv.Z2 else  diff_Z3_wrt1)) if cv.D<0 else ((diff_Z1_dd_wrt1 if cv.ZL==cv.Z1_dd else (diff_Z2_dd_wrt1 if cv.ZL==cv.Z2_dd else diff_Z3_dd_wrt1)) if cv.D==0 else diff_Z1_d_wrt1)
        diff_ZL_wrt2 = (diff_Z1_wrt2 if cv.ZL==cv.Z1 else (diff_Z2_wrt2 if cv.ZL==cv.Z2 else  diff_Z3_wrt2)) if cv.D<0 else ((diff_Z1_dd_wrt2 if cv.ZL==cv.Z1_dd else (diff_Z2_dd_wrt2 if cv.ZL==cv.Z2_dd else diff_Z3_dd_wrt2)) if cv.D==0 else diff_Z1_d_wrt2)

        diff_ZV_wrt1 = (diff_Z1_wrt1 if cv.ZV==cv.Z1 else (diff_Z2_wrt1 if cv.ZV==cv.Z2 else  diff_Z3_wrt1)) if cv.D<0 else ((diff_Z1_dd_wrt1 if cv.ZV==cv.Z1_dd else (diff_Z2_dd_wrt1 if cv.ZV==cv.Z2_dd else diff_Z3_dd_wrt1)) if cv.D==0 else 0)
        diff_ZV_wrt2 = (diff_Z1_wrt2 if cv.ZV==cv.Z1 else (diff_Z2_wrt2 if cv.ZV==cv.Z2 else  diff_Z3_wrt2)) if cv.D<0 else ((diff_Z1_dd_wrt2 if cv.ZV==cv.Z1_dd else (diff_Z2_dd_wrt2 if cv.ZV==cv.Z2_dd else diff_Z3_dd_wrt2)) if cv.D==0 else 0)

        diff_phl_wrt1 = 0 if (cv.ZL-cv.B)<=0 or cv.B*(cv.r2 - cv.r1) == 0 or (cv.ZL + cv.r1 * cv.B) == 0 or (cv.ZL + cv.r2 * cv.B) == 0 or (cv.ZL + cv.r2 * cv.B) * (cv.ZL + cv.r1 * cv.B) < 0  else ((diff_ZL_wrt1)-(diff_ZL_wrt1-diff_B_wrt1)/(cv.ZL-cv.B)-((((cv.B*(cv.r2-cv.r1))*diff_A_wrt1-(diff_B_wrt1*(cv.r2-cv.r1)+cv.B*(diff_r2_wrt1-diff_r1_wrt1))*cv.A)/(cv.B*(cv.r2-cv.r1))**2)*np.log((cv.ZL+cv.r2*cv.B)/(cv.ZL+cv.r1*cv.B))+(cv.A/(cv.B*(cv.r2-cv.r1)))*1/((cv.ZL+cv.r2*cv.B)/(cv.ZL+cv.r1*cv.B))*(((cv.ZL+cv.r1*cv.B)*(diff_ZL_wrt1+diff_r2_wrt1*cv.B+cv.r2*diff_B_wrt1)-(cv.ZL+cv.r2*cv.B)*(diff_ZL_wrt1+diff_r1_wrt1*cv.B+cv.r1*diff_B_wrt1))/(cv.ZL+cv.r1*cv.B)**2)))
        diff_phl_wrt2 = 0 if (cv.ZL-cv.B)<=0 or cv.B*(cv.r2 - cv.r1) == 0 or (cv.ZL + cv.r1 * cv.B) == 0 or (cv.ZL + cv.r2 * cv.B) == 0 or (cv.ZL + cv.r2 * cv.B) * (cv.ZL + cv.r1 * cv.B) < 0 else ((diff_ZL_wrt2)-(diff_ZL_wrt2-diff_B_wrt2)/(cv.ZL-cv.B)-((((cv.B*(cv.r2-cv.r1))*diff_A_wrt2-(diff_B_wrt2*(cv.r2-cv.r1)+cv.B*(diff_r2_wrt2-diff_r1_wrt2))*cv.A)/(cv.B*(cv.r2-cv.r1))**2)*np.log((cv.ZL+cv.r2*cv.B)/(cv.ZL+cv.r1*cv.B))+(cv.A/(cv.B*(cv.r2-cv.r1)))*1/((cv.ZL+cv.r2*cv.B)/(cv.ZL+cv.r1*cv.B))*(((cv.ZL+cv.r1*cv.B)*(diff_ZL_wrt2+diff_r2_wrt2*cv.B+cv.r2*diff_B_wrt2)-(cv.ZL+cv.r2*cv.B)*(diff_ZL_wrt2+diff_r1_wrt2*cv.B+cv.r1*diff_B_wrt2))/(cv.ZL+cv.r1*cv.B)**2)))

        diff_phv_wrt1 = 0 if (cv.ZV-cv.B)<=0 or cv.B*(cv.r2 - cv.r1) == 0 or (cv.ZV + cv.r1 * cv.B) == 0 or (cv.ZV + cv.r2 * cv.B) == 0 or (cv.ZV + cv.r2 * cv.B) * (cv.ZV + cv.r1 * cv.B) < 0 else ((diff_ZV_wrt1)-(diff_ZV_wrt1-diff_B_wrt1)/(cv.ZV-cv.B)-((((cv.B*(cv.r2-cv.r1))*diff_A_wrt1-(diff_B_wrt1*(cv.r2-cv.r1)+cv.B*(diff_r2_wrt1-diff_r1_wrt1))*cv.A)/(cv.B*(cv.r2-cv.r1))**2)*np.log((cv.ZV+cv.r2*cv.B)/(cv.ZV+cv.r1*cv.B))+(cv.A/(cv.B*(cv.r2-cv.r1)))*1/((cv.ZV+cv.r2*cv.B)/(cv.ZV+cv.r1*cv.B))*(((cv.ZV+cv.r1*cv.B)*(diff_ZV_wrt1+diff_r2_wrt1*cv.B+cv.r2*diff_B_wrt1)-(cv.ZV+cv.r2*cv.B)*(diff_ZV_wrt1+diff_r1_wrt1*cv.B+cv.r1*diff_B_wrt1))/(cv.ZV+cv.r1*cv.B)**2)))
        diff_phv_wrt2 = 0 if (cv.ZV-cv.B)<=0 or cv.B*(cv.r2 - cv.r1) == 0 or (cv.ZV + cv.r1 * cv.B) == 0 or (cv.ZV + cv.r2 * cv.B) == 0 or (cv.ZV + cv.r2 * cv.B) * (cv.ZV + cv.r1 * cv.B) < 0 else ((diff_ZV_wrt2)-(diff_ZV_wrt2-diff_B_wrt2)/(cv.ZV-cv.B)-((((cv.B*(cv.r2-cv.r1))*diff_A_wrt2-(diff_B_wrt2*(cv.r2-cv.r1)+cv.B*(diff_r2_wrt2-diff_r1_wrt2))*cv.A)/(cv.B*(cv.r2-cv.r1))**2)*np.log((cv.ZV+cv.r2*cv.B)/(cv.ZV+cv.r1*cv.B))+(cv.A/(cv.B*(cv.r2-cv.r1)))*1/((cv.ZV+cv.r2*cv.B)/(cv.ZV+cv.r1*cv.B))*(((cv.ZV+cv.r1*cv.B)*(diff_ZV_wrt2+diff_r2_wrt2*cv.B+cv.r2*diff_B_wrt2)-(cv.ZV+cv.r2*cv.B)*(diff_ZV_wrt2+diff_r1_wrt2*cv.B+cv.r1*diff_B_wrt2))/(cv.ZV+cv.r1*cv.B)**2)))

        diff_ph_diff_wrt1 = np.sign(cv.phl-cv.phv)*(diff_phl_wrt1-diff_phv_wrt1)
        diff_ph_diff_wrt2 = np.sign(cv.phl-cv.phv)*(diff_phl_wrt2-diff_phv_wrt2)

        diff_t1_wrt1 = gasConstant*ov.bp_final1*(diff_ZV_wrt1-diff_ZL_wrt1)
        diff_t1_wrt2 = gasConstant*ov.bp_final1*(diff_ZV_wrt2-diff_ZL_wrt2)

        diff_t2_wrt1 = 0 if cv.b == 0 or (cv.u**2-4*cv.w) <= 0 else ((cv.b*np.sqrt(cv.u**2-4*cv.w))*(diff_ac_wrt1*(ov.bp_final1*cv.diff_alpha_wrt_temp-cv.alpha_eos)+cv.ac*(ov.bp_final1*diff_diff_alpha_wrt1-diff_alpha_wrt1))-(diff_b_wrt1*np.sqrt(cv.u**2-4*cv.w)+cv.b*0.5*(2*cv.u*diff_u_wrt1-4*diff_w_wrt1)/np.sqrt(cv.u**2-4*cv.w))*(cv.ac*(ov.bp_final1*cv.diff_alpha_wrt_temp-cv.alpha_eos)))/(cv.b*np.sqrt(cv.u**2-4*cv.w))**2
        diff_t2_wrt2 = 0 if cv.b == 0 or (cv.u**2-4*cv.w) <= 0 else ((cv.b*np.sqrt(cv.u**2-4*cv.w))*(diff_ac_wrt2*(ov.bp_final1*cv.diff_alpha_wrt_temp-cv.alpha_eos)+cv.ac*(ov.bp_final1*diff_diff_alpha_wrt2-diff_alpha_wrt2))-(diff_b_wrt2*np.sqrt(cv.u**2-4*cv.w)+cv.b*0.5*(2*cv.u*diff_u_wrt2-4*diff_w_wrt2)/np.sqrt(cv.u**2-4*cv.w))*(cv.ac*(ov.bp_final1*cv.diff_alpha_wrt_temp-cv.alpha_eos)))/(cv.b*np.sqrt(cv.u**2-4*cv.w))**2

        diff_t3_wrt1 = 0 if (cv.ZV+cv.B*cv.r2) == 0 or (cv.ZV+cv.B*cv.r1) == 0 or (cv.ZV+cv.B*cv.r2)*(cv.ZV+cv.B*cv.r1) < 0 else (((cv.ZV+cv.B*cv.r1)/(cv.ZV+cv.B*cv.r2))*((cv.ZV+cv.B*cv.r1)*(diff_ZV_wrt1+diff_B_wrt1*cv.r2+cv.B*diff_r2_wrt1)-(diff_ZV_wrt1+diff_B_wrt1*cv.r1+cv.B*diff_r1_wrt1)*(cv.ZV+cv.B*cv.r2))/(cv.ZV+cv.B*cv.r1)**2)
        diff_t3_wrt2 = 0 if (cv.ZV+cv.B*cv.r2) == 0 or (cv.ZV+cv.B*cv.r1) == 0 or (cv.ZV+cv.B*cv.r2)*(cv.ZV+cv.B*cv.r1) < 0 else (((cv.ZV+cv.B*cv.r1)/(cv.ZV+cv.B*cv.r2))*((cv.ZV+cv.B*cv.r1)*(diff_ZV_wrt2+diff_B_wrt2*cv.r2+cv.B*diff_r2_wrt2)-(diff_ZV_wrt2+diff_B_wrt2*cv.r1+cv.B*diff_r1_wrt2)*(cv.ZV+cv.B*cv.r2))/(cv.ZV+cv.B*cv.r1)**2)

        diff_t4_wrt1 = 0 if (cv.ZL+cv.B*cv.r2) == 0 or (cv.ZL+cv.B*cv.r1) == 0 or (cv.ZL+cv.B*cv.r2)*(cv.ZL+cv.B*cv.r1) < 0 else (((cv.ZL+cv.B*cv.r1)/(cv.ZL+cv.B*cv.r2))*((cv.ZL+cv.B*cv.r1)*(diff_ZL_wrt1+diff_B_wrt1*cv.r2+cv.B*diff_r2_wrt1)-(diff_ZL_wrt1+diff_B_wrt1*cv.r1+cv.B*diff_r1_wrt1)*(cv.ZL+cv.B*cv.r2))/(cv.ZL+cv.B*cv.r1)**2)
        diff_t4_wrt2 = 0 if (cv.ZL+cv.B*cv.r2) == 0 or (cv.ZL+cv.B*cv.r1) == 0 or (cv.ZL+cv.B*cv.r2)*(cv.ZL+cv.B*cv.r1) < 0 else (((cv.ZL+cv.B*cv.r1)/(cv.ZL+cv.B*cv.r2))*((cv.ZL+cv.B*cv.r1)*(diff_ZL_wrt2+diff_B_wrt2*cv.r2+cv.B*diff_r2_wrt2)-(diff_ZL_wrt2+diff_B_wrt2*cv.r1+cv.B*diff_r1_wrt2)*(cv.ZL+cv.B*cv.r2))/(cv.ZL+cv.B*cv.r1)**2)

        diff_hv_bp_wrt1 = (diff_t1_wrt1+diff_t2_wrt1*(cv.t3-cv.t4)+cv.t2*(diff_t3_wrt1-diff_t4_wrt1))/1000.
        diff_hv_bp_wrt2 = (diff_t1_wrt2+diff_t2_wrt2*(cv.t3-cv.t4)+cv.t2*(diff_t3_wrt2-diff_t4_wrt2))/1000.

        diff_vb_wrt1 = diff_ZL_wrt1*gasConstant*ov.bp_final1/pressure
        diff_vb_wrt2 = diff_ZL_wrt2*gasConstant*ov.bp_final1/pressure

        diff_wrt1 = self.scales[0]*(2*(ov.hv_bp-cv.hv_bp)/ov.hv_bp**2)*(-1.)*diff_hv_bp_wrt1+self.scales[1]*(2*(ov.vb-cv.vb)/ov.vb**2)*(-1)*diff_vb_wrt1+self.scales[2]*2*(max(0,(cv.ph_diff-10**-7)))*(0 if(cv.ph_diff-10**-7)<=0 else diff_ph_diff_wrt1)
        diff_wrt2 = self.scales[0]*(2*(ov.hv_bp-cv.hv_bp)/ov.hv_bp**2)*(-1.)*diff_hv_bp_wrt2+self.scales[1]*(2*(ov.vb-cv.vb)/ov.vb**2)*(-1)*diff_vb_wrt2+self.scales[2]*2*(max(0,(cv.ph_diff-10**-7)))*(0 if(cv.ph_diff-10**-7)<=0 else diff_ph_diff_wrt2)

        diff_wrt1 = (np.sign(diff_wrt1)*10**7) if np.abs(diff_wrt1)>10**7 else diff_wrt1
        diff_wrt2 = (np.sign(diff_wrt2)*10**7) if np.abs(diff_wrt2)>10**7 else diff_wrt2  
        return np.array([diff_wrt1, diff_wrt2])

    def pt(self, ov, cv):
        diff_alpha_wrt1 = ov.alpha_pr
        diff_alpha_wrt2 = 0
        
        diff_diff_alpha_wrt1 = ov.alpha_pr*(((2*ov.alpha_m-2)/(ov.bp_final1/ov.tc_k)+ov.alpha_l*(-2*ov.alpha_m)*(ov.bp_final1/ov.tc_k)**(2*ov.alpha_m-1))/ov.tc_k)
        diff_diff_alpha_wrt2 = 0

        diff_ziac_wrt1 = 0
        diff_ziac_wrt2 = ov.zc_final

        diff_om_c_wrt1 = 0
        diff_om_c_wrt2 = -3. * diff_ziac_wrt2

        diff_a1_d_wrt1 = 0
        diff_a1_d_wrt2 = -3. * diff_ziac_wrt2

        diff_a2_d_wrt1 = 0
        diff_a2_d_wrt2 = 3. * 2. * cv.ziac * diff_ziac_wrt2

        diff_a3_d_wrt1 = 0
        diff_a3_d_wrt2 = -1. * 3. * cv.ziac**2 * diff_ziac_wrt2

        diff_Q_d_wrt1 = 0
        diff_Q_d_wrt2 = (3. * diff_a2_d_wrt2 - 2. * cv.a1_d * diff_a1_d_wrt2)/9.

        diff_L_d_wrt1 = 0
        diff_L_d_wrt2 = (9. * (diff_a1_d_wrt2 * cv.a2_d + cv.a1_d * diff_a2_d_wrt2) - 27. * diff_a3_d_wrt2 -2. * 3. * diff_a1_d_wrt2 * cv.a1_d**2)/54. 

        diff_D_d_wrt1 = 0
        diff_D_d_wrt2 = 3. * diff_Q_d_wrt2 * cv.Q_d**2 + 2. * diff_L_d_wrt2 * cv.L_d

        diff_S1_d_wrt1 = 0 if cv.L_d+np.sqrt(cv.D_d) == 0 else ((-1*(1./3.)*(-1*(diff_L_d_wrt1+0.5*diff_D_d_wrt1/np.sqrt(cv.D_d)))*(-1*(cv.L_d+np.sqrt(cv.D_d)))**(1./3.-1)) if (cv.L_d+np.sqrt(cv.D_d))<0 else ((1./3.)*(diff_L_d_wrt1+0.5*diff_D_d_wrt1/np.sqrt(cv.D_d))*(cv.L_d+np.sqrt(cv.D_d))**(1./3.-1)))
        diff_S1_d_wrt2 = 0 if cv.L_d+np.sqrt(cv.D_d) == 0 else ((-1*(1./3.)*(-1*(diff_L_d_wrt2+0.5*diff_D_d_wrt2/np.sqrt(cv.D_d)))*(-1*(cv.L_d+np.sqrt(cv.D_d)))**(1./3.-1)) if (cv.L_d+np.sqrt(cv.D_d))<0 else ((1./3.)*(diff_L_d_wrt2+0.5*diff_D_d_wrt2/np.sqrt(cv.D_d))*(cv.L_d+np.sqrt(cv.D_d))**(1./3.-1)))

        diff_S2_d_wrt1 = 0 if cv.L_d-np.sqrt(cv.D_d) == 0 else ((-1*(1./3.)*(-1*(diff_L_d_wrt1-0.5*diff_D_d_wrt1/np.sqrt(cv.D_d)))*(-1*(cv.L_d-np.sqrt(cv.D_d)))**(1./3.-1)) if (cv.L_d-np.sqrt(cv.D_d))<0 else ((1./3.)*(diff_L_d_wrt1-0.5*diff_D_d_wrt1/np.sqrt(cv.D_d))*(cv.L_d-np.sqrt(cv.D_d))**(1./3.-1)))
        diff_S2_d_wrt2 = 0 if cv.L_d-np.sqrt(cv.D_d) == 0 else ((-1*(1./3.)*(-1*(diff_L_d_wrt2-0.5*diff_D_d_wrt2/np.sqrt(cv.D_d)))*(-1*(cv.L_d-np.sqrt(cv.D_d)))**(1./3.-1)) if (cv.L_d-np.sqrt(cv.D_d))<0 else ((1./3.)*(diff_L_d_wrt2-0.5*diff_D_d_wrt2/np.sqrt(cv.D_d))*(cv.L_d-np.sqrt(cv.D_d))**(1./3.-1)))

        diff_Z1_ov_wrt1 = 0 if cv.D_d>=0 else (2*(-0.5*diff_Q_d_wrt1/np.sqrt(-1*cv.Q_d)*np.cos(1./3.*(np.arccos(cv.L_d/np.sqrt(-cv.Q_d**3)))+2*np.pi/3)+np.sqrt(-1*cv.Q_d)*(-1)*(1./3.*-1/np.sqrt(1-(cv.L_d/np.sqrt(-cv.Q_d**3))**2))*((np.sqrt(-cv.Q_d**3)*diff_L_d_wrt1-(0.5*-1*3*diff_Q_d_wrt1*cv.Q_d**2)/np.sqrt(-cv.Q_d**3)*cv.L_d)/(-cv.Q_d**3))*np.sin(1./3.*(np.arccos(cv.L_d/np.sqrt(-cv.Q_d**3)))+2*np.pi/3))-diff_a1_d_wrt1/3)
        diff_Z1_ov_wrt2 = 0 if cv.D_d>=0 else (2*(-0.5*diff_Q_d_wrt2/np.sqrt(-1*cv.Q_d)*np.cos(1./3.*(np.arccos(cv.L_d/np.sqrt(-cv.Q_d**3)))+2*np.pi/3)+np.sqrt(-1*cv.Q_d)*(-1)*(1./3.*-1/np.sqrt(1-(cv.L_d/np.sqrt(-cv.Q_d**3))**2))*((np.sqrt(-cv.Q_d**3)*diff_L_d_wrt2-(0.5*-1*3*diff_Q_d_wrt2*cv.Q_d**2)/np.sqrt(-cv.Q_d**3)*cv.L_d)/(-cv.Q_d**3))*np.sin(1./3.*(np.arccos(cv.L_d/np.sqrt(-cv.Q_d**3)))+2*np.pi/3))-diff_a1_d_wrt2/3)

        diff_Z2_ov_wrt1 = 0 if cv.D_d>=0 else (2*(-0.5*diff_Q_d_wrt1/np.sqrt(-1*cv.Q_d)*np.cos(1./3.*(np.arccos(cv.L_d/np.sqrt(-cv.Q_d**3)))+4*np.pi/3)+np.sqrt(-1*cv.Q_d)*(-1)*(1./3.*-1/np.sqrt(1-(cv.L_d/np.sqrt(-cv.Q_d**3))**2))*((np.sqrt(-cv.Q_d**3)*diff_L_d_wrt1-(0.5*-1*3*diff_Q_d_wrt1*cv.Q_d**2)/np.sqrt(-cv.Q_d**3)*cv.L_d)/(-cv.Q_d**3))*np.sin(1./3.*(np.arccos(cv.L_d/np.sqrt(-cv.Q_d**3)))+4*np.pi/3))-diff_a1_d_wrt1/3)
        diff_Z2_ov_wrt2 = 0 if cv.D_d>=0 else (2*(-0.5*diff_Q_d_wrt2/np.sqrt(-1*cv.Q_d)*np.cos(1./3.*(np.arccos(cv.L_d/np.sqrt(-cv.Q_d**3)))+4*np.pi/3)+np.sqrt(-1*cv.Q_d)*(-1)*(1./3.*-1/np.sqrt(1-(cv.L_d/np.sqrt(-cv.Q_d**3))**2))*((np.sqrt(-cv.Q_d**3)*diff_L_d_wrt2-(0.5*-1*3*diff_Q_d_wrt2*cv.Q_d**2)/np.sqrt(-cv.Q_d**3)*cv.L_d)/(-cv.Q_d**3))*np.sin(1./3.*(np.arccos(cv.L_d/np.sqrt(-cv.Q_d**3)))+4*np.pi/3))-diff_a1_d_wrt2/3)

        diff_Z3_ov_wrt1 = 0 if cv.D_d>=0 else (2*(-0.5*diff_Q_d_wrt1/np.sqrt(-1*cv.Q_d)*np.cos(1./3.*(np.arccos(cv.L_d/np.sqrt(-cv.Q_d**3))))+np.sqrt(-1*cv.Q_d)*(-1)*(1./3.*-1/np.sqrt(1-(cv.L_d/np.sqrt(-cv.Q_d**3))**2))*((np.sqrt(-cv.Q_d**3)*diff_L_d_wrt1-(0.5*-1*3*diff_Q_d_wrt1*cv.Q_d**2)/np.sqrt(-cv.Q_d**3)*cv.L_d)/(-cv.Q_d**3))*np.sin(1./3.*(np.arccos(cv.L_d/np.sqrt(-cv.Q_d**3)))))-diff_a1_d_wrt1/3)
        diff_Z3_ov_wrt2 = 0 if cv.D_d>=0 else (2*(-0.5*diff_Q_d_wrt2/np.sqrt(-1*cv.Q_d)*np.cos(1./3.*(np.arccos(cv.L_d/np.sqrt(-cv.Q_d**3))))+np.sqrt(-1*cv.Q_d)*(-1)*(1./3.*-1/np.sqrt(1-(cv.L_d/np.sqrt(-cv.Q_d**3))**2))*((np.sqrt(-cv.Q_d**3)*diff_L_d_wrt2-(0.5*-1*3*diff_Q_d_wrt2*cv.Q_d**2)/np.sqrt(-cv.Q_d**3)*cv.L_d)/(-cv.Q_d**3))*np.sin(1./3.*(np.arccos(cv.L_d/np.sqrt(-cv.Q_d**3)))))-diff_a1_d_wrt2/3)

        diff_Z1_ov_d_wrt1 = diff_S1_d_wrt1+diff_S2_d_wrt1-diff_a1_d_wrt1/3.
        diff_Z1_ov_d_wrt2 = diff_S1_d_wrt2+diff_S2_d_wrt2-diff_a1_d_wrt2/3.

        diff_Z1_ov_dd_wrt1 = ((-1*(1./3.)*(-1*diff_L_d_wrt1)*(-1*cv.L_d)**(1./3.-1)) if cv.L_d<0 else (1./3.*diff_L_d_wrt1*cv.L_d**(1./3.-1))) *2-diff_a1_d_wrt1/3
        diff_Z1_ov_dd_wrt2 = ((-1*(1./3.)*(-1*diff_L_d_wrt2)*(-1*cv.L_d)**(1./3.-1)) if cv.L_d<0 else (1./3.*diff_L_d_wrt2*cv.L_d**(1./3.-1))) *2-diff_a1_d_wrt2/3

        diff_Z2_ov_dd_wrt1 = (((-1*(1./3.)*(-1*diff_L_d_wrt1)*(-1*cv.L_d)**(1./3.-1)) if cv.L_d<0 else (1./3.*diff_L_d_wrt1*cv.L_d**(1./3.-1))) +diff_a1_d_wrt1/3) * -1.
        diff_Z2_ov_dd_wrt2 = (((-1*(1./3.)*(-1*diff_L_d_wrt2)*(-1*cv.L_d)**(1./3.-1)) if cv.L_d<0 else (1./3.*diff_L_d_wrt2*cv.L_d**(1./3.-1))) +diff_a1_d_wrt2/3) * -1.
        
        diff_Z3_ov_dd_wrt1 = diff_Z2_ov_dd_wrt1
        diff_Z3_ov_dd_wrt2 = diff_Z2_ov_dd_wrt2

        diff_om_b_wrt1 = (diff_Z1_ov_wrt1 if cv.om_b == cv.Z1_ov else (diff_Z2_ov_wrt1 if cv.om_b == cv.Z2_ov else diff_Z3_ov_wrt1)) if cv.D_d < 0 else ((diff_Z1_ov_dd_wrt1 if cv.om_b == cv.Z1_ov_dd else (diff_Z2_ov_dd_wrt1 if cv.om_b == cv.Z2_ov_dd else  diff_Z3_ov_dd_wrt1)) if cv.D_d == 0 else diff_Z1_ov_d_wrt1)
        diff_om_b_wrt2 = (diff_Z1_ov_wrt2 if cv.om_b == cv.Z1_ov else (diff_Z2_ov_wrt2 if cv.om_b == cv.Z2_ov else diff_Z3_ov_wrt2)) if cv.D_d < 0 else ((diff_Z1_ov_dd_wrt2 if cv.om_b == cv.Z1_ov_dd else (diff_Z2_ov_dd_wrt2 if cv.om_b == cv.Z2_ov_dd else  diff_Z3_ov_dd_wrt2)) if cv.D_d == 0 else diff_Z1_ov_d_wrt2)

        diff_om_a_wrt1 = 3.*2.*diff_ziac_wrt1*cv.ziac+3.*((1.-2.*cv.ziac)*diff_om_b_wrt1+(-2.*diff_ziac_wrt1)*cv.om_b)+2.*diff_om_b_wrt1*cv.om_b-3.*diff_ziac_wrt1
        diff_om_a_wrt2 = 3.*2.*diff_ziac_wrt2*cv.ziac+3.*((1.-2.*cv.ziac)*diff_om_b_wrt2+(-2.*diff_ziac_wrt2)*cv.om_b)+2.*diff_om_b_wrt2*cv.om_b-3.*diff_ziac_wrt2

        diff_b_wrt1 = diff_om_b_wrt1*gasConstant*ov.tc_k/(ov.pc_mpa*10**6)
        diff_b_wrt2 = diff_om_b_wrt2*gasConstant*ov.tc_k/(ov.pc_mpa*10**6)

        diff_c_wrt1 = diff_om_c_wrt1*gasConstant*ov.tc_k/(ov.pc_mpa*10**6)
        diff_c_wrt2 = diff_om_c_wrt2*gasConstant*ov.tc_k/(ov.pc_mpa*10**6)

        diff_ac_wrt1 = diff_om_a_wrt1*gasConstant**2*ov.tc_k**2/(ov.pc_mpa*10**6)
        diff_ac_wrt2 = diff_om_a_wrt2*gasConstant**2*ov.tc_k**2/(ov.pc_mpa*10**6)

        diff_ac_alpha_wrt1 = (cv.ac*diff_alpha_wrt1+diff_ac_wrt1*cv.alpha_eos)
        diff_ac_alpha_wrt2 = (cv.ac*diff_alpha_wrt2+diff_ac_wrt2*cv.alpha_eos)

        diff_u_wrt1 = (cv.b*(diff_b_wrt1+diff_c_wrt1)-(cv.b+cv.c)*diff_b_wrt1)/cv.b**2
        diff_u_wrt2 = (cv.b*(diff_b_wrt2+diff_c_wrt2)-(cv.b+cv.c)*diff_b_wrt2)/cv.b**2

        diff_w_wrt1 = 0 if cv.b == 0 else (-1*(diff_c_wrt1*cv.b-cv.c*diff_b_wrt1)/cv.b**2)
        diff_w_wrt2 = 0 if cv.b == 0 else (-1*(diff_c_wrt2*cv.b-cv.c*diff_b_wrt2)/cv.b**2)

        diff_r2_wrt1 = 0 if (cv.u**2-4*cv.w) <= 0 else (diff_u_wrt1+0.5*(2*cv.u*diff_u_wrt1-4*diff_w_wrt1)*(cv.u**2-4*cv.w)**-0.5)/2
        diff_r2_wrt2 = 0 if (cv.u**2-4*cv.w) <= 0 else (diff_u_wrt2+0.5*(2*cv.u*diff_u_wrt2-4*diff_w_wrt2)*(cv.u**2-4*cv.w)**-0.5)/2

        diff_r1_wrt1 = 0 if (cv.u**2-4*cv.w) <= 0 else (diff_u_wrt1-0.5*(2*cv.u*diff_u_wrt1-4*diff_w_wrt1)*(cv.u**2-4*cv.w)**-0.5)/2  
        diff_r1_wrt2 = 0 if (cv.u**2-4*cv.w) <= 0 else (diff_u_wrt2-0.5*(2*cv.u*diff_u_wrt2-4*diff_w_wrt2)*(cv.u**2-4*cv.w)**-0.5)/2              

        diff_A_wrt1 = diff_ac_alpha_wrt1*pressure*10**6/(gasConstant**2*ov.bp_final1**2)
        diff_A_wrt2 = diff_ac_alpha_wrt2*pressure*10**6/(gasConstant**2*ov.bp_final1**2)

        diff_B_wrt1 = 0
        diff_B_wrt2 = diff_b_wrt2*pressure*10**6/(gasConstant*ov.bp_final1)   

        diff_a1_wrt1 = 0
        diff_a1_wrt2 = ((cv.u*diff_B_wrt2+diff_u_wrt2*cv.B)-diff_B_wrt2) 

        diff_a2_wrt1 = diff_A_wrt1+(cv.w*2*cv.B*diff_B_wrt1+diff_w_wrt1*cv.B**2)-(cv.u*diff_B_wrt1+diff_u_wrt1*cv.B)-(cv.u*2*cv.B*diff_B_wrt1+diff_u_wrt1*cv.B**2)
        diff_a2_wrt2 = diff_A_wrt2+(cv.w*2*cv.B*diff_B_wrt2+diff_w_wrt2*cv.B**2)-(cv.u*diff_B_wrt2+diff_u_wrt2*cv.B)-(cv.u*2*cv.B*diff_B_wrt2+diff_u_wrt2*cv.B**2)

        diff_a3_wrt1 = -1*((cv.A*diff_B_wrt1+diff_A_wrt1*cv.B)+(cv.w*2*cv.B*diff_B_wrt1+diff_w_wrt1*cv.B**2)+(cv.w*3*cv.B**2*diff_B_wrt1+diff_w_wrt1*cv.B**3))
        diff_a3_wrt2 = -1*((cv.A*diff_B_wrt2+diff_A_wrt2*cv.B)+(cv.w*2*cv.B*diff_B_wrt2+diff_w_wrt2*cv.B**2)+(cv.w*3*cv.B**2*diff_B_wrt2+diff_w_wrt2*cv.B**3))

        diff_Q_wrt1 = (3*diff_a2_wrt1-2*cv.a1*diff_a1_wrt1)/9.
        diff_Q_wrt2 = (3*diff_a2_wrt2-2*cv.a1*diff_a1_wrt2)/9.

        diff_L_wrt1 = (9*(cv.a1*diff_a2_wrt1+diff_a1_wrt1*cv.a2)-27*diff_a3_wrt1-2*3*diff_a1_wrt1*cv.a1**2)/54.
        diff_L_wrt2 = (9*(cv.a1*diff_a2_wrt2+diff_a1_wrt2*cv.a2)-27*diff_a3_wrt2-2*3*diff_a1_wrt2*cv.a1**2)/54.

        diff_D_wrt1 = 3*diff_Q_wrt1*cv.Q**2+2*diff_L_wrt1*cv.L
        diff_D_wrt2 = 3*diff_Q_wrt2*cv.Q**2+2*diff_L_wrt2*cv.L

        diff_S1_wrt1 = 0 if cv.L+np.sqrt(cv.D) == 0 else ((-1*(1./3.)*(-1*(diff_L_wrt1+0.5*diff_D_wrt1/np.sqrt(cv.D)))*(-1*(cv.L+np.sqrt(cv.D)))**(1./3.-1)) if (cv.L+np.sqrt(cv.D))<0 else ((1./3.)*(diff_L_wrt1+0.5*diff_D_wrt1/np.sqrt(cv.D))*(cv.L+np.sqrt(cv.D))**(1./3.-1)))
        diff_S1_wrt2 = 0 if cv.L+np.sqrt(cv.D) == 0 else ((-1*(1./3.)*(-1*(diff_L_wrt2+0.5*diff_D_wrt2/np.sqrt(cv.D)))*(-1*(cv.L+np.sqrt(cv.D)))**(1./3.-1)) if (cv.L+np.sqrt(cv.D))<0 else ((1./3.)*(diff_L_wrt2+0.5*diff_D_wrt2/np.sqrt(cv.D))*(cv.L+np.sqrt(cv.D))**(1./3.-1)))

        diff_S2_wrt1 = 0 if cv.L-np.sqrt(cv.D) == 0 else ((-1*(1./3.)*(-1*(diff_L_wrt1-0.5*diff_D_wrt1/np.sqrt(cv.D)))*(-1*(cv.L-np.sqrt(cv.D)))**(1./3.-1)) if (cv.L-np.sqrt(cv.D))<0 else ((1./3.)*(diff_L_wrt1-0.5*diff_D_wrt1/np.sqrt(cv.D))*(cv.L-np.sqrt(cv.D))**(1./3.-1)))
        diff_S2_wrt2 = 0 if cv.L-np.sqrt(cv.D) == 0 else ((-1*(1./3.)*(-1*(diff_L_wrt2-0.5*diff_D_wrt2/np.sqrt(cv.D)))*(-1*(cv.L-np.sqrt(cv.D)))**(1./3.-1)) if (cv.L-np.sqrt(cv.D))<0 else ((1./3.)*(diff_L_wrt2-0.5*diff_D_wrt2/np.sqrt(cv.D))*(cv.L-np.sqrt(cv.D))**(1./3.-1)))

        diff_Z1_d_wrt1 = diff_S1_wrt1+diff_S2_wrt1-diff_a1_wrt1/3
        diff_Z1_d_wrt2 = diff_S1_wrt2+diff_S2_wrt2-diff_a1_wrt2/3

        diff_Z1_dd_wrt1 = ((-1*(1./3.)*(-1*diff_L_wrt1)*(-1*cv.L)**(1./3.-1)) if cv.L<0 else (1./3.*diff_L_wrt1*cv.L**(1./3.-1))) *2-diff_a1_wrt1/3
        diff_Z1_dd_wrt2 = ((-1*(1./3.)*(-1*diff_L_wrt2)*(-1*cv.L)**(1./3.-1)) if cv.L<0 else (1./3.*diff_L_wrt2*cv.L**(1./3.-1))) *2-diff_a1_wrt2/3

        diff_Z2_dd_wrt1 = (((-1*(1./3.)*(-1*diff_L_wrt1)*(-1*cv.L)**(1./3.-1)) if cv.L<0 else (1./3.*diff_L_wrt1*cv.L**(1./3.-1))) +diff_a1_wrt1/3) * -1.
        diff_Z2_dd_wrt2 = (((-1*(1./3.)*(-1*diff_L_wrt2)*(-1*cv.L)**(1./3.-1)) if cv.L<0 else (1./3.*diff_L_wrt2*cv.L**(1./3.-1))) +diff_a1_wrt2/3) * -1.
        
        diff_Z3_dd_wrt1 = diff_Z2_dd_wrt1
        diff_Z3_dd_wrt2 = diff_Z2_dd_wrt2

        diff_Z1_wrt1 = 0 if cv.D>=0 or cv.Q>=0 else (2*(-0.5*diff_Q_wrt1/np.sqrt(-1*cv.Q)*np.cos(1./3.*(np.arccos( 1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3) ))+2*np.pi/3)+np.sqrt(-1*cv.Q)*(-1)*(1./3.*-1/np.sqrt(1-(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3))**2))*((np.sqrt(-cv.Q**3)*diff_L_wrt1-(0.5*-1*3*diff_Q_wrt1*cv.Q**2)/np.sqrt(-cv.Q**3)*cv.L)/(-cv.Q**3))*np.sin(1./3.*(np.arccos(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3)))+2*np.pi/3))-diff_a1_wrt1/3)
        diff_Z1_wrt2 = 0 if cv.D>=0 or cv.Q>=0 else (2*(-0.5*diff_Q_wrt2/np.sqrt(-1*cv.Q)*np.cos(1./3.*(np.arccos( 1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3) ))+2*np.pi/3)+np.sqrt(-1*cv.Q)*(-1)*(1./3.*-1/np.sqrt(1-(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3))**2))*((np.sqrt(-cv.Q**3)*diff_L_wrt2-(0.5*-1*3*diff_Q_wrt2*cv.Q**2)/np.sqrt(-cv.Q**3)*cv.L)/(-cv.Q**3))*np.sin(1./3.*(np.arccos(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3)))+2*np.pi/3))-diff_a1_wrt2/3)

        diff_Z2_wrt1 = 0 if cv.D>=0 or cv.Q>=0 else (2*(-0.5*diff_Q_wrt1/np.sqrt(-1*cv.Q)*np.cos(1./3.*(np.arccos( 1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3) ))+4*np.pi/3)+np.sqrt(-1*cv.Q)*(-1)*(1./3.*-1/np.sqrt(1-(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3))**2))*((np.sqrt(-cv.Q**3)*diff_L_wrt1-(0.5*-1*3*diff_Q_wrt1*cv.Q**2)/np.sqrt(-cv.Q**3)*cv.L)/(-cv.Q**3))*np.sin(1./3.*(np.arccos(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3)))+4*np.pi/3))-diff_a1_wrt1/3)
        diff_Z2_wrt2 = 0 if cv.D>=0 or cv.Q>=0 else (2*(-0.5*diff_Q_wrt2/np.sqrt(-1*cv.Q)*np.cos(1./3.*(np.arccos( 1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3) ))+4*np.pi/3)+np.sqrt(-1*cv.Q)*(-1)*(1./3.*-1/np.sqrt(1-(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3))**2))*((np.sqrt(-cv.Q**3)*diff_L_wrt2-(0.5*-1*3*diff_Q_wrt2*cv.Q**2)/np.sqrt(-cv.Q**3)*cv.L)/(-cv.Q**3))*np.sin(1./3.*(np.arccos(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3)))+4*np.pi/3))-diff_a1_wrt2/3)

        diff_Z3_wrt1 = 0 if cv.D>=0 or cv.Q>=0 else (2*(-0.5*diff_Q_wrt1/np.sqrt(-1*cv.Q)*np.cos(1./3.*(np.arccos( 1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3) )))+np.sqrt(-1*cv.Q)*(-1)*(1./3.*-1/np.sqrt(1-(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3))**2))*((np.sqrt(-cv.Q**3)*diff_L_wrt1-(0.5*-1*3*diff_Q_wrt1*cv.Q**2)/np.sqrt(-cv.Q**3)*cv.L)/(-cv.Q**3))*np.sin(1./3.*(np.arccos(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3)))))-diff_a1_wrt1/3)
        diff_Z3_wrt2 = 0 if cv.D>=0 or cv.Q>=0 else (2*(-0.5*diff_Q_wrt2/np.sqrt(-1*cv.Q)*np.cos(1./3.*(np.arccos( 1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3) )))+np.sqrt(-1*cv.Q)*(-1)*(1./3.*-1/np.sqrt(1-(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3))**2))*((np.sqrt(-cv.Q**3)*diff_L_wrt2-(0.5*-1*3*diff_Q_wrt2*cv.Q**2)/np.sqrt(-cv.Q**3)*cv.L)/(-cv.Q**3))*np.sin(1./3.*(np.arccos(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3)))))-diff_a1_wrt2/3)

        diff_ZL_wrt1 = (diff_Z1_wrt1 if cv.ZL==cv.Z1 else (diff_Z2_wrt1 if cv.ZL==cv.Z2 else  diff_Z3_wrt1)) if cv.D<0 else ((diff_Z1_dd_wrt1 if cv.ZL==cv.Z1_dd else (diff_Z2_dd_wrt1 if cv.ZL==cv.Z2_dd else diff_Z3_dd_wrt1)) if cv.D==0 else diff_Z1_d_wrt1)
        diff_ZL_wrt2 = (diff_Z1_wrt2 if cv.ZL==cv.Z1 else (diff_Z2_wrt2 if cv.ZL==cv.Z2 else  diff_Z3_wrt2)) if cv.D<0 else ((diff_Z1_dd_wrt2 if cv.ZL==cv.Z1_dd else (diff_Z2_dd_wrt2 if cv.ZL==cv.Z2_dd else diff_Z3_dd_wrt2)) if cv.D==0 else diff_Z1_d_wrt2)

        diff_ZV_wrt1 = (diff_Z1_wrt1 if cv.ZV==cv.Z1 else (diff_Z2_wrt1 if cv.ZV==cv.Z2 else  diff_Z3_wrt1)) if cv.D<0 else ((diff_Z1_dd_wrt1 if cv.ZV==cv.Z1_dd else (diff_Z2_dd_wrt1 if cv.ZV==cv.Z2_dd else diff_Z3_dd_wrt1)) if cv.D==0 else 0)
        diff_ZV_wrt2 = (diff_Z1_wrt2 if cv.ZV==cv.Z1 else (diff_Z2_wrt2 if cv.ZV==cv.Z2 else  diff_Z3_wrt2)) if cv.D<0 else ((diff_Z1_dd_wrt2 if cv.ZV==cv.Z1_dd else (diff_Z2_dd_wrt2 if cv.ZV==cv.Z2_dd else diff_Z3_dd_wrt2)) if cv.D==0 else 0)

        diff_phl_wrt1 = 0 if (cv.ZL-cv.B)<=0 or cv.B*(cv.r2 - cv.r1) == 0 or (cv.ZL + cv.r1 * cv.B) == 0 or (cv.ZL + cv.r2 * cv.B) == 0 or (cv.ZL + cv.r2 * cv.B) * (cv.ZL + cv.r1 * cv.B) < 0  else ((diff_ZL_wrt1)-(diff_ZL_wrt1-diff_B_wrt1)/(cv.ZL-cv.B)-((((cv.B*(cv.r2-cv.r1))*diff_A_wrt1-(diff_B_wrt1*(cv.r2-cv.r1)+cv.B*(diff_r2_wrt1-diff_r1_wrt1))*cv.A)/(cv.B*(cv.r2-cv.r1))**2)*np.log((cv.ZL+cv.r2*cv.B)/(cv.ZL+cv.r1*cv.B))+(cv.A/(cv.B*(cv.r2-cv.r1)))*1/((cv.ZL+cv.r2*cv.B)/(cv.ZL+cv.r1*cv.B))*(((cv.ZL+cv.r1*cv.B)*(diff_ZL_wrt1+diff_r2_wrt1*cv.B+cv.r2*diff_B_wrt1)-(cv.ZL+cv.r2*cv.B)*(diff_ZL_wrt1+diff_r1_wrt1*cv.B+cv.r1*diff_B_wrt1))/(cv.ZL+cv.r1*cv.B)**2)))
        diff_phl_wrt2 = 0 if (cv.ZL-cv.B)<=0 or cv.B*(cv.r2 - cv.r1) == 0 or (cv.ZL + cv.r1 * cv.B) == 0 or (cv.ZL + cv.r2 * cv.B) == 0 or (cv.ZL + cv.r2 * cv.B) * (cv.ZL + cv.r1 * cv.B) < 0 else ((diff_ZL_wrt2)-(diff_ZL_wrt2-diff_B_wrt2)/(cv.ZL-cv.B)-((((cv.B*(cv.r2-cv.r1))*diff_A_wrt2-(diff_B_wrt2*(cv.r2-cv.r1)+cv.B*(diff_r2_wrt2-diff_r1_wrt2))*cv.A)/(cv.B*(cv.r2-cv.r1))**2)*np.log((cv.ZL+cv.r2*cv.B)/(cv.ZL+cv.r1*cv.B))+(cv.A/(cv.B*(cv.r2-cv.r1)))*1/((cv.ZL+cv.r2*cv.B)/(cv.ZL+cv.r1*cv.B))*(((cv.ZL+cv.r1*cv.B)*(diff_ZL_wrt2+diff_r2_wrt2*cv.B+cv.r2*diff_B_wrt2)-(cv.ZL+cv.r2*cv.B)*(diff_ZL_wrt2+diff_r1_wrt2*cv.B+cv.r1*diff_B_wrt2))/(cv.ZL+cv.r1*cv.B)**2)))

        diff_phv_wrt1 = 0 if (cv.ZV-cv.B)<=0 or cv.B*(cv.r2 - cv.r1) == 0 or (cv.ZV + cv.r1 * cv.B) == 0 or (cv.ZV + cv.r2 * cv.B) == 0 or (cv.ZV + cv.r2 * cv.B) * (cv.ZV + cv.r1 * cv.B) < 0 else ((diff_ZV_wrt1)-(diff_ZV_wrt1-diff_B_wrt1)/(cv.ZV-cv.B)-((((cv.B*(cv.r2-cv.r1))*diff_A_wrt1-(diff_B_wrt1*(cv.r2-cv.r1)+cv.B*(diff_r2_wrt1-diff_r1_wrt1))*cv.A)/(cv.B*(cv.r2-cv.r1))**2)*np.log((cv.ZV+cv.r2*cv.B)/(cv.ZV+cv.r1*cv.B))+(cv.A/(cv.B*(cv.r2-cv.r1)))*1/((cv.ZV+cv.r2*cv.B)/(cv.ZV+cv.r1*cv.B))*(((cv.ZV+cv.r1*cv.B)*(diff_ZV_wrt1+diff_r2_wrt1*cv.B+cv.r2*diff_B_wrt1)-(cv.ZV+cv.r2*cv.B)*(diff_ZV_wrt1+diff_r1_wrt1*cv.B+cv.r1*diff_B_wrt1))/(cv.ZV+cv.r1*cv.B)**2)))
        diff_phv_wrt2 = 0 if (cv.ZV-cv.B)<=0 or cv.B*(cv.r2 - cv.r1) == 0 or (cv.ZV + cv.r1 * cv.B) == 0 or (cv.ZV + cv.r2 * cv.B) == 0 or (cv.ZV + cv.r2 * cv.B) * (cv.ZV + cv.r1 * cv.B) < 0 else ((diff_ZV_wrt2)-(diff_ZV_wrt2-diff_B_wrt2)/(cv.ZV-cv.B)-((((cv.B*(cv.r2-cv.r1))*diff_A_wrt2-(diff_B_wrt2*(cv.r2-cv.r1)+cv.B*(diff_r2_wrt2-diff_r1_wrt2))*cv.A)/(cv.B*(cv.r2-cv.r1))**2)*np.log((cv.ZV+cv.r2*cv.B)/(cv.ZV+cv.r1*cv.B))+(cv.A/(cv.B*(cv.r2-cv.r1)))*1/((cv.ZV+cv.r2*cv.B)/(cv.ZV+cv.r1*cv.B))*(((cv.ZV+cv.r1*cv.B)*(diff_ZV_wrt2+diff_r2_wrt2*cv.B+cv.r2*diff_B_wrt2)-(cv.ZV+cv.r2*cv.B)*(diff_ZV_wrt2+diff_r1_wrt2*cv.B+cv.r1*diff_B_wrt2))/(cv.ZV+cv.r1*cv.B)**2)))

        diff_ph_diff_wrt1 = np.sign(cv.phl-cv.phv)*(diff_phl_wrt1-diff_phv_wrt1)
        diff_ph_diff_wrt2 = np.sign(cv.phl-cv.phv)*(diff_phl_wrt2-diff_phv_wrt2)

        diff_t1_wrt1 = gasConstant*ov.bp_final1*(diff_ZV_wrt1-diff_ZL_wrt1)
        diff_t1_wrt2 = gasConstant*ov.bp_final1*(diff_ZV_wrt2-diff_ZL_wrt2)

        diff_t2_wrt1 = 0 if cv.b == 0 or (cv.u**2-4*cv.w) <= 0 else ((cv.b*np.sqrt(cv.u**2-4*cv.w))*(diff_ac_wrt1*(ov.bp_final1*cv.diff_alpha_wrt_temp-cv.alpha_eos)+cv.ac*(ov.bp_final1*diff_diff_alpha_wrt1-diff_alpha_wrt1))-(diff_b_wrt1*np.sqrt(cv.u**2-4*cv.w)+cv.b*0.5*(2*cv.u*diff_u_wrt1-4*diff_w_wrt1)/np.sqrt(cv.u**2-4*cv.w))*(cv.ac*(ov.bp_final1*cv.diff_alpha_wrt_temp-cv.alpha_eos)))/(cv.b*np.sqrt(cv.u**2-4*cv.w))**2
        diff_t2_wrt2 = 0 if cv.b == 0 or (cv.u**2-4*cv.w) <= 0 else ((cv.b*np.sqrt(cv.u**2-4*cv.w))*(diff_ac_wrt2*(ov.bp_final1*cv.diff_alpha_wrt_temp-cv.alpha_eos)+cv.ac*(ov.bp_final1*diff_diff_alpha_wrt2-diff_alpha_wrt2))-(diff_b_wrt2*np.sqrt(cv.u**2-4*cv.w)+cv.b*0.5*(2*cv.u*diff_u_wrt2-4*diff_w_wrt2)/np.sqrt(cv.u**2-4*cv.w))*(cv.ac*(ov.bp_final1*cv.diff_alpha_wrt_temp-cv.alpha_eos)))/(cv.b*np.sqrt(cv.u**2-4*cv.w))**2

        diff_t3_wrt1 = 0 if (cv.ZV+cv.B*cv.r2) == 0 or (cv.ZV+cv.B*cv.r1) == 0 or (cv.ZV+cv.B*cv.r2)*(cv.ZV+cv.B*cv.r1) < 0 else (((cv.ZV+cv.B*cv.r1)/(cv.ZV+cv.B*cv.r2))*((cv.ZV+cv.B*cv.r1)*(diff_ZV_wrt1+diff_B_wrt1*cv.r2+cv.B*diff_r2_wrt1)-(diff_ZV_wrt1+diff_B_wrt1*cv.r1+cv.B*diff_r1_wrt1)*(cv.ZV+cv.B*cv.r2))/(cv.ZV+cv.B*cv.r1)**2)
        diff_t3_wrt2 = 0 if (cv.ZV+cv.B*cv.r2) == 0 or (cv.ZV+cv.B*cv.r1) == 0 or (cv.ZV+cv.B*cv.r2)*(cv.ZV+cv.B*cv.r1) < 0 else (((cv.ZV+cv.B*cv.r1)/(cv.ZV+cv.B*cv.r2))*((cv.ZV+cv.B*cv.r1)*(diff_ZV_wrt2+diff_B_wrt2*cv.r2+cv.B*diff_r2_wrt2)-(diff_ZV_wrt2+diff_B_wrt2*cv.r1+cv.B*diff_r1_wrt2)*(cv.ZV+cv.B*cv.r2))/(cv.ZV+cv.B*cv.r1)**2)

        diff_t4_wrt1 = 0 if (cv.ZL+cv.B*cv.r2) == 0 or (cv.ZL+cv.B*cv.r1) == 0 or (cv.ZL+cv.B*cv.r2)*(cv.ZL+cv.B*cv.r1) < 0 else (((cv.ZL+cv.B*cv.r1)/(cv.ZL+cv.B*cv.r2))*((cv.ZL+cv.B*cv.r1)*(diff_ZL_wrt1+diff_B_wrt1*cv.r2+cv.B*diff_r2_wrt1)-(diff_ZL_wrt1+diff_B_wrt1*cv.r1+cv.B*diff_r1_wrt1)*(cv.ZL+cv.B*cv.r2))/(cv.ZL+cv.B*cv.r1)**2)
        diff_t4_wrt2 = 0 if (cv.ZL+cv.B*cv.r2) == 0 or (cv.ZL+cv.B*cv.r1) == 0 or (cv.ZL+cv.B*cv.r2)*(cv.ZL+cv.B*cv.r1) < 0 else (((cv.ZL+cv.B*cv.r1)/(cv.ZL+cv.B*cv.r2))*((cv.ZL+cv.B*cv.r1)*(diff_ZL_wrt2+diff_B_wrt2*cv.r2+cv.B*diff_r2_wrt2)-(diff_ZL_wrt2+diff_B_wrt2*cv.r1+cv.B*diff_r1_wrt2)*(cv.ZL+cv.B*cv.r2))/(cv.ZL+cv.B*cv.r1)**2)

        diff_hv_bp_wrt1 = (diff_t1_wrt1+diff_t2_wrt1*(cv.t3-cv.t4)+cv.t2*(diff_t3_wrt1-diff_t4_wrt1))/1000.
        diff_hv_bp_wrt2 = (diff_t1_wrt2+diff_t2_wrt2*(cv.t3-cv.t4)+cv.t2*(diff_t3_wrt2-diff_t4_wrt2))/1000.

        diff_vb_wrt1 = diff_ZL_wrt1*gasConstant*ov.bp_final1/pressure
        diff_vb_wrt2 = diff_ZL_wrt2*gasConstant*ov.bp_final1/pressure

        diff_wrt1 = self.scales[0]*(2*(ov.hv_bp-cv.hv_bp)/ov.hv_bp**2)*(-1.)*diff_hv_bp_wrt1+self.scales[1]*(2*(ov.vb-cv.vb)/ov.vb**2)*(-1)*diff_vb_wrt1+self.scales[2]*2*(max(0,(cv.ph_diff-10**-7)))*(0 if(cv.ph_diff-10**-7)<=0 else diff_ph_diff_wrt1)
        diff_wrt2 = self.scales[0]*(2*(ov.hv_bp-cv.hv_bp)/ov.hv_bp**2)*(-1.)*diff_hv_bp_wrt2+self.scales[1]*(2*(ov.vb-cv.vb)/ov.vb**2)*(-1)*diff_vb_wrt2+self.scales[2]*2*(max(0,(cv.ph_diff-10**-7)))*(0 if(cv.ph_diff-10**-7)<=0 else diff_ph_diff_wrt2)

        diff_wrt1 = (np.sign(diff_wrt1)*10**7) if np.abs(diff_wrt1)>10**7 else diff_wrt1
        diff_wrt2 = (np.sign(diff_wrt2)*10**7) if np.abs(diff_wrt2)>10**7 else diff_wrt2    
        return np.array([diff_wrt1, diff_wrt2])

    def poas_4(self, ov, cv):
        diff_alpha_wrt1 = ov.alpha_pr
        diff_alpha_wrt2 = 0
        
        diff_diff_alpha_wrt1 = ov.alpha_pr*(((2*ov.alpha_m-2)/(ov.bp_final1/ov.tc_k)+ov.alpha_l*(-2*ov.alpha_m)*(ov.bp_final1/ov.tc_k)**(2*ov.alpha_m-1))/ov.tc_k)
        diff_diff_alpha_wrt2 = 0

        diff_om_b_wrt1 = 0
        diff_om_b_wrt2 = (0.9129*ov.zc_final**2-0.0551*ov.zc_final+0.0368) if ((0.9129*ov.zc_final**2-0.0551*ov.zc_final+0.0368)*ov.om_b_multiplier)<ov.om_b else 0

        diff_del_b_wrt1 = 0
        diff_del_b_wrt2 = 0 if ((ov.zc_final-cv.om_b)**3+0.25*(1-3*ov.zc_final+cv.om_b)**2+3*ov.zc_final*cv.om_b-cv.om_b*(1+2*cv.om_b))<0 else (-3*diff_om_b_wrt2*(ov.zc_final-cv.om_b)**2+0.25*2*diff_om_b_wrt2*(1-3*ov.zc_final+cv.om_b)+3*ov.zc_final*diff_om_b_wrt2-diff_om_b_wrt2-2*2*cv.om_b*diff_om_b_wrt2)

        diff_om_c_wrt1 = 0
        diff_om_c_wrt2 = diff_om_b_wrt2/2. if cv.del_b == 0 else (diff_om_b_wrt2+0.5*4*diff_del_b_wrt2*(4*cv.del_b)**-0.5)/2

        diff_om_d_wrt1 = 0
        diff_om_d_wrt2 = diff_om_b_wrt2/2. if cv.del_b == 0 else (diff_om_b_wrt2-0.5*4*diff_del_b_wrt2*(4*cv.del_b)**-0.5)/2

        diff_om_a_wrt1 = 0
        diff_om_a_wrt2 = (cv.om_b*diff_om_c_wrt2+diff_om_b_wrt2*cv.om_c)+(cv.om_b*diff_om_d_wrt2+diff_om_b_wrt2*cv.om_d)+(cv.om_c*diff_om_d_wrt2+diff_om_c_wrt2*cv.om_d)+diff_om_c_wrt2+diff_om_d_wrt2

        diff_b_wrt1 = 0
        diff_b_wrt2 = diff_om_b_wrt2*gasConstant*ov.tc_k/(ov.pc_mpa*10**6)

        diff_c_wrt1 = 0
        diff_c_wrt2 = diff_om_c_wrt2*gasConstant*ov.tc_k/(ov.pc_mpa*10**6)

        diff_d_wrt1 = 0
        diff_d_wrt2 = diff_om_d_wrt2*gasConstant*ov.tc_k/(ov.pc_mpa*10**6)

        diff_ac_wrt1 = 0
        diff_ac_wrt2 = diff_om_a_wrt2*gasConstant**2*ov.tc_k**2/(ov.pc_mpa*10**6)

        diff_ac_alpha_wrt1 = (cv.ac*diff_alpha_wrt1+diff_ac_wrt1*cv.alpha_eos)
        diff_ac_alpha_wrt2 = (cv.ac*diff_alpha_wrt2+diff_ac_wrt2*cv.alpha_eos)

        diff_u_wrt1 = 0
        diff_u_wrt2 = 0 if cv.b==0 else (cv.b*(diff_c_wrt2+diff_d_wrt2)-(cv.c+cv.d)*diff_b_wrt2)/cv.b**2

        diff_w_wrt1 = 0
        diff_w_wrt2 = 0 if cv.b==0 else -1.*(cv.b**2*(cv.c*diff_d_wrt2+diff_c_wrt2*cv.d)-2*cv.b*diff_b_wrt2*(cv.c*cv.d))/cv.b**4

        diff_r2_wrt1 = 0
        diff_r2_wrt2 = 0 if (cv.u**2-4*cv.w) <= 0 else (diff_u_wrt2+0.5*(2*cv.u*diff_u_wrt2-4*diff_w_wrt2)*(cv.u**2-4*cv.w)**-0.5)/2

        diff_r1_wrt1 = 0
        diff_r1_wrt2 = 0 if (cv.u**2-4*cv.w) <= 0 else (diff_u_wrt2-0.5*(2*cv.u*diff_u_wrt2-4*diff_w_wrt2)*(cv.u**2-4*cv.w)**-0.5)/2              

        diff_A_wrt1 = diff_ac_alpha_wrt1*pressure*10**6/(gasConstant**2*ov.bp_final1**2)
        diff_A_wrt2 = diff_ac_alpha_wrt2*pressure*10**6/(gasConstant**2*ov.bp_final1**2)

        diff_B_wrt1 = 0
        diff_B_wrt2 = diff_b_wrt2*pressure*10**6/(gasConstant*ov.bp_final1)   

        diff_a1_wrt1 = 0
        diff_a1_wrt2 = ((cv.u*diff_B_wrt2+diff_u_wrt2*cv.B)-diff_B_wrt2) 

        diff_a2_wrt1 = diff_A_wrt1+(cv.w*2*cv.B*diff_B_wrt1+diff_w_wrt1*cv.B**2)-(cv.u*diff_B_wrt1+diff_u_wrt1*cv.B)-(cv.u*2*cv.B*diff_B_wrt1+diff_u_wrt1*cv.B**2)
        diff_a2_wrt2 = diff_A_wrt2+(cv.w*2*cv.B*diff_B_wrt2+diff_w_wrt2*cv.B**2)-(cv.u*diff_B_wrt2+diff_u_wrt2*cv.B)-(cv.u*2*cv.B*diff_B_wrt2+diff_u_wrt2*cv.B**2)

        diff_a3_wrt1 = -1*((cv.A*diff_B_wrt1+diff_A_wrt1*cv.B)+(cv.w*2*cv.B*diff_B_wrt1+diff_w_wrt1*cv.B**2)+(cv.w*3*cv.B**2*diff_B_wrt1+diff_w_wrt1*cv.B**3))
        diff_a3_wrt2 = -1*((cv.A*diff_B_wrt2+diff_A_wrt2*cv.B)+(cv.w*2*cv.B*diff_B_wrt2+diff_w_wrt2*cv.B**2)+(cv.w*3*cv.B**2*diff_B_wrt2+diff_w_wrt2*cv.B**3))

        diff_Q_wrt1 = (3*diff_a2_wrt1-2*cv.a1*diff_a1_wrt1)/9.
        diff_Q_wrt2 = (3*diff_a2_wrt2-2*cv.a1*diff_a1_wrt2)/9.

        diff_L_wrt1 = (9*(cv.a1*diff_a2_wrt1+diff_a1_wrt1*cv.a2)-27*diff_a3_wrt1-2*3*diff_a1_wrt1*cv.a1**2)/54.
        diff_L_wrt2 = (9*(cv.a1*diff_a2_wrt2+diff_a1_wrt2*cv.a2)-27*diff_a3_wrt2-2*3*diff_a1_wrt2*cv.a1**2)/54.

        diff_D_wrt1 = 3*diff_Q_wrt1*cv.Q**2+2*diff_L_wrt1*cv.L
        diff_D_wrt2 = 3*diff_Q_wrt2*cv.Q**2+2*diff_L_wrt2*cv.L

        diff_S1_wrt1 = (-1*(1./3.)*(-1*(diff_L_wrt1+0.5*diff_D_wrt1/np.sqrt(cv.D)))*(-1*(cv.L+np.sqrt(cv.D)))**(1./3.-1)) if (cv.L+np.sqrt(cv.D))<0 else ((1./3.)*(diff_L_wrt1+0.5*diff_D_wrt1/np.sqrt(cv.D))*(cv.L+np.sqrt(cv.D))**(1./3.-1))
        diff_S1_wrt2 = (-1*(1./3.)*(-1*(diff_L_wrt2+0.5*diff_D_wrt2/np.sqrt(cv.D)))*(-1*(cv.L+np.sqrt(cv.D)))**(1./3.-1)) if (cv.L+np.sqrt(cv.D))<0 else ((1./3.)*(diff_L_wrt2+0.5*diff_D_wrt2/np.sqrt(cv.D))*(cv.L+np.sqrt(cv.D))**(1./3.-1))

        diff_S2_wrt1 = (-1*(1./3.)*(-1*(diff_L_wrt1-0.5*diff_D_wrt1/np.sqrt(cv.D)))*(-1*(cv.L-np.sqrt(cv.D)))**(1./3.-1)) if (cv.L-np.sqrt(cv.D))<0 else ((1./3.)*(diff_L_wrt1-0.5*diff_D_wrt1/np.sqrt(cv.D))*(cv.L-np.sqrt(cv.D))**(1./3.-1))
        diff_S2_wrt2 = (-1*(1./3.)*(-1*(diff_L_wrt2-0.5*diff_D_wrt2/np.sqrt(cv.D)))*(-1*(cv.L-np.sqrt(cv.D)))**(1./3.-1)) if (cv.L-np.sqrt(cv.D))<0 else ((1./3.)*(diff_L_wrt2-0.5*diff_D_wrt2/np.sqrt(cv.D))*(cv.L-np.sqrt(cv.D))**(1./3.-1))

        diff_Z1_d_wrt1 = diff_S1_wrt1+diff_S2_wrt1-diff_a1_wrt1/3
        diff_Z1_d_wrt2 = diff_S1_wrt2+diff_S2_wrt2-diff_a1_wrt2/3

        diff_Z1_dd_wrt1 = ((-1*(1./3.)*(-1*diff_L_wrt1)*(-1*cv.L)**(1./3.-1)) if cv.L<0 else (1./3.*diff_L_wrt1*cv.L**(1./3.-1))) *2-diff_a1_wrt1/3
        diff_Z1_dd_wrt2 = ((-1*(1./3.)*(-1*diff_L_wrt2)*(-1*cv.L)**(1./3.-1)) if cv.L<0 else (1./3.*diff_L_wrt2*cv.L**(1./3.-1))) *2-diff_a1_wrt2/3

        diff_Z2_dd_wrt1 = (((-1*(1./3.)*(-1*diff_L_wrt1)*(-1*cv.L)**(1./3.-1)) if cv.L<0 else (1./3.*diff_L_wrt1*cv.L**(1./3.-1))) +diff_a1_wrt1/3) * -1.
        diff_Z2_dd_wrt2 = (((-1*(1./3.)*(-1*diff_L_wrt2)*(-1*cv.L)**(1./3.-1)) if cv.L<0 else (1./3.*diff_L_wrt2*cv.L**(1./3.-1))) +diff_a1_wrt2/3) * -1.
        
        diff_Z3_dd_wrt1 = diff_Z2_dd_wrt1
        diff_Z3_dd_wrt2 = diff_Z2_dd_wrt2

        diff_Z1_wrt1 = 0 if cv.D>=0 or cv.Q>=0 else (2*(-0.5*diff_Q_wrt1/np.sqrt(-1*cv.Q)*np.cos(1./3.*(np.arccos( 1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3) ))+2*np.pi/3)+np.sqrt(-1*cv.Q)*(-1)*(1./3.*-1/np.sqrt(1-(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3))**2))*((np.sqrt(-cv.Q**3)*diff_L_wrt1-(0.5*-1*3*diff_Q_wrt1*cv.Q**2)/np.sqrt(-cv.Q**3)*cv.L)/(-cv.Q**3))*np.sin(1./3.*(np.arccos(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3)))+2*np.pi/3))-diff_a1_wrt1/3)
        diff_Z1_wrt2 = 0 if cv.D>=0 or cv.Q>=0 else (2*(-0.5*diff_Q_wrt2/np.sqrt(-1*cv.Q)*np.cos(1./3.*(np.arccos( 1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3) ))+2*np.pi/3)+np.sqrt(-1*cv.Q)*(-1)*(1./3.*-1/np.sqrt(1-(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3))**2))*((np.sqrt(-cv.Q**3)*diff_L_wrt2-(0.5*-1*3*diff_Q_wrt2*cv.Q**2)/np.sqrt(-cv.Q**3)*cv.L)/(-cv.Q**3))*np.sin(1./3.*(np.arccos(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3)))+2*np.pi/3))-diff_a1_wrt2/3)

        diff_Z2_wrt1 = 0 if cv.D>=0 or cv.Q>=0 else (2*(-0.5*diff_Q_wrt1/np.sqrt(-1*cv.Q)*np.cos(1./3.*(np.arccos( 1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3) ))+4*np.pi/3)+np.sqrt(-1*cv.Q)*(-1)*(1./3.*-1/np.sqrt(1-(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3))**2))*((np.sqrt(-cv.Q**3)*diff_L_wrt1-(0.5*-1*3*diff_Q_wrt1*cv.Q**2)/np.sqrt(-cv.Q**3)*cv.L)/(-cv.Q**3))*np.sin(1./3.*(np.arccos(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3)))+4*np.pi/3))-diff_a1_wrt1/3)
        diff_Z2_wrt2 = 0 if cv.D>=0 or cv.Q>=0 else (2*(-0.5*diff_Q_wrt2/np.sqrt(-1*cv.Q)*np.cos(1./3.*(np.arccos( 1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3) ))+4*np.pi/3)+np.sqrt(-1*cv.Q)*(-1)*(1./3.*-1/np.sqrt(1-(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3))**2))*((np.sqrt(-cv.Q**3)*diff_L_wrt2-(0.5*-1*3*diff_Q_wrt2*cv.Q**2)/np.sqrt(-cv.Q**3)*cv.L)/(-cv.Q**3))*np.sin(1./3.*(np.arccos(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3)))+4*np.pi/3))-diff_a1_wrt2/3)

        diff_Z3_wrt1 = 0 if cv.D>=0 or cv.Q>=0 else (2*(-0.5*diff_Q_wrt1/np.sqrt(-1*cv.Q)*np.cos(1./3.*(np.arccos( 1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3) )))+np.sqrt(-1*cv.Q)*(-1)*(1./3.*-1/np.sqrt(1-(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3))**2))*((np.sqrt(-cv.Q**3)*diff_L_wrt1-(0.5*-1*3*diff_Q_wrt1*cv.Q**2)/np.sqrt(-cv.Q**3)*cv.L)/(-cv.Q**3))*np.sin(1./3.*(np.arccos(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3)))))-diff_a1_wrt1/3)
        diff_Z3_wrt2 = 0 if cv.D>=0 or cv.Q>=0 else (2*(-0.5*diff_Q_wrt2/np.sqrt(-1*cv.Q)*np.cos(1./3.*(np.arccos( 1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3) )))+np.sqrt(-1*cv.Q)*(-1)*(1./3.*-1/np.sqrt(1-(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3))**2))*((np.sqrt(-cv.Q**3)*diff_L_wrt2-(0.5*-1*3*diff_Q_wrt2*cv.Q**2)/np.sqrt(-cv.Q**3)*cv.L)/(-cv.Q**3))*np.sin(1./3.*(np.arccos(1. if np.abs(cv.L/np.sqrt(-cv.Q**3)) > 1 else cv.L/np.sqrt(-cv.Q**3)))))-diff_a1_wrt2/3)

        diff_ZL_wrt1 = (diff_Z1_wrt1 if cv.ZL==cv.Z1 else (diff_Z2_wrt1 if cv.ZL==cv.Z2 else  diff_Z3_wrt1)) if cv.D<0 else ((diff_Z1_dd_wrt1 if cv.ZL==cv.Z1_dd else (diff_Z2_dd_wrt1 if cv.ZL==cv.Z2_dd else diff_Z3_dd_wrt1)) if cv.D==0 else diff_Z1_d_wrt1)
        diff_ZL_wrt2 = (diff_Z1_wrt2 if cv.ZL==cv.Z1 else (diff_Z2_wrt2 if cv.ZL==cv.Z2 else  diff_Z3_wrt2)) if cv.D<0 else ((diff_Z1_dd_wrt2 if cv.ZL==cv.Z1_dd else (diff_Z2_dd_wrt2 if cv.ZL==cv.Z2_dd else diff_Z3_dd_wrt2)) if cv.D==0 else diff_Z1_d_wrt2)

        diff_ZV_wrt1 = (diff_Z1_wrt1 if cv.ZV==cv.Z1 else (diff_Z2_wrt1 if cv.ZV==cv.Z2 else  diff_Z3_wrt1)) if cv.D<0 else ((diff_Z1_dd_wrt1 if cv.ZV==cv.Z1_dd else (diff_Z2_dd_wrt1 if cv.ZV==cv.Z2_dd else diff_Z3_dd_wrt1)) if cv.D==0 else 0)
        diff_ZV_wrt2 = (diff_Z1_wrt2 if cv.ZV==cv.Z1 else (diff_Z2_wrt2 if cv.ZV==cv.Z2 else  diff_Z3_wrt2)) if cv.D<0 else ((diff_Z1_dd_wrt2 if cv.ZV==cv.Z1_dd else (diff_Z2_dd_wrt2 if cv.ZV==cv.Z2_dd else diff_Z3_dd_wrt2)) if cv.D==0 else 0)

        diff_phl_wrt1 = 0 if (cv.ZL-cv.B)<=0 or cv.B*(cv.r2 - cv.r1) == 0 or (cv.ZL + cv.r1 * cv.B) == 0 or (cv.ZL + cv.r2 * cv.B) == 0 or (cv.ZL + cv.r2 * cv.B) * (cv.ZL + cv.r1 * cv.B) < 0  else ((diff_ZL_wrt1)-(diff_ZL_wrt1-diff_B_wrt1)/(cv.ZL-cv.B)-((((cv.B*(cv.r2-cv.r1))*diff_A_wrt1-(diff_B_wrt1*(cv.r2-cv.r1)+cv.B*(diff_r2_wrt1-diff_r1_wrt1))*cv.A)/(cv.B*(cv.r2-cv.r1))**2)*np.log((cv.ZL+cv.r2*cv.B)/(cv.ZL+cv.r1*cv.B))+(cv.A/(cv.B*(cv.r2-cv.r1)))*1/((cv.ZL+cv.r2*cv.B)/(cv.ZL+cv.r1*cv.B))*(((cv.ZL+cv.r1*cv.B)*(diff_ZL_wrt1+diff_r2_wrt1*cv.B+cv.r2*diff_B_wrt1)-(cv.ZL+cv.r2*cv.B)*(diff_ZL_wrt1+diff_r1_wrt1*cv.B+cv.r1*diff_B_wrt1))/(cv.ZL+cv.r1*cv.B)**2)))
        diff_phl_wrt2 = 0 if (cv.ZL-cv.B)<=0 or cv.B*(cv.r2 - cv.r1) == 0 or (cv.ZL + cv.r1 * cv.B) == 0 or (cv.ZL + cv.r2 * cv.B) == 0 or (cv.ZL + cv.r2 * cv.B) * (cv.ZL + cv.r1 * cv.B) < 0 else ((diff_ZL_wrt2)-(diff_ZL_wrt2-diff_B_wrt2)/(cv.ZL-cv.B)-((((cv.B*(cv.r2-cv.r1))*diff_A_wrt2-(diff_B_wrt2*(cv.r2-cv.r1)+cv.B*(diff_r2_wrt2-diff_r1_wrt2))*cv.A)/(cv.B*(cv.r2-cv.r1))**2)*np.log((cv.ZL+cv.r2*cv.B)/(cv.ZL+cv.r1*cv.B))+(cv.A/(cv.B*(cv.r2-cv.r1)))*1/((cv.ZL+cv.r2*cv.B)/(cv.ZL+cv.r1*cv.B))*(((cv.ZL+cv.r1*cv.B)*(diff_ZL_wrt2+diff_r2_wrt2*cv.B+cv.r2*diff_B_wrt2)-(cv.ZL+cv.r2*cv.B)*(diff_ZL_wrt2+diff_r1_wrt2*cv.B+cv.r1*diff_B_wrt2))/(cv.ZL+cv.r1*cv.B)**2)))

        diff_phv_wrt1 = 0 if (cv.ZV-cv.B)<=0 or cv.B*(cv.r2 - cv.r1) == 0 or (cv.ZV + cv.r1 * cv.B) == 0 or (cv.ZV + cv.r2 * cv.B) == 0 or (cv.ZV + cv.r2 * cv.B) * (cv.ZV + cv.r1 * cv.B) < 0 else ((diff_ZV_wrt1)-(diff_ZV_wrt1-diff_B_wrt1)/(cv.ZV-cv.B)-((((cv.B*(cv.r2-cv.r1))*diff_A_wrt1-(diff_B_wrt1*(cv.r2-cv.r1)+cv.B*(diff_r2_wrt1-diff_r1_wrt1))*cv.A)/(cv.B*(cv.r2-cv.r1))**2)*np.log((cv.ZV+cv.r2*cv.B)/(cv.ZV+cv.r1*cv.B))+(cv.A/(cv.B*(cv.r2-cv.r1)))*1/((cv.ZV+cv.r2*cv.B)/(cv.ZV+cv.r1*cv.B))*(((cv.ZV+cv.r1*cv.B)*(diff_ZV_wrt1+diff_r2_wrt1*cv.B+cv.r2*diff_B_wrt1)-(cv.ZV+cv.r2*cv.B)*(diff_ZV_wrt1+diff_r1_wrt1*cv.B+cv.r1*diff_B_wrt1))/(cv.ZV+cv.r1*cv.B)**2)))
        diff_phv_wrt2 = 0 if (cv.ZV-cv.B)<=0 or cv.B*(cv.r2 - cv.r1) == 0 or (cv.ZV + cv.r1 * cv.B) == 0 or (cv.ZV + cv.r2 * cv.B) == 0 or (cv.ZV + cv.r2 * cv.B) * (cv.ZV + cv.r1 * cv.B) < 0 else ((diff_ZV_wrt2)-(diff_ZV_wrt2-diff_B_wrt2)/(cv.ZV-cv.B)-((((cv.B*(cv.r2-cv.r1))*diff_A_wrt2-(diff_B_wrt2*(cv.r2-cv.r1)+cv.B*(diff_r2_wrt2-diff_r1_wrt2))*cv.A)/(cv.B*(cv.r2-cv.r1))**2)*np.log((cv.ZV+cv.r2*cv.B)/(cv.ZV+cv.r1*cv.B))+(cv.A/(cv.B*(cv.r2-cv.r1)))*1/((cv.ZV+cv.r2*cv.B)/(cv.ZV+cv.r1*cv.B))*(((cv.ZV+cv.r1*cv.B)*(diff_ZV_wrt2+diff_r2_wrt2*cv.B+cv.r2*diff_B_wrt2)-(cv.ZV+cv.r2*cv.B)*(diff_ZV_wrt2+diff_r1_wrt2*cv.B+cv.r1*diff_B_wrt2))/(cv.ZV+cv.r1*cv.B)**2)))

        diff_ph_diff_wrt1 = np.sign(cv.phl-cv.phv)*(diff_phl_wrt1-diff_phv_wrt1)
        diff_ph_diff_wrt2 = np.sign(cv.phl-cv.phv)*(diff_phl_wrt2-diff_phv_wrt2)

        diff_t1_wrt1 = gasConstant*ov.bp_final1*(diff_ZV_wrt1-diff_ZL_wrt1)
        diff_t1_wrt2 = gasConstant*ov.bp_final1*(diff_ZV_wrt2-diff_ZL_wrt2)

        diff_t2_wrt1 = 0 if cv.b == 0 or (cv.u**2-4*cv.w) <= 0 else ((cv.b*np.sqrt(cv.u**2-4*cv.w))*(diff_ac_wrt1*(ov.bp_final1*cv.diff_alpha_wrt_temp-cv.alpha_eos)+cv.ac*(ov.bp_final1*diff_diff_alpha_wrt1-diff_alpha_wrt1))-(diff_b_wrt1*np.sqrt(cv.u**2-4*cv.w)+cv.b*0.5*(2*cv.u*diff_u_wrt1-4*diff_w_wrt1)/np.sqrt(cv.u**2-4*cv.w))*(cv.ac*(ov.bp_final1*cv.diff_alpha_wrt_temp-cv.alpha_eos)))/(cv.b*np.sqrt(cv.u**2-4*cv.w))**2
        diff_t2_wrt2 = 0 if cv.b == 0 or (cv.u**2-4*cv.w) <= 0 else ((cv.b*np.sqrt(cv.u**2-4*cv.w))*(diff_ac_wrt2*(ov.bp_final1*cv.diff_alpha_wrt_temp-cv.alpha_eos)+cv.ac*(ov.bp_final1*diff_diff_alpha_wrt2-diff_alpha_wrt2))-(diff_b_wrt2*np.sqrt(cv.u**2-4*cv.w)+cv.b*0.5*(2*cv.u*diff_u_wrt2-4*diff_w_wrt2)/np.sqrt(cv.u**2-4*cv.w))*(cv.ac*(ov.bp_final1*cv.diff_alpha_wrt_temp-cv.alpha_eos)))/(cv.b*np.sqrt(cv.u**2-4*cv.w))**2

        diff_t3_wrt1 = 0 if (cv.ZV+cv.B*cv.r2) == 0 or (cv.ZV+cv.B*cv.r1) == 0 or (cv.ZV+cv.B*cv.r2)*(cv.ZV+cv.B*cv.r1) < 0 else (((cv.ZV+cv.B*cv.r1)/(cv.ZV+cv.B*cv.r2))*((cv.ZV+cv.B*cv.r1)*(diff_ZV_wrt1+diff_B_wrt1*cv.r2+cv.B*diff_r2_wrt1)-(diff_ZV_wrt1+diff_B_wrt1*cv.r1+cv.B*diff_r1_wrt1)*(cv.ZV+cv.B*cv.r2))/(cv.ZV+cv.B*cv.r1)**2)
        diff_t3_wrt2 = 0 if (cv.ZV+cv.B*cv.r2) == 0 or (cv.ZV+cv.B*cv.r1) == 0 or (cv.ZV+cv.B*cv.r2)*(cv.ZV+cv.B*cv.r1) < 0 else (((cv.ZV+cv.B*cv.r1)/(cv.ZV+cv.B*cv.r2))*((cv.ZV+cv.B*cv.r1)*(diff_ZV_wrt2+diff_B_wrt2*cv.r2+cv.B*diff_r2_wrt2)-(diff_ZV_wrt2+diff_B_wrt2*cv.r1+cv.B*diff_r1_wrt2)*(cv.ZV+cv.B*cv.r2))/(cv.ZV+cv.B*cv.r1)**2)

        diff_t4_wrt1 = 0 if (cv.ZL+cv.B*cv.r2) == 0 or (cv.ZL+cv.B*cv.r1) == 0 or (cv.ZL+cv.B*cv.r2)*(cv.ZL+cv.B*cv.r1) < 0 else (((cv.ZL+cv.B*cv.r1)/(cv.ZL+cv.B*cv.r2))*((cv.ZL+cv.B*cv.r1)*(diff_ZL_wrt1+diff_B_wrt1*cv.r2+cv.B*diff_r2_wrt1)-(diff_ZL_wrt1+diff_B_wrt1*cv.r1+cv.B*diff_r1_wrt1)*(cv.ZL+cv.B*cv.r2))/(cv.ZL+cv.B*cv.r1)**2)
        diff_t4_wrt2 = 0 if (cv.ZL+cv.B*cv.r2) == 0 or (cv.ZL+cv.B*cv.r1) == 0 or (cv.ZL+cv.B*cv.r2)*(cv.ZL+cv.B*cv.r1) < 0 else (((cv.ZL+cv.B*cv.r1)/(cv.ZL+cv.B*cv.r2))*((cv.ZL+cv.B*cv.r1)*(diff_ZL_wrt2+diff_B_wrt2*cv.r2+cv.B*diff_r2_wrt2)-(diff_ZL_wrt2+diff_B_wrt2*cv.r1+cv.B*diff_r1_wrt2)*(cv.ZL+cv.B*cv.r2))/(cv.ZL+cv.B*cv.r1)**2)

        diff_hv_bp_wrt1 = (diff_t1_wrt1+diff_t2_wrt1*(cv.t3-cv.t4)+cv.t2*(diff_t3_wrt1-diff_t4_wrt1))/1000.
        diff_hv_bp_wrt2 = (diff_t1_wrt2+diff_t2_wrt2*(cv.t3-cv.t4)+cv.t2*(diff_t3_wrt2-diff_t4_wrt2))/1000.

        diff_vb_wrt1 = diff_ZL_wrt1*gasConstant*ov.bp_final1/pressure
        diff_vb_wrt2 = diff_ZL_wrt2*gasConstant*ov.bp_final1/pressure

        diff_wrt1 = self.scales[0]*(2*(ov.hv_bp-cv.hv_bp)/ov.hv_bp**2)*(-1.)*diff_hv_bp_wrt1+self.scales[1]*(2*(ov.vb-cv.vb)/ov.vb**2)*(-1)*diff_vb_wrt1+self.scales[2]*2*(max(0,(cv.ph_diff-10**-7)))*(0 if(cv.ph_diff-10**-7)<=0 else diff_ph_diff_wrt1)
        diff_wrt2 = self.scales[0]*(2*(ov.hv_bp-cv.hv_bp)/ov.hv_bp**2)*(-1.)*diff_hv_bp_wrt2+self.scales[1]*(2*(ov.vb-cv.vb)/ov.vb**2)*(-1)*diff_vb_wrt2+self.scales[2]*2*(max(0,(cv.ph_diff-10**-7)))*(0 if(cv.ph_diff-10**-7)<=0 else diff_ph_diff_wrt2)

        diff_wrt1 = (np.sign(diff_wrt1)*10**7) if np.abs(diff_wrt1)>10**7 else diff_wrt1
        diff_wrt2 = (np.sign(diff_wrt2)*10**7) if np.abs(diff_wrt2)>10**7 else diff_wrt2    
        return np.array([diff_wrt1, diff_wrt2])


class CalculatedValues(object):
    def __init__(self, parent, df, eos, normalize=False):
        self.__dict__ = parent.__dict__.copy()
        self._prepare(df, eos, normalize)

    def _prepare(self, df, eos, normalize):
        if not normalize:
            row = self._intermediate(df, eos)
            self.row = row
        else:
            df = self._final(df, eos)
            self.df = df

    def _final(self, df_o, eos):
        df = df_o.copy(deep=True)
        for index, row in df.iterrows():
            row = self._intermediate(row, eos)
            for col in ["hv_bp", "vb", "Z1", "Z2", "Z3", "ZL", "ZV", "phl", "phv", "ph_diff"]:
                df.loc[df.seq == row.seq, col] = row[col]
        df = self._fixNegative(df, df_o, eos)
        return df

    def _fixNegative(self, df, ov, eos):
        df["marker"] = df.apply(lambda row: 0 if row.ph_diff == 0 or row.ph_diff > 10**-2 else row.ph_diff, axis=1)
        x = df.loc[df.marker != 0, "seq"].tolist()
        for col in ["vb", "hv_bp"] + self._nvar:
            y = df.loc[df.marker != 0, col].tolist()
            df[col] = df.apply(lambda row: row[col] if row.marker != 0 else self._interpolate(x, y, row, col, ov, eos), axis=1)
        return df

    def _interpolate(self, x, y, row, col, ov, eos):
        val_c = monotone(x, y, row.seq)[0]
        if col not in self._nvar and eos != "poas_4":
            val_o = ov.loc[ov.seq == row.seq, col].values[0]
            if val_c > val_o * 1.05 or val_c < val_o * 0.95:
                val_c = val_o
        return val_c

    def _intermediate(self, row_o, eos):
        row = row_o.copy(deep=True)
        row["alpha_eos"] = row.alpha_pr * row.alpha_multiplier
        row["diff_alpha_wrt_temp"] = row.alpha_eos*(((2.*row.alpha_m-2.)/(row.bp_final1/row.tc_k)+row.alpha_l*(-2*row.alpha_m)*(row.bp_final1/row.tc_k)**(2*row.alpha_m-1))/row.tc_k)
        row = getattr(self, eos)(row)
        row = self._zlzv(row)
        row = self._cost(row, row_o)
        return row

    def poas_4(self, row):
        row["om_b"] = ((0.9129*row.zc_final**2-0.0551*row.zc_final+0.0368)*row.om_b_multiplier) if ((0.9129*row.zc_final**2-0.0551*row.zc_final+0.0368)*row.om_b_multiplier) < row.om_b else row.om_b
        row["del_b"] = ((row.zc_final-row.om_b)**3+0.25*(1-3*row.zc_final+row.om_b)**2+3*row.zc_final*row.om_b-row.om_b*(1+2*row.om_b))
        row["del_b"] = 0 if row.del_b<0 else row.del_b
        row["om_c"] = (row.om_b+1-3*row.zc_final+(4*row.del_b)**0.5)/2
        row["om_d"] = (row.om_b+1-3*row.zc_final-(4*row.del_b)**0.5)/2
        row["om_a"] = (3*row.zc_final**2+row.om_b*row.om_c+row.om_b*row.om_d+row.om_c*row.om_d+row.om_c+row.om_d)
        row["b"] = row.om_b*gasConstant*row.tc_k/(row.pc_mpa*10**6)
        row["c"] = row.om_c*gasConstant*row.tc_k/(row.pc_mpa*10**6)
        row["d"] = row.om_d*gasConstant*row.tc_k/(row.pc_mpa*10**6)
        row["ac"] = (row.om_a*gasConstant**2*row.tc_k**2/(row.pc_mpa*10**6))
        row["ac_alpha"] = (row.ac*row.alpha_eos)
        row["u"] = 0 if row.b==0 else ((row.c+row.d)/row.b)
        row["w"] = 0 if row.b==0 else (-1*(row.c*row.d)/row.b**2)
        return row

    def pt(self, row):
        row["ziac"] = row.zc_final * row.zc_multiplier
        row["om_c"] = 1.-3.*row.ziac
        row["a1_d"] = 2.-3.*row.ziac
        row["a2_d"] = 3.*row.ziac**2
        row["a3_d"] = -1.*row.ziac**3
        row = self._omb(row)
        row["om_a"] = 3*row.ziac**2+3*(1-2*row.ziac)*row.om_b+row.om_b**2+1-3*row.ziac
        row["b"] = row.om_b*gasConstant*row.tc_k/(row.pc_mpa*10**6)
        row["c"] = row.om_c*gasConstant*row.tc_k/(row.pc_mpa*10**6)
        row["ac"] = (row.om_a*gasConstant**2*row.tc_k**2/(row.pc_mpa*10**6))
        row["ac_alpha"] = (row.ac*row.alpha_eos)
        row["u"] = 0 if row.b==0 else ((row.c+row.b)/row.b)
        row["w"] = 0 if row.b==0 else (-1*(row.c/row.b))
        return row

    def sw_poas(self, row):
        row["omega_final"] = row.omega_final * row.omega_multiplier
        row["a1_d"] = 3./(1.+6.*row.omega_final)
        row["a2_d"] = 3./(1.+6.*row.omega_final)
        row["a3_d"] = -1./(1.+6.*row.omega_final)
        row = self._omb(row)
        row["final_q"] = row.om_b
        row["eta"] = 1./(3.*(1.+row.final_q*row.omega_final))
        row["om_b"] = row.final_q * row.eta
        row["om_a"] = (1.-row.eta*(1.-row.final_q))**3
        row["b"] = row.om_b*gasConstant*row.tc_k/(row.pc_mpa*10**6)
        row["ac"] = (row.om_a*gasConstant**2*row.tc_k**2/(row.pc_mpa*10**6))
        row["ac_alpha"] = (row.ac*row.alpha_eos)
        row["u"] = 1. + 3.*row.omega_final
        row["w"] = -1 if row.omega_final<0 else -3.*row.omega_final
        return row

    def als(self, row):
        row["omega_final"] = row.omega_final * row.omega_multiplier
        row["om_b"] = 0.001 if (0.08974-0.03452*row.omega_final+0.0033*row.omega_final**2) < 0.001 else (0.08974-0.03452*row.omega_final+0.0033*row.omega_final**2)
        row["om_c"] = 0.001 if (0.03686+0.00405*row.omega_final-0.01073*row.omega_final**2+0.00157*row.omega_final**3) < 0.001 else (0.03686+0.00405*row.omega_final-0.01073*row.omega_final**2+0.00157*row.omega_final**3)
        row["om_d"] = 0.001 if (0.154+0.14122*row.omega_final-0.00272*row.omega_final**2-0.00484*row.omega_final**3) < 0.001 else (0.154+0.14122*row.omega_final-0.00272*row.omega_final**2-0.00484*row.omega_final**3)
        row["om_a"] = 0.001 if (0.44869+0.04024*row.omega_final+0.01111*row.omega_final**2-0.00576*row.omega_final**3) < 0.001 else (0.44869+0.04024*row.omega_final+0.01111*row.omega_final**2-0.00576*row.omega_final**3)
        row["b"] = row.om_b*gasConstant*row.tc_k/(row.pc_mpa*10**6)
        row["c"] = row.om_c*gasConstant*row.tc_k/(row.pc_mpa*10**6)
        row["d"] = row.om_d*gasConstant*row.tc_k/(row.pc_mpa*10**6)
        row["ac"] = (row.om_a*gasConstant**2*row.tc_k**2/(row.pc_mpa*10**6))
        row["ac_alpha"] = (row.ac*row.alpha_eos)
        row["u"] = 0. if row.b == 0 else ((row.d - row.c)/row.b)
        row["w"] = 0. if row.b == 0 else (-1. * row.d * row.c/row.b**2)
        return row

    def generic_poas_a(self, row):
        row["om_b"] = ((0.9129*row.zc_final**2-0.0551*row.zc_final+0.0368)*row.om_b_multiplier) if ((0.9129*row.zc_final**2-0.0551*row.zc_final+0.0368)*row.om_b_multiplier) < row.om_b else row.om_b
        row["b"] = row.om_b*gasConstant*row.tc_k/(row.pc_mpa*10**6)
        row["pf"] = row.b * 10**6/row.vc_final
        row["u"] = 0 if row.pf == 0 else (1. + 1./row.pf * (1./row.zc_final - 3.))
        row["w"] = 0 if ((row.u-1)*row.pf**3+3*row.pf**2) == 0 else ((1-3*row.pf-row.u**2*row.pf**3-3*row.u*row.pf**2)/((row.u-1)*row.pf**3+3*row.pf**2))
        row["om_a"] = 3. * row.zc_final**2 - row.w * row.om_b**2 + row.u * (row.om_b + row.om_b**2)
        row["ac"] = (row.om_a*gasConstant**2*row.tc_k**2/(row.pc_mpa*10**6))
        row["ac_alpha"] = (row.ac*row.alpha_eos)
        return row

    def poas_4a(self, row):
        row["beta"] = row.beta_est * row.beta_multiplier
        row["om_b"] = row.om_b_min if (row.zc_final - row.beta) < row.om_b_min else (row.zc_final - row.beta)
        row["om_c"] = (1. - row.beta)**2 * (row.beta - 0.25)
        row["om_d"] = row.zc_final - (1. - row.beta)/2.
        row["om_a"] = (1. - row.beta)**3
        row["b"] = row.om_b*gasConstant*row.tc_k/(row.pc_mpa*10**6)
        row["c"] = row.om_c*(gasConstant*row.tc_k/(row.pc_mpa*10**6))**2
        row["d"] = row.om_d*gasConstant*row.tc_k/(row.pc_mpa*10**6)
        row["ac"] = (row.om_a*gasConstant**2*row.tc_k**2/(row.pc_mpa*10**6))
        row["ac_alpha"] = (row.ac*row.alpha_eos)
        row["u"] = 0 if row.b == 0 else ((-2. * row.d)/row.b)
        row["w"] = 0 if row.b == 0 else ((row.d**2 + row.c)/row.b**2)
        return row

    def pr(self, row):
        row["om_b"] = 0.0777960739038885*row.om_b_multiplier
        row["om_a"] = 0.457235528921382
        row["b"] = row.om_b*gasConstant*row.tc_k/(row.pc_mpa*10**6)
        row["pf"] = row.b * 10**6/row.vc_final
        row["ac"] = (row.om_a*gasConstant**2*row.tc_k**2/(row.pc_mpa*10**6))
        row["ac_alpha"] = (row.ac*row.alpha_eos)
        row["u"] = 0 if row.pf == 0 else (1. + 1./row.pf * (1./row.zc_final - 3.))
        row["w"] = 0 if ((row.u-1)*row.pf**3+3*row.pf**2) == 0 else ((1-3*row.pf-row.u**2*row.pf**3-3*row.u*row.pf**2)/((row.u-1)*row.pf**3+3*row.pf**2))
        return row

    def srk(self, row):
        row["om_b"] = 0.0866403499649577*row.om_b_multiplier
        row["om_a"] = 0.427480233540341
        row["b"] = row.om_b*gasConstant*row.tc_k/(row.pc_mpa*10**6)
        row["pf"] = row.b * 10**6/row.vc_final
        row["ac"] = (row.om_a*gasConstant**2*row.tc_k**2/(row.pc_mpa*10**6))
        row["ac_alpha"] = (row.ac*row.alpha_eos)
        row["u"] = 0 if row.pf == 0 else (1. + 1./row.pf * (1./row.zc_final - 3.))
        row["w"] = 0 if ((row.u-1)*row.pf**3+3*row.pf**2) == 0 else ((1-3*row.pf-row.u**2*row.pf**3-3*row.u*row.pf**2)/((row.u-1)*row.pf**3+3*row.pf**2))
        return row

    def _omb(self, row):
        row["Q_d"] = (3.*row.a2_d-row.a1_d**2)/9.
        row["L_d"] = (9.*row.a1_d*row.a2_d-27.*row.a3_d-2.*row.a1_d**3)/54.
        row["D_d"] = row.Q_d**3+row.L_d**2
        row["S1_d"] = (-1.*(-1.*(row.L_d+np.sqrt(row.D_d)))**(1./3.)) if (row.L_d+np.sqrt(row.D_d))<0 else (row.L_d+np.sqrt(row.D_d))**(1./3.)
        row["S2_d"] = (-1.*(-1.*(row.L_d-np.sqrt(row.D_d)))**(1./3.)) if (row.L_d-np.sqrt(row.D_d))<0 else (row.L_d-np.sqrt(row.D_d))**(1./3.)
        row["theta"] = np.degrees(np.arccos(1. if np.abs(row.L_d/np.sqrt(-row.Q_d**3)) > 1 else row.L_d/np.sqrt(-row.Q_d**3) ))
        row["Z1_ov"] = 0 if row.Q_d >= 0 else 2.*np.sqrt(-row.Q_d)*np.cos(np.radians(row.theta*1./3.+120)) - row.a1_d/3.
        row["Z2_ov"] = 0 if row.Q_d >= 0 else 2.*np.sqrt(-row.Q_d)*np.cos(np.radians(row.theta*1./3.+240)) - row.a1_d/3.
        row["Z3_ov"] = 0 if row.Q_d >= 0 else 2.*np.sqrt(-row.Q_d)*np.cos(np.radians(row.theta*1./3.)) - row.a1_d/3.
        row["Z1_ov_d"] = row.S1_d + row.S2_d - row.a1_d/3.        
        row["Z1_ov_dd"] = ((-1.*(-1.*row.L_d)**1./3.) if row.L_d < 0 else row.L_d**(1./3.)) * 2. - row.a1_d/3.
        row["Z2_ov_dd"] = (((-1.*(-1.*row.L_d)**1./3.) if row.L_d < 0 else row.L_d**(1./3.))  + row.a1_d/3.) * -1
        row["Z3_ov_dd"] = row.Z2_ov_dd
        row["om_b1"] = self._min([row.Z1_ov, row.Z2_ov, row.Z3_ov])
        row["om_b2"] = self._min([row.Z1_ov_dd, row.Z2_ov_dd, row.Z3_ov_dd])
        row["om_b"] = row.om_b1 if row.D_d<0 else (row.Z1_ov_d if row.D_d > 0 else row.om_b2)  
        return row   

    def _cost(self, row, ov):
        row["t1"] = gasConstant*row.bp_final1*(row.ZV-row.ZL)
        row["t2"] = 10**6 if ((row.b*np.sqrt(row.u**2-4*row.w)) == 0 or (row.u**2-4*row.w) < 0) else (row.ac*(row.bp_final1*row.diff_alpha_wrt_temp-row.alpha_eos)/(row.b*np.sqrt(row.u**2-4*row.w)))
        if ((row.ZV+row.B*row.r2) == 0 or (row.ZV+row.B*row.r1) == 0 or ((row.ZV+row.B*row.r2)*(row.ZV+row.B*row.r1)) < 0):
            row["t3"] = -1.
        else:
            row["t3"] = (np.log((row.ZV+row.B*row.r2)/(row.ZV+row.B*row.r1)))
        if ((row.ZL+row.B*row.r2) == 0 or (row.ZL+row.B*row.r1) == 0 or  ((row.ZL+row.B*row.r2)*(row.ZL+row.B*row.r1)) < 0):
            row["t4"] = -5.
        else:
            row["t4"] = (np.log((row.ZL+row.B*row.r2)/(row.ZL+row.B*row.r1)))
        row["hv_bp"] = ((row.t1+row.t2*(row.t3-row.t4))/1000.)
        row["vb"] = row.ZL*gasConstant*row.bp_final1/pressure
        return row

    def _zlzv(self, row):
        row["r1"] = 0 if (row.u**2-4*row.w) <= 0 else (row.u-np.sqrt(row.u**2-4*row.w))/2
        row["r2"] = 0 if (row.u**2-4*row.w) <= 0 else (row.u+np.sqrt(row.u**2-4*row.w))/2
        row["A"] = row.ac_alpha*pressure*10**6/(gasConstant**2*row.bp_final1**2)
        row["B"] = (row.b*pressure*10**6/(gasConstant*row.bp_final1))
        row["a1"] = (row.u*row.B-row.B-1)
        row["a2"] = (row.A+row.w*row.B**2-row.u*row.B-row.u*row.B**2)
        row["a3"] = -1.*(row.A*row.B+row.w*row.B**2+row.w*row.B**3)
        row["Q"] = (3*row.a2-row.a1**2)/9
        row["L"] = (9*row.a1*row.a2-27*row.a3-2*row.a1**3)/54
        row["D"] = row.Q**3+row.L**2
        row["S1"] = (-1.*(-1.*(row.L+np.sqrt(row.D)))**(1./3.)) if (row.L+np.sqrt(row.D))<0 else (row.L+np.sqrt(row.D))**(1./3.)
        row["S2"] = (-1.*(-1.*(row.L-np.sqrt(row.D)))**(1./3.)) if (row.L-np.sqrt(row.D))<0 else (row.L-np.sqrt(row.D))**(1./3.)
        row["Z1"] = 0 if row.Q >= 0 else 2.*np.sqrt(-row.Q)*np.cos(1./3.*(np.arccos(1. if np.abs(row.L/np.sqrt(-row.Q**3)) > 1 else row.L/np.sqrt(-row.Q**3) ))+2*np.pi/3.) - row.a1/3.
        row["Z2"] = 0 if row.Q >= 0 else 2.*np.sqrt(-row.Q)*np.cos(1./3.*(np.arccos(1. if np.abs(row.L/np.sqrt(-row.Q**3)) > 1 else row.L/np.sqrt(-row.Q**3) ))+4*np.pi/3.) - row.a1/3.
        row["Z3"] = 0 if row.Q >= 0 else 2.*np.sqrt(-row.Q)*np.cos(1./3.*(np.arccos(1. if np.abs(row.L/np.sqrt(-row.Q**3)) > 1 else row.L/np.sqrt(-row.Q**3) ))) - row.a1/3.
        row["Z1_d"] = row.S1 + row.S2 - row.a1/3.        
        row["Z1_dd"] = ((-1.*(-1.*row.L)**1./3.) if row.L < 0 else row.L**(1./3.))  * 2. - row.a1/3.
        row["Z2_dd"] = (((-1.*(-1.*row.L)**1./3.) if row.L < 0 else row.L**(1./3.)) + row.a1/3.) * -1
        row["Z3_dd"] = row.Z2_dd
        row["ZL"] = self._min([row.Z1, row.Z2, row.Z3]) if row.D < 0 else (self._min([row.Z1_dd, row.Z2_dd, row.Z3_dd]) if row.D == 0 else row.Z1_d)
        row["ZV"] = self._max([row.Z1, row.Z2, row.Z3]) if row.D < 0 else (self._max([row.Z1_dd, row.Z2_dd, row.Z3_dd]) if row.D == 0 else 0)
        row["phl"] = self._fugacity(row.r1, row.r2, row.A, row.B, row.ZL, -1.)
        row["phv"] = self._fugacity(row.r1, row.r2, row.A, row.B, row.ZV, 1.)
        row["ph_diff"] = np.abs(row.phl - row.phv)
        return row

    def _fugacity(self, r1, r2, A, B, root, penalty):
        if (root - B) <= 0 or (B*(r2-r1)) == 0 or (root+r1*B) == 0 or (root+r2*B) == 0 or ((root+r2*B)*(root+r1*B)) < 0:
            return penalty
        else:
            return ((root-1)-np.log(root-B)-(A/(B*(r2-r1)))*np.log((root+r2*B)/(root+r1*B)))

    def _min(self, data):
        data = [x for x in data if x > 0]
        if len(data) > 0:
            return min(data)
        return 0

    def _max(self, data):
        data = [x for x in data if x > 0]
        if len(data) > 0:
            return max(data)
        return 0


def _min(data):
    data = [x for x in data if x > 0]
    if len(data) > 0:
        return min(data)
    return 0


class ObservedValues(object):
    def __init__(self, parent, eos):
        self.__dict__ = parent.__dict__.copy()
        self._prepare(eos)

    def _prepare(self, eos):
        df = cm_data(self.args.collection, self.logger)
        cols = ["mf", "bp_final1", "tcbytb", "vc_final", "zc_final", "pc_mpa", "vb", "hv_bp", "alpha_l", "alpha_m", "tc_k", "seq", "component", "omega_final"]
        droplist = [col for col in df.columns if col not in cols]
        df.loc[df.tcbytb < 1.0001, "tcbytb"] = 1.0001
        df["tc_k"] = df.bp_final1 * df.tcbytb
        df["alpha_pr"] = ((df.bp_final1/df.tc_k)**(2*(df.alpha_m-1))*(np.exp(df.alpha_l*(1-(df.bp_final1/df.tc_k)**(2*df.alpha_m)))))
        df.drop(columns=droplist, inplace=True)
        getattr(self, eos)(df)
        self.df = df

    def poas_4(self, df):
        df["a1_d"] = (3.*df.zc_final-1.75)*-1.
        df["a2_d"] = (3.*df.zc_final**2+0.5*(1.-3.*df.zc_final))
        df["a3_d"] = (df.zc_final**3+0.25*(1.-3.*df.zc_final)**2)*-1.
        self._omb(df)

    def _omb(self, df):
        df["Q_d"] = (3.*df.a2_d-df.a1_d**2)/9.
        df["L_d"] = (9.*df.a1_d*df.a2_d-27.*df.a3_d-2.*df.a1_d**3)/54.
        df["D"] = df.Q_d**3+df.L_d**2
        df["S1"] = df.apply(lambda row: (-1.*(-1.*(row.L_d+np.sqrt(row.D)))**(1./3.)) if (row.L_d+np.sqrt(row.D))<0 else (row.L_d+np.sqrt(row.D))**(1./3.), axis=1)
        df["S2"] = df.apply(lambda row: (-1.*(-1.*(row.L_d-np.sqrt(row.D)))**(1./3.)) if (row.L_d-np.sqrt(row.D))<0 else (row.L_d-np.sqrt(row.D))**(1./3.), axis=1)        
        df["theta"] = df.apply(lambda row: np.degrees(np.arccos(1. if np.abs(row.L_d/np.sqrt(-row.Q_d**3)) > 1 else row.L_d/np.sqrt(-row.Q_d**3) )), axis=1)
        df["Z1"] = 2.*np.sqrt(-df.Q_d)*np.cos(np.radians(df.theta*1./3.+120)) - df.a1_d/3.
        df["Z2"] = 2.*np.sqrt(-df.Q_d)*np.cos(np.radians(df.theta*1./3.+240)) - df.a1_d/3.
        df["Z3"] = 2.*np.sqrt(-df.Q_d)*np.cos(np.radians(df.theta*1./3.)) - df.a1_d/3.
        df["Z1_d"] = df.S1 + df.S2 - df.a1_d/3.        
        df["Z1_dd"] = df.apply(lambda row: (-1.*(-1.*row.L_d)**1./3.) if row.L_d < 0 else row.L_d**(1./3.), axis=1) * 2. - df.a1_d/3.
        df["Z2_dd"] = (df.apply(lambda row: (-1.*(-1.*row.L_d)**1./3.) if row.L_d < 0 else row.L_d**(1./3.), axis=1)  + df.a1_d/3.) * -1
        df["Z3_dd"] = df.Z2_dd
        df["om_b1"] = df.apply(lambda row: _min([row.Z1, row.Z2, row.Z3]), axis=1)
        df["om_b2"] = df.apply(lambda row: _min([row.Z1_dd, row.Z2_dd, row.Z3_dd]), axis=1)
        df["om_b"] = df.apply(lambda row: row.om_b1 if row.D<0 else (row.Z1_d if row.D > 0 else row.om_b2), axis=1) * 1.12

    def pt(self, df):
        self._common(df)

    def sw_poas(self, df):
        self._common(df)

    def als(self, df):
        self._common(df)

    def generic_poas_a(self, df):
        self._common(df)
        df["a1_d"] = (3.*df.zc_final-1.75)*-1.
        df["a2_d"] = (3.*df.zc_final**2+0.5*(1.-3.*df.zc_final))
        df["a3_d"] = (df.zc_final**3+0.25*(1.-3.*df.zc_final)**2)*-1.
        self._omb(df)

    def poas_4a(self, df):
        self._common(df)
        df["beta_est"] = 0.79132*df.zc_final-0.02207
        df["om_b_min"] = (0.02207+0.20868*df.zc_final)*0.215

    def pr(self, df):
        self._common(df)

    def srk(self, df):
        self._common(df)

    def _common(self, df):
        p4 = eosbp_data(self.args.collection, self.logger, "poas_4")
        self._fixhvvb(p4)
        df[["hv_bp", "vb"]] = p4[["hv_bp", "vb"]]

    def _fixhvvb(self, df_o):
        df = df_o.copy(deep=True)
        ls = df.index.max()
        cm = cm_data(self.args.collection, self.logger)
        df["mass"] = cm.mass
        df = df.loc[df.index < ls].tail(11)
        df["xi"] = np.log(df.mass)
        df["yi"] = np.log(df.hv_bp)
        df["xiyi"] = df.xi * df.yi
        df["xi2"] = df.xi**2

        xi = df.xi.sum()
        yi = df.yi.sum()
        xiyi = df.xiyi.sum()
        xi2 = df.xi2.sum()
        samples = df.shape[0]
        b = (xi*xiyi-yi*xi2)/(xi**2-samples*xi2)
        a = (yi-samples*b)/xi

        df_o.loc[df_o.index == ls, "hv_bp"] = np.exp(np.log(cm.loc[cm.index == ls, "mass"].values[0])*a + b)
        if df_o.loc[df_o.index == ls, "hv_bp"].values[0] <= 0:
            df_o.loc[df_o.index == ls, "hv_bp"] = 1.
        df_o.loc[df_o.index == ls, "vb"] = cm.loc[cm.index == ls, "vb"].values[0]







