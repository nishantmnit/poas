import argparse
import os
import shutil
import socket
from multiprocessing import Pool

import Scripts.pvt.settings as settings
from Scripts.pvt.core.send_email import send_email
from Scripts.pvt.core.storage.excel import write_excel
from Scripts.pvt.core.test.compare import compare

wells = ["AB8", "ALUMM", "ASPHALTENE10", "AustraliaOilA", "BASEA", "BUAE471", "BahnousGC", "Bakken", "Brazil",
         "C10WHITSON", "CPAX2", "DGC", "Deepwater1G", "Deepwater1o", "GC2", "GC3", "GC4", "GC5", "GC6", "GC7",
         "HIGHSOURGAS", "IRANA", "IRANB", "IRANC", "IRANW3", "IRANWELLX8", "M132", "M152", "M161", "M161A", "M59",
         "MALAYSIA2", "MALAYSIA5", "MCCLAIN45", "MCCLAINC", "MU83", "NORTHALASKAG", "NorthAlskaA", "NorthAlskaB",
         "NorthAlskaE", "NorthAlskaF", "NorthSEA34", "OMAN4", "PhDAshok", "SAB1", "SHALEGC2", "SUDANA", "TOTALF3",
         "TOTBOIL2", "TOTGASC3", "TOTHEAVY1", "TRINIDADPL511", "TRINIDADPL515", "TURWELLB60", "TURWELLBSS1",
         "TURWELLDS1", "TURWELLE2", "U137", "U137_X1", "UAE30770", "UAEWA22", "WAXOIL2", "WAXOIL3", "WELL128",
         "WELLXXBOMOLE", "WHITSONCO", "WHITSONVO", "WestVirginia", "veryheavy"]


def getoptions():
    parser = argparse.ArgumentParser()
    parser.add_argument('--collection', dest='collection', default='', help='Well collection name MC/MC1/MC2')
    parser.add_argument('--validate', dest='validate', default=False, action='store_true',
                        help='Provide self flag to just validate data.')
    parser.add_argument('--bias', dest='bias', default='general',
                        help='Bias - general/gamma/beta. Used in MDBR analysis')
    parser.add_argument('--mass', dest='mass', default=False, action='store_true',
                        help='Provide self flag for mass calculations.')
    parser.add_argument('--density', dest='density', default=False, action='store_true',
                        help='Provide self flag for density calculations.')
    parser.add_argument('--bp', dest='bp', default=False, action='store_true',
                        help='Provide self flag for bp calculations.')
    parser.add_argument('--ri', dest='ri', default=False, action='store_true',
                        help='Provide self flag for Refractivity calculations.')
    parser.add_argument('--cm', dest='cm', default=False, action='store_true',
                        help='Provide self flag for Component Modeling.')
    parser.add_argument('--eosbp', dest='eosbp', default=False, action='store_true',
                        help='Provide self flag for EOS at BP.')
    parser.add_argument('--tr', dest='tr', default=False, action='store_true', help='Provide self flag for EOS at TR.')
    parser.add_argument('--sol', dest='sol', default=False, action='store_true',
                        help='Provide self flag for EOS Solubility.')
    parser.add_argument('--debug', dest='debug', default=False, action='store_true', help='Run in debug mode')
    parser.add_argument('--clear', dest='clear', default=False, action='store_true', help='Clear previous run data')
    args = parser.parse_args()
    if args.collection == "":
        args.collection = wells
    else:
        args.collection = [args.collection]
    return args


args = getoptions()


def run_process_mbp(well):
    print("starting well = %s" % well)
    pvt_main = os.path.join(settings.PVT_DIR, "main.py")
    cmd = "python %s --collection %s %s %s %s %s %s %s %s %s %s %s --bias %s" % (
        pvt_main,
        well, "--validate" if args.validate else "", "--mass" if args.mass else "", "--density" if args.density else "",
        "--bp" if args.bp else "", "--ri" if args.ri else "", "--cm" if args.cm else "",
        "--eosbp" if args.eosbp else "",
        "--tr" if args.tr else "", "--sol" if args.sol else "", "--debug" if args.debug else "", args.bias)
    os.system(cmd)
    print("completed well = %s" % well)


def clear_logs(clear, wells):
    if clear:
        for well in wells:
            path = os.path.join(settings.LOG_DIR, well)
            if os.path.exists(path):
                shutil.rmtree(path)


def clear_debug(clear, wells):
    if clear:
        for well in wells:
            path = os.path.join(settings.DEBUG_DIR, well)
            if os.path.exists(path):
                shutil.rmtree(path)


def compare_debug(wells, bias, debug):
    dfs = list()
    if args.debug:
        for well in wells:
            print "Auditing %s" % well
            dfs.append(compare(settings.GOLDEN_DIR, settings.DEBUG_DIR, well, bias))
    write_excel(debug, settings.DEBUG_TEST_DIR, ["audit"], wells, dfs)


def start_test():
    send_email(subject="POAS PVT TEST ALERT", body="PVT Test has started on %s for following wells \n%s" % (
        socket.gethostname(), "\n".join(args.collection)))
    clear_logs(args.clear, args.collection)
    clear_debug(args.clear, args.collection)

    num = 6
    if len(args.collection) < 6:
        num = len(args.collection)

    pool = Pool(processes=num)
    pool.map(run_process_mbp, args.collection)
    compare_debug(args.collection, args.bias, args.debug)
    send_email(attachment=os.path.join(settings.DEBUG_TEST_DIR, "audit.xlsx"), subject="POAS PVT TEST ALERT",
               body="PVT Test has completed on %s for following wells \n%s" % (
                   socket.gethostname(), "\n".join(args.collection)))


if __name__ == "__main__":
    start_test()



    # usage
    # nohup python main.py --validate --mass --density --bp --ri --cm --eosbp --tr --sol --debug --clear> main.out &

# nohup python main.py --cm --collection AB8 --debug --clear> main.out &

# python main.py --cm --collection ASPHALTENE10 --debug
