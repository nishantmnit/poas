[com.canonical.Unity8]
usage-mode = 'Windowed'

[org.gnome.crypto.pgp]
keyservers=['hkp://keyserver.ubuntu.com:11371', 'hkp://pool.sks-keyservers.net', 'ldap://keyserver.pgp.com']

[org.gnome.desktop.background]
show-desktop-icons=true
picture-uri='file:///usr/share/backgrounds/warty-final-ubuntu.png'

[org.gnome.desktop.interface]
gtk-theme="Ambiance"
icon-theme="ubuntu-mono-dark"
cursor-theme="DMZ-White"
font-name="Ubuntu 11"
monospace-font-name="Ubuntu Mono 13"

[org.gnome.desktop.peripherals.touchpad]
tap-to-click = true

[org.gnome.desktop.sound]
theme-name = 'ubuntu'

[org.gnome.desktop.session]
session-name="ubuntu"

[org.gnome.desktop.wm.keybindings]
maximize = ['<Primary><Super>Up','<Super>Up','<Primary><Alt>KP_5']
minimize = ['<Primary><Alt>KP_0']
move-to-corner-ne = ['<Primary><Alt>KP_Prior']
move-to-corner-nw = ['<Primary><Alt>KP_Home']
move-to-corner-se = ['<Primary><Alt>KP_Next']
move-to-corner-sw = ['<Primary><Alt>KP_End']
move-to-side-e = ['<Primary><Alt>KP_Right']
move-to-side-n = ['<Primary><Alt>KP_Up']
move-to-side-s = ['<Primary><Alt>KP_Down']
move-to-side-w = ['<Primary><Alt>KP_Left']
toggle-maximized = ['<Primary><Alt>KP_5']
toggle-shaded = ['<Primary><Alt>s']
unmaximize = ['<Primary><Super>Down','<Super>Down','<Alt>F5']
show-desktop = ['<Primary><Super>d','<Primary><Alt>d','<Super>d']
switch-to-workspace-up = ['<Control><Alt>Up']
switch-to-workspace-down = ['<Control><Alt>Down']
move-to-workspace-up = ['<Shift><Control><Alt>Up']
move-to-workspace-down = ['<Shift><Control><Alt>Down']

[org.gnome.desktop.wm.preferences]
button-layout='close,minimize,maximize:'
mouse-button-modifier='<Alt>'
theme="Ambiance"
titlebar-font='Ubuntu Bold 11'
action-middle-click-titlebar='lower'
titlebar-uses-system-font=false

[org.gnome.eog.ui]
sidebar=false

[org.gnome.Empathy.conversation]
theme="adium"
theme-variant="Normal"
adium-path="/usr/share/adium/message-styles/ubuntu.AdiumMessageStyle"

[org.gnome.Empathy.notifications]
notifications-focus=true

[org.gnome.Epiphany]
default-search-engine='Google'
search-engines= [('DuckDuckGo', 'https://duckduckgo.com/?q=%s&amp;t=canonical', '!ddg'), ('Google', 'https://www.google.com/search?client=ubuntu&channel=es&q=%s', '!g'), ('Bing', 'https://www.bing.com/search?q=%s', '!b')]

[org.gnome.login-screen]
logo='/lib/plymouth/ubuntu_logo.png'

[org.gnome.mutter.keybindings]
toggle-tiled-left = ['<Primary><Super>Left','<Super>Left']
toggle-tiled-right = ['<Primary><Super>Right','<Super>Right']

[org.gnome.nautilus.desktop]
home-icon-visible=false
trash-icon-visible=false
volumes-visible=false

[org.gnome.nautilus.icon-view]
default-zoom-level='small'

[org.gnome.nautilus.preferences]
sort-directories-first=true
enable-interactive-search=true
open-folder-on-dnd-hover=false

[org.gnome.rhythmbox.encoding-settings]
media-type-presets={'audio/x-vorbis':'Ubuntu', 'audio/mpeg':'Ubuntu'}

[org.gnome.rhythmbox.rhythmdb]
monitor-library=true

[org.gnome.rhythmbox.plugins]
active-plugins=['artsearch', 'audiocd','audioscrobbler','cd-recorder','daap','dbus-media-server','generic-player','ipod','iradio','mmkeys','mpris','mtpdevice','notification','power-manager']

[org.gnome.settings-daemon.plugins.power]
button-power = 'interactive'
button-sleep = 'suspend'
critical-battery-action = 'suspend'

[org.gnome.settings-daemon.plugins.xsettings]
antialiasing = 'rgba'

[org.gnome.settings-daemon.plugins.media-keys]
screensaver = '<Control><Alt>l'

[org.gnome.settings-daemon.plugins.print-notifications]
active = false

[org.gnome.settings-daemon.plugins.updates]
active = false

[org.gnome.settings-daemon.plugins.background]
active = false

[org.gnome.shell]
favorite-apps=[ 'ubiquity.desktop', 'firefox.desktop', 'evolution.desktop', 'rhythmbox.desktop', 'org.gnome.Photos.desktop', 'org.gnome.Nautilus.desktop', 'org.gnome.Software.desktop', 'yelp.desktop' ]

[org.gnome.sushi]
client-decoration = false

[org.gnome.software]
first-run=false
official-sources=['ubuntu-*']

[org.gtk.Settings.FileChooser]
sort-directories-first = true

[org.onboard]
layout='Compact'
theme='Nightshade'
key-label-font='Ubuntu'
key-label-overrides=['RWIN::super-group', 'LWIN::super-group']
xembed-onboard=true

[org.onboard.window]
docking-enabled=true
force-to-top=true
