import datetime
import pandas as pd
from Lib.DBLib import getDbCon

import Lib.Utils as Utils
import settings

logger = Utils.getLogger("Mongo central storage", settings.LOG_DIR)


def write(df_o, db, collection, **kwargs):
    conn = get_connection(db, collection)
    df = df_o.copy(deep=True)
    df["index"] = df.index  # preserve index to be set again in fetch
    df.reset_index(inplace=True, drop=True)  # there can be duplicates in index
    df.index = df.index.map(str)
    df["seq"] = range(0, df.shape[0])
    data = df.to_dict()
    for key, value in kwargs.items():
        data[key] = value
    for k, v in data.items():
        k = k.encode('ascii', 'ignore')
        data[k] = v
    data["CreatedAt"] = datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
    data["CreatedBy"] = 'main'
    delete_all(db, collection, **kwargs)
    r = conn.insert(data, check_keys=False)
    # logger.info('CENTRAL_WRITE:{%s, "id": "%s"}' % (kwargs, r))


def delete_all(db, collection, **kwargs):
    # logger.info('CENTRAL_DELETE:{%s}' % kwargs)
    conn = get_connection(db, collection)
    conn.remove(kwargs)


def fetch(db, collection, **kwargs):
    gen = find_all(db, collection, **kwargs)
    dfs_dict, id = dict(), 0
    for record in gen:
        id += 1
        df_per_rec = pd.DataFrame(index=record["index"].values())
        for key in record.keys():
            if key not in ["CreatedAt", "CreatedBy", "_id", "index"] + kwargs.keys():
                if type(record[key]) is dict:
                    df_per_rec[key] = record[key].values()
                else:
                    df_per_rec[key] = record[key]
        dfs_dict[id] = df_per_rec
    df = pd.concat(dfs_dict.values())
    df.sort_values(by=["seq"], inplace=True)
    return df


def find_all(db, collection, **kwargs):
    conn = get_connection(db, collection)
    data = list()
    for key, value in kwargs.items():
        data.append({key: value})
    gen = conn.find({'$and': data}) if len(data) > 0 else conn.find()
    if gen.count() == 0:
        raise RuntimeError("Found no document = %s. Please check and re-run." % data)
    return gen


def get_connection(db, collection):
    db_con = getDbCon(db)
    conn = db_con[collection]
    return conn
