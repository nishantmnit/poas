import matplotlib
import matplotlib.pyplot as plt
from matplotlib.collections import PolyCollection
from matplotlib import ticker
from mpl_toolkits.mplot3d import axes3d
import numpy as np
from matplotlib import colors as mcolors
import matplotlib.dates as mdates
import datetime as dt
import matplotlib.dates as dates
import matplotlib.ticker as ticker
import re, os, sys, datetime, time
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from scipy.interpolate import make_interp_spline, BSpline


cwd = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(0, "%s/../../Lib" %(cwd))

from Utils import *
from GraphLib import *

class WMLGraphs(object):
    def __init__(this, service=None, collection= None, logger=None):
        this.logger= logger
        this.collection=collection
        this.service=service
        pass

    def format_date(this, x, pos=None):
         return dates.num2date(x).strftime('%H:%M:%S')


    def plot_WellTrajectory_MD_INC_AZ(this, output, Identifier, collection, module, graphName, title):
        plt.style.use('dark_background')
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        # The zdir keyword makes it plot the "z" vertex dimension (Time)
        # along the y axis. The zs keyword sets each polygon at the
        # correct Time value.
        ax.set_title(title, color='white')

        for workover in reversed(sorted(output)):
            if workover == 'workover1': continue
            x, y, z=[], [], []
            for d in output[workover]:
                for i in range(len(d['TVD'])): 
                    x.append(d['GridEast'][i])
                    y.append(d['GridNorth'][i])
                    z.append(d['TVD'][i])
            ax.plot3D(x[::-1], y[::-1], z[::-1], label=workover)
        
        ax.set_xlabel('X:Easting(Meter)')
        ax.set_ylabel('Y:Northing(Meter)')
        ax.set_zlabel('Z:TVD')
        plt.legend()

        #Z axis position
        # tmp_planes= ax.zaxis._PLANES
        # ax.zaxis._PLANES = ( tmp_planes[2], tmp_planes[3], tmp_planes[0], tmp_planes[1], tmp_planes[4], tmp_planes[5])

        # ax.w_yaxis.set_major_locator(ticker.FixedLocator(time_data)) 
        # ax.w_yaxis.set_major_formatter(ticker.FuncFormatter(this.format_date))
        # for tl in ax.w_yaxis.get_ticklabels():
        #        tl.set_ha('left')
        #        tl.set_rotation(350) 
        ax.view_init(elev=-144, azim=39)      
        plt.tight_layout(pad=1.7)

        #plt.show()
        graphPath= os.path.join(getPOASRootDir(), 'Scripts', 'WellModel', 'output', collection, module, graphName)
        if not os.path.exists(os.path.dirname(graphPath)):
            os.makedirs(os.path.dirname(graphPath))        
        fig.savefig(graphPath, dpi=fig.dpi)
        plt.cla()
        plt.close(fig)
        this.printSTDOUTMsg(Identifier, graphPath)


    def printSTDOUTMsg(this, i, v):
        v= os.path.join(os.path.basename(os.path.dirname(v)), (os.path.basename(v)))
        print 'MCOutput:{"i":"%s", "graph":"%s"}' %(i, v)
