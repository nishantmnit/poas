import numpy as np
import sys, re, os, argparse, getopt, datetime, time
from math import exp, log, sqrt, log10, tan, radians, cos, acos, degrees, atan, sin
import math
from itertools import groupby

cwd = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(0, "%s/../../Lib" %(cwd))

from Utils import *
from DBLib import *
from WMLGraphs import *

class WellTrajectory(object):
    def __init__(this, service=None, wellno=None, logger=None):
        this.getoptions()
        this.wellno= this.args.wellno if wellno is None else wellno
        this.service= service
        this.logger= logger
        this.db= getDbCon(this.args.service if service is None else service)
        this.coll= this.db[this.args.wellno if wellno is None else wellno] 
        this.dateYmdHms= datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
        #collectionCentral= 'Central'
        #if collectionCentral not in this.db.collection_names(): this.db.create_collection(collectionCentral)
        #this.collectionCentral= this.db[collectionCentral]

    def getoptions(this):
        parser = argparse.ArgumentParser()
        parser.add_argument("-s", "--service",      dest='service',     default="WML",          help='RTS/WML')
        parser.add_argument("-w", "--wellno",       dest='wellno',      default="Well1",        help='Well name Well1/Well2/Well3')
        parser.add_argument("-e", "--excel",        dest='excel',       default=None,           help='input data in excel')
        parser.add_argument("-p", "--printoutput",  dest="printoutput", action='store_true',    default=False)
        this.args = parser.parse_args()

    def getChangeInMD(this, change, start, end, buildRate_i, turnRate_i, INC_i, AZ_i, GridEastX_i, GridNorthY_i, finalTVD_i, x_easting_i, y_northing_i, MSL):
        MD_change= np.arange(start, end, change)
        if len(MD_change) == 0: MD_change= np.array([start, end])
        if end - MD_change[-1] > 0: MD_change=np.append(MD_change, [MD_change[-1]+(end - MD_change[-1])]) #append last difference or last value
        MD_changeLen= len(MD_change)
        INC_betnMd_deg= [INC_i]
        for i in range(1, MD_changeLen): INC_betnMd_deg=np.append(INC_betnMd_deg, [INC_betnMd_deg[-1]+buildRate_i/100.*(MD_change[i]-MD_change[i-1])])
        INC_betnMd_rad= np.radians(INC_betnMd_deg)
        AZ_betnMd_deg= [AZ_i]
        for i in range(1, MD_changeLen): AZ_betnMd_deg=np.append(AZ_betnMd_deg, [AZ_betnMd_deg[-1]+turnRate_i/100.*(MD_change[i]-MD_change[i-1])])
        AZ_betnMd_rad= np.radians(AZ_betnMd_deg)
        DLS=100./(MD_change[1:]-MD_change[0:-1])*2.*np.degrees(np.arcsin((np.sqrt((np.sin(np.radians((INC_betnMd_deg[1:]-INC_betnMd_deg[0:-1])/2.)))**2.+np.sin(np.radians(INC_betnMd_deg[0:-1]))*np.sin(np.radians(INC_betnMd_deg[1:]))*(np.sin(np.radians((AZ_betnMd_deg[1:]-AZ_betnMd_deg[0:-1])/2.)))**2))))
        DLS= np.append(DLS, [DLS[-1]])
        alpha= DLS[1:]*(MD_change[1:]-MD_change[0:-1])/100.
        functionAlpha=  np.array([(1.+a**2./12.*(1.+a**2./10.*(1.+a**2./168.*(1.+31.*a**2./18.)))) if(a<0.2) else ((tan((radians(a))/2.))/(radians(a)/2.)) for a in alpha])
        changeInTVD= (MD_change[1:]-MD_change[0:-1])/2.*functionAlpha*(np.cos(INC_betnMd_rad[1:])+np.cos(INC_betnMd_rad[0:-1]))
        finalTVD= [MD_change[0] if finalTVD_i is None else finalTVD_i]
        for i in range(0, MD_changeLen-1): finalTVD=np.append(finalTVD, [finalTVD[-1]+changeInTVD[i]])
        delE=((np.sin(INC_betnMd_rad[0:-1])*np.sin(AZ_betnMd_rad[0:-1])+np.sin(INC_betnMd_rad[1:])*np.sin(AZ_betnMd_rad[1:]))/3.280839895)*((MD_change[1:]-MD_change[0:-1])/2.)*functionAlpha
        delN=((np.sin(INC_betnMd_rad[0:-1])*np.cos(AZ_betnMd_rad[0:-1])+np.sin(INC_betnMd_rad[1:])*np.cos(AZ_betnMd_rad[1:]))/3.280839895)*((MD_change[1:]-MD_change[0:-1])/2.)*functionAlpha
        x_easting= [GridEastX_i if x_easting_i is None else x_easting_i]
        for i in range(0, MD_changeLen-1): x_easting=np.append(x_easting, [x_easting[-1]+delE[i]])
        y_northing= [GridNorthY_i if y_northing_i is None else y_northing_i]
        for i in range(0, MD_changeLen-1): y_northing=np.append(y_northing, [y_northing[-1]+delN[i]])
        
        buildRate= np.repeat(buildRate_i, MD_changeLen)
        turnRate= np.repeat(turnRate_i, MD_changeLen)
        delE= np.insert(delE, 0, 0)
        delN= np.insert(delN, 0, 0)
        
        out= {'MD':MD_change, 'Inclination':INC_betnMd_deg, 'Azimuth':AZ_betnMd_deg, 'TVD':finalTVD, 'TVDSS':finalTVD-MSL, 'East':delE, 'North':delN, 'GridEast':x_easting, 'GridNorth':y_northing, 'DLS':DLS, 'BuildRate':buildRate, 'TurnRate':turnRate}
        return out, finalTVD[-1], x_easting[-1], y_northing[-1]

    def findINC(this, MD, TVD, AZ):
        changeInTVD= TVD[1:]-TVD[0:-1]
        INC= [0]
        for i in range(1, len(changeInTVD)+1): 
            INC.append(acos(0.99999999999 if((2.*changeInTVD[i-1])/(MD[i]-MD[i-1])-cos(INC[-1]))>0.99999999999 else ((2*changeInTVD[i-1])/(MD[i]-MD[i-1])-cos(INC[-1]))))
        INC= np.degrees(INC)
        return INC

    def getFinalINCradian(this, changeInTVD, AZradian, obsE, obsN, guessINCradian):
        multiplier= np.array([round(i, 2) for i in np.linspace(0.01, 2, 200)]+[round(i, 2) for i in np.linspace(2.1, 3, 10)]+[round(i, 2) for i in np.linspace(3.2, 10, 35)])
        idx= np.arange(len(multiplier)) #0..244
        finalINCradian=[guessINCradian[0]]
        for i in range(1, len(changeInTVD)+1):
            a= (obsE[i]-((sin(finalINCradian[-1])*sin(AZradian[i-1])+np.sin(guessINCradian[i]*multiplier)*sin(AZradian[i]))/(cos(finalINCradian[-1])+np.cos(guessINCradian[i]*multiplier))))
            a= np.abs(np.divide(a, obsE[i], out=np.zeros_like(a), where=obsE[i] != 0))
            b= (obsN[i]-((sin(finalINCradian[-1])*cos(AZradian[i-1])+np.sin(guessINCradian[i]*multiplier)*cos(AZradian[i]))/(cos(finalINCradian[-1])+np.cos(guessINCradian[i]*multiplier))))
            b= np.abs(np.divide(b, obsN[i], out=np.zeros_like(b), where=obsN[i] != 0))
            obj= a+b
            miltiplier_minObj= multiplier[np.argmin(obj)]
            idx_miltiplier_minObj= np.argmin(obj)
            idx2= idx[0] if(idx_miltiplier_minObj-2)<=idx[0] else idx[-1] if(idx_miltiplier_minObj-2)>=idx[-1] else (idx_miltiplier_minObj-2)
            idx1= idx[0] if(idx_miltiplier_minObj-1)<=idx[0] else idx[-1] if(idx_miltiplier_minObj-1)>=idx[-1] else (idx_miltiplier_minObj-1)
            idx3= idx[0] if(idx_miltiplier_minObj+1)<=idx[0] else idx[-1] if(idx_miltiplier_minObj+1)>=idx[-1] else (idx_miltiplier_minObj+1)
            mul1, mul2, mul3, mul_min= multiplier[idx1], multiplier[idx2], multiplier[idx3], miltiplier_minObj
            obj1, obj2, obj3, obj_min= obj[idx1], obj[idx2], obj[idx3], np.min(obj)
            k1= (mul1**3-mul2**3)*(mul_min-mul2)-(mul_min**3-mul2**3)*(mul1-mul2)
            k2= (mul1**2-mul2**2)*(mul_min-mul2)-(mul_min**2-mul2**2)*(mul1-mul2)
            k3= (mul1**3-mul2**3)*(mul3-mul2)-(mul3**3-mul2**3)*(mul1-mul2)
            k4= (mul1**2-mul2**2)*(mul3-mul2)-(mul3**2-mul2**2)*(mul1-mul2)
            p1= (obj1-obj2)*(mul_min-mul2)-(obj_min-obj2)*(mul1-mul2)
            p2= (obj1-obj2)*(mul3-mul2)-(obj3-obj2)*(mul1-mul2)
            a= 0. if(k1*k4-k2*k3)==0 else (p1*k4-p2*k2)/(k1*k4-k2*k3)
            b= 0. if k2==0 else (p1-a*k1)/k2
            c= 0. if (mul1-mul2)==0 else ((obj1-obj2)-a*(mul1**3-mul2**3)-b*(mul1**2-mul2**2))/(mul1-mul2)
            d= obj2-(a*mul2**3+b*mul2**2+c*mul2)
            mul_min1= mul_min if((4.*b**2.-12.*a*c)<0 or a==0) else ((-2.*b+sqrt(4.*b**2.-12.*a*c))/(6.*a))
            mul_min2= mul_min if((4.*b**2.-12.*a*c)<0 or a==0) else ((-2.*b-sqrt(4.*b**2.-12.*a*c))/(6.*a))
            fn_mul_min1= a*mul_min1**3+b*mul_min1**2+c*mul_min1+d
            fn_mul_min2= a*mul_min2**3+b*mul_min2**2+c*mul_min2+d
            final_mul_min= mul_min1 if fn_mul_min1<=fn_mul_min1 else mul_min2
            finalINCradian.append(final_mul_min*guessINCradian[i])
        INCangle_degrees= np.degrees(np.array(finalINCradian))
        finalINCangle_degrees= np.array([ (INCangle_degrees[i-1]+INCangle_degrees[i+1])/2. if INCangle_degrees[i] > 150 else INCangle_degrees[i] for i in range(1, len(INCangle_degrees)-1)])
        finalINCangle_degrees= np.insert(finalINCangle_degrees, 0, INCangle_degrees[0])
        finalINCangle_degrees= np.insert(finalINCangle_degrees, len(finalINCangle_degrees), INCangle_degrees[-1])
        return finalINCangle_degrees

    def getFinalChangeInMD(this, changeInTVD, INC, AZ, alphaFun, guessChangeInMD):
        multiplier= np.array([0.005]+[round(i, 2) for i in np.linspace(0.01, 2, 200)]+[round(i, 2) for i in np.linspace(2.1, 50, 480)])
        idx= np.arange(len(multiplier)) #0..680
        finalChangeInMD=[]
        for i in range(1, len(changeInTVD)+1):
            obj= np.abs((changeInTVD[i-1]-(alphaFun[i-1]*(guessChangeInMD[i-1]*multiplier)/2.*(cos(radians(INC[i-1]))+cos(radians(INC[i])))))/changeInTVD[i-1])
            miltiplier_minObj= multiplier[np.argmin(obj)]
            idx_miltiplier_minObj= np.argmin(obj)
            idx2= idx[0] if(idx_miltiplier_minObj-2)<=idx[0] else idx[-1] if(idx_miltiplier_minObj-2)>=idx[-1] else (idx_miltiplier_minObj-2)
            idx1= idx[0] if(idx_miltiplier_minObj-1)<=idx[0] else idx[-1] if(idx_miltiplier_minObj-1)>=idx[-1] else (idx_miltiplier_minObj-1)
            idx3= idx[0] if(idx_miltiplier_minObj+1)<=idx[0] else idx[-1] if(idx_miltiplier_minObj+1)>=idx[-1] else (idx_miltiplier_minObj+1)
            mul1, mul2, mul3, mul_min= multiplier[idx1], multiplier[idx2], multiplier[idx3], miltiplier_minObj
            obj1, obj2, obj3, obj_min= obj[idx1], obj[idx2], obj[idx3], np.min(obj)
            k1= (mul1**3-mul2**3)*(mul_min-mul2)-(mul_min**3-mul2**3)*(mul1-mul2)
            k2= (mul1**2-mul2**2)*(mul_min-mul2)-(mul_min**2-mul2**2)*(mul1-mul2)
            k3= (mul1**3-mul2**3)*(mul3-mul2)-(mul3**3-mul2**3)*(mul1-mul2)
            k4= (mul1**2-mul2**2)*(mul3-mul2)-(mul3**2-mul2**2)*(mul1-mul2)
            p1= (obj1-obj2)*(mul_min-mul2)-(obj_min-obj2)*(mul1-mul2)
            p2= (obj1-obj2)*(mul3-mul2)-(obj3-obj2)*(mul1-mul2)
            a= 0. if(k1*k4-k2*k3)==0 else (p1*k4-p2*k2)/(k1*k4-k2*k3)
            b= 0. if k2==0 else (p1-a*k1)/k2
            c= 0. if (mul1-mul2)==0 else ((obj1-obj2)-a*(mul1**3-mul2**3)-b*(mul1**2-mul2**2))/(mul1-mul2)
            d= obj2-(a*mul2**3+b*mul2**2+c*mul2)
            mul_min1= mul_min if((4.*b**2.-12.*a*c)<0 or a==0) else ((-2.*b+sqrt(4.*b**2.-12.*a*c))/(6.*a))
            mul_min2= mul_min if((4.*b**2.-12.*a*c)<0 or a==0) else ((-2.*b-sqrt(4.*b**2.-12.*a*c))/(6.*a))
            fn_mul_min1= a*mul_min1**3+b*mul_min1**2+c*mul_min1+d
            fn_mul_min2= a*mul_min2**3+b*mul_min2**2+c*mul_min2+d
            final_mul_min= mul_min1 if fn_mul_min1<=fn_mul_min1 else mul_min2
            finalChangeInMD.append(final_mul_min*guessChangeInMD[i-1])
        return finalChangeInMD

    def findMD_INC_AZ(this, TVD, GridEastX, GridNorthY, MSL):
        MD, INC, AZ= None, None, None
        changeInTVD= TVD[1:]-TVD[0:-1]
        changeInEasting=  (GridEastX[1:]-GridEastX[0:-1])*3.280839895
        changeInNorthing= (GridNorthY[1:]-GridNorthY[0:-1])*3.280839895
        obsE= np.divide(changeInEasting, changeInTVD, out=np.zeros_like(changeInEasting), where=changeInTVD != 0)
        obsE= np.insert(obsE, 0, 0.0)
        obsN= np.divide(changeInNorthing, changeInTVD, out=np.zeros_like(changeInNorthing), where=changeInTVD != 0)
        obsN= np.insert(obsN, 0, 0.0)
        AZdegree= []
        for i in range(len(changeInEasting)):
            a=None
            if(changeInEasting[i]==0 and changeInNorthing[i]<0): a= 180.
            elif(changeInEasting[i]==0 and changeInNorthing[i]>0): a=0.
            elif(changeInNorthing[i]==0 and changeInEasting[i]<0): a=270.
            elif(changeInNorthing[i]==0 and changeInEasting[i]>0): a=90.
            elif(changeInEasting[i]==0 and changeInNorthing[i]==0): a=0.
            elif(changeInEasting[i]<0 and changeInNorthing[i]>0): a=360.+degrees(atan(changeInEasting[i]/changeInNorthing[i]))
            elif(changeInEasting[i]>0 and changeInNorthing[i]<0): a=180.+degrees(atan(changeInEasting[i]/changeInNorthing[i]))
            elif(changeInEasting[i]<0 and changeInNorthing[i]<0): a=180+degrees(atan(changeInEasting[i]/changeInNorthing[i]))
            else: a=degrees(atan(changeInEasting[i]/changeInNorthing[i]))
            AZdegree.append(a)
        AZdegree= np.insert(AZdegree, 0, AZdegree[0])
        AZradian= np.radians(AZdegree)
        #guessINCdegree= np.abs(np.degrees(np.arctan((changeInEasting**2+changeInNorthing**2)/changeInTVD)))  # divide by zero ?
        changeInEpN2= changeInEasting**2+changeInNorthing**2
        guessINCdegree= np.abs(np.degrees(np.arctan(np.divide(changeInEpN2, changeInTVD, out=np.full_like(changeInEpN2, 90.), where=changeInTVD != 0))))
        #print guessINCdegree
        guessINCdegree= np.insert(guessINCdegree, 0, 0.0)
        guessINCradian= np.radians(guessINCdegree)
        finalINCangle_degrees= this.getFinalINCradian(changeInTVD, AZradian, obsE, obsN, guessINCradian)
        doglegRadian= np.arccos((np.cos(np.radians(finalINCangle_degrees[0:-1]))*np.cos(np.radians(finalINCangle_degrees[1:]))+np.sin(np.radians(finalINCangle_degrees[0:-1]))*np.sin(np.radians(finalINCangle_degrees[1:]))*(np.cos(np.radians(AZdegree[1:]))-np.cos(np.radians(AZdegree[0:-1])))))
        doglegRadian= np.insert(doglegRadian, 0, 0.0)
        changeInMD_Factor=np.sqrt((TVD[1:]-TVD[0:-1])**2+((GridEastX[1:]-GridEastX[0:-1])*3.280839895)**2+((GridNorthY[1:]-GridNorthY[0:-1])*3.280839895)**2)
        changeInMD_Factor= np.insert(changeInMD_Factor, 0, 0.0)
        changeInMD= [(TVD[i]-TVD[i-1]) if(1. if(sin((doglegRadian[i]-doglegRadian[i-1])/2.)==0) else((changeInMD_Factor[i]/2.)*(doglegRadian[i]-doglegRadian[i-1])/sin((doglegRadian[i]-doglegRadian[i-1])/2.)))<(TVD[i]-TVD[i-1]) else 1. if sin((doglegRadian[i]-doglegRadian[i-1])/2.)==0 else ((changeInMD_Factor[i]/2.)*(doglegRadian[i]-doglegRadian[i-1])/sin((doglegRadian[i]-doglegRadian[i-1])/2.)) for i in range(1, len(changeInMD_Factor))]
        changeInMD= np.insert(changeInMD, 0, 0.0)
        MD= [changeInMD[0]]
        for i in range(1, len(changeInMD)): MD.append(changeInMD[i]+MD[-1])
        smoothINCradian= [0]
        for i in range(1, len(changeInMD)): smoothINCradian.append(acos(0.99999999999 if((2.*(TVD[i]-TVD[i-1]))/changeInMD[i]-cos(smoothINCradian[-1]))>0.99999999999 else ((2.*(TVD[i]-TVD[i-1]))/changeInMD[i]-cos(smoothINCradian[-1]))))
        INCdegree= np.degrees(smoothINCradian)
        return np.array(MD), INCdegree, AZdegree

    def getAlpha(this, INC, AZ):
        alpha= 2.*np.degrees(np.arcsin((np.sqrt((np.sin(np.radians((INC[1:]-INC[0:-1])/2.)))**2+np.sin(np.radians(INC[0:-1]))*np.sin(np.radians(INC[1:]))*(np.sin(np.radians((AZ[1:]-AZ[0:-1])/2.)))**2))))
        return alpha

    def getAlphaFunction(this, alpha):
        alphaFun= np.array([(1.+a**2./12.*(1.+a**2./10.*(1.+a**2./168.*(1.+31.*a**2./18.)))) if(a<0.2) else ((tan((radians(a))/2.))/(radians(a)/2.)) for a in alpha])
        return alphaFun

    def findMDUsingRegion1(this, TVD, INC, AZ):
        changeInTVD= TVD[1:]-TVD[0:-1]
        #alpha= 2.*np.degrees(np.arcsin((np.sqrt((np.sin(np.radians((INC[1:]-INC[0:-1])/2.)))**2+np.sin(np.radians(INC[0:-1]))*np.sin(np.radians(INC[1:]))*(np.sin(np.radians((AZ[1:]-AZ[0:-1])/2.)))**2))))
        #alpha= np.insert(alpha, 0, 0.0)
        alpha= this.getAlpha(INC, AZ)
        alpha= np.insert(alpha, 0, 0.0)
        #alphaFun= np.array([(1.+a**2./12.*(1.+a**2./10.*(1.+a**2./168.*(1.+31.*a**2./18.)))) if(a<0.2) else ((tan((radians(a))/2.))/(radians(a)/2.)) for a in alpha])
        alphaFun= this.getAlphaFunction(alpha)
        changeInMD1= changeInTVD*2./(alphaFun[1:]*(np.cos(np.radians(INC[1:]))+np.cos(np.radians(INC[0:-1]))))
        changeInMD2= changeInTVD*2./((np.cos(np.radians(INC[1:]))+np.cos(np.radians(INC[0:-1]))))
        changeInMDFinal= np.array([changeInMD2[i] if changeInMD1[i] < changeInTVD[i] else changeInMD1[i] for i in range(len(changeInTVD))])
        #changeInMDFinal= np.insert(changeInMDFinal, 0, 0)
        return changeInMDFinal


    def findMDUsingRegion2_1(this, changeInTVD, INC, AZ):
        alpha= this.getAlpha(INC, AZ)
        alphaFun= this.getAlphaFunction(alpha)
        guessChangeInMD= np.array([c*100. if c > 100 else 100. for c in changeInTVD])
        changeInMDFinal= this.getFinalChangeInMD(changeInTVD, INC, AZ, alphaFun, guessChangeInMD)
        return changeInMDFinal

    def findMDUsingRegion2(this, TVD, INC, AZ):
        changeInMDFinal= []
        changeInTVD= TVD[1:]-TVD[0:-1]
        regionMarkers= []
        TVDlessThan0001Count=0
        for i in range(len(changeInTVD)):
            regionMarkers.append(2.1 if abs(changeInTVD[i]) >= 0.0001 else 2.2)
        res=[(-1,-1,-1)] # region, startIndex, stopIndex
        for region, j in groupby(regionMarkers): res.append((region, res[-1][2]+1, res[-1][2]+len(list(j))))
        allchangeInMDFinal=[]
        for r in res[1:]:
            region, startIndex, stopIndex= r
            if region == 2.1:
                finalChangeInMD= this.findMDUsingRegion2_1(changeInTVD[startIndex:stopIndex+1], INC[startIndex:stopIndex+2], AZ[startIndex:stopIndex+2])
                allchangeInMDFinal.extend(finalChangeInMD)
            else:
                TVDlessThan0001Count += len(changeInTVD[startIndex:stopIndex+1])
                allchangeInMDFinal.extend(['x' for x in range(len(changeInTVD[startIndex:stopIndex+1]))])
        return allchangeInMDFinal, TVDlessThan0001Count

    def case4_TVD_GridEastX_GridNorthY(this, TVD, GridEastX, GridNorthY, MSL):
        MD, INC, AZ= this.findMD_INC_AZ(TVD, GridEastX, GridNorthY, MSL)
        output= this.case1_MD_INC_AZ(MD, INC, AZ, GridEastX, GridNorthY, MSL)
        return output        

    def case3_MD_TVD_AZ(this, MD, TVD, AZ, GridEastX, GridNorthY, MSL):
        INC= this.findINC(MD, TVD, AZ)
        output= this.case1_MD_INC_AZ(MD, INC, AZ, GridEastX, GridNorthY, MSL)
        return output        
        
    def case2_TVD_INC_AZ(this, TVD, INC, AZ, GridEastX, GridNorthY, MSL, MDatTargetDepth):
        regionMarkers= []
        TVDlessThan0001Counts=0.
        #MD= [0., changeInMDFinal[0]]
        for i in range(len(INC)):
            regionMarkers.append(1 if INC[i] <= 83 or INC[i] >= 93 else 2)
        res=[(-1,-1,-1)] # region, startIndex, stopIndex
        for region, j in groupby(regionMarkers): res.append((region, res[-1][2]+1, res[-1][2]+len(list(j))))
        start= 0
        allchangeInMDFinal=[]
        for r in res[1:]:
            start += 1
            region, startIndex, stopIndex= r
            if start > 1: startIndex -= 1
            if region == 1:
                cMD= this.findMDUsingRegion1(TVD[startIndex:stopIndex+1], INC[startIndex:stopIndex+1], AZ[startIndex:stopIndex+1])
                allchangeInMDFinal.extend(cMD)
                #print "region1:", MD
            else:
                cMD, TVDlessThan0001Count= this.findMDUsingRegion2(TVD[startIndex:stopIndex+1], INC[startIndex:stopIndex+1], AZ[startIndex:stopIndex+1])
                TVDlessThan0001Counts += TVDlessThan0001Count
                allchangeInMDFinal.extend(cMD)   
        allRegionsChangeInMDsum=0
        for i in allchangeInMDFinal: 
            if i != 'x': allRegionsChangeInMDsum += i

        MDfor0001=MDatTargetDepth-allRegionsChangeInMDsum
        changeInMDforEach0001= MDfor0001/TVDlessThan0001Counts

        MD= [0., changeInMDforEach0001 if allchangeInMDFinal[0] == 'x' else allchangeInMDFinal[0]]
        for c in allchangeInMDFinal[1:]: MD= np.append(MD, [MD[-1] + (changeInMDforEach0001 if c == 'x' else c)])
        
        output= this.case1_MD_INC_AZ(MD, INC, AZ, GridEastX, GridNorthY, MSL) 
        return output        

    def case1_MD_INC_AZ(this, MD, INC, AZ, GridEastX, GridNorthY, MSL):
        changeInINC= INC[1:]-INC[:-1]
        changeInAZ= AZ[1:]-AZ[:-1]
        curveLen= MD[1:]-MD[:-1]
        buildRate= (changeInINC*100)/curveLen
        turnRate= (changeInAZ*100)/curveLen

        out= {'MD':[MD[0]], 'Inclination':[INC[0]], 'Azimuth':[AZ[0]], 'TVD':[0], 'TVDSS':[-MSL], 'East':[0], 'North':[0], 'GridEast':[GridEastX[0]], 'GridNorth':[GridNorthY[0]], 'DLS':[0], 'BuildRate':[0], 'TurnRate':[0]}
        MD= MD[1:] #0th is always zero 
        INC= INC[1:] #0th is always zero
        AZ= AZ[1:] #0th is always zero
        interval= 1
        change= 5
        finalOutput, finalTVD_i, x_easting_i, y_northing_i= [], None, None, None
        finalOutput.append(out)    
        for i in range(len(MD)-2):
            start= MD[i]  
            end= MD[i+interval]
            if end - start < change: change= abs(end - start)
            #print "end:", end, "start:", start, "change:", change
            out, finalTVD_i, x_easting_i, y_northing_i= this.getChangeInMD(change, start, end, buildRate[i+1], turnRate[i+1], INC[i], AZ[i], GridEastX[i], GridNorthY[i], finalTVD_i, x_easting_i, y_northing_i, MSL)
            finalOutput.append(out)
        return finalOutput

    def insertUpdateWML(this, out, workover):
        this.coll.remove({'Property': 'WellTrajectoryOutput'})
        c= 0
        for d in out[workover]:
            for i in range(len(d['MD'])): 
                try: 
                    #q = {'Property': 'WellTrajectoryOutput', 'MD': d['MD'][i], 'Inclination': d['Inclination'][i], 'Azimuth': d['Azimuth'][i], 'TVD': d['TVD'][i],}
                    v = {
                           'Property': 'WellTrajectoryOutput', 
                           'WorkOver': workover, 
                           "MD": d['MD'][i],
                           "Inclination": d['Inclination'][i],
                           "Azimuth": d['Azimuth'][i],
                           "TVD": d['TVD'][i],
                           "TVDSS": d['TVDSS'][i],
                           "East": d['East'][i],
                           "North": d['North'][i],
                           "GridEast": d['GridEast'][i],
                           "GridNorth": d['GridNorth'][i],
                           "DLS": d['DLS'][i],
                           "BuildRate": d['BuildRate'][i],
                           "TurnRate": d['TurnRate'][i],
                           "UpdatedAt": this.dateYmdHms,
                           "UpdatedBy": 'WellTrajectory'
                        }
                    r= this.coll.insert(v)
                    c += 1
                except Exception as e:
                    this.logger.error("WorkOver:%s, MD:%s, Error:%s" %(workover, d['MD'][i], e.message))
                    sys.exit(1)
        this.logger.info("%s records saved in collection %s, 'Property':'WellTrajectoryOutput', 'WorkOver':'%s'" %(c, this.args.wellno, workover))
        print ("%s records saved in collection %s, 'Property':'WellTrajectoryOutput', 'WorkOver':'%s'" %(c, this.args.wellno, workover))

    def getWorkOversFromWML(this):
        wo= [d for d in this.coll.distinct('WorkOver', {'Property':'WellTrajectoryInput'})]
        return wo

    def getWellTrajectoryWorkOverData(this, workover):
        this.df_general_data= pd.DataFrame(list(this.coll.find({'Property': 'general-data'}))).set_index('param').fillna(np.nan)
        this.df_reference_depth= pd.DataFrame(list(this.coll.find({'Property': 'reference-depth'}))).set_index('param').fillna(np.nan)

        MSL= this.df_reference_depth['value']['msl']
        if this.isBlank([MSL], 0):
            this.logger.error("Mean Sea Level is missing in MongoDB:WML:reference-depth:pram:msl")
            sys.exit(1)  
        MDatTargetDepth= this.df_reference_depth['value']['target_depth']

        Md, Inclination, Azimuth, TVD, GridEastX, GridNorthY, EastDX, NorthDY= [], [], [], [], [], [], [], []
        wellhead_cord_e= this.df_general_data['value']['wellhead_cord_e']
        wellhead_cord_n= this.df_general_data['value']['wellhead_cord_n']
        this.well_profile= this.df_general_data['value']['well_profile'].lower()

        #for d in this.coll.find({ 'Property': { '$eq': 'WellTrajectoryInput' }}):
        cnt=0
        for d in this.coll.find({ 'Property':'WellTrajectoryInput', 'WorkOver':workover}):
            Md.append(d['MD'])
            Inclination.append(d['Inclination'])
            Azimuth.append(d['Azimuth'])
            TVD.append(d['TVD'])
            if cnt == 0 and not this.isBlank([wellhead_cord_e], 0) and wellhead_cord_e != d['GridEast']:
                this.logger.error("general-data.wellhead_cord_e(%s) != GridEast[0](%s)" %(wellhead_cord_e, d['GridEast']))
                sys.exit(1)
            if cnt == 0 and not this.isBlank([wellhead_cord_n], 0) and wellhead_cord_n != d['GridNorth']:
                this.logger.error("general-data.wellhead_cord_n(%s) != GridNorth[0](%s)" %(wellhead_cord_n, d['GridNorth']))
                sys.exit(1)
            GridEastX.append(0.00  if this.isBlank([wellhead_cord_e], 0) else d['GridEast']) #assume 0.00 by default, -99999999.0 is set by ETL for blank float column types
            GridNorthY.append(0.00 if this.isBlank([wellhead_cord_e], 0) else d['GridNorth']) #assume 0.00 by default, -99999999.0 is set by ETL for blank float column types
            EastDX.append(d['East'])
            NorthDY.append(d['North'])
            cnt += 1
        return np.array(Md), np.array(Inclination), np.array(Azimuth), np.array(TVD), np.array(GridEastX), np.array(GridNorthY), MSL, MDatTargetDepth, np.array(EastDX), np.array(NorthDY)
    
    def exportWMLinCSV(this):
        host= getMongoHost()
        cmd= 'mongoexport -h %s -d WML -c %s --type=csv --fields WorkOver,MD,Inclination,Azimuth,TVD,TVDSS,East,North,GridEast,GridNorth,DLS,BuildRate,TurnRate  -q \'{"Property":{"$eq":"WellTrajectoryOutput"}}\' --sort "{WorkOver:1, MD:1}"  --out output/report_%s.csv' %(host, this.wellno, case)
        returncode, stdout, stderr= execShell(cmd)
        if returncode != 0:
            this.logger.error(stderr, stdout)
        else:
            this.logger.info("report_%s.csv generated" %(case))

    def isBlank(this, col, start=1, zeroIsBlank=False):
        blanks= ['', None, -99999999.0]
        r=None
        if zeroIsBlank:
            blanks= blanks[:]
            blanks.append(0.0)
            r=any(x in blanks or math.isnan(x) for x in col) #checking all values in col for GridEastX, and GridNorthY
        else:
            r=any(x in blanks or math.isnan(x) for x in col[start:5])
        return r

    def getCase(this, workover, Md, Inclination, Azimuth, TVD, GridEastX, GridNorthY, MDatTargetDepth):
        case= None
        if this.well_profile.lower() == 'vertical':
            this.logger.info("Well:%s, WorkOver:%s, Matched case 5: well_profile:vertical" %(this.wellno, workover))
            case=5
        elif (not this.isBlank(Md)) and (not this.isBlank(Inclination)) and (not this.isBlank(Azimuth)):
            this.logger.info("Well:%s, WorkOver:%s, Matched case 1: Md, Inclination and Azimuth provided by the user" %(this.wellno, workover))
            case=1
        elif(not this.isBlank(TVD)) and (not this.isBlank(Inclination)) and (not this.isBlank(Azimuth)) and (not this.isBlank([MDatTargetDepth], 0)):
            this.logger.info("Well:%s, WorkOver:%s, Matched case 2: TVD, Inclination, Azimuth and MDatTargetDepth provided by the user" %(this.wellno, workover))
            case=2
        elif(not this.isBlank(Md)) and (not this.isBlank(TVD)) and (not this.isBlank(Azimuth)):
            this.logger.info("Well:%s, WorkOver:%s, Matched case 3: Md, TVD and Azimuth provided by the user" %(this.wellno, workover))
            case=3
        elif(not this.isBlank(TVD)) and (not this.isBlank(GridEastX, 0, True)) and (not this.isBlank(GridNorthY, 0, True)):
            this.logger.info("Well:%s, WorkOver:%s, Matched case 4: TVD, GridEastX and GridNorthY provided by the user" %(this.wellno, workover))
            case=4
        else:
            this.logger.error("We do not have required data for computation!")
        this.logger.info("Case selected:%d" %(case))
        return case

    def pushExcelInputinMongo(this, df, property):
        r= this.coll.remove({'Property':property})
        seq_no= 1
        for index, row in df.iterrows():
            d=row.to_dict()
            d['Property']= property
            d['CreatedAt']= this.dateYmdHms
            this.coll.insert(d)
            seq_no += 1

    def trimDfHeaders(this, df):
        renameCol = {}
        for col in df.columns:
            c = col.strip()
            renameCol[col] = c 
        df.rename(columns= renameCol, inplace=True)
        return df

    def case5_well_profile_vertical(this, MD, INC, AZ, TVD, GridEastX, GridNorthY, MSL, MDatTargetDepth, EastDX, NorthDY):
        if (not this.isBlank(MD) and not this.isBlank(TVD)):
            for i in range(len(MD)):
                if abs(MD[i]-TVD[i]) > 10**-4:
                    this.logger.error("There should not be any difference in MD and TVD when well_profile=vertical is selected")
                    sys.exit(1)
            MD= np.copy(TVD)
        elif(not this.isBlank(MD)):
            TVD= np.copy(MD)
        elif(not this.isBlank(TVD)):
            MD= np.copy(TVD)
        else:
            this.logger.error("well_profile=vertical is selected, please provide MD or TVD!")
            sys.exit(1)

        #out= {'MD':[MD[0]], 'Inclination':[INC[0]], 'Azimuth':[AZ[0]], 'TVD':[0], 'TVDSS':[-MSL], 'East':[0], 'North':[0], 'GridEast':[GridEastX[0]], 'GridNorth':[GridNorthY[0]], 'DLS':[0], 'BuildRate':[0], 'TurnRate':[0]}
        finalOutput=[]
        #finalOutput.append(out)
        for i in range(len(MD)-2):
            out= {'MD':[MD[i]], 'Inclination':[0], 'Azimuth':[0], 'TVD':[TVD[i]], 'TVDSS':[TVD[i]-MSL], 'East':[EastDX[i]], 'North':[NorthDY[i]], 'GridEast':[GridEastX[0]], 'GridNorth':[GridNorthY[0]], 'DLS':[0], 'BuildRate':[0], 'TurnRate':[0]}
            finalOutput.append(out)
        return finalOutput


    def readExcel(this):
        xls = pd.ExcelFile(this.args.excel)
        this.pushExcelInputinMongo(this.trimDfHeaders(pd.read_excel(xls, sheet_name='general-data')), 'general-data')
        this.logger.info('Populated general-data')
        this.pushExcelInputinMongo(this.trimDfHeaders(pd.read_excel(xls, sheet_name='reference-depth')), 'reference-depth')
        this.logger.info('Populated reference-depth')
        returncode, out, err= execShell('python %s/../ETL/WellTrajectoryInput.py --fileslist %s --service WML --cleanup --collection %s' %(cwd, this.args.excel, this.args.wellno))
        if returncode != 0:
            this.logger.error('STDOUT:%s, STDERROR:%s' %(out, err))
            sys.exit(1)
        else:
            this.logger.info('Populated Well Trajectory data:\n%s' %(out))

        #print this.df_fluid_types.fluid_type.bo

    def main(this):
        #Md= [0.00,110.00,201.00,290.00,384.00,476.00,540.00,663.00,754.00,845.00,937.00,1030.00,1125.00,1220.00,1315.00,1410.00,1504.00,1599.00,1695.00,1790.00,1884.00,1979.00,2074.00,2169.00,2264.00,2359.00,2454.00,2549.00,2644.00,2739.00,2834.00,2928.00,3023.00,3118.00,3213.00,3308.00,3404.00,3499.00,3594.00,3690.00,3785.00,3880.00,3975.00,4070.00,4165.00,4260.00,4364.00,4429.00,4461.00,4493.00,4524.00,4556.00,4588.00,4619.00,4715.00,4810.00,4903.00,4999.00,5093.00,5189.00,5284.00,5379.00,5474.00,5569.00,5664.00,5759.00,5854.00,5949.00,6044.00,6140.00,6235.00,6330.00,6425.00,6520.00,6560.00,6645.00,6677.00,6773.00,6868.00,6962.00,7057.00,7153.00,7248.00,7343.00,7438.00,7516.00,7604.00,7699.00,7772.75,7805.00,7900.00,7995.00,8001.85,8089.00,8185.00,8279.00,8375.00,8470.00,8565.00,8660.00,8755.00,8850.00,8945.00,9030.70,9040.00,9133.00,9228.00,9292.00,9331.00]
        #Inclination= [0.00,0.36,0.52,0.52,0.51,0.67,0.79,0.79,0.80,0.81,0.85,0.88,0.87,0.77,0.65,0.59,0.53,0.49,0.48,0.42,0.35,0.31,0.26,0.23,0.29,0.34,0.39,0.41,0.33,0.25,0.28,0.36,0.35,0.36,0.42,0.49,0.49,0.72,0.93,1.11,1.07,1.02,1.07,1.03,1.05,0.96,0.91,0.72,0.53,1.05,1.64,2.62,4.39,5.28,7.85,10.04,13.93,18.39,21.91,25.98,29.26,32.02,36.36,40.30,43.39,46.17,48.25,50.78,53.53,57.23,62.13,64.27,67.00,69.01,69.63,70.01,70.39,70.71,71.62,73.33,76.21,77.56,77.36,80.50,81.06,81.08,79.45,79.51,79.82,79.95,86.11,89.20,89.25,89.88,90.25,91.67,93.71,93.64,92.16,91.61,91.73,91.17,90.86,91.47,91.54,91.30,90.86,90.93,91.00]
        #Azimuth= [0.00,299.04,294.64,293.99,292.90,296.04,298.54,300.26,302.22,304.29,301.97,302.15,305.54,306.31,306.12,304.47,303.39,300.54,297.93,291.99,279.00,265.06,250.91,225.47,231.26,173.97,185.53,173.56,162.90,134.76,106.30,85.84,68.30,52.71,39.61,22.26,7.32,350.47,347.71,340.83,343.52,338.72,340.22,344.75,350.13,355.84,2.44,7.45,58.57,117.64,128.62,139.02,142.53,149.74,151.81,147.01,146.83,151.18,159.07,161.51,162.53,163.38,163.37,163.21,162.93,162.38,162.46,161.89,161.56,161.01,161.17,161.06,162.11,162.25,162.25,162.46,162.55,162.85,164.48,164.22,165.58,165.68,166.28,163.87,162.76,162.39,160.70,160.80,160.15,159.87,160.75,163.69,163.85,165.84,168.07,168.67,168.14,167.02,166.39,166.33,166.01,166.06,166.41,166.99,167.05,167.13,167.12,166.98,166.80]
        #GridEastX= [746920.47300,746920.38,746920.19,746919.97,746919.73,746919.47,746919.25,746918.80,746918.47,746918.14,746917.80,746917.44,746917.07,746916.74,746916.45,746916.19,746915.96,746915.74,746915.52,746915.32,746915.14,746914.97,746914.83,746914.73,746914.63,746914.58,746914.58,746914.58,746914.62,746914.69,746914.80,746914.96,746915.13,746915.28,746915.42,746915.54,746915.60,746915.59,746915.51,746915.36,746915.20,746915.02,746914.84,746914.68,746914.57,746914.50,746914.49,746914.52,746914.56,746914.68,746914.86,746915.12,746915.49,746915.93,746917.55,746919.86,746923.08,746927.23,746931.32,746935.31,746939.44,746943.76,746948.42,746953.58,746959.21,746965.29,746971.71,746978.45,746985.62,746993.35,747001.44,747009.81,747018.14,747026.35,747029.83,747037.20,747039.96,747048.17,747055.87,747063.24,747070.52,747077.59,747084.44,747091.76,747099.97,747107.00,747115.37,747124.75,747132.14,747135.45,747145.13,747153.96,747154.54,747161.49,747168.09,747173.87,747179.74,747185.96,747192.61,747199.43,747206.35,747213.34,747220.22,747226.23,747226.87,747233.20,747239.65,747244.02,747246.72] #UTMX
        #GridNorthY= [3215614.54500,3215614.60,3215614.69,3215614.79,3215614.90,3215615.02,3215615.13,3215615.38,3215615.58,3215615.80,3215616.02,3215616.25,3215616.49,3215616.74,3215616.95,3215617.13,3215617.28,3215617.42,3215617.54,3215617.64,3215617.69,3215617.70,3215617.67,3215617.61,3215617.52,3215617.39,3215617.21,3215617.01,3215616.82,3215616.70,3215616.63,3215616.62,3215616.66,3215616.75,3215616.89,3215617.08,3215617.32,3215617.62,3215618.03,3215618.53,3215619.06,3215619.56,3215620.05,3215620.56,3215621.07,3215621.57,3215622.09,3215622.37,3215622.45,3215622.44,3215622.31,3215622.06,3215621.59,3215620.93,3215618.00,3215614.14,3215609.21,3215602.22,3215593.26,3215582.08,3215569.31,3215555.20,3215539.61,3215522.42,3215503.94,3215484.47,3215464.22,3215443.25,3215421.54,3215398.74,3215375.10,3215350.65,3215325.63,3215300.07,3215289.20,3215266.03,3215257.28,3215230.94,3215204.64,3215178.33,3215151.36,3215123.75,3215096.33,3215068.88,3215041.50,3215019.09,3214994.02,3214967.14,3214946.30,3214937.20,3214910.15,3214882.61,3214880.61,3214854.97,3214826.46,3214798.41,3214769.78,3214741.56,3214713.41,3214685.29,3214657.18,3214629.09,3214600.97,3214575.56,3214572.80,3214545.18,3214516.95,3214497.94,3214486.37] #UTMY
        #this.case1_MD_INC_AZ(np.array(Md), np.array(Inclination), np.array(Azimuth), np.array(GridEastX), np.array(GridNorthY))
        #first value of MD, INC and AZ are always zero

        module= 'WellTrajectory'
        if this.args.excel is not None: this.readExcel()
        workovers= this.getWorkOversFromWML()
        output= {}
        for workover in workovers:
            Md, Inclination, Azimuth, TVD, GridEastX, GridNorthY, MSL, MDatTargetDepth, EastDX, NorthDY= this.getWellTrajectoryWorkOverData(workover)
            case= this.getCase(workover, Md, Inclination, Azimuth, TVD, GridEastX, GridNorthY, MDatTargetDepth)
            if case == 1:
                output[workover]= this.case1_MD_INC_AZ(Md, Inclination, Azimuth, GridEastX, GridNorthY, MSL)
            elif case == 2:
                output[workover]= this.case2_TVD_INC_AZ(TVD, Inclination, Azimuth, GridEastX, GridNorthY, MSL, MDatTargetDepth)
            elif case == 3:
                output[workover]= this.case3_MD_TVD_AZ(Md, TVD, Azimuth, GridEastX, GridNorthY, MSL)
            elif case == 4:
                output[workover]= this.case4_TVD_GridEastX_GridNorthY(TVD, GridEastX, GridNorthY, MSL)
            elif case == 5:
                output[workover]= this.case5_well_profile_vertical(Md, Inclination, Azimuth, TVD, GridEastX, GridNorthY, MSL, MDatTargetDepth, EastDX, NorthDY)
            else:
                this.logger.error("Valid case of mandatory columns not found in input WellTrajectory - WorkOver:%s data supplied by user!" %(workover))
                sys.exit(1)
            this.insertUpdateWML(output, workover)
            output['original']= [{'TVD':TVD, 'GridEast':GridEastX, 'GridNorth':GridNorthY}]

        if this.args.printoutput:
            grapgObj= WMLGraphs(this.service, this.wellno, this.logger)
            grapgObj.plot_WellTrajectory_MD_INC_AZ(output, 'WML', this.wellno, module, 'WellTrajectory_MD_INC_AZ.png', 'WellTrajectory_MD_INC_AZ')
            this.exportWMLinCSV()

if __name__ == "__main__":
        service= 'WML'
        logger= getLogger('WellTrajectory', 'Well1')
        wellno= None
        ob= WellTrajectory(service, wellno, logger)
        ob.main()

#import data from excel:
#python ../ETL/WellTrajectoryInput.py   --fileslist ~/Downloads/welltrajectorydata5wells/Well-D.xlsx  --service WML --cleanup --wellno WellD
#computation:
#python  welltrajectory.py -w WellA -p -e inputs/WellA.xlsx
