import psutil

from Scripts.esp.core.get_logger import get_logger
from Scripts.esp.core.storage.fetch_most import get_sensor_status
from Scripts.esp.core.storage.mongo import getDbCon, update

logger = get_logger("sensor stop")

sensors_conn = getDbCon("Central", "sensors")


def update_sensor_status(well_name, gauge_name, sensor_name):
    conn = getDbCon("Central", "sensors")
    where = dict()
    where["well_name"] = well_name
    where["gauge_name"] = gauge_name
    where["sensor_name"] = sensor_name
    update_set = {"read_status": "inactive", "pid": None}
    update_set = {'$set': update_set}
    update(conn, where, update_set, upsert={'upsert': False})


def stop_sensor(well_name, gauge_name, sensor_name):
    conn = getDbCon("Central", "sensors")
    # get sensor metadata to find out if its already running
    status = get_sensor_status(conn, well_name, gauge_name, sensor_name)
    if status["read_status"] == "active" and status["pid"] is not None:
        try:
            kill_tree(status["pid"])
        except Exception as e:
            logger.exception(e)
            logger.debug("Failed to kill process. Still setting the status to not running.")
        update_sensor_status(well_name, gauge_name, sensor_name)
        logger.debug("Sensor read stopped")
    else:
        logger.debug("Cannot stop. sensor reading not active.")


def kill_tree(pid, including_parent=True):
    parent = psutil.Process(pid)
    for child in parent.children(recursive=True):
        child.kill()
    if including_parent:
        parent.kill()


if __name__ == "__main__":
    stop_sensor("infinity", "ESP Sensor_Module", "motor_current")
