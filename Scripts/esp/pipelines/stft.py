import time

import pandas as pd

from Scripts.esp.core.get_logger import get_logger
from Scripts.esp.core.storage.mongo import fetch, insert
from Scripts.esp.modules.stft.stft import stft
from Scripts.esp.pipelines.read_sensors import BaseReader

logger = get_logger("stft")


class STFT(BaseReader):
    def __init__(self, db, well, gauge_name, sensor_name):
        super(STFT, self).__init__(db, well, gauge_name, sensor_name, "last_stft_time", "internal")
        self.signal = []
        self.input = "denoise_signal"
        self.output = "stft_signal"
        self.pipeline_size = 1
        self.mute = True

    def read_sensor(self):
        logger.debug("Started process to run STFT for Well = %s, Gauge = %s, Sensor = %s" % (
            self.well, self.gauge_name, self.sensor_name))
        while True:
            try:
                if self.read_next():
                    self.queue_signal()
                    self.sleep()
            except (KeyboardInterrupt, Exception) as e:
                logger.debug("Something went wrong in stft! %s" % e)
                continue

    def read_next_batch(self):
        where = [{'property': {'$eq': self.input}}, {'gauge_name': {'$eq': self.gauge_name}},
                 {'sensor_name': {'$eq': self.sensor_name}}]
        where += [{"created_at": {"$gt": self.last_read_time}}]
        where = {'$and': where}
        columns = {"output_signal": 1, "created_at": 1, "x_axis": 1}
        batch = fetch(self.well_conn, where, columns=columns)
        if len(batch) > 0 and not self.mute:
            logger.debug("got de-noised signal of length = %s" % len(batch))
        data = pd.DataFrame(batch)
        return data.to_dict('records')

    def queue_signal(self):
        signal = self.read_next_batch()
        if len(signal) > 0:
            signal_df = self.create_signal_frame(signal)
            if signal_df.shape[0] >= self.pipeline_size:
                self.run_stft(signal_df)

    def create_signal_frame(self, signal):
        df = pd.DataFrame(signal)
        df = df.sort_values(by=["created_at"], ascending=True).reset_index(drop=True)
        df = df[df.index < self.pipeline_size]
        return df

    def run_stft(self, df):
        if not self.mute:
            logger.debug("STFT signal.")
        self.last_read_time = df.created_at.max()
        x_axis = df.x_axis.values[0]
        y_axis = df.output_signal.values[0]
        df = pd.DataFrame(data=list(zip(x_axis, y_axis)), columns=["x_axis", "y_axis"])
        stft_signal = stft(df, self.sleep_time)
        stft_signal["min_x_axis"] = df.x_axis.min()
        stft_signal["max_x_axis"] = df.x_axis.max()
        self.push_signal(stft_signal)
        self.update_sensor_status("last_stft_time")

    def push_signal(self, data):
        data["gauge_name"] = self.gauge_name
        data["sensor_name"] = self.sensor_name
        data["property"] = self.output
        data["created_at"] = time.time()
        insert(self.well_conn, data)


if __name__ == "__main__":
    obj = STFT("RTS", "R169", "ESP Sensor_Module", "motor_current")
    obj.read_sensor()
