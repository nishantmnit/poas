import time

import pandas as pd

from Scripts.esp.core.get_logger import get_logger
from Scripts.esp.core.storage.mongo import fetch, insert
from Scripts.esp.modules.qaqc import qa_qc
from Scripts.esp.pipelines.read_sensors import BaseReader

logger = get_logger("qa qc")


class QA_QC(BaseReader):
    def __init__(self, db, well, gauge_name, sensor_name):
        super(QA_QC, self).__init__(db, well, gauge_name, sensor_name, "last_qa_qc_time", "internal")
        self.signal = []
        self.input = "raw_input_signal"
        self.output = "qa_qc_signal"
        self.pipeline_size = 5 if self.sleep_time <= 60 else 12
        self.previous_read_time = self.last_read_time
        self.mute = True

    def read_sensor(self):
        logger.debug("Started process to perform QA QC for Well = %s, Gauge = %s, Sensor = %s" % (
            self.well, self.gauge_name, self.sensor_name))
        while True:
            try:
                if self.read_next():
                    self.queue_signal()
                    self.sleep()
            except (KeyboardInterrupt, Exception) as e:
                logger.debug("Something went wrong in qa qc! %s" % e)
                continue

    def read_next_batch(self):
        where = [{'property': {'$eq': self.input}}, {'gauge_name': {'$eq': self.gauge_name}},
                 {'sensor_name': {'$eq': self.sensor_name}}]
        where += [{"created_at": {"$gt": self.last_read_time}}]
        where = {'$and': where}
        columns = {"x_axis": 1, "y_axis": 1, "_id": 0, "created_at": 1}
        batch = fetch(self.well_conn, where, columns=columns)
        if len(batch) > 0 and not self.mute:
            logger.debug("got raw signal of length = %s" % len(batch))
        data = pd.DataFrame(batch)
        if data.shape[0] > 0:
            self.last_read_time = data.created_at.max()
        return data.to_dict('records')

    def queue_signal(self):
        signal = self.read_next_batch()
        self.signal = self.signal + signal
        if len(self.signal) > 0:
            signal_df = self.create_signal_frame()
            if signal_df.shape[0] >= self.pipeline_size:
                self.signal = self.run_qa_qc(signal_df)
            else:
                self.signal = signal_df[["x_axis", "y_axis", "created_at"]].to_dict('records')

    def create_signal_frame(self):
        df = pd.DataFrame(self.signal)
        df = df[pd.to_numeric(df['y_axis'], errors='coerce').notnull()].reset_index(drop=True)
        df = df.sort_values(by=["x_axis"], ascending=True).reset_index(drop=True)
        df["frequency"] = self.sleep_time if self.sleep_time <= 60 else 60
        df["received_after"] = df.x_axis - df.x_axis.shift(1)
        df = df.loc[df.received_after != 0].reset_index(drop=True)
        return df

    def run_qa_qc(self, df):
        if not self.mute:
            logger.debug("Performing QA QC.")
        df = qa_qc(df)
        df["created_at"].fillna(self.last_read_time, inplace=True)
        save_signal = df.loc[
            df.created_at > self.previous_read_time, ["x_axis", "y_axis", "interpolated", "bad_signal"]].to_dict(
            'records')
        for data_dict in save_signal:
            self.push_signal(data_dict)

        self.update_sensor_status("last_qa_qc_time")

        next_signal = df[["x_axis", "y_axis", "created_at"]].to_dict('records')
        if len(next_signal) >= self.pipeline_size:
            next_signal = next_signal[-(self.pipeline_size-1):]

        self.previous_read_time = self.last_read_time
        return next_signal

    def push_signal(self, data):
        data["gauge_name"] = self.gauge_name
        data["sensor_name"] = self.sensor_name
        data["property"] = self.output
        data["created_at"] = time.time()
        insert(self.well_conn, data)
