import datetime
import json
import os
import time

import Scripts.esp.settings as settings
from Scripts.esp.core.storage.fetch_most import get_sensor_metadata
from Scripts.esp.core.storage.mongo import getDbCon, insert, update
from Scripts.esp.core.get_logger import get_logger

# from threading import Thread

logger = get_logger("read sensor data")


def get_sleep_time(frequency, frequency_unit):
    frequency = float(frequency)
    if frequency_unit.lower() == "hour":
        sleep_time = 60 * 60 * frequency
    elif frequency_unit.lower() == "minute":
        sleep_time = 60 * frequency
    elif frequency_unit.lower() == "day":
        sleep_time = 24 * 60 * 60 * frequency
    else:
        sleep_time = frequency
    return sleep_time


class BaseReader(object):
    def __init__(self, db, well, gauge_name, sensor_name, what="last_read_time", method="internal"):
        # super(BaseReader, self).__init__()
        self.method = method
        self.db, self.well = db, well
        self.well_conn = getDbCon(self.db, self.well)
        self.central_conn = getDbCon("Central")
        self.gauge_name, self.sensor_name = gauge_name, sensor_name
        self.sensor = get_sensor_metadata(self.central_conn["sensors"], self.well, self.gauge_name, self.sensor_name)
        self.sleep_time = get_sleep_time(self.sensor.data_sampling_frequency, self.sensor.data_sampling_frequency_unit)
        self.last_read_time = self.sensor[what] if what in self.sensor else 0
        self.input = ""
        self.output = "raw_input_signal"
        self.mute = True

    def read_sensor(self):
        raise NotImplementedError("This is base read method. This should be extended and implemented for the sensor "
                                  "read method")

    def push_signal(self, data):
        data["x_axis_input"] = data.pop(self.sensor.x_axis)
        data["x_axis"] = time.mktime(
            datetime.datetime.strptime(data["x_axis_input"], self.sensor.datetime_format).timetuple())
        data["y_axis"] = data.pop(self.sensor.y_axis)
        data["gauge_name"] = self.gauge_name
        data["sensor_name"] = self.sensor_name
        data["property"] = self.output
        data["created_at"] = time.time()
        if not self.mute:
            logger.debug("saving raw signal = %s" % data)
        insert(self.well_conn, data)
        self.update_sensor_status()

    def read_next(self):
        next_read_time = self.last_read_time + self.sleep_time
        timestamp_now = time.time()
        if timestamp_now >= next_read_time:
            return True
        return False

    def sleep(self, pipeline_size=1):
        next_read_time = self.last_read_time + self.sleep_time * pipeline_size
        timestamp_now = time.time()
        sleep_time = 0 if next_read_time - timestamp_now < 0 else next_read_time - timestamp_now
        time.sleep(sleep_time)

    def update_sensor_status(self, what="last_read_time"):
        where = dict()
        where["well_name"] = self.well
        where["gauge_name"] = self.gauge_name
        where["sensor_name"] = self.sensor_name
        update_set = {what: self.last_read_time}
        update_set = {'$set': update_set}
        update(self.central_conn["sensors"], where, update_set, upsert={'upsert': False})


class SensorReader(BaseReader):
    def read_sensor(self):
        if self.method.lower() == "internal":
            obj = InternalReader(self.db, self.well, self.gauge_name, self.sensor_name, self.method)
            obj.read_sensor()
        else:
            raise NotImplementedError("This only supports internal sensor reads only at the moment.")


class InternalReader(BaseReader):
    def read_sensor(self):
        sensor_location = os.path.join(settings.ESP_DIR, "playground", "sensors", self.well, self.gauge_name,
                                       "%s.json" % self.sensor_name)
        logger.debug("Started process for reading sensor for Well = %s, Gauge = %s, Sensor = %s" % (
            self.well, self.gauge_name, self.sensor_name))
        while True:
            try:
                if self.read_next():
                    self.last_read_time = time.time()
                    with open(sensor_location, "r") as fh:
                        rows = json.load(fh)
                        if isinstance(rows, list):
                            for row in rows:
                                self.push_signal(row)
                        else:
                            self.push_signal(rows)
                    self.sleep()
            except (KeyboardInterrupt, Exception) as e:
                logger.debug("Something went wrong in read sensor! %s" % e)
                continue


if __name__ == "__main__":
    test_obj = SensorReader("RTS", "infinity", "ESP Sensor_Module", "motor_current", "internal")
    test_obj.read_sensor()
