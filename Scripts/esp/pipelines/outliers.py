import time

import pandas as pd

from Scripts.esp.core.get_logger import get_logger
from Scripts.esp.core.storage.mongo import fetch, insert
from Scripts.esp.modules.outliers import fix_outlier
from Scripts.esp.pipelines.read_sensors import BaseReader

logger = get_logger("Fix Outliers")


class Outliers(BaseReader):
    def __init__(self, db, well, gauge_name, sensor_name):
        super(Outliers, self).__init__(db, well, gauge_name, sensor_name, "last_outlier_time", "internal")
        self.signal = []
        self.input = "qa_qc_signal"
        self.output = "outlier_signal"
        self.pipeline_size = 5
        self.previous_read_time = self.last_read_time
        self.mute = True

    def read_sensor(self):
        logger.debug("Started process to remove outliers for Well = %s, Gauge = %s, Sensor = %s" % (
            self.well, self.gauge_name, self.sensor_name))
        while True:
            try:
                if self.read_next():
                    self.queue_signal()
                    self.sleep()
            except (KeyboardInterrupt, Exception) as e:
                logger.debug("Something went wrong in outlier removal! %s" % e)
                continue

    def read_next_batch(self):
        where = [{'property': {'$eq': self.input}}, {'gauge_name': {'$eq': self.gauge_name}},
                 {'sensor_name': {'$eq': self.sensor_name}}]
        where += [{"created_at": {"$gt": self.last_read_time}}]
        where = {'$and': where}
        columns = {"x_axis": 1, "y_axis": 1, "_id": 0, "created_at": 1}
        batch = fetch(self.well_conn, where, columns=columns)
        if len(batch) > 0 and not self.mute:
            logger.debug("got qa qc signal of length = %s" % len(batch))
        data = pd.DataFrame(batch)
        return data.to_dict('records')

    def queue_signal(self):
        signal = self.read_next_batch()
        self.signal = self.signal + signal
        if len(self.signal) > 0:
            signal_df = self.create_signal_frame()
            if signal_df.shape[0] >= self.pipeline_size:
                self.signal = self.run_outlier_removal(signal_df)
            else:
                self.signal = signal_df[["x_axis", "y_axis", "created_at"]].to_dict('records')

    def create_signal_frame(self):
        df = pd.DataFrame(self.signal)
        df = df.sort_values(by=["x_axis"], ascending=True).reset_index(drop=True)
        df = df[df.index < self.pipeline_size]
        self.last_read_time = df.created_at.max()
        return df

    def run_outlier_removal(self, df):
        if not self.mute:
            logger.debug("Identifying and fixing outliers.")
        df.loc[df.index == 2, "y_axis"] = fix_outlier(df)
        save_signal = df.loc[
            df.created_at > self.previous_read_time, ["x_axis", "y_axis"]].to_dict('records')

        for data_dict in save_signal:
            self.push_signal(data_dict)

        self.update_sensor_status("last_outlier_time")

        next_signal = df[["x_axis", "y_axis", "created_at"]].to_dict('records')
        if len(next_signal) >= self.pipeline_size:
            next_signal = next_signal[-(self.pipeline_size-1):]

        self.previous_read_time = self.last_read_time
        return next_signal

    def push_signal(self, data):
        data["gauge_name"] = self.gauge_name
        data["sensor_name"] = self.sensor_name
        data["property"] = self.output
        data["created_at"] = time.time()
        insert(self.well_conn, data)


if __name__ == "__main__":
    obj = Outliers("RTS", "R169", "Wellhead Gauge", "wellhead_pressure")
    obj.read_sensor()
