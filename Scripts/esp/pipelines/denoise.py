import time

import pandas as pd

from Scripts.esp.core.get_logger import get_logger
from Scripts.esp.core.storage.mongo import fetch, insert
from Scripts.esp.modules.denoise.denoise import de_noise
from Scripts.esp.pipelines.read_sensors import BaseReader

logger = get_logger("De-noise")


class Denoise(BaseReader):
    def __init__(self, db, well, gauge_name, sensor_name):
        super(Denoise, self).__init__(db, well, gauge_name, sensor_name, "last_denoise_time", "internal")
        self.signal = []
        self.input = "outlier_signal"
        self.output = "denoise_signal"
        self.pipeline_size = 256
        self.mute = True

    def read_sensor(self):
        logger.debug("Started process to de-noise signal for Well = %s, Gauge = %s, Sensor = %s" % (
            self.well, self.gauge_name, self.sensor_name))
        while True:
            try:
                if self.read_next():
                    self.queue_signal()
                    self.sleep()
            except (KeyboardInterrupt, Exception) as e:
                logger.debug("Something went wrong in de-noising! %s" % e)
                continue

    def read_next_batch(self):
        where = [{'property': {'$eq': self.input}}, {'gauge_name': {'$eq': self.gauge_name}},
                 {'sensor_name': {'$eq': self.sensor_name}}]
        where += [{"created_at": {"$gt": self.last_read_time}}]
        where = {'$and': where}
        columns = {"x_axis": 1, "y_axis": 1, "_id": 0, "created_at": 1}
        batch = fetch(self.well_conn, where, columns=columns)
        if len(batch) > 0 and not self.mute:
            logger.debug("got outlier signal of length = %s" % len(batch))
        data = pd.DataFrame(batch)
        return data.to_dict('records')

    def queue_signal(self):
        signal = self.read_next_batch()
        if len(signal) > 0:
            signal_df = self.create_signal_frame(signal)
            if signal_df.shape[0] >= self.pipeline_size:
                self.run_de_noising(signal_df)

    def create_signal_frame(self, signal):
        df = pd.DataFrame(signal)
        df = df.sort_values(by=["x_axis"], ascending=True).reset_index(drop=True)
        df = df[df.index < self.pipeline_size]
        return df

    def run_de_noising(self, df):
        if not self.mute:
            logger.debug("De-noising the signal.")
        self.last_read_time = df.created_at.max()
        de_noised_signal = de_noise(df)
        de_noised_signal["min_x_axis"] = df.x_axis.min()
        de_noised_signal["max_x_axis"] = df.x_axis.max()
        self.push_signal(de_noised_signal)
        self.update_sensor_status("last_denoise_time")

    def push_signal(self, data):
        data["gauge_name"] = self.gauge_name
        data["sensor_name"] = self.sensor_name
        data["property"] = self.output
        data["created_at"] = time.time()
        insert(self.well_conn, data)


if __name__ == "__main__":
    obj = Denoise("RTS", "infinity", "ESP Sensor_Module", "motor_current")
    obj.read_sensor()
