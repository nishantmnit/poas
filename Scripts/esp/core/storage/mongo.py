from Lib.DBLib import getDbCon
from Lib.Utils import getLogger
from Scripts.esp.settings import LOG_DIR
import datetime
import pandas as pd

logger = getLogger("ESP Mongo Storage")


def write_signal(data, db, well, **kwargs):
    conn = get_connection(db, well)
    for key, value in kwargs.items():
        data[key] = value
    for k, v in data.items():
        k = k.encode('ascii', 'ignore')
        v = None if pd.isnull(v) else v
        data[k] = v
    data["Property"] = "Signal"
    data["CreatedAt"] = datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
    data["CreatedBy"] = 'main'
    r = conn.insert(data, check_keys=False)
    logger.debug('ESP_WRITE:{%s, "id": "%s"}' % (kwargs, r))


def write_bulk_signal(df_o, db, well, **kwargs):
    df = df_o.copy(deep=True)
    df["index"] = df.index  # preserve index to be set again in fetch
    df.reset_index(inplace=True, drop=True)  # there can be duplicates in index
    df.index = df.index.map(str)
    df["seq"] = range(0, df.shape[0])
    data = df.to_dict()
    for key, value in kwargs.items():
        data[key] = value
    for k, v in data.items():
        k = k.encode('ascii', 'ignore')
        data[k] = v
    data["Property"] = "Signal"
    data["CreatedAt"] = datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
    data["CreatedBy"] = 'main'
    conn = get_connection(db, well)
    r = conn.insert(data, check_keys=False)
    logger.debug('ESP_WRITE:{%s, "id": "%s"}' % (kwargs, r))


def fetch_bulk_signal(db, well, **kwargs):
    gen = find_all(db, well, **kwargs)[0]
    df = pd.DataFrame(index=gen["index"].values())
    for key in gen.keys():
        if key not in ["CreatedAt", "CreatedBy", "_id", "index"] + kwargs.keys():
            if type(gen[key]) is dict:
                df[key] = gen[key].values()
            else:
                df[key] = gen[key]
    df.sort_values(by=["seq"], inplace=True)
    return df


def find_all(db, well, **kwargs):
    conn = get_connection(db, well)
    data = list()
    for key, value in kwargs.items():
        data.append({key: value})
    gen = conn.find({'$and': data}) if len(data) > 0 else conn.find()
    if gen.count() == 0:
        raise RuntimeError("Found no documents = %s. Please check and re-run." % data)
    return gen


def clear_signal(db, well, **kwargs):
    logger.debug('ESP_DELETE:{%s}' % kwargs)
    conn = get_connection(db, well)
    conn.remove(kwargs)


def fetch(con, where, sort=None, limit=None, count=None, columns=None):
    f = con.find(where, columns)
    if sort is not None:
        f = f.sort(sort)
    if limit is not None:
        f = f.limit(limit)
    if count is not None:
        f = f.count()
        return f
    data = [d for d in f]
    return data


def fetch_cursor(con, where, sort=None, limit=None):
    f = con.find(where)
    if sort is not None:
        f = f.sort(sort)
    if limit is not None:
        f = f.limit(limit)
    return f


def update(con, where, sset, upsert):
    r = con.update(where, sset, **upsert)
    return r


def update_many(con, where, sset, upsert):
    r = con.update_many(where, sset, **upsert)
    return r


def insert_ifnotexist(con, where, sset):
    r = con.update(where, {'$setOnInsert': sset}, **{'upsert': True})
    return r


def insert(con, data_dict):
    r = con.insert(data_dict)
    return r


def delete(con, **kwargs):
    r = con.remove(kwargs)
    return r


def create_index(con, index_col_list, index_name, background=True, uniq=False):
    con.create_index(index_col_list, background=background, name=index_name, unique=uniq)


def get_connection(db, well):
    db_con = getDbCon(db)
    conn = db_con[well]
    return conn
