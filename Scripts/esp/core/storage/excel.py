import os

import pandas as pd


def write_excel(logger, debug, dir, file, sheets, dfs):
    if not debug:
        return True
    ensure_dir(dir)
    filename = get_file(dir, file)
    writer = pd.ExcelWriter(filename, engine="openpyxl")
    for i in range(len(sheets)):
        dfs[i].to_excel(writer, sheet_name=sheets[i])
    writer.save()
    if logger is not None:
        logger.info("PVT_DEBUG: Saved debug excel - %s" % filename)


def get_file(dir, filename):
    filename = [str(i) for i in filename]
    filename = os.path.join(dir, "_".join(filename))
    filename = "%s.xlsx" % filename
    return filename


def ensure_dir(debug_dir):
    if not os.path.isdir(debug_dir):
        os.makedirs(debug_dir)
