import datetime
from copy import deepcopy
import sys
import pandas as pd

import numpy as np
import pymongo
from bson.objectid import ObjectId

from Lib.Utils import getLogger
from Scripts.esp.core.storage.mongo import (
    getDbCon,
    fetch,
    update,
    insert,
    create_index,
    update_many,
    insert_ifnotexist
)
from Scripts.esp.settings import LOG_DIR

logger = getLogger("fetch_most")


# nishant's version
def get_sensor_metadata(conn, well_name, gauge_name, sensor_name):
    where = {"well_name": well_name, "gauge_name": gauge_name, "sensor_name": sensor_name}
    sensor = fetch(conn, where)
    if len(sensor) == 0:
        logger.exception("We could not find any active sensor or well. Please check if you have defined the well "
                         "and/or sensor.")
        sys.exit(-1)
    else:
        sensor = sensor[0]
    sensor = pd.Series(sensor)
    sensor["z_axis"] = sensor.z_axis if not pd.isnull(sensor.z_axis) else "z_axis"
    return pd.Series(sensor)


def get_sensor_status(conn, well_name, gauge_name, sensor_name):
    # get sensor metadata to find out if its already running
    sensor = get_sensor_metadata(conn, well_name, gauge_name, sensor_name)
    if sensor is None:
        logger.exception("We could not find any active sensor or well. Please check if you have defined the well "
                         "and/or sensor.")
        sys.exit(-1)
    status = {"last_read_time": 0, "last_qa_qc_time": 0, "last_outlier_time": 0,
              "last_denoise_time": 0, "last_stft_time": 0, "last_cwt_time": 0, "last_cqt_time": 0}
    for key in status.keys():
        if key in sensor:
            status[key] = sensor[key]

    status["pid"] = sensor.pid if "pid" in sensor else None

    status["read_status"] = sensor.read_status if "read_status" in sensor else "inactive"

    return status


# pre-nishant
def raw_interpolates_data_in(con, signal_type, sensor_id, status=None, limit=None, desc=None):
    ands = [{'Property': {'$eq': 'Signal'}}, {'SensorId': {'$eq': sensor_id}},
            {'%s_SignalQuality' % (signal_type): {'$ne': 'B'}}]
    if status is not None: ands.append({'%s_Status' % (signal_type): {'$in': status}})
    where = {'$and': ands}
    sort = 'DateTime'
    if desc is not None:
        sort = [("DateTime", pymongo.DESCENDING)]
    return fetch(con, where, sort, limit)


def rts_b_sensor_test_data_count(con, sensor_id):
    ands = [{'Property': {'$eq': 'Signal'}}, {'SensorId': {'$eq': sensor_id}}]
    where = {'$and': ands}
    return fetch(con, where, limit=1, count=True)


def rts_b_sensor_test_data(con, sensor_id):
    ands = [{'Property': {'$eq': 'Signal'}}, {'SensorId': {'$eq': sensor_id}}]
    where = {'$and': ands}
    sort = 'DateTime'
    return fetch(con, where, sort)


def raw_interpolates_data_notin(con, signal_type, sensor_id, status=None, limit=None, desc=None):
    ands = [{'Property': {'$eq': 'Signal'}}, {'SensorId': {'$eq': sensor_id}},
            {'%s_QaQcStatus' % (signal_type): {'$eq': 'A'}}, {'%s_SignalQuality' % (signal_type): {'$ne': 'B'}}]
    if status is not None: ands.append({'%s_Status' % (signal_type): {'$nin': status}})
    where = {'$and': ands}
    sort = 'DateTime'
    if desc is not None: sort = [("DateTime", pymongo.DESCENDING)]
    return fetch(con, where, sort, limit)


def raw_interpolates_update(con, _id, data_dict, signalType):
    dateYmdHms = datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
    where = {'_id': _id, '%s_Status' % (signalType): {'$in': ['A', 'B']}}
    sset = rts_m_default_i('raw_interpolates')
    sset.update(data_dict)
    sset = {'$set': sset}
    upsert = {'upsert': False}
    return update(con, where, sset, upsert)


def interpolates_data(con, mongo_status_col, sensor_id, status, limit):
    where = {'$and': [{'Property': {'$eq': 'Signal'}}, {'SensorId': {'$eq': sensor_id}},
                      {mongo_status_col: {'$eq': status}}]}
    sort = 'DateTime'
    return fetch(con, where, sort, limit)


def interpolates_prepend_data(con, mongo_status_col, sensor_id, outliersize):
    where = {
        '$and': [{mongo_status_col: {'$ne': 'Y'}}, {'Property': {'$eq': 'Signal'}}, {'SensorId': {'$eq': sensor_id}}]}
    sort = [("DateTime", pymongo.DESCENDING)]
    return fetch(con, where, sort, outliersize)


def index_unique_DateTime(con, signal_types):
    for signal_type in signal_types:
        indexName = 'uniq_dt_%s' % (signal_type)
        if indexName not in con.index_information():
            cols = [("DateTime", pymongo.DESCENDING), ("Property", pymongo.DESCENDING),
                    ("SensorId", pymongo.DESCENDING)]
            cols.append(("%s_Status" % (signal_type), pymongo.DESCENDING))
            cols.append(("%s_FEStatus" % (signal_type), pymongo.DESCENDING))
            cols.append(("%s_QaQcStatus" % (signal_type), pymongo.DESCENDING))
            cols.append(("%s_SignalQuality" % (signal_type), pymongo.DESCENDING))
            create_index(con, cols, indexName, True, False)
            logger.info("Index:%s created" % (indexName))

        indexName = 'fetch_%s' % (signal_type)
        if indexName not in con.index_information():
            cols = [("Property", pymongo.DESCENDING), ("SensorId", pymongo.DESCENDING)]
            cols.append(("%s_Status" % (signal_type), pymongo.DESCENDING))
            cols.append(("%s_FEStatus" % (signal_type), pymongo.DESCENDING))
            cols.append(("%s_QaQcStatus" % (signal_type), pymongo.DESCENDING))
            cols.append(("%s_SignalQuality" % (signal_type), pymongo.DESCENDING))
            create_index(con, cols, indexName, True, False)
            logger.info("Index:%s created" % (indexName))

        indexName = 'denoise_%s' % signal_type
        if indexName not in con.index_information():
            cols = [("Property", pymongo.DESCENDING), ("SensorId", pymongo.DESCENDING),
                    ("%s_Status" % signal_type, pymongo.DESCENDING)]
            create_index(con, cols, indexName, True, False)
            logger.info("Index:%s created" % (indexName))


def interpolates_update_B(con, type, mongo_s, mongo_status_col, mongo_signal_quality, upd_status):
    dateYmdHms = datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
    where = {'_id': mongo_s['_id']}
    upsert = {'upsert': False}
    sset = {'$set': {"%s_interpolated" % (type): None, mongo_status_col: upd_status, mongo_signal_quality: 'B',
                     'UpdatedBy': 'filter.interpolation', 'UpdatedAt': dateYmdHms}}
    return update(con, where, sset, upsert)


def interpolates_update_G(con, type, mongo_s_i, mongo_s, mongo_status_col, mongo_signal_quality, upd_status):
    dateYmdHms = datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
    where = {'_id': mongo_s['_id']}
    sset = {'$set': {"%s_interpolated" % (type): mongo_s_i, mongo_status_col: upd_status, mongo_signal_quality: 'G',
                     'UpdatedBy': 'filter.interpolation', 'UpdatedAt': dateYmdHms}}
    upsert = {'upsert': False}
    return update(con, where, sset, upsert)


def outlires_data(con, mongo_status_col, sensor_id, status):
    where = {'$and': [{'Property': {'$eq': 'Signal'}}, {'SensorId': {'$eq': sensor_id}},
                      {mongo_status_col: {'$eq': status}}]}
    sort = 'DateTime'
    return fetch(con, where, sort)


def outliers_update(con, type, mongo_s, swo_outlier, signal_quality, outlierstat, mongo_status_col, upd_status):
    dateYmdHms = datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
    where = {'_id': mongo_s['_id']}
    sset = {'$set': {"%s_outliersremoved" % (type): swo_outlier if signal_quality == 'G' else None,
                     "%s_outlierstat" % (type): outlierstat, mongo_status_col: upd_status,
                     'UpdatedBy': 'filter.outlier', 'UpdatedAt': dateYmdHms}}
    upsert = {'upsert': False}
    return update(con, where, sset, upsert)


def convertNumToStringKeys(d):
    ds = dict()
    for k1 in d.keys():
        ds2 = dict()
        for k2 in d[k1].keys():
            ds2["%s" % (k2)] = np.ndarray.tolist(d[k1][k2]) if (type(d[k1][k2]) is np.ndarray) else d[k1][k2]
        ds["%s" % (k1)] = ds2
    return ds


def denoise_ep_insert(con, sensor_id, decomposed, decomposedf, signal_d_derivative1, signal_d_derivative2, energies,
                      energiesf, power, powerf, freqs, time, signal_type):
    dateYmdHms = datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
    outd = dict()
    outd['Property'] = 'SignalDenoiseEnergy'
    outd['CreatedAt'] = dateYmdHms
    outd['SignalType'] = signal_type
    outd['SensorId'] = sensor_id
    outd['Time'] = time
    outd['Energy'] = energies.tolist()
    outd['EnergyF'] = energiesf.tolist()
    outd['Power'] = power.tolist()
    outd['PowerF'] = powerf.tolist()
    outd['Frequency'] = freqs
    outd['Decomposed'] = decomposed
    outd['Decomposedf'] = decomposedf
    outd['Derivative1'] = signal_d_derivative1
    outd['Derivative2'] = signal_d_derivative2
    return insert(con, outd)


def denoise_ams_insert(con, sensor_id, all_miltipliers_stat, datetimes, signal_type):
    outd = dict()
    outd['Property'] = 'SignalDenoiseMultipliers'
    outd['SignalType'] = signal_type
    outd['SensorId'] = sensor_id
    outd['DateTime'] = datetimes[0]
    ds, dsr = [], []
    for i in range(0, len(all_miltipliers_stat['decomposed'])):
        if all_miltipliers_stat['decomposed'][i] is not None:
            d1 = convertNumToStringKeys(all_miltipliers_stat['decomposed'][i])
            d2 = convertNumToStringKeys(all_miltipliers_stat['decomposed_r'][i])
            ds.append(d1)
            dsr.append(d2)
        else:
            ds.append(None)
            dsr.append(None)
    all_miltipliers_stat1 = deepcopy(all_miltipliers_stat)
    all_miltipliers_stat1['decomposed'] = ds
    all_miltipliers_stat1['decomposed_r'] = dsr
    outd[
        'MultipliersStat'] = all_miltipliers_stat1  # Statistics of SNR=5,10,15,..95,100 is stored in data_dict. Check structure in var denoise().all_miltipliers_stat
    return insert(con, outd)


def denoise_data_count(con, mongo_status_col, sensor_id):
    where = {'$and': [{'Property': {'$eq': 'Signal'}}, {'SensorId': {'$eq': sensor_id}},
                      {mongo_status_col: {'$in': ['O', 'P']}}]}
    return fetch(con, where, count=True)


def denoise_data(con, mongo_status_col, sensor_id, limit):
    where = {'$and': [{'Property': {'$eq': 'Signal'}}, {'SensorId': {'$eq': sensor_id}},
                      {mongo_status_col: {'$in': ['O', 'P']}}]}
    sort = 'DateTime'
    return fetch(con, where, sort, limit)


def denoise_update(con, signal_type, mongo_s, signal_d, signal_quality, denoisestat, _id4, _id5, update_status,
                   mongo_status_col, mongo_festatus_col):
    dateYmdHms = datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
    where = {'_id': mongo_s['_id']}
    sset = {'$set':
                {"%s_denoised" % (signal_type): signal_d if signal_quality == 'G' else None,
                 "%s_energypower" % (signal_type): _id5,
                 "%s_denoisestat" % (signal_type): denoisestat,
                 "%s_denoiseMultipliers" % (signal_type): _id4,
                 mongo_status_col: update_status,
                 mongo_festatus_col: update_status,
                 'UpdatedBy': 'filter.denoise', 'UpdatedAt': dateYmdHms
                 }
            }
    upsert = {'upsert': False}
    return update(con, where, sset, upsert)


def stft_insert(con, signal_type, sensor_id, datetimes, windowfunction_stft, times_stft, amplitudes_stft,
                phaseangles_stft, mx_f1, frequency_stft):
    dateYmdHms = datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
    outd = dict()
    outd['Property'] = 'SignalSTFT'
    outd['SignalType'] = signal_type
    outd['SensorId'] = sensor_id
    outd['CreatedAt'] = dateYmdHms
    outd['DateTime'] = datetimes
    outd['WindowFunction_stft'] = windowfunction_stft
    outd['Times_stft'] = times_stft
    outd['Amplitudes_stft'] = amplitudes_stft
    outd['PhaseAngles_stft'] = phaseangles_stft
    outd['MaxAmpFrequency_stft'] = mx_f1
    outd['Frequency_stft'] = np.ndarray.tolist(frequency_stft)
    return insert(con, outd)


def cwt_insert(con, signal_type, sensor_id, datetimes, frequencies_dict, amplitudes_dict):
    dateYmdHms = datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
    outd = dict()
    outd['Property'] = 'SignalCWT'
    outd['Frequency_cwt'] = frequencies_dict
    outd['Amplitudes_cwt'] = amplitudes_dict
    outd['SignalType'] = signal_type
    outd['SensorId'] = sensor_id
    outd['DateTime'] = datetimes
    outd['CreatedAt'] = dateYmdHms
    return insert(con, outd)


def fft_insert(con, signal_type, sensor_id, datetimes, fft_amplitudes, fft_phaseangles):
    dateYmdHms = datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
    outd = dict()
    outd['Property'] = 'SignalFFT'
    outd['SignalType'] = signal_type
    outd['SensorId'] = sensor_id
    outd['CreatedAt'] = dateYmdHms
    outd['DateTime'] = datetimes
    outd['Amplitudes_fft'] = np.ndarray.tolist(fft_amplitudes)
    outd['PhaseAngles_fft'] = np.ndarray.tolist(fft_phaseangles)
    return insert(con, outd)


def kpca_insert(con, data_dict, signal_type):
    dateYmdHms = datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
    outd = dict()
    outd['Property'] = 'SignalKPCA'
    outd['SignalType'] = signal_type
    outd['CreatedAt'] = dateYmdHms
    for k in data_dict:
        outd[k] = data_dict[k]
    return insert(con, outd)


def lda_kfda_insert(con, data_dict, signal_type):
    dateYmdHms = datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
    outd = dict()
    outd['Property'] = 'SignalLDAKFDA'
    outd['SignalType'] = signal_type
    outd['CreatedAt'] = dateYmdHms
    for k in data_dict:
        outd[k] = data_dict[k]
    return insert(con, outd)


def rts_effad_data(con, mongo_status_col, sensor_id, status):
    where = {'$and': [{'Property': {'$eq': 'Signal'}}, {'SensorId': {'$eq': sensor_id}},
                      {mongo_status_col: {'$eq': status}}]}
    sort = 'DateTime'
    return fetch(con, where, sort)


def rts_effad_data_count(con, mongo_status_col, sensor_id, status):
    where = {'$and': [{'Property': {'$eq': 'Signal'}}, {'SensorId': {'$eq': sensor_id}},
                      {mongo_status_col: {'$eq': status}}]}
    return fetch(con, where, count=True)


def rts_effad_update(con, _id, k_mul_id, k_ca1_id, k_ca2_id, l_mul_id, l_ca1_id, l_ca2_id, denoise_columns):
    dateYmdHms = datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
    where = {'_id': _id}
    upsert = {'upsert': False}
    udata = {'PcaKpca_denoised_mul': k_mul_id, 'PcaKpca_denoised_ca1': k_ca1_id, 'PcaKpca_denoised_ca2': k_ca2_id,
             'LdaKfda_denoised_mul': l_mul_id, 'LdaKfda_denoised_ca1': l_ca1_id, 'LdaKfda_denoised_ca2': l_ca2_id,
             'UpdatedBy': 'rts.extractFeaturesFromAllDenoise', 'UpdatedAt': dateYmdHms}
    for signal_type in denoise_columns: udata['%s_FEStatus' % (signal_type)] = 'C'
    sset = {'$set': udata}
    return update(con, where, sset, upsert)


def rts_sck_data_count(con, mongo_status_col, sensor_id, status):
    where = {'$and': [{'Property': {'$eq': 'Signal'}}, {'SensorId': {'$eq': sensor_id}},
                      {mongo_status_col: {'$eq': status}}]}
    return fetch(con, where, count=True)


def rts_sck_data(con, mongo_status_col, sensor_id, status):
    where = {'$and': [{'Property': {'$eq': 'Signal'}}, {'SensorId': {'$eq': sensor_id}},
                      {mongo_status_col: {'$eq': status}}]}
    sort = 'DateTime'
    return fetch(con, where, sort)


def rts_sck_update(con, _id, _id1, _id2, _id3, signal_kpca_denoised_ind, signal_type, update_status, mongo_status_col):
    dateYmdHms = datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
    where = {'_id': _id}
    upsert = {'upsert': False}
    sset = {'$set': {"%s_stft" % (signal_type): _id1,
                     "%s_cwt_d" % (signal_type): _id2,
                     "%s_fft" % (signal_type): _id3,
                     "%s_kpcaDenoisedInd" % (signal_type): signal_kpca_denoised_ind,
                     mongo_status_col: update_status,
                     'UpdatedBy': 'filter.stftCwtKpca',
                     'UpdatedAt': dateYmdHms}}
    return update(con, where, sset, upsert)


def rts_ips_data(con, mongo_status_col, sensor_id, status):
    where = {'$and': [{'Property': {'$eq': 'Signal'}}, {'SensorId': {'$eq': sensor_id}},
                      {mongo_status_col: {'$eq': status}}]}
    sort = [("DateTime", pymongo.ASCENDING)]
    limit = 1
    return fetch(con, where, sort, limit)


def rts_ips_update_many(con, sensor_id, signal_type, min_dt, max_dt):
    dateYmdHms = datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
    where = {'$and': [{'Property': {'$eq': 'Signal'}}, {'SensorId': {'$eq': sensor_id}}, {'DateTime': {'$gte': min_dt}},
                      {'DateTime': {'$lt': max_dt}}]}
    upsert = {'upsert': False}
    sset = {'$set': {"%s_FEStatus" % (signal_type): 'X', 'UpdatedBy': 'rts.extractFeaturesFromAllDenoise',
                     'UpdatedAt': dateYmdHms}}
    return update_many(con, where, sset, upsert)


def rts_m_create_coll(db, collName):
    if collName not in db.collection_names(): db.create_collection(collName)
    coll = db[collName]
    return coll


def rts_m_default_i(proc_name=None):
    dateYmdHms = datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
    DEFAULT_INSERT = {
        "UpdatedBy": "rts_bridge" if proc_name is None else proc_name,
        "UpdatedAt": dateYmdHms
    }
    return DEFAULT_INSERT


def rts_m_update(con, property, collection, data_dict, sensorid):
    where = {'Property': property, 'WellNo': collection}
    if sensorid is not None: where['SensorId'] = sensorid
    sset = rts_m_default_i()
    sset.update(data_dict)
    sset.update(where)
    upsert = {'upsert': True}
    return update(con, where, sset, upsert)


def rts_m_insert_ifnotexist(con, where, data_dict):
    sset = rts_m_default_i()
    sset.update(data_dict)
    return insert_ifnotexist(con, where, sset)


def rts_m_push_1(con, where, data_dict, upser):
    sset = rts_m_default_i()
    sset.update(data_dict)
    sset = {'$set': sset}
    upsert = {'upsert': upser}
    return update(con, where, sset, upsert)


def rts_b_wells_data(con, wellcode):
    where = {'_id': ObjectId('%s' % (wellcode))}
    return fetch(con, where)
