import logging
import logging.config
import os

from Lib.Utils import getPOASRootDir, raiseErrIfFileNotFound, setLogFile, ensureDir
from Scripts.esp.settings import LOG_DIR


def get_logger(qualname, master_module=False):
    configFile = os.path.join(getPOASRootDir(), 'Config', 'log.conf')
    raiseErrIfFileNotFound("Logging configuration file not found", configFile)
    log_dir = LOG_DIR
    setLogFile(qualname, log_dir, master_module)
    ensureDir(os.environ['PyLogfile'])
    logging.config.fileConfig(configFile)
    logger = logging.getLogger('PyLogfile')
    return logger
