import argparse
import re

import psutil
from bson.objectid import ObjectId

from Lib.DBLib import *
from Lib.Utils import *
from Scripts.esp.core.storage.fetch_most import rts_b_wells_data


class reset(object):
    def __init__(this):
        this.getoptions()
        this.logger= get_logger('RTS')
        this.db= getDbCon(this.args.service)
        this.dbCentral= getDbCon('Central') 
        #this.well_info= this.get_wellsdata()
        #this.coll= this.db[this.well_info['Collection']] 
        if 'WELLS' not in this.dbCentral.collection_names(): this.dbCentral.create_collection('WELLS')
        this.collCentral= this.dbCentral['WELLS']
        this.dateYmdHms= datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
        wells_sensors_Data= this.well_data()
        _id= this.collCentral.insert(wells_sensors_Data)

    def getoptions(this):
        parser = argparse.ArgumentParser()
        parser.add_argument("--service",           dest='service',          default="RTS",   help='RTS')
        parser.add_argument("--wellcode",          dest='wellcode',         default=None,    help='_id from Central.WELLS')
        parser.add_argument("--rmcentralwellsid",  dest='rmcentralwellsid', default=False,   help='_id from Central.WELLS', action='store_true')
        parser.add_argument("--debug",             dest="debug",                             help="Wait for user confirmation", action='store_true')
        this.args = parser.parse_args()
        if this.args.wellcode is None:
            print("please provide --wellcode")
            sys.exit(1)

    def createCollIfNotExists(this, db, collName):
        if collName not in db.collection_names(): db.create_collection(collName)
        coll= db[collName] 
        return coll       


    def get_wellsdata(this):
        coll= this.createCollIfNotExists(this.dbCentral, 'WELLS')
        wells_info= rts_b_wells_data(coll, this.args.wellcode)
        for w in wells_info:
            if w['_id'] == ObjectId(this.args.wellcode): 
                return w
        this.logger.error('%s is not setup in Central.WELL database!' %(this.args.wellcode))
        sys.exit(1)

    def kill_pid(this, process_name):
        this.logger.info("Killing rts_manager")
        child = subprocess.Popen(['pgrep -f %s' %(process_name)], stdout=subprocess.PIPE, shell=True)
        result = child.communicate()[0]
        result= result.strip().split('\n')
        if result[0] == '':
            this.logger.info("Process name:%s not found" %(process_name))
            return
        for r in result:
            p = psutil.Process(int(r))
            if this.args.debug:
                this.logger.info(p.as_dict()['cmdline'])
                yn= raw_input("Kill above process? ([y]/n) ").strip()
                if yn in ['y', 'Y', '']:
                    p.terminate()
                    this.logger.info("killed pid: %s" %(r))
                else:
                    this.logger.info("skipped pid: %s" %(r))
            else:
                p.terminate()
                this.logger.info("killed pid: %s" %(r))
        return

    def drop_coll_in_rts(this):
        if this.args.debug:
            this.logger.info("Collection:%s from DB:%s will be dropped" %(this.well_info['Collection'], this.args.service))
            yn= raw_input("Would you like to remove? ([y]/n)").strip()
            if yn in ['y', 'Y', '']:
                this.coll.drop()
                this.logger.info("Removed collection: %s" %(this.well_info['Collection']))
            else:
                this.logger.info("skipped!")
        else:
            this.coll.drop()
            this.logger.info("Removed collection: %s" %(this.well_info['Collection']))

    def add_sample_well_in_central(this):
        if this.args.rmcentralwellsid:
            if this.args.debug:
                this.logger.info("WellName like :.*%s.* from Central.WELLS will be removed" %(this.well_info['WellName']))
                yn= raw_input("Would you like to remove? ([y]/n)").strip()
                if yn in ['y', 'Y', '']:
                    rgx = re.compile('.*%s.*' %(this.well_info['WellName']), re.IGNORECASE)  # compile the regex
                    _id = this.collCentral.remove({'WellName':rgx})    
                    this.logger.info("removed WellName:%s from Central.WELLS, %s" %(this.well_info['WellName'], _id))
            else:
                rgx = re.compile('.*%s.*' %(this.well_info['WellName']), re.IGNORECASE)  # compile the regex
                _id = this.collCentral.remove({'WellName':rgx})    
                #_id= this.collCentral.remove({'WellName':ObjectId('%s' %(this.args.wellcode))})
                this.logger.info("removed WellName:%s from Central.WELLS, %s" %(this.well_info['WellName'], _id))
        count= len([s for s in this.collCentral.find({'_id':ObjectId('%s' %(this.args.wellcode))})])
        if count == 0:
            wells_sensors_Data= this.well_data()
            _id= this.collCentral.insert(wells_sensors_Data)
            this.logger.info("Sample Well (R183 - hardcoded) inserted into Central.WELLS collection, %s" %(_id))
        else:
            this.logger.info("Records already found in Central.WELLS collection!")

    def main(this):
        #this.kill_pid('rts_manager.py')
        #this.kill_pid('rts_bridge.py')
        #this.drop_coll_in_rts()
        this.add_sample_well_in_central()


    def well_data(this):
        return {
            "WellId" : 1,
            "Collection" : 'R183',
            "WellIdentity" : "",
            "WellCode" : 'R183',
            "WellName" : 'R183',
            "TargetEast" : "-79.24",
            "TargetNorth" : "43.4",
            "FieldName" : "Flynn Landry",
            "ReservoirName" : "Denton Duncan",
            "systemSummary" : {
                "WellType" : "Oil Producer",
                "WellCompletionType" : "Sand Control",
                "WellEnvironment" : "Land",
                "PerforationLayer" : "",
                "InitialCompletionDate" : "15/09/2021"
            },
            "esp" : {
                "Workover" : {
                    "No" : "0",
                    "Date" : "15/09/2021"
                }
            },
            "Status" : "Open",
            "ArtificialLift" : "Yes",
            "ArtificialLiftType" : "ESP",
            "CreatedAt" : 1631695063,
            "UpdatedAt" : 1631695177,
            "UpdatedBy" : "600278d33639713f4307ffa2",
            "sensors" : [
                {
                    "code" : "wellhead_pressure",
                    "guage_name" : "Wellhead Gauge",
                    "sensor_name" : "wellhead_pressure",
                    "sensor_datetime_format" : "%Y-%m-%d %H:%M:%S",
                    "field" : {
                        "type" : "pressure",
                        "unit" : "psi",
                        "change_unit_date" : False,
                        "new_unit" : "psi"
                    },
                    "x" : "Time",
                    "y" : "value",
                    "z" : "",
                    "sensor_status" : "Active",
                    "data_sampling_frequency" : [{
                        "value" : "12",
                        "unit" : "hour",
                        "seconds" : 43200,
                        "updated_at" : 5555
                    },],
                    "permisible_limit" : {
                        "min" : "1",
                        "max" : "100",
                        "opt" : "500"
                    },
                    "data_visualization_grouping" : "A+B+C / None",
                    "updated_at" : 1631695357,
                    "updated_by" : "600278d33639713f4307ffa2"
                },
                {
                    "code" : "wellhead_temperature",
                    "guage_name" : "Wellhead Gauge",
                    "sensor_name" : "wellhead_temperature",
                    "sensor_datetime_format" : "%Y-%m-%d %H:%M:%S",
                    "field" : {
                        "type" : "temperature",
                        "unit" : "deg F",
                        "change_unit_date" : False,
                        "new_unit" : "deg F"
                    },
                    "x" : "Time",
                    "y" : "value",
                    "z" : "",
                    "sensor_status" : "Active",
                    "data_sampling_frequency" : [{
                        "value" : "12",
                        "unit" : "hour",
                        "seconds" : 43200,
                        "updated_at" : 5555
                    },],
                    "permisible_limit" : {
                        "min" : "1",
                        "max" : "100",
                        "opt" : "500"
                    },
                    "data_visualization_grouping" : "",
                    "updated_at" : 1631695357,
                    "updated_by" : "600278d33639713f4307ffa2"
                },
                {
                    "code" : "flowline_pressure",
                    "guage_name" : "Flow Line Gauge",
                    "sensor_name" : "flowline_pressure",
                    "sensor_datetime_format" : "%Y-%m-%d %H:%M:%S",
                    "field" : {
                        "type" : "pressure",
                        "unit" : "psi",
                        "change_unit_date" : False,
                        "new_unit" : "psi"
                    },
                    "x" : "Time",
                    "y" : "value",
                    "z" : "",
                    "sensor_status" : "Active",
                    "data_sampling_frequency" : [{
                        "value" : "12",
                        "unit" : "hour",
                        "seconds" : 43200,
                        "updated_at" : 5555
                    },],
                    "new_data_sampling_frequency" : {
                        "updated_at" : False,
                        "value" : "1",
                        "unit" : "minute"
                    },
                    "permisible_limit" : {
                        "min" : "1",
                        "max" : "100",
                        "opt" : "500"
                    },
                    "data_visualization_grouping" : "",
                    "updated_at" : 1631695357,
                    "updated_by" : "600278d33639713f4307ffa2"
                },
                {
                    "code" : "motor_current",
                    "guage_name" : "ESP Sensor_Module",
                    "sensor_name" : "motor_current",
                    "sensor_datetime_format" : "%Y-%m-%d %H:%M:%S",
                    "field" : {
                        "type" : "electric current",
                        "unit" : "ampere",
                        "change_unit_date" : False,
                        "new_unit" : "ampere"
                    },
                    "x" : "Time",
                    "y" : "value",
                    "z" : "",
                    "sensor_status" : "Active",
                    "data_sampling_frequency" : [{
                        "value" : "12",
                        "unit" : "hour",
                        "seconds" : 43200,
                        "updated_at" : 5555
                    },],
                    "permisible_limit" : {
                        "min" : "1",
                        "max" : "100",
                        "opt" : "500"
                    },
                    "data_visualization_grouping" : "",
                    "updated_at" : 1631695357,
                    "updated_by" : "600278d33639713f4307ffa2"
                },
                {
                    "code" : "pump_intake_pressure",
                    "guage_name" : "ESP Sensor_Module",
                    "sensor_name" : "pump_intake_pressure",
                    "sensor_datetime_format" : "%Y-%m-%d %H:%M:%S",
                    "field" : {
                        "type" : "pressure",
                        "unit" : "psi",
                        "change_unit_date" : False,
                        "new_unit" : "psi"
                    },
                    "x" : "Time",
                    "y" : "value",
                    "z" : "",
                    "sensor_status" : "Active",
                    "data_sampling_frequency" : [{
                        "value" : "12",
                        "unit" : "hour",
                        "seconds" : 43200,
                        "updated_at" : 5555,
                    },],
                    "permisible_limit" : {
                        "min" : "1",
                        "max" : "100",
                        "opt" : "500"
                    },
                    "data_visualization_grouping" : "",
                    "updated_at" : 1631695357,
                    "updated_by" : "600278d33639713f4307ffa2"
                },
                {
                    "code" : "pump_intake_temperature",
                    "guage_name" : "ESP Sensor_Module",
                    "sensor_name" : "pump_intake_temperature",
                    "sensor_datetime_format" : "%Y-%m-%d %H:%M:%S",
                    "field" : {
                        "type" : "temperature",
                        "unit" : "deg F",
                        "change_unit_date" : False,
                        "new_unit" : "deg F"
                    },
                    "x" : "Time",
                    "y" : "value",
                    "z" : "",
                    "sensor_status" : "Active",
                    "data_sampling_frequency" : [{
                        "value" : "12",
                        "unit" : "hour",
                        "seconds" : 43200,
                        "updated_at" : 5555
                    },],
                    "permisible_limit" : {
                        "min" : "1",
                        "max" : "100",
                        "opt" : "500"
                    },
                    "data_visualization_grouping" : "",
                    "updated_at" : 1631695357,
                    "updated_by" : "600278d33639713f4307ffa2"
                },
                {
                    "code" : "motor_temperature",
                    "guage_name" : "ESP Sensor_Module",
                    "sensor_name" : "motor_temperature",
                    "sensor_datetime_format" : "%Y-%m-%d %H:%M:%S",
                    "field" : {
                        "type" : "temperature",
                        "unit" : "deg F",
                        "change_unit_date" : False,
                        "new_unit" : "deg F"
                    },
                    "x" : "Time",
                    "y" : "value",
                    "z" : "",
                    "sensor_status" : "Active",
                    "data_sampling_frequency" : [{
                        "value" : "12",
                        "unit" : "hour",
                        "seconds" : 43200,
                        "updated_at" : 5555
                    },],
                    "permisible_limit" : {
                        "min" : "1",
                        "max" : "100",
                        "opt" : "500"
                    },
                    "data_visualization_grouping" : "",
                    "updated_at" : 1631695357,
                    "updated_by" : "600278d33639713f4307ffa2"
                },
                {
                    "code" : "pump_discharge_pressure",
                    "guage_name" : "ESP Sensor_Module",
                    "sensor_name" : "pump_discharge_pressure",
                    "sensor_datetime_format" : "%Y-%m-%d %H:%M:%S",
                    "field" : {
                        "type" : "pressure",
                        "unit" : "psi",
                        "change_unit_date" : False,
                        "new_unit" : "psi"
                    },
                    "x" : "Time",
                    "y" : "value",
                    "z" : "",
                    "sensor_status" : "Active",
                    "data_sampling_frequency" : [{
                        "value" : "12",
                        "unit" : "hour",
                        "seconds" : 43200,
                        "updated_at" : 5555
                    },],
                    "permisible_limit" : {
                        "min" : "1",
                        "max" : "100",
                        "opt" : "500"
                    },
                    "data_visualization_grouping" : "",
                    "updated_at" : 1631695357,
                    "updated_by" : "600278d33639713f4307ffa2"
                },
                {
                    "code" : "esp_frequency",
                    "guage_name" : "VSD/Switch_Board",
                    "sensor_name" : "esp_frequency",
                    "sensor_datetime_format" : "%Y-%m-%d %H:%M:%S",
                    "field" : {
                        "type" : "frequency",
                        "unit" : "hz",
                        "change_unit_date" : False,
                        "new_unit" : "hz"
                    },
                    "x" : "Time",
                    "y" : "value",
                    "z" : "",
                    "sensor_status" : "Active",
                    "data_sampling_frequency" : [{
                        "value" : "12",
                        "unit" : "hour",
                        "seconds" : 43200,
                        "updated_at" : 5555
                    },],
                    "permisible_limit" : {
                        "min" : "1",
                        "max" : "100",
                        "opt" : "500"
                    },
                    "data_visualization_grouping" : "\u0000",
                    "updated_at" : 1631695357,
                    "updated_by" : "600278d33639713f4307ffa2"
                }
            ]
        }



if __name__ == "__main__":
    ob=reset()
    #ob.main()


#python playground/well_info.py --wellname R183
#python playground/reset.py --wellcode 6144809324f72599ac46aff0 [--rmcentralwellsid --debug]

