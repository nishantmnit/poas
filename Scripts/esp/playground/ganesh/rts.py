#!/usr/bin/env python

from Scripts.esp.modules.fft import *
from Scripts.esp.modules.interpolates import *
from Scripts.esp.modules.raw_interpolates import *

from Scripts.esp.core.storage.fetch_most import rts_effad_data, rts_effad_data_count, rts_effad_update
from Scripts.esp.core.storage.fetch_most import rts_ips_data, rts_ips_update_many
from Scripts.esp.core.storage.fetch_most import rts_sck_data, rts_sck_data_count, rts_sck_update


def get_options():
    parser = argparse.ArgumentParser()
    parser.add_argument("--service", dest='service', default="RTS", help='RTS')
    parser.add_argument("--collection", dest='collection', default="WellB",
                        help='Well collection name Well1/WellB/Well3')
    parser.add_argument("--sensorid", dest='sensorid', default="Sensor1", help='SensorId Sensor1/SensorB')
    parser.add_argument("--outliersize", dest="outliersize", default=32, help="32xn, n=1,2,3...128")
    parser.add_argument("--denoisesize_p", dest="denoisesize_p", default=256,
                        help="n, n=64,128,256,512,1024,2048,4096")
    parser.add_argument("--denoisesize_d", dest="denoisesize_d", default=512,
                        help="n, n=64,128,256,512,1024,2048,4096")
    parser.add_argument("--debug", dest="debug", help="plot graphs, print debug messages", action='store_true')

    args = parser.parse_args()
    args.outliersize = int(args.outliersize)
    args.denoisesize_d = int(args.denoisesize_d)
    args.denoisesize_p = int(args.denoisesize_p)
    return args


class rts:
    def __init__(self, service, collection, sensorid, signalDateTimeFormat='%d/%m/%Y %H:%M'):
        self.args = get_options()

        self.service = self.args.service if service is None else service
        self.collection = self.args.collection if collection is None else collection
        self.sensorid = self.args.sensorid if sensorid is None else sensorid

        self.logger = get_logger('rts')
        self.db = getDbCon(self.service if service is None else service)
        self.coll = self.db[self.collection if collection is None else collection]

        self.dateYmdHms = datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
        self.signalDateTimeFormat = 'a'  # '%d/%m/%Y %H:%M'

    def rawInterpolateSignals(self, qaqcSize, raw_interpolate_size, _signalTypes, samplingFreq, sensorDateTimeFormat):
        iob = raw_interpolates(self.service, self.collection, self.sensorid, self.logger)
        counts = iob.main(qaqcSize, raw_interpolate_size, _signalTypes, samplingFreq, sensorDateTimeFormat)
        return counts

    def interpolateSignal(self, outliersize, denoiseColumns):
        iob = interpolates(self.service, self.collection, self.sensorid, self.logger)
        iob.interpolateSignal(outliersize, denoiseColumns, 'Y', 'I')

    def removeOutliers(self, outliersize, denoiseColumns):
        oob = outliers(self.service, self.collection, self.sensorid, self.logger)
        oob.removeOutliers(outliersize, denoiseColumns, 'I', 'O')

    def removeNoise(self, denoiseColumns, denoiseSize, status, updateStatus):
        dob = denoise(self.service, self.collection, self.sensorid, self.logger, self.args.debug,
                      self.signalDateTimeFormat)
        dob.removeNoise(denoiseColumns, denoiseSize, status, updateStatus)

    def getStftMongoId(self, signal, datetimes, signalType):
        stftobj = stft(self.service, self.collection, self.sensorid, self.logger, self.signalDateTimeFormat)
        return stftobj.main(signal, datetimes, signalType)

    def getFeatures(self, signal, datetimes, signalType):
        pass
        # obj = features(self.service, self.collection, self.sensorid, self.logger, self.signalDateTimeFormat)
        # obj.svm()

    def getCwtMongoId(self, signal, datetimes, signalType, inputSiganlType):
        centralFrequency = [3, 6]
        cwtobj = cwt(self.service, self.collection, self.sensorid, self.logger, self.signalDateTimeFormat,
                     inputSiganlType)
        return cwtobj.main(signal, datetimes, centralFrequency, signalType)

    def getFftMongoId(self, signal_d, datetimes, signalType):
        fftobj = fft(self.service, self.collection, self.sensorid, self.logger, self.signalDateTimeFormat)
        return fftobj.main(signal_d, datetimes, signalType)

    def getkpca(self, signal_d, datetimes, type):
        # ob= kpca(self.service, self.collection, self.sensorid, self.logger, self.signalDateTimeFormat)
        # return ob.main(signal_d, datetimes, type, self.signalType if type == 'denoised_ind' else type)
        return None

    def get_lda_kfda(self, signal_d, datetimes, type):
        # ob= lda_kfda(self.service, self.collection, self.sensorid, self.logger, self.signalDateTimeFormat)
        # return ob.main(signal_d, datetimes, type, type)
        return None

    def ignorePartialSignals(self, denoiseColumns, status):
        mins = []
        for c in denoiseColumns:
            mongoStatusCol = '%s_FEStatus' % (c)
            m = rts_ips_data(self.coll, mongoStatusCol, self.sensorid, status)
            if len(m) > 0: mins.append(m[0]['DateTime'])
        maxDtFromMins = max(mins)
        minDtFromMins = min(mins)
        for signalType in denoiseColumns:
            rts_ips_update_many(self.coll, self.sensorid, signalType, minDtFromMins, maxDtFromMins)

    def isReadyForMachineLearn(self, denoiseSize, denoiseColumns, status):
        flag = True
        for signalType in denoiseColumns:
            mongoStatusCol = '%s_FEStatus' % (signalType)
            count = rts_effad_data_count(self.coll, mongoStatusCol, self.sensorid, status)
            if count < denoiseSize:
                self.logger.debug(
                    "Signal:%s, FEStatus=D, Count:%d is not ready for Feature extraction, wait for next denoised signals" % (
                        signalType, count))
                flag = False
                break
        return flag

    def extractFeaturesFromAllDenoise(self, denoiseSize, denoiseColumns, status='D'):
        # if signalQuality is B then?
        dateYmdHms = datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
        if not self.isReadyForMachineLearn(denoiseSize, denoiseColumns, status): return
        self.ignorePartialSignals(denoiseColumns, status)
        if not self.isReadyForMachineLearn(denoiseSize, denoiseColumns, status): return
        allDenoisedSignals = [[] for i in range(len(denoiseColumns))]
        mongoStatusCol = '%s_FEStatus' % (denoiseColumns[0])
        ms_datetimes, ids, ms_datetime = [], [], None
        removeBadSignals = {}
        for signals in rts_effad_data(self.coll, mongoStatusCol, self.sensorid, status):
            for si in range(0, len(denoiseColumns)):
                mongoSignalQuality = '%s_SignalQuality' % (denoiseColumns[si])
                if signals[mongoSignalQuality] == 'B': removeBadSignals[si] = denoiseColumns[si]
                allDenoisedSignals[si].append(signals['%s_denoised' % (denoiseColumns[si])])

            ms_datetimes.append(signals['DateTime'])
            ids.append(signals['_id'])
            if (len(ms_datetimes) % denoiseSize == 0):
                allDenoisedSignals1, denoiseColumns1 = [], []
                for si in range(0, len(denoiseColumns)):
                    if si in removeBadSignals.keys():
                        self.logger.warning('Removed BAD signal:%s from Features extraction' % (removeBadSignals[si]))
                        continue
                    allDenoisedSignals1.append(allDenoisedSignals[si])
                    denoiseColumns1.append(denoiseColumns[si])
                self.logger.info("Feature extraction is in progress for signals: %s" % (",".join(denoiseColumns1)))
                allDenoisedSignals1 = np.array([np.array(xi) for xi in allDenoisedSignals1])
                k_mul_id = self.getkpca(allDenoisedSignals1, ms_datetimes, 'denoised_mul')
                k_ca1_id = self.getkpca(allDenoisedSignals1, ms_datetimes, 'denoised_ca1')
                k_ca2_id = self.getkpca(allDenoisedSignals1, ms_datetimes, 'denoised_ca2')
                l_mul_id = self.get_lda_kfda(allDenoisedSignals1, ms_datetimes, 'denoised_mul')
                l_ca1_id = self.get_lda_kfda(allDenoisedSignals1, ms_datetimes, 'denoised_ca1')
                l_ca2_id = self.get_lda_kfda(allDenoisedSignals1, ms_datetimes, 'denoised_ca2')
                for i in range(len(ids)):
                    r = rts_effad_update(self.coll, ids[i], k_mul_id, k_ca1_id, k_ca2_id, l_mul_id, l_ca1_id, l_ca2_id,
                                         denoiseColumns1)
                allDenoisedSignals1 = [[]] * len(denoiseColumns1)
                ms_datetime = ms_datetimes[-1]
                ms_datetimes = []
                self.logger.info(
                    "Features extraction (pca,kpca,lda,kfda) from Denoised signals completed for: %s, DenoisedAt:%s" % (
                        ','.join(denoiseColumns1), ms_datetime))

    def stftCwtKpca(self, denoiseColumns, denoisesize_d=512, status='D'):
        stft_cwt_fft_size = denoisesize_d
        dateYmdHms = datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
        c = 0
        retStatus = {}
        for signalType in denoiseColumns:
            self.signalType = signalType
            mongoStatusCol = '%s_Status' % signalType
            mongoSignalQuality = '%s_SignalQuality' % signalType
            signalQuality = 'G'
            mongo_s, signal, signal_d, signal_o, signal_kpca_denoised_ind, signal_dt = [], [], [], [], [], []
            if rts_sck_data_count(self.coll, mongoStatusCol, self.sensorid, status) >= denoisesize_d:
                updateStatus = 'C'
                retStatus[signalType] = {'DenoisedAt': '', 'DenoisedCount': 0, 'DenoisedStatus': updateStatus}
                c = 0
                for signals in rts_sck_data(self.coll, mongoStatusCol, self.sensorid, status):
                    mongo_s.append(signals)
                    if signals[mongoSignalQuality] == 'B': signalQuality = 'B'
                    signal_d.append(signals["%s_denoised" % signalType])
                    signal_dt.append(signals['DateTime'])
                    # signal_o.append(signals["%s_outliersremoved" %(signalType)])
                    if len(signal_d) % denoisesize_d == 0:
                        if signalQuality == 'G':
                            signal_kpca_denoised_ind = self.getkpca(np.array([signal_d]),
                                                                    [ms['DateTime'] for ms in mongo_s], 'denoised_ind')
                        # if signalQuality== 'G': signal_kpca=None
                        for x in range(0, len(signal_d), stft_cwt_fft_size):
                            _id1, mnF, mxF, _id2, _id3 = None, None, None, None, None
                            if signalQuality == 'G':
                                self.logger.debug(
                                    "STFT, CWT and FFT computation in progress for signal type: %s" % (signalType))
                                _id1, mnF, mxF = self.getStftMongoId(signal_d[x:x + stft_cwt_fft_size],
                                                                     signal_dt[x:x + stft_cwt_fft_size], signalType)
                                self.logger.debug("STFT completed for signal type: %s" % (signalType))
                                _id2 = self.getCwtMongoId(signal_d[x:x + stft_cwt_fft_size],
                                                          signal_dt[x:x + stft_cwt_fft_size], signalType, 'denoised')
                                self.logger.debug("CWT completed for denoised signal type: %s" % (signalType))
                                # _id4= self.getCwtMongoId(signal_o[x:x+stft_cwt_fft_size], [mongo_s[x]["DateTime"],mongo_s[x+1]["DateTime"]], signalType, 'outliersremoved')
                                # self.logger.debug("CWT completed for outliersremoved signal type: %s" %(signalType))
                                _id3 = self.getFftMongoId(signal_d[x:x + stft_cwt_fft_size],
                                                          signal_dt[x:x + stft_cwt_fft_size], signalType)
                            for i in range(x, x + stft_cwt_fft_size):
                                r = rts_sck_update(self.coll, mongo_s[i]['_id'], _id1, _id2, _id3,
                                                   signal_kpca_denoised_ind, signalType, updateStatus, mongoStatusCol)
                                c += r['n']
                            self.logger.debug('%s fft,stft,cwt and kpca computed!, updateCounts:%s, updateStatus:%s' % (
                                signalType, c, updateStatus))
                        retStatus[signalType] = {
                            'stftCwtKpcaAt': '%s|%s' % (mongo_s[0]['DateTime_str'], mongo_s[-1]['DateTime_str']),
                            'stftCwtKpcaCount': c, 'stftCwtKpcaStatus': updateStatus}
                        mongo_s, signal, signal_d, signal_kpca_denoised_ind, signal_dt = [], [], [], [], []
            else:
                self.logger.debug('Min records (%d) not found for fft,pcakpca,stft,cwt! status:%s, signalType:%s' % (
                    denoisesize_d, status, signalType))
        return retStatus

    def allMultipliersStat_smooth_print(self, swoOutlier, allMultipliersStat):
        allMultipliersStat_smooth = {'signal_d_smooth': [], 'RMSE': [], 'SNR': [], 'PRD': [], 'R': [], 'Mean': []}
        for i in range(len(allMultipliersStat['SNR'])):
            if allMultipliersStat['RMSE'][i] is None:
                allMultipliersStat_smooth['signal_d_smooth'].append(None)
                allMultipliersStat_smooth['RMSE'].append(None)
                allMultipliersStat_smooth['SNR'].append(None)
                allMultipliersStat_smooth['PRD'].append(None)
                allMultipliersStat_smooth['R'].append(None)
                allMultipliersStat_smooth['Mean'].append(None)
            else:
                signal_d_smooth, signal_d_derivative1, signal_d_derivative2 = self.smooth_denoise(
                    allMultipliersStat['Signal_r'][i])
                RMSE, SNR, PRD, R, Mean = self.objectiveDenoise(swoOutlier, signal_d_smooth)
                allMultipliersStat_smooth['signal_d_smooth'].append(signal_d_smooth)
                allMultipliersStat_smooth['RMSE'].append(RMSE)
                allMultipliersStat_smooth['SNR'].append(SNR)
                allMultipliersStat_smooth['PRD'].append(PRD)
                allMultipliersStat_smooth['R'].append(R)
                allMultipliersStat_smooth['Mean'].append(Mean)


# rts.py
if __name__ == "__main__":
    #    try:
    ob = rts('RTS', 'X238', 'WellheadGauge')
    ob.stftCwtKpca(['motor_current, pump_intake_temperature', 'motor_temperature'], 256)
    # ob.removeNoise(['motor_current, pump_intake_temperature', 'motor_temperature'], 512, 'O', 'D')
