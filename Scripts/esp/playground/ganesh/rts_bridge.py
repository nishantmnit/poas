#! /usr/bin/python
from optparse import OptionParser
from time import sleep

import psutil
import rpyc
from bson.objectid import ObjectId

import settings
from Lib.DBLib import *
from Lib.Utils import *
from Scripts.esp.core.storage.fetch_most import rts_b_wells_data, rts_b_sensor_test_data_count, rts_b_sensor_test_data


def triggerWell(wellcode, sensorcode):
    print ("Triggered wellcode:%s, sensorcode:%s" % (wellcode, sensorcode))
    obj = rts_bridge()
    # getattr(obj, "bridge_%s" %(wellcode))(wellcode)
    getattr(obj, "bridge_%s" % ('WellB'))(wellcode, sensorcode)


class rts_bridge():
    def __init__(self):
        self.readOpts()
        self.config = self.setConfig()
        self.initDb()
        try:
            self.conn = rpyc.connect(self.config['server'], self.config['port'])
        except Exception as e:
            print ("Failed to establish connection with queue. Check if server is running.")
            print (e)
            # self.mail()
            quit()
        self.client = self.conn.root

    def setConfig(self):
        ymlConfig = readConfig(self.options.configfile)
        if not definedInHash(ymlConfig, self.options.service):
            print ("Please set config for self service in config file first.")
            quit()
        else:
            return ymlConfig[self.options.service]

    def initDb(self):
        self.RTSDb = getDbCon(self.options.service)  # default is RTS
        self.RTSDb_Test = getDbCon('RTS_TEST')  # default is RTS
        self.CenDb = getDbCon('Central')  # default is RTS

    def mail(self):
        body = "Action = %s</br>" % ('action')
        # sendMail("ganesh", self.options.email, "Job Scheduler failed to connect Queue manager", body)

        # <float>  Second => <float>*1
        # <float>  Minute => <float>*60
        # <float>  Hour   => <float>*60*60
        # <float>  Day    => <float>*60*60*60

    def readOpts(self):
        actions = ["bridgews", "refreshwellsdata", "stopws", "stopserver"]
        parser = OptionParser(usage='usage: %prog [options]')
        parser.add_option("-t", "--service", dest="service", default="RTS", help="service Name (RTS/..)")
        parser.add_option("-w", "--wellcode", dest="wellcode", default=None, help="mongo id from Central.WELLS")
        parser.add_option("-s", "--sensorcode", dest="sensorcode", default=None, help="Sensor id")
        parser.add_option("-a", "--action", dest="action", default='bridgews', help='/'.join(actions))
        parser.add_option("-c", "--configfile", dest="configfile",
                          default='%s/Config/JobScheduler.yml' % (settings.POAS_ROOT), help="config file")
        # parser.add_option("-c", "--configfile",     dest="configfile",          default='%s/Config/JobScheduler.yml' %(POAS_ROOT), help="config file")
        # parser.add_option("--mlsize",               dest="mlsize",              default=512,                  help="Features extraction i.e. ML size")
        # parser.add_option("--outliersize",          dest="outliersize",         default=32,                   help="outlier removal size")
        # parser.add_option("--denoisesizep",         dest="denoisesizep",        default=256,                  help="partial denoise size")
        # parser.add_option("--denoisesized",         dest="denoisesized",        default=256,                  help="final denoise size")
        # parser.add_option("--samplingfreq",         dest="samplingfreq",        default='43200',               help="For 1 hour => 3600 seconds")
        # parser.add_option("--sensordtformat",       dest="sensordtformat",      default='%d/%m/%Y %H:%M:%S',  help="DateTime format from sensor in linux style")
        # parser.add_option("--forwardmarchsize",     dest="rawinterpolatesize",  default=5,                    help="Minimum signal counts required for raw signals interpolation (should be >= 15 and <= 32)")
        (options, args) = parser.parse_args()
        self.options = options
        if not self.options.action in actions:
            print("Please provide valid action from %s!" % (actions))
            sys.exit(1)
        self.logger = get_logger('rts_bridge')

    def trimDfHeaders(self, df):
        renameCol = {}
        for col in df.columns:
            c = col.strip()
            renameCol[col] = c
        df.rename(columns=renameCol, inplace=True)
        return df

    def bridgews(self):
        if self.options.wellcode is None or self.options.sensorcode is None:
            self.logger.error("Usage: python rts_bridge.py --wellcode <WellB> --sensorcode <Sensor1>")
            sys.exit(1)
        process_name = 'rts_bridge.py.*-w.*%s.*-s.*%s' % (self.options.wellcode, self.options.sensorcode)
        process_name1 = 'rts_bridge.py.*-s.*%s.*-w.*%s' % (self.options.sensorcode, self.options.wellcode)
        if self.isAlreadyRunning(process_name) or self.isAlreadyRunning(process_name1):
            self.logger.error("rts_bridge.py is already running for --wellcode %s --sensorcode %s" % (
                self.options.wellcode, self.options.sensorcode))
            sys.exit(1)
        signalTypes, samplingfreq, self.well_info = self.refreshwellsdata()

        self.coll_rts_test = self.RTSDb_Test[self.well_info['Collection']]
        cnt = rts_b_sensor_test_data_count(self.coll_rts_test, self.options.sensorcode)
        if cnt == 0:
            self.logger.error("No record found for Sensor code:%s in RTS_TEST.%s" % (
                self.options.sensorcode, self.well_info['Collection']))
            sys.exit(1)

        i = 1
        for d in rts_b_sensor_test_data(self.coll_rts_test, self.options.sensorcode):
            # remove
            # loopme= True
            # while loopme:
            #     yn= raw_input("process next record? ([y]/n)").strip()
            #     if yn in ['y', 'Y', '']:
            #         loopme= False

            self.logger.debug("processing record count:%d" % (i))
            json = dict()
            json["DateTime"] = d["x_axis"]
            json[self.options.sensorcode] = d["y_axis"]
            json['%s_z_axis' % (self.options.sensorcode)] = d["z_axis"]

            ret_id = self.client.push_raw(self.options.service, self.well_info['Collection'], self.options.sensorcode,
                                          json)
            self.logger.debug("Record pushed into mongo:%s" % (ret_id))
            sleep(0.01)
        while 1:
            self.client.loop_outlier_denoise(self.options.service, self.well_info['Collection'],
                                             self.options.sensorcode)
            if samplingfreq <= 60:
                sleep(30)
            else:
                sleep(10)

    def createCollIfNotExists(self, db, collName):
        if collName not in db.collection_names(): db.create_collection(collName)
        coll = db[collName]
        return coll

    def refreshwellsdata(self):
        if self.options.wellcode is None or self.options.sensorcode is None:
            self.logger.error("Usage: python rts_bridge.py --wellcode <WellB> --sensorcode <Sensor1>")
            sys.exit(1)
        coll = self.createCollIfNotExists(self.CenDb, 'WELLS')
        wells_info = rts_b_wells_data(coll, self.options.wellcode)
        well_info = None
        for w in wells_info:
            if w['_id'] == ObjectId(self.options.wellcode):
                well_info = w
                break
        if well_info is None:
            self.logger.error('%s is not setup in Central.WELL database!' % (self.options.wellcode))
            sys.exit(1)
        signalTypes, samplingfreq = self.client.refreshwellsdata(self.options.service, well_info['Collection'],
                                                                 self.options.sensorcode, well_info)
        return signalTypes, samplingfreq, well_info

    def isAlreadyRunning(self, process_name):
        # below will work on linux, mac, ubuntu; Need separate code for windows.
        child = subprocess.Popen(['pgrep -f %s' % (process_name)], stdout=subprocess.PIPE, shell=True)
        result = child.communicate()[0]
        result = result.strip().split('\n')
        f = True
        if len(result) <= 1:
            f = False
        return f

    def kill_pid(self, process_name):
        # below will work on linux, mac, ubuntu; Need separate code for windows.
        self.logger.info("Killing: %s" % (process_name))
        child = subprocess.Popen(['pgrep -f %s' % (process_name)], stdout=subprocess.PIPE, shell=True)
        result = child.communicate()[0]
        result = result.strip().split('\n')
        if result[0] == '':
            self.logger.info("Process name:%s not found" % (process_name))
            return 0
        for r in result:
            p = psutil.Process(int(r))
            p.terminate()
            self.logger.info("killed pid: %s" % (r))
        return 1

    def stopws(self):
        if self.options.wellcode is None or self.options.sensorcode is None:
            self.logger.error("Usage: python rts_bridge.py --wellcode <WellB> --sensorcode <Sensor1>")
            sys.exit(1)
        w = self.options.wellcode
        s = self.options.sensorcode
        process_name = 'rts_bridge.py.*-w.*%s.*-s.*%s' % (w, s)
        r = self.kill_pid(process_name)
        if r == 0:
            process_name = 'rts_bridge.py.*-s.*%s.*-w.*%s' % (s, w)
            r = self.kill_pid(process_name)
        return

    def stopserver(self):
        process_name = 'rts_manager.py'
        r = self.kill_pid(process_name)
        return


if __name__ == "__main__":
    user_obj = rts_bridge()
    getattr(user_obj, "%s" % user_obj.options.action)()

    # Discussion with Khalid on 7'th Sep 2021 at 11:55AM
    # --wellcode is referring to mongodb => Central.WELLS.WellCode
    # --sensorcode is referring to mongodb => Central.WELLS.Sensors.Code

    # getattr(obj, "bridge_%s_%s" %(obj.options.wellcode, obj.options.sensorcode))(obj.options.wellcode, obj.options.sensorcode)
    # getattr(obj, "bridge_%s" %('WellB'))(obj.options.wellcode, obj.options.sensorcode)
    # for i in range(1,3):
    #     p= Process(target=triggerWell, args=('Well_%d' %(i),'Sensor_%d' %(i),))
    #     p.start()
    # python rts_bridge.py --wellcode WellB --sensorcode Sensor1 --action gui_SignalTypes --data WHP,PI,PX
    # python rts_bridge.py --wellcode WellB --sensorcode Sensor1 --action gui_DenoiseSignalTypes --data WHP,PI,PX
    # python rts_bridge.py --wellcode WellB --sensorcode Sensor1 --action gui_MLSignalTypes --data WHP,PI,PX
    # python rts_bridge.py --wellcode WellB --sensorcode Sensor1 --action gui_OutlierSize --data 32
    # python rts_bridge.py --wellcode WellB --sensorcode Sensor1 --action gui_DenoiseSize --data 512,1024
    # python rts_bridge.py --wellcode WellB --sensorcode Sensor1 --action gui_MLSize --data 1024
    # python rts_bridge.py --wellcode WellB --sensorcode Sensor1 --action bridgews &
    # python rts_bridge.py --wellcode WellB --sensorcode Sensor2 --action bridgews &

# python playground/well_info.py --wellname R169
# python playground/reset.py --wellcode 61454f2b24f725bed5dbae5f [--rmcentralwellsid --debug]
# spark-submit rts_manager.py &
# python  rts_bridge.py -w 61454f2b24f725bed5dbae5f -s wellhead_pressure
