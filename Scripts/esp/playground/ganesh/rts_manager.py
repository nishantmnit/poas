#!/usr/bin/env python

import datetime
import os
import threading
from optparse import OptionParser

import pandas as pd
import psutil
import rpyc
from Scripts.esp.rts import rts
from pyspark import SparkConf  # SPARK
from pyspark import SparkContext  # SPARK
from rpyc.utils.server import ThreadedServer

import settings
from Lib.DBLib import getDbCon
from Lib.Utils import *
from Scripts.esp.core.storage.fetch_most import rts_m_insert_ifnotexist

os.environ['OPENBLAS_NUM_THREADS'] = '%s' % (psutil.cpu_count(logical=False))


def readOpts():
    parser = OptionParser(usage='usage: %prog [options]')
    parser.add_option("-s", "--service", dest="service", default="RTS", help="service Name (RTS/..)")
    parser.add_option("-c", "--configfile", dest="configfile",
                      default='%s/Config/JobScheduler.yml' % settings.POAS_ROOT, help="config file")
    (options, args) = parser.parse_args()
    if options.service is None:
        parser.error("service is required.")
        quit()
    return options


class Queue():
    def __init__(self):
        self.options = readOpts()
        self.logger = get_logger('rts_manager')
        self.config = self.setConfig()
        self.dateYmdHms = datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
        self.service = self.options.service
        self.RTSDb = getDbCon(self.options.service)  # default is RTS
        self.CenDb = getDbCon('Central')  # default is RTS

    def setConfig(self):
        ymlConfig = readConfig(self.options.configfile)
        if not definedInHash(ymlConfig, self.options.service):
            self.logger.error("Please set config for self service in config file first.")
            quit()
        else:
            return ymlConfig[self.options.service]


signalsForRawInterpolate, signalsForOutlier, signalsForDenoise = {}, {}, {}
rawInterpInProgress, outlierInProgress, denoiseInProgress, stftCwtKpcaInProgress, exFeaturesInProgress = {}, {}, {}, {}, {}
poasDateTimeFormat, sensorDateTimeFormats = '%d/%m/%Y %H:%M:%S', {}
signalTypes, denoiseSignalTypes, mlSignalTypes = {}, {}, {}
qaqcSize, rawInterpolatorSize, outlierSize, denoiseSize_p, denoiseSize_d, mlSize = {}, {}, {}, {}, {}, {}
well_info = {}
samplingFreqs = {}
lock = 0


def removeNoisePool(args):
    global poasDateTimeFormat
    service, collection, sensorid, column, size, status, updateStatus = args
    collsensorid = '%s_%s' % (collection, sensorid)
    obj = rts(service, collection, sensorid, poasDateTimeFormat)
    stat = obj.removeNoise(column, size, status, updateStatus)
    return stat


def stftCwtKpcaPool(args):
    global poasDateTimeFormat
    service, collection, sensorid, size_d, column, status = args
    collsensorid = '%s_%s' % (collection, sensorid)
    obj = rts(service, collection, sensorid, poasDateTimeFormat)
    stat = obj.stftCwtKpca(column, size_d, status)
    return stat


class rts_manager(rpyc.Service):
    def on_connect(self, conn):
        global lock
        self.logger = get_logger('rts_manager')
        lock = 1

    def on_disconnect(self, conn):
        global lock
        lock = 0

    def getDenoiseSignalTypes(self, service, collection, sensorid):
        global denoiseSignalTypes
        collsensorid = '%s_%s' % (collection, sensorid)
        return denoiseSignalTypes[collsensorid]

    def rawInterpolator(self, service, collection, sensorid):
        global rawInterpInProgress
        global qaqcSize
        global signalTypes
        global samplingFreqs
        global rawInterpolatorSize
        global sensorDateTimeFormats
        collsensorid = '%s_%s' % (collection, sensorid)
        _signalTypes = signalTypes[collsensorid]

        collsensorid = '%s_%s' % (collection, sensorid)
        if collsensorid not in rawInterpInProgress:
            rawInterpInProgress[collsensorid] = 0
        if rawInterpInProgress[collsensorid] == 0:
            rawInterpInProgress[collsensorid] = 1
            obj = rts(service, collection, sensorid)
            obj.rawInterpolateSignals(qaqcSize[collsensorid], rawInterpolatorSize[collsensorid], _signalTypes,
                                      samplingFreqs[collsensorid], sensorDateTimeFormats[collsensorid])
            rawInterpInProgress[collsensorid] = 0
        self.removeOutliers(service, collection,
                            sensorid)  # testing is pending hence commented, it's not in 1st milestone
        self.removeNoise(service, collection, sensorid)  # testing is pending hence commented, it's not in 1st milestone

    def removeOutliers(self, service, collection, sensorid):
        global outlierInProgress
        global poasDateTimeFormat
        global outlierSize
        collsensorid = '%s_%s' % (collection, sensorid)
        if collsensorid not in outlierInProgress:
            outlierInProgress[collsensorid] = 0
        if outlierInProgress[collsensorid] == 0:
            outlierInProgress[collsensorid] = 1
            denoiseSignalTypes = self.getDenoiseSignalTypes(service, collection, sensorid)
            obj = rts(service, collection, sensorid, poasDateTimeFormat)
            obj.interpolateSignal(outlierSize[collsensorid], denoiseSignalTypes)
            obj.removeOutliers(outlierSize[collsensorid], denoiseSignalTypes)
            outlierInProgress[collsensorid] = 0

    def isReadyForDenoise(self, service, collection, sensorid, status, size):
        denoiseSignalTypes = self.getDenoiseSignalTypes(service, collection, sensorid)
        db = queue.RTSDb
        if service == 'RTS':
            db = queue.RTSDb  # change db for other service
        coll = db[collection]
        flag = True
        for signalType in denoiseSignalTypes:
            count = coll.count({'%s_Status' % (signalType): {'$eq': status}})
            if count < size:
                self.logger.debug("Signal:%s_Status=%s, Count:%d is not ready for denoise, required count:%d" % (
                    signalType, status, count, size))
                flag = False
                break
        return flag

    def removeNoisePartialOrFinal(self, service, collection, sensorid, args):
        self.logger.info("Well:%s, SensorId:%s, rts_manager::removeNoise started for args:\n%s" % (
            collection, sensorid, pd.DataFrame(args)))
        conf = SparkConf().setAppName(collection).setExecutorEnv('PYTHONPATH', settings.POAS_ROOT)
        sc = SparkContext.getOrCreate(conf=conf)
        distData = sc.parallelize(args)
        results = distData.map(removeNoisePool).collect()
        df_results = []
        for i in range(len(results)): df_results.append(pd.DataFrame(results[i]))
        df_results = pd.concat(df_results, axis=1)
        pd.set_option('display.max_columns', None)
        pd.set_option('display.max_rows', None)
        pd.set_option('display.width', 200000)
        pd.set_option('display.max_colwidth', 200000)

        self.logger.info("Well:%s, SensorId:%s, rts_manager::removeNoise completed" % (collection, sensorid))
        for i in range(0, len(df_results.columns), 3):
            self.logger.info("\n%s" % (df_results.iloc[:, i:i + 3]))
        # sc.stop()                

    def removeNoise(self, service, collection, sensorid):
        global denoiseInProgress
        global denoiseSize_p
        global denoiseSize_d
        global samplingFreqs
        collsensorid = '%s_%s' % (collection, sensorid)
        if collsensorid not in denoiseInProgress: denoiseInProgress[collsensorid] = 0
        if denoiseInProgress[collsensorid] == 0:
            denoiseInProgress[collsensorid] = 1
            denoiseSignalTypes = self.getDenoiseSignalTypes(service, collection, sensorid)
            status = 'O'
            updateStatus = 'P'
            ranDenoise = False
            loopMe = True
            while loopMe:
                if denoiseSize_p[collsensorid] == denoiseSize_d[collsensorid]:
                    updateStatus = 'D'
                    if self.isReadyForDenoise(service, collection, sensorid, status, denoiseSize_d[collsensorid]):
                        args = [[service, collection, sensorid, [c], denoiseSize_d[collsensorid], status, updateStatus]
                                for c in denoiseSignalTypes]
                        # self.removeNoisePartialOrFinal(service, collection, sensorid, args) #removed SPARK
                        removeNoisePool(args[0])
                        ranDenoise = True
                else:
                    if self.isReadyForDenoise(service, collection, sensorid, status,
                                              denoiseSize_p[collsensorid]) and self.isReadyForDenoise(service,
                                                                                                      collection,
                                                                                                      sensorid, 'P',
                                                                                                      denoiseSize_p[
                                                                                                          collsensorid]):
                        args = [[service, collection, sensorid, [c], denoiseSize_d[collsensorid], 'P', 'D'] for c in
                                denoiseSignalTypes]
                        # self.removeNoisePartialOrFinal(service, collection, sensorid, args) #removed SPARK
                        removeNoisePool(args[0])
                        ranDenoise = True
                    if self.isReadyForDenoise(service, collection, sensorid, status, denoiseSize_p[collsensorid]):
                        args = [[service, collection, sensorid, [c], denoiseSize_p[collsensorid], status, updateStatus]
                                for c in denoiseSignalTypes]
                        removeNoisePool(args[0])
                        # self.removeNoisePartialOrFinal(service, collection, sensorid, args) #removed SPARK

                if ranDenoise:
                    self.logger.info(
                        "Well:%s, rts_manager::stftCwtKpca and extractFeaturesFromAll started" % (collection))
                    # t3= threading.Thread(target=self.stftCwtKpca, args=[service, collection, sensorid]) #removed SPARK
                    t3 = threading.Thread(target=stftCwtKpcaPool, args=[
                        [service, collection, sensorid, denoiseSize_d[collsensorid], [denoiseSignalTypes[0]], 'D']])
                    # t4= threading.Thread(target=self.extractFeaturesFromAll, args=[service, collection, sensorid])
                    t3.start()
                    # t4.start()
                    t3.join()
                    # t4.join()
                    self.logger.info(
                        "Well:%s, SensorId:%s, rts_manager::stftCwtKpca and extractFeaturesFromAll completed" % (
                            collection, sensorid))

                if samplingFreqs[collsensorid] <= 60:
                    loopMe = False
                else:
                    if self.isReadyForDenoise(service, collection, sensorid, status, denoiseSize_d[collsensorid]):
                        loopMe = True
                    else:
                        loopMe = False
            denoiseInProgress[collsensorid] = 0

    def stftCwtKpca(self, service, collection, sensorid):
        global denoiseSize_d
        collsensorid = '%s_%s' % (collection, sensorid)
        denoiseSignalTypes = self.getDenoiseSignalTypes(service, collection, sensorid)
        args = []
        for c in denoiseSignalTypes:
            args.append([service, collection, sensorid, denoiseSize_d[collsensorid], [c], 'D'])
        self.logger.info(
            "Well:%s, SensorId:%s, rts_manager1::stftCwtKpca started:\n%s" % (collection, sensorid, pd.DataFrame(args)))
        conf = SparkConf().setAppName(collection).setExecutorEnv('PYTHONPATH', settings.POAS_ROOT)
        sc = SparkContext.getOrCreate(conf=conf);
        distData = sc.parallelize(args)
        results = distData.map(stftCwtKpcaPool).collect()
        df_results = []
        for i in range(len(results)): df_results.append(pd.DataFrame(results[i]))
        df_results = pd.concat(df_results, axis=1)
        pd.set_option('display.max_columns', None)
        pd.set_option('display.max_rows', None)
        pd.set_option('display.width', 200000)
        pd.set_option('display.max_colwidth', 200000)
        self.logger.info("Well:%s, SensorId:%s, rts_manager1::stftCwtKpca completed" % (collection, sensorid))
        for i in range(0, len(df_results.columns), 3):
            self.logger.info("\n%s" % (df_results.iloc[:, i:i + 3]))
            # sc.stop()
        return 0

    def extractFeaturesFromAll(self, service, collection, sensorid):
        global poasDateTimeFormat
        global mlSize
        global mlSignalTypes
        self.logger.info("Well:%s, SensorId:%s, rts_manager::extractFeaturesFromAll started:%s" % (
            collection, sensorid, mlSignalTypes[collection]))
        collsensorid = '%s_%s' % (collection, sensorid)
        obj = rts(service, collection, sensorid, poasDateTimeFormat)
        obj.extractFeaturesFromAllDenoise(mlSize[collection], mlSignalTypes[collection], 'D')
        self.logger.info("Well:%s, SensorId:%s, rts_manager::extractFeaturesFromAll completed:%s" % (
            collection, sensorid, mlSignalTypes[collection]))
        return 0

    def datetimeToUTCtimestamp(self, datetime_str, dateformat):
        utc_timestamp = None
        try:
            d = datetime.datetime.strptime(str(datetime_str), dateformat)
            utc_timestamp = calendar.timegm(d.timetuple())
        except Exception as e:
            self.logger.error(
                "datetime format in excel is not same as Sensors.DateTimeFormat, datetimeToUTCtimestamp failed, Error:%s" % (
                    str(e)))
            self.logger.error("datetime_str:%s, dateformat:%s" % (datetime_str, dateformat))
            sys.exit(1)
        # datetime= datetime.datetime.utcfromtimestamp(utc_timestamp)
        return utc_timestamp

    def exposed_loop_outlier_denoise(self, service, collection, sensorid):
        # self.logger.info('Well:%s, SensorId:%s, outlier_denoise loop is in progess..' %(collection, sensorid))
        self.call_raw_interpolator(service, collection, sensorid)

    # def call_outlier_denoise(self, service, collection, sensorid, denoise=0):        
    #     t1= threading.Thread(target=self.rawInterpolator, args=[service, collection, sensorid])
    #     t1.start()
    #     t2= threading.Thread(target=self.removeOutliers, args=[service, collection, sensorid])
    #     t2.start()
    #     if denoise == 1:
    #         t3= threading.Thread(target=self.removeNoise, args=[service, collection, sensorid])
    #         t3.start()

    def call_raw_interpolator(self, service, collection, sensorid):
        t1 = threading.Thread(target=self.rawInterpolator, args=[service, collection, sensorid])
        t1.start()

    def exposed_push_raw(self, service, collection, sensorid, json):
        global sensorDateTimeFormats
        global signalsForRawInterpolate
        global signalsForDenoise
        global signalTypes
        global qaqcSize
        global samplingFreqs
        collsensorid = '%s_%s' % (collection, sensorid)
        _signalTypes = signalTypes[collsensorid]
        db = queue.RTSDb
        if service == 'RTS':  db = queue.RTSDb  # change db for other service
        coll = db[collection]

        where = dict()
        where['Property'] = 'Signal'
        where['SensorId'] = sensorid
        where['DateTime'] = self.datetimeToUTCtimestamp(json['DateTime'], sensorDateTimeFormats[collsensorid])

        data_dict = dict()
        data_dict.update(where)
        data_dict['DateTime_str'] = str(json['DateTime'])
        for k in _signalTypes: data_dict[k] = '-' if math.isnan(json[k]) else json[k]
        # for k in _signalTypes: data_dict[k]= '-' if ((isinstance(json[k], basestring) and json[k].strip() == '') or (math.isnan(json[k]))) else json[k]      
        for c in _signalTypes: data_dict["%s_Status" % (c)] = 'A'
        for c in _signalTypes: data_dict["%s_FEStatus" % (c)] = 'A'
        for c in _signalTypes: data_dict["%s_QaQcStatus" % (c)] = 'A'
        for c in _signalTypes: data_dict["%s_POASRemark" % (c)] = ''
        for c in _signalTypes: data_dict["%s_SignalQuality" % (c)] = ''

        data_dict['DateTime'] = where['DateTime']

        _id = rts_m_insert_ifnotexist(coll, where, data_dict)
        # self.logger.info('rts_manager: %s' %(_id))
        if 'upserted' not in _id.keys():
            # self.logger.warn('Skipped record, Property:Signal, SensorId:%s, DateTime:%s' %(data_dict['SensorId'], data_dict['DateTime']))
            return
        data_dict['_id'] = _id['upserted']

        self.logger.debug("push_raw samplingFreqs:%s, signalTypes:%s" % (samplingFreqs, signalTypes))

        if collsensorid not in signalsForRawInterpolate: signalsForRawInterpolate[
            collsensorid] = 0  # TODO in all cases:: replace 0 by actual counts in mongodb
        signalsForRawInterpolate[collsensorid] += 1

        is_ms = True
        if float(samplingFreqs[collsensorid]) > 60: is_ms = False

        if is_ms and signalsForRawInterpolate[collsensorid] % qaqcSize[collsensorid] == 0:
            self.logger.debug(
                "MS call_raw_interpolator is in progess for %d records" % (signalsForRawInterpolate[collsensorid]))
            self.call_raw_interpolator(service, collection, sensorid)
            signalsForRawInterpolate[collsensorid] = 0
        else:
            self.logger.debug("Hours/Day call_raw_interpolator is in progess")
            self.call_raw_interpolator(service, collection, sensorid)
        return _id

    def exposed_push(self, service, collection, sensorid, json):
        global poasDateTimeFormat
        global signalsForOutlier
        global signalsForDenoise
        global signalTypes
        global outlierSize
        collsensorid = '%s_%s' % (collection, sensorid)
        signalColumns = signalTypes[collsensorid]
        denoiseSignalTypes = self.getDenoiseSignalTypes(service, collection, sensorid)

        for k in signalColumns:
            if k not in json:
                self.logger.error("Error: missing mandatory signal fields mapping in 'json' from RTSBridge:%s" % (k))
                exit(1)
        db = queue.RTSDb
        if service == 'RTS':  db = queue.RTSDb  # change db for other service
        coll = db[collection]

        where = dict()
        where['Property'] = 'Signal'
        where['SensorId'] = sensorid
        where['DateTime'] = self.datetimeToUTCtimestamp(json['DateTime'], poasDateTimeFormat)

        data_dict = dict()
        data_dict.update(where)
        data_dict['DateTime_str'] = json['DateTime']
        for k in signalColumns: data_dict[k] = '-' if json[k].strip() == '' else json[k].strip()
        for c in denoiseSignalTypes: data_dict["%s_Status" % (c)] = 'Y'
        for c in denoiseSignalTypes: data_dict["%s_FEStatus" % (c)] = 'Y'
        data_dict['DateTime'] = where['DateTime']
        _id = rts_m_insert_ifnotexist(coll, where, data_dict)
        if 'upserted' not in _id.keys():
            # self.logger.warn('Skipped record, Property:Signal, SensorId:%s, DateTime:%s' %(data_dict['SensorId'], data_dict['DateTime']))
            return
        data_dict['_id'] = _id['upserted']
        if collsensorid not in signalsForOutlier: signalsForOutlier[collsensorid] = 0
        signalsForOutlier[collsensorid] += 1

        if signalsForOutlier[collsensorid] % outlierSize[collsensorid] == 0:
            self.call_outlier_denoise(service, collection, sensorid, 1)
            signalsForOutlier[collsensorid] = 0
        return data_dict

    def exposed_refreshwellsdata(self, service, wellcode, sensorcode, w):
        global sensorDateTimeFormats
        global signalTypes, denoiseSignalTypes, mlSignalTypes
        global qaqcSize, outlierSize, denoiseSize_p, denoiseSize_d, mlSize, rawInterpolatorSize
        global samplingFreqs, well_info
        st = None
        sf, st, df = None, [], None
        for s in w['sensors']:
            if s['code'] != sensorcode: continue
            df = s['sensor_datetime_format']
            sf = sorted(s['data_sampling_frequency'], key=lambda k: k['updated_at'])[-1]['seconds']
            st.append(s['code'])  # only one sensor code appended
        collsensorid = '%s_%s' % (w['Collection'], sensorcode)
        samplingFreqs[collsensorid] = sf
        signalTypes[collsensorid] = st
        denoiseSignalTypes[collsensorid] = st
        mlSignalTypes[wellcode] = st  # its per well
        sensorDateTimeFormats[collsensorid] = df
        qaqcSize[collsensorid] = 32 if sf <= 60 else 5
        rawInterpolatorSize[collsensorid] = 12  # used in HD only
        outlierSize[collsensorid] = 32
        denoiseSize_p[collsensorid] = 512
        denoiseSize_d[collsensorid] = 512
        mlSize[wellcode] = 512
        self.logger.info("refreshwells data samplingFreqs:%s, signalTypes:%s" % (samplingFreqs, signalTypes))
        return st, sf


if __name__ == "__main__":
    queue = Queue()
    server = ThreadedServer(rts_manager, port=queue.config['port'])
    server.start()
