#!/usr/bin/env python
import argparse

from bson.objectid import ObjectId

import settings_play
from Lib.DBLib import *
from Lib.Utils import *
from Scripts.esp.core.storage.fetch_most import rts_b_wells_data, rts_m_insert_ifnotexist


class load_wells(object):
    def __init__(this):
        this.getoptions()
        this.service= this.args.service
        this.wellcode= this.args.wellcode
        this.logger= get_logger('rts')
        this.CenDb= getDbCon('Central') #default is RTS
        this.well_info= this.get_wellsdata()
        this.db= getDbCon(this.service)
        this.coll= this.db[this.well_info['Collection']] 
        this.dateYmdHms= datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
        
    def getoptions(this):
        parser = argparse.ArgumentParser()
        parser.add_argument("--service",     dest='service',     default="RTS_TEST",     help='RTS_TEST')
        parser.add_argument("--wellcode",    dest='wellcode',    default='6142da6d24f7250877831763',  help='Well Mongo id of R169')
        parser.add_argument("--cleanup",     dest='cleanup',     default=False,          help='Drop RTS_TEST.<wellcode>', action='store_true')
        this.args = parser.parse_args()

    def get_sensor_details(this):
        sensors= dict()
        for s in this.well_info['sensors']:
            if s['code'] in sensors.keys():
                this.logger.error('duplicate sensorcode:%s found in Central.WELLS' %(s['code']))
                sys.exit(1)
            sensors[s['code']]= s
        return sensors

    def createCollIfNotExists(this, db, collName):
        if collName not in db.collection_names(): db.create_collection(collName)
        coll= db[collName] 
        return coll       

    def get_wellsdata(this):
        coll= this.createCollIfNotExists(this.CenDb, 'WELLS')
        wells_info= rts_b_wells_data(coll, this.wellcode)
        for w in wells_info:
            if w['_id'] == ObjectId(this.wellcode): 
                return w
        this.logger.error('%s is not setup in Central.WELL database!' %(this.wellcode))
        sys.exit(1)

    def trimDfHeaders(this, df):
        renameCol = {}
        for col in df.columns:
            c = col.strip()
            renameCol[col] = c.lower() 
        df.rename(columns= renameCol, inplace=True)
        return df

    def datetimeToUTCtimestamp(this, datetime_str, dateformat):
        utc_timestamp= None
        try:
            d= datetime.datetime.strptime(str(datetime_str), dateformat) 
            utc_timestamp= calendar.timegm(d.timetuple())
        except Exception as e:
            this.logger.error("datetime format in excel is not same as Sensors.DateTimeFormat, datetimeToUTCtimestamp failed, Error:%s" %(str(e)))
            this.logger.error("datetime_str:%s, dateformat:%s" %(datetime_str, dateformat))
            sys.exit(1)
        #datetime= datetime.datetime.utcfromtimestamp(utc_timestamp)
        return utc_timestamp

    def drop_well_in_RTS_TEST(this):
        if this.args.cleanup:
            this.coll.drop()
            this.logger.info("%s is dropped from %s database" %(this.well_info['Collection'], this.service))

    def main(this):
        this.logger.info('Processing of WellName:%s is started in %s.%s' %(this.well_info['WellName'], this.service, this.well_info['Collection']))        
        sensors= this.get_sensor_details()
        wellPath= os.path.join(settings_play.POAS_ROOT, 'Scripts', 'esp', 'test', 'golden_data', '%s.xlsx' %(this.well_info['WellName']))
	this.logger.info(wellPath)
        if not os.path.isfile(wellPath):
            this.logger.error("%s not found!" %(wellPath))
            sys.exit(1)
        this.drop_well_in_RTS_TEST()
        sensorcodes= sensors.keys()
        for sensorcode in sensorcodes:
            sensorDateTimeFormat= sensors[sensorcode]['sensor_datetime_format']
            sensorname= sensors[sensorcode]['sensor_name']
            this.logger.info("processing sheet:%s" %(sensorname))    

            #if sensorname.lower() != 'esp_frequency': continue
            xls = pd.ExcelFile(wellPath)
            sheet_names= [x for x in xls.sheet_names]
            sheet_names_lower= [x.lower() for x in sheet_names]
            
            if sensorname.lower() not in sheet_names_lower:
                this.logger.error("%s tab not found in well:%s" %(sensorname, wellPath))
                sys.exit()
            sensorname= sheet_names[sheet_names_lower.index(sensorname.lower())]
            df= this.trimDfHeaders(pd.read_excel(xls, sheet_name=sensorname, encoding=sys.getfilesystemencoding(), parse_cols="C,D,E,F,G,H,I,J,K", skiprows=[0,1]))
            where= dict()
            where['Property']= 'Signal' 
            where['SensorId']= sensorcode


            i =0
            for index, row in df.iterrows():
                this.logger.debug("processing sheet:%s, count:%d" %(sensorname, index))
                json= dict()
                d=row.to_dict()
                where['DateTime']= this.datetimeToUTCtimestamp(d['x_axis'], sensorDateTimeFormat)
                data_dict= dict()
                data_dict.update(where)
                data_dict.update(d)
                _id= rts_m_insert_ifnotexist(this.coll, where, data_dict)
                if 'upserted' not in _id.keys(): 
                    this.logger.debug('Duplicate record found, SensorId:%s, DateTime:%s' %(data_dict['SensorId'], d['x_axis']))
                else:
                    this.logger.debug('Record loaded, SensorId:%s, DateTime:%s, %s' %(data_dict['SensorId'], d['x_axis'], _id))
                #if i == 300: sys.exit(0)
                i+=1

if __name__ == "__main__":
    ob=load_wells()
    ob.main()

#python playground/well_info.py --wellname R169
#python playground/load_wells.py --service RTS_TEST --wellcode 61454f2b24f725bed5dbae5f [--cleanup]

