import os

import numpy as np
import pandas as pd

from Scripts.esp.modules.denoise import de_noise
from Scripts.pvt.core.storage.excel import write_excel


def save_to_debug_excel(output):
    signal = output["input_signal"]
    df = pd.DataFrame(columns=["signal"], data=signal)

    detail_bins = output["input_detail_bins"]
    lns = []
    for key, value in detail_bins.items():
        lns.append(len(value))
    max_ln = max(lns)
    for key, value in detail_bins.items():
        ava = len(value)
        if max_ln > ava:
            value = np.pad(value, (0, max_ln - ava), 'constant')
        detail_bins[key] = value

    approx_bins = output["input_approx_bins"]
    lns = []
    for key, value in approx_bins.items():
        lns.append(len(value))
    max_ln = max(lns)
    for key, value in approx_bins.items():
        ava = len(value)
        if max_ln > ava:
            value = np.pad(value, (0, max_ln - ava), 'constant')
        approx_bins[key] = value
    detail = pd.DataFrame(detail_bins)
    approx = pd.DataFrame(approx_bins)

    out_detail_bins = output["output_detail_bins"]
    lns = []
    for key, value in detail_bins.items():
        lns.append(len(value))
    max_ln = max(lns)
    for key, value in detail_bins.items():
        ava = len(value)
        if max_ln > ava:
            value = np.pad(value, (0, max_ln - ava), 'constant')
        out_detail_bins[key] = value

    out_approx_bins = output["output_approx_bins"]
    lns = []
    for key, value in approx_bins.items():
        lns.append(len(value))
    max_ln = max(lns)
    for key, value in approx_bins.items():
        ava = len(value)
        if max_ln > ava:
            value = np.pad(value, (0, max_ln - ava), 'constant')
        out_approx_bins[key] = value
    out_detail = pd.DataFrame(out_detail_bins)
    out_approx = pd.DataFrame(out_approx_bins)

    out_signal = output["output_signal"]
    out_df = pd.DataFrame(columns=["signal"], data=out_signal)

    level_detailed_coefficients = output["level_detailed_coefficients"]
    level_detailed_coefficients = pd.DataFrame(level_detailed_coefficients)

    kurtosis = pd.DataFrame(output["kurtosis"])

    write_excel(True, os.path.join(".", "debug"), ["db4"],
                ["input", "output", "input_detail_bins", "input_approx_bins", "out_detail_bins", "out_approx_bins",
                 "level_detailed_coefficients", "kurtosis"],
                [df, out_df, detail, approx, out_detail, out_approx, level_detailed_coefficients, kurtosis])


if __name__ == "__main__":
    from Scripts.esp.playground.nishant.test_signal.signals import noisy_signal2

    test_signal = noisy_signal2  # [i for i in range(256)]
    test_df = pd.DataFrame(columns=["x_axis", "y_axis"], data=list(zip(test_signal, test_signal)))
    test_output = de_noise(test_df)
    save_to_debug_excel(test_output)
