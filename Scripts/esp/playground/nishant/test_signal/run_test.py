import signals as test_signals


def create_node_df(data):
    data = {x: data[x] for x in data if x not in ["nodes"]}
    return pd.DataFrame(data)


for signal in ["signal1", "signal2", "noisy_signal1", "noisy_signal2"]:
    test_signal = getattr(test_signals, signal)
    test_df = pd.DataFrame(columns=["y_axis", "x_axis"],
                           data=list(zip(test_signal, [i for i in range(len(test_signal))])))
    test_output = de_noise(test_df)
    signal_df = pd.DataFrame(columns=["input", "output"],
                             data=list(zip(test_output["input_signal"], test_output["output_signal"])))

    signal_properties = pd.DataFrame(test_output["signal_properties"], index=[0])

    max_len = len(test_output["input_detail_bins"][test_output["input_detail_bins"]["nodes"][0]])
    for node in test_output["input_detail_bins"]["nodes"]:
        node_len = len(test_output["input_detail_bins"][node])
        if node_len < max_len:
            test_output["input_detail_bins"][node] = np.pad(test_output["input_detail_bins"][node],
                                                            (0, max_len - node_len), "constant")
            test_output["output_detail_bins"][node] = np.pad(test_output["output_detail_bins"][node],
                                                             (0, max_len - node_len), "constant")

    for node in test_output["input_approx_bins"]["nodes"]:
        node_len = len(test_output["input_approx_bins"][node])
        if node_len < max_len:
            test_output["input_approx_bins"][node] = np.pad(test_output["input_approx_bins"][node],
                                                            (0, max_len - node_len), "constant")
            test_output["output_approx_bins"][node] = np.pad(test_output["output_approx_bins"][node],
                                                             (0, max_len - node_len), "constant")

    input_detail_bins_df = create_node_df(test_output["input_detail_bins"])
    output_detail_bins_df = create_node_df(test_output["output_detail_bins"])

    input_detail_energy_bins_df = pd.DataFrame(test_output["input_detail_energy_bins"], index=[0])
    output_detail_energy_bins_df = pd.DataFrame(test_output["output_detail_energy_bins"], index=[0])

    input_approx_bins_df = create_node_df(test_output["input_approx_bins"])
    output_approx_bins_df = create_node_df(test_output["output_approx_bins"])

    input_approx_energy_bins_df = pd.DataFrame(test_output["input_approx_energy_bins"], index=[0])
    output_approx_energy_bins_df = pd.DataFrame(test_output["output_approx_energy_bins"], index=[0])

    from Scripts.pvt.core.storage.excel import write_excel

    write_excel(True, os.path.join(".", "debug"), [signal],
                ["signal", "signal_properties", "input_detail_bins", "output_detail_bins",
                 "input_detail_energy_bins", "output_detail_energy_bins",
                 "input_approx_bins", "output_approx_bins",
                 "input_approx_energy_bins", "output_approx_energy_bins"],
                [signal_df, signal_properties, input_detail_bins_df, output_detail_bins_df,
                 input_detail_energy_bins_df, output_detail_energy_bins_df,
                 input_approx_bins_df, output_approx_bins_df,
                 input_approx_energy_bins_df, output_approx_energy_bins_df])
