import argparse
import re

import pandas as pd

from Scripts.esp.core.get_logger import get_logger
from Scripts.esp.core.storage.fetch_most import get_sensor_metadata
from Scripts.esp.core.storage.mongo import (
    write_bulk_signal,
    clear_signal,
    getDbCon,
    insert,
    delete
)

logger = get_logger("create sensors")


def read_options():
    parser = argparse.ArgumentParser()
    parser.add_argument('--excel', dest='excel', help='Provide the excel file name with location.')

    args = parser.parse_args()
    if not args.excel:
        print "Please provide excel file name to import."
        quit()
    logger.info("Loading %s excel in 'RTS' DB and streaming data." % args.excel)
    return args


def fix_header(df):
    old_columns = df.columns.tolist()
    new_columns = [x.strip() for x in old_columns]
    new_columns = [re.sub(r"[ -.]", "_", x.lower()) for x in new_columns]
    new_columns_bkp = new_columns[:]
    df.fillna("missing", inplace=True)
    sub_header = df.loc[df.index == 0].values.tolist()[0]
    sub_header = [re.sub(r"[ -.]", "_", x.lower()) for x in sub_header]

    for i in range(len(new_columns)):
        col = new_columns[i]
        sub_col = sub_header[i]
        if sub_col != "missing":
            if col.startswith("unnamed"):
                prev_col = [x for x in new_columns_bkp[0:i] if not x.startswith("unnamed")][-1]
                col = "%s_%s" % (prev_col, sub_col)
            else:
                col = "%s_%s" % (col, sub_col)
        new_columns[i] = col
    return new_columns


def wells(excel_file):
    logger.info("Extracting well details")
    df = pd.read_excel(excel_file, sheet_name="Well List", skiprows=1)
    sub_header = df.loc[df.index == 0]
    df.drop([0], inplace=True)
    new_columns = fix_header(sub_header)
    df = df.rename(columns=dict(zip(df.columns.tolist(), new_columns)))
    df["display_well_name"] = df.well_name
    df["present_status"] = "open"
    for index, row in df.iterrows():
        conn = getDbCon("Central", "wells")
        delete(conn, well_name=row.well_name)
        insert(conn, row.to_dict())
    return df


def sensors(excel_file, well_name):
    logger.info("Extracting sensor details")
    df = pd.read_excel(excel_file, sheet_name="Sensors", skiprows=2)
    sub_header = df.loc[df.index == 0]
    df.drop([0], inplace=True)
    new_columns = fix_header(sub_header)
    df = df.rename(columns=dict(zip(df.columns.tolist(), new_columns)))
    df = df.loc[df.sensor_status.str.lower() == "active"]
    df.reset_index(inplace=True)
    df["read_method"] = "Internal"
    # df["sensor_status"] = "active"
    df["display_gauge_name"] = df.gauge_name
    df["display_sensor_name"] = df.sensor_name
    df["last_read_time"] = 0
    df["pid"] = 0
    df["read_status"] = "inactive"
    df["sensor_prediction"] = None
    df["well_name"] = well_name
    df["datetime_format"] = "%Y-%m-%d %H:%M:%S.%f"
    df["seq"] = range(0, df.shape[0])
    conn = getDbCon("Central", "sensors")
    delete(conn, well_name=well_name)
    for index, row in df.iterrows():
        insert(conn, row.to_dict())
    return df


def sheet_index(excel_file, sheet_name):
    excel_file = pd.ExcelFile(excel_file)
    sheet_names = excel_file.sheet_names
    sheet_names = [name.lower() for name in sheet_names]
    index = sheet_names.index(sheet_name.lower())
    return index


def push_data_in_sensor(excel_file, well_name, gauge_name, sensor_name):
    logger.info(
        "Creating sensor in sensors DB for well = %s, gauge = %s, sensor = %s" % (well_name, gauge_name, sensor_name))
    conn = getDbCon("Central", "sensors")
    sensor = get_sensor_metadata(conn, well_name, gauge_name, sensor_name)
    logger.info("Reading gauge == %s | sensor == %s data" % (gauge_name, sensor_name))
    df = pd.read_excel(excel_file, sheet_name=sheet_index(excel_file, sensor_name), skiprows=2)[
        ["x_axis", "y_axis", "z_axis"]]
    df.rename(columns={"x_axis": sensor.x_axis, "y_axis": sensor.y_axis, "z_axis": sensor.z_axis}, inplace=True)
    df[sensor.x_axis] = df.apply(lambda row: str(row[sensor.x_axis]).split(".")[0], axis=1)
    clear_signal(db="sensors", well=well_name, sensor_name=sensor_name, gauge_name=gauge_name)
    write_bulk_signal(df, db="sensors", well=well_name, sensor_name=sensor_name, gauge_name=gauge_name)


def save_data(excel_file):
    well_df = wells(excel_file)
    well_name = well_df.well_name.values[0]
    logger.info("Well Name == %s" % well_name)
    sensors_df = sensors(excel_file, well_name)
    for index, row in sensors_df.iterrows():
        push_data_in_sensor(excel_file, well_name, row.gauge_name, row.sensor_name)


if __name__ == "__main__":
    user_args = read_options()
    save_data(user_args.excel)
