import argparse
import datetime
import json
import os
import random
import time
from itertools import product
from multiprocessing import Pool

import pandas as pd

from Scripts.esp.core.get_logger import get_logger
import Scripts.esp.settings as settings
from Scripts.esp.core.storage.fetch_most import get_sensor_metadata
from Scripts.esp.core.storage.mongo import (
    fetch_bulk_signal,
    getDbCon,
    fetch,
)

logger = get_logger("create sensors")
miss_signals = False

sensors_conn = getDbCon("Central", "sensors")


def read_options():
    parser = argparse.ArgumentParser()
    parser.add_argument('--well', dest='well', default=None, help='Well collection name MC/MC1/MC2')
    parser.add_argument('--miss_signals', dest='miss_signals', default=False, action='store_true',
                        help='Replicate scenario of missing signals and same signals from sensor')
    parser.add_argument('--infinity', dest='infinity', default=False, action='store_true',
                        help='Create an endless data stream')
    args = parser.parse_args()
    if not args.well:
        print "Please provide well name to create sensors for."
        quit()
    global miss_signals
    miss_signals = args.miss_signals
    return args


def relay_next(sleep_time, last_push_time):
    next_push_time = float(last_push_time + sleep_time)
    timestamp_now = float(time.time())
    if timestamp_now >= next_push_time:
        return True
    return False


def get_sleep_time(well_name, gauge_name, sensor_name, ):
    sensor = get_sensor_metadata(sensors_conn, well_name, gauge_name, sensor_name)
    frequency, frequency_unit = sensor.data_sampling_frequency, sensor.data_sampling_frequency_unit
    frequency = float(frequency)
    if frequency_unit.lower() == "hour":
        sleep_time = 60 * 60 * frequency
    elif frequency_unit.lower() == "minute":
        sleep_time = 60 * frequency
    elif frequency_unit.lower() == "day":
        sleep_time = 24 * 60 * 60 * frequency
    else:
        sleep_time = frequency
    return sleep_time


def emit_signal(row, sensor, well_name, gauge_name, sensor_name):
    sensor_file = os.path.join(os.path.join(settings.ESP_DIR, "playground", "sensors", well_name, gauge_name),
                               "%s.json" % sensor_name)
    if miss_signals and random.randint(0, 5) == 5:
        logger.debug("Missing this signal.")
        logger.debug("Emptying the sensor.")
        open(sensor_file, "w").close()
    else:
        row[sensor.x_axis] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f")
        with open(sensor_file, "w") as fh:
            logger.debug("Signal == %s" % row.to_dict())
            fh.write(json.dumps(row.to_dict(), indent=4, sort_keys=True, default=str))


def clear_and_ensure_dir(well_name, gauge_name, sensor_name):
    sensor = os.path.join(settings.ESP_DIR, "playground", "sensors", well_name, gauge_name)
    if not os.path.exists(sensor):
        try:
            os.makedirs(sensor)
        except Exception:
            pass
    sensor_file = os.path.join(sensor, "%s.json" % sensor_name)
    if os.path.exists(sensor_file):
        try:
            os.remove(sensor_file)
        except Exception:
            pass


def add_error(data):
    error = random.randint(-20, 20)
    data = data + data*error/100
    return data


def push_signals(args, infinity=None, max_seq=None):
    well_name, gauge_sensor_name = args[0], args[1]
    gauge_name, sensor_name = gauge_sensor_name[0], gauge_sensor_name[1]
    logger.debug("Reading gauge == %s | sensor == %s data" % (gauge_name, sensor_name))
    clear_and_ensure_dir(well_name, gauge_name, sensor_name)
    df = fetch_bulk_signal(db="sensors", well=well_name, gauge_name=gauge_name, sensor_name=sensor_name)
    sensor = get_sensor_metadata(sensors_conn, well_name, gauge_name, sensor_name)
    sleep_time = get_sleep_time(well_name, gauge_name, sensor_name)

    if infinity is not None:
        df["seq"] = df.seq + max_seq
        # introduce error
        df[sensor.y_axis] = df.apply(lambda row_err: add_error(row_err[sensor.y_axis]), axis=1)
    for index, row in df.iterrows():
        emit_signal(row, sensor, well_name, gauge_name, sensor_name)
        last_push_time = float(time.time())
        while not relay_next(sleep_time, last_push_time):
            time.sleep(sleep_time)

    if user_args.infinity:
        push_signals(args, infinity=True, max_seq=df.seq.max()+1)


def stream_data(well_name):
    sensors_df = pd.DataFrame(fetch(sensors_conn, {"well_name": well_name}))
    sensors_df = sensors_df.loc[sensors_df.sensor_status.str.lower() == "active"]
    cartesian = [i for i in
                 product(*[[well_name], list(zip(sensors_df.gauge_name.tolist(), sensors_df.sensor_name.tolist()))])]
    pool = Pool(processes=len(sensors_df.sensor_name.tolist()))
    pool.map(push_signals, cartesian)
    pool.close()


if __name__ == "__main__":
    user_args = read_options()
    stream_data(user_args.well)
