import importlib
import os

import pandas as pd

from Scripts.esp.modules.stft.stft import stft
from Scripts.esp.settings import DEBUG_DIR
from Scripts.pvt.core.send_email import send_email
from Scripts.pvt.core.storage.excel import write_csv


def run_test(signal_name, sinusoidal, sampling):
    test_signals = importlib.import_module("Scripts.esp.playground.nishant.test_signal.signals")
    test_signal = getattr(test_signals, signal_name)

    if len(test_signal) < 512 and sampling <= 0.5:
        test_signal = test_signal + test_signal

    test_x_axis = [i * sampling for i in range(len(test_signal))]
    test_signal_df = pd.DataFrame(data=list(zip(test_signal, test_x_axis)), columns=["y_axis", "x_axis"])
    test_sensor = {"data_sampling_frequency": sampling, "data_sampling_frequency_unit": "second",
                   "signal_character": sinusoidal, "frequency": 50}
    test_sensor = pd.Series(test_sensor)
    test_output = stft(test_signal_df, test_sensor)
    test_df = pd.DataFrame(test_output).sort_values(by=["seq"]).reset_index(drop=True)

    save_dir = os.path.join(DEBUG_DIR, signal_name)
    write_csv(debug=True, save_to_dir=save_dir, filename=["output_from_%s_input_sampling_time_%s" % (sampling, sinusoidal)],
              df=test_df,
              index=False)

    write_csv(debug=True, save_to_dir=save_dir, filename=["input_signal"],
              df=test_signal_df,
              index=False)


if __name__ == "__main__":
    import socket

    intervals = [10 ** -3, 0.005, 0.01, 0.05, 0.1, 0.5, 1] + [int(i * 5) for i in range(1, 13)]
    signals = ["signal1", "signal2", "noisy_signal1", "noisy_signal2", "stft_test_signal", "i_motor_denoised"]
    characters = ["sinusoidal", "non-sinusoidal"]

    body = "Test has been started for following signals.\n"
    for signal in signals:
        for character in characters:
            for interval in intervals:
                body += "%s - %s - %s\n" % (signal, character, interval)
    body += "Thanks,\nPOAS Automation"
    send_email(subject="POAS ESP STFT TEST ALERT", body=body)

    for signal in signals:
        for character in characters:
            for interval in intervals:
                print ("Executing for signal - %s, character - %s, interval - %s " % (signal, character, interval))
                run_test(signal, character, interval)

    send_email(subject="POAS ESP STFT TEST ALERT", body="STFT Test has completed on %s." % socket.gethostname())
