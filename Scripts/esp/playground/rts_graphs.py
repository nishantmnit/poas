#matplotlib.use('TkAgg')
import matplotlib.dates as dates
import matplotlib.ticker as ticker
import numpy as np
from matplotlib import cm
from matplotlib import colors as mcolors
from matplotlib.collections import PolyCollection
from scipy.interpolate import make_interp_spline, BSpline

# import settings
from Lib.GraphLib import *


class rts_graphs(object):
    def __init__(this, collection= None, logger=None):
        this.logger= logger
        this.collection=collection
        pass

    def format_date(this, x, pos=None):
         return dates.num2date(x).strftime('%H:%M:%S')
        
    def denoiseTimeFreqEnergy3D(this, time, frequencies, coefs, Identifier, signaltype, title, xlabel, ylabel, zlabel):
        frequencies= np.array(frequencies[0][1:])
        time= np.array([t[-1] for t in time ])
        coefs= np.array([c[1:] for c in coefs]).transpose()
        # print time.shape
        # print coefs.shape
        # print frequencies.shape
        graphPath= os.path.join(getPOASRootDir(), 'Graphs', this.collection, signaltype, '%s_3D_Full.png' %(title))
        if not os.path.exists(os.path.dirname(graphPath)):
            os.makedirs(os.path.dirname(graphPath))
        plt.style.use('dark_background')
        fig = plt.figure(figsize=(10,5))
        ax = fig.gca(projection='3d')
        #ax.set_facecolor('k')
        ax.set_title(title, color='white')
        #ax.xaxis.label.set_color('white')
        #ax.yaxis.label.set_color('white')
        #ax.zaxis.label.set_color('white')
        ax.tick_params(axis='x')
        ax.tick_params(axis='y')
        ax.tick_params(axis='z')
        #t= time
        #temp, t = np.meshgrid(np.arange(0, len(time)), time)
        t= np.tile(time, (len(coefs), 1))
        cc= coefs
        temp, f = np.meshgrid(np.arange(0, len(coefs[0])), frequencies)
        #R = np.sqrt(X**2 + Y**2)
        #Z = np.sin(R)
        # Plot the surface.
        # t= np.transpose(t)
        # f= np.transpose(f)
        # cc= np.transpose(cc)

        surf = ax.plot_surface(t, f, cc, cmap=cm.jet, linewidth=1, antialiased=False)
        #surf= ax.plot_wireframe(t, f, c, rstride=10, cstride=10)
        # Customize the z axis.
        #ax.set_zlim(min(frequencies), max(frequencies))
        #ax.set_zlim(-1.01, 1.1)
        #ax.zaxis.set_major_locator(LinearLocator(10))
        #ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
        ax.set_xlabel('X-Time')
        ax.set_ylabel('Y-Freq')
        ax.set_zlabel('Z-Energy')
        #plt.yscale('symlog')
        #plt.yscale('log')
        # Add a color bar which maps values to colors.
        fig.colorbar(surf, shrink=0.5, aspect=10)
        ax.view_init(elev=16, azim=-154)     
        plt.tight_layout(pad=1.7)

        fig.savefig(graphPath, dpi=fig.dpi)
        plt.show()
        plt.cla()
        plt.close(fig)
        this.printSTDOUTMsg(Identifier, graphPath)

    def denoiseFreqVsEnergy2DScatter(this, time, freqs, energies, Identifier, signalType, title, xlabel, ylabel):
        plt.style.use('dark_background')
        fig, ax = plt.subplots()
        labelS=len(time)
        for d in range(len(time)):
            plt.scatter(freqs[d][1:], energies[d][1:])        
            if labelS <= 5:
                plt.plot(freqs[d][1:], energies[d][1:], label='%s' %(time[d][-1]))        
            else:
                plt.plot(freqs[d][1:], energies[d][1:])        
            labelS -= 1
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        plt.title(title)
        plt.legend()
        plt.tight_layout(pad=1.7)
        #plt.show()
        graphPath= os.path.join(getPOASRootDir(), 'Graphs', this.collection, signalType, '%s_2D_Full.png' %(title))
        if not os.path.exists(os.path.dirname(graphPath)):
            os.makedirs(os.path.dirname(graphPath))        
        fig.savefig(graphPath, dpi=fig.dpi)
        plt.cla()
        plt.close(fig)
        this.printSTDOUTMsg(Identifier, graphPath)

    def denoiseFreqVsEnergy2DBar(this, ngroups, energies, energiesf, freqLables, Identifier, signalType):
        plt.rcParams["figure.figsize"] = (8,4)
        means_frank = energies[1:]
        means_guido = energiesf[1:]

        # create plot
        fig, ax = plt.subplots()
        index = np.arange(ngroups)
        bar_width = 0.3
        opacity = 0.7

        rects1 = plt.bar(index, means_frank, bar_width, alpha=opacity, color='b', label='Energy')

        rects2 = plt.bar(index + bar_width, means_guido, bar_width, alpha=opacity, color='g', label='Filtered Energy')

        plt.xlabel('Frequency')
        plt.ylabel('Energy')
        plt.title('Energy by Frequency')
        plt.xticks(index + bar_width, freqLables[1:])
        plt.legend()
        plt.tight_layout(pad=1.7)
        plt.xticks(rotation=90, fontsize=7)
        plt.yticks(fontsize=7)
        #plt.show()
        graphPath= os.path.join(getPOASRootDir(), 'Graphs', this.collection, signalType, "denoiseFreqVsEnergy2DBar.png")
        if not os.path.exists(os.path.dirname(graphPath)):
            os.makedirs(os.path.dirname(graphPath))        
        fig.savefig(graphPath, dpi=fig.dpi)
        plt.cla()
        plt.close(fig)
        this.printSTDOUTMsg(Identifier, graphPath)

    def denoiseSNRvsOrigReconSignal3D(this, SNRs, times, osig, rsig, Identifier, signalType):
        fig = plt.figure(figsize=(12, 9))
        Xi, Yi = np.meshgrid(times, SNRs)
        ax = fig.gca(projection = '3d')
        ax.set_xlabel('Time')
        ax.set_ylabel('SNR')
        ax.set_zlabel('Signal')
        plt.title('SNRvsOrigReconSignal')

        c= ['#e6194b', '#3cb44b', '#ffe119', '#4363d8', '#f58231', '#911eb4', '#46f0f0', '#f032e6', '#bcf60c', '#fabebe', '#008080', '#e6beff', '#9a6324', '#800000', '#aaffc3', '#808000', '#ffd8b1', '#000075', '#808080', '#000000']
        for i in range(0, len(SNRs)):
            #ax.plot3D(Xi[i], Yi[i], osig[i], 'gray')
            ax.plot3D(Xi[i], Yi[i], rsig[i], c[i])

        ax.legend(['Original Signal', 'Denoised Signal'], loc='best')
        plt.tight_layout(pad=1.7)

        graphPath= os.path.join(getPOASRootDir(), 'Graphs', this.collection, signalType, "denoiseSNRvsOrigReconSignal3D.png")
        if not os.path.exists(os.path.dirname(graphPath)):
            os.makedirs(os.path.dirname(graphPath))        
        fig.savefig(graphPath, dpi=fig.dpi)
        plt.cla()
        plt.close(fig)
        this.printSTDOUTMsg(Identifier, graphPath)

    def denoiseSNRvsNoise3D(this, SNRs, times, noise, Identifier, signalType):
        noise= np.asarray(noise)
        fig = plt.figure(figsize=(12, 9))
        Xi, Yi = np.meshgrid(times, SNRs)
        ax = fig.gca(projection = '3d')
        ax.set_xlabel('Time')
        ax.set_ylabel('SNR')
        ax.set_zlabel('Noise')
        #c= ['#e6194b', '#3cb44b', '#ffe119', '#4363d8', '#f58231', '#911eb4', '#46f0f0', '#f032e6', '#bcf60c', '#fabebe', '#008080', '#e6beff', '#9a6324', '#800000', '#aaffc3', '#808000', '#ffd8b1', '#000075', '#808080', '#000000']
        
        barsize=0.4
        #T, A = np.meshgrid(times, SNRs)
        # Xi = T.flatten()
        # Yi = A.flatten()
        # Zi = np.zeros(noise.size)
        # dx = barsize * np.ones(noise.size)
        # dy = barsize * np.ones(noise.size)
        # dz = np.asarray(noise).flatten()

        #ax.bar3d(Xi, Yi, Zi, dx, dy, dz)
        c= ['#e6194b', '#3cb44b', '#ffe119', '#4363d8', '#f58231', '#911eb4', '#46f0f0', '#f032e6', '#bcf60c', '#fabebe', '#008080', '#e6beff', '#9a6324', '#800000', '#aaffc3', '#808000', '#ffd8b1', '#000075', '#808080', '#000000']        
        for i in range(0, len(SNRs)):
            #ax.bar3d(Xi[i], Yi[i], np.zeros(len(times)), np.ones(len(times)) * barsize, np.ones(len(times)) * barsize, noise[i])
            ax.plot3D(Xi[i], Yi[i], noise[i], c[i])


        #ax.legend(['Original Signal', 'Denoised Signal'], loc='best')

        graphPath= os.path.join(getPOASRootDir(), 'Graphs', this.collection, signalType, "denoiseSNRvsNoise3D.png")
        if not os.path.exists(os.path.dirname(graphPath)):
            os.makedirs(os.path.dirname(graphPath))   
        plt.tight_layout(pad=1.7)

        fig.savefig(graphPath, dpi=fig.dpi)
        plt.cla()
        plt.close(fig)
        this.printSTDOUTMsg(Identifier, graphPath)

    def denoiseFreqVsEnergy3DBar(this, SNRs, frequencies, energies, energiesf, freqLables, Identifier, signalType):
        energies= np.asarray(energies)
        energiesf= np.asarray(energiesf)

        fig = plt.figure(figsize=(12, 9))
        barsize=0.4
        ax = fig.gca(projection = '3d')

        # T, A = np.meshgrid(frequencies, SNRs)
        # Xi = T.flatten()
        # Yi = A.flatten()
        # Zi = np.zeros(energies.size)
        # dx = barsize * np.ones(energies.size)
        # dy = barsize * np.ones(energies.size)
        # dz = np.asarray(energies).flatten()

        # T, A = np.meshgrid(frequencies + barsize, SNRs)
        # Xi2 = T.flatten()
        # Yi2 = A.flatten()
        # dz2 = np.asarray(energiesf).flatten()

        ax.set_xlabel('Frequency')
        ax.set_ylabel('SNR')
        ax.set_zlabel('Energy')
        # ax.bar3d(Xi, Yi, Zi, dx, dy, dz, color = 'g')
        # ax.bar3d(Xi2, Yi2, Zi, dx, dy, dz2, color = 'r')

        Xi, Yi = np.meshgrid(frequencies, SNRs)

        for i in range(0, len(SNRs)):
            ax.bar3d(Xi[i], Yi[i], np.zeros(len(frequencies)), np.ones(len(frequencies)) * barsize, np.ones(len(frequencies)) * barsize, energiesf[i])
            #ax.bar3d(np.asarray(Xi[i])+barsize, Yi[i], np.zeros(len(frequencies)), np.ones(len(frequencies)) * barsize, np.ones(len(frequencies)) * barsize, energies[i])

        #plt.show()
        graphPath= os.path.join(getPOASRootDir(), 'Graphs', this.collection, signalType, "denoiseFreqVsEnergy3DBar.png")
        if not os.path.exists(os.path.dirname(graphPath)):
            os.makedirs(os.path.dirname(graphPath))    
        plt.tight_layout(pad=1.7)

        fig.savefig(graphPath, dpi=fig.dpi)
        plt.cla()
        plt.close(fig)
        this.printSTDOUTMsg(Identifier, graphPath)

    def plotTotalProjectionFleets(this, Identifier, collection, data, lables, graphName, signalType, title, XLabel='Time', YLabel= 'FD'): #2Din3D
        plt.style.use('dark_background')
        fig, ax = plt.subplots()
        for d in range(len(data)):
            plt.scatter(data[d][0], data[d][1], label=lables[d])        
        plt.xlabel(XLabel)
        plt.ylabel(YLabel)
        plt.title(title)
        plt.legend()
        plt.tight_layout(pad=1.7)
        #plt.show()
        graphPath= os.path.join(getPOASRootDir(), 'Graphs', collection, signalType, graphName)
        if not os.path.exists(os.path.dirname(graphPath)):
            os.makedirs(os.path.dirname(graphPath))        
        fig.savefig(graphPath, dpi=fig.dpi)
        plt.cla()
        plt.close(fig)
        this.printSTDOUTMsg(Identifier, graphPath)

    def plotCcm2D(this, Identifier, collection, d, graphName, signalType, title): 
        plt.style.use('dark_background')
        fig, ax = plt.subplots()
        plt.scatter(d[0], d[1], label='RawData', c='green')
        plt.scatter(d[0], d[2], label='CalcData1', c='blue')
        plt.scatter(d[0], d[3], label='CalcData2', c='yellow')
        plt.plot(d[0], d[2], c='blue')
        plt.plot(d[0], d[3], c='yellow')
        ax.legend()
        plt.xlabel('L (Signal Length)')
        plt.ylabel('Regression Coeff')
        plt.title(title)
        plt.legend()
        plt.tight_layout(pad=1.7)
        #plt.show()
        graphPath= os.path.join(getPOASRootDir(), 'Graphs', collection, signalType, graphName)
        if not os.path.exists(os.path.dirname(graphPath)):
            os.makedirs(os.path.dirname(graphPath))        
        fig.savefig(graphPath, dpi=fig.dpi)
        plt.cla()
        plt.close(fig)
        this.printSTDOUTMsg(Identifier, graphPath)

    def plotCcm3D(this, Identifier, collection, d, graphName, signalType, title): 
        plt.style.use('dark_background')
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        # The zdir keyword makes it plot the "z" vertex dimension (Time)
        # along the y axis. The zs keyword sets each polygon at the
        # correct Time value.

        ax.plot3D(d[0], d[1], d[2], 'black')
        
        ax.set_xlabel('X:x(t) - Original')
        ax.set_ylabel('Y:x(t-tou)')
        ax.set_zlabel('Z:x(t-2*tou)')
        #Z axis position
        # tmp_planes= ax.zaxis._PLANES
        # ax.zaxis._PLANES = ( tmp_planes[2], tmp_planes[3], tmp_planes[0], tmp_planes[1], tmp_planes[4], tmp_planes[5])

        # ax.w_yaxis.set_major_locator(ticker.FixedLocator(time_data)) 
        # ax.w_yaxis.set_major_formatter(ticker.FuncFormatter(this.format_date))
        # for tl in ax.w_yaxis.get_ticklabels():
        #        tl.set_ha('left')
        #        tl.set_rotation(350) 
        plt.tight_layout(pad=1.7)

        #plt.show()
        graphPath= os.path.join(getPOASRootDir(), 'Graphs', collection, signalType, graphName)
        if not os.path.exists(os.path.dirname(graphPath)):
            os.makedirs(os.path.dirname(graphPath))        
        #fig.savefig(graphPath, dpi=fig.dpi)
        #plt.cla()
        #plt.close(fig)
        this.printSTDOUTMsg(Identifier, graphPath)



    def denoiseFreqVsVarianceScatter(this, variances, freqLables, Identifier, signalType):
        index = np.arange(len(freqLables)-1)
        fig, ax = plt.subplots()
        plt.scatter(index, variances[1:])

        xnew = np.linspace(index.min(), index.max(), 300) 
        spl = make_interp_spline(index, variances[1:], k=2)  # type: BSpline
        power_smooth = spl(xnew)
        plt.plot(xnew, power_smooth, label='Variance')
        
        plt.xlabel('Frequency')
        plt.ylabel('Variance')
        plt.title('Variance by Frequency')
        plt.xticks(index, freqLables[1:])
        plt.legend()
        plt.tight_layout(pad=1.7)
        plt.xticks(rotation=90, fontsize=7)
        plt.yticks(fontsize=7)
        #plt.show()
        graphPath= os.path.join(getPOASRootDir(), 'Graphs', this.collection, signalType, "denoiseFreqVsVarianceScatter.png")
        if not os.path.exists(os.path.dirname(graphPath)):
            os.makedirs(os.path.dirname(graphPath))        
        fig.savefig(graphPath, dpi=fig.dpi)
        plt.cla()
        plt.close(fig)
        this.printSTDOUTMsg(Identifier, graphPath)


    def cwt3d(this, time, coefs, frequencies, graphName, Identifier):        
        graphPath = os.path.join(getPOASRootDir(), 'Graphs', this.collection, "%s.png" %(graphName))
        if not os.path.exists(os.path.dirname(graphPath)):
            os.makedirs(os.path.dirname(graphPath))
        plt.style.use('dark_background')
        fig = plt.figure(figsize=(10,5))
        ax = fig.gca(projection='3d')
        #ax.set_facecolor('k')
        ax.set_title(graphName, color='white')
        #ax.xaxis.label.set_color('white')
        #ax.yaxis.label.set_color('white')
        #ax.zaxis.label.set_color('white')
        ax.tick_params(axis='x')
        ax.tick_params(axis='y')
        ax.tick_params(axis='z')
        #t= time
        #temp, t = np.meshgrid(np.arange(0, len(time)), time)
        t= np.tile(time, (len(coefs), 1))
        cc= coefs
        temp, f = np.meshgrid(np.arange(0, len(coefs[0])), frequencies)
        #R = np.sqrt(X**2 + Y**2)
        #Z = np.sin(R)
        # Plot the surface.
        # t= np.transpose(t)
        # f= np.transpose(f)
        # cc= np.transpose(cc)
        surf = ax.plot_surface(t, f, cc, cmap=cm.jet, linewidth=1, antialiased=False)
        #surf= ax.plot_wireframe(t, f, c, rstride=10, cstride=10)
        # Customize the z axis.
        #ax.set_zlim(min(frequencies), max(frequencies))
        #ax.set_zlim(-1.01, 1.1)
        #ax.zaxis.set_major_locator(LinearLocator(10))
        #ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
        ax.set_xlabel('X-Time')
        ax.set_ylabel('Y-Freq')
        ax.set_zlabel('Z-Amp')
        #plt.yscale('symlog')
        #plt.yscale('log')
        # Add a color bar which maps values to colors.
        fig.colorbar(surf, shrink=0.5, aspect=10)
        ax.view_init(elev=16, azim=-154)     
        plt.tight_layout(pad=1.7)

        fig.savefig(graphPath, dpi=fig.dpi)
        #plt.show()
        plt.cla()
        plt.close(fig)
        this.printSTDOUTMsg(Identifier, graphPath)

    def plot_contour(this, x,y,z,resolutionx = 103,resolutiony = 128,contour_method='linear'):
        resolutionx = str(resolutionx)+'j'
        resolutiony = str(resolutiony)+'j'
        X,Y = np.mgrid[min(x):max(x):complex(resolutionx),   min(y):max(y):complex(resolutiony)]
        points = [[a,b] for a,b in zip(x,y)]
        #Z = griddata(points, z, (X, Y), method=contour_method)
        return X,Y

    def denoiseTimeFreqEnergyContour(this, time, frequencies, energies, Identifier, signalType, title, xlabel, ylabel, zlabel, signalDateTimeFormat):
        graphPath= os.path.join(getPOASRootDir(), 'Graphs', this.collection, signalType, "%s_contour.png" %(title))
        if not os.path.exists(os.path.dirname(graphPath)):
            os.makedirs(os.path.dirname(graphPath))
        freq= np.array(frequencies)
        freq= freq[:,1:]
        t= np.array([datetimeUTCToDatetimeobj(x[0]) for x in time])
        t= np.tile(t, (freq.shape[1], 1)).transpose()
        energies= np.array(energies)
        energies= energies[:,1:]
        temp, freqs = np.meshgrid(energies, freq)
        
        plt.style.use('dark_background')        
        fig,ax=plt.subplots(figsize=(10,5))

        cp = ax.contourf(t, freq, energies, cmap=cm.jet, antialiased=False)
        cbar= fig.colorbar(cp, aspect=20) # Add a colorbar to a plot
        cbar.set_label('energy', rotation=270, size=8)
        ax.set_title('%s' %(title))
        ax.set_xlabel('time')
        ax.set_ylabel('frequency')
        #ax.xaxis.set_major_formatter(mdates.DateFormatter("%Y:%m:%d:%H:%M:%S"))
        plt.xticks(rotation=90)
        plt.tight_layout(pad=1.7)

        fig.savefig(graphPath, dpi=fig.dpi)
        plt.cla()
        plt.close(fig)
        this.printSTDOUTMsg(Identifier, graphPath)
        #plt.show()

    def cwt3d_2d(this, time, coefs, frequencies, graphName, Identifier):
        graphName= "%s_2D" %(graphName)
        graphPath= os.path.join(getPOASRootDir(), 'Graphs', this.collection, "%s.png" %(graphName))
        if not os.path.exists(os.path.dirname(graphPath)):
            os.makedirs(os.path.dirname(graphPath))

        t= np.tile(time, (len(coefs), 1))
        cc= coefs
        temp, f = np.meshgrid(np.arange(0, len(coefs[0])), frequencies)
        
        
        plt.style.use('dark_background')        
        fig,ax=plt.subplots(figsize=(10,5))
        X=t
        Y=f
        Z=cc
        # print "X-time"
        # print X
        # print "Y-freq"
        # print Y
        # print "Z-Amp"
        # print Z
        # X= np.tile([0.0,0.199,0.349,0.5,0.699,0.84,1.0], (8, 1))
        # Y= np.meshgrid(np.arange(0, 7), [0.000, 0.263, 0.526, 0.632, 0.737, 0.842, 0.947, 1.000])[1]
        # Z= np.array([[0.392,0.496,0.500,0.500,0.500,0.500,0.5],[0.286,0.472,0.494,0.500,0.500,0.500,0.500],[0.094,0.304,0.434,0.496,0.500,0.500,0.500],[-0.036,0.118,0.308,0.460,0.500,0.500,0.500],[-0.052,-0.042,0.120,0.328,0.480,0.500,0.500],[-0.212,-0.192,-0.120,0.004,0.266,0.438,0.496],[-0.320,-0.362,-0.348,-0.352,-0.304,-0.247,-0.145],[-0.328,-0.414,-0.454,-0.460,-0.478,-0.474,-0.490]])
        #plt.yscale('symlog')
        #X,Y = this.plot_contour(time,frequencies,cc,len(frequencies),len(time),contour_method='linear')
        #X,Y = this.plot_contour(time,frequencies,cc,8,7,contour_method='linear')
        cp = ax.contourf(X, Y, Z, cmap=cm.jet, antialiased=False)
        cbar= fig.colorbar(cp, aspect=20) # Add a colorbar to a plot
        cbar.set_label('Amplitude', rotation=270, size=8)
        ax.set_title('%s' %(graphName))
        ax.set_xlabel('X-Time')
        ax.set_ylabel('Y-Freq')
        plt.tight_layout(pad=1.7)

        fig.savefig(graphPath, dpi=fig.dpi)
        plt.cla()
        plt.close(fig)
        this.printSTDOUTMsg(Identifier, graphPath)
        #plt.show()

    def pca_kpca(this, Identifier, collection, graphName, title, data): #2Din3D
        #logger= get_logger('Filter')
        graphPath= os.path.join(getPOASRootDir(), 'Graphs', this.collection, "%s.png" %(graphName))
        if not os.path.exists(os.path.dirname(graphPath)):
            os.makedirs(os.path.dirname(graphPath))
        try:
            fig, ax = plt.subplots(figsize=(15,7))
            ax.set_facecolor('k')
            plt.locator_params(axis='both', nticks=10, tight=True, nbins=10)
            
            legends=['TSquareNormDist', '97%Confidence', '90%Confidence', '85%Confidence', 'Mean']
            for i in range(len(legends)):
                if i == 0: 
                    x= data[i][:,0], 
                    y= data[i][:,1]
                else:
                    x= data[i][0] 
                    y= data[i][1] 
                plt.scatter(x, y, label=legends[i])

            ax.legend()          
            plt.tight_layout(pad=1.7)
            ax.set_title(title, fontsize=10)
            ax.set_xlabel('TSquare', fontsize=10)
            ax.set_ylabel('NormalDistribution', fontsize=10)

            fig.savefig(graphPath, dpi=fig.dpi)
            plt.cla()
            plt.close(fig)
            this.printSTDOUTMsg(Identifier, graphPath)
        except Exception as e:
            this.logger.error("Error: Exception occured in pca_kpca:%s" %(e))
        return graphPath


    def pca_kpca_time_tsq(this, Identifier, collection, graphName, title, data): 
        #logger= get_logger('Filter')
        graphPath= os.path.join(getPOASRootDir(), 'Graphs', this.collection, "%s.png" %(graphName))
        if not os.path.exists(os.path.dirname(graphPath)):
            os.makedirs(os.path.dirname(graphPath))
        try:
            fig, ax = plt.subplots(figsize=(15,7))
            ax.set_facecolor('k')
            plt.locator_params(axis='both', nticks=10, tight=True, nbins=10)
            
            plt.scatter(data[0][:,0], data[0][:,1], label='TSquare')
            legends=['97%Confidence', '90%Confidence', '85%Confidence']
            for i in range(len(legends)):
                plt.plot(data[0][:,0], np.repeat(data[2+i][0], len(data[0][:,0])),label=legends[i])

            ax.legend()            
            ax.set_title(title, fontsize=10)
            ax.set_xlabel('Time', fontsize=10)
            ax.set_ylabel('TSquare', fontsize=10)
            plt.tight_layout(pad=1.7)
            fig.savefig(graphPath, dpi=fig.dpi)
            plt.cla()
            plt.close(fig)
            this.printSTDOUTMsg(Identifier, graphPath)
        except Exception as e:
            this.logger.error("Error: Exception occured in pca_kpca_time_tsq:%s" %(e))
        return graphPath


    def pca_kpca_tsq_tsqcummperc(this, Identifier, collection, graphName, title, data): 
        #logger= get_logger('Filter')
        graphPath= os.path.join(getPOASRootDir(), 'Graphs', this.collection, "%s.png" %(graphName))
        if not os.path.exists(os.path.dirname(graphPath)):
            os.makedirs(os.path.dirname(graphPath))
        try:
            fig, ax = plt.subplots(figsize=(15,7))
            ax.set_facecolor('k')
            plt.locator_params(axis='both', nticks=10, tight=True, nbins=10)
            data0= data[0].copy()
            plt.scatter(data0[:,3], data0[:,2], label='TSquare')
            legends=['97%Confidence', '90%Confidence', '85%Confidence']
            for i in range(len(legends)):
                plt.plot(data0[:,3], np.repeat(data[2+i][0], len(data[0][:,0])),label=legends[i])
            # for i in range(len(legends)):
            #     #times= np.arange(len(data0[:,0]))
            #     plt.plot(data0[:,2], np.repeat(data[2+i][0], len(data0[:,2])),label=legends[i])

            ax.legend()            
            ax.set_title(title, fontsize=10)
            ax.set_xlabel('TSquareCummPerc', fontsize=10)
            ax.set_ylabel('TSquare', fontsize=10)
            plt.tight_layout(pad=1.7)
            fig.savefig(graphPath, dpi=fig.dpi)
            plt.cla()
            plt.close(fig)
            this.printSTDOUTMsg(Identifier, graphPath)
        except Exception as e:
            this.logger.error("Error: Exception occured in pca_kpca_tsq_tsqcummperc:%s" %(e))
        return graphPath

    def kpca_fleets(this, Identifier, collection, graphName, title, data): #2Din3D
        graphPath= os.path.join(getPOASRootDir(), 'Graphs', this.collection, "%s.png" %(graphName))
        if not os.path.exists(os.path.dirname(graphPath)):
            os.makedirs(os.path.dirname(graphPath))
        try:    
            fig, ax = plt.subplots()
            ax.set_facecolor('k')
            plt.locator_params(axis='both', nticks=10, tight=True, nbins=10)
            for d in range(0, len(data)):
                plt.plot(np.arange(1, len(data[d])+1), data[d])
            plt.legend()
            plt.tight_layout(pad=1.7)
            ax.set_title(title, fontsize=10)
            ax.set_xlabel('Feature', fontsize=10)
            ax.set_ylabel('?', fontsize=10)
            fig.savefig(graphPath, dpi=fig.dpi)
            #plt.show()
            plt.cla()
            plt.close(fig)
            this.printSTDOUTMsg(Identifier, graphPath)
        except Exception as e:
            this.logger.error("Error: Exception occured in kpca_fleets:%s" %(e))
        return graphPath

    def pca_keca(this, Identifier, collection, graphName, title, data): #2Din3D
        #logger= get_logger('Filter')
        graphPath= os.path.join(getPOASRootDir(), 'Graphs', this.collection, "%s.png" %(graphName))
        if not os.path.exists(os.path.dirname(graphPath)):
            os.makedirs(os.path.dirname(graphPath))
        try:
            fig, ax = plt.subplots(figsize=(15,7))
            ax.set_facecolor('k')
            plt.locator_params(axis='both', nticks=10, tight=True, nbins=10)
            plt.scatter(data[0], data[1])
            ax.set_title(title, fontsize=10)
            ax.set_xlabel('Time', fontsize=10)
            ax.set_ylabel('KECA', fontsize=10)
            plt.tight_layout(pad=1.7)
            fig.savefig(graphPath, dpi=fig.dpi)
            plt.cla()
            plt.close(fig)
            this.printSTDOUTMsg(Identifier, graphPath)
        except Exception as e:
            this.logger.error("Error: Exception occured in pca_keca:%s" %(e))
        return graphPath

    def pc1pc2(this, Identifier, collection, graphName, title, data): #2Din3D
        graphPath= os.path.join(getPOASRootDir(), 'Graphs', this.collection, "%s.png" %(graphName))
        if not os.path.exists(os.path.dirname(graphPath)):
            os.makedirs(os.path.dirname(graphPath))
        try:
            fig, ax = plt.subplots(figsize=(15,7))
            ax.set_facecolor('k')
            plt.locator_params(axis='both', nticks=10, tight=True, nbins=10)
            #plt.plot(np.arange(1, len(data[d])+1), data[d], label='Fleet_%d' %(d))
            plt.scatter(data[:,0], data[:,1], label='SrNovsPC1')
            #plt.plot(data[0], data[1], label='PC1vsPC2')
            plt.legend()
            plt.tight_layout(pad=1.7)
            ax.set_title(title, fontsize=10)
            ax.set_xlabel('PC1', fontsize=10)
            ax.set_ylabel('PC2', fontsize=10)
            fig.savefig(graphPath, dpi=fig.dpi)
            plt.cla()
            plt.close(fig)
            this.printSTDOUTMsg(Identifier, graphPath)
        except Exception as e:
            this.logger.error("Error: Exception occured in pc1pc2:%s" %(e))
        return graphPath

    def pc1pc2pc3(this, Identifier, collection, graphName, title, data): #2Din3D
        graphPath= os.path.join(getPOASRootDir(), 'Graphs', this.collection, "%s.png" %(graphName))
        if not os.path.exists(os.path.dirname(graphPath)):
            os.makedirs(os.path.dirname(graphPath))
        try:
            plt.style.use('dark_background')
            fig = plt.figure(figsize=(12, 9))
            ax = fig.gca(projection = '3d')
            #ax.set_facecolor('k')
            X, Y, Z = data[:,0], data[:,1], data[:,3]

            ax.scatter(X, Y, Z, c='r', marker='o')
            #plt.legend()
            plt.tight_layout(pad=1.7)
            ax.set_title(title, fontsize=10)
            ax.set_xlabel('PC1', fontsize=10)
            ax.set_ylabel('PC2', fontsize=10)
            ax.set_zlabel('PC3', fontsize=10)
            fig.savefig(graphPath, dpi=fig.dpi)
            plt.cla()
            plt.close(fig)
            this.printSTDOUTMsg(Identifier, graphPath)
        except Exception as e:
            this.logger.error("Error: Exception occured in pc1pc2pc3:%s" %(e))
        return graphPath

    def srnopc1(this, Identifier, collection, graphName, title, data): #2Din3D
        graphPath= os.path.join(getPOASRootDir(), 'Graphs', this.collection, "%s.png" %(graphName))
        if not os.path.exists(os.path.dirname(graphPath)):
            os.makedirs(os.path.dirname(graphPath))
        try:
            fig, ax = plt.subplots(figsize=(15,7))
            ax.set_facecolor('k')
            plt.locator_params(axis='both', nticks=10, tight=True, nbins=10)
            #plt.plot(np.arange(1, len(data[d])+1), data[d], label='Fleet_%d' %(d))
            plt.scatter(np.arange(1, len(data[:,0])+1), data[:,0], label='SrNovsPC1')
            #plt.plot(np.arange(1, len(data[0])+1), data[0])            
            plt.legend()
            plt.tight_layout(pad=1.7)

            ax.set_title(title, fontsize=10)
            ax.set_xlabel('SrNo', fontsize=10)
            ax.set_ylabel('PC1', fontsize=10)
            fig.savefig(graphPath, dpi=fig.dpi)
            plt.cla()
            plt.close(fig)
            this.printSTDOUTMsg(Identifier, graphPath)
        except Exception as e:
            this.logger.error("Error: Exception occured in srnopc1:%s" %(e))
        return graphPath


    def stft3d(this, Identifier, collection, graphName, time_data, freq_data, amp_data): #2Din3D
        #logger= get_logger('Filter')
        graphPath= os.path.join(getPOASRootDir(), 'Graphs', this.collection, "%s.png" %(graphName))
        if not os.path.exists(os.path.dirname(graphPath)):
            os.makedirs(os.path.dirname(graphPath))
        try:
            verts = []
            for i in range(len(time_data)):
                # adding a zero amplitude at the beginning and the end to get a nice
                # flat bottom on the polygons
                xs = np.concatenate([[freq_data[0,i]], freq_data[:,i], [freq_data[-1,i]]])
                ys = np.concatenate([[0],amp_data[:,i],[0]])
                verts.append(list(zip(xs, ys)))

            fcolors=[]
            for name, hex in mcolors.BASE_COLORS.iteritems(): fcolors.append(name)
            for name, hex in mcolors.CSS4_COLORS.iteritems(): fcolors.append(name) 
            poly = PolyCollection(verts, facecolors = fcolors[0:len(amp_data[0])])
            poly.set_alpha(0.8)
            plt.style.use('dark_background')
            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')
            # The zdir keyword makes it plot the "z" vertex dimension (Time)
            # along the y axis. The zs keyword sets each polygon at the
            # correct Time value.

            ax.add_collection3d(poly, zs=time_data, zdir='y')

            ax.set_xlim3d(freq_data.min(), freq_data.max())
            ax.set_xlabel('Frequency')
            ax.set_ylim3d(time_data[0], time_data[-1])
            #ax.set_ylabel('Time')
            ax.set_zlim3d(amp_data.min(), amp_data.max())
            ax.set_zlabel('Amplitude')
            #Z axis position
            # tmp_planes= ax.zaxis._PLANES
            # ax.zaxis._PLANES = ( tmp_planes[2], tmp_planes[3], tmp_planes[0], tmp_planes[1], tmp_planes[4], tmp_planes[5])

            ax.w_yaxis.set_major_locator(ticker.FixedLocator(time_data)) 
            ax.w_yaxis.set_major_formatter(ticker.FuncFormatter(this.format_date))
            for tl in ax.w_yaxis.get_ticklabels():
                   tl.set_ha('left')
                   tl.set_rotation(350) 
            ax.view_init(elev=25, azim=-44) 
            plt.tight_layout(pad=1.7)
            
            #plt.show()
            this.printSTDOUTMsg(Identifier, graphPath)
            fig.savefig('%s' %(graphPath), dpi=fig.dpi)
            plt.cla()
            plt.close(fig)
        except Exception as e:
            this.logger.error("Error: Exception occured in stft3d:%s" %(e))
        return graphPath

    def printSTDOUTMsg(this, i, v):
        v= os.path.join(os.path.basename(os.path.dirname(v)), (os.path.basename(v)))
        print 'MCOutput:{"i":"%s", "graph":"%s"}' %(i, v)
