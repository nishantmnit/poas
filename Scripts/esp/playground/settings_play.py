import os
import sys
import warnings
from os.path import dirname as up

ESP_DIR = os.path.dirname(os.path.abspath(__file__))
LOG_DIR = os.path.join(ESP_DIR, 'log')
DEBUG_DIR = os.path.join(ESP_DIR, 'debug')
CSV_DATA = os.path.join(ESP_DIR, "core", "storage", "csv_input_data")

# test variable
GOLDEN_DIR = os.path.join(ESP_DIR, "test", "golden_data")
DEBUG_TEST_DIR = os.path.join(ESP_DIR, "test")

POAS_ROOT = up(up(up(up(__file__))))

if POAS_ROOT not in sys.path:
    sys.path.insert(0, POAS_ROOT)

if ESP_DIR not in sys.path:
    sys.path.insert(0, ESP_DIR)

if not sys.warnoptions:
    warnings.simplefilter("ignore")
