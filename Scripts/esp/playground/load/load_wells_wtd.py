#!/usr/bin/env python
import argparse
import math

import numpy as np
from bson.objectid import ObjectId

import Scripts.esp.playground.settings_play
from Lib.DBLib import *
from Lib.Utils import *
from Scripts.esp.core.storage.fetch_most import rts_b_wells_data


class load_wells_wtd(object):
    def __init__(this):
        this.getoptions()
        this.service= this.args.service
        this.wellcode= this.args.wellcode
        this.logger= get_logger('wtd')
        this.CenDb= getDbCon('Central') #default is RTS
        this.well_info= this.get_wellsdata()
        this.db= getDbCon(this.service)
        this.coll= this.db[this.well_info['Collection']] 
        this.dateYmdHms= datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
        
    def getoptions(this):
        parser = argparse.ArgumentParser()
        parser.add_argument("--service",     dest='service',     default="WTD",                      help='WTD')
        parser.add_argument("--wellcode",    dest='wellcode',    default='6142da6d24f7250877831763',  help='Well Mongo id of R169')
        parser.add_argument("--dateformat",  dest='dateformat',  default=None,                        help='time columns format of Realtime Production Data sheet') #%Y-%m-%d %H:%M:%S
        parser.add_argument("--cleanup",     dest='cleanup',     default=False,                       help='Drop WTD.<wellcode>', action='store_true')
        parser.add_argument("--outputtype",  dest='outputtype',  default='separateid',                help='allinoneid/separateid')
        this.args = parser.parse_args()

    def createCollIfNotExists(this, db, collName):
        if collName not in db.collection_names(): db.create_collection(collName)
        coll= db[collName] 
        return coll       

    def get_wellsdata(this):
        coll= this.createCollIfNotExists(this.CenDb, 'WELLS')
        wells_info= rts_b_wells_data(coll, this.wellcode)
        for w in wells_info:
            if w['_id'] == ObjectId(this.wellcode): 
                return w
        this.logger.error('%s is not setup in Central.WELL database!' %(this.wellcode))
        sys.exit(1)

    def trimDfHeaders(this, df):
        renameCol = {}
        for col in df.columns:
            c = col.strip()
            renameCol[col] = c.lower() 
        df.rename(columns= renameCol, inplace=True)
        return df

    def datetimeToUTCtimestamp(this, datetime_str, dateformat):
        utc_timestamp= None
        try:
            d= datetime.datetime.strptime(str(datetime_str), dateformat) 
            utc_timestamp= calendar.timegm(d.timetuple())
        except Exception as e:
            this.logger.error("datetime format in excel is not same as Sensors.DateTimeFormat, datetimeToUTCtimestamp failed, Error:%s" %(str(e)))
            this.logger.error("datetime_str:%s, dateformat:%s" %(datetime_str, dateformat))
            sys.exit(1)
        #datetime= datetime.datetime.utcfromtimestamp(utc_timestamp)
        return utc_timestamp

    def drop_well_in_WTD(this):
        if this.args.cleanup:
            this.coll.drop()
            this.logger.info("%s is dropped from %s database" %(this.well_info['Collection'], this.service))


    def index_unique_DateTime(this, con):
        indexName= 'uniqueDateTime'
        if indexName not in con.index_information():
            cols= [("property", pymongo.DESCENDING), ("datetime", pymongo.DESCENDING), ("test_type", pymongo.DESCENDING)]
            con.create_index(cols, background=True, name=indexName, unique=True)
            this.logger.info("Index:%s created" %(indexName))

    def index_unique_DateTime_data(this, con):
        indexName= 'uniqueDateTime'
        if indexName not in con.index_information():
            cols= [("property", pymongo.DESCENDING), ("datetime", pymongo.DESCENDING), ("test_type", pymongo.DESCENDING), ("data", pymongo.DESCENDING)]
            con.create_index(cols, background=True, name=indexName, unique=True)
            this.logger.info("Index:%s created" %(indexName))

    def allinoneid(this, settings, xls, excel_sheet_name):
        this.index_unique_DateTime(this.coll)

        df= this.trimDfHeaders(pd.read_excel(xls, sheet_name=excel_sheet_name, encoding=sys.getfilesystemencoding(), skiprows=[0,1]))
        for index, row in df.iterrows():
            this.logger.debug("count:%d" %(index))
            json= dict()
            d=row.to_dict()
            for k in df.keys(): 
                if re.search('.*?unnamed.*', k): del d[k]

            where= dict()
            data_dict= dict()

            where['property']= 'WTD' 
            if 'datetime' not in d.keys():
                this.logger.error('mandatory column:datetime is missing in input data')
                sys.exit(1)

            if pd.isnull(d['datetime']):
                continue
            data_dict['datetime_str']= str(d['datetime'])
            where['datetime']= this.datetimeToUTCtimestamp(d['datetime'], settings['data']['datetime'][-1]['format'] if this.args.dateformat is None else this.args.dateformat)
            where['test_type']= d['test_type']

            for column in settings['data']:
            
                cu= '%s_unit' %(column)
                if column in d.keys():
                    if (isinstance(d[column], float) and math.isnan(d[column])) or (isinstance(d[column], str) and d[column].strip() == ''):
                        data_dict[column]= ''
                    else:
                        data_dict[column]= d[column]
                else:
                    data_dict[column]= ''
                    
                if cu in d.keys():
                    if (isinstance(d[cu], float) and math.isnan(d[cu])) or (isinstance(d[cu], str) and d[cu].strip() == ''):
                        data_dict[cu]= ''
                    else:
                        data_dict[cu]= d[cu]
                else:
                    data_dict[cu]= ''
            data_dict.update(where)
            data_dict['created_at']= calendar.timegm(datetime.datetime.now().timetuple())
            data_dict['updated_at']= ''
            #rts_m_push_1
            this.coll.update(where, {'$setOnInsert':data_dict} , **{'upsert':True})
            #_id= rts_m_insert_ifnotexist(this.coll, where, data_dict)
            #_id= this.coll.update(where, data_dict, **{'upsert':True})
            #this.logger.info('Record insered/updated, property:"WTD", datetime:%s, _id:%s' %(where['datetime'], _id))

    def separateid(this, settings, xls, excel_sheet_name):
        this.index_unique_DateTime_data(this.coll)
        df= this.trimDfHeaders(pd.read_excel(xls, sheet_name=excel_sheet_name, encoding=sys.getfilesystemencoding(), skiprows=[0,1]))
        for index, row in df.iterrows():
            this.logger.debug("count:%d" %(index))
            json= dict()
            d=row.to_dict()
            for k in df.keys(): 
                if re.search('.*?unnamed.*', k): del d[k]

            where= dict()
            where['property']= 'WTD' 
            if 'datetime' not in d.keys():
                this.logger.error('mandatory column:datetime is missing in input data')
                sys.exit(1)

            if pd.isnull(d['datetime']):
                continue
            where['datetime']= this.datetimeToUTCtimestamp(d['datetime'], settings['data']['datetime'][-1]['format'] if this.args.dateformat is None else this.args.dateformat)
            where['test_type']= d['test_type']

            for column in settings['data']:
                data_dict= dict()
                data_dict['datetime_str']= str(d['datetime'])
                where['data']= column
                if column in d.keys():
                    if (isinstance(d[column], float) and math.isnan(d[column])) or (isinstance(d[column], str) and d[column].strip() == ''):
                        continue
                    else:
                        data_dict['value']= d[column]
                        cu= '%s_unit' %(column)
                        if cu in d.keys(): data_dict['unit']= d[cu]
                else:
                    continue
                data_dict.update(where)
                data_dict['created_at']= calendar.timegm(datetime.datetime.now().timetuple())
                data_dict['updated_at']= ''
                this.coll.update(where, {'$setOnInsert':data_dict} , **{'upsert':True})
            #_id= rts_m_insert_ifnotexist(this.coll, where, data_dict)
            #_id= this.coll.update(where, data_dict, **{'upsert':True})
            #this.logger.info('Record insered/updated, property:"WTD", datetime:%s, _id:%s' %(where['datetime'], _id))


    def load_excel_sheet(this, settings, wellPath):
        excel_sheet_name= "Well Test Data"
        this.logger.info("processing sheet:%s" %(excel_sheet_name))    
        xls = pd.ExcelFile(wellPath)
        sheet_names= [x for x in xls.sheet_names]
        sheet_names_lower= [x.lower() for x in sheet_names]
        
        if excel_sheet_name.lower() not in sheet_names_lower:
            this.logger.error("%s tab not found in well:%s" %(excel_sheet_name, wellPath))
            sys.exit()
        excel_sheet_name= sheet_names[sheet_names_lower.index(excel_sheet_name.lower())]

        if this.args.outputtype == 'allinoneid':
            this.allinoneid(settings, xls, excel_sheet_name)
        else:
            this.separateid(settings, xls, excel_sheet_name)
        this.logger.info("processing completed!")    

    def main(this):
        this.logger.info('Processing of WellName:%s is started in %s.%s' %(this.well_info['WellName'], this.service, this.well_info['Collection']))        
        wellPath= os.path.join(Scripts.esp.playground.settings_play.POAS_ROOT, 'Scripts', 'esp', 'test', 'golden_data', '%s.xlsx' %(this.well_info['WellName']))
        this.logger.info("processing excel:%s" %(wellPath))    
        if not os.path.isfile(wellPath):
            this.logger.error("%s not found!" %(wellPath))
            sys.exit(1)
        this.drop_well_in_WTD()
        settings= this.add_settings()
        this.load_excel_sheet(settings, wellPath)


    def createCollIfNotExists(this, db, collName):
        if collName not in db.collection_names(): db.create_collection(collName)
        coll= db[collName] 
        return coll       

    def add_settings(this):
        coll_settings= this.createCollIfNotExists(this.db, 'Settings')
        count= len([s for s in coll_settings.find()])
        if count == 0:
            default_settings= this.get_default_settings()
            _id= coll_settings.insert(default_settings)
            this.logger.info("Default settings inserted into WTD.Settings collection, _id:%s" %(_id))
        else:
            this.logger.info("Settings already found in WTD.Settings collection!")
            return [s for s in coll_settings.find()][0]

    def get_default_settings(this):
        return {
            "data": {
                "datetime" : [{
                    "format" : "%Y-%m-%d %H:%M:%S" #since data will be inserted from GUI form, this will be changed to unix timestamp
                },],
                "test_type" : ['choke_size_mofification', 'water_cut', 'prod_gor', 'pressure_test'],
                "liquid_rate" : [{
                    "name": "Liquid Rate",
                    "unit" : "stb/day",
                    "value" : "",
                    "updated_at" : 5555
                },],
                "water_cut" : [{
                    "name": "Water Cut",
                    "unit" : "%",
                    "value" : "",
                    "updated_at" : 5555
                },],
                "oil_rate" : [{
                    "name" : "Oil Rate",
                    "unit" : "stb/day",
                    "value" : "",
                    "updated_at" : 5555
                },],  
                "gor" : [{
                    "name" : "GOR",
                    "unit" : "scf/stb",
                    "value" : "",
                    "updated_at" : 5555
                },],
                "glr" : [{
                    "name" : "GLR",
                    "unit" : "scf/stb",
                    "value" : "",
                    "updated_at" : 5555
                },],
                "salt_concentration" : [{
                    "name" : "Salt Concentration",
                    "unit" : "ppm",
                    "value" : "",
                    "updated_at" : 5555
                },],
                "sulphur" : [{
                    "name" : "Sulphur",
                    "unit" : "xyz",
                    "value" : "",
                    "updated_at" : 5555
                },],
                "whp" : [{
                    "name" : "WHP",
                    "unit" : "psi",
                    "value" : "",
                    "updated_at" : 5555
                },],
                "wht" : [{
                    "name" : "WHT",
                    "unit" : "degf",
                    "value" : "",
                    "updated_at" : 5555
                },],
                "left_choke" : [{
                    "name" : "Left Choke",
                    "value" : "1/64 inch",
                    "value" : "",
                    "updated_at" : 5555
                },],
                "right_choke" : [{
                    "name" : "Right Choke",
                    "unit" : "inch",
                    "value" : "",
                    "updated_at" : 5555
                },],
                "test_manifold_pressure" : [{
                    "name" : "Test Manifold Pressure",
                    "unit" : "psi",
                    "value" : "",
                    "updated_at" : 5555
                },],
                "flp" : [{
                    "name" : "FLP",
                    "unit" : "psi",
                    "value" : "",
                    "updated_at" : 5555
                },],
                }
            }


if __name__ == "__main__":
    ob=load_wells_wtd()
    ob.main()

#python playground/well_info.py --wellname R186
#python playground/load/load_wells_wtd.py --wellcode 614a010324f725a4d1f9d035 --cleanup --outputtype allinoneid

