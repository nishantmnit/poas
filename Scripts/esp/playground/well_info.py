#!/usr/bin/env python
import argparse
import re

from Lib.DBLib import *
from Lib.Utils import *


class well_info(object):
    def __init__(this):
        this.getoptions()
        this.wellcode= this.args.wellname
        this.logger= get_logger('rts')
        this.CenDb= getDbCon('Central') #default is RTS
        
    def getoptions(this):
        parser = argparse.ArgumentParser()
        parser.add_argument("--wellname",    dest='wellname',    default=None,    help='Well name')
        this.args = parser.parse_args()

    def get_sensor_details(this, well_info):
        sensors= dict()
        if 'sensors' in well_info.keys():
            for s in well_info['sensors']:
                if not isinstance(s['data_sampling_frequency'], list): continue
                if s['code'] in sensors.keys():
                    this.logger.error('duplicate sensorcode:%s found in Central.WELLS' %(s['code']))
                    sys.exit(1)
                sensors[s['code']]= s
                print "_id:%s, WellName:%s, Collection:%s, sensor.code:%s, sensor_name:%s, data_sampling_frequency.seconds:%s" %(well_info['_id'], well_info['WellName'], well_info['Collection'], s['code'], s['sensor_name'], sorted(s['data_sampling_frequency'], key=lambda k: k['updated_at'])[-1]['seconds'])
        return sensors

    def createCollIfNotExists(this, db, collName):
        if collName not in db.collection_names(): db.create_collection(collName)
        coll= db[collName] 
        return coll       

    def main(this):
        coll= this.createCollIfNotExists(this.CenDb, 'WELLS')
        cursor=[]
        if this.args.wellname is None:
            cursor = coll.find()
        else:
            rgx = re.compile('.*%s.*' %(this.args.wellname), re.IGNORECASE)  # compile the regex
            cursor = coll.find({'WellName':rgx})    
        for w in cursor:
            this.get_sensor_details(w)


if __name__ == "__main__":
    ob=well_info()
    ob.main()

#python playground/well_info.py --wellname R

