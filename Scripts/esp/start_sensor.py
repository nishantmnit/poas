import os
import sys
import pymongo
from multiprocessing import Process

from Scripts.esp.core.get_logger import get_logger
from Scripts.esp.core.storage.fetch_most import get_sensor_metadata, get_sensor_status
from Scripts.esp.core.storage.mongo import getDbCon, update
from Scripts.esp.pipelines.cwt import CWT
from Scripts.esp.pipelines.cqt import CQT
from Scripts.esp.pipelines.denoise import Denoise
from Scripts.esp.pipelines.outliers import Outliers
from Scripts.esp.pipelines.qaqc import QA_QC
from Scripts.esp.pipelines.read_sensors import SensorReader
from Scripts.esp.pipelines.stft import STFT

logger = get_logger("sensor start")

sensors_conn = getDbCon("Central", "sensors")


def update_sensor_status(well_name, gauge_name, sensor_name, status):
    conn = getDbCon("Central", "sensors")
    where = dict()
    where["well_name"] = well_name
    where["gauge_name"] = gauge_name
    where["sensor_name"] = sensor_name
    update_set = {"read_status": status, "pid": os.getpid() if status == "active" else None}
    update_set = {'$set': update_set}
    update(conn, where, update_set, upsert={'upsert': False})


def create_index(well_name):
    conn = getDbCon("RTS", well_name)
    index_name = "rts_index"
    if index_name not in conn.index_information():
        columns = ["property", "gauge_name", "sensor_name", "created_at", "min_x_axis", "max_x_axis"]
        index_order = [(column, pymongo.ASCENDING) for column in columns]
        conn.create_index(index_order, background=True, name="rts_index")


def start_pipelines(well_name, gauge_name, sensor_name):
    update_sensor_status(well_name, gauge_name, sensor_name, status="active")

    processes = list()
    processes.append(start_reader(well_name, gauge_name, sensor_name))
    processes.append(start_qa_qc(well_name, gauge_name, sensor_name))
    processes.append(start_outlier_fixing(well_name, gauge_name, sensor_name))
    processes.append(start_de_noising(well_name, gauge_name, sensor_name))
    processes.append(start_stft(well_name, gauge_name, sensor_name))
    processes.append(start_cwt(well_name, gauge_name, sensor_name))
    processes.append(start_cqt(well_name, gauge_name, sensor_name))

    # join threads
    for process in processes:
        process.join()

    # at the end free-up reader
    update_sensor_status(well_name, gauge_name, sensor_name, status="inactive")


def read_pipeline(pipeline):
    read_process = Process(target=pipeline.read_sensor)
    read_process.start()
    return read_process


def start_reader(well_name, gauge_name, sensor_name):
    sensor = get_sensor_metadata(sensors_conn, well_name, gauge_name, sensor_name)
    logger.debug("Frequency = %s %s" % (sensor.data_sampling_frequency, sensor.data_sampling_frequency_unit))
    reader = SensorReader("RTS", well_name, gauge_name, sensor_name, method=sensor.read_method)
    return read_pipeline(reader)


def start_qa_qc(well_name, gauge_name, sensor_name):
    qa_qc = QA_QC("RTS", well_name, gauge_name, sensor_name)
    return read_pipeline(qa_qc)


def start_outlier_fixing(well_name, gauge_name, sensor_name):
    outlier = Outliers("RTS", well_name, gauge_name, sensor_name)
    return read_pipeline(outlier)


def start_de_noising(well_name, gauge_name, sensor_name):
    de_noise = Denoise("RTS", well_name, gauge_name, sensor_name)
    return read_pipeline(de_noise)


def start_stft(well_name, gauge_name, sensor_name):
    stft = STFT("RTS", well_name, gauge_name, sensor_name)
    return read_pipeline(stft)


def start_cwt(well_name, gauge_name, sensor_name):
    cwt = CWT("RTS", well_name, gauge_name, sensor_name)
    return read_pipeline(cwt)


def start_cqt(well_name, gauge_name, sensor_name):
    cqt = CQT("RTS", well_name, gauge_name, sensor_name)
    return read_pipeline(cqt)


def read_sensor(well_name, gauge_name, sensor_name):
    status = get_sensor_status(sensors_conn, well_name, gauge_name, sensor_name)
    if status["read_status"] == "active":
        logger.exception("Sensor read already in progress for well = %s, gauge = %s, sensor = %s" % (
            well_name, gauge_name, sensor_name))
        sys.exit(-1)

    create_index(well_name)
    start_pipelines(well_name, gauge_name, sensor_name)


if __name__ == "__main__":
    read_sensor("R169", "ESP Sensor_Module", "motor_current")
