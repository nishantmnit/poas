import argparse
import json
from Scripts.esp.core.get_logger import get_logger
from Scripts.esp.core.storage.fetch_most import get_sensor_status
from Scripts.esp.core.storage.mongo import getDbCon, delete
from Scripts.esp.stop_sensor import stop_sensor
from Scripts.esp.start_sensor import read_sensor


logger = get_logger("sensor reader", master_module=True)

sensors_conn = getDbCon("Central", "sensors")


def read_options():
    parser = argparse.ArgumentParser()
    parser.add_argument('--well', dest='well_name', default=None, help='Well collection name MC/MC1/MC2')
    parser.add_argument('--gauge', dest='gauge_name', default=None, help='Gauge Name')
    parser.add_argument('--sensor', dest='sensor_name', default=None, help='Sensor name')
    parser.add_argument('--clear', dest='clear', default=False, action='store_true',
                        help='clear signals and start fresh')
    parser.add_argument('--start', dest='start', default=False, action='store_true',
                        help='start reading the sensor')
    parser.add_argument('--status', dest='status', default=False, action='store_true',
                        help='return status of sensor')
    parser.add_argument('--stop', dest='stop', default=False, action='store_true',
                        help='stop sensor reading')
    args = parser.parse_args()
    if not args.well_name or not args.gauge_name or not args.sensor_name:
        print "Please provide well name, gauge name and sensor name to read."
        quit()
    if not (args.clear or args.start or args.stop or args.status):
        print "Please provide at-least one action to perform."
        quit()
    return args


def clear_sensor(well_name):
    # clear previous signal if user asked to clear the signal
    well_conn = getDbCon("RTS", well_name)
    delete(well_conn)


def run(args):
    if args.start:
        read_sensor(args.well_name, args.gauge_name, args.sensor_name)
    elif args.status:
        print json.dumps(
            get_sensor_status(sensors_conn, args.well_name, args.gauge_name, args.sensor_name))
    elif args.stop:
        stop_sensor(args.well_name, args.gauge_name, args.sensor_name)
    elif args.clear:
        clear_sensor(args.well_name)


if __name__ == "__main__":
    user_args = read_options()
    run(user_args)

