#!/usr/bin/env python
import numpy as np
import sys, re, os, argparse, getopt, datetime, time
import pandas as pd
import xlsxwriter
from collections import OrderedDict, defaultdict

import settings
from Lib.Utils import *
from Lib.DBLib import *
from Lib.GraphLib import *
#from Scripts.esp.modules.rts_graphs import *
from Scripts.esp.core.eigen import *

class report(object):
    def __init__(this):
        this.getoptions()
        this.service= this.args.service
        this.collection= this.args.collection
        this.sensorId= this.args.sensorid
        this.lastdt= int(this.args.lastdt)
        this.logger= getLogger('report')
        this.db= getDbCon(this.service)
        this.coll= this.db[this.collection] 
        this.limitRecords=512

    def getoptions(this):
        parser = argparse.ArgumentParser()
        parser.add_argument("--service",       dest='service',       default="RTS",      help='RTS')
        parser.add_argument("--collection",    dest='collection',    default="X238",    help='Well collection name Well1/WellB/Well3')
        parser.add_argument("--sensorid",      dest='sensorid',      default="WellheadGauge",  help='SensorId Sensor1/SensorB')
        parser.add_argument("--out",           dest='out',           default="qaqc,interpolates,outliers,denoised,cwt,stft,pca_kpca_ind,pca_kpca_mul,lda_kfda", help='comma separated output reports')
        parser.add_argument("--lastdt",        dest='lastdt',        default=None,       help='last unix timestamp for report e.g. 1228781940')
        parser.add_argument("--debug",         dest="debug",                             help="print debug messages", action='store_true')
        this.args = parser.parse_args()
        if this.args.lastdt is None: 
            print "please provide lastdt:d/m/Y H:M:S"
            sys.exit(1)
        #this.lastdt= datetime.datetime.strptime(this.args.lastdt, '%d/%m/%Y %H:%M:%S')
        this.dateYmdHms= datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')


    def sendInExcel(this):
        fileName= os.path.join("mongoreports", "Report_%s_%s.xlsx" %(this.collection, this.sensorId))
        this.logger.info("Report generation is in progress:%s" %(fileName))
        if not os.path.exists(os.path.dirname(fileName)):
            os.makedirs(os.path.dirname(fileName))

        writer = pd.ExcelWriter(fileName, engine='xlsxwriter') 
        wb  = writer.book
        #WellB
        signalTypes=[this.sensorId]
        
        for signalType in signalTypes:
                # print "Printing in progress - SignalType:%s" %(signalType)
                try:
                    for o in this.args.out.split(','):
                        if o == 'pca_kpca_mul':
                            getattr(this, "print_%s" %(o))(writer, signalTypes, 'PcaKpca_denoised_mul', 'denoised_mul')  
                            getattr(this, "print_%s" %(o))(writer, signalTypes, 'PcaKpca_denoised_ca1', 'denoised_ca1')  
                            getattr(this, "print_%s" %(o))(writer, signalTypes, 'PcaKpca_denoised_ca2', 'denoised_ca2')  
                        elif o == 'lda_kfda':
                            getattr(this, "print_%s" %(o))(writer, signalTypes, 'LdaKfda_denoised_mul', 'denoised_mul')  
                            getattr(this, "print_%s" %(o))(writer, signalTypes, 'LdaKfda_denoised_ca1', 'denoised_ca1')  
                            getattr(this, "print_%s" %(o))(writer, signalTypes, 'LdaKfda_denoised_ca2', 'denoised_ca2')  
                        else:
                           getattr(this, "print_%s" %(o))(writer, signalType)  
                except Exception as e:
                    print "Error - SignalType:%s, Error:%s" %(signalType, e.message)
                else:
                    print "Completed succeessfully - SignalType:%s" %(signalType)

        wb.close()

    def print_qaqc(this, writer, signalType):
        this.logger.info("printing qaqc")
        sheetName= "%s_qaqc" %(signalType)
        cursor= this.coll.find(({'Property': 'Signal', 'SensorId': this.sensorId, "%s_Status" %(signalType):{ '$in': ['D', 'C']}, "DateTime":{ '$lt': this.lastdt}})).sort("DateTime", pymongo.DESCENDING).limit(this.limitRecords)
        df= pd.DataFrame(list(cursor))
        col1= "DateTime_str"
        col2= "DateTime"
        col3= "%s" %(signalType) 
        col4= "%s_QaQcStatus" %(signalType) 
        col5= "%s_POASRemark" %(signalType)
        df[[col1, col2, col3, col4, col5]].to_excel(writer, sheet_name=sheetName)
        #this.insertGraph(writer, 'interpolated.png', signalType, 'E2', signalType)

    def print_interpolates(this, writer, signalType):
        this.logger.info("printing interpolated")
        sheetName= "%s_interpolates" %(signalType)
        cursor= this.coll.find(({'Property': 'Signal', 'SensorId': this.sensorId, "%s_Status" %(signalType):{ '$in': ['D', 'C']}, "%s_SiganlQuality" %(signalType):{ '$ne': 'B'}, "DateTime":{ '$lt': this.lastdt}})).sort("DateTime", pymongo.DESCENDING).limit(this.limitRecords)
        df= pd.DataFrame(list(cursor))
        col= "%s" %('DateTime_str')
        col1= "%s" %(signalType) 
        col2= "%s_interpolated" %(signalType)
        df[[col,col1, col2]].to_excel(writer, sheet_name=sheetName)
        #this.insertGraph(writer, 'interpolated.png', signalType, 'E2', signalType)

    def print_outliers(this, writer, signalType):
        this.logger.info("printing outliers removed")
        sheetName= "%s_outliers" %(signalType)
        cursor= this.coll.find({'Property': 'Signal', 'SensorId': this.sensorId, "%s_Status" %(signalType):{ '$in': ['D', 'C']}, "%s_SiganlQuality" %(signalType):{ '$ne': 'B'}, "DateTime":{ '$lt': this.lastdt}}).sort("DateTime", pymongo.DESCENDING).limit(this.limitRecords)
        df= pd.DataFrame(list(cursor))
        #col1= "%s" %(signalType) 
        col= "%s" %('DateTime_str')
        col1= "%s" %(signalType)
        col2= "%s_interpolated" %(signalType)
        col3= "%s_outliersremoved" %(signalType)
        df[[col, col1, col2, col3]].to_excel(writer, sheet_name=sheetName)
        col4= "%s_outlierstat" %(signalType)
        pd.DataFrame({'Noise': df[col4].str.split("|", n = 1, expand = True)[1]}).to_excel(writer, sheet_name=sheetName, startcol=5, index=False)        
        pd.DataFrame({'SelectedWavelet': df[col4].str.split("|", n = 1, expand = True)[0]}).to_excel(writer, sheet_name=sheetName, startcol=6, index=False)

    def insert_image(this, row, col, writer, sheetName, graph):
        if not os.path.exists(graph):
            this.logger.warning("Image:%s not found!" %(graph))
        else:
            worksheet = writer.sheets[sheetName]
            worksheet.insert_image(row, col, graph)

    def print_denoised(this, writer, signalType):
        sheetName= "%s_denoised" %(signalType)
        this.logger.info("printing %s" %(sheetName))
        cursor= this.coll.find({'Property': 'Signal', 'SensorId': this.sensorId, "%s_Status" %(signalType):{ '$in': ['D', 'C']}, "%s_SiganlQuality" %(signalType):{ '$ne': 'B'}, "DateTime":{ '$lt': this.lastdt}}).sort("DateTime", pymongo.DESCENDING).limit(this.limitRecords)
        df= pd.DataFrame(list(cursor))
        col1= "%s" %(signalType) 
        col2= "%s_interpolated" %(signalType)
        col3= "%s_outliersremoved" %(signalType)
        col4= "%s_denoised" %(signalType)
        sCol= 0
        df[[col4]].to_excel(writer, sheet_name=sheetName, index=True, startcol=sCol)
        col5= "%s_denoisestat" %(signalType)
        sCol += 2
        pd.DataFrame({'RMSE': df[col5][0].split('|')[0]}, index=[0]).to_excel(writer, sheet_name=sheetName, startcol=sCol, index=False)
        sCol += 1
        pd.DataFrame({'SNR': df[col5][0].split('|')[1]}, index=[0]).to_excel(writer, sheet_name=sheetName, startcol=sCol, index=False)
        sCol += 1
        pd.DataFrame({'PRD': df[col5][0].split('|')[2]}, index=[0]).to_excel(writer, sheet_name=sheetName, startcol=sCol, index=False)
        sCol += 1
        pd.DataFrame({'R': df[col5][0].split('|')[3]}, index=[0]).to_excel(writer, sheet_name=sheetName, startcol=sCol, index=False)
        sCol += 1
        pd.DataFrame({'Er': df[col5][0].split('|')[4]}, index=[0]).to_excel(writer, sheet_name=sheetName, startcol=sCol, index=False)
        sCol += 1
        pd.DataFrame({'Wavelet': df[col5][0].split('|')[5]}, index=[0]).to_excel(writer, sheet_name=sheetName, startcol=sCol, index=False)
        sCol += 1
        pd.DataFrame({'MaxDecompLevels': df[col5][0].split('|')[6]}, index=[0]).to_excel(writer, sheet_name=sheetName, startcol=sCol, index=False)
        sCol += 1
        pd.DataFrame({'MaxDecompCoeffs': df[col5][0].split('|')[7]}, index=[0]).to_excel(writer, sheet_name=sheetName, startcol=sCol, index=False)
        sCol += 1
        pd.DataFrame({'Multipler': df[col5][0].split('|')[8]}, index=[0]).to_excel(writer, sheet_name=sheetName, startcol=sCol, index=False)

        #pd.DataFrame(filter(lambda x: not re.match('^a+$', x), df["%s_LastNodes" %(signalType)][0]), columns=['LastNodes']).to_excel(writer, sheet_name=sheetName, startcol=16, index=False)
        #pd.DataFrame(df["%s_thresholds" %(signalType)][0], columns=['Thresholds']).to_excel(writer, sheet_name=sheetName, startcol=17, index=False)

        cursor= this.coll.find({'Property': 'Signal', 'SensorId': this.sensorId, "%s_Status" %(signalType):{ '$in': ['D', 'C']}}).sort("DateTime", pymongo.DESCENDING).limit(this.limitRecords)
        df= pd.DataFrame(list(cursor))
        _id= df.sort_values(by='DateTime', ascending=False)['%s_energypower' %(signalType)].head(1).values[0]
        cursor= this.coll.find({'Property': 'SignalDenoiseEnergy', 'SignalType':signalType , '_id':_id})
        df1= pd.DataFrame(list(cursor))

        # df1[['Derivative1']].to_excel(writer, sheet_name=sheetName, index=False, startcol=16)
        # df1[['Derivative2']].to_excel(writer, sheet_name=sheetName, index=False, startcol=17)
        sCol += 1
        pd.DataFrame({'Derivative1': [dd for d in df1['Derivative1'] for dd in d]}).to_excel(writer, sheet_name=sheetName, index=False, startcol=sCol)
        sCol += 1
        pd.DataFrame({'Derivative2': [dd for d in df1['Derivative2'] for dd in d]}).to_excel(writer, sheet_name=sheetName, index=False, startcol=sCol)

        sCol += 1
        pd.DataFrame({'Before Threshold->': ""}, index=[0]).to_excel(writer, sheet_name=sheetName, startcol=sCol, index=False)
        col6= "Decomposed"
        idx= True
        sCol += 1
        col= 19 
        for l in df1[col6].to_dict(OrderedDict).values()[0].keys():
            for ll in df1[col6].to_dict(OrderedDict).values()[0]["%s" %(l)].keys():
                for ad in sorted(df1[col6].to_dict(OrderedDict).values()[0]["%s" %(l)]["%s" %(ll)].keys()):
                    pd.DataFrame({ad: df1[col6].to_dict(OrderedDict).values()[0]["%s" %(l)]["%s" %(ll)][ad]}).to_excel(writer, sheet_name=sheetName, startcol=sCol, index=idx, header=True)
                    idx= False
                    col += 2 if col == 19 else 1

        sCol += 2
        pd.DataFrame({'After Threshold->': ""}, index=[0]).to_excel(writer, sheet_name=sheetName, startcol=sCol, index=False)
        col6= "Decomposedf"
        idx= True
        sCol += 1 
        colt= col
        for l in df1[col6].to_dict(OrderedDict).values()[0].keys():
            for ll in df1[col6].to_dict(OrderedDict).values()[0]["%s" %(l)].keys():
                for ad in sorted(df1[col6].to_dict(OrderedDict).values()[0]["%s" %(l)]["%s" %(ll)].keys()):
                    pd.DataFrame({ad: df1[col6].to_dict(OrderedDict).values()[0]["%s" %(l)]["%s" %(ll)][ad]}).to_excel(writer, sheet_name=sheetName, startcol=sCol, index=idx, header=True)
                    idx= False
                    sCol += 2 if sCol == colt else 1

        #Denoise 2D and 3D graphs:
        #this.insertGraph_OutlierDenoised(writer, 'denoised.png', signalType, 'I6', sheetName)  #Enable this
        # graph= os.path.join('../..', 'Graphs', this.collection, signalType, 'denoiseFreqVsEnergy2DBar.png')
        # this.insert_image(24, 8, writer, sheetName, graph)
        # graph= os.path.join('../..', 'Graphs', this.collection, signalType, 'denoiseFreqVsVarianceScatter.png')
        # this.insert_image(45, 8, writer, sheetName, graph)
        # graph= os.path.join('../..', 'Graphs', this.collection, signalType, 'denoiseSNRvsOrigReconSignal3D.png')
        # this.insert_image(68, 8, writer, sheetName, graph)
        # graph= os.path.join('../..', 'Graphs', this.collection, signalType, 'denoiseSNRvsNoise3D.png')
        # this.insert_image(113, 8, writer, sheetName, graph)
        # graph= os.path.join('../..', 'Graphs', this.collection, signalType, 'denoiseFreqVsEnergy3DBar.png')
        # this.insert_image(160, 8, writer, sheetName, graph)
        # graph= os.path.join('../..', 'Graphs', this.collection, signalType, 'denoised.png')
        # this.insert_image(206, 8, writer, sheetName, graph)

        graph= os.path.join('../..', 'Graphs', this.collection, signalType, '%s_EnergyByFrequency_2D_Full.png' %(signalType))
        this.insert_image(24, 8, writer, sheetName, graph)
        graph= os.path.join('../..', 'Graphs', this.collection, signalType, '%s_PowerByFrequency_2D_Full.png' %(signalType))
        this.insert_image(54, 8, writer, sheetName, graph)
        graph= os.path.join('../..', 'Graphs', this.collection, signalType, '%s_TimeFreqEnergy_3D_Full.png' %(signalType))
        this.insert_image(84, 8, writer, sheetName, graph)
        graph= os.path.join('../..', 'Graphs', this.collection, signalType, '%s_TimeFreqPower_3D_Full.png' %(signalType))
        this.insert_image(124, 8, writer, sheetName, graph)
        graph= os.path.join('../..', 'Graphs', this.collection, signalType, 'denoiseSNRvsNoise3D.png')
        this.insert_image(154, 8, writer, sheetName, graph)
        graph= os.path.join('../..', 'Graphs', this.collection, signalType, 'denoiseSNRvsOrigReconSignal3D.png')
        this.insert_image(205, 8, writer, sheetName, graph)
        graph= os.path.join('../..', 'Graphs', this.collection, signalType, '%s_TimeFreqPower_contour.png' %(signalType))
        this.insert_image(240, 8, writer, sheetName, graph)
        graph= os.path.join('../..', 'Graphs', this.collection, signalType, '%s_TimeFreqEnergy_contour.png' %(signalType))
        this.insert_image(290, 8, writer, sheetName, graph)


        denoiseMultipliers_id= df.sort_values(by='DateTime', ascending=False)['%s_denoiseMultipliers' %(signalType)].head(1).values[0]
        cursor= this.coll.find({'Property': 'SignalDenoiseMultipliers', 'SignalType':signalType , '_id':denoiseMultipliers_id})
        df= pd.DataFrame(list(cursor))        
        col += 1
        pd.DataFrame({'SNR': df['MultipliersStat'][0]['SNR']}).to_excel(writer, sheet_name=sheetName, startcol=col, index=True, header=True)
        col += 2
        pd.DataFrame({'Er': df['MultipliersStat'][0]['Er']}).to_excel(writer, sheet_name=sheetName, startcol=col, index=False, header=True)
        col += 1
        pd.DataFrame({'RMSE': df['MultipliersStat'][0]['RMSE']}).to_excel(writer, sheet_name=sheetName, startcol=col, index=False, header=True)
        col += 1
        pd.DataFrame({'PRD': df['MultipliersStat'][0]['PRD']}).to_excel(writer, sheet_name=sheetName, startcol=col, index=False, header=True)
        col += 1
        pd.DataFrame({'R': df['MultipliersStat'][0]['R']}).to_excel(writer, sheet_name=sheetName, startcol=col, index=False, header=True)
        col += 1
        pd.DataFrame({'Mean': df['MultipliersStat'][0]['Mean']}).to_excel(writer, sheet_name=sheetName, startcol=col, index=False, header=True)
        col += 1
        pd.DataFrame({'WEE': df['MultipliersStat'][0]['WEE']}).to_excel(writer, sheet_name=sheetName, startcol=col, index=False, header=True)
        col += 1
        pd.DataFrame({'%EnergyInDetail': df['MultipliersStat'][0]['percEnergyInDetail']}).to_excel(writer, sheet_name=sheetName, startcol=col, index=False, header=True)
        col += 1
        pd.DataFrame({'%EnergyInApprox': df['MultipliersStat'][0]['percEnergyInApprox']}).to_excel(writer, sheet_name=sheetName, startcol=col, index=False, header=True)
        col += 1
        pd.DataFrame({'%EnergyInFilteredSignal': df['MultipliersStat'][0]['percEnergyInFilteredSignal']}).to_excel(writer, sheet_name=sheetName, startcol=col, index=False, header=True)
        col += 1
        pd.DataFrame({'%EnergyLossInNoise': df['MultipliersStat'][0]['percEnergyLossInNoise']}).to_excel(writer, sheet_name=sheetName, startcol=col, index=False, header=True)
        return col+1
        #worksheet.insert_image(position, graph)

    def print_cwt(this, writer, signalType):
        sheetame="%s_cwt" %(signalType)
        this.logger.info("printing %s" %(sheetame))
        cursor= this.coll.find({'Property': 'Signal', 'SensorId': this.sensorId, "%s_Status" %(signalType):{ '$in': ['C']}, "DateTime":{ '$lt': this.lastdt}}).sort("DateTime", pymongo.DESCENDING).limit(this.limitRecords)
        df= pd.DataFrame(list(cursor))
        cwt_id= df.sort_values(by='DateTime', ascending=False)['%s_cwt_d' %(signalType)].head(1).values[0]
        #df= pd.DataFrame(list(cursor))
        cursor= this.coll.find({'Property': 'SignalCWT', 'SignalType':signalType , '_id':cwt_id})
        df= pd.DataFrame(list(cursor))
        nWavelets= len(df['Frequency_cwt'][0].keys())
        #Frequency
        start= 0
        for wavelet in df['Frequency_cwt'][0].keys():
            pd.DataFrame({'Frequency_%s' %(wavelet): df['Frequency_cwt'][0][wavelet]}).transpose().to_excel(writer, sheet_name=sheetame, startcol=start, index=True)
            for f in range(0, len(df['Amplitudes_cwt'][0][wavelet])):
                idx=True
                pd.DataFrame({'Amplitude_%s' %(wavelet): df['Amplitudes_cwt'][0][wavelet][f]}).to_excel(writer, sheet_name=sheetame, startcol=start+f+1, startrow=3, index=False, header=False)
                idx=False

            rstart =1 
            this.insertGraph(writer, 'cwt3d_denoised_%s_%s.png' %(signalType, wavelet), signalType, 'AU2', sheetame, rstart, start + len(df['Amplitudes_cwt'][0][wavelet])+2)
            rstart += 27
            this.insertGraph(writer, 'cwt3d_denoised_%s_%s_2D.png' %(signalType, wavelet), signalType, 'AU2', sheetame, rstart, start + len(df['Amplitudes_cwt'][0][wavelet])+2)
            for j in [(0,41),(41,75),(75,106)]:  
                s= j[0]
                e= j[1]
                rstart += 27
                graphName1= "cwt3d_denoised_%s_%s_%d_%d" %(signalType, wavelet, s, e)
                this.insertGraph(writer, '%s.png' %(graphName1), signalType, 'AU2', sheetame, rstart, start + len(df['Amplitudes_cwt'][0][wavelet])+2)
                rstart += 27
                graphName1= "cwt3d_denoised_%s_%s_%d_%d_2D" %(signalType, wavelet, s, e)
                this.insertGraph(writer, '%s.png' %(graphName1), signalType, 'AU2', sheetame, rstart, start + len(df['Amplitudes_cwt'][0][wavelet])+2)

            start += len(df['Frequency_cwt'][0][wavelet]) + 17

        #pd.DataFrame({'Frequency': df['Frequency_cwt'][0]}).to_excel(writer, sheet_name=sheetname, startcol=1)
        
        #df['Frequency_cwt'].to_excel(writer, sheet_name=sheetname, startcol=1)

        #df[['Times_stft', 'Frequency_stft', 'Amplitudes_stft', 'PhaseAngles_stft', 'MaxAmpFrequency_stft', 'WindowFunction_stft']].to_excel(writer, sheet_name="stft")
        #df['Frequency_stft']= pd.to_numeric(df.Frequency_stft, errors='coerce')
        #pd.DataFrame({'Times': df['Times_stft'][0]}).to_excel(writer, sheet_name="stft", startcol=0)
        #pd.DataFrame({'Frequency': df['Frequency_stft'][0]}).to_excel(writer, sheet_name="stft", startcol=2, index=False)
        #for a in range(0, len(df['Amplitudes_stft'][0])):
            #pd.DataFrame({'Amplitude_%d' %(a+1): df['Amplitudes_stft'][0][a]}).to_excel(writer, sheet_name="stft", startcol=3+a, index=False)
        #for a in range(0, len(df['PhaseAngles_stft'][0])):
            #pd.DataFrame({'PhaseAngles_%d' %(a+1): df['PhaseAngles_stft'][0][a]}).to_excel(writer, sheet_name="stft", startcol=3+len(df['Amplitudes_stft'][0])+a, index=False)
        #pd.DataFrame({'Window Function': df['WindowFunction_stft']}).to_excel(writer, sheet_name="stft", startcol=3+(len(df['Amplitudes_stft'][0])*2)+2, index=False)

    def print_stft(this, writer, signalType):
        sheetName="%s_stft" %(signalType)
        this.logger.info("printing %s" %(sheetName))
        cursor= this.coll.find({'Property': 'Signal', 'SensorId': this.sensorId, "%s_Status" %(signalType):{ '$in': ['C']}, "DateTime":{ '$lt': this.lastdt}}).sort("DateTime", pymongo.DESCENDING).limit(this.limitRecords)
        df= pd.DataFrame(list(cursor))
        stft_id= df.sort_values(by='DateTime', ascending=False)['%s_stft' %(signalType)].head(1).values[0]
        #    df= pd.DataFrame(list(cursor))
        cursor= this.coll.find({'Property': 'SignalSTFT', 'SignalType':signalType , '_id':stft_id})
        df= pd.DataFrame(list(cursor))
        #df[['Times_stft', 'Frequency_stft', 'Amplitudes_stft', 'PhaseAngles_stft', 'MaxAmpFrequency_stft', 'WindowFunction_stft']].to_excel(writer, sheet_name=sheetName)
        #df['Frequency_stft']= pd.to_numeric(df.Frequency_stft, errors='coerce')
        #print df.columns.values.tolist()
        
        pd.DataFrame({'Times': df['Times_stft'][0]}).to_excel(writer, sheet_name=sheetName, startcol=0)
        pd.DataFrame({'Frequency': df['Frequency_stft'][0]}).to_excel(writer, sheet_name=sheetName, startcol=2, index=False)
        for a in range(0, len(df['Amplitudes_stft'][0])):
            pd.DataFrame({'Amplitude_%d' %(a+1): df['Amplitudes_stft'][0][a]}).to_excel(writer, sheet_name=sheetName, startcol=3+a, index=False)
        for a in range(0, len(df['PhaseAngles_stft'][0])):
            pd.DataFrame({'PhaseAngles_%d' %(a+1): df['PhaseAngles_stft'][0][a]}).to_excel(writer, sheet_name=sheetName, startcol=3+len(df['Amplitudes_stft'][0])+a, index=False)
        df['MaxAmpFrequency_stft'].to_excel(writer, sheet_name=sheetName, startcol=3+(len(df['Amplitudes_stft'][0])*2)+1, index=False)
        pd.DataFrame({'Window Function': df['WindowFunction_stft']}).to_excel(writer, sheet_name=sheetName, startcol=3+(len(df['Amplitudes_stft'][0])*2)+2, index=False)
        this.insertGraph(writer, 'stft3d_Time_Freq_Amp_%s.png' %(signalType), signalType, 'AU2', sheetName, 2, 3+(len(df['Amplitudes_stft'][0])*2)+4)

    def print_pca_kpca_ind(this, writer, signalType):
        cursor= this.coll.find({'Property': 'Signal', 'SensorId': this.sensorId, "%s_Status" %(signalType):{ '$in': ['C']}, "DateTime":{ '$lt': this.lastdt}}).sort("DateTime", pymongo.DESCENDING).limit(this.limitRecords)
        df= pd.DataFrame(list(cursor))
        col4= "%s_denoised" %(signalType)
        #col4= "%s_outliersremoved" %(signalType)
        denoised_signal= np.asarray(df[[col4]]).transpose()
        kpca_wavelet_id= df.sort_values(by='DateTime', ascending=False)['%s_kpcaDenoisedInd' %(signalType)].head(1).values[0]

        for wavelet in kpca_wavelet_id.keys():
            sheetname="%s_kpca_ind_%s" %(signalType, wavelet)
            this.logger.info("printing %s" %(sheetname))
            #pd.DataFrame({'OutliresRemoved_Signal->': ""}, index=[0]).to_excel(writer, sheet_name=sheetname, startrow=1, index=False)
            pd.DataFrame({'Denoised_Signal->': ""}, index=[0]).to_excel(writer, sheet_name=sheetname, startrow=1, index=False)
            pd.DataFrame(denoised_signal).to_excel(writer, sheet_name=sheetname, startrow=2, startcol=1, index=True)
            Kernels= []
            
            kpca_id= kpca_wavelet_id[wavelet]
            kpca_wavelet_dtls= this.coll.find({'Property': 'SignalKPCA', 'SignalType':signalType , '_id':kpca_id})
            r, c, r1= 3, 1, 4
            for rec in kpca_wavelet_dtls:
                mats= filter(lambda x: re.search('^1', x), rec.keys())
                for matrixName in sorted(mats, key=lambda x: int(x.split('_')[0])):
                    print matrixName, ",", type(rec[matrixName])

                    if re.search('^1', matrixName): 
                        if re.search('.*?_pca_kpca_Mean_NormDist_POAS_TSQ_.*', matrixName):  Kernels.append(matrixName.split('_')[-1])
                        pd.DataFrame({'%s->' %('_'.join(matrixName.split('_')[1:])): ""}, index=[0]).to_excel(writer, sheet_name=sheetname, startrow=r1, index=False)
                        #pd.DataFrame({'%s->' %(matrixName): ""}, index=[0]).to_excel(writer, sheet_name=sheetname, startrow=r1, index=False)
                        r1 += 2
                        if rec[matrixName] is None:
                            pd.DataFrame({'Duplicate of above': ""}, index=[0]).to_excel(writer, sheet_name=sheetname, startrow=r1, index=False)
                            r1 += 2
                        else: 
                            pd.DataFrame(np.asarray(rec[matrixName] if isinstance(rec[matrixName], list) else [rec[matrixName]])).to_excel(writer, sheet_name=sheetname, startrow=r1, index=True)        
                            r1 += (np.asarray((rec[matrixName])).shape)[0] + 2 if isinstance(rec[matrixName], list) else 4
            #if wavelet == 'denoised_ind': wavelet= 'haar'

            gr= 22
            this.insertGraph(writer, '%s_%s_%s.png' %('KPCA_Fleets', wavelet, signalType), signalType, 'AU2', sheetname, gr, 13)
            gr += 50
            this.insertGraph(writer, '%s_%s_%s.png' %('PCA_TSQ_NormDist', wavelet, signalType), signalType, 'AU2', sheetname, gr, 13)
            gr += 50
            this.insertGraph(writer, '%s_%s_%s.png' %('PCA_KPCA_TSQ_NormDist', wavelet, signalType), signalType, 'AU2', sheetname, gr, 13)
            gr += 50
            this.insertGraph(writer, '%s_%s_%s.png' %('PC1vsPC2', wavelet, signalType), signalType, 'AU2', sheetname, gr, 13)
            gr += 50
            this.insertGraph(writer, '%s_%s_%s.png' %('SrNovsPC1', wavelet, signalType), signalType, 'AU2', sheetname, gr, 13)
            gr += 50
            this.insertGraph(writer, '%s_%s_%s.png' %('PC1vsPC2vsPC3', wavelet, signalType), signalType, 'AU2', sheetname, gr, 13)
            gr += 50
            this.insertGraph(writer, '%s_%s_%s.png' %('PCA_Time_TSQ', wavelet, signalType), signalType, 'AU2', sheetname, gr, 13)
            gr += 50
            this.insertGraph(writer, '%s_%s_%s.png' %('PCA_TSQ_TSQCummPerc', wavelet, signalType), signalType, 'AU2', sheetname, gr, 13)
            gr += 50
            this.insertGraph(writer, '%s_%s_%s.png' %('PCA_KPCA_Time_TSQ', wavelet, signalType), signalType, 'AU2', sheetname, gr, 13)
            gr += 50
            this.insertGraph(writer, '%s_%s_%s.png' %('PCA_KPCA_TSQ_TSQCummPerc', wavelet, signalType), signalType, 'AU2', sheetname, gr, 13)
            gr += 50
            
            for k in Kernels:
                this.insertGraph(writer, '%s_%s_%s.png' %('KPCA_KPCA_TSQ_NormDist_%s' %(k), wavelet, signalType), signalType, 'AU2', sheetname, gr, 13)
                gr += 50
                this.insertGraph(writer, '%s_%s_%s.png' %('TimeVsKECA_%s' %(k), wavelet, signalType), signalType, 'AU2', sheetname, gr, 13)
                gr += 50
                this.insertGraph(writer, '%s_%s_%s.png' %('PC1vsPC2_%s' %(k), wavelet, signalType), signalType, 'AU2', sheetname, gr, 13)
                gr += 50
                this.insertGraph(writer, '%s_%s_%s.png' %('SrNovsPC1_%s' %(k), wavelet, signalType), signalType, 'AU2', sheetname, gr, 13)
                gr += 50
                this.insertGraph(writer, '%s_%s_%s.png' %('PC1vsPC2vsPC3_%s' %(k), wavelet, signalType), signalType, 'AU2', sheetname, gr, 13)
                gr += 50
                this.insertGraph(writer, '%s_%s_%s.png' %('KPCA_KPCA_Time_TSQ_%s' %(k), wavelet, signalType), signalType, 'AU2', sheetname, gr, 13)
                gr += 50
                this.insertGraph(writer, '%s_%s_%s.png' %('KPCA_KPCA_TSQ_TSQCummPerc_%s' %(k), wavelet, signalType), signalType, 'AU2', sheetname, gr, 13)
                gr += 50



    def print_pca_kpca_mul(this, writer, signalTypes, mongoCol, kpca_type):
        cursor= this.coll.find({'Property': 'Signal', 'SensorId': this.sensorId, "%s_Status" %(signalTypes[0]):{ '$in': ['C']}, "DateTime":{ '$lt': this.lastdt}}).sort("DateTime", pymongo.DESCENDING).limit(this.limitRecords)
        df= pd.DataFrame(list(cursor))
        kpca_wavelet_id= df.sort_values(by='DateTime', ascending=False)[mongoCol].head(1).values[0]
        sheetname=mongoCol
        this.logger.info("printing %s" %(sheetname))
        #pd.DataFrame({'OutliresRemoved_Signal->': ""}, index=[0]).to_excel(writer, sheet_name=sheetname, startrow=1, index=False)
        
        pd.DataFrame({'Denoised_Signals->': ""}, index=[0]).to_excel(writer, sheet_name=sheetname, startrow=1, index=False)
        row=3
        for signalType in signalTypes:
            col4= "%s_denoised" %(signalType)
            denoised_signal= np.asarray(df[[col4]]).transpose()
            pd.DataFrame(denoised_signal).to_excel(writer, sheet_name=sheetname, startrow=row, startcol=1 if row == 3 else 2 , index=True if row == 3 else False, header=True if row == 3 else False)
            row += 2 if row == 3 else 1

        signalType= kpca_type
        for wavelet in kpca_wavelet_id.keys():
            Kernels= []
            kpca_id= kpca_wavelet_id[wavelet]
            kpca_wavelet_dtls= this.coll.find({'Property': 'SignalKPCA', 'SignalType':signalType , '_id':kpca_id})
            c, r1= 1, row+1
            for rec in kpca_wavelet_dtls:
                mats= filter(lambda x: re.search('^1', x), rec.keys())
                for matrixName in sorted(mats, key=lambda x: int(x.split('_')[0])):
                    print matrixName, ",", type(rec[matrixName])

                    if re.search('^1', matrixName): 
                        if re.search('.*?_pca_kpca_Mean_NormDist_POAS_TSQ_.*', matrixName):  Kernels.append(matrixName.split('_')[-1])
                        pd.DataFrame({'%s->' %('_'.join(matrixName.split('_')[1:])): ""}, index=[0]).to_excel(writer, sheet_name=sheetname, startrow=r1, index=False)
                        #pd.DataFrame({'%s->' %(matrixName): ""}, index=[0]).to_excel(writer, sheet_name=sheetname, startrow=r1, index=False)
                        r1 += 2
                        if rec[matrixName] is None:
                            pd.DataFrame({'Duplicate of above': ""}, index=[0]).to_excel(writer, sheet_name=sheetname, startrow=r1, index=False)
                            r1 += 2
                        else: 
                            pd.DataFrame(np.asarray(rec[matrixName] if isinstance(rec[matrixName], list) else [rec[matrixName]])).to_excel(writer, sheet_name=sheetname, startrow=r1, index=True)        
                            r1 += (np.asarray((rec[matrixName])).shape)[0] + 2 if isinstance(rec[matrixName], list) else 4
            
            #if wavelet == kpca_type : wavelet= 'haar'
            gr= 22
            this.insertGraph(writer, '%s_%s_%s.png' %('PCA_TSQ_NormDist', wavelet, signalType), signalType, 'AU2', sheetname, gr, 13)
            gr += 50
            this.insertGraph(writer, '%s_%s_%s.png' %('PCA_KPCA_TSQ_NormDist', wavelet, signalType), signalType, 'AU2', sheetname, gr, 13)
            gr += 50
            this.insertGraph(writer, '%s_%s_%s.png' %('PC1vsPC2', wavelet, signalType), signalType, 'AU2', sheetname, gr, 13)
            gr += 50
            this.insertGraph(writer, '%s_%s_%s.png' %('SrNovsPC1', wavelet, signalType), signalType, 'AU2', sheetname, gr, 13)
            gr += 50
            this.insertGraph(writer, '%s_%s_%s.png' %('PC1vsPC2vsPC3', wavelet, signalType), signalType, 'AU2', sheetname, gr, 13)
            gr += 50
            this.insertGraph(writer, '%s_%s_%s.png' %('PCA_Time_TSQ', wavelet, signalType), signalType, 'AU2', sheetname, gr, 13)
            gr += 50
            this.insertGraph(writer, '%s_%s_%s.png' %('PCA_TSQ_TSQCummPerc', wavelet, signalType), signalType, 'AU2', sheetname, gr, 13)
            gr += 50
            this.insertGraph(writer, '%s_%s_%s.png' %('PCA_KPCA_Time_TSQ', wavelet, signalType), signalType, 'AU2', sheetname, gr, 13)
            gr += 50
            this.insertGraph(writer, '%s_%s_%s.png' %('PCA_KPCA_TSQ_TSQCummPerc', wavelet, signalType), signalType, 'AU2', sheetname, gr, 13)
            gr += 50
            
            for k in Kernels:
                this.insertGraph(writer, '%s_%s_%s.png' %('KPCA_KPCA_TSQ_NormDist_%s' %(k), wavelet, signalType), signalType, 'AU2', sheetname, gr, 13)
                gr += 50
                this.insertGraph(writer, '%s_%s_%s.png' %('TimeVsKECA_%s' %(k), wavelet, signalType), signalType, 'AU2', sheetname, gr, 13)
                gr += 50
                this.insertGraph(writer, '%s_%s_%s.png' %('PC1vsPC2_%s' %(k), wavelet, signalType), signalType, 'AU2', sheetname, gr, 13)
                gr += 50
                this.insertGraph(writer, '%s_%s_%s.png' %('SrNovsPC1_%s' %(k), wavelet, signalType), signalType, 'AU2', sheetname, gr, 13)
                gr += 50
                this.insertGraph(writer, '%s_%s_%s.png' %('PC1vsPC2vsPC3_%s' %(k), wavelet, signalType), signalType, 'AU2', sheetname, gr, 13)
                gr += 50
                this.insertGraph(writer, '%s_%s_%s.png' %('KPCA_KPCA_Time_TSQ_%s' %(k), wavelet, signalType), signalType, 'AU2', sheetname, gr, 13)
                gr += 50
                this.insertGraph(writer, '%s_%s_%s.png' %('KPCA_KPCA_TSQ_TSQCummPerc_%s' %(k), wavelet, signalType), signalType, 'AU2', sheetname, gr, 13)
                gr += 50

    def print_lda_kfda(this, writer, signalTypes, mongoCol, kpca_type):
        cursor= this.coll.find({'Property': 'Signal', 'SensorId': this.sensorId, "%s_Status" %(signalTypes[0]):{ '$in': ['C']}, "DateTime":{ '$lt': this.lastdt}}).sort("DateTime", pymongo.DESCENDING).limit(this.limitRecords)
        df= pd.DataFrame(list(cursor))
        kpca_wavelet_id= df.sort_values(by='DateTime', ascending=False)[mongoCol].head(1).values[0]

        sheetname=mongoCol
        this.logger.info("printing %s" %(sheetname))
        #pd.DataFrame({'OutliresRemoved_Signal->': ""}, index=[0]).to_excel(writer, sheet_name=sheetname, startrow=1, index=False)
        
        pd.DataFrame({'Denoised_Signals->': ""}, index=[0]).to_excel(writer, sheet_name=sheetname, startrow=1, index=False)
        row=3
        for signalType in signalTypes:
            col4= "%s_denoised" %(signalType)
            denoised_signal= np.asarray(df[[col4]]).transpose()
            pd.DataFrame(denoised_signal).to_excel(writer, sheet_name=sheetname, startrow=row, startcol=1 if row == 3 else 2 , index=True if row == 3 else False, header=True if row == 3 else False)
            row += 2 if row == 3 else 1

        signalType= kpca_type


        kpca_wavelet_dtls= this.coll.find({'Property': 'SignalLDAKFDA', 'SignalType':signalType , '_id':kpca_wavelet_id})
        c, r1= 1, row+1
        FDCounts, fleetCount= 0, 0
        for rec in kpca_wavelet_dtls:
            keys= rec['LDA_Keys']
            for matrixName in keys:
                if matrixName == 'LDA_FDFleets_Total': FDCounts= len(rec[matrixName][0])
                if matrixName == 'LDA_FDFleets_wMatrix1ToN': 
                    fleetCount= len(rec[matrixName])
                #print matrixName, ",", type(rec[matrixName])
                pd.DataFrame({'%s->' %(matrixName): ""}, index=[0]).to_excel(writer, sheet_name=sheetname, startrow=r1, index=False)
                r1 += 1
                if matrixName in ['LDA_FDFleets_wMatrix1ToN', 'LDA_fleetProjectionMeanXY']:
                    for xx in range(len(rec[matrixName])):
                        mdata= rec[matrixName][xx]
                        pd.DataFrame(np.asarray(mdata[0] if isinstance(mdata[0], list) else [mdata[0]])).to_excel(writer, sheet_name=sheetname, startcol=0, startrow=r1, index=False)   
                        pd.DataFrame(np.asarray(mdata[1] if isinstance(mdata[1], list) else [mdata[1]])).to_excel(writer, sheet_name=sheetname, startcol=1, startrow=r1, index=False)        
                        r1 += (np.asarray((mdata[0])).shape)[0] + 2 if isinstance(mdata, list) else 4            
                else:
                    pd.DataFrame(np.asarray(rec[matrixName] if isinstance(rec[matrixName], list) else [rec[matrixName]])).to_excel(writer, sheet_name=sheetname, startrow=r1, index=True)        
                    r1 += (np.asarray((rec[matrixName])).shape)[0] + 2 if isinstance(rec[matrixName], list) else 4

            keys= rec['KFDA_Keys']
            for matrixName in keys:
                pd.DataFrame({'%s->' %(matrixName): ""}, index=[0]).to_excel(writer, sheet_name=sheetname, startrow=r1, index=False)
                r1 += 1
                pd.DataFrame(np.asarray(rec[matrixName] if isinstance(rec[matrixName], list) else [rec[matrixName]])).to_excel(writer, sheet_name=sheetname, startrow=r1, index=True)        
                r1 += (np.asarray((rec[matrixName])).shape)[0] + 2 if isinstance(rec[matrixName], list) else 4

        gr= 22
        for i in range(FDCounts):        
            this.insertGraph(writer, 'LDA_TotalProjection_%s_TimeVsFD%d.png' %(signalType, i), signalType, 'AU2', sheetname, gr, 13)
            gr += 30

        for i in range(fleetCount):        
            for j in range(FDCounts):        
                this.insertGraph(writer, 'LDA_FleetProjection_%s_TimeVsFD%d_Fleet%d.png' %(signalType, j, i), signalType, 'AU2', sheetname, gr, 13)
                gr += 30

        for kernal in ['gaussian']:
            this.insertGraph(writer, 'KFDA_SrNo_KFD1_%s_%s.png' %(signalType, kernal), signalType, 'AU2', sheetname, gr, 13)
            gr += 30
            this.insertGraph(writer, 'KFDA_KFD1_KFD2_%s_%s.png' %(signalType, kernal), signalType, 'AU2', sheetname, gr, 13)
            gr += 30


        # gr += 50
        # this.insertGraph(writer, '%s_%s_%s.png' %('PCA_KPCA_TSQ_NormDist', wavelet, signalType), signalType, 'AU2', sheetname, gr, 13)
        # gr += 50


    def insertGraph(this, writer, graphName, signalType, position, sheetname, row=None, col=None):
        graph= os.path.join('../..', 'Graphs', this.collection, signalType, graphName)
        worksheet = writer.sheets[sheetname]

        if not os.path.exists(graph):
            this.logger.warning("Image:%s not found!" %(graph))
        else:
            worksheet.insert_image(row, col, graph)

            # if row is None:
            #     worksheet.insert_image(position, graph)
            #     pass
            # else:
            #     worksheet.insert_image(row, col, graph)
            #     pass

        # workbook = writer.book
        # #chart4 = workbook.add_chart({'type': 'scatter', 'subtype': 'smooth_with_markers'})
        # chart4 = workbook.add_chart({'type': 'scatter'})
        # # Configure the first series.
        # chart4.add_series({
        #     'name':       '=%s!$B$1' %(sheetname),
        #     'categories': '=%s!$A$2:$A$513' %(sheetname),
        #     'values':     '=%s!$B$2:$B$513' %(sheetname),
        # })

        # # Configure second series.
        # chart4.add_series({
        #     'name':       '=%s!$C$1' %(sheetname),
        #     'categories': '=%s!$A$2:$A$513' %(sheetname),
        #     'values':     '=%s!$C$2:$C$513' %(sheetname),
        # })

        # # Add a chart title and some axis labels.
        # chart4.set_title ({'name': signalType})

        # # Set an Excel chart style.
        # #chart4.set_style(14)

        # # Insert the chart into the worksheet (with an offset).
        # worksheet.insert_chart(position, chart4, {'x_offset': 25, 'y_offset': 10})

    def insertGraph_OutlierDenoised(this, writer, graphName, signalType, position, sheetname, row=None, col=None):
        # #return 0
        graph= os.path.join('../..', 'Graphs', this.collection, signalType, graphName)
        if not os.path.exists(graph):
            this.logger.warning("Image:%s not found!" %(graph))
        else:
            if row is None:
                worksheet = writer.sheets[sheetname]
                #worksheet.insert_image(position, graph)
            else:
                worksheet = writer.sheets[sheetname]
                #worksheet.insert_image(row, col, graph)
        workbook = writer.book
        #chart4 = workbook.add_chart({'type': 'scatter', 'subtype': 'smooth_with_markers'})
        chart4 = workbook.add_chart({'type': 'scatter'})
        # Configure the first series.
        chart4.add_series({
            'name':       '=%s!$C$1' %(sheetname),
            'categories': '=%s!$A$2:$A$513' %(sheetname),
            'values':     '=%s!$C$2:$C$513' %(sheetname),
        })

        # Configure second series.
        chart4.add_series({
            'name':       '=%s!$D$1' %(sheetname),
            'categories': '=%s!$A$2:$A$513' %(sheetname),
            'values':     '=%s!$D$2:$D$513' %(sheetname),
        })

        chart4.add_series({
            'name':       '=%s!$G$1' %(sheetname),
            'categories': '=%s!$A$2:$A$513' %(sheetname),
            'values':     '=%s!$G$2:$G$513' %(sheetname),
        })

        # Add a chart title and some axis labels.
        chart4.set_title ({'name': signalType})

        # Set an Excel chart style.
        #chart4.set_style(14)

        # Insert the chart into the worksheet (with an offset).
        worksheet.insert_chart(position, chart4, {'x_offset': 25, 'y_offset': 10})


if __name__ == "__main__":
        ob= report()
        ob.sendInExcel()

#python playground/well_info.py --wellname R
#db.R169.find({'Property':'Signal', 'SensorId':'wellhead_pressure', 'wellhead_pressure_Status':'C'}).projection({'DateTime_str':1, 'DateTime':1, 'wellhead_pressure':1}).sort({"DateTime":1})
#python -m reports.report --collection R169 --sensorid wellhead_pressure --lastdt 1228781940

