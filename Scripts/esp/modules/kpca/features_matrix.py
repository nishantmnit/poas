import numpy as np

from Scripts.esp.core.get_logger import get_logger
from Scripts.esp.modules.denoise.wavelets import decompose_signal
from scipy.interpolate import PchipInterpolator

logger = get_logger("kpca features matrix")
wavelet = "haar"


def decompose_interpolate(signal):
    approx, detail = decompose_signal(signal, wavelet=wavelet)
    new_x_axis = np.arange(0, len(signal), 2)
    missing_x_axis = np.arange(1, len(signal), 2)
    missing_y_axis = PchipInterpolator(new_x_axis, approx)(missing_x_axis)
    return np.vstack((approx, missing_y_axis)).ravel('F')


def interpolate_to_4096(signal):
    input_x_axis = np.arange(0, len(signal), 1)
    input_y_axis = signal
    sampling_time = len(signal) / 4096.
    new_x_axis = [i * sampling_time for i in range(0, 4096)]
    return PchipInterpolator(input_x_axis, input_y_axis)(new_x_axis)


def enrich(features):
    f1 = average = np.average(features, axis=2)  # average
    f2 = energy = np.sqrt((features ** 2).sum(axis=2) / features.shape[2])  # energy
    adjusted_data = features - average[:, :, np.newaxis]  # mean adjusted data
    f3 = standard_deviation = np.sqrt((adjusted_data ** 2).sum(axis=2) / (features.shape[2] - 1))  # standard deviation
    f4 = 0.5 * (np.max(features, axis=2) - np.min(features, axis=2))
    f5 = (np.sqrt(np.abs(features)).sum(axis=2) / features.shape[2]) ** 2  # absolute of sqrt(data)
    f6 = np.abs(features).sum(axis=2) / features.shape[2]  # absolute of data
    f7 = skewness = (adjusted_data ** 3).sum(axis=2) / ((features.shape[2] - 1) * standard_deviation ** 3)
    f8 = kurtosis = (adjusted_data ** 4).sum(axis=2) / ((features.shape[2] - 1) * standard_deviation ** 4)
    f9 = f4 / f2
    f10 = f4 / f5
    f11 = f2 / f6
    f12 = f4 / f6
    f13 = f3 / f6
    f14 = (1. / np.log10(f3)) * (np.log10(np.abs(features) + 1).sum(axis=2))
    f15 = np.sqrt(f5 / f3)
    f16 = ((np.exp(-1. * adjusted_data ** 2 / (2. * f3[:, :, np.newaxis] ** 2))) / (0.9 * np.sqrt(2. * np.pi))).sum(
        axis=2)
    adjusted_for_log = 0.148110755102455 * features ** 0.898333327667241
    adjusted_for_log[adjusted_for_log <= 10 ** -5] = 10 ** -5
    f17 = np.log10((0.85 / adjusted_for_log ** 0.85) * ((np.abs(features)) ** (0.85 - 1)) * (
        np.exp(np.abs(features) / adjusted_for_log)) ** 0.85)
    f17 = -1. * f17.sum(axis=2)
    features_matrix = np.concatenate((f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12, f13, f14, f15, f16, f17),
                                     axis=1)
    return features_matrix


def normalize(features_matrix):
    average = np.average(features_matrix, axis=0)
    adjusted_data = features_matrix - average  # mean adjusted data
    standard_deviation = np.sqrt((adjusted_data ** 2).sum(axis=0) / (features_matrix.shape[0] - 1))
    matrix = adjusted_data / standard_deviation
    return matrix


def create_feature_matrix(signal):
    features = list()
    for level in range(5):
        signal = decompose_interpolate(signal)
        signal_4096 = interpolate_to_4096(signal)
        feature = np.array_split(signal_4096, 16)
        features.append(feature)
    features = np.array(features).swapaxes(0, 1)
    features_matrix = enrich(features)
    normalized_features_matrix = normalize(features_matrix)
    return features_matrix, normalized_features_matrix


if __name__ == "__main__":
    import pandas as pd

    from Scripts.esp.playground.nishant.test_signal.signals import kpca_test_signal as kpca_test_signal
    from Scripts.pvt.core.storage.excel import write_excel
    from Scripts.esp.settings import DEBUG_DIR

    output, normalized = create_feature_matrix(kpca_test_signal)
    write_excel(True, DEBUG_DIR, ["feature_matrix"], ["features", "normalized"],
                [pd.DataFrame(output), pd.DataFrame(normalized)])
