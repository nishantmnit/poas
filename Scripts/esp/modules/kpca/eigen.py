import math
import numpy as np


def get_projected_eigen_values(max_eigen_val, min_eigen_val):
    projected_eigen_values = []
    projected_eigen = max_eigen_val + max_eigen_val * 10. / 100.
    c = 0
    while True:
        if isinstance(projected_eigen, np.float64) and projected_eigen == 0:
            projected_eigen = 10 ** -6
        projected_eigen_values.append(projected_eigen)
        t = (-0.428430221872081 * math.log(projected_eigen) + 2.31279506862577) * 0.15
        percent_change = 0.05 if t < 0.05 else t
        if projected_eigen < min_eigen_val:
            c += 1
            if c == 6:
                break
        projected_eigen = projected_eigen * (1. - percent_change / 100.)
    return projected_eigen_values


def get_max_min_eigen_value(matrix):
    eigen_values = np.linalg.eigvals(matrix)
    max_eigen_value = max(eigen_values.real)
    if max_eigen_value < 10. ** -6:
        max_eigen_value = 10. ** -6
    return max_eigen_value, 0.000001


def get_eigen_values(matrix):
    matrix_size = matrix.shape[0]
    max_eigen_val, min_eigen_val = get_max_min_eigen_value(matrix)
    projected_eigen_values = get_projected_eigen_values(max_eigen_val, min_eigen_val)
    logic = []
    prev_pf = 0
    im = np.identity(matrix_size)
    for p in projected_eigen_values:
        pf = np.linalg.det(matrix - p * im)
        mf = prev_pf * pf
        logic.append(1 if mf < 0 else 0)
        prev_pf = pf

    eig_values = []
    for i in np.nonzero(np.asarray(logic))[0]:
        mx, mn = projected_eigen_values[i - 1:i + 1]
        avg = (mx + mn) / 2.
        while True:
            fx = np.linalg.det(matrix - mx * im)
            fa = np.linalg.det(matrix - avg * im)

            nmx = mx if fx * fa < 0 else avg
            nmn = avg if fx * fa < 0 else mn
            n_avg = (nmx + nmn) / 2.
            df = nmx - nmn
            if df < 0.00000000001:
                eig_values.append(nmx)
                break
            mx, mn, avg = nmx, nmn, n_avg
    return eig_values


def get_eigen(matrix):
    matrix_size = matrix.shape[0]
    eigen_values = get_eigen_values(matrix)
    eig_vectors = []
    im = np.identity(matrix_size)
    for v in eigen_values:
        cramer_matrix = []
        lambda_matrix = matrix - v * im
        last_val = 10 ** -6 if lambda_matrix[matrix_size - 1, matrix_size - 1] == 0 else 10. ** -5. / lambda_matrix[
            matrix_size - 1, matrix_size - 1]
        for i in range(0, matrix_size - 1):
            if i == 0:
                cramer_matrix.append(lambda_matrix[0, :])
            a1 = lambda_matrix[0:, i]
            a2 = lambda_matrix[0, i]
            r2 = np.zeros(len(a1)) if a2 == 0 else np.apply_along_axis(lambda x, y: x / y, 0, a1, a2)
            lambda_matrix_1 = np.apply_along_axis(lambda x, y, z, i: x[1:] - y * np.repeat(x[0], len(x) - 1), 0,
                                                  lambda_matrix, r2[1:], matrix_size, i)
            cramer_matrix.append(lambda_matrix_1[0, :])
            lambda_matrix = lambda_matrix_1
        cramer_matrix = np.asarray(cramer_matrix)
        if abs(cramer_matrix[matrix_size - 1, matrix_size - 1]) >= abs(last_val):
            cramer_matrix[matrix_size - 1, matrix_size - 1] = last_val

        eig_vector = [cramer_matrix[matrix_size - 1][matrix_size - 1]]
        for i in range(matrix_size - 2, -1, -1):
            eig_vector.insert(0, 10 ** -6 if cramer_matrix[i][i] == 0 else (
                        -1. * sum(cramer_matrix[i][i + 1:] * eig_vector) / cramer_matrix[i][i]))
        eig_vector = np.asarray(eig_vector)
        eig_vectors.append(eig_vector / math.sqrt(sum(eig_vector ** 2.)))
    return np.asarray(eigen_values), np.asarray(eig_vectors).transpose()


def check_symmetric(a, rtol=1e-05, atol=1e-08):
    return np.allclose(a, a.T, rtol=rtol, atol=atol)


if __name__ == "__main__":
    test_eigen_values, test_eigen_vectors = get_eigen(np.array([[0.448812123, 0.444445896], [0.444445896, 0.442222944]]))
    print test_eigen_values, test_eigen_vectors
