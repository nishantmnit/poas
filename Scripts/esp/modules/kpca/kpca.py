import numpy as np
import os

from Scripts.esp.core.get_logger import get_logger
from features_matrix import create_feature_matrix
from Scripts.esp.settings import CSV_DATA
from eigen import get_eigen

logger = get_logger("kpca")


def centralize(kernel):
    ones_matrix = np.ones(kernel.shape)
    count = kernel.shape[0]
    kernel = kernel - 1. / count * (np.dot(ones_matrix, kernel)) - 1. / count * (np.dot(kernel, ones_matrix)) + (
            1. / count ** 2) * (np.dot(ones_matrix, np.dot(kernel, ones_matrix)))
    return kernel


def gaussian_kernel(features_matrix, field):
    sigma_values = pd.read_csv(os.path.join(CSV_DATA, "gaussian_sigma.csv"))
    if sigma_values.loc[sigma_values.field == field].shape[0] == 1:
        sigma_value = sigma_values.loc[sigma_values.field == field, "sigma"].values[0]
    else:
        sigma_value = default_sigma = 0.032
    kernel = list()
    for column in range(features_matrix.shape[1]):
        kernel.append(
            np.exp(((features_matrix - features_matrix[:, column, np.newaxis]) ** 2).sum(axis=0) * -1. * sigma_value))
    return kernel


def normalize_eigen(eigen_values, eigen_vector):
    return np.sqrt(eigen_values) * eigen_vector


def kpca(signal_df, field):
    if signal_df.shape[0] > 4096:
        raise ValueError("KPCA support maximum 4096 signals at a time.")
    logger.debug("kpca started")
    signal = signal_df.y_axis.values
    features_matrix, normalized_feature_matrix = create_feature_matrix(signal)
    kernel = gaussian_kernel(normalized_feature_matrix, field)
    centralized_kernel = centralize(np.array(kernel))
    eigen_values, eigen_vector = get_eigen(centralized_kernel)
    normalized_eigen_vector = normalize_eigen(eigen_values, eigen_vector)
    projected_matrix = np.matmul(centralized_kernel, normalized_eigen_vector)
    logger.debug("kpca completed")
    return features_matrix, normalized_feature_matrix, kernel, centralized_kernel, eigen_values, eigen_vector, normalized_eigen_vector, projected_matrix


if __name__ == "__main__":
    import pandas as pd

    from Scripts.esp.playground.nishant.test_signal.signals import kpca_test_signal as kpca_test_signal
    from Scripts.pvt.core.storage.excel import write_excel
    from Scripts.esp.settings import DEBUG_DIR

    test_x_axis = [i * 1 for i in range(len(kpca_test_signal))]
    test_signal_df = pd.DataFrame(data=list(zip(kpca_test_signal, test_x_axis)), columns=["y_axis", "x_axis"])
    features_matrix, normalized_feature_matrix, kernel, centralized_kernel, eigen_values, eigen_vector, normalized_eigen_vector, projected_matrix = kpca(
        test_signal_df, "electric current")
    write_excel(True, DEBUG_DIR, ["KPCA"],
                ["features_matrix", "normalized_feature_matrix", "kernel", "centralized_kernel", "eigen_values",
                 "eigen_vector", "normalized_eigen_vector", "projected_matrix"],
                [pd.DataFrame(features_matrix), pd.DataFrame(normalized_feature_matrix), pd.DataFrame(kernel),
                 pd.DataFrame(centralized_kernel), pd.DataFrame(eigen_values), pd.DataFrame(eigen_vector),
                 pd.DataFrame(normalized_eigen_vector), pd.DataFrame(projected_matrix)])
