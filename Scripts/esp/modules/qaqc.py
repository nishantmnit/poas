import pandas as pd

from Scripts.pvt.core.optimizers.poas_interpolator import monotone


def qa_qc(df):
    df["create_signal"] = df.received_after / df.frequency - 1
    df["interpolated"] = 0
    df["bad_signal"] = 0
    if df.loc[df.create_signal > 0].shape[0] > 0:
        x = df.x_axis.tolist()
        y = df.y_axis.tolist()
        x_axis = list()
        for index, row in df.loc[df.create_signal > 0].iterrows():
            while row.create_signal > 0:
                x_axis.append(row.x_axis - row.create_signal * row.frequency)
                row["create_signal"] = row.create_signal - 1
        y_axis = monotone(x, y, x_axis)
        bad_signal = [0 for i in range(len(y_axis))]
        if len(x_axis) > 25:
            y_axis[25:] = [10 ** -20 for i in range(len(y_axis[25:]))]
            bad_signal = [1 for i in range(len(y_axis))]

        interpolated_data = pd.DataFrame(columns=["x_axis", "y_axis", "bad_signal"],
                                         data=list(zip(x_axis, y_axis, bad_signal)))
        interpolated_data["interpolated"] = 1
        df = pd.concat([df, interpolated_data])
        df = df.sort_values(by=["x_axis"], ascending=True).reset_index(drop=True)
    return df
