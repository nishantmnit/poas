#!/usr/bin/env python

from Scripts.esp.core.eigen import *
from Scripts.esp.core.storage.fetch_most import lda_kfda_insert
from Scripts.esp.modules.kpca import *
from Scripts.pvt.core.optimizers.poas_interpolator import *


class lda_kfda(kpca):
    def __init__(this, service=None, collection=None, sensorid=None, logger=None, signalDateTimeFormat='%d/%m/%Y %H:%M'):
        super(lda_kfda, this).__init__(service, collection, logger, signalDateTimeFormat)
        this.signalDateTimeFormat= signalDateTimeFormat
        this.collection= collection
        this.service= service
        this.logger= logger
        this.dateYmdHms= datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
        this.sensorId= sensorid
        this.db= getDbCon(service)
        this.coll= this.db[collection] 
        # setattr(this.args, 'debug', True)

    def interpolateSig512To1024(this, signal):
        sigLen= len(signal)
        s1= signal[:].tolist()
        sr1m= np.arange(1, sigLen*2, 2).tolist()
        r= PchipInterpolator(np.arange(0, sigLen*2, 2), s1)(sr1m)
        for m in sr1m[:]: s1.insert(m, r[sr1m.index(m)])  #cA1-len-512
        return s1

    def convertLDAOutputToList(this, kpcaOutput):
        kpcaOutput1= dict()
        for k in kpcaOutput:
            kpcaOutput1[k]= kpcaOutput[k].tolist() if isinstance(kpcaOutput[k], np.ndarray) else kpcaOutput[k]                
        return kpcaOutput1

    def getProjectionMeanXY(this, startX, FDFleets, totalProjFD, meanIndex):
        totalProjectionMeanXY=[]
        for i in range(FDFleets.shape[1]):
            FDFleet= np.asarray([np.arange(startX, startX+len(FDFleets[:,i])).tolist(), FDFleets[:,i].tolist()]).transpose()
            FDFleet=FDFleet[FDFleet[:,1].argsort()]
            r= monotone(FDFleet[:,1].tolist(), FDFleet[:,0].tolist(), [totalProjFD[meanIndex,i]])[0]
            totalProjectionMeanXY.append([r, totalProjFD[meanIndex,i]])
        return totalProjectionMeanXY

    def getLDAEigenFromFleets(this, cAs, iskfda=False):
        ltype= "KFDA" if iskfda else "LDA"
        ldaOutput= OrderedDict()
        ldaOutput['%s_InterpolatedSignals' %(ltype)]= cAs
        cAsLen= cAs.shape[0]
        cAsTotalMean= np.mean(cAs, axis=0)  
        fleetsMean=[]  
        sW=[] # Within class matrix's
        sB=[]
        sWTotal=None
        sBTotal= None
        #cAs= 1024 x 10, cAs= 40 x 10
        fleetSize= 256 
        if iskfda: fleetSize= 10

        for fleet in np.array_split(cAs, cAsLen/fleetSize, axis=0):
            # for ff in fleet:
            #     for f in ff: print f, ",",
            #     print ""
            fleetsMean1= np.mean(fleet, axis=0)    
            fleetsMean.append(fleetsMean1)
            fleetMeanDiff= fleet - fleetsMean1
            withinClassMatrix= np.matmul(fleetMeanDiff.transpose(), fleetMeanDiff)
            sW.append(withinClassMatrix)
            sWTotal= withinClassMatrix.copy() if sWTotal is None else sWTotal+withinClassMatrix
            fleetMean_totalMean= fleetsMean1 - cAsTotalMean
            sB1= fleetMean_totalMean.reshape((-1, 1)) * fleetMean_totalMean
            if not np.any(sB1): sB1[sB1==0.]= 10**-20
            sB.append(sB1)
            sBTotal= sB1.copy() if sBTotal is None else sBTotal+sB1

        wMatrix= None
        if iskfda:
            wMatrix= np.matmul(np.linalg.pinv(sWTotal), sBTotal)
            wMatrixMaxVal= np.max(wMatrix)
            wMatrix= (wMatrix/wMatrixMaxVal)*5. if wMatrix.shape[1] <= 5 else (wMatrix/wMatrixMaxVal)*wMatrix.shape[1]
        else:
            wMatrix= np.matmul(np.linalg.pinv(sWTotal), sBTotal)
            
        ldaOutput['%s_wMatrix' %(ltype)]= wMatrix

        ob=eigen(wMatrix)
        wMatrix_eigenVal, wMatrix_eigenVector= ob.getEigen()
        ldaOutput['%s_wMatrix_eigenVal_Python' %(ltype)]= [str(i) for i in np.linalg.eigvals(wMatrix)]
        ldaOutput['%s_wMatrix_eigenVal' %(ltype)]= wMatrix_eigenVal
        ldaOutput['%s_wMatrix_eigenVector' %(ltype)]= wMatrix_eigenVector
        if iskfda: return ldaOutput.keys(), ldaOutput

        #Total Projection on wMatrix:
        totalProjectionInputMat= np.append([cAsTotalMean], fleetsMean, axis=0)
        totalProjectionInputMat= np.append(totalProjectionInputMat, cAs, axis=0)
        totalProjFD= np.matmul(totalProjectionInputMat, wMatrix_eigenVector) #Fisher Discriminant
        ldaOutput['LDA_TotalProjFD']= totalProjFD  #not required in mongo

        FDFleets= totalProjFD[-cAsLen:,:]
        ldaOutput['LDA_FDFleets_Total']= FDFleets
        totalProjectionMeanXY= this.getProjectionMeanXY(0, FDFleets, totalProjFD, 0)
        ldaOutput['LDA_totalProjectionMeanXY']= totalProjectionMeanXY

        #Total Projection on Fleet wMatrix:
        fleetTotalProjectionMeanXY=[]
        fi= 0
        startX=0
        for FDFleet in np.array_split(FDFleets, cAsLen/fleetSize, axis=0):
            fi += 1 
            r=[]
            [r.extend(x) for x in this.getProjectionMeanXY(startX, FDFleet, totalProjFD, fi)]
            fleetTotalProjectionMeanXY.append(r)
            startX += len(FDFleet)
        ldaOutput['LDA_fleetTotalProjectionMeanXY']= np.array(fleetTotalProjectionMeanXY)

        # print "\nfleetTotalProjectionMeanXY:"       
        # for fp in fleetTotalProjectionMeanXY: print fp
        # for ii in sW[1]:
        #     for z in ii:
        #         print z, ",",
        #     print ""
        wMatrixN_eigenVector= []
        for sWi in range(len(sW)):
            inv= np.linalg.pinv(sW[sWi])
            #print inv
            w1= np.matmul(inv, sBTotal)
            ob=eigen(w1)
            wMatrix_eigenVal, wMatrix_eigenVector= ob.getEigen()
            wMatrixN_eigenVector.append(wMatrix_eigenVector)
            ldaOutput['LDA_wMatrix%d' %(sWi+1)]= w1
            ldaOutput['LDA_wMatrix%d_eigenVal_Python' %(sWi+1)]= [str(i) for i in np.linalg.eigvals(w1)] 
            ldaOutput['LDA_wMatrix%d_eigenVal' %(sWi+1)]= wMatrix_eigenVal
            ldaOutput['LDA_wMatrix%d_eigenVector' %(sWi+1)]= wMatrix_eigenVector

        #Fleet Projection on wMatrix1, wMatrix2 ... wMatrixN:
        fleetProjectionMeanXY=[]
        iv=0
        ldaOutput['LDA_FDFleets_wMatrix1ToN']= []
        startX=0
        for fleet in np.array_split(cAs, cAsLen/fleetSize, axis=0):
            fleetLen= fleet.shape[0]
            fleetsMean1= np.mean(fleet, axis=0)    
            totalProjectionInputMat= np.append([fleetsMean1], fleet, axis=0)
            totalProjFD= np.matmul(totalProjectionInputMat, wMatrixN_eigenVector[iv]) #Fisher Discriminant
            FDFleets= totalProjFD[-fleetLen:,:]
            iv += 1
            ldaOutput['LDA_FDFleets_wMatrix1ToN'].append([np.arange(startX, startX+fleetLen).tolist(),FDFleets.tolist()])
            fleetProjectionMeanXY.append(this.getProjectionMeanXY(startX, FDFleets, totalProjFD, 0))
            startX += fleetLen

        ldaOutput['LDA_fleetProjectionMeanXY']= fleetProjectionMeanXY

        # print "\nfleetProjectionMeanXY:"       
        # for fp in fleetProjectionMeanXY: print fp
    
        return ldaOutput.keys(), ldaOutput

    def getFleetsFinalMatrix(this, fleets):
        fleetsLen= fleets.shape[0]
        fleetsMean= np.average(fleets, axis=0)
        fleetsAdj= fleets - fleetsMean
        fleetsAdj2= fleetsAdj ** 2
        fleetsAdjStdDev= np.sqrt(np.sum(fleetsAdj2, axis=0)/(fleetsLen-1))
        fleetsAdjStdDevMesh= np.meshgrid(np.arange(0, fleetsLen), fleetsAdjStdDev)[1].transpose()
        fleetsFinalMat= np.divide(fleetsAdj, fleetsAdjStdDevMesh, out=np.zeros_like(fleetsAdj), where=fleetsAdjStdDevMesh!=0).transpose()
        return fleetsFinalMat

    def getKFDAEigenFromFleets(this, cAs):
        kfdaOutput= OrderedDict()
        kfdaOutput['KFDA_InterpolatedSignals']= cAs
        cAsLen= cAs.shape[0]
        cAsTotalMean= np.mean(cAs, axis=0)  
        fleetsMean=[]  
        sW=[] # Within class matrix's
        sB=[]
        sWTotal=None
        sBTotal= None

        kpca_projectedMatrix_all=[]
        kpca_projectedMatrix_minCol= None
        #for kernal in (['gaussian', 'exponential', 'linear', 'sigmoid']):
        for kernal in (['gaussian']): #update in graph plotting as well. Or take it in this.kernal
            k, allKpcaProjectedMats= 1, None
            for fleets in np.array_split(cAs, cAsLen/256, axis=0):
                
                fleetsFinalMat= this.getFleetsFinalMatrix(fleets)
                normalizedkernalMatrix, eigenVal, eigenVect= this.eigenValVectOfFinalMat(fleetsFinalMat, kernal)
                # print "\nnormalizedkernalMatrix:\n"
                # for ff in normalizedkernalMatrix: 
                #     for f in ff: print f,",",
                #     print ""

                # print "\neigenVal:\n"
                # print eigenVal
                    
                # print "\neigenVect:\n"
                # for ff in eigenVect: 
                #     for f in ff: print f,",",
                #     print ""

                eigenVal= np.asarray(eigenVal)                    
                normalizedEigenVector1= np.sqrt(eigenVal) * eigenVect
                kpca_projectedMatrix=  np.matmul(normalizedkernalMatrix, normalizedEigenVector1)
                kfdaOutput['KFDA_kpca_projMatrix%d_%s' %(k, kernal)]= kpca_projectedMatrix.tolist()
                if kpca_projectedMatrix_minCol is None: kpca_projectedMatrix_minCol= kpca_projectedMatrix.shape[1] 
                if kpca_projectedMatrix.shape[1] < kpca_projectedMatrix_minCol: kpca_projectedMatrix_minCol= kpca_projectedMatrix.shape[1] 
                kpca_projectedMatrix_all.append(kpca_projectedMatrix)
                #this.printNumpyArray(allKpcaProjectedMats)
                k += 1

            kpca_projectedMatrix_all1=None
            for km in kpca_projectedMatrix_all:
                # print km[:,0:kpca_projectedMatrix_minCol].shape
                kpca_projectedMatrix_all1= km[:,0:kpca_projectedMatrix_minCol] if kpca_projectedMatrix_all1 is None else np.concatenate([kpca_projectedMatrix_all1, km[:,0:kpca_projectedMatrix_minCol]], 0)
            # print kpca_projectedMatrix_all1.shape
            # print "\nkpca_projectedMatrix_all1:\n"
            # for ff in kpca_projectedMatrix_all1: 
            #     for f in ff: print f,",",
            #     print ""

            kfdaOutput["KFDA_kpca_projMatrixLDAInput"]= kpca_projectedMatrix_all1.tolist()
            keys, ldaOutput= this.getLDAEigenFromFleets(kpca_projectedMatrix_all1, iskfda=True)
            projectedKFDMatrix= np.matmul(kpca_projectedMatrix_all1, ldaOutput['KFDA_wMatrix_eigenVector'])

            # print "projectedKFDMatrix:"
            # print projectedKFDMatrix
            projectedKFDMatrix= projectedKFDMatrix * np.sqrt(np.array(ldaOutput['KFDA_wMatrix_eigenVal']).transpose())
            # print projectedKFDMatrix
            kfdaOutput['KFDA_projectedKFDMatrix_%s' %(kernal)]= projectedKFDMatrix.tolist()
        return kfdaOutput.keys(), kfdaOutput


    def getEigen_denoised_ca1(this, wavelet, signals, diffInSeconds, signalType=None):
        return this.getEigen_denoised_caN(wavelet, signals, diffInSeconds, 1, signalType)

    def getEigen_denoised_ca2(this, wavelet, signals, diffInSeconds, signalType=None):
        return this.getEigen_denoised_caN(wavelet, signals, diffInSeconds, 2, signalType)

    def plotKfdaFda(this, type, keys, ldaOutput, datetimes):
        try:
            for kernal in ['gaussian']:
                projectedKFDMatrix= np.array(ldaOutput['KFDA_projectedKFDMatrix_%s' %(kernal)])
                d, l=[], []
                d.append([np.arange(len(projectedKFDMatrix[:,0])), [projectedKFDMatrix[:,0]]])
                l.append('KFD1')
                g = rts_graphs(this.collection, this.logger)
                graphName= 'KFDA_SrNo_KFD1_%s_%s' %(type, kernal)
                title= 'SrNo_KFD1_%s' %(kernal)
                g.plotTotalProjectionFleets("RTS", this.collection, d, l, graphName, this.signalType, title, "SrNo", "KFD1")

                d, l=[], []
                d.append([projectedKFDMatrix[:,0], projectedKFDMatrix[:,1]])
                l.append('KFD')
                g = rts_graphs(this.collection, this.logger)
                graphName= 'KFDA_KFD1_KFD2_%s_%s' %(type,kernal)
                title= 'KFD1_KFD2_%s' %(kernal)
                g.plotTotalProjectionFleets("RTS", this.collection, d, l, graphName, this.signalType, title, "KFD1", "KFD2")
        except Exception as e:
            this.logger.warning("Error in plotKfdaFda, Error:%s" %(e.message))



    def plotLdaFda(this, type, keys, ldaOutput, datetimes):
        try:        
            FDCounts= ldaOutput['LDA_FDFleets_Total'].shape[1]
            for i in range(FDCounts):
                d, l=[], []
                y1= ldaOutput['LDA_FDFleets_Total'][:,i]  #Fleet i
                x1= np.arange(len(y1))  #time
                d.append([[x1], [y1]])
                l.append('FD')
                y2= ldaOutput['LDA_totalProjectionMeanXY'][i][1]  #TotalMean Of Fleet i, 
                x2= ldaOutput['LDA_totalProjectionMeanXY'][i][0]  #predicted X 
                d.append([[x2], [y2]])
                l.append('TotalMean')
                y3= ldaOutput['LDA_fleetTotalProjectionMeanXY'][:,i*2+1]  #FleetMean Of Fleet i, 
                x3= ldaOutput['LDA_fleetTotalProjectionMeanXY'][:,i*2]  #predicted X 
                d.append([[x3], [y3]])
                l.append('FleetMean')
                g = rts_graphs(this.collection, this.logger)
                graphName= 'LDA_TotalProjection_%s_TimeVsFD%d' %(type, i)
                title= 'Total FD%d by Time' %(i+1)
                g.plotTotalProjectionFleets("RTS", this.collection, d, l, graphName, this.signalType, title)
                # d, l=[], []
                # y1= ldaOutput['LDA_FDFleets_wMatrix1ToN'][:,i]  #Fleet i
                # x1= np.arange(len(y1))  #time
                # d.append([[x1], [y1]])
                # l.append('Fleet FD')
                # y3= ldaOutput['LDA_fleetProjectionMeanXY'][:,i*2+1]  #FleetMean Of Fleet i, 
                # x3= ldaOutput['LDA_fleetProjectionMeanXY'][:,i*2]  #predicted X 
                # d.append([[x3], [y3]])
                # l.append('FleetMean')
                # g = rts_graphs(this.collection, this.logger)
                # graphName= 'FleetProjection_%s_TimeVsFD_%d' %(type, i)
                # g.plotTotalProjectionFleets("RTS", this.collection, d, l, graphName, this.signalType, "Fleet FD by Time")
            for j in range(len(ldaOutput['LDA_FDFleets_wMatrix1ToN'])):
                fleet= ldaOutput['LDA_FDFleets_wMatrix1ToN'][j][1]
                x1= ldaOutput['LDA_FDFleets_wMatrix1ToN'][j][0]
                FDCounts= len(fleet[0])
                for i in range(FDCounts):
                    d, l=[], []
                    y1= np.array(fleet)[:,i]  #Fleet i
                    d.append([[x1], [y1]])
                    l.append('Fleet FD')
                    y3= ldaOutput['LDA_fleetProjectionMeanXY'][j][i][1]  #FleetMean Of Fleet i, 
                    x3= ldaOutput['LDA_fleetProjectionMeanXY'][j][i][0]  #predicted X 
                    d.append([[x3], [y3]])
                    l.append('FleetMean')
                    g = rts_graphs(this.collection, this.logger)
                    graphName= 'LDA_FleetProjection_%s_TimeVsFD%d_Fleet%d' %(type, i, j)
                    title= "Fleet%d FD%s by Time" %(j+1, i+1)
                    g.plotTotalProjectionFleets("RTS", this.collection, d, l, graphName, this.signalType, title)
        except Exception as e:
            this.logger.warning("Error in plotLdaFda, Error:%s" %(e.message))

    def getEigen_denoised_mul(this, wavelet, signals, diffInSeconds, signalType=None):
        #print "Denoised Signals shape:=>%s" %(len(signals))
        #print signals.shape
        cAs= []
        for i in range(0, len(signals)):
            #print "Denoised signal %d length:=>%d" %(i, len(signals[i]))
            c= this.interpolateSig512To1024(signals[i])
            cAs.append(c)
            #this.printNumpyArray(signals[i])
        cAs= np.array(cAs).transpose()

        #print "getEigen_denoised_mul fleets Size:", cAs.shape
        keys, ldaOutput= this.getLDAEigenFromFleets(cAs)
        keys1, kfdaOutput= this.getKFDAEigenFromFleets(cAs)
        ldaOutput['LDA_Keys']= keys
        kfdaOutput['KFDA_Keys']= keys1
        return keys, ldaOutput, keys1, kfdaOutput

    def getEigen_denoised_caN(this, wavelet, signals, diffInSeconds, maxDecompLen, signalType=None):
        kpcaOutput= dict()
        cAss= np.array([])
        #print "Denoised %d Signals:=>" %(len(signals))
        for i in range(0, len(signals)): 
            #print "Denoised signal %d length:=>%d" %(i, len(signals[i]))
            c= this.getcAsDecomposed_512('haar', signals[i], maxDecompLen)
            c= np.array([c[:,-1]]).transpose()
            if cAss.size == 0:
                cAss= c.copy()
            else:
                cAss= np.concatenate([cAss, c], 1)
            #this.printNumpyArray(signals[i])
        
        cAs= []    
        for i in range(len(cAss[0])):
            c= this.interpolateSig512To1024(cAss[:,i])
            cAs.append(c)
        cAs= np.array(cAs).transpose()    
        #print "getEigen_denoised_caN fleets Size:", cAs.shape

        #print cAs.shape
        keys, ldaOutput= this.getLDAEigenFromFleets(cAs)
        keys1, kfdaOutput= this.getKFDAEigenFromFleets(cAs)
        ldaOutput['LDA_Keys']= keys
        kfdaOutput['KFDA_Keys']= keys1
        return keys, ldaOutput, keys1, kfdaOutput

    def svm(this, signalOrSignals, datetimes, type, signalType): 
        this.signalType= signalType
        sigLen= len(signalOrSignals)
        this.getLimitedWaveletFamily()
        diffInSeconds, samplFreq= getSamplingFrequency(datetimes[0], datetimes[1])
        wavelet= 'haar'
        this.kpca_wavelet= wavelet
        keys, ldaOutput, keys1, kfdaOutput= getattr(this, "getEigen_%s" %(type))(wavelet, signalOrSignals, diffInSeconds, signalType)
        #if hasattr(this, 'args') and this.args.debug: this.plotLdaFda(type, keys, ldaOutput, datetimes)
        #if hasattr(this, 'args') and this.args.debug: this.plotKfdaFda(type, keys1, kfdaOutput, datetimes)
        ldaOutput= this.convertLDAOutputToList(ldaOutput)
        kfdaOutput= this.convertLDAOutputToList(kfdaOutput)
        lda_kfda_output={}
        for k in ldaOutput.keys():
            lda_kfda_output[k]= ldaOutput[k]
        for k in kfdaOutput.keys():
            lda_kfda_output[k]= kfdaOutput[k]
        return lda_kfda_output


    def main(this, signalOrSignals, datetimes, type, signalType): 
        lda_kfda_output= this.svm(signalOrSignals, datetimes, type, signalType)
        _id= lda_kfda_insert(this.coll, lda_kfda_output, signalType)
        this.logger.debug("LDA_KFDA completed for signal type: %s, id:%s" %(type, _id))        
        return _id
