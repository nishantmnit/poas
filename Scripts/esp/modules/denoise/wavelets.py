import importlib
from collections import OrderedDict

import numpy as np
import pandas as pd

from Scripts.esp.core.get_logger import get_logger

logger = get_logger("wavelets")

olsen_wavelets = ["olsen_haar", "olsen_d4", "olsen_d6", "olsen_d8", "olsen_d10", "olsen_d12", "olsen_d20",
                  "olsen_s4", "olsen_s6", "olsen_s8", "olsen_s10", "olsen_s12", "olsen_s20", "olsen_c6",
                  "olsen_c12", "olsen_c18", "olsen_sp26", "olsen_rsp26", "olsen_sp44", "olsen_rsp44",
                  "olsen_sp48", "olsen_rsp48", "olsen_sp4246", "olsen_sp35", "olsen_rsp35", "olsen_sp39",
                  "olsen_rsp39", "olsen_d14", "olsen_d16", "olsen_d18"]

other_wavelets = ['rbio6_8', 'bior2_4', 'coif14', 'haar', 'db19', 'bior3_1', 'bior3_9', 'db33', 'db18', 'rbio1_1',
                  'rbio4_4',
                  'rbio1_3', 'rbio1_5', 'bior3_7', 'rbio3_3', 'db2', 'rbio3_1', 'rbio3_7', 'rbio2_8', 'rbio2_6',
                  'rbio2_4',
                  'rbio2_2', 'bior2_6', 'bior4_4', 'db28', 'db29', 'db24', 'db25', 'db26', 'db27', 'db20', 'db21',
                  'db22',
                  'db23', 'rbio3_9', 'bior3_5', 'bior2_8', 'bior1_5', 'db38', 'bior5_5', 'bior3_3', 'bior1_3', 'db9',
                  'db37',
                  'rbio5_5', 'rbio3_5', 'db36', 'bior2_2', 'db3', 'bior6_8', 'db5', 'db4', 'db7', 'db6', 'db15', 'db14',
                  'db17',
                  'db16', 'db11', 'db10', 'db13', 'db8', 'coif12', 'coif13', 'coif10', 'coif11', 'coif16', 'coif17',
                  'coif15', 'coif1', 'coif2', 'coif3', 'coif4', 'coif5', 'coif6', 'coif7', 'coif8', 'coif9', 'db35',
                  'db34',
                  'db12', 'db32', 'db31', 'db30']

all_wavelets = olsen_wavelets + other_wavelets


def get_coefficients(wavelet):
    if wavelet in olsen_wavelets:
        wavelet_module = importlib.import_module("Scripts.esp.core.storage.wavelets.olsen.%s" % wavelet)
        return wavelet_module.d, wavelet_module.r
    elif wavelet in other_wavelets:
        wavelet_module = importlib.import_module("Scripts.esp.core.storage.wavelets.other.%s" % wavelet)
        return wavelet_module.d
    else:
        logger.exception("Wavelet not defined")


def get_coefficients_matrix(signal_length, wavelet):
    decompose_coefficients = get_coefficients(wavelet)
    matrix = list()
    left_pad = -2
    right_pad = signal_length - len(decompose_coefficients[0]) - left_pad
    for i in range(signal_length):
        if i % 2 == 0:
            d = decompose_coefficients[0]
            left_pad += 2
            right_pad = signal_length - len(d) - left_pad
        else:
            d = decompose_coefficients[1]
        if right_pad < 0:
            row = np.pad(d[:right_pad], (left_pad, 0), 'constant')
            row[:-1 * right_pad] = d[right_pad:]
            matrix.append(row)
        else:
            matrix.append(np.pad(d, (left_pad, right_pad), "constant"))
    return pd.DataFrame(data=matrix)


def decompose_signal(signal, coefficients_matrix=None, wavelet=None):
    if coefficients_matrix is None and wavelet is not None:
        coefficients_matrix = get_coefficients_matrix(len(signal), wavelet)
    elif wavelet is None and coefficients_matrix is None:
        raise LookupError("Please provide wavelet name or coefficients matrix.")
    decomposed = coefficients_matrix.dot(pd.Series(signal))
    approx = decomposed[0::2].tolist()
    detail = decomposed[1::2].tolist()
    return approx, detail


def frequency_bins(start_frequency_bin):
    bins = [int(i) for i in start_frequency_bin.split("-")]
    approx_bin = "%s-%s" % (bins[0], bins[0] + (bins[1] - bins[0]) / 2)
    detail_bin = "%s-%s" % (bins[0] + (bins[1] - bins[0]) / 2, bins[1])
    return approx_bin, detail_bin


def define_frequency_bins(signal, wavelet, start_frequency_bin=None, detail_nodes=None, approx_nodes=None):
    if detail_nodes is None:
        detail_nodes = OrderedDict()
        detail_nodes["nodes"] = []
        approx_nodes = OrderedDict()
        approx_nodes["nodes"] = []
        start_frequency_bin = "%s-%s" % (0, len(signal))
    deconstruct_coefficients = get_coefficients(wavelet)
    max_decomposition_level = int(np.log2(len(signal) / len(deconstruct_coefficients[0])))
    coefficients_matrix = get_coefficients_matrix(len(signal), wavelet)

    if max_decomposition_level > 0:
        approx, detail = decompose_signal(signal, coefficients_matrix=coefficients_matrix)
        approx_frequency_bin, detail_frequency_bin = frequency_bins(start_frequency_bin)
        detail_nodes["nodes"].append(detail_frequency_bin)
        approx_nodes["nodes"].append(approx_frequency_bin)
        detail_nodes[detail_frequency_bin] = detail
        approx_nodes[approx_frequency_bin] = approx
        detail_nodes, approx_nodes = define_frequency_bins(approx, wavelet, approx_frequency_bin, detail_nodes,
                                                           approx_nodes)
        detail_nodes, approx_nodes = define_frequency_bins(detail, wavelet, detail_frequency_bin, detail_nodes,
                                                           approx_nodes)
    return detail_nodes, approx_nodes


def reconstruct_nodes(detail_nodes, approx_nodes, wavelet):
    for node in range(len(detail_nodes["nodes"]) - 1, -1, -1):
        approx_node = approx_nodes["nodes"][node]
        detail_node = detail_nodes["nodes"][node]
        approx_signal = approx_nodes[approx_node]
        detail_signal = detail_nodes[detail_node]
        signal = [item for tup in zip(approx_signal, detail_signal) for item in tup]
        coefficients_matrix = get_coefficients_matrix(len(signal), wavelet)
        coefficients_matrix_inv = pd.DataFrame(np.linalg.pinv(coefficients_matrix.values), coefficients_matrix.columns,
                                               coefficients_matrix.index)
        reconstructed_node = "%s-%s" % (approx_node.split("-")[0], detail_node.split("-")[1])
        reconstructed_signal = coefficients_matrix_inv.dot(pd.Series(signal)).tolist()
        if reconstructed_node in detail_nodes.keys():
            detail_nodes[reconstructed_node] = reconstructed_signal
        elif reconstructed_node in approx_nodes.keys():
            approx_nodes[reconstructed_node] = reconstructed_signal
        else:
            signal = reconstructed_signal
    return detail_nodes, approx_nodes, signal


def enrich_signal(signal):
    energy = list(np.array(signal) ** 2)
    total_energy = sum(energy)
    pi = [i / total_energy if total_energy != 0 else 0 for i in energy]
    entropy = [0 if i == 0 else i * np.log2(i) for i in pi]
    return energy, pi, entropy


def calc_er(signal):
    energy, pi, entropy = enrich_signal(signal)
    total_energy = sum(energy)
    total_entropy = -1. * sum(entropy)
    er = 1. if total_entropy == 0 else total_energy / total_entropy
    return er


def reconstructed_signal_properties(original_signal, reconstructed_signal):
    original_signal = np.array(original_signal)
    reconstructed_signal = np.array(reconstructed_signal)

    s1 = ((original_signal - reconstructed_signal) ** 2).sum()
    s2 = (original_signal ** 2).sum()

    MEAN = np.average(original_signal - reconstructed_signal)
    RMSE = np.sqrt(s1) / len(reconstructed_signal)

    SNR = 0. if (s1 == 0 or s2 == 0) else 10. * (np.log10(s2 / s1))
    PRD = 0. if (s1 == 0 or s2 == 0) else 100. * np.sqrt(s1 / s2)
    signal_mean = np.average(original_signal)
    reconstructed_signal_mean = np.average(reconstructed_signal)
    si_smean2 = [(s - signal_mean) ** 2. for s in original_signal]
    sir_smean2 = [(s - reconstructed_signal_mean) ** 2. for s in reconstructed_signal]
    div = np.sqrt(sum(si_smean2) * sum(sir_smean2))
    R = sum([(original_signal[s] - signal_mean) * (reconstructed_signal[s] - reconstructed_signal_mean) for s in
             range(0, len(reconstructed_signal))]) / (1. if div == 0 else div)
    return RMSE, SNR, PRD, R, MEAN


if __name__ == "__main__":
    test_signal = [1048.3, 1048.3, 1048.3, 1045.5, 1046.9, 1045.5, 1046.9, 1045.5, 1048.3, 1048.3, 1046.9, 1049.8,
                   1049.8, 1049.8, 1051.3, 1046.9]
    print define_frequency_bins(test_signal, "db4")
