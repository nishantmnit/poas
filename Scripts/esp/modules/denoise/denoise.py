import numpy as np
import pandas as pd
from scipy.fftpack import fft, ifft

from Scripts.esp.core.get_logger import get_logger
from Scripts.esp.modules.denoise.wavelets import (
    other_wavelets,
    get_coefficients,
    define_frequency_bins,
    reconstruct_nodes,
    enrich_signal,
    calc_er,
    reconstructed_signal_properties
)
from Scripts.pvt.core.optimizers.poas_interpolator import monotone

logger = get_logger("de-noise")


def filter_threshold(signal):
    function = np.abs(signal - np.median(signal))
    sigma_j = np.median(function) / 0.6745
    threshold = sigma_j * np.sqrt((2. * np.log2(len(signal))) / len(signal))
    return threshold


def de_noise_eligible_wavelets():
    eligible_wavelets_list = list()
    for wavelet in other_wavelets:
        decompose_coefficients = get_coefficients(wavelet)
        if len(decompose_coefficients[0]) <= 16:
            eligible_wavelets_list.append(wavelet)
    eligible_wavelets_list.sort()
    return eligible_wavelets_list


def smooth_signal(signal):
    SNR = 0
    alpha = 0.1
    de_noised_signal = signal
    while SNR < 46 and alpha <= 1.:
        dft = fft(signal)
        df = pd.DataFrame(columns=["signal", "fft"], data=list(zip(signal, dft)))
        df["gk"] = (alpha + (1. - alpha) * np.cos(2. * np.pi * df.index.values / len(signal)))
        df.loc[df.gk < 0, "gk"] = 10 ** -5
        df["gk"] = df.gk * df.fft
        df["ifft"] = ifft(df.gk)
        de_noised_signal = df.apply(lambda row:
                                    10 ** -20 if np.abs(row.signal) <= 10 ** -20 else np.sqrt(
                                        row.ifft.real ** 2 + row.ifft.imag ** 2),
                                    axis=1)
        RMSE, SNR, PRD, R, MEAN = reconstructed_signal_properties(signal, de_noised_signal)
        alpha += 0.01
    return list(de_noised_signal)


def energy(freq_bins):
    energy_bins = dict()
    for freq_bin in freq_bins["nodes"]:
        signal_energy, pi, entropy = enrich_signal(freq_bins[freq_bin])
        energy_bins[freq_bin] = (np.sqrt(sum(signal_energy)) / len(signal_energy))
    return energy_bins


class analyze_signal:
    def __init__(self, input_signal, wavelet, multiplier, analyze_output=True):
        self.input_signal = input_signal
        self.multiplier = multiplier
        self.wavelet = wavelet
        self.er, self.max_er = 0, 0
        # input analysis
        self.input_detail_bins, self.input_approx_bins = define_frequency_bins(self.input_signal, self.wavelet)
        self.input_approx_energy = energy(self.input_approx_bins)
        self.input_detail_energy = energy(self.input_detail_bins)

        # output analysis
        if analyze_output:
            self.output_detail_bins, self.output_approx_bins, self.output_signal = self.filter_and_reconstruct()
            self.output_approx_energy = energy(self.output_approx_bins)
            self.output_detail_energy = energy(self.output_detail_bins)
            self.RMSE, self.SNR, self.PRD, self.R, self.MEAN = reconstructed_signal_properties(self.input_signal,
                                                                                               self.output_signal)

    def identify_nodes_to_filter(self):
        nodes_to_filter = list()
        last_node = self.input_detail_bins["nodes"][-1]
        node_list = [int(i) for i in last_node.split("-")]
        last_node_length = node_list[1] - node_list[0]
        for node in self.input_detail_bins["nodes"] + self.input_approx_bins["nodes"]:
            node_list = [int(i) for i in node.split("-")]
            if node_list[1] - node_list[0] == last_node_length and node_list[0] != 0:
                nodes_to_filter.append(node)
        self.max_er = len(nodes_to_filter)
        return nodes_to_filter

    def filter_and_reconstruct(self):
        detail_bins, approx_bins = self.input_detail_bins, self.input_approx_bins
        filter_nodes = self.identify_nodes_to_filter()
        for freq_bin in self.input_approx_bins["nodes"] + self.input_detail_bins["nodes"]:
            if freq_bin in filter_nodes:
                filtered_signal = self.filter_freq_bin(freq_bin)
                self.er += calc_er(filtered_signal)
                if freq_bin in self.input_detail_bins["nodes"]:
                    detail_bins[freq_bin] = filtered_signal
                else:
                    approx_bins[freq_bin] = filtered_signal
            else:
                if freq_bin in self.input_detail_bins["nodes"]:
                    detail_bins[freq_bin] = self.input_detail_bins[freq_bin]
                else:
                    approx_bins[freq_bin] = self.input_approx_bins[freq_bin]
        detail_bins, approx_bins, signal = reconstruct_nodes(detail_bins, approx_bins, self.wavelet)
        return detail_bins, approx_bins, signal

    def filter_freq_bin(self, freq_bin):
        signal = np.array(
            self.input_approx_bins[freq_bin] if freq_bin in self.input_approx_bins.keys() else self.input_detail_bins[
                freq_bin])
        threshold = self.multiplier * filter_threshold(signal)
        signal[signal <= threshold] = 0
        return list(signal)


def run_workflow(signal, selected_wavelets, multipliers):
    wavelet_df = pd.DataFrame()
    for wavelet in selected_wavelets:
        logger.debug("Evaluating wavelet = %s" % wavelet)
        outputs = dict()
        columns = ["rmse", "snr", "prd", "r", "mean", "er", "max_er", "multiplier", "output_signal"]
        for key in columns:
            outputs[key] = []
        for multiplier in multipliers:
            signal_obj = analyze_signal(signal, wavelet, multiplier)
            outputs["rmse"].append(signal_obj.RMSE)
            outputs["snr"].append(signal_obj.SNR)
            outputs["prd"].append(signal_obj.PRD)
            outputs["r"].append(signal_obj.R)
            outputs["mean"].append(signal_obj.MEAN)
            outputs["er"].append(signal_obj.er)
            outputs["max_er"].append(signal_obj.max_er)
            outputs["multiplier"].append(multiplier)
            outputs["output_signal"].append(signal_obj.output_signal)

        multiplier_df = pd.DataFrame(outputs)
        multiplier_df["wavelet"] = wavelet

        wavelet_df = pd.concat([wavelet_df, multiplier_df.loc[multiplier_df.index == multiplier_df.index.max()]
                               .reset_index(drop=True)], ignore_index=True)
    return wavelet_df


def identify_wavelet(signal):
    test_wavelets = de_noise_eligible_wavelets()
    test_multipliers = [1.]
    df = run_workflow(signal, test_wavelets, test_multipliers)
    df = df.loc[(df.rmse <= 0.1) & (df.snr >= 46.) & (df.snr < 100)].reset_index(drop=True)
    selected_wavelet = df.loc[df.snr == df.snr.max()].squeeze()
    return selected_wavelet


def tune_multiplier(signal, wavelet):
    wavelets = [wavelet]
    multiplier_thresholds = [0.5, 0.8] + [i / 2. for i in range(2, 21, 1)]
    df = run_workflow(signal, wavelets, multiplier_thresholds)
    df = df.loc[(df.snr >= 46) & (df.rmse <= 0.1) & (df.er == df.max_er)].reset_index(drop=True)
    selected_multiplier = df.loc[df.snr == df.snr.max()].squeeze()
    return selected_multiplier


def level_detailed_coefficients(signal_object):
    signal_length = len(signal_object.input_signal)
    bins = ["%s-%s" % (int(signal_length / (2 ** i * 2.)), int(signal_length / 2 ** i)) for i in range(0, 5)]
    output = dict()
    for freq_bin in bins:
        detail_coefficients = signal_object.input_detail_bins[freq_bin]
        x = range(1, len(detail_coefficients) + 1)
        find_x = np.array(range(1, signal_length + 1)) / ((signal_length * 1.) / len(detail_coefficients))
        output[freq_bin] = monotone(x, detail_coefficients, find_x)
    return output


def kurtosis(freq_bin_dict):
    output = dict()
    for freq_bin, signal in freq_bin_dict.items():
        bin_output = dict()
        abs_signal = np.abs(signal)
        mean = np.average(signal)
        f1 = np.array(signal) ** 2
        f2 = (np.array(signal) - mean) ** 2
        f3 = (np.array(signal) - mean) ** 3
        f4 = (np.array(signal) - mean) ** 4
        bin_output["kurtosis"] = ((1. / len(signal)) * sum(f4)) / ((1. / len(signal)) * sum(f2)) ** 2
        bin_output["skewness"] = ((1./len(signal))*sum(f3))/((1./len(signal))*sum(f2))**(3./2.)
        bin_output["crest_factor"] = max(abs_signal)/np.sqrt(1./len(signal)*sum(f1))
        bin_output["waveform_factor"] = 0 if mean == 0 else (1./mean*np.sqrt(1./len(signal)*sum(f1)))
        bin_output["std_deviation"] = np.sqrt(1./len(signal)*sum(f2))
        bin_output["rms"] = np.sqrt(1./len(signal)*sum(f1))
        bin_output["variance"] = bin_output["std_deviation"]**2
        output[freq_bin] = bin_output
    return output


def prepare_output(x_axis, signal, de_noised_signal):
    output = dict()
    output["input_signal"] = signal
    wavelet = "db4"
    signal_object = analyze_signal(signal, wavelet, 1, analyze_output=False)
    output["input_detail_bins"] = signal_object.input_detail_bins
    output["input_approx_bins"] = signal_object.input_approx_bins
    output["input_detail_energy_bins"] = signal_object.input_detail_energy
    output["input_approx_energy_bins"] = signal_object.input_approx_energy

    signal_object = analyze_signal(de_noised_signal, wavelet, 1, analyze_output=False)
    output["output_signal"] = de_noised_signal
    output["output_detail_bins"] = signal_object.input_detail_bins
    output["output_approx_bins"] = signal_object.input_approx_bins
    output["output_detail_energy_bins"] = signal_object.input_detail_energy
    output["output_approx_energy_bins"] = signal_object.input_approx_energy
    output["x_axis"] = x_axis

    RMSE, SNR, PRD, R, MEAN = reconstructed_signal_properties(signal, de_noised_signal)
    output["signal_properties"] = dict()
    output["signal_properties"]["rmse"] = RMSE
    output["signal_properties"]["snr"] = SNR
    output["signal_properties"]["prd"] = PRD
    output["signal_properties"]["r"] = R
    output["signal_properties"]["mean"] = MEAN

    output["level_detailed_coefficients"] = level_detailed_coefficients(signal_object)
    output["kurtosis"] = kurtosis(output["level_detailed_coefficients"])
    return output


def de_noise(signal_df):
    signal = signal_df.y_axis.tolist()
    selected_wavelet = identify_wavelet(signal)
    if len(selected_wavelet) == 0:
        logger.debug("Not able to find the fit wavelet for this signal. Proceeding with smoothing.")
        output_signal = signal
    else:
        logger.debug("Selected wavelets = %s" % selected_wavelet.to_dict())
        selected_multiplier = tune_multiplier(signal, selected_wavelet.wavelet)
        logger.debug("Final Results => Wavelet = %s | RMSE = %s | SNR = %s | PRD = %s | R = %s | ER = %s | Max ER = "
                     "%s | "
                     "Multiplier = %s "
                     % (selected_multiplier.wavelet, selected_multiplier.rmse, selected_multiplier.snr,
                        selected_multiplier.prd,
                        selected_multiplier.r, selected_multiplier.er, selected_multiplier.max_er,
                        selected_multiplier.multiplier))
        output_signal = selected_wavelet.output_signal

    de_noised_signal = smooth_signal(output_signal)
    return prepare_output(signal_df.x_axis.tolist(), signal, de_noised_signal)


if __name__ == "__main__":
    from Scripts.esp.playground.nishant.test_signal.signals import noisy_signal2

    test_signal = noisy_signal2  # [i for i in range(256)]
    test_df = pd.DataFrame(columns=["x_axis", "y_axis"], data=list(zip(test_signal, test_signal)))
    test_output = de_noise(test_df)
    print (test_output)
