import numpy as np
import pandas as pd

wavelets = ["mexicanhat"]
support_time = 5.


def get_fourier_wavelength(wavelet):
    return 2 * np.pi / np.sqrt(2.5)


def family(wavelet, signal_length, scales):
    df = pd.DataFrame(columns=["n"], data=range(2 * signal_length))
    constant = (2. / np.sqrt(3.)) * np.pi ** (1. / 4.)
    for scale in scales:
        df["t"] = df.n / scale
        df["real"] = constant * (1. - df.t ** 2) * np.exp(-1. * df.t ** 2 / 2.)
        yield scale, df.real.values


if __name__ == "__main__":
    for s, result in family("mexicanhat", 256, [15]):
        print s
