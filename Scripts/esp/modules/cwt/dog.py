import numpy as np
import pandas as pd
import scipy.special

wavelets = ["dog_%s" % i for i in range(1, 9)]
support_time = 4.


def get_fourier_wavelength(wavelet):
    m = int(wavelet.split("_")[1])
    return 2. * np.pi / np.sqrt(m + 0.5)


def dog_1(df):
    return -1. * df.t


def dog_2(df):
    return df.t ** 2 - 1.


def dog_3(df):
    return 3 * df.t - df.t ** 3


def dog_4(df):
    return df.t ** 4 - 6 * df.t ** 2 + 3


def dog_5(df):
    return df.t ** 5 * -1 + 10 * df.t ** 3 - 15 * df.t


def dog_6(df):
    return df.t ** 6 - 15 * df.t ** 4 + 45 * df.t ** 2 - 15


def dog_7(df):
    return df.t ** 7 * -1 + 21 * df.t ** 5 - 105 * df.t ** 3 + 105 * df.t


def dog_8(df):
    return df.t ** 8 - 28 * df.t ** 6 + 210 * df.t ** 4 - 420 * df.t ** 2 + 105


def family(wavelet, signal_length, scales):
    m = int(wavelet.split("_")[1])
    constant = ((-1.) ** (m * 2 + 1 - m) / np.sqrt(scipy.special.gamma((m * 2 + 1) / 2.)))
    df = pd.DataFrame(columns=["n"], data=range(2 * signal_length))
    for scale in scales:
        df["t"] = df.n / scale
        df["t1"] = -1. * (df.t ** 2) / 2.
        df["t2"] = eval(wavelet)(df)
        df["real"] = constant * df.t2 * np.exp(df.t1)
        yield scale, df.real.values


if __name__ == "__main__":
    for s, result in family("dog_1", 256, [15]):
        print s
