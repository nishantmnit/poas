import numpy as np
import pandas as pd

wavelets = ["morlet_3", "morlet_6", "morlet_9", "morlet_15"]
support_time = 5.


def get_fourier_wavelength(wavelet):
    m = int(wavelet.split("_")[1])
    return 4. * np.pi / (m + np.sqrt(2. + m ** 2))


def family(wavelet, signal_length, scales):
    central_frequency = int(wavelet.split("_")[1])
    c_sigma = (1. + np.exp(-1. * central_frequency ** 2) - 2. * np.exp(-1. * (3. / 4. * central_frequency ** 2))) ** (
        -0.5)
    k_sigma = np.exp(-0.5 * central_frequency ** 2)

    df = pd.DataFrame(columns=["n"], data=range(2 * signal_length))
    for scale in scales:
        df["t"] = df.n / scale
        df["real"] = (c_sigma * (np.pi ** (-1. / 4.)) * np.exp(-0.5 * df.t ** 2) * (
                np.cos(central_frequency * df.t) - k_sigma)) / np.sqrt(np.abs(scale))
        df.loc[np.abs(df.real) <= 10 ** -6, "real"] = 0
        yield scale, df.real.values


if __name__ == "__main__":
    for s, result in family("morlet_6", 256, [15]):
        print s
