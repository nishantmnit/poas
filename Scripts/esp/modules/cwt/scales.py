import importlib

import numpy as np


def frequency_scales(wavelet, sampling_time):
    wavelet_module = importlib.import_module("Scripts.esp.modules.cwt.%s" % wavelet.split("_")[0])
    fourier_wavelength = getattr(wavelet_module, "get_fourier_wavelength")(wavelet)
    # if sampling time is less than 20 then hertz else millihertz
    r = np.tile(2 ** (np.arange(0, 16) / 16.), (12, 1))
    scales = (r * np.reshape(2 ** np.repeat(np.arange(0, 12), 16), (12, 16))).flatten()
    scales = np.concatenate((np.arange(0.1, 1, 0.1), scales))
    scales = scales[scales >= 1]
    frequency = 1./(scales * fourier_wavelength * sampling_time)
    # return alternate values
    return scales, frequency


if __name__ == "__main__":
    s, f = frequency_scales("morlet_6", 60)
    print f
