import numpy as np
import pandas as pd
from scipy.misc import comb

wavelets = ["poas_%s" % i for i in range(1, 9)]
support_time = 5.


def get_fourier_wavelength(wavelet):
    wavelength_divisors = [2.27498, 3.06434, 3.8249, 4.63553, 5.53537, 6.50526, 7.50045, 8.50002]
    m = int(wavelet.split("_")[1])
    return 2. * np.pi / wavelength_divisors[m - 1]


def derivative_f1(df_o, n):
    df = df_o.copy(deep=True)
    df["complex_1"] = df.t - 1. * 1j
    df["complex_2"] = df.t + 1. * 1j
    df["power_1"] = df.complex_1 ** (-1. * (n + 1.))
    df["power_2"] = df.complex_2 ** (-1. * (n + 1.))
    df["im_sub"] = df.power_1 - df.power_2
    df["complex_3"] = 0 + (-1. * (-1.) ** n * np.math.factorial(n) / 2.) * 1j
    return df.im_sub * df.complex_3


def derivative_f2(df):
    df["d1_f2"] = -2. * df.t * np.exp((-1. * df.t ** 2))
    df["d2_f2"] = (np.exp((-1. * df.t ** 2))) * (-2. + 4. * df.t ** 2)
    df["d3_f2"] = (np.exp((-1. * df.t ** 2))) * (12. * df.t - 8. * df.t ** 3)
    df["d4_f2"] = (np.exp((-1 * df.t ** 2))) * (12 - 48 * df.t ** 2 + 16 * df.t ** 4)
    df["d5_f2"] = (np.exp((-1 * df.t ** 2))) * (-120 * df.t + 160 * df.t ** 3 - 32 * df.t ** 5)
    df["d6_f2"] = (np.exp((-1 * df.t ** 2))) * (-120 + 720 * df.t ** 2 - 480 * df.t ** 4 + 64 * df.t ** 6)
    df["d7_f2"] = (np.exp((-1 * df.t ** 2))) * (1680 * df.t - 3360 * df.t ** 3 + 1344 * df.t ** 5 - 128 * df.t ** 7)
    df["d8_f2"] = (np.exp((-1 * df.t ** 2))) * (
            1680 - 13440 * df.t ** 2 + 13440 * df.t ** 4 - 3584 * df.t ** 6 + 256 * df.t ** 8)
    return df


def poas_1(df):
    for derivative in range(1):
        df["d%s_f1" % (derivative + 1)] = derivative_f1(df, derivative + 1)
    df["real"] = 1. / np.sqrt(1.60161658) * (df.d1_f1 * df.f2 + df.d1_f2 * df.f1)
    return df


def poas_2(df):
    for derivative in range(2):
        df["d%s_f1" % (derivative + 1)] = derivative_f1(df, derivative + 1)
    df["real"] = 1. / np.sqrt(9.112718044) * (
            df.d2_f1 * df.f2 + comb(2, 1) * df.d1_f1 * df.d1_f2 + comb(2, 2) * df.d2_f2 * df.f1)
    return df


def poas_3(df):
    for derivative in range(3):
        df["d%s_f1" % (derivative + 1)] = derivative_f1(df, derivative + 1)
    df["real"] = 1. / np.sqrt(95.97420331) * (
            df.d3_f1 * df.f2 + comb(3, 1) * df.d2_f1 * df.d1_f2 + comb(3, 2) * df.d1_f1 * df.d2_f2 +
            comb(3, 3) * df.d3_f2 * df.f1)
    return df


def poas_4(df):
    for derivative in range(4):
        df["d%s_f1" % (derivative + 1)] = derivative_f1(df, derivative + 1)
    df["real"] = 1. / np.sqrt(1596.419729) * (
            df.d4_f1 * df.f2 + comb(4, 1) * df.d3_f1 * df.d1_f2 + comb(4, 2) * df.d2_f1 * df.d2_f2 +
            comb(4, 3) * df.d1_f1 * df.d3_f2 + comb(4, 4) * df.d4_f2 * df.f1)
    return df


def poas_5(df):
    for derivative in range(5):
        df["d%s_f1" % (derivative + 1)] = derivative_f1(df, derivative + 1)
    df["real"] = 1. / np.sqrt(38948.93124) * (
            df.d5_f1 * df.f2 + comb(5, 1) * df.d4_f1 * df.d1_f2 + comb(5, 2) * df.d3_f1 * df.d2_f2 +
            comb(5, 3) * df.d2_f1 * df.d3_f2 + comb(5, 4) * df.d1_f1 * df.d4_f2 + comb(5, 5) * df.f1 * df.d5_f2)
    return df


def poas_6(df):
    for derivative in range(6):
        df["d%s_f1" % (derivative + 1)] = derivative_f1(df, derivative + 1)
    df["real"] = 1. / np.sqrt(1330506.195) * (
            df.d6_f1 * df.f2 + comb(6, 1) * df.d5_f1 * df.d1_f2 + comb(6, 2) * df.d4_f1 * df.d2_f2 +
            comb(6, 3) * df.d3_f1 * df.d3_f2 + comb(6, 4) * df.d2_f1 * df.d4_f2 + comb(6, 5) * df.d1_f1 * df.d5_f2
            + comb(6, 6) * df.f1 * df.d6_f2)
    return df


def poas_7(df):
    for derivative in range(7):
        df["d%s_f1" % (derivative + 1)] = derivative_f1(df, derivative + 1)
    df["real"] = 1. / np.sqrt(61348820.77) * (
            df.d7_f1 * df.f2 + comb(7, 1) * df.d6_f1 * df.d1_f2 + comb(7, 2) * df.d5_f1 * df.d2_f2 +
            comb(7, 3) * df.d4_f1 * df.d3_f2 + comb(7, 4) * df.d3_f1 * df.d4_f2 + comb(7, 5) * df.d2_f1 * df.d5_f2
            + comb(7, 6) * df.d1_f1 * df.d6_f2 + comb(7, 7) * df.f1 * df.d7_f2)
    return df


def poas_8(df):
    for derivative in range(8):
        df["d%s_f1" % (derivative + 1)] = derivative_f1(df, derivative + 1)
    df["real"] = 1. / np.sqrt(3697995365) * (
            df.d8_f1 * df.f2 + comb(8, 1) * df.d7_f1 * df.d1_f2 + comb(8, 2) * df.d6_f1 * df.d2_f2 +
            comb(8, 3) * df.d5_f1 * df.d3_f2 + comb(8, 4) * df.d4_f1 * df.d4_f2 + comb(8, 5) * df.d3_f1 * df.d5_f2
            + comb(8, 6) * df.d2_f1 * df.d6_f2 + comb(8, 7) * df.d1_f1 * df.d7_f2
            + comb(8, 8) * df.f1 * df.d8_f2)
    return df


def family(wavelet, signal_length, scales):
    df = pd.DataFrame(columns=["n"], data=range(2 * signal_length))
    for scale in scales:
        df["t"] = df.n / scale
        df["f1"] = 1. / (1. + df.t ** 2)
        df["f2"] = np.exp((-1. * df.t ** 2))
        df = derivative_f2(df)
        df = eval(wavelet)(df)
        yield scale, df.real.values


if __name__ == "__main__":
    for s, result in family("poas_1", 256, [15]):
        print s
