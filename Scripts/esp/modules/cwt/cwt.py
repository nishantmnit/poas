import importlib

import numpy as np
import pandas as pd
from scipy.fftpack import fft, ifft

from Scripts.esp.core.get_logger import get_logger
from scales import frequency_scales

logger = get_logger("CWT")


def cwt(df_o, wavelets=None):
    signal = df_o.y_axis.tolist()
    sampling_time = (df_o.x_axis.shift(-1) - df_o.x_axis).values[0]
    realtime_wavelets = ["mexicanhat", "morlet_6", "dog_7", "poas_5"] if wavelets is None else wavelets
    result = dict()
    for wavelet_name in realtime_wavelets:
        logger.info("Started for wavelet = %s" % wavelet_name)
        df, scales, frequencies = calc_amplitude(signal, sampling_time, wavelet_name)
        result[wavelet_name] = {"amplitudes": df.T.to_dict("list").values(), "scales": list(scales),
                                "frequencies": list(frequencies), "b_values": df_o.x_axis.to_list()}
        # amplitudes will be a list of lists. Each list will be of length = length of scales or frequencies.
        # Hence, for each element in scale there is a corresponding value in a nested list of amplitude
        # for a given b_value or time
        # b_value[0], scales, frequencies, amplitude[0] is one complete data set.
        # b_value[1], scales, frequencies, amplitude[1] is another complete data set.
        # to plot and make each axis of same length in data set, we need to repeat b_value,
        # e.g. b_value[0] to len(scales) times.
        # all the dataset can then be combined into a single data set and fed into plotting library.
    return result


def calc_amplitude(signal, sampling_time, wavelet_name):
    signal_length = len(signal)
    wavelet_family = importlib.import_module("Scripts.esp.modules.cwt.%s" % wavelet_name.split("_")[0])
    scales, frequencies = frequency_scales(wavelet_name, sampling_time)
    columns = ["amplitude_%s" % s for s in range(len(scales))]
    amplitudes = dict()

    signal_fft = fft(np.concatenate((signal, np.zeros(signal_length)), axis=None))
    for scale, wavelet in getattr(wavelet_family, "family")(wavelet_name, signal_length, scales):
        if scale not in amplitudes.keys():
            amplitudes[scale] = list()
        for b_value in range(signal_length):
            b_wavelet = np.concatenate((wavelet[0:b_value + 1][::-1], wavelet[1:]), axis=None)[0:signal_length * 2]
            wavelet_fft = fft(b_wavelet)
            amplitudes[scale].append(max(np.abs(ifft(signal_fft * wavelet_fft))))
    df = pd.DataFrame(amplitudes, index=range(signal_length))
    df = df.rename(columns=dict(zip(scales, columns)))
    return df, scales, frequencies


if __name__ == "__main__":
    from Scripts.esp.playground.nishant.test_signal.signals import cwt_test_signal as cwt_test_signal
    from Scripts.pvt.core.storage.excel import write_excel

    test_x_axis = [i * 60 for i in range(len(cwt_test_signal))]
    test_signal_df = pd.DataFrame(data=list(zip(cwt_test_signal, test_x_axis)), columns=["y_axis", "x_axis"])
    output = cwt(test_signal_df, ["morlet_6"])["morlet_6"]
    print output
