import numpy as np
import pandas as pd
from scipy import signal as scipy_signal, fftpack


def calc_window_length(signal):
    if len(signal) <= 512:
        return 64
    elif len(signal) <= 4096:
        return 128
    else:
        return len(signal) // 2


def get_window(window, window_length):
    if window == "gaussian":
        win = getattr(scipy_signal, window)(window_length, 1)
    else:
        win = getattr(scipy_signal, window)(window_length)
    return win[0:len(win) // 2]


def apply_window(x, win, window_length):
    overlap = window_length // 2
    step = window_length - overlap
    shape = x.shape[:-1] + (x.shape[-1] // step, overlap)
    strides = x.strides[:-1] + (step * x.strides[-1], x.strides[-1])
    result = np.lib.stride_tricks.as_strided(x, shape=shape,
                                             strides=strides)
    result[0] = result[0][::-1]
    result = win * result
    result[0] = result[0][::-1]
    return result


def get_amplitudes(signal, win, window_length):
    win_signal = apply_window(signal, win, window_length)
    fft_signal = fftpack.fft(win_signal, n=len(signal))
    result = np.abs(fft_signal)
    result = [r[0:len(signal) // 2] for r in result]
    amplitudes, max_amplitude = list(), 0
    chunks, chunk = list(), 1
    for a in result:
        amplitudes += list(a)
        chunks = chunks + [chunk for i in range(len(a))]
        max_amplitude += max(a)
        chunk += 1
    return amplitudes, chunks, max_amplitude


def spectrogram(signal_df, window, sampling_frequency):
    signal = signal_df.y_axis.values
    window_length = calc_window_length(signal)
    win = get_window(window, window_length)

    amplitude, chunks, max_amplitude = get_amplitudes(signal, win, window_length)

    frequencies = list(fftpack.fftfreq(len(signal), 1 / sampling_frequency)[0:len(signal) // 2])
    while len(frequencies) < len(amplitude):
        frequencies = frequencies + frequencies
    df = pd.DataFrame(data=list(zip(frequencies, amplitude, chunks)),
                      columns=["frequency", "amplitude", "chunk"])

    end_time = list(np.arange(window_length / 2, signal.shape[-1] + 1, window_length - window_length // 2) / float(
        sampling_frequency) + signal_df.x_axis.values[0])

    start_time = [signal_df.x_axis.values[0]] + end_time[0:-1]
    for i in range(1, len(start_time)+1):
        df.loc[df.chunk == i, "start_time"] = start_time[i-1]
        df.loc[df.chunk == i, "end_time"] = end_time[i-1]
    return df, max_amplitude
