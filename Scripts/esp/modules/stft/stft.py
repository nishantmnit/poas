import numpy as np
import pandas as pd

from Scripts.esp.core.get_logger import get_logger
from Scripts.esp.modules.stft.spectrogram import spectrogram

logger = get_logger("module stft")

windows = ['barthann', 'bartlett', 'blackman', 'blackmanharris', 'bohman', 'cosine', 'flattop', 'hamming',
           'hann', 'triang', 'gaussian']


def calc_stft(signal_df, sampling_frequency):
    stft_outputs = dict()
    amplitudes = list()
    for window in windows:
        stft_outputs[window], amplitude = spectrogram(signal_df, window, sampling_frequency)
        amplitudes.append(amplitude)

    selected_window = windows[np.where(amplitudes == max(amplitudes))[0][0]]
    return stft_outputs[selected_window]


def stft(signal_df, sampling_time):
    stft_output = calc_stft(signal_df, 1. / sampling_time)
    stft_output["seq"] = stft_output.index
    output = stft_output.to_dict("list")
    return output


if __name__ == "__main__":
    from Scripts.esp.playground.nishant.test_signal.signals import i_motor_denoised as stft_test_signal

    from Scripts.pvt.core.storage.excel import write_csv
    from Scripts.esp.settings import DEBUG_DIR

    test_x_axis = [i * 1 for i in range(len(stft_test_signal))]
    test_signal_df = pd.DataFrame(data=list(zip(stft_test_signal, test_x_axis)), columns=["y_axis", "x_axis"])
    test_output = stft(test_signal_df, 1)
    test_df = pd.DataFrame(test_output).sort_values(by=["seq"]).reset_index(drop=True)

    write_csv(debug=True, save_to_dir=DEBUG_DIR, filename=["spectrogram"], df=test_df, index=False)
