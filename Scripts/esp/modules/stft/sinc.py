import numpy as np
import pandas as pd
from numpy import tile, newaxis, dot
from scipy.special import sinc

from Scripts.esp.core.get_logger import get_logger
from Scripts.esp.pipelines.read_sensors import get_sleep_time

logger = get_logger("module sinc")

'''
This module is currently not in use. This can be deleted from few months.
'''


def sinc_interpolate(x, s, u):
    if len(x) != len(s):
        raise ValueError('x and s must be the same length')
    x, s, u = np.asanyarray(x), np.asanyarray(s), np.asanyarray(u)
    # Find the period
    T = s[1] - s[0]
    sincM = tile(u, (len(s), 1)) - tile(s[:, newaxis], (1, len(u)))
    y = dot(x, sinc(sincM / T))
    return list(y)


def define_no_of_batches(sampling_time):
    sampling_times = [10 ** -3, 0.005, 0.01, 0.05, 0.1, 0.5, 1] + [int(i * 5) for i in range(1, 13)]
    batches = [1, 5, 5, 6, 6, 7, 7, 9, 9, 7, 9, 12, 14, 8, 9, 10, 12, 13, 14]
    for i in range(len(sampling_times)):
        if sampling_time <= sampling_times[i]:
            return batches[i]


def define_yield_batch_size(sampling_time):
    sampling_times = [10 ** -3, 0.005, 0.01, 0.05, 0.1, 0.5, 1] + [int(i * 5) for i in range(1, 13)]
    batch_sizes = [512, 512, 1024, 4096, 8192, 32768, 32768, 131072, 262144, 524288, 524288, 524288, 524288, 1048576,
                   1048576, 1048576, 1048576, 1048576, 1048576]
    for i in range(len(sampling_times)):
        if sampling_time <= sampling_times[i]:
            return batch_sizes[i]


def up_resolution(signal_df, sensor):
    input_signal = signal_df.y_axis.tolist()
    start_time = signal_df.x_axis.values[0]
    sleep_time = get_sleep_time(sensor.data_sampling_frequency, sensor.data_sampling_frequency_unit)

    sampling_time = 60. if sleep_time >= 60. else sleep_time
    number_of_batches = define_no_of_batches(sampling_time)
    yield_batch_size = define_yield_batch_size(sampling_time)
    sinc_sampling_time = len(input_signal) * sampling_time / float(number_of_batches * yield_batch_size)

    total_number_of_points = int(len(input_signal) * (1. / sinc_sampling_time) * sampling_time)

    logger.debug(
        "Sampling time = %s | Number of Batches = %s | "
        "STFT batch size = %s |Sinc sampling Time = %s | "
        "Total number of points = %s" % (
            sampling_time, number_of_batches, yield_batch_size, sinc_sampling_time, total_number_of_points))

    x = input_signal
    s = [t * sampling_time for t in range(0, len(input_signal))]
    u = [t * sinc_sampling_time for t in range(0, total_number_of_points)]

    output = []
    batch_size = 512
    start_index = 0
    end_index = len(u)
    batch_end_index = end_index if start_index + batch_size > end_index else start_index + batch_size
    batch_times = []
    while batch_end_index >= start_index:
        batch_test_u = u[start_index: batch_end_index]
        output = output + sinc_interpolate(x, s, batch_test_u)
        batch_times += batch_test_u
        if len(output) == yield_batch_size:
            df = pd.DataFrame(data=output, columns=["y_axis"])
            df["y_axis"] = df.y_axis * np.cos(2. * np.pi * float(sensor.frequency) * np.array(batch_times))
            df["x_axis"] = batch_times + start_time
            batch_times, output = [], []
            yield df, sinc_sampling_time
        start_index = batch_end_index + 1
        batch_end_index = end_index if start_index + batch_size > end_index else start_index + batch_size


if __name__ == "__main__":
    from Scripts.esp.playground.nishant.test_signal.signals import stft_test_signal as signal

    if len(signal) < 512:
        signal = signal + signal

    test_y_axis = signal
    test_input_sampling_time = 5 * 10 ** -3
    test_x_axis = [i * test_input_sampling_time for i in range(len(test_y_axis))]

    test_signal_df = pd.DataFrame(data=list(zip(test_y_axis, test_x_axis)), columns=["y_axis", "x_axis"])
    test_sensor = {"data_sampling_frequency": test_input_sampling_time, "data_sampling_frequency_unit": "second",
                   "signal_character": "sinusoidal", "frequency": "50"}
    test_sensor = pd.Series(test_sensor)

    for stft_batch, test_sampling_time in up_resolution(test_signal_df, test_sensor):
        print len(stft_batch), test_sampling_time
