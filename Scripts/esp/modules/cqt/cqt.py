import pandas as pd

from Scripts.esp.core.get_logger import get_logger
from Scripts.esp.modules.cqt.spectrogram import spectrogram

logger = get_logger("module cqt")


def calc_window_length(signal_length):
    start = 32
    while start <= signal_length:
        yield start
        start = int(2 * start)


def calc_cqt(signal_df, sampling_frequency):
    window = "hann"
    cqt_outputs = dict()
    for window_length in calc_window_length(signal_df.shape[0]):
        logger.debug("Executing cqt for window length = %s" % window_length)
        cqt_outputs[window_length] = spectrogram(signal_df, window, sampling_frequency, window_length).to_dict("list")
    return cqt_outputs


def cqt(signal_df, sampling_time):
    cqt_output = calc_cqt(signal_df, 1. / sampling_time)
    return cqt_output


if __name__ == "__main__":
    from Scripts.esp.playground.nishant.test_signal.signals import i_motor_denoised as cqt_test_signal

    from Scripts.pvt.core.storage.excel import write_excel
    from Scripts.esp.settings import DEBUG_DIR

    test_x_axis = [i * 60 for i in range(len(cqt_test_signal))]
    test_signal_df = pd.DataFrame(data=list(zip(cqt_test_signal, test_x_axis)), columns=["y_axis", "x_axis"])
    test_output = cqt(test_signal_df, 60)

    sheets, dfs = list(), list()
    for out_window_length, out_cqt in test_output.iteritems():
        sheets.append("%s" % out_window_length)
        dfs.append(pd.DataFrame(out_cqt))
    write_excel(debug=True, save_to_dir=DEBUG_DIR, filename=["cqt"], dfs=dfs, sheets=sheets)
