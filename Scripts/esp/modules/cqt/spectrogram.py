import numpy as np
import pandas as pd
from scipy import signal as scipy_signal


def get_window(window, window_length):
    if window == "gaussian":
        win = getattr(scipy_signal, window)(window_length, 1)
    else:
        win = getattr(scipy_signal, window)(window_length)
    return win


def apply_window(x, win, window_length):
    step = window_length
    shape = x.shape[:-1] + (x.shape[-1] // step, window_length)
    strides = x.strides[:-1] + (step * x.strides[-1], x.strides[-1])
    result = np.lib.stride_tricks.as_strided(x, shape=shape,
                                             strides=strides)
    result = win * result
    return result


def get_frequencies(signal_df):
    max_frequency, max_window_length = signal_df.shape[0], signal_df.shape[0]
    octave = 12.
    min_frequency = max_frequency / (2 ** (max_window_length / octave))
    fk = (min_frequency * 2 ** (signal_df.index / octave)).tolist()
    return fk


def spectrogram(signal_df, window, sampling_frequency, window_length):
    signal = signal_df.y_axis.values
    win = get_window(window, window_length)
    frequencies = get_frequencies(signal_df)
    win_signals = apply_window(signal, win, window_length)
    window_counter = 2 * np.pi * np.arange(0, window_length)
    number_of_chunks = len(signal) // window_length
    chunks = [i for i in range(1, number_of_chunks + 1)]

    master_df = pd.DataFrame(data=signal, columns=["signal"])
    master_df["x_axis"] = signal_df.x_axis
    master_df["frequency"] = frequencies
    master_df["chunk_start_end"] = master_df.index // window_length + 1

    amplitudes = dict()
    for chunk in chunks:
        amplitudes[chunk] = []

    for frequency in frequencies:
        cosine = (win_signals * np.cos(window_counter * frequency / sampling_frequency)).sum(axis=1)
        sine = (win_signals * np.sin(window_counter * frequency / sampling_frequency)).sum(axis=1)
        amps = np.abs(cosine + sine * 1j)
        for i in range(len(amps)):
            amplitudes[i + 1].append(amps[i])

    out_df = pd.DataFrame()

    for chunk in chunks:
        df = master_df.copy(deep=True)
        df["chunk"] = chunk
        df["amplitude"] = amplitudes[chunk]
        df["start_time"] = df.loc[df.chunk_start_end == chunk, "x_axis"].min()
        df["end_time"] = df.loc[df.chunk_start_end == chunk, "x_axis"].max()
        out_df = pd.concat([out_df, df], ignore_index=True)

    df = out_df[["chunk", "start_time", "end_time", "frequency", "amplitude"]]

    return df
