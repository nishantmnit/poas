import math

import numpy as np

from Scripts.pvt.core.optimizers.poas_interpolator import monotone


def fix_outlier(df):
    find_x = df.loc[df.index.isin([2])].x_axis.tolist()
    x = df.loc[df.index.isin([0, 1, 3])].x_axis.tolist()
    y = df.loc[df.index.isin([0, 1, 3])].y_axis.tolist()
    forward = monotone(x, y, find_x)
    x = df.loc[df.index.isin([1, 3, 4])].x_axis.tolist()
    y = df.loc[df.index.isin([1, 3, 4])].y_axis.tolist()
    backward = monotone(x, y, find_x)
    actual = df.loc[df.index == 2, "y_axis"].values[0]
    closest = backward if abs(forward - actual) > abs(backward - actual) else forward
    no_of_digits_in_actual = math.floor(math.log10(abs(actual)) + 1)
    tolerance = 200 * np.exp(-2.30258509299404000000 * no_of_digits_in_actual if no_of_digits_in_actual >= 1 else 0.5)
    value = closest if ((actual - closest) / actual) * 100. > tolerance else actual
    return value
