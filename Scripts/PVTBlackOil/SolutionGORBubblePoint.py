import os, sys, math
import matplotlib.pyplot as plt

cwd = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(0, "%s/../../../Python" %(cwd))
from Lib.Utils import *
from Scripts.pvt.core.optimizers.ncg import *
from Scripts.pvt.core.optimizers.poas_interpolator import *


class SolutionGORBubblePoint(object):
    def __init__(this, parent):
        this.__dict__.update(parent.__dict__)
        this.tempRange= 10.
        this.dateYmdHMS= datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')

    def trimDfHeaders(this, df):
        renameCol = {}
        for col in df.columns:
            c = col.strip()
            renameCol[col] = c 
        df.rename(columns= renameCol, inplace=True)
        return df

    def define_region(this, vars, bound, mk_vars):
        lb, ub = list(), list()
        for i in range(len(vars)):
            lb.append(max(bound[i][0], (vars[i] - 4.*vars[i])))
            ub.append(min(bound[i][1], (vars[i] + 4.*vars[i])))
        a_max_l = list()
        for i in range(len(vars)):
            a_max_l.append( 0 if mk_vars[i] == 0 else ( ((vars[i] - ub[i]) / mk_vars[i]) if mk_vars[i] < 0 else ((vars[i] - lb[i]) / mk_vars[i])))
        alpha_max= None
        if 1.5*min(a_max_l) < 10.**-5:
            alpha_max = 10.**-5
        elif 1.5*min(a_max_l) > 50.:
            alpha_max = 50.
        else:
            alpha_max = 1.5*min(a_max_l)

        alpha_min= None
        if((alpha_max*0.0000001)<10.**-13):
            alpha_min= 10.**-13,
        elif((alpha_max*0.0000001)>10.**-8):
            alpha_min= 10**-8
        else:
            alpha_min= alpha_max*0.0000001
        return alpha_max, alpha_min

    def converge_f(this, debug, logger, curr_iteration, grad):
        combined_grad= math.sqrt(grad[0]**2+grad[1]**2+grad[2]**2+grad[3]**2+grad[4]**2+grad[5]**2+grad[6]**2+grad[7]**2+grad[8]**2+grad[9]**2+grad[10]**2+grad[11]**2+grad[12]**2)
        #print "curr_iteration:", curr_iteration, "combined_grad:", combined_grad, "Done."
        if combined_grad <= 10**-3:
            if debug: this.logger.info("Rs converged by rule 1 combined gradient- %s" %(combined_grad))
            return True
        return False

    def fTOs(this, f):
        s = ('%s' % (float(f))).replace('.', '_')
        return s

    def get_gor_data(this):
        # bubble point pressure at rsb or rs[0] from Mongo
        fluid_codes = {}
        for data in this.cenDbPBO.find({'Property': 'SolutionGOR'}):
            fluid_codes[data['OilType']] = {'GOR': data['GOR']}
            for g in data['GOR']: fluid_codes[data['OilType']][float(g)] = data[this.fTOs(g)]
        if this.gor_pb in fluid_codes[this.fluid_code]['GOR']:
            return np.array(fluid_codes[this.fluid_code][this.gor_pb])
        else:
            mx = [i for i in fluid_codes[this.fluid_code]['GOR'] if i > this.gor_pb]
            if len(mx) == 0:
                mx = fluid_codes[this.fluid_code]['GOR'][-1]
                mn = fluid_codes[this.fluid_code]['GOR'][-2]
            else:
                mx = min(mx)
                mn = [i for i in fluid_codes[this.fluid_code]['GOR'] if i < this.gor_pb]
                if len(mn) == 0:
                    mn = 0
                else:
                    mn = max(mn)
            bppMx = np.array(fluid_codes[this.fluid_code][mx])
            if mn == 0:
                bppMn = np.zeros(len(bppMx))
            else:
                bppMn = np.array(fluid_codes[this.fluid_code][mn])
        return bppMn + (bppMx - bppMn) / (mx - mn) * (this.gor_pb - mn)


    def objective(this, _svars, _iniVars, pressure, pr, rs, objFunMul):
        #objective
        A0, A1, A2, A3   = _svars[0]* _iniVars[0], _svars[1]* _iniVars[1],  _svars[2]* _iniVars[2],  _svars[3]* _iniVars[3]
        A4, A5, A6, A7   = _svars[4]* _iniVars[4], _svars[5]* _iniVars[5],  _svars[6]* _iniVars[6],  _svars[7]* _iniVars[7]
        A8, A9, A10, A11 = _svars[8]* _iniVars[8], _svars[9]* _iniVars[9], _svars[10]* _iniVars[10], _svars[11]* _iniVars[11]
        A12= (10.**-3.*_iniVars[12]) if _svars[12]==0 else (copysign(1., _svars[12])*10.**-3. if abs(_svars[12])<=10.**-3. else _svars[12])*_iniVars[12]
        #A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12=0.000594214,0.800195371,0.351155927,1.648604444,1.383684448,-2.415446295,3.945618857,-0.290979816,-0.083018364,-0.29536467,-0.281598998,0.563825399,0.010010279
        mulFactor= pr ** A12
        rsFactor1 =A0*this.pb_res_temp**A1*this.gas_sp_gravity**A2*this.temp_res**A3*this.api_gravity**A4*this.gor_pb**A5*pressure**(A6*this.pb_res_temp**A7*this.gas_sp_gravity**A8*this.temp_res**A9*this.api_gravity**A10*this.gor_pb**A11)
        rs_calc= rsFactor1 * mulFactor
        rslen= len(rs)
        objFun= [objFunMul[i]*(10.**50 if rs_calc[i]>10.**50 else ((rs[i]-rs_calc[i])/rs[i])**2) for i in range(rslen)]
        obj= sum(objFun)
        return obj, rs_calc

    def normDiffVars(this, d):
        r= None
        if(d>=10.**100): r=10.**10
        elif(d>=10.**90): r=(d*10.**-85)
        elif(d>=10.**85): r=(d*10.**-80)
        elif(d>=10.**80): r=(d*10.**-75)
        elif(d>=10.**75): r=(d*10.**-70)
        elif(d>=10.**70): r=(d*10.**-65)
        elif(d>=10.**65): r=(d*10.**-60)
        elif(d>=10.**60): r=(d*10.**-55)
        elif(d>=10.**55): r=(d*10.**-50)
        elif(d>=10.**50): r=(d*10.**-45)
        elif(d>=10.**45): r=(d*10.**-40)
        elif(d>=10.**40): r=(d*10.**-35)
        elif(d>=10.**35): r=(d*10.**-30)
        elif(d>=10.**30): r=(d*10.**-25)
        elif(d>=10.**25): r=(d*10.**-20)
        elif(d>=10.**20): r=(d*10.**-12)
        elif(d>=10.**15): r=(d*10.**-5)
        elif(d>=10.**10): r=(d*10.**-4)
        else: r=d
        return r

    def _cost(this, vars, kwargs, print_r=False):
        _svars, _iniVars, pressure, pr, rs, objFunMul= vars, kwargs[0], kwargs[1], kwargs[2], kwargs[3], kwargs[4]
        #objective
        A0, A1, A2, A3   = _svars[0]* _iniVars[0], _svars[1]* _iniVars[1],  _svars[2]* _iniVars[2],  _svars[3]* _iniVars[3]
        A4, A5, A6, A7   = _svars[4]* _iniVars[4], _svars[5]* _iniVars[5],  _svars[6]* _iniVars[6],  _svars[7]* _iniVars[7]
        A8, A9, A10, A11 = _svars[8]* _iniVars[8], _svars[9]* _iniVars[9], _svars[10]* _iniVars[10], _svars[11]* _iniVars[11]
        A12= (10.**-3.*_iniVars[12]) if _svars[12]==0 else (copysign(1., _svars[12])*10.**-3. if abs(_svars[12])<=10.**-3. else _svars[12])*_iniVars[12]
        mulFactor= pr ** A12
        rsFactor1= -1000. if (this.pb_res_temp**A1>10.**25 or this.gas_sp_gravity**A2>10.**25 or this.temp_res**A3>10.**25 or this.gor_pb**A5>10.**25 or this.api_gravity**A4>10.**25 or (A6*this.pb_res_temp**A7*this.gas_sp_gravity**A8*this.temp_res**A9*this.api_gravity**A10*this.gor_pb**A11)>50.) else (A0*this.pb_res_temp**A1*this.gas_sp_gravity**A2*this.temp_res**A3*this.api_gravity**A4*this.gor_pb**A5*pressure**(A6*this.pb_res_temp**A7*this.gas_sp_gravity**A8*this.temp_res**A9*this.api_gravity**A10*this.gor_pb**A11))
        rs_calc= rsFactor1 * mulFactor
        rslen= len(rs_calc)
        objFun= [objFunMul[i]*(10.**50 if rs_calc[i]>10.**50 else ((rs[i]-rs_calc[i])/rs[i])**2) for i in range(rslen)]
        obj= sum(objFun)
        #print "Total Obj:", obj
        #objective
        tmp3= this.pb_res_temp**A1>10.**25 or this.gas_sp_gravity**A2>10.**25 or this.temp_res**A3>10.**25 or this.gor_pb**A5>10.**25 or this.api_gravity**A4>10.**25 or (A6*this.pb_res_temp**A7*this.gas_sp_gravity**A8*this.temp_res**A9*this.api_gravity**A10*this.gor_pb**A11)>50.
        Diff_rs_factor_var1= -10000. if (tmp3) else (_iniVars[0]*this.pb_res_temp**A1*this.gas_sp_gravity**A2*this.temp_res**A3*this.api_gravity**A4*this.gor_pb**A5*pressure**(A6*this.pb_res_temp**A7*this.gas_sp_gravity**A8*this.temp_res**A9*this.api_gravity**A10*this.gor_pb**A11))

        tmp= 0. if(tmp3) else (A0*this.pb_res_temp**A1*this.gas_sp_gravity**A2*this.temp_res**A3*this.api_gravity**A4*this.gor_pb**A5*pressure**(A6*this.pb_res_temp**A7*this.gas_sp_gravity**A8*this.temp_res**A9*this.api_gravity**A10*this.gor_pb**A11))
        Diff_rs_factor_var2= -10000. if (tmp3) else tmp*(_iniVars[1]*log(this.pb_res_temp))
        Diff_rs_factor_var3= -10000. if (tmp3) else tmp*(_iniVars[2]*log(this.gas_sp_gravity))
        Diff_rs_factor_var4= -10000. if (tmp3) else tmp*(_iniVars[3]*log(this.temp_res))
        Diff_rs_factor_var5= -10000. if (tmp3) else tmp*(_iniVars[4]*log(this.api_gravity))
        Diff_rs_factor_var6= -10000. if (tmp3) else tmp*(_iniVars[5]*log(this.gor_pb))
        Diff_rs_factor_var7= -10000. if (tmp3) else tmp*(np.log(pressure)*(this.pb_res_temp**A7*this.gas_sp_gravity**A8*this.temp_res**A9*this.api_gravity**A10*this.gor_pb**A11)*_iniVars[6])
        tmp1= (A6*this.pb_res_temp**A7*this.gas_sp_gravity**A8*this.temp_res**A9*this.api_gravity**A10*this.gor_pb**A11)
        Diff_rs_factor_var8 = -10000. if (tmp3) else tmp*(np.log(pressure)*tmp1*_iniVars[7]*log(this.pb_res_temp))
        Diff_rs_factor_var9 = -10000. if (tmp3) else tmp*(np.log(pressure)*tmp1*_iniVars[8]*log(this.gas_sp_gravity))
        Diff_rs_factor_var10= -10000. if (tmp3) else tmp*(np.log(pressure)*tmp1*_iniVars[9]*log(this.temp_res))
        Diff_rs_factor_var11= -10000. if (tmp3) else tmp*(np.log(pressure)*tmp1*_iniVars[10]*log(this.api_gravity))
        Diff_rs_factor_var12= -10000. if (tmp3) else tmp*(np.log(pressure)*tmp1*_iniVars[11]*log(this.gor_pb))
        Diff_rs_factor_var13= np.zeros(rslen)
        Diff_mul_factor_var1,Diff_mul_factor_var2,Diff_mul_factor_var3,Diff_mul_factor_var4 ,Diff_mul_factor_var5, Diff_mul_factor_var6 = np.zeros(rslen), np.zeros(rslen), np.zeros(rslen), np.zeros(rslen), np.zeros(rslen), np.zeros(rslen)
        Diff_mul_factor_var7,Diff_mul_factor_var8,Diff_mul_factor_var9,Diff_mul_factor_var10,Diff_mul_factor_var11,Diff_mul_factor_var12= np.zeros(rslen), np.zeros(rslen), np.zeros(rslen), np.zeros(rslen), np.zeros(rslen), np.zeros(rslen)
        Diff_mul_factor_var13=(pr**A12)*np.log(pr)*A12
        Diff_Rs_var1 =mulFactor*Diff_rs_factor_var1 +rsFactor1*Diff_mul_factor_var1
        Diff_Rs_var2 =mulFactor*Diff_rs_factor_var2 +rsFactor1*Diff_mul_factor_var2
        Diff_Rs_var3 =mulFactor*Diff_rs_factor_var3 +rsFactor1*Diff_mul_factor_var3
        Diff_Rs_var4 =mulFactor*Diff_rs_factor_var4 +rsFactor1*Diff_mul_factor_var4
        Diff_Rs_var5 =mulFactor*Diff_rs_factor_var5 +rsFactor1*Diff_mul_factor_var5
        Diff_Rs_var6 =mulFactor*Diff_rs_factor_var6 +rsFactor1*Diff_mul_factor_var6
        Diff_Rs_var7 =mulFactor*Diff_rs_factor_var7 +rsFactor1*Diff_mul_factor_var7
        Diff_Rs_var8 =mulFactor*Diff_rs_factor_var8 +rsFactor1*Diff_mul_factor_var8
        Diff_Rs_var9 =mulFactor*Diff_rs_factor_var9 +rsFactor1*Diff_mul_factor_var9
        Diff_Rs_var10=mulFactor*Diff_rs_factor_var10+rsFactor1*Diff_mul_factor_var10
        Diff_Rs_var11=mulFactor*Diff_rs_factor_var11+rsFactor1*Diff_mul_factor_var11
        Diff_Rs_var12=mulFactor*Diff_rs_factor_var12+rsFactor1*Diff_mul_factor_var12
        Diff_Rs_var13=mulFactor*Diff_rs_factor_var13+rsFactor1*Diff_mul_factor_var13
        tmp= (2.*(np.array(rs)-rs_calc)/np.array(rs)**2)
        diff_total_obj_var1, diff_total_obj_var2, diff_total_obj_var3 =this.normDiffVars(np.sum(tmp*(-1)*Diff_Rs_var1*objFunMul)), this.normDiffVars(np.sum(tmp*(-1)*Diff_Rs_var2*objFunMul)), this.normDiffVars(np.sum(tmp*(-1)*Diff_Rs_var3*objFunMul))
        diff_total_obj_var4, diff_total_obj_var5, diff_total_obj_var6 =this.normDiffVars(np.sum(tmp*(-1)*Diff_Rs_var4*objFunMul)), this.normDiffVars(np.sum(tmp*(-1)*Diff_Rs_var5*objFunMul)), this.normDiffVars(np.sum(tmp*(-1)*Diff_Rs_var6*objFunMul))
        diff_total_obj_var7, diff_total_obj_var8, diff_total_obj_var9 =this.normDiffVars(np.sum(tmp*(-1)*Diff_Rs_var7*objFunMul)), this.normDiffVars(np.sum(tmp*(-1)*Diff_Rs_var8*objFunMul)), this.normDiffVars(np.sum(tmp*(-1)*Diff_Rs_var9*objFunMul))
        diff_total_obj_var10, diff_total_obj_var11, diff_total_obj_var12 =this.normDiffVars(np.sum(tmp*(-1)*Diff_Rs_var10*objFunMul)), this.normDiffVars(np.sum(tmp*(-1)*Diff_Rs_var11*objFunMul)), this.normDiffVars(np.sum(tmp*(-1)*Diff_Rs_var12*objFunMul))
        diff_total_obj_var13= this.normDiffVars(np.sum(tmp*(-1)*Diff_Rs_var13*objFunMul))
        return obj, np.array([diff_total_obj_var1, diff_total_obj_var2, diff_total_obj_var3, diff_total_obj_var4, diff_total_obj_var5, diff_total_obj_var6, diff_total_obj_var7, diff_total_obj_var8, diff_total_obj_var9, diff_total_obj_var10, diff_total_obj_var11, diff_total_obj_var12, diff_total_obj_var13])

    def solve(this):
        this.vars           = np.ones(13) #13 variables.
        this.bounds         = [[-3.,3.] for i in range(12)] #13 iter_vars ini values from excel/gui
        this.bounds.append([-10., 5000.])
        this.bounds         = np.array(this.bounds)
        this.iniVars        = np.array([0.0006,0.856,0.351,1.829,1.462,-2.116,3.867,-0.306,-0.083,-0.306,-0.288,0.525,0.01])
        pressure_obsv       = np.arange(this.pressure[0], this.pressure[-1], -10. if this.pressure[0] <= 150 else -20)
        pressure_obsv       = np.append(pressure_obsv, [this.pressure[-1]])
        rs_obsv             = monotone(this.pressure.tolist(), this.rs.tolist(), pressure_obsv.tolist())
        zero_index          = rs_obsv.index(0) if 0 in rs_obsv else len(rs_obsv)
        pr                  = pressure_obsv/pressure_obsv[0]
        objFunMul           = [0.01 if(pr[i]<=0.05) else 1. if(pr[i]<=0.1) else 100. if(pr[i]>0.2) else 50. for i in range(len(pr))]
        var_calc            = ncg().minimize(this.vars, this.bounds, this._cost, this.logger, max_iter=300, debug=this.debug, region_f=this.define_region, converge_f=this.converge_f, args=(this.iniVars, pressure_obsv[:zero_index], pr[:zero_index], rs_obsv[:zero_index], objFunMul[:zero_index]))
        #obj, rs_calc        = this.objective(this.iniVars, var_calc, pressure_obsv[:zero_index], pr[:zero_index], rs_obsv[:zero_index], objFunMul[:zero_index])
        obj, rs_calc        = this.objective(this.iniVars, var_calc, pressure_obsv, pr, rs_obsv[:zero_index], objFunMul)
        rs_multiplier       = rs_obsv/rs_calc
        rs_final            = rs_calc * rs_multiplier
        this.compVars       = this.iniVars * var_calc 

    def result_solver(this):
        this.tempDegFRange= 5.
        this.tempDegF=np.arange(10., 1205., this.tempDegFRange)
        rsb= np.repeat(this.gor_pb, len(this.tempDegF))
        k1= this.compVars[0]*this.pb_res_temp**this.compVars[1]*this.gas_sp_gravity**this.compVars[2]*this.tempDegF**this.compVars[3]*this.api_gravity**this.compVars[4]*this.gor_pb**this.compVars[5]
        k2= this.compVars[6]*this.pb_res_temp**this.compVars[7]*this.gas_sp_gravity**this.compVars[8]*this.tempDegF**this.compVars[9]*this.api_gravity**this.compVars[10]*this.gor_pb**this.compVars[11]
        pb= np.exp((np.log(rsb)-np.log(k1))/k2)
        pbMiltiplier= this.pb_res_temp / monotone(this.tempDegF.tolist(), pb.tolist(), [this.temp_res])[0]
        this.finalBPpressure= pb*pbMiltiplier
        # rateOfChangePb= (this.finalBPpressure[1:]-this.finalBPpressure[0:-1])/(this.tempDegF[1:]-this.tempDegF[0:-1])
        # this.markerIdx= len(rateOfChangePb)
        # for i in range(len(rateOfChangePb)): 
        #     if rateOfChangePb[i] <= 0.085:
        #         this.markerIdx= i
        #         break
        # this.finalMaxPointTemp= this.temp_res if(this.tempDegF[this.markerIdx] <= this.temp_res) else this.tempDegF[this.markerIdx]
        # this.finalMaxPointPressure= this.pb_res_temp if(this.tempDegF[this.markerIdx] <= this.temp_res) else this.finalBPpressure[this.markerIdx]

    def pIdx(this, n):
        return 0 if (this.markerIdx-n) < 0 else (this.markerIdx-n)

    def nIdx(this, n):
        return len(this.tempDegF) if ((this.markerIdx+n) > len(this.tempDegF)) else this.markerIdx+n

    def setMinMaxIdx(this):
        this.minIdx= 10 #-50  degree
        this.maxIdx= 20 #+100 degree

    def presssureAtMaxTemp(this):
        tempDegFL50= this.tempDegF[this.pIdx(this.minIdx):this.markerIdx+1]
        pressureL50= this.finalBPpressure[this.pIdx(this.minIdx):this.markerIdx+1]
        mata= np.array([np.ones(len(tempDegFL50)), tempDegFL50, tempDegFL50**2]).T
        matA= mata.T.dot(mata)
        matB= np.linalg.inv(matA).dot(mata.T.dot(pressureL50))
        
        temp= this.tempDegF[this.pIdx(this.minIdx):this.nIdx(this.maxIdx)+1]
        pressure1= np.array([matB[0]+matB[1]*temp[i]+matB[2]*temp[i]**2 for i in range(len(temp))])  #matB will have 3 elements always?
        t1= this.tempDegF[this.markerIdx]
        t2= this.tempDegF[this.nIdx(this.maxIdx)]
        p1= pressure1[temp.tolist().index(t1)]
        p2= pressure1[temp.tolist().index(t2)]
        p2= p2 if(p2<(p1/1.25)) else (p1/1.25)
        b2= (t2**2*p1**2-t1**2*p2**2)/(t2**2-t1**2)
        a2= t1**2/(1.-p1**2/b2)
        temp2, factorA, pressure2, diffPresswrtT= [], [], [], []
        t = t2
        offset= None
        while 1:
            fA=(1.-t**2/a2)*b2
            print fA
            if fA < 0: break
            p= np.sqrt(fA)
            pressure2.append(p)
            if offset is None: offset= p
            diffPresswrtT.append((b2/a2)*(-1.*t/p))
            temp2.append(t)
            t += 5. 
        this.offset= offset
        this.temp2= temp2
        this.pressure2= pressure2

        this.diff_at_90= monotone(temp2, diffPresswrtT, [-90.])[0]
        this.pressureMaxTemp= 0. if(((1.-this.diff_at_90**2/a2)*b2)<0) else sqrt((1.-this.diff_at_90**2/a2)*b2)

        this.finalQuadraticSetT= np.array([temp[0], t1, t2])
        this.finalQuadraticSetP= np.array([pressure1[0], p1, p2])


    def finalSmoothedPressure(this):
        mata= np.array([np.ones(len(this.finalQuadraticSetT)), this.finalQuadraticSetT, this.finalQuadraticSetT**2]).T
        matA= mata.T.dot(mata)
        matB= np.linalg.inv(matA).dot(mata.T.dot(this.finalQuadraticSetP))

        temp          = this.tempDegF[this.pIdx(this.minIdx):this.nIdx(this.maxIdx)+1]
        pressure      = np.array([matB[0]+matB[1]*temp[i]+matB[2]*temp[i]**2 for i in range(len(temp))])  #matB will have 3 elements always?
        pressureOffset= pressure[-1]-this.offset

        temp1         = np.append(temp, this.temp2[1:])
        temp1         = np.append(temp1, [temp1[-1]+this.tempDegFRange])
        pressure_quad = np.append(pressure, np.zeros(len(this.pressure2)-1))
        pressure_quad = np.append(pressure_quad, [0.])
        pressure_ellip= np.append(np.zeros(len(pressure)), this.pressure2[1:])
        pressure_ellip= np.append(pressure_ellip, [0.])

        temp2         = [temp1[0]]
        for i in range(1, len(temp1)): temp2.append((temp2[-1]+5.) if((temp2[-1]+5.)<=this.diff_at_90) else this.diff_at_90)
        pressure_corr = [pressure_quad[i] if(temp1[i]<=this.temp2[0]) else this.pressureMaxTemp if(temp2[i]==this.diff_at_90) else (pressure_ellip[i]+pressureOffset) for i in range(len(temp1))]

        temp_final    = np.append(this.tempDegF[0:this.markerIdx+1], temp2[temp.tolist().index(this.tempDegF[this.markerIdx])+1:])
        pressure_final= np.append(this.finalBPpressure[0:this.markerIdx+1], pressure_corr[temp.tolist().index(this.tempDegF[this.markerIdx])+1:])
        degree50idx   = 0
        for i in range(len(temp_final)): 
            if temp_final[i] >= 50.: 
                degree50idx= i
                break
        temp4         = temp_final[degree50idx:]
        pressure4     = pressure_final[degree50idx:]
        mata          = np.array([np.ones(len(temp4)), temp4, temp4**2, temp4**3, temp4**4, temp4**5]).T
        matA          = mata.T.dot(mata)
        matB          = np.linalg.inv(matA).dot(mata.T.dot(pressure4))
        pressureSmooth= []
        #temp4= np.append(temp4, [0.])
        #temp4= np.append(temp4, [0.])
        for i in np.append(this.tempDegF[0:this.markerIdx+1], temp2[temp.tolist().index(this.tempDegF[this.markerIdx])+1:]): #temp_final with first 50 degree
            ps= matB[0]
            for j in range(1,len(matB)): ps += matB[j]*i**j
            pressureSmooth.append(ps)

        offset= pressure_final[this.markerIdx] - pressureSmooth[this.markerIdx]
        pressure_final_smooth_1= [0. if((p+offset)<0.) else (p+offset) for p in pressureSmooth]
        temp_final_smooth    = temp_final
        pressure_final_smooth= np.append(pressure_final[0:this.markerIdx+1], pressure_final_smooth_1[this.markerIdx+1:])
        print np.array([temp_final_smooth, pressure_final_smooth])
        return temp_final_smooth,pressure_final_smooth

    def fTOs(this, f):
        s=('%s' %(float(f))).replace('.', '_')
        return s

    def copyMasterData(this):
        r= this.cenDbPBO.remove({'Property':'SolutionGOR'})
        dt= datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
        fluid_types_excel= 'Reqs/Fluid_Type_DATABASE.xlsx'
        if not os.path.exists(fluid_types_excel):
             this.logger.error("File not found:%s" %(fluid_types_excel)) 
             sys.exit(1)
        xls = pd.ExcelFile(fluid_types_excel)
        for sheetname in this.fluid_codes:
            df= pd.read_excel(xls, sheet_name=sheetname)
            gor=[]
            data={}
            data['Property']='SolutionGOR'
            data['OilType']=sheetname
            for col in df.columns: 
                if isinstance(col, (int, float, long)): 
                    gor.append(float(col))
                    data[this.fTOs(col)]= df[col].tolist()
            gor.sort()
            data['GOR']= gor
            data['CreatedAt']= dt
            this.cenDbPBO.insert(data)
            this.logger.info('MongoDB Central::PBO updated for Property:SolutionGOR, Type:%s' %(sheetname))
        this.logger.info("MongoDB Central database updated!")

    def getMatB(this, seOfOnes):
        m1= this.finalBPpressure[seOfOnes]/this.bppInterpolated_db[seOfOnes]
        t1= np.array([np.ones(len(m1)), this.temp2[seOfOnes]]).T
        matA= t1.T.dot(t1)
        matB= np.linalg.inv(matA).dot(t1.T.dot(m1))
        return m1, matB

    def getBPPusingMiltipler(this):
        s, e= None, None
        this.error_perc_marker= np.array([0 if i >= 0.8 else 1 for i in this.error_perc])
        ones= np.where(this.error_perc_marker == 1)[0]
        mat1B, mat2B, multiplier=None, None, None
        finalBPpressure= None
        if(len(ones)==0): 
            #multiplier= np.ones(len(temp2))
            finalBPpressure= this.bppInterpolated_db
        else: 
            seOfOnes= np.split(ones, np.where(np.diff(ones) != 1)[0]+1)[0] #array of index of consecutiive 1
            if len(seOfOnes) < 2:
                finalBPpressure= this.bppInterpolated_db
            elif(len(seOfOnes) >= 2 and len(seOfOnes) <=6):
                m1, mat1B= this.getMatB(seOfOnes[0:5])
                multiplier= np.array([0.8 if((mat1B[0]+mat1B[1]*i)<0.8) else (mat1B[0]+mat1B[1]*i) for i in this.temp2])
                multiplier[seOfOnes[0]]= m1[0]
                multiplier[seOfOnes[1]:]= 1.
                finalBPpressure= this.bppInterpolated_db * multiplier 
                finalBPpressure[seOfOnes[1:-1]]= this.finalBPpressure[seOfOnes[1:-1]]
            else:
                m1, mat1B= this.getMatB(seOfOnes[0:5])
                multiplier= np.array([0.8 if((mat1B[0]+mat1B[1]*i)<0.8) else (mat1B[0]+mat1B[1]*i) for i in this.temp2])
                multiplier[seOfOnes[0]]= m1[0]
                multiplier[seOfOnes[1:-1]]= 1.
                m2, mat2B= this.getMatB(seOfOnes[5:][-5:])
                multiplier[seOfOnes[-1]]= m2[-1]
                if len(this.temp2) > (seOfOnes[-1]+1):
                    multiplier[seOfOnes[-1]+1:]= np.array([0.8 if((mat2B[0]+mat2B[1]*i)<0.8) else (mat2B[0]+mat2B[1]*i) for i in this.temp2[seOfOnes[-1]+1:]])

                finalBPpressure= this.bppInterpolated_db * multiplier 
                finalBPpressure[seOfOnes[1:-1]]= this.finalBPpressure[seOfOnes[1:-1]]
        return finalBPpressure

    def finalBubblePointPressure(this):
        bppAtRsb= this.get_gor_data()[1:]
        temp1= np.arange(10., 10.*(len(bppAtRsb)+1), 10.)
        bbpAtRefTemp= monotone(temp1.tolist(), bppAtRsb.tolist(), [this.temp_res])[0]
        calibration_Mul= this.pb_res_temp/bbpAtRefTemp
        bpp= bppAtRsb*calibration_Mul
        #this.temp2= np.arange(this.tempRange, 510., this.tempRange)  #10 to 500 dg f
        this.temp2= np.copy(this.output_temps)
        this.bppInterpolated_db= np.array(monotone(temp1.tolist(), bpp.tolist(), this.temp2.tolist()))
        this.finalBPpressure= this.finalBPpressure[0:len(this.bppInterpolated_db)]
        this.error_perc= np.abs((this.bppInterpolated_db-this.finalBPpressure)/this.bppInterpolated_db)*100.
        this.bppCalculated= this.getBPPusingMiltipler()
        #return this.temp2, this.bppInterpolated_db, this.bppCalculated
        return this.temp2, this.bppCalculated

    def plot2D(this, temp, bppDb, bppCalculated):
        if this.debug: 
            plt.style.use('dark_background')
            fig, ax = plt.subplots()
            plt.scatter(temp, bppDb, label='DB')        
            plt.scatter(temp, bppCalculated, label='Calc')        
            plt.xlabel('temp')
            plt.ylabel('pressure')
            plt.title('SolutionGOR Bubble Point')
            plt.legend()
            plt.tight_layout(pad=1.7)
            #plt.show()
            graphPath= os.path.join(getPOASRootDir(), 'Graphs', this.dbname, this.options.wellno, 'SolutionGOR_BubblePoint.png')
            if not os.path.exists(os.path.dirname(graphPath)):
                os.makedirs(os.path.dirname(graphPath))        
            fig.savefig(graphPath, dpi=fig.dpi)
            plt.cla()
            plt.close(fig)
            print "Graphs:%s" %(graphPath)


    # def calculate(this):
    #     this.setMinMaxIdx()
    #     this.presssureAtMaxTemp()
    #     this.finalSmoothedPressure()

    def multipleBubblePoints(this, temp, bppCalculated):
        this.logger.info("Found multipleBubblePoints")
        temp_res= {'temperature':this.temp_res, 'bubble_point':this.pb_res_temp}
        df_mbp_sorted= this.df_multiple_bubble_points.append(temp_res, ignore_index = True)
        df_mbp_sorted.sort_values('temperature', inplace=True)
        df_mbp_sorted= df_mbp_sorted.reset_index(drop=True)
        df_mbp_sorted['calculated_pb']= monotone(temp.tolist(), bppCalculated.tolist(), df_mbp_sorted['temperature'].tolist())
        df_mbp_sorted['multipler']    = df_mbp_sorted['bubble_point']/df_mbp_sorted['calculated_pb']
        df_mbp_sorted['multipler_ln'] = np.log(df_mbp_sorted['multipler'])
        mata= np.array([np.ones(len(df_mbp_sorted['temperature'])), df_mbp_sorted['temperature']]).T
        matA= mata.T.dot(mata)
        matB= np.linalg.inv(matA).dot(mata.T.dot(df_mbp_sorted['multipler_ln'])) 
        temp_Multipler= [0.75 if(math.exp(matB[0]+matB[1]*i)<0.75) else math.exp(matB[0]+matB[1]*i) for i in temp]
        obs_multiplier= df_mbp_sorted['multipler'][df_mbp_sorted.index[df_mbp_sorted['temperature'] == this.temp_res].tolist()[0]]
        ratio_correction= monotone(temp.tolist(), temp_Multipler, [this.temp_res])[0]/obs_multiplier
        final_bubble_point_corrected= temp_Multipler*bppCalculated*ratio_correction
        return final_bubble_point_corrected


    def solve_excel(this):
        this.debug= this.options.debug
        this.coll= this.db[this.options.wellno] 
        this.logger= getLogger(this.dbname, this.options.wellno)
        if this.options.copymasterdata: this.copyMasterData()
        if this.options.excel is not None: this.readExcel()
        this.readMongo()
        this.input_data()
        this.solve()
        #this.calculate()
        this.result_solver()
        temp, bppCalculated= this.finalBubblePointPressure()
        if not this.df_multiple_bubble_points.empty:
            bppCalculated= this.multipleBubblePoints(temp, bppCalculated)
        print np.array([temp, bppCalculated])
        #this.plot2D(temp, bppDb, bppCalculated)

    def readDf(this,df_fluid_types,df_general_data,df_separator,df_separator_corrected,df_multiple_bubble_points):
        this.df_fluid_types           = df_fluid_types
        this.df_general_data          = df_general_data
        this.df_separator             = df_separator
        this.df_separator_corrected   = df_separator_corrected
        this.df_multiple_bubble_points= df_multiple_bubble_points

    def solve_df1(this, df_fluid_types,df_general_data,df_separator,df_separator_corrected,df_multiple_bubble_points,fluid_code, logger, debug=False):
        #this.debug= debug
        #this.logger= logger
        #this.fluid_code= fluid_code
        #this.readDf(df_fluid_types,df_general_data,df_separator,df_separator_corrected,df_multiple_bubble_points) #df inputs from PVT
        #this.input_data()
        this.solve()
        #this.calculate()
        this.result_solver()
        temp, bppCalculated= this.finalBubblePointPressure()
        # bppCalculated= 
        if not this.df_multiple_bubble_points.empty:
            bppCalculated= this.multipleBubblePoints(temp, bppCalculated)
        #print np.array([temp, bppCalculated])
        return temp, bppCalculated

    def solve_df23(this, df_general_data,df_multiple_bubble_points,fluid_code,logger,debug=False):
        if this.gor_pb is None: 
            this.logger.error("general-data::gor_pb is mandatory for case 2 and 3")
            sys.exit(1)
        bppAtRsb        = this.get_gor_data()[1:]
        temp            = np.arange(10., 10.*(len(bppAtRsb)+1), 10.)
        bp_Tres_Data    = monotone(temp.tolist(), bppAtRsb.tolist(), [this.temp_res])[0]
        calibration_Mul = this.pb_res_temp/bp_Tres_Data
        bppDb           = bppAtRsb*calibration_Mul
        #temp2           = np.arange(this.tempRange, 510., this.tempRange)  #10 to 500 dg f
        temp2= np.copy(this.output_temps)
        bppCalculated   = np.array(monotone(temp.tolist(), bppDb.tolist(), temp2.tolist()))
        if not this.df_multiple_bubble_points.empty:
            bppCalculated= this.multipleBubblePoints(temp2, bppCalculated)
        return temp2, bppCalculated

if __name__ == "__main__":
    obj = SolutionGORBubblePoint(dbName='PBO')
    obj.solve_excel()
    
    #Usage: 
    #For excel input:
    #python SolutionGORBubblePoint.py -w AB8 -t 'bo' --excel
    #For dataframe input:
    #obj = SolutionGORBubblePoint(dbName='PBO')
    #obj.solve_df(obj.df_fluid_types,obj.df_general_data,obj.df_separator,obj.df_separator_corrected,obj.df_multiple_bubble_points,'bo', obj.logger, debug=True)
    #USED for updating MASTER/CENTRAL database only. solve_excel() is not updated for case 2, 3 and 4 (its executed from top script i.e. SolutionGOR.py)
    #python SolutionGORBubblePoint.py -w AB8 -t 'bo' -c