import io, os, sys, time, math
from collections import OrderedDict
from copy import deepcopy
import numpy as np
from Correlations import *


class RsTempPressureBOC(object):
    def __init__(this, parent):
        #this.__dict__ = parent.__dict__.copy()
        this.__dict__.update(parent.__dict__)
        this.tempRange= 10.
        this.correlations= Correlations(this)

    def defaultsFor14_696(this):
        gor_pb = 0 if(this.gor_pb is None or math.isnan(this.gor_pb)) else this.gor_pb
        return [14.697, 14.696], [gor_pb, 0.0], [gor_pb, 0.0], [None, None]

    def getBelowBpPressures(this, p):
        end= 14.696
        interval= (p-14.696)/50. if (p-14.696)/50. < 1 else int((p-14.696)/50.)
        bbpp= np.append(np.arange(p, end, -interval), end)          
        return bbpp  

    def yg_lasater(this):
        effective_Oil= (630.-10.*this.api_gravity) if(this.api_gravity<=40.) else (73110.*this.api_gravity**(-1.562))
        yg=None
        if this.caseno == 2: #gor_pb provided
            yg= (this.gor_pb/379.3)/((this.gor_pb/379.3)+350.*this.api_specific_gravity/effective_Oil)
        if this.caseno == 1 or this.caseno == 3: #gor_pb not provided
            pf= this.pb_res_temp*this.gas_sp_gravity/(this.temp_res+459.67)
            yg= (0.00000921882158166909*pf**6. + 0.000179725109716884*pf**5. - 0.00592139779195938*pf**4. + 0.0514814619008946*pf**3. - 0.206451620328116*pf**2. + 0.501545401892031*pf - 0.018872249243691) if(pf<=5) else (0.41964896156801*math.log(pf) + 0.0849694367651798)
        return yg, effective_Oil

    def pb_res_temp_atCorrelation(this, bo_correlations_selected, gor_pb, temp_res):
        pb_res_temp= this.correlations.pb_res_temp_atCorrelations(bo_correlations_selected, gor_pb, temp_res)
        return pb_res_temp

    def multiplier_for_gor_pb_atCorrelation(this, bo_correlations_selected, pb_res_temp_calc):
        multiplier_for_gor_pb= this.correlations.multiplier_for_gor_pb_atCorrelations(bo_correlations_selected, pb_res_temp_calc)
        return multiplier_for_gor_pb

    def rs_atCorrelation(this, bo_correlations_selected, p, t):
        gor, yg= this.correlations.rs_atCorrelations(bo_correlations_selected, p, t)
        return gor, yg

    def rs_correction(this, rs, reducedPressure):
        rp= np.array([1., ((1.+reducedPressure[-1])/2.) if(reducedPressure[-1]>=0.5) else 0.5, reducedPressure[-1]])
        rc= np.array([10**-6, rs[-1]/4., rs[-1]])
        mata= np.array([np.ones(3), rp, rp**2]).T
        matA= mata.T.dot(mata)
        matB= np.linalg.inv(matA).dot(mata.T.dot(rc))
        rs_correction= [0. if(r==1) else 0. if((matB[0]+matB[1]*r+matB[2]*r**2)<0) else (matB[0]+matB[1]*r+matB[2]*r**2) for r in reducedPressure]
        final_rs= np.array(rs - rs_correction)
        return final_rs

    def getFinalRsAt_temp_res(this, bo_correlations_selected):
        data= OrderedDict()
        pressure_at_temp_rs, gor_at_correlation, gor_at_correlation1, yg1 = None, None, None, None

        if this.caseno == 1:
            if this.pb_res_temp <= 14.696: 
                return this.defaultsFor14_696() #gor_pb is not calculated yet hence 0
            pressure_at_temp_rs= this.getBelowBpPressures(this.pb_res_temp)
            temp_at_temp_rs= np.repeat(this.temp_res, len(pressure_at_temp_rs))
            gor_at_correlation, yg1= this.rs_atCorrelation(bo_correlations_selected, pressure_at_temp_rs, this.temp_res)
            gor_at_correlation1= gor_at_correlation
            
        if this.caseno == 2:
            pb_res_temp_calc= this.pb_res_temp_atCorrelation(bo_correlations_selected, this.gor_pb, this.temp_res)
            if pb_res_temp_calc <= 14.696: return this.defaultsFor14_696()
            pressure_at_temp_rs= this.getBelowBpPressures(pb_res_temp_calc)
            temp_at_temp_rs= np.repeat(this.temp_res, len(pressure_at_temp_rs))
            gor_at_correlation, yg1= this.rs_atCorrelation(bo_correlations_selected, pressure_at_temp_rs, this.temp_res)
            gor_at_correlation1= gor_at_correlation
            
        if this.caseno == 3:
            if this.pb_res_temp <= 14.696: return this.defaultsFor14_696()
            pb_res_temp_calc= this.pb_res_temp_atCorrelation(bo_correlations_selected, this.gor_pb, this.temp_res)
            #print pb_res_temp_calc
            pressure_at_temp_rs= this.getBelowBpPressures(this.pb_res_temp)
            temp_at_temp_rs= np.repeat(this.temp_res, len(pressure_at_temp_rs))
            gor_at_correlation1, yg1= this.rs_atCorrelation(bo_correlations_selected, pressure_at_temp_rs, this.temp_res)
            this.multiplier_for_pb_res_temp= this.pb_res_temp/pb_res_temp_calc
            this.multiplier_for_gor_pb= this.multiplier_for_gor_pb_atCorrelation(bo_correlations_selected, pb_res_temp_calc)
            #print this.multiplier_for_gor_pb
            gor_at_correlation= gor_at_correlation1 * this.multiplier_for_gor_pb

        reducedPressure= pressure_at_temp_rs/pressure_at_temp_rs[0]
        final_rs= this.rs_correction(gor_at_correlation, reducedPressure)
        final_rs[-1]= 0.0
        return pressure_at_temp_rs, final_rs, gor_at_correlation1, yg1

    def getFinalRsAt_10To500Temp(this, bo_correlations_selected, pressure_at_temp_rs, final_rs, gor_at_correlation1, yg1):
        data= OrderedDict()
        # if bo_correlations_selected == 'dindoruk':
        #     data['TempDb']= np.arange(60., 510., this.tempRange)
        # else:
        #     data['TempDb']= np.arange(this.tempRange, 510., this.tempRange)
        data['TempDb']= np.copy(this.output_temps)

        data['BPPCalc'] = []
        defaults= this.defaultsFor14_696()
        for differentTemp in data['TempDb']:
            pressure_at_differentTemp, gor_at_correlation, yg = None, None, None
            if this.caseno == 1:
                gorORyg= yg1[0] if bo_correlations_selected == 'lasater' else gor_at_correlation1[0]
                pressure_at_differentTemp= this.pb_res_temp_atCorrelation(bo_correlations_selected, gorORyg, differentTemp)
                if pressure_at_differentTemp <= 14.696: 
                    data["%s" %(differentTemp)]= {'BelowBPPatTemp':defaults[0], "BelowRsAtTemp":defaults[1]}
                    continue
                pressure_at_differentTemp= this.getBelowBpPressures(pressure_at_differentTemp)
                gor_at_correlation, yg= this.rs_atCorrelation(bo_correlations_selected, pressure_at_differentTemp, differentTemp)

            if this.caseno == 2:
                gorORyg= this.yg_lasater()[0] if bo_correlations_selected == 'lasater' else this.gor_pb
                pressure_at_differentTemp= this.pb_res_temp_atCorrelation(bo_correlations_selected, gorORyg, differentTemp)

                if pressure_at_differentTemp <= 14.696: 
                    data["%s" %(differentTemp)]= {'BelowBPPatTemp':defaults[0], "BelowRsAtTemp":defaults[1]}
                    continue
                pressure_at_differentTemp= this.getBelowBpPressures(pressure_at_differentTemp)
                gor_at_correlation, yg= this.rs_atCorrelation(bo_correlations_selected, pressure_at_differentTemp, differentTemp)

            if this.caseno == 3:
                gorORyg= yg1[0] if bo_correlations_selected == 'lasater' else gor_at_correlation1[0]
                pressure_at_differentTemp= this.pb_res_temp_atCorrelation(bo_correlations_selected, gorORyg, differentTemp)
                if pressure_at_differentTemp <= 14.696: 
                    data["%s" %(differentTemp)]= {'BelowBPPatTemp':defaults[0], "BelowRsAtTemp":defaults[1]}
                    continue
                pressure_at_differentTemp= this.getBelowBpPressures(pressure_at_differentTemp)
                gor_at_correlation, yg= this.rs_atCorrelation(bo_correlations_selected, pressure_at_differentTemp, differentTemp)
                gor_at_correlation= gor_at_correlation*this.multiplier_for_gor_pb #override below for lasater
            
            if bo_correlations_selected == 'lasater':
                effective_Oil= (630.-10.*this.api_gravity) if(this.api_gravity<=40.) else (73110.*this.api_gravity**(-1.562))
                if this.caseno == 3:
                    gor_at_correlation= this.gor_pb/(132755.*this.api_specific_gravity*this.gas_sp_gravity/(effective_Oil*(1.-yg[0])))*(132755.*this.api_specific_gravity*this.gas_sp_gravity/(effective_Oil*(1.-yg)))
                if this.caseno == 1:
                    gor_at_correlation= 132755.*this.api_specific_gravity*this.gas_sp_gravity/(effective_Oil*(1.-yg))
            data['BPPCalc'].append(pressure_at_differentTemp[0])
            reducedPressure= pressure_at_differentTemp/pressure_at_differentTemp[0]
            final_rs= this.rs_correction(gor_at_correlation, reducedPressure)
            final_rs[-1]= 0.0
            final_rs[np.where(final_rs<=0)]= 0.0
            data["%s" %(differentTemp)]= {'BelowBPPatTemp':pressure_at_differentTemp.tolist(), "BelowRsAtTemp":final_rs.tolist()}
        return data

    def main(this, bo_correlations_selected):
        pressure_at_temp_rs, final_rs, gor_at_correlation1, yg1= this.getFinalRsAt_temp_res(bo_correlations_selected)
        data= this.getFinalRsAt_10To500Temp(bo_correlations_selected, pressure_at_temp_rs, final_rs, gor_at_correlation1, yg1)
        return data
        # print this.multiplier_for_gor_pb
        # print this.api_gravity
        # print this.gas_sp_gravity

if __name__ == "__main__":
    obj = RsTempPressureBOC()
    logger= getLogger('PBO', 'WellB')
    obj = obj.main(logger, debug=True)
    #python RsTempPressureBOC.py