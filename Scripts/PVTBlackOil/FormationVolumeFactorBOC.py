import io, os, sys, time, math
import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
from collections import OrderedDict
import numpy as np


class FormationVolumeFactorBOC(object):
    def __init__(this, parent):
        this.__dict__.update(parent.__dict__)

    def fvf_below_bp_atCorrelation(this, rsAtTemp, diffTemp, gamma_gs):
        bo_correlations_selected= this.bo_correlations_selected
        fvf_below_bp= None
        if bo_correlations_selected == 'vazquez':
            fvf_below_bp=  (1.+4.677*10.**-4.*rsAtTemp+1.751*10.**-5.*(diffTemp-60.)*(this.api_gravity/gamma_gs)-1.8106*10.**-8.*rsAtTemp*(diffTemp-60.)*(this.api_gravity/gamma_gs)) if(this.api_gravity<=30.) else (1.+4.67*10.**-4.*rsAtTemp+1.1*10.**-5.*(diffTemp-60.)*(this.api_gravity/gamma_gs)+1.337*10.**-9.*rsAtTemp*(diffTemp-60.)*(this.api_gravity/gamma_gs))
        if bo_correlations_selected == 'deghetto':
            fvf_below_bp=  (1.+4.677*10.**-4.*rsAtTemp+1.751*10.**-5.*(diffTemp-60.)*(this.api_gravity/gamma_gs)-1.8106*10.**-8.*rsAtTemp*(diffTemp-60.)*(this.api_gravity/gamma_gs)) if(this.api_gravity<=30.) else (1.+4.67*10.**-4.*rsAtTemp+1.1*10.**-5.*(diffTemp-60.)*(this.api_gravity/gamma_gs)+1.337*10.**-9.*rsAtTemp*(diffTemp-60.)*(this.api_gravity/gamma_gs))
        if bo_correlations_selected == 'standing':
            fvf_below_bp= (0.972+1.47*10.**-4.*(rsAtTemp*(this.gas_sp_gravity/this.api_specific_gravity)**0.5+1.25*diffTemp)**1.175)
        if bo_correlations_selected == 'elam':
            fvf_below_bp= (0.972+1.47*10.**-4.*(rsAtTemp*(this.gas_sp_gravity/this.api_specific_gravity)**0.5+1.25*diffTemp)**1.175)
        if bo_correlations_selected == 'glaso':
            fvf_below_bp= 1.+10.**(-6.58511+(2.91329*np.log10((rsAtTemp*(this.gas_sp_gravity/this.api_specific_gravity)**0.526)+0.986*diffTemp))-0.27683*(np.log10((rsAtTemp*(this.gas_sp_gravity/this.api_specific_gravity)**0.526)+0.986*diffTemp))**2.)
        if bo_correlations_selected in ['almarhoun85', 'almarhoun85rev']:
            fvf_below_bp= 0.574095+7.723532*10.**-4.*(diffTemp+459.67)+2.454005*10.**-3.*((rsAtTemp**0.501538)*(this.gas_sp_gravity**(-0.145526))*(this.api_specific_gravity**(-5.220726)))+3.727676*10.**-5.*((rsAtTemp**0.501538)*(this.gas_sp_gravity**(-0.145526))*(this.api_specific_gravity**(-5.220726)))**2.
        if bo_correlations_selected in ['almarhoun88', 'almarhoun88rev']:
            fvf_below_bp= 0.497069+8.26963*10**-4.*(diffTemp+459.67)+1.82594*10**-3.*((rsAtTemp**0.74239)*(this.gas_sp_gravity**0.323294)*this.api_specific_gravity**(-1.20204))+3.18099*10.**-6.*((rsAtTemp**0.74239)*(this.gas_sp_gravity**0.323294)*this.api_specific_gravity**(-1.20204))**2.
        if bo_correlations_selected == 'petrosky':
            fvf_below_bp= 1.0113+7.2046*10.**-5.*((rsAtTemp**0.3738)*(this.gas_sp_gravity**0.2914)/(this.api_specific_gravity**0.6265)+0.24626*diffTemp**0.5371)**3.0936
        if bo_correlations_selected == 'farshad':
            fvf_below_bp= 1.+10.**(-2.6541+0.5576*np.log10((rsAtTemp**0.5956)*(this.gas_sp_gravity**0.2369)*(this.api_specific_gravity**(-1.3282))+0.0976*diffTemp)+0.3331*(np.log10((rsAtTemp**0.5956)*(this.gas_sp_gravity**0.2369)*(this.api_specific_gravity**(-1.3282))+0.0976*diffTemp))**2.)


        if bo_correlations_selected == 'kartoatmodjo':
            fvf_below_bp= 0.98496+0.0001*(rsAtTemp**0.755*this.gas_sp_gravity**0.25*this.api_specific_gravity**(-1.5)+0.45*diffTemp)**1.5
        if bo_correlations_selected == 'elsharkawi':
            fvf_below_bp= 1.+(40.428*10.**-5.)*rsAtTemp+(63.802*10.**-5.)*(diffTemp-60.)+(0.78*10.**-5.)*(rsAtTemp*(diffTemp-60.)*(this.gas_sp_gravity/this.api_specific_gravity))
        if bo_correlations_selected == 'livitan':
            fvf_below_bp= 1.+0.0005*rsAtTemp*(this.gas_sp_gravity/this.api_specific_gravity)**0.25+(0.2*(diffTemp-60))/(519.67*this.gas_sp_gravity*this.api_specific_gravity)
        if bo_correlations_selected == 'alshammasi':
            fvf_below_bp= 1.+0.0005*rsAtTemp*(this.gas_sp_gravity/this.api_specific_gravity)**0.25+(0.2*(diffTemp-60))/(519.67*this.gas_sp_gravity*this.api_specific_gravity)


        if bo_correlations_selected == 'alshammasirev':
            fvf_below_bp= 0.744596+0.829233*10.**-4.*(rsAtTemp*(this.gas_sp_gravity/this.api_specific_gravity)**0.5+6.7908*diffTemp)**1.175
        if bo_correlations_selected == 'dindoruk':
            fvf_below_bp= np.array([1. if(rsAtTemp[i]<=0) else (9.871766*10.**-1.+7.865146*10.**-4.*((((rsAtTemp[i]**2.510755*this.gas_sp_gravity**(-4.852538))/(this.api_specific_gravity**11.835)+1.365428*10.**5.*(diffTemp-60.)**2.25288+10.0719*rsAtTemp[i])**0.4450849)/((5.352624+2.*rsAtTemp[i]**(-0.6309052)/this.gas_sp_gravity**0.9000749*(diffTemp-60.))**2.))+2.689173*10.**-6.*((((rsAtTemp[i]**2.510755*this.gas_sp_gravity**(-4.852538))/(this.api_specific_gravity**11.835)+1.365428*10.**5.*(diffTemp-60.)**2.25288+10.0719*rsAtTemp[i])**0.4450849)/((5.352624+2.*rsAtTemp[i]**(-0.6309052)/this.gas_sp_gravity**0.9000749*(diffTemp-60.))**2.))**2.+1.100001*10.**-5.*(diffTemp-60.)*(this.api_gravity/this.gas_sp_gravity)) for i in range(len(rsAtTemp))])
        if bo_correlations_selected == 'alnajjar':
            fvf_below_bp= 0.96325+4.9*10.**-4.*(rsAtTemp*(this.gas_sp_gravity/this.api_specific_gravity)**0.5+1.25*diffTemp)
        if bo_correlations_selected == 'mazandarani':
            fvf_below_bp= 0.99117+0.00021*rsAtTemp-2.32*10.**-6.*(rsAtTemp-this.gas_sp_gravity)/this.api_specific_gravity+0.0071*(diffTemp-60.)-(4.3*10.**-7.*(diffTemp-60.)*(1.-this.gas_sp_gravity))
        if bo_correlations_selected == 'mehran':
            fvf_below_bp= 1.+10.**(-4.7486+1.587*(np.log10((rsAtTemp*(this.gas_sp_gravity/this.api_specific_gravity)**0.4211+2.035*diffTemp)))-0.0495*(np.log10((rsAtTemp*(this.gas_sp_gravity/this.api_specific_gravity)**0.4211+2.035*diffTemp)))**2.)



        if bo_correlations_selected == 'lasater':
            fvf_below_bp= (0.972+1.47*10.**-4.*(rsAtTemp*(this.gas_sp_gravity/this.api_specific_gravity)**0.5+1.25*diffTemp)**1.175)

        return fvf_below_bp

    def c0_atCorrelation(this, rsAtTemp, diffTemp, gamma_gs, bppAtTemp, fvfAtTemp):
        bo_correlations_selected= this.bo_correlations_selected
        c0= None
        if bo_correlations_selected == 'vazquez':
            c0= np.abs((-1433.+5.*rsAtTemp+17.2*diffTemp-1180.*gamma_gs+12.61*this.api_gravity)/(10.**5.*bppAtTemp))
        if bo_correlations_selected == 'deghetto':
            c0= np.abs(((-889.6+3.1374*rsAtTemp+20.*diffTemp-627.3*gamma_gs-81.4476*this.api_gravity)/(10.**5.*bppAtTemp)) if(this.api_gravity<=10.) else ((-2841.8+2.9646*rsAtTemp+25.5439*diffTemp-1230.5*gamma_gs+41.91*this.api_gravity)/(10.**5.*bppAtTemp)) if(this.api_gravity<=22.3) else ((-1433.+5.*rsAtTemp+17.2*diffTemp-1180.*gamma_gs+12.61*this.api_gravity)/(10.**5.*bppAtTemp)))
        if bo_correlations_selected == 'standing':
            c0= np.abs((-1433.+5.*rsAtTemp+17.2*diffTemp-1180.*gamma_gs+12.61*this.api_gravity)/(10.**5.*bppAtTemp))
        if bo_correlations_selected == 'elam':
            c0= np.abs((-1433.+5.*rsAtTemp+17.2*diffTemp-1180.*gamma_gs+12.61*this.api_gravity)/(10.**5.*bppAtTemp))
        if bo_correlations_selected == 'glaso':
            c0= np.abs((-1433.+5.*rsAtTemp+17.2*diffTemp-1180.*gamma_gs+12.61*this.api_gravity)/(10.**5.*bppAtTemp))
        if bo_correlations_selected == 'almarhoun85':
            c0= np.abs(np.exp(-14.1042+2.7314/((this.api_specific_gravity+2.18*10.**-4.*rsAtTemp[0]*this.gas_sp_gravity)/fvfAtTemp)+(-56.0605*10.**-6.)*(bppAtTemp-bppAtTemp[0])/((this.api_specific_gravity+2.18*10.**-4.*rsAtTemp[0]*this.gas_sp_gravity)/fvfAtTemp)**3.+(-580.8778)/(diffTemp+460.)))
        if bo_correlations_selected == 'almarhoun85rev':
            c0= np.array([0.0 if(bppAtTemp[0]-bppAtTemp[i])==0 else (((-1.3668*10.**-5*rsAtTemp[i]-1.95682*10.**-8.*rsAtTemp[i]**2.+2.408026*10.**-2.*this.gas_sp_gravity-9.26019*10.**-8.*(diffTemp+459.67)**2.)*math.log(bppAtTemp[i]/bppAtTemp[0]))/(bppAtTemp[0]-bppAtTemp[i])) for i in range(len(bppAtTemp))])
        if bo_correlations_selected == 'almarhoun88':
            c0= np.abs(np.exp(-14.1042+2.7314/((this.api_specific_gravity+2.18*10.**-4.*rsAtTemp[0]*this.gas_sp_gravity)/fvfAtTemp)+(-56.0605*10.**-6.)*(bppAtTemp-bppAtTemp[0])/((this.api_specific_gravity+2.18*10.**-4.*rsAtTemp[0]*this.gas_sp_gravity)/fvfAtTemp)**3.+(-580.8778)/(diffTemp+460.)))
        if bo_correlations_selected == 'almarhoun88rev':
            c0= np.array([0.0 if(bppAtTemp[0]-bppAtTemp[i])==0 else (((-1.3668*10.**-5*rsAtTemp[i]-1.95682*10.**-8.*rsAtTemp[i]**2.+2.408026*10.**-2.*this.gas_sp_gravity-9.26019*10.**-8.*(diffTemp+459.67)**2.)*math.log(bppAtTemp[i]/bppAtTemp[0]))/(bppAtTemp[0]-bppAtTemp[i])) for i in range(len(bppAtTemp))])
        if bo_correlations_selected == 'petrosky':
            c0= np.abs(1.705*10.**-7.*rsAtTemp**0.69357*this.gas_sp_gravity**0.1885*this.api_gravity**0.3272)
        if bo_correlations_selected == 'farshad':
            c0= np.abs(10.**(-5.4531+5.03*10.**-4.*((rsAtTemp**0.1982)*(diffTemp**0.6685)*(this.gas_sp_gravity**(-0.21435))*(this.api_gravity**1.0116)*bppAtTemp**(-0.1616))-3.5*10.**-8.*((rsAtTemp**0.1982)*(diffTemp**0.6685)*(this.gas_sp_gravity**(-0.21435))*(this.api_gravity**1.0116)*bppAtTemp**(-0.1616))**2.))


        if bo_correlations_selected == 'kartoatmodjo':
            gamma_gsc= this.gas_sp_gravity*(1.+0.1595*(this.api_gravity**0.4078)/(this.temp_sep**0.2466)*math.log10(this.p_sep/114.7))
            c0= np.abs((10.**(0.83415+0.5002*np.log10(rsAtTemp)+0.3613*math.log10(this.api_gravity)+0.7606*math.log10(diffTemp)-0.35505*math.log10(gamma_gsc)))/(bppAtTemp*10.**6.))
        if bo_correlations_selected == 'elsharkawi':
            c0= np.abs((-27321.+33.784*rsAtTemp+238.81*diffTemp)/(bppAtTemp*10.**6.))
        if bo_correlations_selected == 'livitan':
            c0= np.abs(10.**(-5.4531+5.03*10.**-4.*(rsAtTemp**0.1982*diffTemp**0.6685*this.gas_sp_gravity**(-0.21435)*this.api_gravity**1.0116*bppAtTemp**(-0.1616))-3.5*10.**-8.*(rsAtTemp**0.1982*diffTemp**0.6685*this.gas_sp_gravity**(-0.21435)*this.api_gravity**1.0116*bppAtTemp**(-0.1616))**2.))
        if bo_correlations_selected == 'alshammasi':
            c0= np.abs(10.**(-5.4531+5.03*10.**-4.*(rsAtTemp**0.1982*diffTemp**0.6685*this.gas_sp_gravity**(-0.21435)*this.api_gravity**1.0116*bppAtTemp**(-0.1616))-3.5*10.**-8.*(rsAtTemp**0.1982*diffTemp**0.6685*this.gas_sp_gravity**(-0.21435)*this.api_gravity**1.0116*bppAtTemp**(-0.1616))**2.))


        if bo_correlations_selected == 'alshammasirev':
            c0= np.abs(10.**(-5.4531+5.03*10.**-4.*(rsAtTemp**0.1982*diffTemp**0.6685*this.gas_sp_gravity**(-0.21435)*this.api_gravity**1.0116*bppAtTemp**(-0.1616))-3.5*10.**-8.*(rsAtTemp**0.1982*diffTemp**0.6685*this.gas_sp_gravity**(-0.21435)*this.api_gravity**1.0116*bppAtTemp**(-0.1616))**2.))
        if bo_correlations_selected == 'dindoruk':
            c0= np.abs((4.487462368+0.00519704*(((((rsAtTemp**0.980922372*this.gas_sp_gravity**(0.021003077))/(this.api_specific_gravity**0.338486128)+20.00006358*(diffTemp-60.)**0.300001059-8.76813622*10.**-1.*rsAtTemp)**1.759732076)/((2.749114986+2.*rsAtTemp**(-1.71357145)/this.gas_sp_gravity**9.999932841*(diffTemp-60.))**2.)))+0.00001258*(((((rsAtTemp**0.980922372*this.gas_sp_gravity**(0.021003077))/(this.api_specific_gravity**0.338486128)+20.00006358*(diffTemp-60.)**0.300001059-8.76813622*10.**-1.*rsAtTemp)**1.759732076)/((2.749114986+2.*rsAtTemp**(-1.71357145)/this.gas_sp_gravity**9.999932841*(diffTemp-60.))**2.)))**2.)*10.**-6.)
        if bo_correlations_selected == 'alnajjar':
            c0= np.abs(10.**(-5.4531+5.03*10.**-4.*((rsAtTemp**0.1982)*(diffTemp**0.6685)*(this.gas_sp_gravity**(-0.21435))*(this.api_gravity**1.0116)*bppAtTemp**(-0.1616))-3.5*10.**-8.*((rsAtTemp**0.1982)*(diffTemp**0.6685)*(this.gas_sp_gravity**(-0.21435))*(this.api_gravity**1.0116)*bppAtTemp**(-0.1616))**2.))
        if bo_correlations_selected == 'mazandarani':
            c0= np.abs(10.**(-5.4531+5.03*10.**-4.*((rsAtTemp**0.1982)*(diffTemp**0.6685)*(this.gas_sp_gravity**(-0.21435))*(this.api_gravity**1.0116)*bppAtTemp**(-0.1616))-3.5*10.**-8.*((rsAtTemp**0.1982)*(diffTemp**0.6685)*(this.gas_sp_gravity**(-0.21435))*(this.api_gravity**1.0116)*bppAtTemp**(-0.1616))**2.))
        if bo_correlations_selected == 'mehran':
            c0= np.abs(10.**(-5.4531+5.03*10.**-4.*((rsAtTemp**0.1982)*(diffTemp**0.6685)*(this.gas_sp_gravity**(-0.21435))*(this.api_gravity**1.0116)*bppAtTemp**(-0.1616))-3.5*10.**-8.*((rsAtTemp**0.1982)*(diffTemp**0.6685)*(this.gas_sp_gravity**(-0.21435))*(this.api_gravity**1.0116)*bppAtTemp**(-0.1616))**2.))


        if bo_correlations_selected == 'lasater':
            c0= np.abs((-1433.+5.*rsAtTemp+17.2*diffTemp-1180.*gamma_gs+12.61*this.api_gravity)/(10.**5.*bppAtTemp))
        return c0


    def fvf_below_bp(this):
        gamma_gs= this.gas_sp_gravity*(1.+5.912*10.**-5.*this.api_gravity*this.temp_sep*math.log10(this.p_sep/114.7))
        for i in range(len(this.data['TempDb'])):
            diffTemp= this.data['TempDb'][i]
            rsAtTemp = np.array(this.data['%s' %(diffTemp)]['BelowRsAtTemp'])
            fvf_below_bp= this.fvf_below_bp_atCorrelation(rsAtTemp, diffTemp, gamma_gs)
            fvf_below_bp[-1]= 1.
            this.data['%s' %(this.data['TempDb'][i])].update({'BelowFVFAtTemp': fvf_below_bp})

    def alltemp_above_bp(this):
        gamma_gs= this.gas_sp_gravity*(1.+5.912*10.**-5.*this.api_gravity*this.temp_sep*math.log10(this.p_sep/114.7))
        all_temp_above_bp= OrderedDict()        
        for i in range(len(this.data['TempDb'])):
            #i = 28 #REMOVE
            diffTemp= this.data['TempDb'][i]

            rsAtTemp = this.data['%s' %(this.data['TempDb'][i])]['BelowRsAtTemp'][0]
            bppAtTemp= this.data['%s' %(this.data['TempDb'][i])]['BelowBPPatTemp'][0]
            fvfAtTemp= this.data['%s' %(this.data['TempDb'][i])]['BelowFVFAtTemp'][0]
            bppAtTemp0= bppAtTemp

            interval= ((bppAtTemp*8.)-bppAtTemp)/25.
            maxP= bppAtTemp*8.
            bppAtTemp= np.arange(bppAtTemp, maxP, interval) # PRESSURE RANGE FOR ABOVE BUBBLE POINT
            rsAtTemp= np.repeat(rsAtTemp, len(bppAtTemp))
            c0= this.c0_atCorrelation(rsAtTemp, diffTemp, gamma_gs, bppAtTemp, fvfAtTemp)  #IsothermalOilCompressibility
            fvf_above_bp=[fvfAtTemp]
            for j in range(1, len(bppAtTemp)): fvf_above_bp.append(fvfAtTemp*math.exp((c0[j])*(bppAtTemp0-bppAtTemp[j])))
            all_temp_above_bp['%s' %(this.data['TempDb'][i])]= {'AboveFVFAtTemp': fvf_above_bp, 'AboveBPPAtTemp': bppAtTemp, 'AboveRsAtTemp': rsAtTemp}
        return all_temp_above_bp

    def alltemp_above_below_bp(this, all_temp_above_bp):
        alltemp_pres_rs_fvf=OrderedDict()
        c = 1
        for i in this.data['TempDb']:
            #i= 150.0  #REMOVE
            str_i= '%s' %(i)
            alltemp_pres_rs_fvf[str_i.replace('.', '_')]= {
                    'output_temps_seq_no': c,
                    'Pressure': np.append(all_temp_above_bp[str_i]['AboveBPPAtTemp'][::-1], this.data[str_i]['BelowBPPatTemp'][1:]).tolist(),
                    'Rs'      : np.append(all_temp_above_bp[str_i]['AboveRsAtTemp'][::-1],  this.data[str_i]['BelowRsAtTemp'][1:]).tolist(),
                    'FVF'     : np.append(all_temp_above_bp[str_i]['AboveFVFAtTemp'][::-1], this.data[str_i]['BelowFVFAtTemp'][1:]).tolist()
            }
            c+= 1
        return alltemp_pres_rs_fvf


    def main(this, data, bo_correlations_selected):
        this.data= data 
        this.bo_correlations_selected= bo_correlations_selected
        this.fvf_below_bp()
        all_temp_above_bp= this.alltemp_above_bp()
        alltemp_pres_rs_fvf= this.alltemp_above_below_bp(all_temp_above_bp)
        return alltemp_pres_rs_fvf

        # # this.logger.info("Done: after_solver_Below_Bubble_Rev1_2")
        # this.logger.info("In progress: above_Bubble_case23")
        # all_temp_above_bp= this.above_Bubble_case23(FVF_all_temp_below_bp)
        # this.logger.info("Done: above_Bubble_case23")
        # alltemp_pres_rs_fvf= this.alltemp_above_below_bp(FVF_all_temp_below_bp, all_temp_above_bp)
        # this.logger.info("Done: alltemp_above_below_bp")
        # return alltemp_pres_rs_fvf
