import os, math
import matplotlib.pyplot as plt
from collections import OrderedDict
from Scripts.pvt.core.optimizers.ncg import *
from Scripts.pvt.core.optimizers.poas_interpolator import *
from Scripts.pvt.core.optimizers.poas_interpolator import monotone


class FormationVolumeFactor(object):
    def __init__(this, parent):
        this.__dict__.update(parent.__dict__)

    def define_region(this, vars, bound, mk_vars):
        lb, ub = list(), list()
        for i in range(len(vars)):
            lb.append(max(bound[i][0], (vars[i] - 4.*vars[i])))
            ub.append(min(bound[i][1], (vars[i] + 4.*vars[i])))
        a_max_l = list()
        for i in range(len(vars)):
            a_max_l.append( 0 if mk_vars[i] == 0 else ( ((vars[i] - ub[i]) / mk_vars[i]) if mk_vars[i] < 0 else ((vars[i] - lb[i]) / mk_vars[i])))
        alpha_max= None
        if 1.5*min(a_max_l) < 10.**-5:
            alpha_max = 10.**-5
        elif 1.5*min(a_max_l) > 50.:
            alpha_max = 50.
        else:
            alpha_max = 1.5*min(a_max_l)

        alpha_min= None
        if((alpha_max*0.0000001)<10.**-13):
            alpha_min= 10.**-13,
        elif((alpha_max*0.0000001)>10.**-8):
            alpha_min= 10**-8
        else:
            alpha_min= alpha_max*0.0000001
        return alpha_max, alpha_min

    def converge_f(this, debug, logger, curr_iteration, grad):
        combined_grad= math.sqrt(grad[0]**2)
        #print "curr_iteration:", curr_iteration, "combined_grad:", combined_grad, "Done."
        if combined_grad <= 10**-3:
            if debug: this.logger.info("Rs converged by rule 1 combined gradient- %s" %(combined_grad))
            return True
        return False

    def objective(this, _svar, _iniVar, _pressureAtRef, _rsAtRef, gas_sp_gravity, specificGravityofCrudeOil):
        #objective
        _svar  = _svar[0] #only one var
        rhopo  = _iniVar * _svar
        rhoa   = -49.893+85.0149*gas_sp_gravity-3.70373*gas_sp_gravity*rhopo+0.047981*gas_sp_gravity*rhopo**2+2.98914*rhopo-0.035688*rhopo**2
        f_calc = (_rsAtRef*gas_sp_gravity+4600.*specificGravityofCrudeOil)/(73.71+_rsAtRef*gas_sp_gravity/rhoa)
        obj    = (rhopo - f_calc)**2
        return obj, f_calc

    def _cost(this, svars, kwargs, print_r=False):
        _svar, _iniVar, _pressureAtRef, _rsAtRef, gas_sp_gravity, specificGravityofCrudeOil = svars[0], kwargs[0][0], kwargs[1], kwargs[2], kwargs[3], kwargs[4]
        #objective
        rhopo  = _iniVar * _svar
        rhoa   = -49.893+85.0149*gas_sp_gravity-3.70373*gas_sp_gravity*rhopo+0.047981*gas_sp_gravity*rhopo**2+2.98914*rhopo-0.035688*rhopo**2
        f_calc = (_rsAtRef*gas_sp_gravity+4600.*specificGravityofCrudeOil)/(73.71+_rsAtRef*gas_sp_gravity/rhoa)
        obj    = (rhopo - f_calc)**2
        #objective
        diff_Rhoa_var1= -3.70373*gas_sp_gravity*_iniVar+0.047981*gas_sp_gravity*2*rhopo*_iniVar+2.98914*_iniVar-0.035688*2.*rhopo*_iniVar
        diff_F_var1= ((_rsAtRef*gas_sp_gravity+4600.*specificGravityofCrudeOil)/(73.71+_rsAtRef*gas_sp_gravity/rhoa)**2)*(_rsAtRef*gas_sp_gravity/rhoa**2)*diff_Rhoa_var1
        diff_obj_var1= 2.*(rhopo - f_calc)*(_iniVar-diff_F_var1)
        diff_total_obj_var1= None
        if(diff_obj_var1>=10.**100): diff_total_obj_var1=10.**10
        elif(diff_obj_var1>=10.**90): diff_total_obj_var1=(diff_obj_var1*10.**-85)
        elif(diff_obj_var1>=10.**85): diff_total_obj_var1=(diff_obj_var1*10.**-80)
        elif(diff_obj_var1>=10.**80): diff_total_obj_var1=(diff_obj_var1*10.**-75)
        elif(diff_obj_var1>=10.**75): diff_total_obj_var1=(diff_obj_var1*10.**-70)
        elif(diff_obj_var1>=10.**70): diff_total_obj_var1=(diff_obj_var1*10.**-65)
        elif(diff_obj_var1>=10.**65): diff_total_obj_var1=(diff_obj_var1*10.**-60)
        elif(diff_obj_var1>=10.**60): diff_total_obj_var1=(diff_obj_var1*10.**-55)
        elif(diff_obj_var1>=10.**55): diff_total_obj_var1=(diff_obj_var1*10.**-50)
        elif(diff_obj_var1>=10.**50): diff_total_obj_var1=(diff_obj_var1*10.**-45)
        elif(diff_obj_var1>=10.**45): diff_total_obj_var1=(diff_obj_var1*10.**-40)
        elif(diff_obj_var1>=10.**40): diff_total_obj_var1=(diff_obj_var1*10.**-35)
        elif(diff_obj_var1>=10.**35): diff_total_obj_var1=(diff_obj_var1*10.**-30)
        elif(diff_obj_var1>=10.**30): diff_total_obj_var1=(diff_obj_var1*10.**-25)
        elif(diff_obj_var1>=10.**25): diff_total_obj_var1=(diff_obj_var1*10.**-20)
        elif(diff_obj_var1>=10.**20): diff_total_obj_var1=(diff_obj_var1*10.**-12)
        elif(diff_obj_var1>=10.**15): diff_total_obj_var1=(diff_obj_var1*10.**-5)
        elif(diff_obj_var1>=10.**10): diff_total_obj_var1=(diff_obj_var1*10.**-4)
        else: diff_total_obj_var1=diff_obj_var1
        return obj, np.array([diff_total_obj_var1])

    def reftemp_solver_below_bp(this):
        svars    = np.ones(1) #1 variable.
        bounds   = np.array([[0.1, 5.]]) 
        iniVar   = np.array([50.]) #po initial
        pressure= np.array(this.input_belowbp_pres_interp)
        referenceTemp= np.repeat(this.temp_res, len(pressure))
        rs, var_calc= [], []
        for i in range(len(pressure)):
            rs.append(0. if this.input_belowbp_rs_interp[i] == 10.**-6 else  this.input_belowbp_rs_interp[i])
            var_calc.append(ncg().minimize(svars, bounds, this._cost, this.logger, max_iter=300, debug=this.debug, region_f=this.define_region, converge_f=this.converge_f, args=(iniVar, pressure[i], rs[i], this.gas_sp_gravity, this.SpecificGravityOfCrudeOil))[0])
        rhopo= np.array(var_calc) * np.repeat(iniVar[0], len(var_calc))
        rs= np.array(rs)
        pressureCorrection= (0.167+16.181*(10.**(-0.0425*rhopo)))*(pressure/1000.)-0.01*(0.299+263.*(10.**(-0.0603*rhopo)))*(pressure/1000.)**2
        tempCorrection= (0.00302+1.505*(rhopo+pressureCorrection)**-0.951)*(referenceTemp-60.)**0.938-(0.0216-0.0233*10.**(-0.0161*(rhopo+pressureCorrection)))*(referenceTemp-60.)**0.475
        oilDensity= rhopo + pressureCorrection - tempCorrection
        fvf_calc =(62.42797*this.SpecificGravityOfCrudeOil+0.01357*rs*this.gas_sp_gravity)/oilDensity
        multiplier= np.array(this.input_belowbp_fvf_interp)/fvf_calc
        final_fvf= fvf_calc*multiplier
        return rs, multiplier

    def plot2D(this, temp, bppDb, bppCalculated):
        if this.debug: 
            plt.style.use('dark_background')
            fig, ax = plt.subplots()
            plt.scatter(temp, bppDb, label='DB')        
            plt.scatter(temp, bppCalculated, label='Calc')        
            plt.xlabel('temp')
            plt.ylabel('pressure')
            plt.title('SolutionGOR Bubble Point')
            plt.legend()
            plt.tight_layout(pad=1.7)
            #plt.show()
            graphPath= os.path.join(getPOASRootDir(), 'Graphs', this.dbName, this.options.wellno, 'SolutionGOR_BubblePoint.png')
            if not os.path.exists(os.path.dirname(graphPath)):
                os.makedirs(os.path.dirname(graphPath))        
            fig.savefig(graphPath, dpi=fig.dpi)
            plt.cla()
            plt.close(fig)
            print "Graphs:%s" %(graphPath)

    def input_solver1and3(this, pres_inter, rs_inter, fvf_inter, gas_sp_gravity, specificGravityofCrudeOil, temp_res, input_abovebp_pres, input_abovebp_rs, input_abovebp_fvf, data):        
        this.input_belowbp_pres_interp= pres_inter
        this.input_belowbp_rs_interp= rs_inter
        this.input_belowbp_fvf_interp= fvf_inter
        this.gas_sp_gravity= gas_sp_gravity
        this.SpecificGravityOfCrudeOil= specificGravityofCrudeOil
        this.temp_res= temp_res
        this.input_abovebp_pres= input_abovebp_pres
        this.input_abovebp_rs  = input_abovebp_rs
        this.input_abovebp_fvf = input_abovebp_fvf
        this.data= data #solver 3

    def solve1(this, pr_inter, rs_inter, fvf_inter, data):
        this.input_solver1and3(pr_inter, rs_inter, fvf_inter, this.gas_sp_gravity, this.SpecificGravityOfCrudeOil, this.temp_res, this.input_abovebp_pres, this.input_abovebp_rs, this.input_abovebp_fvf, data)
        this.logger.info("In progress: reftemp_solver_below_bp")
        rs, multiplier= this.reftemp_solver_below_bp()
        this.logger.info("Done: reftemp_solver_below_bp")
        this.logger.info("In progress: alltemp_solver_below_bp")
        FVF_all_temp_below_bp= this.alltemp_solver_below_bp(rs, multiplier)
        this.logger.info("Done: alltemp_solver_below_bp")
        this.logger.info("In progress: reftemp_above_bp")
        s, c_obs= this.reftemp_above_bp()
        this.logger.info("Done: reftemp_above_bp")
        this.logger.info("In progress: alltemp_above_bp")
        all_temp_above_bp= this.alltemp_above_bp(FVF_all_temp_below_bp, s, c_obs)
        this.logger.info("Done: alltemp_above_bp")
        this.logger.info("In progress: alltemp_above_below_bp")
        alltemp_pres_rs_fvf= this.alltemp_above_below_bp(FVF_all_temp_below_bp, all_temp_above_bp)
        this.logger.info("Done: alltemp_above_below_bp")
        return rs, multiplier, s, c_obs, alltemp_pres_rs_fvf
    
    def alltemp_above_below_bp(this, FVF_all_temp_below_bp, all_temp_above_bp):
        alltemp_pres_rs_fvf=OrderedDict()
        c= 1
        for i in this.data['TempDb']:
            #i= 150.0  #REMOVE
            str_i= '%s' %(i)
            alltemp_pres_rs_fvf[str_i.replace('.', '_')]= {
                    'output_temps_seq_no': c,
                    'Pressure': np.append(all_temp_above_bp[str_i]['AboveBPPAtTemp'], this.data[str_i]['BelowBPPatTemp'][1:]).tolist(),
                    'Rs'      : np.append(all_temp_above_bp[str_i]['AboveRsAtTemp'],  this.data[str_i]['BelowRsAtTemp'][1:]).tolist(),
                    'FVF'     : np.append(all_temp_above_bp[str_i]['AboveFVFAtTemp'],  FVF_all_temp_below_bp[str_i]['BelowFVFAtTemp'][1:]).tolist()
            }
            c +=1
        return alltemp_pres_rs_fvf

    def alltemp_above_bp(this, FVF_all_temp_below_bp, s, c_obs):
        all_temp_above_bp= OrderedDict()
        for i in range(len(this.data['TempDb'])):
            this.logger.info("alltemp_above_bp poas_interpolator in progress for %s" %(i))
            #i = 28 #REMOVE
            bppAtTemp= this.data['%s' %(this.data['TempDb'][i])]['BelowBPPatTemp'][0]
            rsAtTemp = this.data['%s' %(this.data['TempDb'][i])]['BelowRsAtTemp'][0]
            fvfAtTemp= FVF_all_temp_below_bp['%s' %(this.data['TempDb'][i])]['BelowFVFAtTemp'][0]

            interval= int((bppAtTemp*8.)/25.)
            maxP= this.input_abovebp_pres[0] if bppAtTemp*8. < this.input_abovebp_pres[0] else bppAtTemp*8.
            p        = np.arange(bppAtTemp, maxP, interval)[::-1]  # PRESSURE RANGE FOR ABOVE BUBBLE POINT

            #p        = np.arange(bppAtTemp, this.input_abovebp_pres[0]+250., 250.)[::-1]
            x        = (bppAtTemp-p)/bppAtTemp
            c_cal    = np.array(monotone(s.tolist(), c_obs.tolist(), x.tolist()))
            final_fvf= fvfAtTemp*np.exp(x * c_cal)
            locate   = np.where(abs(s[0])-np.abs(x) > 0.)
            if len(locate[0]) > 0 and len(p[locate][0:8]) > 1:
                x1= p[locate][0:8]
                fvf1= final_fvf[locate][0:8]
                lnx1= np.log(x1)
                lnfvf1= np.log(fvf1)
                mata= np.array([np.ones(len(lnx1)), lnx1]).T
                matA= mata.T.dot(mata)
                matB= np.linalg.inv(matA).dot(mata.T.dot(lnfvf1)) 
                fvfCalculatedFromPower= math.exp(matB[0]+matB[1]*math.log(x1[0]))
                ratio= fvf1[0]/fvfCalculatedFromPower
                pressureCurve= p[0:locate[0][0]+1]
                FVFcalculationPowerCurve= (np.exp(matB[0]+matB[1]*np.log(pressureCurve)))*ratio
                final_fvf= np.append(FVFcalculationPowerCurve, final_fvf[locate[0][0]+1:])
            all_temp_above_bp['%s' %(this.data['TempDb'][i])]= {'AboveFVFAtTemp': final_fvf, 'AboveBPPAtTemp': p, 'AboveRsAtTemp': np.repeat(rsAtTemp, len(p))}
        return all_temp_above_bp

    def reftemp_above_bp(this):
        input_abovebp_pres= np.array(this.input_abovebp_pres)
        s= (input_abovebp_pres[-1]-input_abovebp_pres)/input_abovebp_pres[-1]
        c_obs= np.array([1. if(s[i] == 0) else (math.log(this.input_abovebp_fvf[i]/this.input_abovebp_fvf[-1])*(1./s[i])) for i in range(len(s))])
        return s, c_obs

    def alltemp_solver_below_bp(this, rs, multiplier):
        FVF_all_temp_below_bp=OrderedDict()
        for i in range(len(this.data['TempDb'])):
            this.logger.info("In progress: alltemp_solver_below_bp poas_interpolator::%s" %(this.data['TempDb'][i]))
            # if this.data['TempDb'][i] == '325.0':
                # print "here"
            #i = 28 #REMOVE
            bppAtTemp= np.array(this.data['%s' %(this.data['TempDb'][i])]['BelowBPPatTemp'])
            rsAtTemp = np.array(this.data['%s' %(this.data['TempDb'][i])]['BelowRsAtTemp'])
            rsAtTemp1, var_calc= [], []
            iniVar   = np.array([50.]) #po initial
            for j in range(len(bppAtTemp)):
                svars    = np.ones(1) #1 variable.
                bounds   = np.array([[0.1, 5.]]) 
                rsAtTemp1.append(0. if rsAtTemp[j] == 10.**-6 else rsAtTemp[j])
                var_calc.append(ncg().minimize(svars, bounds, this._cost, this.logger, max_iter=300, debug=this.debug, region_f=this.define_region, converge_f=this.converge_f, args=(iniVar, bppAtTemp[j], rsAtTemp1[j], this.gas_sp_gravity, this.SpecificGravityOfCrudeOil))[0])
            temp= np.repeat(this.data['TempDb'][i], len(bppAtTemp))
            rsAtTemp1= np.array(rsAtTemp)
            rhopo= np.array(var_calc) * np.repeat(iniVar[0], len(var_calc))
            pressureCorrection= (0.167+16.181*(10.**(-0.0425*rhopo)))*(bppAtTemp/1000.)-0.01*(0.299+263.*(10.**(-0.0603*rhopo)))*(bppAtTemp/1000.)**2
            tempCorrection= ((0.00302+1.505*(rhopo+pressureCorrection)**-0.951)*(temp-60.)**0.938-(0.0216-0.0233*10.**(-0.0161*(rhopo+pressureCorrection)))*(temp-60.)**0.475) if(this.data['TempDb'][i]>=70) else ((0.00302+1.505*(rhopo+pressureCorrection)**-0.951)*(0.0520861711468201*np.exp(0.0707079316752941*temp))-(0.0216-0.0233*10.**(-0.0161*(rhopo+pressureCorrection)))*(0.000729768281324872*temp**2 - 0.0155294253637099*temp + 0.579360887241886))
            oilDensity= rhopo + pressureCorrection - tempCorrection
            fvf_calc =(62.42797*this.SpecificGravityOfCrudeOil+0.01357*rsAtTemp1*this.gas_sp_gravity)/oilDensity
            multiplier1= np.array(monotone(rs.tolist(), multiplier.tolist(), rsAtTemp1.tolist()))
            final_fvf= fvf_calc*multiplier1
            final_fvf[np.where(final_fvf < 1.)]= 1.
            final_fvf[-1]= 1. #last bpp is 14.696, rs is 0.0, fvf is 1.0
            FVF_all_temp_below_bp['%s' %(this.data['TempDb'][i])]= {'BelowFVFAtTemp': final_fvf}
            this.logger.info("Done: alltemp_solver_below_bp::%s" %(this.data['TempDb'][i]))
            #break #REMOVE
        return FVF_all_temp_below_bp

        #Case 2 and 3:-------------------------------------------------------------------------:Case 2 and 3

    def solve23(this, data):
        this.data= data #solver 3
        this.logger.info("In progress: after_solver_Below_Bubble_Rev1_2")
        FVF_all_temp_below_bp= None
        this.multiplier_fvf_pb= None
        this.calc_multiplier_fvf_pb()

        if this.poasml_revision_selected == 'rev1':
            FVF_all_temp_below_bp, multiplier_fvf_pb= this.after_solver_Below_Bubble_Rev1(this.data)
        else:
            FVF_all_temp_below_bp, multiplier_fvf_pb= this.after_solver_Below_Bubble_Rev2(this.data)
        this.logger.info("Done: after_solver_Below_Bubble_Rev1_2")
        this.logger.info("In progress: above_Bubble_case23")
        all_temp_above_bp= this.above_Bubble_case23(FVF_all_temp_below_bp)
        this.logger.info("Done: above_Bubble_case23")
        alltemp_pres_rs_fvf= this.alltemp_above_below_bp(FVF_all_temp_below_bp, all_temp_above_bp)
        this.logger.info("Done: alltemp_above_below_bp")
        return alltemp_pres_rs_fvf

    def calc_multiplier_fvf_pb(this): #calculate at temp_res
        data= dict()
        data['TempDb']= [this.temp_res]
        data['%s' %(this.temp_res)]=dict()
        data['%s' %(this.temp_res)]['BelowBPPatTemp']= [this.pb_res_temp]
        data['%s' %(this.temp_res)]['BelowRsAtTemp']= [this.gor_pb]
        if this.poasml_revision_selected == 'rev1':
            FVF_all_temp_below_bp, this.multiplier_fvf_pb= this.after_solver_Below_Bubble_Rev1(data)
        else:
            FVF_all_temp_below_bp, this.multiplier_fvf_pb= this.after_solver_Below_Bubble_Rev2(data)
        this.logger.info("MultiplierUsed:%s,%s,%s,%s,%s,%s,%s,%s,%s" %(this.options.wellno, this.temp_res, this.pb_res_temp,this.gor_pb,this.fvf_pb, this.api_gravity, this.gas_sp_gravity, this.poasml_revision_selected, this.multiplier_fvf_pb))

    def getCoeff(this, X, Y):
        mata= np.array([np.ones(len(X)), X, X**2]).T
        matA= mata.T.dot(mata)
        matB=None
        try:
            matB= np.linalg.inv(matA).dot(mata.T.dot(Y)) 
        except:
            matB= np.linalg.pinv(matA).dot(mata.T.dot(Y)) 
        return matB

    def above_Bubble_case23(this, FVF_all_temp_below_bp):
        all_temp_above_bp= OrderedDict()
        for i in range(len(this.data['TempDb'])):
            this.logger.info("above_Bubble_case23 in progress for %s" %(this.data['TempDb'][i]))
            # if (this.data['TempDb'][i]) == 240.0:
                # print "here"
            bppAtTemp= this.data['%s' %(this.data['TempDb'][i])]['BelowBPPatTemp'][0]
            rsAtTemp = this.data['%s' %(this.data['TempDb'][i])]['BelowRsAtTemp'][0]

            interval= int((bppAtTemp*8.)/25.)
            maxP= bppAtTemp*8.

            #bppAtTemp= np.arange(bppAtTemp, bppAtTemp*5, 250.)[::-1]
            bppAtTemp= np.arange(bppAtTemp, maxP, interval)[::-1] # PRESSURE RANGE FOR ABOVE BUBBLE POINT
            rsAtTemp= np.repeat(rsAtTemp, len(bppAtTemp))
            dens_apparent= 0.61703*10.**(-0.00326*this.api_gravity)+(1.51775-0.54351*math.log10(this.api_gravity))*math.log10(this.gas_sp_gravity)
            conversion= 0.028317/0.159  #hardcoded?
            rsAtTemp1= np.array([0. if rsAtTemp[j] == 10.**-6 else rsAtTemp[j] for j in range(len(rsAtTemp))])
            if this.poasml_revision_selected == 'rev1':
                rhopo= (FVF_all_temp_below_bp['%s' %(this.data['TempDb'][i])]['BelowRHOPOAtTemp'][0])*0.0160185
                rhopo= np.repeat(rhopo, len(bppAtTemp))
            else:
                rhopo= (this.SpecificGravityOfCrudeOil+0.001223*rsAtTemp1*conversion*this.gas_sp_gravity)/(1.+0.001223*rsAtTemp1*conversion*this.gas_sp_gravity/dens_apparent)
            temp= np.repeat(this.data['TempDb'][i], len(bppAtTemp))
            first_fvf_from_below_bp= FVF_all_temp_below_bp['%s' %(this.data['TempDb'][i])]['BelowFVFAtTemp'][0]
            final_fvf, multiplier_fvf_pb= this.processRhopo(rhopo, bppAtTemp, temp, rsAtTemp1, first_fvf_from_below_bp)
            all_temp_above_bp['%s' %(this.data['TempDb'][i])]= {'AboveFVFAtTemp': final_fvf, 'AboveBPPAtTemp': bppAtTemp, 'AboveRsAtTemp': rsAtTemp1}
        return all_temp_above_bp

    def find_nearest(this, array, value):
        array = np.asarray(array)
        idx = (np.abs(array - value)).argmin()
        return idx, array[idx]

    def getRev2Multiplier(this):
        return ((5.542348271E-11*this.gor_pb**3 - 7.001397606235E-08*this.gor_pb**2 + 0.0000397063821717806*this.gor_pb + 1.00343855999692) if(this.gor_pb<1300) else (0.517620956734778*this.gor_pb**0.0994488664487963)) if(this.api_gravity<=30) else ((1.691825E-14*this.gor_pb**4 - 1.1186283262E-10*this.gor_pb**3 + 2.5050523461284E-07*this.gor_pb**2 - 0.000326172822895141*this.gor_pb+ 1.09633452324753) if(this.gor_pb<=2800) else (9.62617288330966*this.gor_pb**-0.330245056775733)) if(this.api_gravity<=35) else ((9.05340406E-12*this.gor_pb**3 - 5.477960183599E-08*this.gor_pb**2 - 0.0000187062333062076*this.gor_pb + 1.04211720177118) if(this.gor_pb<=3600) else (1.25304754686904*math.exp(-0.000160581936871705*this.gor_pb))) if(this.api_gravity<=40) else ((-6.81914E-15*this.gor_pb**4 + 8.117362641E-11*this.gor_pb**3 - 3.0879022809335E-07*this.gor_pb**2 + 0.000307475327489179*this.gor_pb + 0.957313164734268) if(this.gor_pb<=3000) else (24.7791036305745*this.gor_pb**-0.437744173032469)) if(this.api_gravity<=45) else ((-7.46453713E-12*this.gor_pb**3 + 2.508428510946E-08*this.gor_pb**2 - 0.0000430806302949483*this.gor_pb + 1.04733906131687) if(this.gor_pb<=4000) else (1.58962223405974*math.exp(-0.000171385041097362*this.gor_pb)))

    def getRev1Multiplier(this):
        return ((-2.09E-18*this.gor_pb**6 + 8.78778E-15*this.gor_pb**5 - 1.353339485E-11*this.gor_pb**4 + 9.35867924764E-09*this.gor_pb**3 - 3.03724590634351E-06*this.gor_pb**2 + 0.000535438915783373*this.gor_pb + 0.998341769716922) if(this.gor_pb<=1300) else (2.74084521901785*this.gor_pb**-0.142865474418701)) if(this.api_gravity<=30) else ((2.28064637933E-09*this.gor_pb**2 - 0.000178726868860418*this.gor_pb + 1.08325028875836) if(this.gor_pb<=2500) else (1.29008790505058*math.exp(-0.000295619810322929*this.gor_pb))) if(this.api_gravity<=35) else ((2.067351825E-11*this.gor_pb**3 - 1.0982804104652E-07*this.gor_pb**2 - 0.0000102944767625906*this.gor_pb+ 1.06774197415799) if(this.gor_pb<=3500) else (9.92136881272006*this.gor_pb**-0.330687196666038)) if(this.api_gravity<=40) else ((0*this.gor_pb**6 - 1.829E-17*this.gor_pb**5 + 8.357198E-14*this.gor_pb**4 - 1.0301550181E-10*this.gor_pb**3 - 1.7483976827165E-07*this.gor_pb**2 + 0.000235873276571976*this.gor_pb + 1.00341302867217) if(this.gor_pb<=4000) else (1.15953619258512*math.exp(-0.000172282935140801*this.gor_pb))) if(this.api_gravity<=45) else ((1.69E-18*this.gor_pb**5 - 2.757741E-14*this.gor_pb**4 + 1.5130186871E-10*this.gor_pb**3 - 3.6392129225164E-07*this.gor_pb**2 + 0.000295943098206147*this.gor_pb + 0.975034529231939) if(this.gor_pb<=3000) else (1.78564045817829*math.exp(-0.000243983180009242*this.gor_pb)))

    def processRhopo(this, rhopogcc, bppAtTemp, temp, rsAtTemp1, first_fvf_from_below_bp=None):
        if bppAtTemp[-1] == 14.696: temp[-1]= 60.
        densityofCrudeOil= this.SpecificGravityOfCrudeOil*0.9991026
        mfactor= -1.2818+4.8303*(rhopogcc/densityofCrudeOil)-2.5485*(rhopogcc/densityofCrudeOil)**2
        nfactor= 0.6827-1.3039*(rhopogcc/densityofCrudeOil)+0.6212*(rhopogcc/densityofCrudeOil)**2
        rhoeo= np.array([rhopogcc[i] if ((temp[i]-32.)*5./9.)<1 else (densityofCrudeOil if(rsAtTemp1[i]==0.) else (rhopogcc[i]*(mfactor[i]+nfactor[i]*math.log((bppAtTemp[i]/145.038)/((temp[i]-32.)*5./9.))))) for i in range(len(bppAtTemp))])
        pressureCorrection= (0.00038794+0.0375885*10.**(-2.653*rhopogcc))*(bppAtTemp/145.038)*np.exp(-1.*(bppAtTemp/145.038)*(1.00763*10.**-6.+0.00088631*10.**(-3.7645*rhopogcc))/(0.00038794+0.0375885*10.**(-2.653*rhopogcc)))
        tempCorrection= np.array([0. if(rsAtTemp1[i]==0.) else ((0.000169756+0.000933538*((temp[i]-32.)*5./9.-15.56)-0.00000153832*((temp[i]-32.)*5./9.-15.56)**2.)*math.exp(-1.*(12.9686+0.00401368*((temp[i]-32.)*5./9.-15.56)-0.00011863*((temp[i]-32.)*5./9.-15.56)**2.)*pressureCorrection[i])) for i in range(len(pressureCorrection))])
        oilDensity= (rhoeo+pressureCorrection-tempCorrection)/0.0160185
        FVF_Bo_Calc= (62.42797*this.SpecificGravityOfCrudeOil+0.01357*rsAtTemp1*this.gas_sp_gravity)/oilDensity
        if first_fvf_from_below_bp is not None: FVF_Bo_Calc[-1]= first_fvf_from_below_bp #copy first fvf from below bubble point
        reducedPressure= bppAtTemp/bppAtTemp[0]
        X= np.array([this.find_nearest(reducedPressure, 0.2)[1], this.find_nearest(reducedPressure, 0.15)[1], reducedPressure[-1]])
        Y= np.array([FVF_Bo_Calc[this.find_nearest(reducedPressure, 0.2)[0]], FVF_Bo_Calc[this.find_nearest(reducedPressure, 0.15)[0]]*0.9996, 1.])
        matB= this.getCoeff(X, Y)
        
        #
        multiplier_fvf_pb=None #formula is diff for Rev1 and Rev2
        if this.multiplier_fvf_pb is None:
            gorAtBbp= rsAtTemp1[0]
            if this.poasml_revision_selected == 'rev1':
                #multiplier_fvf_pb= 0.981261268994193*gorAtBbp**0.00822329992168882
                multiplier_fvf_pb= this.getRev1Multiplier()
            else:
                #multiplier_fvf_pb=0.984985400564186*gorAtBbp**0.00663368842846082
                multiplier_fvf_pb= this.getRev2Multiplier()

            if math.isnan(this.fvf_pb) or this.fvf_pb is None:
                this.logger.debug("fvf_pb is NOT provided, multiplier used:%s" %(multiplier_fvf_pb))
            else:
                multiplier_fvf_pb= this.fvf_pb/FVF_Bo_Calc[0]
                this.logger.debug("fvf_pb is provided, calculated multiplier used:%s" %(multiplier_fvf_pb))
            return np.array([1.]), multiplier_fvf_pb #this is just for calculating and returning multiplier value, first paramater is not used anywhere 
        else:
            multiplier_fvf_pb= this.multiplier_fvf_pb

        FVF_Bo_Calc_final= None
        if first_fvf_from_below_bp is not None:
            FVF_Bo_Calc_final= FVF_Bo_Calc*multiplier_fvf_pb
            FVF_Bo_Calc_final[-1]= first_fvf_from_below_bp #copy first fvf from below bubble point
        else:
            FVF_Bo_Calc_final= np.array([((FVF_Bo_Calc[i]*(1.+(multiplier_fvf_pb-1.)/(1.-reducedPressure[-1])*abs(reducedPressure[i]-reducedPressure[-1])))) if(bppAtTemp[i] <= 175.0) else ((matB[0]+matB[1]*reducedPressure[i]+matB[2]*reducedPressure[i]**2) if(reducedPressure[i]<=X[0]) else (FVF_Bo_Calc[i]*(1.+(multiplier_fvf_pb-1.)/(1.-X[0])*abs(reducedPressure[i]-X[0])))) for i in range(len(reducedPressure))])
        return FVF_Bo_Calc_final, multiplier_fvf_pb

    def after_solver_Below_Bubble_Rev1(this, data):
        FVF_all_temp_below_bp=OrderedDict()
        for i in range(len(data['TempDb'])):
            # if (data['TempDb'][i]) == 240.0:
                # print "here"            
            this.logger.info("In progress: after_solver_Below_Bubble_Rev1::%s" %(data['TempDb'][i]))
            bppAtTemp= np.array(data['%s' %(data['TempDb'][i])]['BelowBPPatTemp'])
            rsAtTemp = np.array(data['%s' %(data['TempDb'][i])]['BelowRsAtTemp'])
            rsAtTemp1, var_calc= [], []
            iniVar   = np.array([50.]) #po initial
            for j in range(len(bppAtTemp)):
                svars    = np.ones(1) #1 variable.
                bounds   = np.array([[0.1, 5.]]) 
                rsAtTemp1.append(0. if rsAtTemp[j] == 10.**-6 else rsAtTemp[j])
                var_calc.append(ncg().minimize(svars, bounds, this._cost, this.logger, max_iter=300, debug=this.debug, region_f=this.define_region, converge_f=this.converge_f, args=(iniVar, bppAtTemp[j], rsAtTemp1[j], this.gas_sp_gravity, this.SpecificGravityOfCrudeOil))[0])
            temp= np.repeat(data['TempDb'][i], len(bppAtTemp))
            rsAtTemp1= np.array(rsAtTemp)
            rhopo= np.array(var_calc) * np.repeat(iniVar[0], len(var_calc))
            rhopogcc= rhopo*0.0160185
            fvf_below_bpp, multiplier_fvf_pb= this.processRhopo(rhopogcc, bppAtTemp, temp, rsAtTemp1)
            fvf_below_bpp[np.where(fvf_below_bpp < 1.)]= 1.
            fvf_below_bpp[-1]= 1. #last bpp is 14.696, rs is 0.0, fvf is 1.0
            FVF_all_temp_below_bp['%s' %(data['TempDb'][i])]= {'BelowFVFAtTemp': fvf_below_bpp, 'BelowRHOPOAtTemp': rhopo}
        return FVF_all_temp_below_bp, multiplier_fvf_pb
        
    def after_solver_Below_Bubble_Rev2(this, data):
        FVF_all_temp_below_bp=OrderedDict()
        for i in range(len(data['TempDb'])):
            # if (data['TempDb'][i]) == 240.0:
                # print "here" 
            this.logger.info("In progress: after_solver_Below_Bubble_Rev2::%s" %(data['TempDb'][i]))
            bppAtTemp= np.array(data['%s' %(data['TempDb'][i])]['BelowBPPatTemp'])
            rsAtTemp = np.array(data['%s' %(data['TempDb'][i])]['BelowRsAtTemp'])
            dens_apparent= 0.61703*10.**(-0.00326*this.api_gravity)+(1.51775-0.54351*math.log10(this.api_gravity))*math.log10(this.gas_sp_gravity)
            conversion= 0.028317/0.159  #hardcoded?
            rsAtTemp1= np.array([0. if rsAtTemp[j] == 10.**-6 else rsAtTemp[j] for j in range(len(rsAtTemp))])
            rhopo= (this.SpecificGravityOfCrudeOil+0.001223*rsAtTemp1*conversion*this.gas_sp_gravity)/(1.+0.001223*rsAtTemp1*conversion*this.gas_sp_gravity/dens_apparent)
            temp= np.repeat(data['TempDb'][i], len(bppAtTemp))
            fvf_below_bpp, multiplier_fvf_pb= this.processRhopo(rhopo, bppAtTemp, temp, rsAtTemp1)
            fvf_below_bpp[np.where(fvf_below_bpp < 1.)]= 1.
            fvf_below_bpp[-1]= 1. #last bpp is 14.696, rs is 0.0, fvf is 1.0
            FVF_all_temp_below_bp['%s' %(data['TempDb'][i])]= {'BelowFVFAtTemp': fvf_below_bpp}
        return FVF_all_temp_below_bp, multiplier_fvf_pb


if __name__ == "__main__":
    logger= getLogger('PBO', 'WellB')
    obj = FormationVolumeFactor().solve1(logger, None, debug=False)
    #python FormationVolumeFactor.py -e Reqs/Rs_Bubble_Pont_Solver.xlsx -t 'blackoil-viscosity<=5cp' -s
