from FormationVolumeFactor import *
from Scripts.pvt.core.optimizers.poas_interpolator import monotone


class RsTempPressure(object):
    def __init__(this, parent):
        this.__dict__.update(parent.__dict__)

    def input_solver1(this, pressure, rs, a1, a2, a3):        
        this.pressure = pressure
        this.rs       = rs
        this.pr       = pressure/pressure[0]
        this.a1       = a1
        this.a2       = a2
        this.a3       = a3

    def input_solver2(this, temp, bppCalculated):        
        this.temp         = temp
        this.bppCalculated= bppCalculated

    def calc_1(this):        
        data= OrderedDict()
        data['TempDb']= this.temp.tolist()
        #data['BPPDb'] = this.bppInterpolated_db.tolist()
        data['BPPCalc'] = this.bppCalculated.tolist()
        for i in range(len(this.temp)):
            temp= this.temp[i]
            this.logger.info("In progress poas_interpolator:%s" %(temp))
            p= this.bppCalculated[i]
            #bpp= np.append(np.arange(p, this.pressure[-1], -20.), this.pressure[-1])
            bpp= None
            if p <= 14.696:
                #data["%s" %(temp)]= {'BelowBPPatTemp':[14.696], "BelowRsAtTemp":[0.0]}
                data["%s" %(temp)]= {'BelowBPPatTemp':[14.697, 14.696], "BelowRsAtTemp":[this.gor_pb, 0.0]}
                continue
            else:
                end= 14.696 
                interval= (p-14.696)/50. if (p-14.696)/50. < 1 else int((p-14.696)/50.)
                bpp= np.append(np.arange(p, end, -interval), end)
            reducedPressure= bpp/p
            a1= np.array(monotone(this.pr.tolist(), this.a1.tolist(), reducedPressure.tolist()))
            a2= np.array(monotone(this.pr.tolist(), this.a2.tolist(), reducedPressure.tolist()))
            a3= np.array(monotone(this.pr.tolist(), this.a3.tolist(), reducedPressure.tolist()))
            rsr_calc= a1*reducedPressure**a2+(1.-a1)*reducedPressure**a3
            rs_calc = rsr_calc * this.rs[0]
            rp= np.array([1., ((1.+reducedPressure[-1])/2.) if(reducedPressure[-1]>=0.5) else 0.5, reducedPressure[-1]])
            rc= np.array([0., rs_calc[-1]/4., rs_calc[-1]])
            mata= np.array([np.ones(3), rp, rp**2]).T
            matA= mata.T.dot(mata)
            matB= np.linalg.inv(matA).dot(mata.T.dot(rc))
            rs_correction= [0. if(r==1) else 0. if((matB[0]+matB[1]*r+matB[2]*r**2)<0) else (matB[0]+matB[1]*r+matB[2]*r**2) for r in reducedPressure]
            final_rs= rs_calc - rs_correction
            final_rs[np.where(final_rs<=0)]= 0.0
            marker= np.where(final_rs[1:]-final_rs[0:-1]>1)[0]
            if len(marker)>0:
                marker= marker+1 #first index removed from above subtraction
                for z in range(marker[0], len(final_rs)):
                    if final_rs[z] > final_rs[marker[0]-1] and z not in marker.tolist():
                        marker= np.append(marker, z)
                final_rs_outlierremoved= np.delete(final_rs, marker)
                bpp_outlierremoved= np.delete(bpp, marker)
                final_rs= np.array(
                    monotone(bpp_outlierremoved.tolist(), final_rs_outlierremoved.tolist(), bpp.tolist()))
            final_rs[np.where(final_rs<=0)]= 0.0
            final_rs[-1]= 0.0 #last bpp is 14.696            
            data["%s" %(temp)]= {'BelowBPPatTemp':bpp.tolist(), "BelowRsAtTemp":final_rs.tolist()}
        return data

    def calc_23(this):        
        data= OrderedDict()
        data['TempDb']= this.temp.tolist()
        data['BPPCalc'] = this.bppCalculated.tolist()
        for i in range(len(this.temp)):
            temp= this.temp[i]
            p= this.bppCalculated[i]
            if p <= 14.696:
                gor_pb = 0 if(this.gor_pb is None or math.isnan(this.gor_pb)) else this.gor_pb
                data["%s" %(temp)]= {'BelowBPPatTemp':[14.697, 14.696], "BelowRsAtTemp":[gor_pb, 0.0]}
                #data["%s" %(temp)]= {'BelowBPPatTemp':[14.696], "BelowRsAtTemp":[0.0]}
                continue
            else:
                end= 14.696 
                interval= (p-14.696)/50. if (p-14.696)/50. < 1 else int((p-14.696)/50.)
                bpp= np.append(np.arange(p, end, -interval), end)
            pr= bpp/p
            a1= 9.73*10.**-7.*(this.gas_sp_gravity**1.672608)*(this.api_gravity**0.92987)*(temp**0.247235)*(p**1.056052)
            a2= 0.022339*(this.gas_sp_gravity**(-1.00475))*(this.api_gravity**0.337711)*(temp**0.132795)*(p**0.302065)
            a3= 0.725167*(this.gas_sp_gravity**(-1.48548))*(this.api_gravity**(-0.164741))*(temp**(-0.09133))*(p**0.047094)
            rsr_calc= a1*pr**a2+(1.-a1)*pr**a3
            rs_calc = rsr_calc * this.gor_pb
            rp= np.array([1., ((1.+pr[-1])/2.) if(pr[-1]>=0.5) else 0.5, pr[-1]])
            rc= np.array([0., rs_calc[-1]/4., rs_calc[-1]])
            mata= np.array([np.ones(3), rp, rp**2]).T
            matA= mata.T.dot(mata)
            matB= np.linalg.inv(matA).dot(mata.T.dot(rc))
            rs_correction= [0. if(r==1) else 0. if((matB[0]+matB[1]*r+matB[2]*r**2)<0) else (matB[0]+matB[1]*r+matB[2]*r**2) for r in pr]
            final_rs= rs_calc - rs_correction
            final_rs[np.where(final_rs<=0)]= 0.0
            marker= np.where(final_rs[1:]-final_rs[0:-1]>1)[0]
            if len(marker)>0:
                marker= marker+1 #first index removed from above subtraction
                for z in range(marker[0], len(final_rs)):
                    if final_rs[z] > final_rs[marker[0]-1] and z not in marker.tolist():
                        marker= np.append(marker, z)
                final_rs_outlierremoved= np.delete(final_rs, marker)
                bpp_outlierremoved= np.delete(bpp, marker)
                final_rs= np.array(
                    monotone(bpp_outlierremoved.tolist(), final_rs_outlierremoved.tolist(), bpp.tolist()))
            final_rs[-1]= 0.0 #last bpp is 14.696
            final_rs[np.where(final_rs<=0)]= 0.0
            data["%s" %(temp)]= {'BelowBPPatTemp':bpp.tolist(), "BelowRsAtTemp":final_rs.tolist()}
        return data

    def solve1(this, pressure, rs, a1, a2, a3, temp, bppCalculated, gor_pb, logger, debug=False):
        this.gor_pb= gor_pb
        this.input_solver1(pressure, rs, a1, a2, a3)
        this.input_solver2(temp, bppCalculated)
        data= this.calc_1()
        return data

    def solve23(this, temp, bppCalculated):
        this.temp             = temp
        this.bppCalculated    = bppCalculated
        data= this.calc_23()
        return data

    # def solve4(this, df_general_data, temp, bppCalculated, logger, debug=False):
    #     this.debug               = debug
    #     this.logger              = logger
    #     this.temp                = temp
    #     this.bppCalculated       = bppCalculated
    #     this.gas_sp_gravity   = float(df_general_data['value']['gas_sp_gravity'])
    #     this.api_gravity     = float(df_general_data['value']['api_gravity'])
    #     this.bubblePointPressure = float(df_general_data['value']['pb_res_temp'])
    #     this.reservoirTemp       = float(df_general_data['value']['temp_res'])
    #     this.fluid_type          = df_general_data['value']['fluid_type']
    #     this.pb_correlation      = df_general_data['value']['pb_correlation']
    #     this.api_specific_gravity =141.5/(131.5+this.api_gravity)
    #     data= this.calc_4()
    #     return data


if __name__ == "__main__":
    obj = RsTempPressure()
    logger= getLogger('PBO', 'WellB')
    obj = obj.solve(logger, debug=True)
    #python RsTempPressure.py