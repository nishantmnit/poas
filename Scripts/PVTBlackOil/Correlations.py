import io, os, sys, time, math
from collections import OrderedDict
from copy import deepcopy
import numpy as np


class Correlations(object):
    def __init__(this, parent):
        this.__dict__.update(parent.__dict__)

    def yg_lasater(this):
        effective_Oil= (630.-10.*this.api_gravity) if(this.api_gravity<=40.) else (73110.*this.api_gravity**(-1.562))
        yg=None
        if this.caseno == 2: #gor_pb provided
            yg= (this.gor_pb/379.3)/((this.gor_pb/379.3)+350.*this.api_specific_gravity/effective_Oil)
        if this.caseno == 1 or this.caseno == 3: #gor_pb not provided
            pf= this.pb_res_temp*this.gas_sp_gravity/(this.temp_res+459.67)
            yg= (0.00000921882158166909*pf**6. + 0.000179725109716884*pf**5. - 0.00592139779195938*pf**4. + 0.0514814619008946*pf**3. - 0.206451620328116*pf**2. + 0.501545401892031*pf - 0.018872249243691) if(pf<=5) else (0.41964896156801*math.log(pf) + 0.0849694367651798)
        return yg, effective_Oil

    def pb_res_temp_atCorrelations(this, bo_correlations_selected, gor_pb, temp_res):
        pb_res_temp= None
        if bo_correlations_selected == 'vazquez':
            pb_res_temp=((gor_pb/(0.0362*this.gas_sp_gravity*math.exp(25.724*(this.api_gravity/(temp_res+459.67)))))**(1./1.0937)) if(this.api_gravity<=30) else ((gor_pb/(0.0178*this.gas_sp_gravity*math.exp(23.931*(this.api_gravity/(temp_res+459.67)))))**(1./1.187))
        if bo_correlations_selected == 'deghetto':
            gamma_gs=this.gas_sp_gravity*(1.+5.912*10.**-5.*this.api_gravity*this.temp_sep*math.log10(this.p_sep/114.7))
            pb_res_temp= ((gor_pb/this.gas_sp_gravity)**(1./1.1128)*10.7025/10.**(0.00169*this.api_gravity-0.00156*temp_res)) if(this.api_gravity<=10) else ((56.434*gor_pb/(gamma_gs*10.**(10.9267*(this.api_gravity/(temp_res+459.67)))))**(1./1.2057)) if(this.api_gravity<=22.3) else ((gor_pb/(0.10084*(gamma_gs**0.2556)*10.**(7.4576*this.api_gravity/(temp_res+459.67))))**1.0134) if(this.api_gravity<=31.1) else ((gor_pb/(0.01347*(gamma_gs**0.3873)*10.**(12.753*this.api_gravity/(temp_res+459.67))))**0.8536)
        if bo_correlations_selected == 'standing':
            pb_res_temp= 18.2*(((gor_pb/this.gas_sp_gravity)**0.83)*10.**(0.00091*temp_res-0.0125*this.api_gravity)-1.4)
        if bo_correlations_selected == 'elam':
            pb_res_temp= (gor_pb**0.702)/(this.gas_sp_gravity**0.514)*math.exp(0.00348*temp_res-0.0282*this.api_gravity+3.58)
        if bo_correlations_selected == 'glaso':
            pb_res_temp= 10.**(1.7669+1.7447*(math.log10(((gor_pb/this.gas_sp_gravity)**0.816)*(temp_res**0.172)/(this.api_gravity**0.989)))-0.30218*(math.log10(((gor_pb/this.gas_sp_gravity)**0.816)*(temp_res**0.172)/(this.api_gravity**0.989)))**2.)
        if bo_correlations_selected in ['almarhoun85','almarhoun85rev']:
            pb_res_temp= (-64.13891+7.02362*10.**-3.*((gor_pb**0.722569)*(this.api_specific_gravity**3.04659)/(this.gas_sp_gravity**1.879109)*(temp_res+459.67)**1.302347)-2.278475*10.**-9.*((gor_pb**0.722569)*(this.api_specific_gravity**3.04659)/(this.gas_sp_gravity**1.879109)*(temp_res+459.67)**1.302347)**2.)
        if bo_correlations_selected in ['almarhoun88','almarhoun88rev']:
            pb_res_temp= (5.38088*10.**-3.*(gor_pb**0.715082)*(this.api_specific_gravity**3.1437)*(this.gas_sp_gravity**(-1.87784))*(temp_res+459.67)**1.32657)
        if bo_correlations_selected == 'petrosky':
            pb_res_temp= 112.727*((gor_pb**0.5774)/(this.gas_sp_gravity**0.8439)*(10.**(4.561*10.**-5.*temp_res**1.3911-7.916*10.**-4.*this.api_gravity**1.541))-12.34)
        if bo_correlations_selected == 'farshad':
            pb_res_temp= 64.14*((gor_pb**0.6343)/(this.gas_sp_gravity**1.15036*10**(7.97*10**-3*this.api_gravity-3.35*10**-4*temp_res))-7.2818)
        


        if bo_correlations_selected == 'kartoatmodjo':
            gamma_gsc= this.gas_sp_gravity*(1.+0.1595*(this.api_gravity**0.4078)/(this.temp_sep**0.2466)*math.log10(this.p_sep/114.7))
            pb_res_temp= ((gor_pb/((0.05958*gamma_gsc**0.7972)*10.**(13.1405*this.api_gravity/(temp_res+459.67))))**0.998602) if(this.api_gravity<=30.) else ((gor_pb/((0.0315*gamma_gsc**0.7587)*10.**(11.289*this.api_gravity/(temp_res+459.67))))**0.914328)
        if bo_correlations_selected == 'elsharkawi':
            pb_res_temp= ((gor_pb/(this.gas_sp_gravity*10.**(0.4636*this.api_gravity/temp_res-1.2179)))**(1./1.18026)) if(this.api_gravity<=30.) else ((gor_pb/(this.gas_sp_gravity**0.04439*this.api_gravity**1.1394*10.**(8.392*10.**-4.*temp_res-2.188)))**(1./0.94776))
        if bo_correlations_selected == 'livitan':
            pb_res_temp= 14.7*((gor_pb/this.gas_sp_gravity)**0.85)*(this.api_specific_gravity**5.)*((temp_res+459.67)/519.67)**1.5
        if bo_correlations_selected == 'alshammasi':
            pb_res_temp= 14.7*((gor_pb/this.gas_sp_gravity)**0.85)*(this.api_specific_gravity**5.)*((temp_res+459.67)/519.67)**1.5


        if bo_correlations_selected == 'alshammasirev':
            pb_res_temp= ((this.api_specific_gravity**7.8076228)*(this.gas_sp_gravity*gor_pb*(temp_res+459.67))**0.8212156)/(math.exp(2.2239006*this.gas_sp_gravity*this.api_specific_gravity))
        if bo_correlations_selected == 'dindoruk':
            pb_res_temp= 1.86997927*(gor_pb**1.221486524*(10.**((1.42828*10.**-10.*temp_res**2.844591797-6.74896*10.**-4.*this.api_gravity**1.225226436)/((0.033383304+2.*(this.gas_sp_gravity**0.084226069)*(gor_pb**(-0.272945957)))**2.)))/(this.gas_sp_gravity**1.370508349)+0.011688308)
        if bo_correlations_selected == 'alnajjar':
            pb_res_temp= (7.92*(gor_pb/this.gas_sp_gravity)**1.025*math.exp(-24.244*(this.api_gravity/(temp_res+459.67)))) if(this.api_gravity<=30.) else (30.91*(gor_pb/this.gas_sp_gravity)**0.816*math.exp(-19.748*(this.api_gravity/(temp_res+459.67))))
        if bo_correlations_selected == 'mazandarani':
            pb_res_temp= 1.09373*10.**-4.*gor_pb**0.5502*this.gas_sp_gravity**(-1.71956)*this.api_specific_gravity**2.5486*(temp_res+460.)**2.0967
        if bo_correlations_selected == 'mehran':
            pb_res_temp= 3.146*gor_pb**0.8035*this.gas_sp_gravity**(-1.3114)*this.api_specific_gravity**3.3925*temp_res**0.3466


        if bo_correlations_selected == 'lasater':
            yg= gor_pb
            pb_res_temp= ((696.110761485993*yg**6. - 1757.69547527489*yg**5. + 1676.10390661389*yg**4. - 744.633127278123*yg**3. + 159.997441144377*yg**2. - 11.5546141328804*yg + 0.319382704010309)*(temp_res+459.67)/this.gas_sp_gravity) if(yg<=0.8) else ((7.69837192911587*math.log(yg) + 7.15500097310337)*(temp_res+459.67)/this.gas_sp_gravity)

        if bo_correlations_selected == 'poasbo':
            masss= 0.5*((725.033154132545*math.exp(-0.0257680583978972*this.api_gravity)) if(this.api_gravity<15) else (0.0135714285714279*this.api_gravity**2. - 10.9750000000001*this.api_gravity + 649.892857142859) if(this.api_gravity<35) else (68696.1027360536*this.api_gravity**-1.54670611394534) if(this.api_gravity<75) else (6084./(this.api_gravity-5.9)))+0.5*((-6.00820817924E-09*this.api_gravity**6. + 9.3868069129915E-07*this.api_gravity**5. - 8.46154020132701E-06*this.api_gravity**4. - 0.00758988272475668*this.api_gravity**3. + 0.744218161062434*this.api_gravity**2. - 35.8894121532945*this.api_gravity+ 883.983158174593) if(this.api_gravity<=70) else (68696.1027360536*this.api_gravity**-1.54670611394534-5.) if(this.api_gravity<75) else (6084./(this.api_gravity-5.9)))

            factorq= ((gor_pb/379.3)/((gor_pb/379.3)+(350*this.api_specific_gravity)/masss))*100
            factorp= ((46.5617600690785*0.46686859324966) if(factorq<=10) else (-7.1143090452086E-07*factorq**6 + 0.000160655059767057*factorq**5 - 0.0135787791200388*factorq**4 + 0.537900916148428*factorq**3 - 10.0845420220969*factorq**2 + 96.7032223421474*factorq - 232.643482039578) if(factorq<=50) else (1.45007587811833*factorq**2 - 121.752639456637*factorq + 3299.73720237408)) if(this.api_gravity<=25) else ((49.3587468278483*factorq**0.496370800905798) if(factorq<=5) else (-1.3396161212612E-07*factorq**6 + 0.0000349358213874505*factorq**5 - 0.00335071834473636*factorq**4 + 0.1483997722731*factorq**3 - 2.75682664339276*factorq**2 + 27.8315185279062*factorq + 30.7978359078067) if(factorq<=80) else (2.8316627938118*factorq**2 - 344.265167681863*factorq + 12008.0923824352)) if(this.api_gravity<=35) else ((79.5978774656123*factorq**0.404829620261232) if(factorq<=10) else (-0.0000002483368666522*factorq**6 + 0.0000634211059018067*factorq**5 - 0.00604333852873069*factorq**4 + 0.261895948863619*factorq**3 - 4.57179162947328*factorq**2 + 29.7348349136005*factorq + 150.201597928884) if(factorq<=80) else (5.74596149897627*factorq**1.37411716498799)) if(this.api_gravity<=45) else ((213.957738294262*math.exp(0.0325941343746459*factorq)) if(factorq<=40) else (2.78812831482354*factorq**1.52547778445952))
            pb_res_temp= factorp*(temp_res**0.11*this.api_specific_gravity)/this.gas_sp_gravity

        return pb_res_temp


    def multiplier_for_gor_pb_atCorrelations(this, bo_correlations_selected, pb_res_temp_calc):
        multiplier_for_gor_pb= None
        if bo_correlations_selected == 'vazquez':
            multiplier_for_gor_pb= this.gor_pb/((0.0362*this.gas_sp_gravity*this.pb_res_temp**1.0937*math.exp(25.724*(this.api_gravity/(this.temp_res+459.67)))) if(this.api_gravity<=30) else (0.0178*this.gas_sp_gravity*this.pb_res_temp**1.187*math.exp(23.931*(this.api_gravity/(this.temp_res+459.67)))))
        if bo_correlations_selected == 'deghetto':
            gamma_gs=this.gas_sp_gravity*(1.+5.912*10.**-5.*this.api_gravity*this.temp_sep*math.log10(this.p_sep/114.7))
            multiplier_for_gor_pb= this.gor_pb/(((this.pb_res_temp*this.gas_sp_gravity**(1./1.1128)/(10.7025*10.**(0.00156*this.temp_res-0.00169*this.api_gravity)))**1.1128) if(this.api_gravity<=10.) else ((this.pb_res_temp**1.2057)/56.434*gamma_gs*10.**(10.9267*(this.api_gravity/(this.temp_res+459.67)))) if(this.api_gravity<=22.3) else ((this.pb_res_temp**(1./1.0134)*0.10084*gamma_gs**0.2556*10.**(7.4576*(this.api_gravity/(this.temp_res+459.67))))) if(this.api_gravity<=31.1) else ((this.pb_res_temp**(1./0.8536)*0.01347*gamma_gs**0.3873*10.**(12.753*(this.api_gravity/(this.temp_res+459.67))))))
        if bo_correlations_selected == 'standing':
            multiplier_for_gor_pb= this.gor_pb/(this.gas_sp_gravity*((this.pb_res_temp/18.2+1.4)*10.**(0.0125*this.api_gravity-0.00091*this.temp_res))**1.2048)
        if bo_correlations_selected == 'elam':
            multiplier_for_gor_pb= this.gor_pb/(((this.pb_res_temp*this.gas_sp_gravity**0.514)/(math.exp(0.00348*this.temp_res-0.0282*this.api_gravity+3.58)))**(1./0.702))
        if bo_correlations_selected == 'glaso':
            multiplier_for_gor_pb= this.gor_pb/(((10.**((-1.7447+math.sqrt(1.7447**2.-4.*(-0.30218)*(1.7669-math.log10(this.pb_res_temp))))/(2.*(-0.30218))))*(this.gas_sp_gravity**0.816)*(this.api_gravity**0.989)/this.temp_res**0.172)**(1./0.816))
        if bo_correlations_selected in ['almarhoun85', 'almarhoun85rev']:
            multiplier_for_gor_pb= this.gor_pb/(((-7.02362*10.**-3.+math.sqrt((7.02362*10.**-3.)**2.-4.*(-2.278475*10.**-9.)*(-64.13891-this.pb_res_temp)))/(2.*(-2.278475*10.**-9.)))*(this.gas_sp_gravity**1.879109)/((this.api_specific_gravity**3.04659)*(this.temp_res+459.67)**1.302347))**(1./0.722569)
        if bo_correlations_selected in ['almarhoun88', 'almarhoun88rev']:
            multiplier_for_gor_pb= this.gor_pb/(((this.pb_res_temp*this.gas_sp_gravity**1.87784)/(0.00538088*(this.api_specific_gravity**3.1437)*(this.temp_res+459.67)**1.32657))**(1./0.715082))
        if bo_correlations_selected == 'petrosky':
            multiplier_for_gor_pb= this.gor_pb/(((this.pb_res_temp/112.727+12.34)*(this.gas_sp_gravity**0.8439)/(10.**(4.561*10.**-5.*this.temp_res**1.3911-7.916*10.**-4.*this.api_gravity**1.541)))**(1./0.5774))
        if bo_correlations_selected == 'farshad':
            multiplier_for_gor_pb= this.gor_pb/(((this.pb_res_temp/64.14+7.2818)*(this.gas_sp_gravity**1.15036)*(10**(7.97*10**-3*this.api_gravity-3.35*10**-4*this.temp_res)))**(1/0.6343))
        

        if bo_correlations_selected == 'kartoatmodjo':
            gamma_gsc= this.gas_sp_gravity*(1.+0.1595*(this.api_gravity**0.4078)/(this.temp_sep**0.2466)*math.log10(this.p_sep/114.7))
            multiplier_for_gor_pb= this.gor_pb/(((this.pb_res_temp**(1./0.998602))*((0.05958*gamma_gsc**0.7972)*10.**(13.1405*this.api_gravity/(this.temp_res+459.67)))) if(this.api_gravity<=30.) else ((this.pb_res_temp**(1./0.914328))*((0.0315*gamma_gsc**0.7587)*10.**(11.289*this.api_gravity/(this.temp_res+459.67)))))
        if bo_correlations_selected == 'elsharkawi':
            multiplier_for_gor_pb= this.gor_pb/(((this.pb_res_temp**1.18026*(this.gas_sp_gravity*10.**(0.4636*this.api_gravity/this.temp_res-1.2179)))) if(this.api_gravity<=30.) else ((this.pb_res_temp**0.94776*(this.gas_sp_gravity**0.04439*this.api_gravity**1.1394*10**(8.392*10.**-4.*this.temp_res-2.188)))))
        if bo_correlations_selected == 'livitan':
            multiplier_for_gor_pb= this.gor_pb/(((this.pb_res_temp/14.7)*this.gas_sp_gravity**0.85/(this.api_specific_gravity**5.*((this.temp_res+459.67)/519.67)**1.5))**(1./0.85))
        if bo_correlations_selected == 'alshammasi':
            multiplier_for_gor_pb= this.gor_pb/(((this.pb_res_temp/14.7)*this.gas_sp_gravity**0.85/(this.api_specific_gravity**5.*((this.temp_res+459.67)/519.67)**1.5))**(1./0.85))


        if bo_correlations_selected == 'alshammasirev':
            multiplier_for_gor_pb= this.gor_pb/(((this.pb_res_temp*(math.exp(2.2239006*this.gas_sp_gravity*this.api_specific_gravity))*(this.api_specific_gravity**(-7.8076228)))**(1./0.8212156))/(this.gas_sp_gravity*(this.temp_res+459.67)))
        if bo_correlations_selected == 'dindoruk':
            this.dindoruk_multiplier= this.gor_pb/(((pb_res_temp_calc/3.35975497+28.10133245)*(this.gas_sp_gravity**1.57905016)*(10.**((4.86996*10.**-6.*this.api_gravity**5.730982539+9.9251*10.**-3.*this.temp_res**1.776179364)/((44.2500268+2.*this.api_gravity**2.702889206*pb_res_temp_calc**(-0.744335673))**2.))))**0.928131344)
            multiplier_for_gor_pb= this.gor_pb/((((this.pb_res_temp/3.35975497+28.10133245)*(this.gas_sp_gravity**1.57905016)*(10.**((4.86996*10.**-6.*this.api_gravity**5.730982539+9.9251*10.**-3.*this.temp_res**1.776179364)/((44.2500268+2.*this.api_gravity**2.702889206*this.pb_res_temp**(-0.744335673))**2.))))**0.928131344)*this.dindoruk_multiplier)
        if bo_correlations_selected == 'alnajjar':
            multiplier_for_gor_pb= this.gor_pb/((((this.pb_res_temp*this.gas_sp_gravity**1.025)/(7.92*math.exp(-24.244*(this.api_gravity/(this.temp_res+459.67)))))**(1./1.025)) if(this.api_gravity<=30.) else (((this.pb_res_temp*this.gas_sp_gravity**0.816)/(30.91*math.exp(-19.748*(this.api_gravity/(this.temp_res+459.67)))))**(1./0.816)))
        if bo_correlations_selected == 'mazandarani':
            multiplier_for_gor_pb= this.gor_pb/((this.pb_res_temp/(1.09373*10.**-4.*this.gas_sp_gravity**(-1.71956)*this.api_specific_gravity**2.5486*(this.temp_res+460.)**2.0967))**(1./0.5502))
        if bo_correlations_selected == 'mehran':
            multiplier_for_gor_pb= this.gor_pb/((this.pb_res_temp/(3.146*this.gas_sp_gravity**(-1.3114)*this.api_specific_gravity**3.3925*this.temp_res**0.3466))**(1./0.8035))


        if bo_correlations_selected == 'lasater':
            yg, effective_Oil= this.yg_lasater()
            gor_pb= 132755.*this.api_specific_gravity*this.gas_sp_gravity/(effective_Oil*(1.-yg))
            multiplier_for_gor_pb= this.gor_pb/gor_pb
        return multiplier_for_gor_pb


    def rs_atCorrelations(this, bo_correlations_selected, p, t):
        gor, yg= None, None
        if bo_correlations_selected == 'vazquez':
            if isinstance(p, float):
                gor= (0.0362*this.gas_sp_gravity*p**1.0937*math.exp(25.724*(this.api_gravity/(t+459.67)))) if(this.api_gravity<=30.) else (0.0178*this.gas_sp_gravity*p**1.187*math.exp(23.931*(this.api_gravity/(t+459.67))))
            else:
                gor= np.array([(0.0362*this.gas_sp_gravity*p[i]**1.0937*np.exp(25.724*(this.api_gravity/(t+459.67)))) if(this.api_gravity<=30.) else (0.0178*this.gas_sp_gravity*p[i]**1.187*np.exp(23.931*(this.api_gravity/(t+459.67)))) for i in range(len(p))])
        if bo_correlations_selected == 'deghetto':
            gamma_gs=this.gas_sp_gravity*(1.+5.912*10.**-5.*this.api_gravity*this.temp_sep*math.log10(this.p_sep/114.7))
            gor= ((p*this.gas_sp_gravity**(1./1.1128)/(10.7025*10.**(0.00156*t-0.00169*this.api_gravity)))**1.1128) if(this.api_gravity<=10) else ((p**1.2057)/56.434*gamma_gs*10.**(10.9267*(this.api_gravity/(t+459.67)))) if(this.api_gravity<=22.3) else ((p**(1./1.0134)*0.10084*gamma_gs**0.2556*10.**(7.4576*(this.api_gravity/(t+459.67))))) if(this.api_gravity<=31.1) else ((p**(1./0.8536)*0.01347*gamma_gs**0.3873*10.**(12.753*(this.api_gravity/(t+459.67)))))
        if bo_correlations_selected == 'standing':
            gor= (this.gas_sp_gravity*((p/18.2+1.4)*10.**(0.0125*this.api_gravity-0.00091*t))**1.2048)
        if bo_correlations_selected == 'elam':
            gor= (((p*this.gas_sp_gravity**0.514)/(np.exp(0.00348*t-0.0282*this.api_gravity+3.58)))**(1./0.702))
        if bo_correlations_selected == 'glaso':
            gor= ((10.**((-1.7447+np.sqrt(1.7447**2.-4.*(-0.30218)*(1.7669-np.log10(p))))/(2.*(-0.30218))))*(this.gas_sp_gravity**0.816)*(this.api_gravity**0.989)/t**0.172)**(1./0.816)
        if bo_correlations_selected in ['almarhoun85', 'almarhoun85rev']:
            gor= (((-7.02362*10.**-3.+np.sqrt((7.02362*10.**-3.)**2.-4.*(-2.278475*10.**-9.)*(-64.13891-p)))/(2.*(-2.278475*10.**-9.)))*(this.gas_sp_gravity**1.879109)/((this.api_specific_gravity**3.04659)*(t+459.67)**1.302347))**(1./0.722569)
        if bo_correlations_selected in ['almarhoun88', 'almarhoun88rev']:
            gor= (((p*this.gas_sp_gravity**1.87784)/(0.00538088*(this.api_specific_gravity**3.1437)*(t+459.67)**1.32657))**(1./0.715082))
        if bo_correlations_selected == 'petrosky':
            gor= (((p/112.727+12.34)*(this.gas_sp_gravity**0.8439)/(10.**(4.561*10.**-5.*t**1.3911-7.916*10.**-4.*this.api_gravity**1.541)))**(1./0.5774))
        if bo_correlations_selected == 'farshad':
            gor= (((p/64.14+7.2818)*(this.gas_sp_gravity**1.15036)*(10**(7.97*10**-3*this.api_gravity-3.35*10**-4*t)))**(1/0.6343))
        

        if bo_correlations_selected == 'kartoatmodjo':
            gamma_gsc= this.gas_sp_gravity*(1.+0.1595*(this.api_gravity**0.4078)/(this.temp_sep**0.2466)*math.log10(this.p_sep/114.7))
            gor= ((p**(1./0.998602))*((0.05958*gamma_gsc**0.7972)*10.**(13.1405*this.api_gravity/(t+459.67)))) if(this.api_gravity<=30) else ((p**(1./0.914328))*((0.0315*gamma_gsc**0.7587)*10.**(11.289*this.api_gravity/(t+459.67))))
        if bo_correlations_selected == 'elsharkawi':
            gor= (((p**1.18026*(this.gas_sp_gravity*10.**(0.4636*this.api_gravity/t-1.2179)))) if(this.api_gravity<=30.) else ((p**0.94776*(this.gas_sp_gravity**0.04439*this.api_gravity**1.1394*10.**(8.392*10.**-4.*t-2.188)))))
        if bo_correlations_selected == 'livitan':
            gor= (((p/14.7)*this.gas_sp_gravity**0.85/(this.api_specific_gravity**5.*((t+459.67)/519.67)**1.5))**(1./0.85))
        if bo_correlations_selected == 'alshammasi':
            gor= (((p/14.7)*this.gas_sp_gravity**0.85/(this.api_specific_gravity**5.*((t+459.67)/519.67)**1.5))**(1./0.85))


        if bo_correlations_selected == 'alshammasirev':
            gor= (((p*(math.exp(2.2239006*this.gas_sp_gravity*this.api_specific_gravity))*(this.api_specific_gravity**(-7.8076228)))**(1./0.8212156))/(this.gas_sp_gravity*(t+459.67)))
        if bo_correlations_selected == 'dindoruk':
            if hasattr(this, 'dindoruk_multiplier'):
                gor= ((((p/3.35975497+28.10133245)*(this.gas_sp_gravity**1.57905016)*(10.**((4.86996*10.**-6.*this.api_gravity**5.730982539+9.9251*10.**-3.*t**1.776179364)/((44.2500268+2.*this.api_gravity**2.702889206*p**(-0.744335673))**2.))))**0.928131344)*this.dindoruk_multiplier)
            else:
                gor= ((((p/3.35975497+28.10133245)*(this.gas_sp_gravity**1.57905016)*(10.**((4.86996*10.**-6.*this.api_gravity**5.730982539+9.9251*10.**-3.*t**1.776179364)/((44.2500268+2.*this.api_gravity**2.702889206*p**(-0.744335673))**2.))))**0.928131344))
        if bo_correlations_selected == 'alnajjar':
            gor= ((((p*this.gas_sp_gravity**1.025)/(7.92*math.exp(-24.244*(this.api_gravity/(t+459.67)))))**(1./1.025)) if(this.api_gravity<=30.) else (((p*this.gas_sp_gravity**0.816)/(30.91*math.exp(-19.748*(this.api_gravity/(t+459.67)))))**(1./0.816)))
        if bo_correlations_selected == 'mazandarani':
            gor= ((p/(1.09373*10.**-4.*this.gas_sp_gravity**(-1.71956)*this.api_specific_gravity**2.5486*(t+460.)**2.0967))**(1./0.5502))
        if bo_correlations_selected == 'mehran':
            gor= ((p/(3.146*this.gas_sp_gravity**(-1.3114)*this.api_specific_gravity**3.3925*t**0.3466))**(1./0.8035))


        if bo_correlations_selected == 'lasater':
            effective_Oil= (630.-10.*this.api_gravity) if(this.api_gravity<=40.) else (73110.*this.api_gravity**(-1.562))
            pf= p*this.gas_sp_gravity/(t+459.67)
            yg= None
            if isinstance(p, float):
                yg= (0.00000921882158166909*pf**6. + 0.000179725109716884*pf**5. - 0.00592139779195938*pf**4. + 0.0514814619008946*pf**3. - 0.206451620328116*pf**2. + 0.501545401892031*pf - 0.018872249243691) if(pf<=5) else (0.41964896156801*math.log(pf) + 0.0849694367651798)
                if yg <= 0: yg=0.001
            else:
                yg= np.array([(0.00000921882158166909*pf1**6. + 0.000179725109716884*pf1**5. - 0.00592139779195938*pf1**4. + 0.0514814619008946*pf1**3. - 0.206451620328116*pf1**2. + 0.501545401892031*pf1 - 0.018872249243691) if(pf1<=5) else (0.41964896156801*math.log(pf1) + 0.0849694367651798) for pf1 in pf])
                yg[np.where(yg <= 0)]= 0.001
            gor= 132755.*this.api_specific_gravity*this.gas_sp_gravity/(effective_Oil*(1.-yg))

        if bo_correlations_selected == 'poasbo': #name has bo but its used in poas_ml only. Its invalid for bo_correlations.
            masss= 0.5*((725.033154132545*math.exp(-0.0257680583978972*this.api_gravity)) if(this.api_gravity<15) else (0.0135714285714279*this.api_gravity**2. - 10.9750000000001*this.api_gravity + 649.892857142859) if(this.api_gravity<35) else (68696.1027360536*this.api_gravity**-1.54670611394534) if(this.api_gravity<75) else (6084./(this.api_gravity-5.9)))+0.5*((-6.00820817924E-09*this.api_gravity**6. + 9.3868069129915E-07*this.api_gravity**5. - 8.46154020132701E-06*this.api_gravity**4. - 0.00758988272475668*this.api_gravity**3. + 0.744218161062434*this.api_gravity**2. - 35.8894121532945*this.api_gravity+ 883.983158174593) if(this.api_gravity<=70) else (68696.1027360536*this.api_gravity**-1.54670611394534-5.) if(this.api_gravity<75) else (6084./(this.api_gravity-5.9)))
            factorp= p*this.gas_sp_gravity/(t**0.11*this.api_specific_gravity)
            factorq= ((-0.00793560089973866*factorp**2. + 1.97778011934522*factorp - 114.41118586137) if(factorp<=136) else (-6.178E-17*factorp**6. + 4.8711083E-13*factorp**5. - 1.25836165514E-09*factorp**4. + 1.42919033035437E-06*factorp**3. - 0.000774954308014051*factorp**2. + 0.244939565557711*factorp - 11.9424709533657) if(factorp<=1000) else (21.114860120259*math.log(factorp) - 91.3685079832296)) if(this.api_gravity<=25) else ((-0.000202297555035913*factorp**2. + 0.118265760744603*factorp - 2.84706969240795) if(factorp<=100) else ( -1.07E-18*factorp**6. + 1.188339E-14*factorp**5. - 5.131665236E-11*factorp**4. + 1.1269218440053E-07*factorp**3. - 0.000143323443560302*factorp**2. + 0.127099743524652*factorp- 3.64092169411876) if(factorp<=3000) else (16.6529347741494*math.log(factorp) - 50.3848226818556)) if(this.api_gravity<=35) else ((-0.0000617505085242264*factorp**2. + 0.0689263423821503*factorp + 8.68477065815183) if(factorp<=500) else (-2.33E-18*factorp**6. + 2.318853E-14*factorp**5. - 0.000000000087519946*factorp**4. + 1.5499468837495E-07*factorp**3. - 0.000132358185322748*factorp**2. + 0.0752408698415015*factorp + 15.8728675678551) if(factorp<=3000) else (1.52140142864335E-06*factorp**2. - 0.00386312501632633*factorp + 79.118584011818)) if(this.api_gravity<=45) else ((39.0614357294835*math.log(factorp) - 218.351200193447) if(factorp<=500) else (-1.403E-17*factorp**6. + 0.0000000000001482809*factorp**5. - 6.3480718839E-10*factorp**4. + 1.40988402611006E-06*factorp**3. - 0.00171230320305804*factorp**2. + 1.09983828337329*factorp - 246.958985653535) if(factorp<=2000) else (17.3679003244452*math.log(factorp) - 59.9030114350434))
            gor= (((350.*this.api_specific_gravity)/masss)*(factorq/100.)/(1.-factorq/100.))*379.3

        return gor, yg