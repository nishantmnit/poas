#POAS Machine Learning fpr RS Solver - case 1, case 2, case 3, pseudo case 4
from openpyxl.utils.cell import get_column_letter

from SolutionGORBubblePoint import *
from RsTempPressure import *
from RsTempPressureBOC import *
from FormationVolumeFactorBOC import *
from Correlations import *

cwd = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(0, "%s/../../../Python" %(cwd))
from Lib.Utils import *
from Lib.DBLib import *
from Scripts.pvt.core.optimizers.ncg import *
from Scripts.pvt.core.optimizers.poas_interpolator import *

#gas_sp_gravity: averageGasGravity
#temp_res: reservoirTemp
#pb_res_temp: bubblePointPressureAtReservoirTemp
#api_gravity: stockTankOilAPI
#fluid_type: fluid_code
#liq_density[-1]/0.9991026: SpecificGravityOfCrudeOil

class rssolver(object):
    def __init__(this, dbName=None):
        this.fluid_codes= ['bo','vh', 'vl', 'gc']
        this.readOpts()
        this.dbname= this.options.dbname if dbName is None else dbName
        this.db= getDbCon(this.dbname)
        this.coll= this.db[this.options.wellno] #using PBO
        this.cenDb= getDbCon('Central') #default is RTS
        this.cenDbPBO= this.cenDb[dbName]
        this.dateYmdHMS= datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
        this.debug= this.options.debug
        this.logger= getLogger(this.dbname, this.options.wellno)
        
    def readOpts(this):
        parser = OptionParser(usage='usage: %prog [options]')
        parser.add_option("-b", "--dbname",          dest="dbname",          default='PBO') 
        parser.add_option("-w", "--wellno",          dest="wellno",          default=None) 
        parser.add_option("-c", "--copymasterdata",  dest="copymasterdata",  action='store_true', default=False)
        parser.add_option("-t", "--fluid_code",      dest="fluid_code",      default=None,        help=','.join(this.fluid_codes))
        parser.add_option("-e", "--excel",           dest="excel",           default=None)
        parser.add_option("-d", "--debug",           dest="debug",           action='store_true', default=False)
        parser.add_option("-p", "--printoutput",     dest="printoutput",     action='store_true', default=False)
        parser.add_option("-s", "--skipcomputation", dest="skipcomputation", action='store_true', default=False)
        (options, args) = parser.parse_args()
        this.options = options
        
        if this.options.wellno is None:
        	print "Usage: SolutionGOR.py -w <AB8>"
        	sys.exit(0)
        if this.options.excel is not None:
            this.excel= this.options.excel
            if not os.path.exists(this.excel): 
                print "Excel file not found:%s" %(this.excel)
                sys.exit(1)


    def trimDfHeaders(this, df):
        renameCol = {}
        for col in df.columns:
            c = col.strip()
            renameCol[col] = c 
        df.rename(columns= renameCol, inplace=True)
        return df

    def fTOs(this, f):
        s = ('%s' % (float(f))).replace('.', '_')
        return s


    def copyMasterData(this):
        r= this.cenDbPBO.remove({'Property':'SolutionGOR'})
        dt= datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
        fluid_types_excel= 'Reqs/Fluid_Type_DATABASE.xlsx'
        if not os.path.exists(fluid_types_excel):
             this.logger.error("File not found:%s" %(fluid_types_excel)) 
             sys.exit(1)
        xls = pd.ExcelFile(fluid_types_excel)
        for sheetname in this.fluid_codes:
            df= pd.read_excel(xls, sheet_name=sheetname)
            gor=[]
            data={}
            data['Property']='SolutionGOR'
            data['OilType']=sheetname
            for col in df.columns: 
                if isinstance(col, (int, float, long)): 
                    gor.append(float(col))
                    data[this.fTOs(col)]= df[col].tolist()
            gor.sort()
            data['GOR']= gor
            data['CreatedAt']= dt
            this.cenDbPBO.insert(data)
            this.logger.info('MongoDB Central::PBO updated for Property:SolutionGOR, Type:%s' %(sheetname))
        this.logger.info("MongoDB Central database updated!")
        sys.exit(0)

    def compSpecificGravityOfCrudeOil(this):
        this.SpecificGravityOfCrudeOil= 141.5/(float(this.df_general_data['value']['api_gravity'])+131.5)
        this.logger.info("SpecificGravityOfCrudeOil calculated from general_data.api_gravity: %s" %(this.SpecificGravityOfCrudeOil))


    def correct_gs_a_gravity(this):
        if not this.df_separator.empty:
            #if gas_sp_gravity_calc is not None and api_gravity_calc is not None and (this.gas_sp_gravity!= gas_sp_gravity_calc or this.api_gravity!=api_gravity_calc) then show warning on UI.
            this.gas_sp_gravity, this.api_gravity= this.calc_gs_a_gravity_from_separator()
            this.df_general_data.loc['gas_sp_gravity', 'value']= this.gas_sp_gravity
            this.df_general_data.loc['api_gravity', 'value']= this.api_gravity
            
            this.SpecificGravityOfCrudeOil= float(this.df_separator['liq_density'].iloc[-1])/0.9991026
            this.logger.info("SpecificGravityOfCrudeOil calculated from separator.liq_density: %s" %(this.SpecificGravityOfCrudeOil))
        else:
            this.compSpecificGravityOfCrudeOil()

    def calc_separator_corrected(this):
        bo_fvf, gor_pb, pb_res_temp, temp_res= None, None, None, None
        temp_res= this.df_general_data['value']['temp_res']
        if not math.isnan(this.df_separator['bo_fvf'].iloc[0]):
           bo_fvf= this.df_separator['bo_fvf'].iloc[0]
        
        if ((not math.isnan(this.df_general_data['value']['gor_pb'])) and  (not math.isnan(this.df_general_data['value']['gor_pb']))):
            pb_res_temp= this.df_general_data['value']['pb_res_temp']
            gor_pb= this.df_general_data['value']['gor_pb']
        else:
            pb_res_temp, gor_pb= this.calc_pb_res_temp_from_separator()
        if bo_fvf is None:
            api_gravity = this.df_general_data['value']['api_gravity']
            gas_sp_gravity=None
            if math.isnan(this.df_general_data['value']['gas_sp_gravity']):
                gas_sp_gravity, api_gravity= this.calc_gs_a_gravity_from_separator()        
            else:    
                gas_sp_gravity = this.df_general_data['value']['gas_sp_gravity']
            specific_gravity_of_crude_oil= 141.5/(131.5+api_gravity)
            density_of_crude_oil= specific_gravity_of_crude_oil*0.9991026

            if not math.isnan(this.df_separator['liq_density'].iloc[0]):
                liq_density= (this.df_separator['liq_density'].iloc[0])/0.0160185
                bo_fvf=(62.42797*specific_gravity_of_crude_oil+0.01357*gor_pb*gas_sp_gravity)/liq_density
            else:
                dens_apparent= 0.61703*10.**(-0.00326*api_gravity)+(1.51775-0.54351*math.log10(api_gravity))*math.log10(gas_sp_gravity) 
                conversion=0.028317/0.159               
                rhopo= (specific_gravity_of_crude_oil+0.001223*gor_pb*conversion*gas_sp_gravity)/(1.+0.001223*gor_pb*conversion*gas_sp_gravity/dens_apparent)
                mfactor =-1.2818+4.8303*(rhopo/density_of_crude_oil)-2.5485*(rhopo/density_of_crude_oil)**2.
                nfactor =0.6827-1.3039*(rhopo/density_of_crude_oil)+0.6212*(rhopo/density_of_crude_oil)**2.
                rhoeo =rhopo if(((temp_res-32.)*5./9.)<1) else (density_of_crude_oil if(gor_pb==0) else (rhopo*(mfactor+nfactor*math.log((pb_res_temp/145.038)/((temp_res-32.)*5./9.)))))
                pressureCor= (0.00038794+0.0375885*10.**(-2.653*rhopo))*(pb_res_temp/145.038)*math.exp(-1.*(pb_res_temp/145.038)*(1.00763*10.**-6.+0.00088631*10.**(-3.7645*rhopo))/(0.00038794+0.0375885*10.**(-2.653*rhopo)))
                tempCor= 0. if(gor_pb==0) else ((0.000169756+0.000933538*((temp_res-32.)*5./9.-15.56)-0.00000153832*((temp_res-32.)*5./9.-15.56)**2.)*math.exp(-1.*(12.9686+0.00401368*((temp_res-32.)*5./9.-15.56)-0.00011863*((temp_res-32.)*5./9.-15.56)**2.)*pressureCor))
                oil_density= (rhoeo+pressureCor-tempCor)/0.0160185
                bo_fvf= (62.42797*specific_gravity_of_crude_oil+0.01357*gor_pb*gas_sp_gravity)/oil_density
                multipler= 0.971
                bo_fvf=bo_fvf*multipler

        gor_dv_oil= np.array(this.df_dv_oil['gor'])
        max_gor_dv_oil= max(gor_dv_oil[~np.isnan(gor_dv_oil)])
        pb_index= np.where(gor_dv_oil == max_gor_dv_oil)[0][0]
        fvf_bod= this.df_dv_oil['bo_fvf'].iloc[pb_index]
        gor_pb_dv= this.df_dv_oil['gor'].iloc[pb_index]

        dv_oil_bo_fvf= np.array(this.df_dv_oil['bo_fvf'])
        this.df_separator_corrected['bo_fvf']=  [1. if((bo_fvf*(b/fvf_bod))<1) else (bo_fvf*(b/fvf_bod)) for b in dv_oil_bo_fvf]

        gor_separator_corrected= np.empty(len(gor_dv_oil))
        gor_separator_corrected[:]= np.nan
        gor_separator_corrected[pb_index:]= [0.0 if((gor_pb-(gor_pb_dv-g)*(bo_fvf/fvf_bod))<=0) else (gor_pb-(gor_pb_dv-g)*(bo_fvf/fvf_bod)) for g in gor_dv_oil[pb_index:]]

        this.df_separator_corrected['gor']     = gor_separator_corrected
        this.df_separator_corrected['pressure']= this.df_dv_oil['pressure']
        this.df_separator_corrected['liq_density']= this.df_dv_oil['liq_density']


    def calc_pb_res_temp_from_separator(this):
        this.pb_res_temp= this.df_separator['pressure'].iloc[0]  #"Total at Bubble Point" is at first row and its verified in checkMandatoryFields()
        if not math.isnan(this.df_separator['gor'].iloc[0]):
            this.gor_pb= this.df_separator['gor'].iloc[0]
        else:
            this.gor_pb= sum(this.df_separator[1:-2]['gor'])
        
        this.df_general_data.loc['gor_pb', 'value']= this.gor_pb 
        this.df_general_data.loc['pb_res_temp', 'value']= this.pb_res_temp                 
        return this.pb_res_temp, this.gor_pb

    def input_data(this):  
        this.gas_sp_gravity             = float(this.df_general_data['value']['gas_sp_gravity']) #take from MongoDB user input. If not available then compute
        this.temp_res                   = float(this.df_general_data['value']['temp_res'])
        this.pb_res_temp                = float(this.df_general_data['value']['pb_res_temp'])
        this.api_gravity                = float(this.df_general_data['value']['api_gravity'])
        this.fluid_code                 = this.df_general_data['value']['fluid_type']
        this.fvf_pb                     = this.df_general_data['value']['fvf_pb'] #used as multipler in case 23 or 5
        this.gor_pb                     = float(this.df_general_data['value']['gor_pb'])

        this.p_sep                      = float(this.df_general_data['value']['p_sep']) if 'p_sep' in this.df_general_data['value'].index else 100.
        this.temp_sep                   = float(this.df_general_data['value']['temp_sep']) if 'temp_sep' in this.df_general_data['value'].index else 60.
        this.api_specific_gravity       =141.5/(131.5+this.api_gravity)
        
        this.ml_caseno= None
        this.bo_caseno= None

        if (this.df_separator_corrected.empty and (not this.df_dv_oil.empty) and (not this.df_separator.empty)):
            this.calc_separator_corrected()

        if not this.df_separator_corrected.empty:
            this.df_separator_corrected.sort_values(by='pressure', ascending=False, inplace=True)
            gor= np.array(this.df_separator_corrected['gor'].astype(float).tolist())
            if math.isnan(this.df_general_data['value']['gor_pb']):
                this.gor_pb= max(gor[~np.isnan(gor)])
                this.df_general_data.loc['gor_pb', 'value']= this.gor_pb 

            if math.isnan(this.df_general_data['value']['pb_res_temp']):
                pressure= np.array(this.df_separator_corrected['pressure'].astype(float).tolist())
                this.pb_res_temp= pressure[np.where(gor == this.gor_pb)[0][0]]
                this.df_general_data.loc['pb_res_temp', 'value']= this.pb_res_temp 


            bubblePointPressureIdx   = (this.df_separator_corrected['pressure']-this.pb_res_temp).abs().argsort()[0]        
            this.pressure            = np.array(this.df_separator_corrected['pressure'].iloc[bubblePointPressureIdx:].astype(float).tolist())
            this.rs                  = np.array(this.df_separator_corrected['gor'].iloc[bubblePointPressureIdx:].astype(float).tolist())
            this.fvf                 = np.array(this.df_separator_corrected['bo_fvf'].iloc[bubblePointPressureIdx:].astype(float).tolist())
            #required for FVF computation
            this.input_abovebp_pres= np.array(this.df_separator_corrected['pressure'].iloc[0:bubblePointPressureIdx+1].astype(float).tolist())
            this.input_abovebp_rs  = np.repeat(this.df_separator_corrected['gor'].iloc[bubblePointPressureIdx].astype(float), len(this.input_abovebp_pres))
            this.input_abovebp_fvf = np.array(this.df_separator_corrected['bo_fvf'].iloc[0:bubblePointPressureIdx+1].astype(float).tolist())
            this.correct_gs_a_gravity()
            
            this.ml_caseno=1
        else:                
            if math.isnan(this.df_general_data['value']['pb_res_temp']):
                if not this.df_separator.empty:
                    this.calc_pb_res_temp_from_separator()
                else:
                    if not math.isnan(this.df_general_data['value']['gor_pb']):
                        #got to ml_caseno=2 and "calculate using bo_caseno=2"
                        this.pb_res_temp_bo_case2= "calculate using bo_caseno=2"
                    else:
                        this.logger.error('pb_res_temp not available in general_data, separator data is not available to compute it, gor_pb is also not aviable to compute pb_res_temp using correlations!')
                        exit(1)

            this.compSpecificGravityOfCrudeOil()
            if math.isnan(this.df_general_data['value']['gor_pb']):
                this.ml_caseno=4
            else:
                this.ml_caseno=2 #(or 3)


        #get case for black_oil:
        #user selected black_oil_corrleations for predictions.
        if (math.isnan(this.df_general_data['value']['gor_pb']) and not math.isnan(this.df_general_data['value']['pb_res_temp'])):
            this.bo_caseno=1 #calculate gor_pb using poas_ml_caseno4
        elif((not math.isnan(this.df_general_data['value']['gor_pb'])) and math.isnan(this.df_general_data['value']['pb_res_temp'])):
            this.bo_caseno=2 #calculate pb_res_temp using black_oil_correlations
        elif((not math.isnan(this.df_general_data['value']['gor_pb'])) and (not math.isnan(this.df_general_data['value']['pb_res_temp']))):
            this.bo_caseno=3 #original/revise pb_res_temp correlation output if user requested from general_data.bo_correlations_output
        else:
            this.logger("gor_pb or pb_res_temp is not available to process black_oil_correlations")
            sys.exit(1)
        if this.df_output_temps.empty:
            this.output_temps= np.array([60., 100., int(this.temp_res/2.), round(this.temp_res, 2)])
        else:
            this.output_temps= np.array(this.df_output_temps['Temperature'])
        this.logger.info("output_temps:%s" %(this.output_temps))

    def calc_gs_a_gravity_from_separator(this): #Seperator Test DATA
        gas_sp_gravity_calc, api_gravity_calc, = None, None
        try:
            df_separator1 = this.df_separator[1:]

            if len(df_separator1) <= 2 and df_separator1['gas_gravity'].isnull().values.any():
                this.logger.warn("Require min 2 values to compute missing gas_gravity")
                return
            if len(df_separator1) > 2 and df_separator1['gas_gravity'].isnull().values.any():
                if df_separator1['gas_gravity'][0:2].isnull().values.any():
                    this.logger.warn("Require initial 2 values to compute missing gas_gravity")
                    return
                else:
                    dataIdxes=[]
                    for index, row in df_separator1.iterrows(): 
                        if np.isnan(row['gas_gravity']):break
                        dataIdxes.append(index)
                    lnx= np.log(np.array(df_separator1['pressure'][dataIdxes]/df_separator1['temperature'][dataIdxes]))
                    lny= np.log(np.array(df_separator1['gas_gravity'][dataIdxes]))
                    mata= np.array([np.ones(len(lnx)), lnx]).T
                    matA= mata.T.dot(mata)
                    matB= np.linalg.inv(matA).dot(mata.T.dot(lny)) 
                    for index, row in df_separator1.iterrows(): 
                        if np.isnan(row['gas_gravity']): df_separator1.at[index, 'gas_gravity']= math.exp(matB[0]+matB[1]*math.log(df_separator1['pressure'][index]/df_separator1['temperature'][index]))
            gas_sp_gravity_calc= (df_separator1['gor']*df_separator1['gas_gravity']).sum()/df_separator1['gor'].sum()
            gas_sp_gravity_calc= this.gas_sp_gravity if((gas_sp_gravity_calc*0.99)<=this.gas_sp_gravity<=(gas_sp_gravity_calc*1.01)) else gas_sp_gravity_calc
            api_gravity_calc   = this.api_gravity if(((141.5/(float(df_separator1['liq_density'].iloc[-1])*0.9991026)-131.5)*0.99)<=this.api_gravity<=((141.5/(df_separator1['liq_density'].iloc[-1]*0.9991026)-131.5)*1.01)) else (141.5/(df_separator1['liq_density'].iloc[-1]*0.9991026)-131.5)
        except Exception as e:
            print "Error in calculation of gas_sp_gravity_calc, api_gravity_calc in SolutionGOR::calc_gs_a_gravity_from_separator , Error:%s" %(e.message)
        return gas_sp_gravity_calc, api_gravity_calc

    def define_region(this, vars, bound, mk_vars):
        lb, ub = list(), list()
        for i in range(len(vars)):
            lb.append(max(bound[i][0], (vars[i] - 4.*vars[i])))
            ub.append(min(bound[i][1], (vars[i] + 4.*vars[i])))
        a_max_l = list()
        for i in range(len(vars)):
            a_max_l.append( 0 if mk_vars[i] == 0 else ( ((vars[i] - ub[i]) / mk_vars[i]) if mk_vars[i] < 0 else ((vars[i] - lb[i]) / mk_vars[i]) ) )

        alpha_max= None
        if 1.5*min(a_max_l) < 10.**-50:
            alpha_max = 10.**-50
        elif 1.5*min(a_max_l) > 50.:
            alpha_max = 50.
        else:
            alpha_max = 1.5*min(a_max_l)
        alpha_min = alpha_max*0.001
        return alpha_max, alpha_min

    def converge_f(this, debug, logger, curr_iteration, grad):
        combined_grad= np.sqrt(grad[0]**2+grad[1]**2+grad[2]**2)
        #print "curr_iteration:", curr_iteration, "combined_grad:", combined_grad
        if combined_grad <= 10.**-3:
            if debug: this.logger.info("Rs converged by rule 1 combined gradient- %s" %(combined_grad))
            return True
        return False

    def objective(this, _vars, _a1, _a2, _a3, pr, rsr):
        #objective
        a1= _vars[0] * _a1
        a2= _vars[1] * _a2
        a3= _vars[2] * _a3
        rsr_calc= a1*pr**a2+(1.-a1)*pr**a3
        obj= (rsr-rsr_calc)**2 if(rsr == 0) else ((rsr -rsr_calc)/rsr)**2
        return obj, rsr_calc

    def _cost(this, vars, kwargs, print_r=False):
        _vars, _a1, _a2, _a3, pr, rsr= vars, kwargs[0], kwargs[1], kwargs[2], kwargs[3], kwargs[4]
        #objective
        a1= _vars[0] * _a1
        a2= _vars[1] * _a2
        a3= _vars[2] * _a3
        rsr_calc= a1*pr**a2+(1.-a1)*pr**a3
        obj= (rsr-rsr_calc)**2 if(rsr == 0) else ((rsr -rsr_calc)/rsr)**2
        #objective
        Diff_Rsr_Calc_var1= _a1*pr**a2-_a1*pr**a3
        Diff_Rsr_Calc_var2=  0. if pr==0 else a1*_a2*log(pr)*pr**a2 
        Diff_Rsr_Calc_var3= 0. if pr==0 else ((1.-a1)*_a3*log(pr)*pr**a3)
        diff_obj_var1= (2.*(rsr-rsr_calc)*(-1.)*Diff_Rsr_Calc_var1) if rsr==0 else (2.*(rsr-rsr_calc)*(-1)*Diff_Rsr_Calc_var1/rsr**2)
        diff_obj_var2= (2.*(rsr-rsr_calc)*(-1.)*Diff_Rsr_Calc_var2) if rsr==0 else (2.*(rsr-rsr_calc)*(-1)*Diff_Rsr_Calc_var2/rsr**2)
        diff_obj_var3= (2.*(rsr-rsr_calc)*(-1.)*Diff_Rsr_Calc_var3) if rsr==0 else (2.*(rsr-rsr_calc)*(-1)*Diff_Rsr_Calc_var3/rsr**2)
        return obj, np.array([diff_obj_var1, diff_obj_var2, diff_obj_var3])

    def plotGraphs(this, pr_inter, var_calcs, title, ylabel):
        plt.style.use('dark_background')
        fig, ax = plt.subplots()
        plt.scatter(pr_inter, var_calcs)        
        plt.plot(pr_inter, var_calcs, label=ylabel)        
        plt.xlabel('pressure')
        plt.ylabel(ylabel)
        plt.title(title)
        plt.legend()
        plt.tight_layout(pad=1.7)
        #plt.show()
        graphPath= os.path.join(getPOASRootDir(), 'Graphs', this.dbname, this.options.wellno, 'SolutionGOR_%s.png' %(title))
        if not os.path.exists(os.path.dirname(graphPath)):
            os.makedirs(os.path.dirname(graphPath))        
        fig.savefig(graphPath, dpi=fig.dpi)
        plt.cla()
        plt.close(fig)
        print "Graphs:%s" %(graphPath)

    def solve(this):
        pr_inter = np.arange(this.pressure[0], this.pressure[-1], -10. if this.pressure[0] <= 150 else -20)
        pr_inter= np.append(pr_inter, [this.pressure[-1]])
        rs_inter= monotone(this.pressure.tolist(), this.rs.tolist(), pr_inter.tolist())
        fvf_inter= monotone(this.pressure.tolist(), this.fvf.tolist(), pr_inter.tolist())
        
        pr= pr_inter/this.pressure[0]
        rsr= rs_inter/this.rs[0]       
        a1= 9.73*10.**-7.*(this.gas_sp_gravity**1.672608)*(this.api_gravity**0.92987)*(this.temp_res**0.247235)*(this.pb_res_temp**1.056052)
        a2= 0.022339*(this.gas_sp_gravity**(-1.00475))*(this.api_gravity**0.337711)*(this.temp_res**0.132795)*(this.pb_res_temp**0.302065)
        a3= 0.725167*(this.gas_sp_gravity**(-1.48548))*(this.api_gravity**(-0.164741))*(this.temp_res**(-0.09133))*(this.pb_res_temp**0.047094)
        this.vars     = np.ones(3) #3 iter_vars, inital guess values 1
        this.bounds   = np.array([[0.1, 10], [0.1, 10], [0.1, 10]]) # 3 bounds

        var_calcs, combined_grad=[], []
        for i in range(len(rsr)):
        	# if i == 0: continue
	        var_calc = ncg().minimize(this.vars, this.bounds, this._cost, this.logger, max_iter=300, debug=this.debug, region_f=this.define_region, converge_f=this.converge_f, args=(a1, a2, a3, pr[i], rsr[i]))
        	var_calcs.append(var_calc)
        	#not required:
        	obj, rsr_calc= this.objective(var_calc, a1, a2, a3, pr[i], rsr[i])
        	obj, grad= this._cost(var_calc, [a1, a2, a3, pr[i], rsr[i]])
        	combined_grad.append(np.sqrt(grad[0]**2+grad[1]**2+grad[2]**2))
        	# if i == 1: break

        var_calcs= np.array(var_calcs)
        # print "Pressure interpolated:"
       	# print pr_inter
        # print "Rs interpolated:"
        # print rs_inter
        # print "FVF interpolated:"
        # print fvf_inter
       	# print "Calculated iter_vars:"
        # print var_calcs #store in mongodb
        if this.debug: 
        	for i in range(var_calcs.shape[1]):
	        	this.plotGraphs(pr_inter, var_calcs[:,i], 'SolutionGOR_Var_%d' %(i+1), 'var_%d' %(i+1))
		        #3 scatter graph of x:this.pressure, y:valr_calcs[:,0], x:this.pressure, y:valr_calcs[:,1], x:this.pressure, y:valr_calcs[:,2], 
        return pr_inter, rs_inter, fvf_inter, var_calcs
        #print combined_grad

    def pushExcelInputinMongo(this, df, property):
        r= this.coll.remove({'Property':property})
        seq_no= 1
        for index, row in df.iterrows():
            d=row.to_dict()
            d['seq_no']= seq_no
            d['Property']= property
            d['CreatedAt']= this.dateYmdHMS
            this.coll.insert(d)
            seq_no += 1

    def readExcel(this):
        xls = pd.ExcelFile(this.excel)
        this.pushExcelInputinMongo(this.trimDfHeaders(pd.read_excel(xls, sheet_name='fluid_types')), 'fluid_types')
        this.pushExcelInputinMongo(this.trimDfHeaders(pd.read_excel(xls, sheet_name='general-data')), 'general-data')
        this.pushExcelInputinMongo(this.trimDfHeaders(pd.read_excel(xls, sheet_name='separator')), 'separator')
        this.pushExcelInputinMongo(this.trimDfHeaders(pd.read_excel(xls, sheet_name='multiple_bubble_points')), 'multiple_bubble_points')
        this.pushExcelInputinMongo(this.trimDfHeaders(pd.read_excel(xls, sheet_name='separator_corrected')), 'separator_corrected')
        this.pushExcelInputinMongo(this.trimDfHeaders(pd.read_excel(xls, sheet_name='poasml_revision_selected')), 'poasml_revision_selected')
        this.pushExcelInputinMongo(this.trimDfHeaders(pd.read_excel(xls, sheet_name='pb_correlations_selected')), 'pb_correlations_selected')
        this.pushExcelInputinMongo(this.trimDfHeaders(pd.read_excel(xls, sheet_name='bo_correlations_selected')), 'bo_correlations_selected')
        this.pushExcelInputinMongo(this.trimDfHeaders(pd.read_excel(xls, sheet_name='correlation_types_selected')), 'correlation_types_selected')
        this.pushExcelInputinMongo(this.trimDfHeaders(pd.read_excel(xls, sheet_name='dv-oil')), 'dv-oil')
        if 'output_temps' in xls.sheet_names: this.pushExcelInputinMongo(this.trimDfHeaders(pd.read_excel(xls, sheet_name='output_temps')), 'output_temps')
        #print this.df_fluid_types.fluid_type.bo

    def checkMandatoryFields(this):
        out= False
        if math.isnan(this.df_general_data['value']['temp_res']):
            this.logger.error("Please provide general_data.temp_res!")
            out= True
        if math.isnan(this.df_general_data['value']['api_gravity']):
            this.logger.info("Please provide general_data.api_gravity for SpecificGravityOfCrudeOil calculation!")
            out= True
        if not this.df_separator.empty:
            this.df_separator.sort_values(by='seq_no', inplace=True)
            if this.df_separator['stage'].iloc[-1] != 'Stock Tank Condition':
                this.logger.error("Last seq_no in separator.stage should be 'Stock Tank Condition'")
                out= True
            if this.df_separator['stage'].iloc[0] != 'Total at Bubble Point':
                this.logger.error("First seq_no in separator.stage should be 'Total at Bubble Point'")
                out= True
            if math.isnan(this.df_separator['liq_density'].iloc[-1]):
                this.logger.info("Please provide separator.liq_density for SpecificGravityOfCrudeOil calculation!")
                out= True
        if out: sys.exit(1)


    def readMongo(this):
        this.df_fluid_types              = pd.DataFrame(list(this.coll.find({'Property': 'fluid_types'}))).set_index('fluid_code').fillna(np.nan)
        this.df_general_data             = pd.DataFrame(list(this.coll.find({'Property': 'general-data'}))).set_index('param').fillna(np.nan)
        this.df_separator                = pd.DataFrame(list(this.coll.find({'Property': 'separator'}))).fillna(np.nan)
        this.df_separator_corrected      = pd.DataFrame(list(this.coll.find({'Property': 'separator_corrected'}))).fillna(np.nan)
        this.df_multiple_bubble_points   = pd.DataFrame(list(this.coll.find({'Property': 'multiple_bubble_points'}))).fillna(np.nan)
        this.df_poasml_revision_selected = pd.DataFrame(list(this.coll.find({'Property': 'poasml_revision_selected'}))).fillna(np.nan)
        this.df_pb_correlations_selected = pd.DataFrame(list(this.coll.find({'Property': 'pb_correlations_selected'}))).fillna(np.nan)
        this.df_bo_correlations_selected = pd.DataFrame(list(this.coll.find({'Property': 'bo_correlations_selected'}))).fillna(np.nan)
        this.df_correlation_types_selected = pd.DataFrame(list(this.coll.find({'Property': 'correlation_types_selected'}))).fillna(np.nan)
        this.df_dv_oil                     = pd.DataFrame(list(this.coll.find({'Property': 'dv-oil'}))).fillna(np.nan)
        this.df_output_temps               = pd.DataFrame(list(this.coll.find({'Property': 'output_temps'}))).fillna(np.nan)

    def unit_k_degf(this, d):
        return (d-273.15)*9./5.+32.

    def unit_degf_k(this, d):
        return ((5.*(d-32.))/9.)+273.15

    def unit_mpa_psi(this, d):
        return d*145.038

    def unit_psi_mpa(this, d):
        return d/145.038

    def unit_scum_scfstb(this, d):
        return d/0.178105620841002

    def unit_scfstb_scum(this, d):
        return d*0.178105620841002

    def convertDataUnit(this):                
        #this.df_general_data['value']['temp_res']     = this.unit_k_degf(float(this.df_general_data['value']['temp_res']))
        #this.df_general_data['value']['pb_res_temp']  = this.unit_mpa_psi(float(this.df_general_data['value']['pb_res_temp']))
        #this.df_general_data['value']['gor_pb']       = this.unit_scum_scfstb(float(this.df_general_data['value']['gor_pb']))

        this.df_general_data.loc['temp_res', 'value']     = this.unit_k_degf(float(this.df_general_data['value']['temp_res']))
        this.df_general_data.loc['pb_res_temp', 'value']  = this.unit_mpa_psi(float(this.df_general_data['value']['pb_res_temp']))
        this.df_general_data.loc['gor_pb', 'value']       = this.unit_scum_scfstb(float(this.df_general_data['value']['gor_pb']))


        if not this.df_separator.empty:
            this.df_separator['pressure']                 = this.unit_mpa_psi(this.df_separator['pressure'])
            this.df_separator['temperature']              = this.unit_k_degf(this.df_separator['temperature'])
            this.df_separator['gor']                      = this.unit_scum_scfstb(this.df_separator['gor'])
        if not this.df_separator_corrected.empty:
            this.df_separator_corrected['gor']            = this.unit_scum_scfstb(this.df_separator_corrected['gor'])
            this.df_separator_corrected['pressure']       = this.unit_mpa_psi(this.df_separator_corrected['pressure'])
        if not this.df_multiple_bubble_points.empty:
            this.df_multiple_bubble_points['temperature'] = this.unit_k_degf(this.df_multiple_bubble_points['temperature'])
            this.df_multiple_bubble_points['bubble_point']= this.unit_mpa_psi(this.df_multiple_bubble_points['bubble_point'])
        if not this.df_dv_oil.empty:
            this.df_dv_oil['pressure']                    = this.unit_mpa_psi(this.df_dv_oil['pressure'])
            this.df_dv_oil['gor']                         = this.unit_scum_scfstb(this.df_dv_oil['gor'])


    def getUniqueOutputCase(this, flag='ml'):
        outputCase= this.correlation_type_selected
        ui_title= this.correlation_type_selected_name
        if flag == 'ml' and hasattr(this,'pb_correlation'):
            outputCase += '_%s' %(this.pb_correlation)
            ui_title += ' %s' %(this.pb_correlation_name)
        elif(flag == 'bo' and hasattr(this,'bo_correlation')):
            outputCase += '_%s' %(this.bo_correlation)
            ui_title += ' %s' %(this.bo_correlation_name)
        if(flag == 'ml' and hasattr(this,'poasml_revision_selected')):
            outputCase += '_%s' %(this.poasml_revision_selected)
            ui_title += ' %s' %(this.poasml_revision_selected_name)
        outputCase += '_case%s' %(this.caseno)
        return outputCase, ui_title

    def convertDataUnitGORBack(this, alltemp_pres_rs_fvf):
        alltemp_pres_rs_fvf1={}
        for temp in alltemp_pres_rs_fvf:
            temp1= '%s' %(str(this.unit_degf_k(float(temp.replace('_', '.')))).replace('.', '_'))
            if temp1 not in alltemp_pres_rs_fvf1.keys(): alltemp_pres_rs_fvf1[temp1]={}
            if 'Pressure' in alltemp_pres_rs_fvf[temp].keys(): 
                alltemp_pres_rs_fvf1[temp1]['Pressure']=this.unit_psi_mpa(np.array(alltemp_pres_rs_fvf[temp]['Pressure'])).tolist()
            if 'Rs' in alltemp_pres_rs_fvf[temp].keys():
                alltemp_pres_rs_fvf1[temp1]['Rs']= this.unit_scfstb_scum(np.array(alltemp_pres_rs_fvf[temp]['Rs'])).tolist()
            if 'FVF' in alltemp_pres_rs_fvf[temp].keys():
                alltemp_pres_rs_fvf1[temp1]['FVF']= alltemp_pres_rs_fvf[temp]['FVF']
            if 'output_temps_seq_no' in alltemp_pres_rs_fvf[temp].keys():
                alltemp_pres_rs_fvf1[temp1]['output_temps_seq_no']= alltemp_pres_rs_fvf[temp]['output_temps_seq_no']
        return alltemp_pres_rs_fvf1

    def updateMongo(this, temp, bppCalculated, rs, multiplier, s, c_obs, alltemp_pres_rs_fvf, error=None):
        bppCalculated= this.unit_psi_mpa(bppCalculated)
        temp= this.unit_degf_k(temp)
        alltemp_pres_rs_fvf= this.convertDataUnitGORBack(alltemp_pres_rs_fvf)
        d={}
        caseno, casetitle= this.getUniqueOutputCase()
        d['Property']= 'SolutionGOR'
        d['OutputCase']= caseno
        d['GuiTitle']= casetitle
        d['CaseNo']= this.caseno
        d['CreatedAt']= this.dateYmdHMS
        d['Temp']=temp.tolist()
        # d['BppDb']=bppDb.tolist()
        d['BppCalculated']=bppCalculated.tolist()
        d['Rs']=rs.tolist()
        d['Multipler']=multiplier.tolist()
        d['AboveBP_S']=s.tolist()
        d['AboveBP_C_Observed']=c_obs.tolist()
        d['Alltemp_pres_rs_fvf']=alltemp_pres_rs_fvf
        d['Alltemp_pres_rs_fvf_inp']= {'Pressure': this.unit_psi_mpa(np.append(this.input_abovebp_pres, this.pressure[1:])).tolist(), 'Rs': this.unit_scfstb_scum(np.append(this.input_abovebp_rs, this.rs[1:])).tolist(), 'FVF': np.append(this.input_abovebp_fvf, this.fvf[1:]).tolist()}
        d['Error']= error 
        #d= {'Property':'SolutionGOR', 'CreatedAt':datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S'), 'Rs': rs.tolist(), 'Multipler':multiplier.tolist(), 'AboveBP_S':s.tolist(), 'AboveBP_C_Observed':c_obs.tolist(), 'Alltemp_pres_rs_fvf':alltemp_pres_rs_fvf}
        this.coll.update_many({'Property':'SolutionGOR', 'OutputCase': caseno}, {'$set': d}, **{'upsert':True})

    def updateMongo23(this, temp, bppCalculated, alltemp_pres_rs_fvf, flag='ml', error=None):
        bppCalculated= this.unit_psi_mpa(bppCalculated)
        temp= this.unit_degf_k(temp)
        alltemp_pres_rs_fvf= this.convertDataUnitGORBack(alltemp_pres_rs_fvf)
        
        d={}
        caseno, casetitle= this.getUniqueOutputCase(flag)
        d['Property']= 'SolutionGOR'
        d['OutputCase']= caseno
        d['GuiTitle']= casetitle
        d['CaseNo']= this.caseno
        d['CreatedAt']= this.dateYmdHMS
        d['Temp']=temp.tolist()
        # d['BppDb']=bppDb.tolist()
        d['BppCalculated']=bppCalculated.tolist()
        d['Alltemp_pres_rs_fvf']=alltemp_pres_rs_fvf
        d['Error']= error
        #d['Alltemp_pres_rs_fvf_inp']= {'Pressure': np.append(this.input_abovebp_pres, this.pressure[1:]).tolist(), 'Rs': np.append(this.input_abovebp_rs, this.rs[1:]).tolist(), 'FVF': np.append(this.input_abovebp_fvf, this.fvf[1:]).tolist()}
        #d= {'Property':'SolutionGOR', 'CreatedAt':datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S'), 'Rs': rs.tolist(), 'Multipler':multiplier.tolist(), 'AboveBP_S':s.tolist(), 'AboveBP_C_Observed':c_obs.tolist(), 'Alltemp_pres_rs_fvf':alltemp_pres_rs_fvf}
        this.coll.update_many({'Property':'SolutionGOR', 'OutputCase': caseno}, {'$set': d}, **{'upsert':True})

    def print_alltemp_pres_rs_fvf(this, case=1):
        outd=  os.path.join(cwd, 'output')
        if not os.path.isdir(outd): os.mkdir(outd)
        this.coll= this.db[this.options.wellno] #using PBO
        outf= "output/SolutionGOR_output_%s.xlsx" %(os.path.basename(this.options.wellno)) 
        outputCases= this.coll.find({'Property': 'SolutionGOR'})
        writer = pd.ExcelWriter(outf, engine='xlsxwriter') 
        wb  = writer.book
        for outputCase in outputCases:
            print "Printing %s" %(outputCase['OutputCase'])
            sheet_name= outputCase['OutputCase'][-31:]
            caseno= outputCase['CaseNo']
            df= pd.DataFrame(list(this.coll.find({'Property': 'SolutionGOR', 'OutputCase': outputCase['OutputCase']})))
            col=0
            pd.DataFrame({'Temp':df['Temp'][0]}).to_excel(writer, sheet_name=sheet_name, startrow=0, startcol=col, index=False)
            col += 1
            pd.DataFrame({'BppCalculated':df['BppCalculated'][0]}).to_excel(writer, sheet_name=sheet_name, startrow=0, startcol=col, index=False)
            col += 2

            if df['Error'][0] is not None:
                pd.DataFrame({'Error': df['Error']}).to_excel(writer, sheet_name=sheet_name, startrow=0, startcol=col, index=False)
                col += 5

            if re.search('poas_ml', sheet_name)  and caseno == 1:
                pd.DataFrame({'Multipler':df['Multipler'][0]}).to_excel(writer, sheet_name=sheet_name, startrow=0, startcol=col, index=False)
                col += 1
                pd.DataFrame({'Rs':df['Rs'][0]}).to_excel(writer, sheet_name=sheet_name, startrow=0, startcol=col, index=False)
                col += 2
                pd.DataFrame({'AboveBP_S':df['AboveBP_S'][0]}).to_excel(writer, sheet_name=sheet_name, startrow=0, startcol=col, index=False)
                col += 1
                pd.DataFrame({'AboveBP_C_Observed':df['AboveBP_C_Observed'][0]}).to_excel(writer, sheet_name=sheet_name, startrow=0, startcol=col, index=False)
                col += 2
                pd.DataFrame({'Input_Pressure':df['Alltemp_pres_rs_fvf_inp'][0]['Pressure']}).to_excel(writer, sheet_name=sheet_name, startrow=0, startcol=col, index=False)
                col += 1
                pd.DataFrame({'Input_Rs':df['Alltemp_pres_rs_fvf_inp'][0]['Rs']}).to_excel(writer, sheet_name=sheet_name, startrow=0, startcol=col, index=False)
                col += 1
                pd.DataFrame({'Input_FVF':df['Alltemp_pres_rs_fvf_inp'][0]['FVF']}).to_excel(writer, sheet_name=sheet_name, startrow=0, startcol=col, index=False)
                col += 2
            else:
                pd.DataFrame(list(this.coll.find({'Property': 'general-data'}))).set_index('param')['value'].to_excel(writer, sheet_name=sheet_name, startrow=0, startcol=col, index=True)
                col += 3

            #for temp, value in sorted(data_dict(df['Alltemp_pres_rs_fvf'][0]).items(), key=lambda item: float(item[0].replace('_', '.'))):
            #for temp in sorted(data_dict(df['Alltemp_pres_rs_fvf'][0]).keys()):
            for temp, value in sorted(dict(df['Alltemp_pres_rs_fvf'][0]).items(), key=lambda t: t[1]['output_temps_seq_no']):

                pd.DataFrame({'%s->' %(temp):''}, index=[0]).to_excel(writer, sheet_name=sheet_name, startrow=0, startcol=col, index=False)
                pd.DataFrame(df['Alltemp_pres_rs_fvf'][0][temp]).to_excel(writer, sheet_name=sheet_name, startrow=1, startcol=col, index=False)

                chart = wb.add_chart({'type': 'scatter'})
                if re.search('poas_ml', sheet_name) and caseno == 1:
                    chart.add_series({
                        'name':       'RefTemp',
                        'categories': '=%s!$J$2:$J$%s' %(sheet_name, len(df['Alltemp_pres_rs_fvf_inp'][0]['Pressure'])+1),
                        'values':     '=%s!$L$2:$L$%s' %(sheet_name, len(df['Alltemp_pres_rs_fvf_inp'][0]['FVF'])+1),
                    })

                if re.search('poas_ml', sheet_name) and ( case == 2 or case ==3 or case == 4):
                    chart.add_series({
                        'name':       'RefTemp',
                        'categories': '=%s!$D$2:$D$%s' %(sheet_name, len(df['Alltemp_pres_rs_fvf_inp'][0]['Pressure'])+1),
                        'values':     '=%s!$F$2:$F$%s' %(sheet_name, len(df['Alltemp_pres_rs_fvf_inp'][0]['FVF'])+1),
                    })
                
                cl = get_column_letter(col+2) #one extra for index column
                cl2= get_column_letter(col+1)

                chart.add_series({
                    'name':       '=%s!$%s$1' %(sheet_name, cl2),
                    'categories': '=%s!$%s$3:$%s$%s' %(sheet_name, cl,  cl,  len(df['Alltemp_pres_rs_fvf'][0][temp]['Pressure'])+2),
                    'values':     '=%s!$%s$3:$%s$%s' %(sheet_name, cl2, cl2, len(df['Alltemp_pres_rs_fvf'][0][temp]['FVF'])+2),
                })
                chart.set_title ({'name': temp})
                worksheet = writer.sheets[sheet_name]                
                worksheet.insert_chart('%s15' %(cl), chart, {'x_offset': 25, 'y_offset': 10})
                col += 5
        wb.close()
        print("Output report:%s" %(outf))

    def get_gor_pb_from_pb_correlations(this):
        gor_pb, yg=Correlations(this).rs_atCorrelations(this.pb_correlation, this.pb_res_temp, this.temp_res)        
        return gor_pb

    def solution_poas_ml(this):
        for idx, row in this.df_poasml_revision_selected.iterrows():
            this.poasml_revision_selected= row['id']
            this.poasml_revision_selected_name= row['name']
            
            temp, bppCalculated, data= None, None, None
            if this.caseno == 1:
                try:
                    pr_inter, rs_inter, fvf_inter, var_calcs= this.solve()
                    ob= SolutionGORBubblePoint(this)
                    temp, bppCalculated= ob.solve_df1(this.df_fluid_types,this.df_general_data,this.df_separator,this.df_separator_corrected,this.df_multiple_bubble_points,this.fluid_code, this.logger, this.debug)
                    
                    ob= RsTempPressure(this)
                    data= ob.solve1(pr_inter, rs_inter, var_calcs.T[0], var_calcs.T[1], var_calcs.T[2], temp, bppCalculated, this.gor_pb, this.logger, this.debug)
                    
                    ob= FormationVolumeFactor(this)
                    rs, multiplier, s, c_obs, alltemp_pres_rs_fvf= ob.solve1(pr_inter, rs_inter, fvf_inter, data)
                    this.updateMongo(temp, bppCalculated, rs, multiplier, s, c_obs, alltemp_pres_rs_fvf)
                except:
                    error= '%s is not valid for this case!' %(this.poasml_revision_selected)
                    this.updateMongo(np.array([]), np.array([]), np.array([]), np.array([]), np.array([]), np.array([]), [], error)

            if this.caseno == 2 or this.caseno == 3: 
                if hasattr(this,'pb_res_temp_bo_case2'): 
                    this.logger.info("pb_res_temp is not available, calculating it using bo_caseno=2")
                    for idx, raw in this.df_pb_correlations_selected.iterrows():
                        this.pb_correlation= raw['pb_correlation_id']
                        this.pb_correlation_name= raw['pb_correlation_name']
                        this.logger.info("Started pb_correlations_selected:%s" %(this.pb_correlation))
                        temp, bppCalculated, alltemp_pres_rs_fvf= np.array([]), np.array([]), []
                        try:
                            if this.pb_correlation == 'dindoruk': 
                                for t in this.output_temps: 
                                    if t <= 60:  raise Exception('dindoruk is invalid for temps <= 60!')

                            this.pb_res_temp= Correlations(this).pb_res_temp_atCorrelations(this.pb_correlation, this.gor_pb, this.temp_res)
                            this.df_general_data.loc['pb_res_temp', 'value']= this.pb_res_temp

                            temp, bppCalculated= SolutionGORBubblePoint(this).solve_df23(this.df_general_data,this.df_multiple_bubble_points,this.fluid_code, this.logger, this.debug)
                            ob= RsTempPressure(this)
                            data= ob.solve23(temp, bppCalculated)
                            ob= FormationVolumeFactor(this)
                            alltemp_pres_rs_fvf= ob.solve23(data)
                            this.updateMongo23(temp, bppCalculated, alltemp_pres_rs_fvf, 'ml')
                        except:
                            error= '%s is not valid for this case!' %(this.pb_correlation)
                            this.updateMongo23(temp, bppCalculated, alltemp_pres_rs_fvf, 'ml', error)

                        this.logger.info("Completed pb_correlations_selected:%s" %(this.pb_correlation))

                else:
                    temp, bppCalculated, alltemp_pres_rs_fvf= np.array([]), np.array([]), []
                    try:
                        temp, bppCalculated= SolutionGORBubblePoint(this).solve_df23(this.df_general_data,this.df_multiple_bubble_points,this.fluid_code, this.logger, this.debug)
                        ob= RsTempPressure(this)
                        data= ob.solve23(temp, bppCalculated)                    
                        ob= FormationVolumeFactor(this)
                        alltemp_pres_rs_fvf= ob.solve23(data)
                        this.updateMongo23(temp, bppCalculated, alltemp_pres_rs_fvf, 'ml')
                    except Exception as e:
                        this.logger.error(e.message)
                        error= '%s is not valid for this case!' %(this.poasml_revision_selected)
                        this.updateMongo23(temp, bppCalculated, alltemp_pres_rs_fvf, 'ml', error)


            if this.caseno == 4:
                #calculate gor_pb using pb_correlations first.
                for idx, raw in this.df_pb_correlations_selected.iterrows():
                    temp, bppCalculated, alltemp_pres_rs_fvf= np.array([]), np.array([]), []
                    this.pb_correlation= raw['pb_correlation_id']
                    this.pb_correlation_name= raw['pb_correlation_name']
                    try:
                        if this.pb_correlation == 'dindoruk': 
                            for t in this.output_temps: 
                                if t <= 60:  raise Exception('dindoruk is invalid for temps <= 60!')

                        this.gor_pb= this.get_gor_pb_from_pb_correlations()
                        #this.df_general_data['value']['gor_pb']= gor_pb
                        this.df_general_data.loc['gor_pb', 'value']= this.gor_pb

                        ob= SolutionGORBubblePoint(this)
                        temp, bppCalculated= ob.solve_df23(this.df_general_data,this.df_multiple_bubble_points,this.fluid_code, this.logger, this.debug)
                    
                        ob= RsTempPressure(this)
                        data= ob.solve23(temp, bppCalculated)
                    
                        ob= FormationVolumeFactor(this)    
                        alltemp_pres_rs_fvf= ob.solve23(data)
                        this.updateMongo23(temp, bppCalculated, alltemp_pres_rs_fvf, 'ml')
                    except:
                        error= '%s is not valid for this case!' %(this.pb_correlation)
                        this.updateMongo23(temp, bppCalculated, alltemp_pres_rs_fvf, 'ml', error)


    def solution_black_oil_correlations(this):
        for idx, raw in this.df_bo_correlations_selected.iterrows():
            this.bo_correlation= raw['bo_correlation_id']
            this.bo_correlation_name= raw['bo_correlation_name']
            print "Stared:", this.bo_correlation
            try:
                if this.bo_correlation == 'dindoruk': 
                    for t in this.output_temps: 
                        if t <= 60:  raise Exception('dindoruk is invalid for temps <= 60!')
                ob= RsTempPressureBOC(this)
                data= ob.main(this.bo_correlation)
                ob= FormationVolumeFactorBOC(this)    
                alltemp_pres_rs_fvf= ob.main(data, this.bo_correlation)
                #print alltemp_pres_rs_fvf
                this.updateMongo23(data['TempDb'], np.array(data['BPPCalc']), alltemp_pres_rs_fvf, 'bo')
                print "Completed:", this.bo_correlation
            except:
                error= '%s is not valid for this case!' %(this.bo_correlation)
                this.updateMongo23(np.array([]), np.array([]), [], 'bo', error)

    def cleanupMongoOutput(this):
        r= this.coll.remove({'Property':'SolutionGOR'})


    def solve_excel_db(this):
        if this.options.copymasterdata: this.copyMasterData()
        if this.options.excel is not None: this.readExcel() #this will be migrated to readMongo()
        this.readMongo() #using MBP
        this.checkMandatoryFields()
        this.convertDataUnit() #using MBP
        this.input_data()
        this.cleanupMongoOutput()
        for idx, row in this.df_correlation_types_selected.iterrows():
            this.correlation_type_selected  = row['id']
            this.correlation_type_selected_name  = row['Name']
            if this.correlation_type_selected == 'poas_ml':
                this.caseno= this.ml_caseno
                this.logger.info("Started correlation_type_selected:%s, caseno:%s" %(this.correlation_type_selected, this.caseno))
                this.solution_poas_ml()
                this.logger.info("Completed correlation_type_selected:%s, caseno:%s" %(this.correlation_type_selected, this.caseno))
            else:
                this.caseno= this.bo_caseno
                this.logger.info("Started correlation_type_selected:%s, caseno:%s" %(this.correlation_type_selected, this.caseno))
                this.solution_black_oil_correlations()
                this.logger.info("Completed correlation_type_selected:%s, caseno:%s" %(this.correlation_type_selected, this.caseno))

if __name__ == "__main__":
    obj = rssolver(dbName='PBO')
    if obj.options.skipcomputation:
        pass
    else:
        obj.solve_excel_db()
    if obj.options.printoutput:
        obj.print_alltemp_pres_rs_fvf()

    #logger= getLogger(obj.dbName, 'Well1')
    #obj.solve_df(obj.df_fluid_types,obj.df_general_data,obj.df_separator,obj.df_separator_corrected, logger, debug=True)
    
    #Usage: 
    
    #For excel input:
    #python SolutionGOR.py -w AB8 -t 'bo' --excel
    
    #For dataframe input:
    # obj = rssolver(dbName='PBO')
    # obj.solve_df(obj.df_fluid_types,obj.df_general_data,obj.df_separator,obj.df_separator_corrected, logger, debug=True)

#python SolutionGOR.py -w ALUMM -c 1 -e Reqs/ALUMM/ALUMM-ganesh.xlsx
