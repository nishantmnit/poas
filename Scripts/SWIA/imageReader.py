import sys, re, os, argparse, getopt, datetime, time, matplotlib
from PIL import Image
import numpy as np
from collections import OrderedDict 
matplotlib.use('WebAgg')
#WebAgg,  MacOSX
from mpl_toolkits import mplot3d 
import numpy as np 
import matplotlib.pyplot as plt 
import matplotlib.image as mpimg
from matplotlib import colors
from math import radians, cos
import matplotlib.cm as cm

cwd = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(0, "%s/../../Lib" %(cwd))

from Utils import *
#Base colors

class imageReader(object):
	def __init__(this):
		this.logger= getLogger('imageReader', 'RTS')
		this.getBaseColors()
		this.getoptions()
		this.dateYmdHms= datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')

	def getBaseColors(this):
		this.base_colors= np.array([[0,0,0], [255,255,255], [255,0,0], [0,255,0], [0,0,255], [255,255,0], [0,255,255], [255,0,255], [192,192,192], [128,128,128], [128,0,0], [128,128,0], [0,128,0], [128,0,128], [0,128,128], [0,0,128]])
		this.base_colors_name= np.array(['black', 'white', 'red', 'lime', 'blue', 'yellow', 'cyan', 'magenta', 'silver', 'gray', 'maroon', 'olive', 'green', 'purple', 'teal', 'navy'])

		# this.base_colors= np.array([[0,0,0], [255,0,0], [0,0,255], [0,128,0]])
		# this.base_colors_name= np.array(['black', 'red', 'blue', 'green'])		
		this.base_colors= np.array([[0, 255, 164], [0, 238, 23], [255, 255, 255], [255, 0, 0], [0, 255, 255], [239, 229, 104], [223, 213, 87], [227, 195, 67], [215, 161, 21], [199, 145, 27], [186, 110, 0], [170, 93, 4], [154, 76, 0], [121, 42, 0], [88, 2, 0], [53, 13, 0], [0, 0, 0]])
		this.base_colors_name= np.array(['color1','color2','color3','color4','color5','color6','color7','color8','color9','color10','color11','color12','color13','color14','color15','color16','color17'])
		this.mrayl= [-1500.00,-1000.00,-500,0.30,2.60,3.00,3.50,4.00,4.50,5.00,5.50,6.00,6.50,7.00,7.50,8.00,8.50]

	def getoptions(this):
		parser = argparse.ArgumentParser()
		parser.add_argument("--x_start",	 	dest='x_s',	   			default=0., 	type=float,  	help='X axis start position')
		parser.add_argument("--y_start",	 	dest='y_s',	   			default=0., 	type=float,  	help='Y axis start position')
		parser.add_argument("--x_end",		 	dest='x_e',	   			default=0., 	type=float,   	help='X axis end position  ')
		parser.add_argument("--y_end",		 	dest='y_e',	   			default=0., 	type=float,  	help='Y axis end position  ')
		parser.add_argument("--casingdiameter",	dest='casingdiameter',	default=None, 	type=float,  	help='Casing Diameter in inches')
		parser.add_argument("--getcolor",	 	dest='getcolor',		default=None,   help='comma separated colors from:%s' %(this.base_colors_name))
		parser.add_argument("--image", 		 	dest="image",	 		default=None, 	help="image location")
		parser.add_argument("--debug",		 	dest="debug",	 		help="print debug messages", action='store_true')
		this.args = parser.parse_args()
		if this.args.image is None or this.args.casingdiameter is None:
			print "Usage: python imageReader.py --image <image location> --casingdiameter <float>"
			sys.exit(1)

		if this.args.getcolor is not None:
			this.args.getcolor= this.args.getcolor.lower()
			for c in this.args.getcolor.split(','):
				if (c.lower() not in this.base_colors_name):
					print "Please provide valid color name from: " %(this.base_colors_name)
					sys.exit(1)
	#255,255,254
	def pixel_closest(this, c):
		color= np.array(c)
		distances = np.sqrt(np.sum((this.base_colors-color)**2,axis=1))
		index_of_smallest = np.where(distances==np.amin(distances))
		nearest_color = this.base_colors[index_of_smallest[0][0]]
		return tuple(nearest_color)

	def update_pixel(this):
		#UPDATE PIXELS:
		this.list_of_pixels_updated = map(this.pixel_closest, this.list_of_pixels)
		this.pim.putdata(this.list_of_pixels_updated)
		#this.pim.show()
		if this.args.debug: this.logger.info("Pixels updated to closest pixel of base colors")
		#im = np.apply_along_axis(update_pixel, 2, im)
		#OR
		# width, height, x = im.shape
		# for i in range(width):
		# 	for j in range(len(im[i])):
		# 		#print 'i:', i, 'j:', j, ',', im[i][j]
		# 		# if i == 2 and j == 334:
		# 		im[i][j]= closest(im[i][j])
		# 		#rint "closest:", im[i][j]

	def getImage(this):
		# Load image, ensure not palettised, and make into Numpy array
		if this.args.debug: this.logger.info("Input image file:%s" %(this.args.image))

		#cv2_pixels= cv2.imread(this.args.image)[:, :, ::-1] #convert from BGR to RGB
		#print type(cv2_pixels)

		this.pim = Image.open(this.args.image).convert('RGB')
		this.list_of_pixels = list(this.pim.getdata())
		#print "Total Pixels in image:", len(this.list_of_pixels)
		# im1= np.unique(im.reshape(-1, im.shape[2]), axis=0)
		# print "unique shape:"
		# print im1.shape

	def getxy(this):
		pim_arr  = np.array(this.pim)
		x_e= this.args.x_s + pim_arr.shape[0]-1 if this.args.x_e == 0 else this.args.x_e
		y_e= this.args.y_s + pim_arr.shape[1]-1 if this.args.y_e == 0 else this.args.y_e
		this.x=np.linspace(this.args.x_s, x_e, pim_arr.shape[0], True) - 180.
		this.y=np.linspace(this.args.y_s, y_e, pim_arr.shape[1], True)
		#print "x, y shape:", this.x.shape, this.y.shape
		# Get X and Y coordinates of all blue pixels
		# Define the blue colour we want to find - PIL uses RGB ordering
		# red = [118, 225, 22]
		# red= [2, 2, 19]
		# X,Y = np.where(np.all(im==red,axis=2))

	def getUniqPixelsLen(this, list_of_pixels):
		pim_arr= np.array(this.pim)
		this.unique_colors= list(dict.fromkeys(list_of_pixels))
		if this.args.debug: this.logger.info("Number of Unique Pixels in image:%s, y/x shape:%s" %(len(this.unique_colors), pim_arr.shape))

	def printUniqPixels(this):
		pim_arr= np.array(this.pim)
		#this.unique_colors= np.unique(pim_arr.reshape(-1, pim_arr.shape[2]), axis=0)
		#this.unique_colors= list(data_dict.fromkeys(list_of_pixels))
		up= []
		width, height, x = pim_arr.shape
		for i in range(width):
			for j in range(len(pim_arr[i])):
				p= ','.join(map(str, pim_arr[i][j]))
				if p not in up: up.append(p)
		up= [ map(int, p.split(',')) for p in up]
		up_names= this.base_colors_name[[np.where(np.all(this.base_colors == u, axis=1))[0][0] for u in up]]
		if this.args.debug: this.logger.info("Unique Pixels in image:%s" %(up))
		if this.args.debug: this.logger.info("Unique Pixels in image:%s" %(up_names))

	def getUniqPixelsName(this):
		this.unique_colors_names= this.base_colors_name[[np.where(np.all(this.base_colors == u, axis=1))[0][0] for u in this.unique_colors]]
		if this.args.debug: this.logger.info("Unique colors in image:%s" %(this.unique_colors_names))
		# im1= np.unique(im.reshape(-1, im.shape[2]), axis=0)
		# print "unique shape1:"
		# for i1 in im1:
		# 	for b1 in range(len(base_colors)):
		# 		if np.array_equal(i1, base_colors[b1]):
		# 			print this.base_colors_name[b1]
		# print im1[0:100]
		# print im1.shape

	def getmraylRGB(this, m):
		mv= None
		try:
			mv= this.mraylRGB[','.join(map(str, m))]
		except:
			mv= 'NotFound'
			print mv
		return mv

	def setmraylRGB(this):
		this.mraylRGB=OrderedDict()
		for i in range(len(this.base_colors)):
			rgb=','.join(map(str, this.base_colors[i]))
			mrayl= this.mrayl[i]
			this.mraylRGB[rgb]= mrayl
		print this.mraylRGB

	def getCsv(this):
		this.setmraylRGB()
		X, Y, Z, mraylRGB= [], [], [], []
		outputFile= "graph_coordinates_%s.csv" %(os.path.basename(this.args.image))
		f = open(outputFile, "w")
		pim_arr= np.array(this.pim)
		f.write("y, x, pixel_RGB, mraylValue, z_radius\n")
		casing_radius= this.args.casingdiameter/2.
		if this.args.getcolor is not None:
			Z= [[] for i in range(pim_arr.shape[0])]
			for c in this.args.getcolor.split(','):
				getcolor_rgb= this.base_colors[np.where(this.base_colors_name == c.lower())]
				x,y= np.where(np.all(pim_arr == getcolor_rgb, axis=2))
				z_radius= abs(casing_radius*cos(radians(this.x[x[i]])))
				for i in range(len(x)): #x and y of same range hence one loop
					f.write("%s,%s,%s,%s,%s\n" %(this.y[y[i]],this.x[x[i]], pim_arr[x[i]][y[i]], this.getmraylRGB(pim_arr[x[i]][y[i]]), z_radius))
					#Z[x[i]].append(this.getmraylRGB(pim_arr[x[i]][y[i]])) #wrong code. First extract all x,y and z (i.e. below code in else part and then make z=0 zero where pim_arr[x[i]][y[i]]= c
		else:
			for i in range(pim_arr.shape[0]): #y
				Z.append([])
				mraylRGB.append([])
				for j in range(len(pim_arr[i])): #x
					mv= this.getmraylRGB(pim_arr[i][j])
					z_radius= abs(casing_radius*cos(radians(this.x[i])))
					f.write("%s,%s,%s,%s,%s\n"%(this.y[j],this.x[i],pim_arr[i][j], mv, z_radius))
					Z[i].append(z_radius)
					mraylRGB[i].append(pim_arr[i][j])
					# if abs(mv) >= 500:
					# 	Z[i].append(0.)
					# else:
					# 	Z[i].append(mv)
					#print this.y[j],",", this.x[i],",", mv
		this.logger.info("Graph co-ordinates are extracted into:%s" %(outputFile))
		return this.y, this.x, np.array(Z), np.array(mraylRGB)/255.  # Z has y,x

		# (Pdb) p time.shape
		# (512,)
		# (Pdb) p coefs.shape
		# (106, 512)
		# (Pdb) p frequencies.shape
		# (106,)
		# (Pdb) 

	def getGraphColorMap(this):
		cmap = colors.ListedColormap((this.base_colors/255.).tolist())
		bounds=this.mrayl[:]
		bounds.insert(0, -2000)
		norm = colors.BoundaryNorm(bounds, cmap.N)
		return cmap, bounds, norm

	def plotme(this, x, z, y, mraylRGB, graphName, Identifier, collection):
		graphPath= os.path.join(getPOASRootDir(), 'Graphs', collection, "%s.png" %(graphName))
		if not os.path.exists(os.path.dirname(graphPath)):
			os.makedirs(os.path.dirname(graphPath))
		plt.style.use('dark_background')
		fig = plt.figure(figsize=(10,5))
		cmap, bounds, norm= this.getGraphColorMap()
		#SURFACE
		ax = fig.gca(projection='3d') #for 3D
		ax.set_title(graphName, color='white')
		ax.tick_params(axis='x')
		ax.tick_params(axis='y')
		ax.tick_params(axis='z')
		t= np.tile(x, (len(z), 1))
		cc= z
		temp, f = np.meshgrid(np.arange(0, len(z[0])), y)
		ax.set_xlabel('X')
		ax.set_ylabel('Y')
		ax.set_zlabel('Z-Radius')
		ax = fig.gca(projection='3d')
		#surf = ax.plot_surface(t, f, cc,  cmap=cmap, norm=norm)
		#fig.colorbar(surf, cmap=cmap, norm=norm, boundaries=bounds, ticks=bounds, format="%.1f") #shrink=0.5, aspect=10, 
		print "mraylRGB shape:", mraylRGB.shape
		print "cc shape:", cc.shape
		#surf = ax.plot_surface(f, t, cc, facecolors=mpimg.imread('test.png'), linewidth=0, rstride=2, cstride=2, antialiased=True, shade=False)
		surf = ax.plot_surface(f, t, cc, facecolors=mraylRGB, linewidth=0, rstride=1, cstride=1, antialiased=False, shade=False)
		m = cm.ScalarMappable(cmap=cmap, norm=norm)
		m.set_array([])
		fig.colorbar(m, boundaries=bounds, ticks=bounds, format="%.1f")
		#fig.colorbar(surf, shrink=0.5, aspect=10) #shrink=0.5, aspect=10, 
		ax.view_init(elev=16, azim=-154)	 
		plt.tight_layout(pad=1.7)
		plt.show()


		#CONTOUR
		# ax = fig.gca() #for 2D contour
		# ax.set_title(graphName, color='white')
		# ax.tick_params(axis='x')
		# ax.tick_params(axis='y')
		# ax.tick_params(axis='z')
		# t= np.tile(x, (len(z), 1))
		# cc= z
		# temp, f = np.meshgrid(np.arange(0, len(z[0])), y)
		# ax.set_xlabel('X')
		# ax.set_ylabel('Y')
		# cp = ax.contourf(t, f, cc, cmap=cmap, norm=norm)
		# cbar= fig.colorbar(cp, cmap=cmap, norm=norm, boundaries=bounds, ticks=bounds, format="%.1f") # Add a colorbar to a plot
		# plt.xticks(rotation=90)
		# plt.tight_layout(pad=1.7)
		# plt.show()


	def showImageOnMatplotlib(this):
		img= Image.open(this.args.image).convert('RGB')
		#img = mpimg.imread(this.args.image)
		#imgplot = plt.imshow(np.flipud(img), origin='lower') #fliped - [::-1]
		imgplot = plt.imshow(np.flipud(img), origin='lower')
		lines, = plt.plot([3,2,1])
		plt.show()		

	def process(this):
		this.getImage()
		#this.showImageOnMatplotlib() #display x, y and RGB
		this.getxy()
		this.getUniqPixelsLen(this.list_of_pixels)
		this.update_pixel()
		this.getUniqPixelsLen(this.list_of_pixels_updated)
		this.printUniqPixels()
		#this.getUniqPixelsName()
		x,y,z,mraylRGB= this.getCsv()
		this.plotme(x, z, y, mraylRGB, this.args.image, 'SWIA', 'NA')

if __name__ == "__main__":
	ob= imageReader()
	ob.process()
#python imageReader.py --image test.png --debug --x_start 0 --x_end 360 --y_start 7300 --y_end 7500 --casingdiameter 8.5
