import errno
import logging
import os
import shutil
import subprocess
import sys
import argparse
import time


def del_dir(tgt):
    try:
        if os.path.exists(tgt) and os.path.isdir(tgt):
            shutil.rmtree(tgt)
    except OSError as e:
        raise


def clear_dir(tgt, ignore_list=[]):
    for f in os.listdir(tgt):
        if f in ignore_list:
            continue
        path = os.path.join(tgt, f)
        try:
            shutil.rmtree(path)
        except OSError:
            os.remove(path)


def copy_dir(src, dst):
    del_dir(dst)
    try:
        shutil.copytree(src, dst, ignore=shutil.ignore_patterns("poas_env"))
    except OSError as exc:  # python >2.5
        if exc.errno in (errno.ENOTDIR, errno.EINVAL):
            print("Trying alternate method")
            shutil.copy(src, dst)
        else:
            raise


def exec_command(cmd, logger, cwd=None):
    logger.info("Executing - %s" % " ".join(cmd))
    if cwd is not None:
        logger.info("Executing in %s" % cwd)
        proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=cwd)
    else:
        proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    res = proc.communicate()
    if proc.returncode != 0:
        logger.info(res[1])
        sys.exit(-1)
    else:
        for line in res[0].decode(encoding='utf-8').split('\n'):
            logger.info(line)


def symlink(src, tgt):
    try:
        if os.path.exists(tgt) and os.path.isdir(tgt):
            shutil.rmtree(tgt)
        os.symlink(src, tgt)
    except Exception as exc:  # python >2.5
        raise Exception(exc)


def get_logger():
    if not os.path.exists("logs"):
        os.makedirs("logs")
    logger = logging.getLogger("deploy")
    logger.setLevel(logging.DEBUG)

    # stdout handler
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    # create file handler which logs even debug messages
    fh = logging.FileHandler('logs/generic.log', mode="w")
    fh.setLevel(logging.DEBUG)
    fh.setFormatter(formatter)
    logger.addHandler(fh)
    return logger


def read_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--deploy', dest='deploy', default=False, action='store_true',
                        help='create new build and deploy to latest')
    parser.add_argument('--rollback', dest='rollback', default=False, action='store_true',
                        help='rollback to any of the previous builds')
    user_options = parser.parse_args()
    if not (user_options.deploy or user_options.rollback):
        print "Please provide at-least one flag to run the program"
        quit()
    return user_options


class generic(object):
    def __init__(self):
        self.args = read_args()
        self.rsync_src = "/home/dev_poas_user/source/poas"
        self.deploy_dir = "/home/dev_poas_user/public_html"
        self.last_build = self.get_last_build()
        self.next_build = self.get_next_build()
        self.all_builds = self.get_all_builds()
        self.logger = get_logger()
        self.logger.info("*" * 10 + "Starting build" + "*" * 10)
        self.logger.info("Last Build = %s | Next Build = %s" % (self.last_build, self.next_build))
        self.logger.info("Code source dir == %s" % self.rsync_src)

    def get_last_build(self):
        latest = os.path.realpath("%s/latest" % self.deploy_dir)
        return os.path.basename(os.path.normpath(latest))

    def get_next_build(self):
        last_build = [int(i) for i in self.get_last_build().split('.', 2)]
        next_build = last_build
        if last_build[-1] < 9:
            next_build[-1] = next_build[-1] + 1
        elif last_build[-2] < 9:
            next_build[-1] = 0
            next_build[-2] = next_build[-2] + 1
        else:
            next_build[-1] = 0
            next_build[-2] = 0
            next_build[0] = next_build[0] + 1
        next_build = [str(i) for i in next_build]
        next_build = ".".join(next_build)
        return next_build

    def get_all_builds(self):
        if os.path.exists(self.deploy_dir):
            all_dirs = [dI for dI in os.listdir(self.deploy_dir) if os.path.isdir(os.path.join(self.deploy_dir, dI))]
            return sorted([i for i in all_dirs if i[0].isdigit()])
        return []

    def build(self):
        src = "%s/%s/" % (self.deploy_dir, self.last_build)
        dst = "%s/%s/" % (self.deploy_dir, self.next_build)
        self.logger.info("Building %s" % dst)
        copy_dir(src, dst)

        source_dirs = ["Python", "pci-ws"]

        for source_dir in source_dirs:
            cmd = ['rsync', '-av', '--exclude=*.svn', '--exclude=.htaccess', '--exclude=robots.txt',
                   '--exclude=sitemap.xml', '--exclude=sitemap/*',
                   "%s/%s/" % (self.rsync_src, source_dir),
                   "%s/%s/." % (dst, source_dir)]
            exec_command(cmd, self.logger)
        self.logger.info("*" * 10 + "Build Completed" + "*" * 10)

    def fix_permissions(self):
        self.logger.info("Fixing dir and files permissions!")
        cmd = ["find", "%s/%s" % (self.deploy_dir, self.next_build), "-type", "d", "-exec", "chmod", "755", "{}", ";"]
        exec_command(cmd, self.logger)
        cmd = ["find", "%s/%s" % (self.deploy_dir, self.next_build), "-type", "f", "-exec", "chmod", "644", "{}", ";"]
        exec_command(cmd, self.logger)

    def point_release(self, version):
        tmp_tgt = "%s/latest_temp" % self.deploy_dir
        tgt = "%s/latest" % self.deploy_dir
        src = "%s/%s" % (self.deploy_dir, version)
        self.logger.info("Pointing %s to %s" % (tgt, src))
        os.symlink(src, tmp_tgt)
        os.rename(tmp_tgt, tgt)

    def rollback(self):
        print ("version -- created_at")
        for version in self.all_builds:
            created_at = time.ctime(os.path.getctime("%s/%s" % (self.deploy_dir, version)))
            print ("%s -- %s" % (version, created_at))
        print ("*" * 30)
        user_opt = raw_input("Please choose one of the available versions from above to rollback to : ")
        print ("*" * 30)
        if user_opt in self.all_builds:
            self.logger.info("Rolling back to version = %s." % (user_opt))
            self.point_release(user_opt)
            self.logger.info("Rollback completed!")
        else:
            print ("Invalid version selected. Please try again!")
            self.rollback()

    def clear_old_versions(self):
        self.logger.info("Checking if clean-up of old versions needed.")
        self.logger.info("%s versions available" % len(self.all_builds))
        now = time.time()
        if len(self.all_builds) > 5:
            clear_candidates = self.all_builds[0: len(self.all_builds) - 5]
            for version in clear_candidates:
                if os.path.getctime(os.path.join(self.deploy_dir, version)) < now - 5 * 86400:
                    self.logger.info("%s is older than 5 days. Removing" % version)
                    del_dir(os.path.join(self.deploy_dir, version))
                else:
                    self.logger.info("Skipping %s as it is not older than 5 days old." % version)

    def clear_cache(self):
        self.logger.info("Clearing assets and cache!")
        dst = "%s/%s/frontend/web/assets" % (self.deploy_dir, self.next_build)
        clear_dir(dst, ["thumbnails"])
        dst = "%s/%s/frontend/runtime" % (self.deploy_dir, self.next_build)
        clear_dir(dst)

    def deploy(self):
        self.logger.info("Updating source = %s" % self.rsync_src)
        cmd = ["svn", "up", "%s/." % self.rsync_src]
        exec_command(cmd, self.logger)
        self.logger.info("Source updated")
        self.build()
        try:
            self.clear_cache()
        except:
            pass
        self.point_python()
        self.fix_permissions()
        self.configs_changes()
        self.point_release(self.next_build)
        self.clear_old_versions()

    def configs_changes(self):
        pass

    def point_python(self):
        tgt = "%s/%s/poas_env" % (self.deploy_dir, self.next_build)
        src = "/home/dev_poas_user/miniconda3/envs/poas_env/bin/python"
        self.logger.info("Pointing %s to %s" % (tgt, src))
        os.symlink(src, tgt)


if __name__ == "__main__":
    obj = generic()
    if obj.args.deploy:
        obj.deploy()
    else:
        obj.rollback()
