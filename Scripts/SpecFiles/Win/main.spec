# -*- mode: python -*-
import os, sys

block_cipher = pyi_crypto.PyiBlockCipher(key='!!OmNishantKhalidGaneshayNamah!!')

def getPOASRootDir():
    return os.path.join(DISTPATH, '..', '..')

def getpathex():  
    root= getPOASRootDir()
    pathex= [root, os.path.join(root, 'Lib'), os.path.join(root, 'Config'), 'D:\\python2.7.15\\Lib\\site-packages\\scipy\\extra-dll'] 
    print("pathex:", pathex)
    return pathex

def getdatas():
    root= getPOASRootDir()
    datas= [(os.path.join(root, 'Config', 'log.conf'), '.')] 
    print("datas:", datas)
    return datas

a = Analysis([os.path.join('..', '..', 'MBPCharacterization', 'main.py')],
             pathex=getpathex(),
             binaries=[],
             datas=getdatas(),
             hiddenimports=['scipy._lib.messagestream', 'yaml', 'logging.config', 'email.mime.multipart.MIMEMultipart', 'scipy.optimize.curve_fit'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)

pyz = PYZ(a.pure, a.zipped_data, cipher=block_cipher)

exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='main',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=True )

coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               name='main')
