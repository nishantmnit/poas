# -*- mode: python -*-
import os, sys

block_cipher = pyi_crypto.PyiBlockCipher(key='!!OmNishantKhalidGaneshayNamah!!')

def getPOASRootDir():
    return os.path.join(DISTPATH, '../..')

def getpathex():  
    root= getPOASRootDir()
    pathex= [root, root + '/Lib/', root + '/Config/' ] 
    return pathex

def getdatas():
    root= getPOASRootDir()
    datas= [(root + '/Config/log.conf', '.')] 
    return datas

a = Analysis(['../../Filter/filter.py'],
             pathex=getpathex(),
             binaries=[],
             datas=getdatas(),
             hiddenimports=['scipy._lib.messagestream', 'yaml', 'logging.config', 'email.mime.multipart.MIMEMultipart', 'pywt._extensions._cwt', 'pywt._extensions._pywt'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='filter',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=True )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               name='filter')
