# -*- mode: python -*-
import os, sys

block_cipher = pyi_crypto.PyiBlockCipher(key='#(M4r5<o^}m$S@-i;r"}')

def getPOASRootDir():
    return os.path.join(SPECPATH, '..', '..', '..', '..')

def getpathex():  
    root= getPOASRootDir()
    pathex= [root, 'D:\\python2.7.15\\Lib\\site-packages\\scipy\\extra-dll'] 
    return pathex

def getdatas():
    root= getPOASRootDir()
    datas= [('.')] 
    return datas


a = Analysis([os.path.join(getPOASRootDir(), 'Install', 'POASLauncher.py')],
             pathex=getpathex(),
             binaries=[],
             datas=[],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)

pyz = PYZ(a.pure, a.zipped_data, cipher=block_cipher)
               
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name='POAS.exe',
          debug=False,
          strip=False,
          upx=True,
          runtime_tmpdir=None,
          console=True )
