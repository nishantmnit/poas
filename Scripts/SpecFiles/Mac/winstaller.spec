import os, sys

block_cipher = pyi_crypto.PyiBlockCipher(key='#(M4r5<o^}m$S@-i;r"}')

def getPOASRootDir():
    return os.path.join(SPECPATH, '..', '..', '..', '..')

def getpathex():  
    root= getPOASRootDir()
    pathex= [root, os.path.join(root, 'Python', 'Lib'), os.path.join(root, 'Python', 'Config')] 
    return pathex

def getdatas():
    root= getPOASRootDir()
    datas= [(os.path.join(root, 'Install', 'pic1.jpg'),'.'), (os.path.join(root, 'Install', 'pic2.jpg'),'.'), (os.path.join(root, 'Install', 'pic3.jpg'),'.'), (os.path.join(root, 'Install', 'shortcut_icon.ico'),'.'), (os.path.join(root, 'Install', 'shortcut.jpg'),'.'), (os.path.join(root, 'Install', 'installer_gif.gif'),'.'), (os.path.join(root, 'Install', 'SetEnv', '*'), os.path.join('.', 'SetEnv', '')),(os.path.join(root, 'Install', 'SetEnv', 'Release', 'SetEnv.exe', ''), os.path.join('.', 'SetEnv', 'Release', '')) ] 
    #datas= [] 
    return datas


a = Analysis([os.path.join(getPOASRootDir(), 'Install', 'TkGUI.py')],
             pathex=getpathex(),
             binaries=[],
             datas=getdatas(),
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)

pyz = PYZ(a.pure, a.zipped_data, cipher=block_cipher)

exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name='Install.exe',
          debug=False,
          strip=False,
          upx=True,
          runtime_tmpdir=None,
          console=True )
