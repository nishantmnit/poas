import os
import sys
import warnings

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
LOG_DIR = os.path.join(ROOT_DIR, 'log')
DEBUG_DIR = os.path.join(ROOT_DIR, 'debug')


if not sys.warnoptions:
    warnings.simplefilter("ignore")
