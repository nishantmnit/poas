import pymongo


def getMongoHost():
    return 'localhost'


def getDbCon(Db=None, Coll=None):
    # this.con= pymongo.MongoClient("mongodb+srv://ambreganesh:Chunnu31@cluster0-ie1j6.mongodb.net/test?retryWrites
    # =true")
    con = pymongo.MongoClient()
    if Db is not None:
        con = con[Db]
        if Coll is not None:
            con = con[Coll]
    return con
