#! /usr/bin/python
import matplotlib
#matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
from Utils import *


def plotGraph2D(axes, prop, logger):
    fig, ax = plt.subplots(figsize=(15,7))
    ax.set_facecolor('k')
    plt.locator_params(axis='both', nticks=10, tight=True, nbins=10)
    for p in axes:
        if p['type'] == 'scatter': plt.scatter(*p['axesPoints'], **p['axesProp'])
        else: plt.plot(*p['axesPoints'], **p['axesProp'])
    if 'title'  in prop: ax.set_title(prop['title'], fontsize=10)
    if 'xlabel' in prop: ax.set_xlabel(prop['xlabel'], fontsize=10)
    if 'ylabel' in prop: ax.set_ylabel(prop['ylabel'], fontsize=10)
    plt.legend(loc=prop['legendLoc'] if 'legendLoc' in prop else 1, prop={'size': 10})
    # plt.grid() #dotted lines in graph
    saveGraph(plt, prop['graphPath'], logger)
    #plt.show()

def plotGraph2Din3D(axes, prop, logger):
    mpl.rcParams['legend.fontsize'] = 8
    fig = plt.figure(figsize=(12,10))
    ax = fig.gca(projection='3d')
    colors=['r', 'g', 'b', 'k', 'v']
    markers=['^', '>', 'o', 's', 'v']
    i= 0 
    for p in axes:
        if p['type'] == 'scatter':
            ax.scatter(*p['axesPoints'], zdir='z', **p['axesProp'])
            #ax.scatter(p['x'], p['y'], p['y'], zdir='z', marker=markers[i], label=p['label'])
        else:
            plt.plot(*p['axesPoints'], zs=0, zdir='z', **p['axesProp'])
            #plt.plot(p['x'], p['y'], zs=0, zdir='y', label=p['label'])
        i += 1
    if 'title'  in prop: ax.set_title(prop['title'], fontsize=8)
    if 'xlabel' in prop: ax.set_xlabel(prop['xlabel'], fontsize=8)
    #if 'ylabel' in prop: ax.set_ylabel(prop['ylabel'], fontsize=8)
    if 'zlabel' in prop: ax.set_zlabel(prop['zlabel'], fontsize=8)
    #ax.w_yaxis.set_ticklabels([])
    ax.legend(loc=2) # upper left corner
    saveGraph(fig, prop['graphPath'], logger)
    #http://jeffskinnerbox.me/notebooks/matplotlib-2d-and-3d-plotting-in-ipython.html
    #plt.show()

def plotGraph3D(axes, prop, logger):
    mpl.rcParams['legend.fontsize'] = 8
    fig = plt.figure(figsize=(12,10))
    ax = fig.gca(projection='3d')
    for p in axes:
        if p['type'] == 'scatter':
            ax.plot(p['x'], p['y'], p['z'], label=p['label'])
        else:
            ax.plot(p['x'], p['y'], p['z'], label=p['label'])
    if 'title'  in prop: ax.set_title(prop['title'], fontsize=8)
    if 'xlabel' in prop: ax.set_xlabel(prop['xlabel'], fontsize=8)
    if 'ylabel' in prop: ax.set_ylabel(prop['ylabel'], fontsize=8)
    if 'zlabel' in prop: ax.set_zlabel(prop['zlabel'], fontsize=8)
    #ax.w_zaxis.set_ticklabels([])
    ax.legend(loc=2) # upper left corner
    saveGraph(fig, prop['graphPath'], logger)
    #http://jeffskinnerbox.me/notebooks/matplotlib-2d-and-3d-plotting-in-ipython.html
    #plt.show()

def saveGraph (plt, graphPath, logger):
    ensureDir(graphPath)
    graphPathTmp= "%s.tmp.png" %(graphPath)
    try:
        plt.savefig(graphPathTmp, dpi=150)
        shutil.move(graphPathTmp, graphPath)
    except Exception as e:
        #Copy blank image to path
        logger.error("%s,%s" %(e.message, e.args))
        raise processError("Error occured in Graph savefig:%s,%s" %(e.message, e.args))
    else:
        if os.path.exists(graphPath):
            #print "Graph:%s" %(graphPath)
            pass
        else:    
            logger.error("Graph not found:%s" %(graphPath))
            #Copy blank image to path
            raise processError("Graph not found:%s, copied blank graph", (type, graphPath))
