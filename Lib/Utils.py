#! /usr/bin/python
import cStringIO
import calendar
import csv
import datetime
import fnmatch
import logging
import logging.config
import os
import shutil
import smtplib
import subprocess
import sys
import yaml
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from time import strftime


class Error(Exception):
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return "\n!! Error => " + self.msg + "\n"


def sendMail(frm, to, sub, body):
    msg = MIMEMultipart('alternative')
    msg['Subject'] = sub
    msg['From'] = frm
    msg['To'] = to
    msg.attach(MIMEText(body, 'html'))
    s = smtplib.SMTP('127.0.0.1')
    s.sendmail(frm, to, msg.as_string())
    s.quit()
    return 0


def getPOASRootDir():
    application_path = os.path.dirname(os.path.realpath(__file__))
    if getattr(sys, 'frozen', False):
        application_path = os.path.join(os.path.dirname(sys.executable), '..',
                                        '..')  # change to .. for -onefile executable
    return os.path.join(application_path, '..')


def getLogger(qualname, master_module=False):
    configFile = os.path.join(getPOASRootDir(), 'Config', 'log.conf')
    raiseErrIfFileNotFound("Logging configuration file not found", configFile)
    dir = (os.path.join(getPOASRootDir(), 'Log') if os.environ["LOG_DIR"] == "" else os.environ["LOG_DIR"])
    setLogFile(qualname, dir, master_module)
    ensureDir(os.environ['PyLogfile'])
    logging.config.fileConfig(configFile)
    logger = logging.getLogger('PyLogfile')
    # logger.debug("Log file = %s" % os.environ['PyLogfile'])
    return logger


def setLogFile(qualname, dir, master_module):
    qualname = qualname.replace(" ", "_")
    if ('PyLogfile' in os.environ and (
            os.environ['PyLogfile'] is not None or len(str(os.environ['PyLogfile']).strip()) != 0)
            and not master_module):
        logFile = os.environ['PyLogfile']
    else:
        if 'PyLogfile' in os.environ and fileExists(os.environ['PyLogfile']):
            try:
                os.remove(os.environ['PyLogfile'])
            except Exception as e:
                pass
        os.environ['PyLogfile'] = os.path.join(dir, "%s_%s.log" % (
            qualname, datetime.datetime.now().strftime("%Y%m%d%H%M%S")))
    return


def getCodeDir(programName):
    d = os.path.dirname(programName)
    # f= os.path.basename(programName)
    if d == '':
        d = os.getcwd()
    return d


def validateDate(date):
    try:
        return datetime.datetime.strptime(date, "%d-%m-%Y")
    except ValueError:
        processError("Incorrect DeliveryDt (%s) format, should be 'DD-MM-YYYY'" % (date))


def defined(variable):
    return False if (variable is None or len(str(variable).strip()) == 0) else True


def raiseErrIfFileNotFound(msg, filePath):
    if not os.path.isfile(filePath):
        raise processError("%s file doesn't exist" % (filePath))


def definedInHash(hsh, key):
    return True if (key in hsh and defined(hsh[key])) else False


def fileExists(File):
    return os.path.exists(File)


def findAllFiles(mydir, startsWith=''):
    try:
        return sorted([f for f in os.listdir(mydir) if fnmatch.fnmatch(f, '*%s.bcp' % (startsWith))])
    except:
        raise processError("%s directory doesn't exist" % (mydir))


def moveFiles(mydir):
    try:
        for f in os.listdir(mydir):
            if f.endswith('.bcp'):
                f = "%s/%s" % (mydir, f)
                shutil.move(f, "%s.done" % (f))
    except:
        raise processError("Failed to move file %s to done" % (f))


def processError(msg='', exitCode=1):
    try:
        raise Error("{0}".format(msg))
    except Error as e:
        sys.stderr.write(str(e))
        sys.exit(exitCode)


def readConfig(configFile):
    if (fileExists(configFile)):
        return yaml.safe_load(open(configFile))
    else:
        processError("Config file -- %s not found!" % (configFile))


def log(msg=''):
    logger = getLogger('PyLogfile')
    logger.info('%s' % (msg))


def getDateAsInt():
    return int(datetime.datetime.now().strftime("%Y%m%d"))


def getDateAsDate():
    return datetime.datetime.now().strftime("%m/%d/%Y %H:%M:%S")


def convertDateToInt(date):
    return int(strftime("%M %d %Y", date))


def ensureDir(file):
    dir = os.path.dirname(file)
    if not os.path.exists(dir):
        os.makedirs(dir)


def removeDir(dir):
    try:
        shutil.rmtree(dir)
    except:
        None


def mergeFiles(dir, file, hdr, printHdr):
    with open(file, 'w') as outfile:
        if (printHdr == "yes"):
            outfile.write(hdr)
        for fname in os.listdir(dir):
            if fnmatch.fnmatch(fname, 'part*'):
                with open('%s/%s' % (dir, fname)) as infile:
                    for line in infile:
                        outfile.write(line)


def copy(src, dest):
    try:
        shutil.copy2(src, dest)
    except:
        processError('Error while trying to copy %s to %s' % (src, dest))


def runNamedMethod(obj, method, *args):
    return getattr(obj, method)(*args)


def readFile(sc, file, delim, cols):
    try:
        sqlContext = SQLContext(sc)
        if not os.path.isfile(file):
            log("%s file not found. Assuming there is no output for this file." % (file))
            rdd = sc.emptyRDD()
            cols = structSchema(cols)
        else:
            log("Loading file = %s" % (file))
            rdd = sc.textFile(file).map(lambda x: x.split(delim), )
            if rdd.isEmpty():
                log("%s file is blank" % (file))
                cols = structSchema(cols)
            return sqlContext.createDataFrame(rdd, schema=cols)
    except Exception as e:
        print (e)
        raise processError("Can not read file = %s" % (file))


def row2csv(row):
    buffer = cStringIO.StringIO()
    writer = csv.writer(buffer)
    writer.writerow([str(s).encode("utf-8") for s in row])
    buffer.seek(0)
    return buffer.read().strip()


def execShell(cmd):
    proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    std_out, std_err = proc.communicate()
    returncode = proc.returncode
    if returncode != 0:
        err_msg = "ErrorCode:%s, STDOUT:%s, ErrorMessage:%s" % (returncode, std_out.strip(), std_err.strip())
    return returncode, std_out.strip(), std_err.strip()


def datetimeStrToUTC(datetime_str, dateformat):
    d = datetime.datetime.strptime(datetime_str, dateformat)
    utc_timestamp = calendar.timegm(d.timetuple())
    # datetime= datetime.datetime.utcfromtimestamp(utc_timestamp)
    return utc_timestamp


def datetimeUTCToDatetimeobj(datetime_utc):
    dt = datetime.datetime.utcfromtimestamp(datetime_utc)
    return dt  # datetime object


def datetimeObjToUTC(datetime_obj):
    datetime_utc = calendar.timegm(datetime_obj.timetuple())
    return datetime_utc  # datetime object


def getSamplingFrequency(datetime1, datetime2):
    dt1 = datetime.datetime.utcfromtimestamp(datetime1)
    dt2 = datetime.datetime.utcfromtimestamp(datetime2)
    # dt1= datetime.datetime.strptime(datetime1, this.signalDateTimeFormat) #'%m/%d/%Y %H:%M:%S' set in init
    # dt2= datetime.datetime.strptime(datetime2, this.signalDateTimeFormat)
    diffInSeconds = (dt2 - dt1).total_seconds()
    return (diffInSeconds, 1. / diffInSeconds)
